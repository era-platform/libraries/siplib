
%% ====================================================================
%% Types
%% ====================================================================

-include("../include/r_sip_nk.hrl").
-include("r_sip_log.hrl").

-define(Auto100answer, false). % test

%% ====================================================================
%% Define modules
%% ====================================================================

-define(APP, era_sip).

-define(APP_BOOT, era_boot).
%-define(APP_MIXER, era_mixer). % defined in r_sip_ivr.hrl
-define(APP_TRACE, era_trace).
-define(APP_SCRIPT, era_script).
-define(APP_MG, era_mg).

-define(U, r_sip_utils).
-define(SUPV, r_sip_supv).

-define(SERVERS, r_sip_responsible_servers).
-define(REGISTRAR, r_sip_registrar).
-define(DC, r_sip_domaincenter).
-define(ACCOUNTS, r_sip_accountstore).
-define(NUMBERPLAN, r_sip_numberplan).
-define(CALLEE, r_sip_callee).
-define(REPRESENTATIVE, r_sip_representative).

-define(LOCALDOMAIN, r_sip_localdomain).
-define(ROUTE, r_sip_do_route).
-define(FILTER, r_sip_do_filter).
-define(LOGGING, r_sip_do_log).
-define(BESTIFACE, r_plug_bestinterface_lib).
-define(TRANSLIT, r_sip_translit).
-define(NOREG, r_sip_noreg_srv).
-define(CsWorkerSupv, r_sip_b2bua_callstats_worker_supv).
-define(CsWorkerSrv, r_sip_b2bua_callstats_worker_srv).
-define(SipWorkerSupv, r_sip_worker_supv).
-define(SipWorkerSrv, r_sip_worker_srv).

-define(SipAlg,era_sip_alg).

-define(FILTER_SUPV, era_env_sbc_filter_supv).
-define(FILTER_SRV, era_env_sbc_filter_srv).

-define(FILTER_STORE, r_sip_store_srv).

-define(STAT, r_sip_stat).
-define(STAT_SUPV, r_sip_stat_supv).
-define(STAT_STORE, r_sip_stat_store_adapter).
-define(STAT_STORE_Q, r_sip_stat_store_srv).
-define(STAT_REQ, r_sip_stat_req).
-define(STAT_FACADE, r_sip_stat_utils).
-define(STAT_LOGEXTRACTOR, r_sip_stat_log_extractor).

-define(SIP_SRV, r_sip_srv).
-define(DLG_STORE, r_sip_store_dlg_srv).
-define(SIPSTORE, r_sip_store_srv).
-define(WATCHER, r_sip_watcher_srv).
-define(CFG, r_sip_config).

-define(FSMT, r_sip_fsm_translator).

-define(SIP_B2B_NOTIFY, r_sip_b2bua_notify).
-define(SIP_IVR_CB, r_sip_ivr_cb).
-define(SIP_IVR_FSM, r_sip_ivr_fsm).
-define(SIP_CONF_FSM, r_sip_conf_fsm).
-define(SIP_B2B_FSM, r_sip_b2bua_fsm).

-define(EU, basiclib_utils).
-define(WORKER, basiclib_worker_srv).

-define(ENV, basiclib_srv).
-define(ENVCERT, r_env_certificate).
-define(ENVCFG, r_env_config).
-define(ENVCFGU, r_env_config_utils).
-define(ENVNAMING, r_env_naming).
-define(ENVGLOBAL, r_env_global).
-define(ENVGLOBALX, r_env_global_x).
-define(ENVCROSS, r_env_cross).
-define(ENVCOPY, r_env_copier).
-define(ENVSTORE, basiclib_store).
-define(MONITOR, basiclib_monitor_srv).
-define(ENVTRACE, r_env_trace).
-define(ENVMULTICALL, r_env_multicall).
-define(ENVPING, r_env_ping).
-define(ENVRPC, r_env_rpc).
-define(ENVMODIFIER, r_env_modifier).
-define(ENV_SCRVAR_MAN, r_env_script_vars_manager).
-define(ENV_STRATEGY_SYNC, r_env_sync_strategy_srv).
-define(URLENCODE, r_env_urlencode).
-define(ENVCALL, r_env_call).
-define(ENVLIC, r_env_lic).
-define(ENVDC, r_env_dccache).
-define(EnvRoles, r_env_roles).
-define(EnvRoleU, r_env_role_utils).
-define(ENVEVENTGATE, r_env_event).
-define(ENVSNICERT, r_env_sni_certificate).

-define(Rfile, r_env_wrapper_file).
-define(Rfilelib, r_env_wrapper_filelib).
-define(Rzip, r_env_wrapper_zip).

-define(B2BUA_UTILS, r_sip_b2bua_utils). % for cluster services (extract callid)
-define(B2BUA_BINDS, r_sip_b2bua_fsm_utils_bindings).
-define(B2BUA_STAT, r_sip_b2bua_stat).
-define(ESG_STAT, r_sip_esg_stat).
-define(CONF_STAT, r_sip_conf_stat).
-define(IVR_STAT, r_sip_ivr_stat).
-define(SG_STAT, r_sip_sg_stat).
-define(PROMPT_STAT, r_sip_prompt_stat).

-define(EXT_REG, r_sip_esg_register).
-define(EXT_REGSRV, r_sip_esg_reg_srv).
-define(EXT_UTILS, r_sip_esg_utils).

%% ====================================================================
%% Define other
%% ====================================================================

% ------------------------
% responses
-define(ReplyOpts(Code,Reason), {Code, [{reason,{sip,Code,Reason}}]}).
-define(ReplyOpts(Code,Reason,Append), {Code, [{reason,{sip,Code,Reason}}] ++ Append}).
-define(ReplyOptsEx(Code,RPhrase,Reason), {Code, [{reason_phrase,?EU:to_binary(RPhrase)}, {reason,{sip,Code,Reason}}]}).
-define(ReplyOptsEx(Code,RPhrase,Reason,Append), {Code, [{reason_phrase,?EU:to_binary(RPhrase)}, {reason,{sip,Code,Reason}}] ++ Append}).
%-define(ReplyOpts(Code,Reason), {Code, [{reason, ?EU:to_binary(Reason)}]}).
%-define(ReplyOpts(Code,RPhrase,Reason), {Code, [{reason_phrase,?EU:to_binary(RPhrase)}, {reason,?EU:to_binary(Reason)}]}).
%
-define(Ok200(Reason), ?ReplyOpts(200,Reason)).
-define(BadRequest(Reason), ?ReplyOpts(400,Reason)).
-define(Forbidden(Reason), ?ReplyOpts(403,Reason)).
-define(NotFound(Reason), ?ReplyOpts(404,Reason)).
-define(MethodNotAllowed(Reason), ?ReplyOpts(405,Reason)).
-define(RequestTimeout(Reason), ?ReplyOpts(408,Reason)).
-define(MediaNotSupported(Reason), ?ReplyOpts(415,Reason)).
-define(IntervalTooBrief(MinExpires,Reason), {423, [{add,{<<"Min-Expires">>,MinExpires}}, {reason,{sip,423,Reason}}]}).
-define(TemporarilyUnavailable(Reason), ?ReplyOpts(480,Reason)).
-define(CallLegTransDoesNotExist(Reason), ?ReplyOpts(481,Reason)).
-define(LoopDetected(Reason), ?ReplyOpts(482,Reason)).
-define(TooManyHops(Reason), ?ReplyOpts(483,Reason)).
-define(BusyHere(Reason), ?ReplyOpts(486,Reason)).
-define(RequestTerminated(Reason), ?ReplyOpts(487,<<"Request Terminated">>,Reason)).
-define(Pending(Reason), ?ReplyOpts(491,Reason)).
-define(InternalError(Reason), ?ReplyOpts(500,Reason)).
-define(InternalErrorEx(Reason,Append), ?ReplyOptsEx(500,Reason,Append)).
-define(ServiceUnavailable(Reason), ?ReplyOpts(503,Reason)).
-define(Decline(Reason), ?ReplyOpts(603,Reason)).


-define(ConfPrefix, "conf-").
-define(SelPrefix, "conf-sel-").
-define(IvrPrefix, "ivr-").
-define(PromptPrefix, "prompt-").
-define(DummyIvrPrefix, "dummy-ivr-").
-define(HuntPrefix, "hunt-").

%% ====================================================================
%% Define logs
%% ====================================================================



