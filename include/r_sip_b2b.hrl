

%% ====================================================================
%% Types
%% ====================================================================
% ----------------------
-record(state_forking, {
    version=1,
    pid :: pid(),
    dialogid :: binary(),
    dialogidhash :: integer(), % #370
    acallid :: binary(),
    acallidhash :: integer(), % #370
    invite_id :: binary(), % RP-385
    invite_ts :: integer(), % id/timestamp
    map, % #{app, dlgnum, init_req, opts, store_data, last_response, last_out_callid_idx, applied_redirects}
    use_media=true, % if media should be used or not
    media,
    total_timer_ref :: reference(),
    ref :: reference(),
    %
    a, % #side{}
    b, % #side{} optinally filled
    %
    b_rule=[],
    b_prep=[],
    b_act=[],
    b_fin=[],
    fixed_forks = false,
    %
    usedtags=[],
    %
    starttime,
    finaltime,
    %
    stopreason
  }).

% ----------------------
-record(state_dlg, {
    version=1,
    pid :: pid(),    
    dialogid :: binary(),
    dialogidhash :: integer(), % #370
    acallid :: binary(), % #370,
    acallidhash :: integer(), % #370
    invite_id :: binary(), % RP-385
    invite_ts :: integer(), % id/timestamp
    map, % #{app, dlgnum, init_callid, opts, store_data, last_out_callid_idx, timer_migration_ref}
    use_media=true, % if media should be used or not
    media, % #media{}
    lstate, % map #{} for every sub state (migration, etc) | undefined
    reinvite, % #reinvite{}
    refer, % #refer{}
    timer_ref,
    %
    starttime,
    answertime,
    finaltime,
    %
    a,
    b,
      %
    stopreason
  }).

% ----------------------
-record(side_fork, {
    callid,
    rhandle,
    dhandle, % only after response (1xx too)
    opts,
    %
    remoteuri,
    remotetag,
    requesturi,
    remotecontacts, % RP-1297
    %
    localuri,
    localtag,
    localcontact,
    localsdp,
    %
    callednum :: {S::atom(),U::binary(),D::binary()}, % real routed aor of call (sipuser+domain if internal | internalpbx, number+domain if others)
    realnumaor :: {S::atom(),U::binary(),D::binary()}, % RP-726 real side numbered aor (for record rule purposes)
    sipuser :: undefined | [{Key::atom(),Value::binary()}], % internal sipuser account info (login,name,phone,domain,ext)
    host,
    redircond, % dc redirect rules, loaded once at routing
    dir,
    %
    cmnopts,
    inviteopts,
    %
    allow_events = [], % TODO: update on 2xx response too if header is found
    %
    rule_timeout,
    rule_timer_ref,
    last_response_code,
    resp_timer_ref,
    %
    starttime,
    finaltime,
    res
  }).

% ----------------------
-record(side, {
    rhandle,
    dhandle,
    callid,
    opts,
    %
    remoteuri,
    remotetag,
    remotecontacts,
    remotesdp,
    %
    localuri,
    localtag,
    localcontact,
    localsdp,
    last_remote_party,
    %
    callednum :: {S::atom(),U::binary(),D::binary()}, % real routed aor of call (sipuser+domain if internal | internalpbx, number+domain if others)
    realnumaor :: {S::atom(),U::binary(),D::binary()}, % RP-726 real side numbered aor (for record rule purposes)
    sipuser :: undefined | [{Key::atom(),Value::binary()}], % internal sipuser account info (login,name,phone,domain,ext)
    host,
    is_rec = false :: boolean(),
    allow_events = [], % TODO: update on every request/response if header is found
    %
    dtmf2send = [] :: [map()],
    dtmf2send_timerref = undefined :: reference(),
    dtmf2send_ref = undefined :: reference(),
    %
    starttime,
    answertime,
    finaltime
  }).

% ----------------------
-record(reinvite, {
    xreq,
    fwdside,
    timestart,
    timer_ref,
    xrhandle,
    xcallid,
    xltag,
    xrsdp,
    yrhandle,
    ycallid,
    yltag,
    ylsdp,
    y,
    offer_opts
  }).

% ----------------------
-record(refer, {
    xreq :: #sipmsg{},
    timestart,
    stage :: binary(),
    timer_ref :: reference(),
    xrhandle,
    xcallid :: binary(),
    xltag :: binary(),
    yrhandle,
    ycallid :: binary(),
    yltag :: binary(),
    referring_dlg_info :: undefined | map() % acallid, acallid_hash, iid, its, [dlgid, dlgid_hash]
  }).


%% ====================================================================
%% Define modules
%% ====================================================================

-define(SIPAPP, r_sip_b2bua).

-define(B2BUA, r_sip_b2bua_cb).
-define(B2BUA_SUPV, r_sip_b2bua_supv).
-define(B2BUA_ROUTER_INVITE, r_sip_b2bua_router_invite).
-define(B2BUA_ROUTER_TEST, r_sip_b2bua_router_test).
-define(B2BUA_ROUTER_SUBSCRIBE, r_sip_b2bua_router_subscribe).
-define(B2BUA_MEDIA, r_sip_b2bua_media).
-define(B2BUA_REFR, r_sip_b2bua_refresher).
-define(B2BUA_CTX, r_sip_b2bua_context).

-define(AUTH,r_sip_b2bua_auth).
-define(FROM,r_sip_from).
-define(REDIRECT,r_sip_redirect).
-define(USRSTATE, r_sip_b2bua_userstate).
-define(CALLSTAT, r_sip_b2bua_callstats_srv).
-define(SUBSCRIPTIONS, r_sip_subscriptions).

-define(B2BUA_DLG, r_sip_b2bua_fsm).

-define(FORKING, r_sip_b2bua_fsm_forking).
-define(DIALOG, r_sip_b2bua_fsm_dialog).
-define(ACKWAITING, r_sip_b2bua_fsm_ackwaiting).
-define(REINVITING, r_sip_b2bua_fsm_reinviting).
-define(SESSIONCHANGING, r_sip_b2bua_fsm_sessionchanging).
-define(REFERING, r_sip_b2bua_fsm_refering).
-define(MIGRATING, r_sip_b2bua_fsm_migrating).

-define(REC, r_sip_b2bua_fsm_dialog_rec).
-define(B2BUA_ASR, r_sip_b2bua_fsm_utils_asr).
-define(B2BUA_REC, r_sip_b2bua_fsm_utils_rec).
-define(B2BUA_DTMF, r_sip_b2bua_fsm_utils_dtmf).

-define(FORKING_UTILS, r_sip_b2bua_fsm_forking_utils).
-define(FORKING_PICKUP, r_sip_b2bua_fsm_forking_pickup).
-define(FORKING_REDIRECT, r_sip_b2bua_fsm_forking_redirect).

-define(FSM_EVENT, r_sip_b2bua_fsm_event).

-define(REPLACES, r_sip_b2bua_fsm_refering_replaces).

-define(EVENT, r_sip_b2bua_eventing_log).
-define(EVENT_CDR, r_sip_b2bua_eventing).
-define(EVENTHANDLER, r_sip_b2bua_event_handler).
-define(EVENTMETA, r_sip_b2bua_event_meta).

%% ====================================================================
%% Define other
%% ====================================================================

-define(FORKING_STATE, 'forking').
-define(ACKWAITING_STATE, 'ackwaiting').
-define(REINVITING_STATE, 'reinviting').
-define(DIALOG_STATE, 'dialog').
-define(STOPPING_STATE, 'stopping').
-define(SESSIONCHANGING_STATE, 'sessionchanging').
-define(REFERING_STATE, 'refering').
-define(MIGRATING_STATE, 'migrating').

-define(DEFAULT_FORK_RULE_TIMEOUT, 30000).
-define(DEFAULT_FORK_100_TIMEOUT, 30000).
-define(DEFAULT_FORK_180_TIMEOUT, 30000).
-define(DEFAULT_FORK_182_TIMEOUT, 300000).
-define(DEFAULT_FORK_183_TIMEOUT, 300000).
-define(STOPPING_TIMEOUT, 2000).
-define(STORE_TIMEOUT, 300000).
-define(STORE_REFRESH, 200000).
-define(REINVITE_TIMEOUT, 16000).
-define(ACKWAITING_TIMEOUT, ?REINVITE_TIMEOUT).
-define(REINVITING_TIMEOUT, 3000). % per one re-inviting attempt
-define(REINVITING_TOTAL_TIMEOUT, 15000). % for all attempts
-define(REFER_TRY_TIMEOUT, 30000).
-define(REFER_WAIT_TIMEOUT, 60000).
-define(DIALOG_TIMEOUT, 7200000).
-define(MIGRATION_TIMEOUT, 16000).

-define(ProxyToInsider, <<"proxy_to_insider_srv">>).

-define(CHECKLIMIT, check_by_options_on_limit).
-define(CHECKLIMIT_K(Pid), {<<"register_req">>,Pid}).

-define(GateIface(Iface), {site_gate_iface,Iface}).
