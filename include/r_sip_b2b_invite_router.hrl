
%% ====================================================================
%% Define modules
%% ====================================================================

-define(IR_UTILS, r_sip_b2bua_router_utils).
-define(IR_EVENT, r_sip_b2bua_router_event).
-define(IR_TRACE, r_sip_b2bua_router_trace).

-define(IR_RULE, r_sip_b2bua_router_rule).
-define(IR_GROUP, r_sip_b2bua_router_rule_group).

-define(IR_TEST, r_sip_b2bua_router_test).
-define(IR_SUBSCRIBE, r_sip_b2bua_router_subscribe).
-define(IR_INVITE, r_sip_b2bua_router_invite).

-define(IR_REFERRED, r_sip_b2bua_router_invite_referred).
-define(IR_URIRULES, r_sip_b2bua_router_invite_urirules).
-define(IR_INSIDE, r_sip_b2bua_router_invite_inside).
-define(IR_OUTSIDE, r_sip_b2bua_router_invite_outside).
-define(IR_FEATURE, r_sip_b2bua_router_invite_feature).
-define(IR_FEATURE_SVC, r_sip_b2bua_router_invite_feature_service).

%% ====================================================================
%% Define other
%% ====================================================================

-define(ABSOLUTE, <<"absolute">>).
-define(UNREGISTERED, <<"unregistered">>).
-define(BUSY, <<"busy">>).

-type aor() :: {Scheme::atom(),Username::binary(),Domain::binary()}.