
% ----------------------
-record(state_conf, {
    version=1,
    pid :: pid(),
    confid :: binary(),
    confidhash :: integer(), % #370
    confnum,
    room, % AOR
    invite_ts :: integer(), % id/timestamp
    ownerpid,
    map, % #{app, dlgnum, init_callid, opts, store_data, last_out_callid_idx, timer_migration_ref}
    media, % #media{}
    lstate, % map #{} for every sub state (migration, etc)| undefined
    timer_ref,
    ref, % unique internal reference
    usedtags = [], % used local/remote tags not to generate already existing
    video = false,
    %
    autoclose = true :: true | false,
    starttime,
    finaltime,
    %
    participants = [], % list of [{CallId,#side{}}]
    forks = [], % list of [{CallId,#{}}]
    topology = #{},
      %
    stopreason
  }).

% ----------------------
-record(side, {
    rhandle,
    dhandle,
    callid,
    dir, % in | out
    req, % #sipmsg
    %
    participantid, % selector participant id
    sel_opts, % selector options (mode, topology)
    %
    remoteuri,
    remotetag,
    remotecontacts,
    remotesdp,
    remoteparty,
    %
    localuri,
    localtag,
    localcontact,
    %
    refer_callid,
    %
    starttime,
    answertime,
    finaltime
  }).

% ----------------------
-record(side_fork, {
    callid,
    rhandle,
    dhandle,
    %
    participantid, % selector participant id
    sel_opts, % selector options (mode, topology)
    %
    remoteuri,
    remotetag,
    remotecontacts,
    remotesdp,
    requesturi,
    %
    localuri,
    localtag,
    localcontact,
    %
    cmnopts,
    inviteopts,
    %
    rule_timeout,
    rule_timer_ref,
    last_response_code,
    resp_timer_ref,
    %
    starttime,
    finaltime,
    res
  }).

% ----------------------
-record(sel_opts, {
    mode_max = <<"sendrecv">> :: binary(), % <<"sendrecv">> | <<"recvonly">> | <<"inactive">>,
    spk = true :: boolean(),
    mic = true :: boolean()
  }).

% ----------------------
-record(state, {
    auto_check
}).

%% ====================================================================
%% Define modules
%% ====================================================================

-define(SIPAPP, r_sip_conf).

-define(CONF, r_sip_conf_cb).
-define(CONF_SUPV, r_sip_conf_supv).
-define(CONF_UTILS, r_sip_conf_utils).

-define(CONF_ROUTER, r_sip_conf_invite_router).
-define(CONF_MEDIA, r_sip_conf_media).

-define(EVENT, r_sip_conf_eventing_log).
-define(EVENT_CDR, r_sip_conf_eventing).
-define(EVENTHANDLER, r_sip_conf_event_handler).
-define(EVENTMETA, r_sip_conf_event_meta).

-define(CONF_SRV, r_sip_conf_fsm).

-define(ACTIVE, r_sip_conf_fsm_active).

-define(FSM_EVENT, r_sip_conf_fsm_event).

-define(ACTIVE_UTILS, r_sip_conf_fsm_active_utils).
-define(ACTIVE_REFER, r_sip_conf_fsm_active_refer).
-define(ACTIVE_CALL, r_sip_conf_fsm_active_call).
-define(TOPOLOGY, r_sip_conf_fsm_active_topology).
-define(ACTIVE_PLAY, r_sip_conf_fsm_active_play).
-define(ACTIVE_MIGRATING, r_sip_conf_fsm_active_migrating).

-define(CONFSTORE, r_sip_conf_store_srv).

%% ====================================================================
%% Define other
%% ====================================================================

-define(ACTIVE_STATE, active).
-define(STOPPING_STATE, stopping).

-define(STORE_TIMEOUT, 300000).
-define(STORE_REFRESH, 200000).
-define(REFER_TIMEOUT, 60000).
-define(STOPPING_TIMEOUT, 2000).

-define(FORK_TIMEOUT_MSG, 'fork_timeout').

-define(CONF_TIMEOUT_MSG, {conference_timeout}).
-define(CONF_TIMEOUT, 5 * 3600 * 1000).
