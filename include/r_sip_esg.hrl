
%% ====================================================================
%% Types
%% ====================================================================
% ----------------------
-record(state_forking, {
    version=1,
    dialogid :: binary(),
    dialogidhash :: integer(), % #370
    map, % #{app, dlgnum, init_req, opts, store_data, last_response, last_out_callid_idx, applied_redirects}
    acallid :: binary(),
    use_media=true, % if media should be used or not
    media,
    total_timer_ref :: reference(),
    ref :: reference(),
    %
    stopfuns = [], % funs to be applied on switching to STOPPING,
    exitfuns = [], % funs to be applied on exit fsm,
    %
    a, % #side{}
    b, % #side{} optinally filled
    %
    b_rule=[],
    b_prep=[],
    b_act=[],
    b_fin=[],
    %
    forking_replaces=undefined, % if forking state got invite+replace
    %
    usedtags=[],
    %
    starttime,
    finaltime
  }).

% ----------------------
-record(state_dlg2, {
    version=1,
    dialogid :: binary(),
    dialogidhash :: integer(), % #370
    map, % #{app, dlgnum, init_callid, opts, store_data, last_out_callid_idx, timer_migration_ref}
    use_media=true, % if media should be used or not
    media, % #media{}
    lstate, % map #{} for every sub state (migration, etc)| undefined
    reinvite, % #reinvite{}
    refer, % #refer{}
    replace, % #replace{}
    timer_ref,
    %
    stopfuns = [], % funs to be applied on switching to STOPPING,
    exitfuns = [], % funs to be applied on exit fsm,
    %
    starttime,
    answertime,
    finaltime,
    %
    a,
    b,
    %
    forking_replaces, % if forking state got invite+replace
      %
    stopreason
  }).

-define(StateDlg, state_dlg2).

% ----------------------
-record(side_fork, {
    callid,
    rhandle,
    calldir,
    %
    remoteuri,
    remotetag,
    requesturi,
    %
    localuri,
    localtag,
    localcontact,
    %
    cmnopts,
    inviteopts,
    %
    rule_timeout,
    rule_timer_ref,
    last_response_code,
    resp_timer_ref,
    %
    starttime,
    finaltime,
    res
  }).

% ----------------------
-record(side, {
    rhandle,
    dhandle,
    callid,
    calldir,
    %
    remoteuri,
    remotetag,
    remotecontacts,
    remotesdp,
    %
    localuri,
    localtag,
    localcontact,
    last_remote_party,
    %
    starttime,
    answertime,
    finaltime
  }).

% ----------------------
-record(reinvite, {
    xreq,
    fwdside,
    timestart,
    timer_ref,
    xrhandle,
    xcallid,
    xltag,
    xrsdp,
    yrhandle,
    ycallid,
      yltag,
    ylsdp,
    y,
    offer_opts
  }).

% ----------------------
-record(refer, {
    stage,        % stage of async operation (wait_z | wait_y)
    fork,         % #side_fork of z
    xcallid,     % callid of refering side x
    xreq,         % refer request
    x,             % #side who refering
    xroute,     % route uri
    xnotify,    % true|false if should notify x
    xtype,        % local | remote of referring side x
    ycallid,    % callid of referred side y
    ylocaltag,  % local tag of referred side y
    ytype,        % local | remote of referred side y
    zcallid,    % callid of referring side z
    startgs,             % grig second when started
    applied_redirects,     % [{Scheme,User,Domain}=AOR] where redirects already done
    timer_ref             % reference of total timer
  }).

% ----------------------
-record(replace, {
    zcallid,    % Z callid
    zreq,         % Z initial invite request
      zside,        % Z side
    ztype,        % Z type
    xside,        % X side
    yside,        % Y side
    ylocaltag,    % Y local tag
    timer_ref     % reference of total timer
  }).

% ----------------------
-record(state, {
    auto_check
}).

%% ====================================================================
%% Define modules
%% ====================================================================

-define(SIPAPP, r_sip_esg).

-define(ESG, r_sip_esg_cb).
-define(ESG_SUPV, r_sip_esg_supv).
-define(ESG_EVENT, r_sip_esg_eventing).

-define(EVENT, r_sip_esg_b2b_event_log).
-define(ESG_ROUTER, r_sip_esg_b2b_invite_router).
-define(ESG_MEDIA, r_sip_esg_b2b_media).
-define(ESG_UTILS, r_sip_esg_b2b_utils).

-define(ESG_DLG, r_sip_esg_fsm).

-define(FORKING, r_sip_esg_fsm_forking).
-define(DIALOG, r_sip_esg_fsm_dialog).
-define(SESSIONCHANGING, r_sip_esg_fsm_sessionchanging).
-define(REFERING, r_sip_esg_fsm_refering).
-define(REPLACING, r_sip_esg_fsm_replacing).
-define(MIGRATING, r_sip_esg_fsm_migrating).
-define(STOPPING, r_sip_esg_fsm_stopping).

-define(FORKING_REPLACING, r_sip_esg_fsm_forking_replacing).
-define(FORKING_UTILS, r_sip_esg_fsm_forking_utils).

-define(EXT_TRANS,r_sip_esg_translit).

%% ====================================================================
%% Define other
%% ====================================================================

-define(FORKING_STATE, 'forking').
-define(DIALOG_STATE, 'dialog').
-define(STOPPING_STATE, 'stopping').
-define(SESSIONCHANGING_STATE, 'sessionchanging').
-define(REFERING_STATE, 'refering').
-define(REPLACING_STATE, 'replacing').
-define(MIGRATING_STATE, 'migrating').

-define(STOPPING_TIMEOUT, 2000).
-define(STORE_TIMEOUT, 300000).
-define(STORE_REFRESH, 200000).
-define(REINVITE_TIMEOUT, 16000).
-define(REFER_TRY_TIMEOUT, 30000).
-define(REFER_WAIT_TIMEOUT, 60000).
-define(DIALOG_TIMEOUT, 7200000).
-define(MIGRATION_TIMEOUT, 16000).

-define(DirInside, 'inside').
-define(DirOutside, 'outside').
-define(SideRemote, 'remote').
-define(SideLocal, 'local').

%% ====================================================================
%% Defines
%% ====================================================================

-define(DEFAULT_FORK_RULE_TIMEOUT, 30000).
-define(DEFAULT_FORK_100_TIMEOUT, 30000).
-define(DEFAULT_FORK_180_TIMEOUT, 30000).
-define(DEFAULT_FORK_182_TIMEOUT, 300000).
-define(DEFAULT_FORK_183_TIMEOUT, 300000).
-define(REFER_TIMEOUT, 60000).
-define(REPLACING_TIMEOUT, 15000).
