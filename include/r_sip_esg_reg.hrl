%% ====================================================================
%% Modules
%% ====================================================================

-define(PING, r_sip_esg_reg_ping).
-define(REGISTER, r_sip_esg_reg_register).
-define(UAC, r_sip_esg_reg_uac).
-define(RUTILS, r_sip_esg_reg_utils).
-define(GARBAGE, r_sip_esg_reg_garbage).
-define(REGSTATE, r_sip_esg_reg_rpstate).

%% ====================================================================
%% Defines
%% ====================================================================

-define(TimeoutRetryReg, 148000). % 28000
-define(TimeoutCheckSettings, 10000).
-define(CheckSettings, {check_settings}).
-define(InitRegs, {init_regs}).
-define(MaxTransactionTimeSec, 32).

-type account_item() :: {reg,Id::binary(),ClusterDomain::binary(),Map::map()}.
-type reg_id() :: {Username::binary(),Domain::binary()}.
-type reg_key() :: {RegId::reg_id(),TS::integer()}.
-type subscriber() :: {GenSrv::pid(),Ref::reference()}.
-type reg_state() :: registered | retry | wait_result | disabled.
-type ping_state() :: ok | error | wait_result | disabled.

%% ====================================================================
%% Other
%% ====================================================================

