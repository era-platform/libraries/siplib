%% ------------------------------------------------------------
%% Define headers
%% ------------------------------------------------------------

% ivr/conf put in request
-define(CallerTypeHeader, <<"X-Era-CallerType">>).
-define(CallerTypeHeaderLow, <<"x-era-callertype">>).

% every internal ua put in request (to check auth and other)
-define(OwnerHeader, <<"X-Era-Owner">>).
-define(OwnerHeaderLow, <<"x-era-owner">>).

% b2bua put in request to esg, esg put in request to b2bua
-define(ExtAccountHeader, <<"X-Era-ExtAccount">>).
-define(ExtAccountHeaderLow, <<"x-era-extaccount">>).

% put in request to ivr
-define(IvrHeader, <<"X-Era-IVR">>).
-define(IvrHeaderLow, <<"x-era-ivr">>).

% put in request to ivr's hunt
-define(HuntHeader, <<"X-Era-HUNT">>).
-define(HuntHeaderLow, <<"x-era-hunt">>).

% put in request to prompt
-define(PromptHeader, <<"X-Era-Prompt">>).
-define(PromptHeaderLow, <<"x-era-prompt">>).

% put in request to ivr's hunt
-define(FCHeader, <<"X-Era-FeatureCode">>).
-define(FCHeaderLow, <<"x-era-featurecode">>).

% put in request to b2bua
-define(B2BHeader, <<"X-Era-B2BUA">>).
-define(B2BHeaderLow, <<"x-era-b2bua">>).

% put by gate in request to b2bua to setup remote address
-define(RcvFromHeader, <<"X-Era-RcvFrom">>).
-define(RcvFromHeaderLow, <<"x-era-rcvfrom">>).

% put by gate in request to b2bua to setup remote address
-define(RcvFromDomHeader, <<"X-Era-FromDomain">>).
-define(RcvFromDomHeaderLow, <<"x-era-fromdomain">>).

% put by misc gates in request to sg/[esg] to show he is not internal
-define(MiscHeader, <<"X-Era-Misc">>).
-define(MiscHeaderLow, <<"x-era-misc">>).

% RP-1575 put by esg in request to b2bua to link calls in one chain by one external call
-define(EsgDlgHeader, <<"X-Era-Esg-Dlg">>).
-define(EsgDlgHeaderLow, <<"x-era-esg-dlg">>).

% RP-1707 link referring A->C dialog to referred A->B dialog. SIP NOTIFY event=refer header, containing callid of referring call.
-define(ReferringCallId, <<"X-Era-Referring-Call-Id">>).
-define(ReferringCallIdLow, <<"x-era-referring-call-id">>).

% RP-2002
-define(ReplacesCallId, <<"X-Era-Replaces-Call-Id">>).
-define(ReplacesCallIdLow, <<"x-era-replaces-call-id">>).

-define(DestCallId, <<"X-Era-Dest-Call-Id">>).
-define(DestCallIdLow, <<"x-era-dest-call-id">>).

% for filtering events in business-logic
-define(GhostHeader, <<"X-Era-Ghost">>). % Era
-define(GhostHeaderLow, <<"x-era-ghost">>). % Era

% bindings
-define(BindingsHeader, <<"X-Era-Bindings">>).
-define(BindingsHeaderLow, <<"x-era-bindings">>).

% headers
-define(CallInfo, <<"Call-Info">>).
-define(CallInfoLow, <<"call-info">>).

-define(RemotePartyId, <<"Remote-Party-ID">>).
-define(RemotePartyIdLow, <<"remote-party-id">>).

-define(RemotePartyIdOpts1xx, [{<<"party">>,<<"called">>},{<<"screen">>,<<"no">>},{<<"privacy">>,<<"off">>}]).
-define(RemotePartyIdOpts2xx, [{<<"party">>,<<"called">>},{<<"screen">>,<<"yes">>},{<<"privacy">>,<<"off">>}]).

-define(PAssertedIdentity, <<"P-Asserted-Identity">>).
-define(PAssertedIdentityLow, <<"p-asserted-identity">>).

-define(PPreferredIdentity, <<"P-Preferred-Identity">>).
-define(PPreferredIdentityLow, <<"p-preferred-identity">>).

-define(ReferredBy, <<"Referred-By">>).
-define(ReferredByLow, <<"referred-by">>).

-define(Replaces, <<"Replaces">>).
-define(ReplacesLow, <<"replaces">>).

-define(Reason, <<"Reason">>).
-define(Warning, <<"Warning">>).
