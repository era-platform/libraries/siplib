

% ----------------------
-record(state_ivr, {
    version=1,
    cid :: binary(), % <<"88c196f2-0641-9b88-a74f-180000000006">>
    ivrid :: binary(), % <<"rDlg-006-S9gFHt">>
    ivridhash :: integer(), % #370
    invite_ts, % id/timestamp
    domain,
    map, % #{app, init_callid, opts, store_data, ...}
    use_media=true, % if media should be used or not
    media, % #media{}
    lstate, % map #{} for every sub state (migration, etc)| undefined
    acked=false,
    timer_ref,
    ref, % unique internal reference
    usedtags = [], % used local/remote tags not to generate already existing
    %
    a, % a side
    acallid, % call-id of opposite side
    scriptmachine, % scriptmachine properties (id, pid, ...)
    op_state, % map of operation state
    meta, % opts to use (remoteparty, contact - when refering to substitute)
    %
    reinvite,
    %
    starttime,
    finaltime,
    %
    forks = [], % list of [{CallId,#{}}]
    %
    stopreason,
    %
    ext_handler_fun, % RP-1470 filter function to external adapter
    ext_data % RP-1470 external state
  }).

% ----------------------
-record(side, {
    rhandle,
    dhandle,
    callid,
    dir :: in | out,
    state :: incoming | outgoing | ringing | early | dialog | bye | cancelled | moved | declined,
    req, % #sipmsg{}, % when dir=in -> incoming request
    responses = [], % when dir=in -> [..., SipReply2, SipReply1]
    %
    remoteuri,
    remotetag,
    remotecontacts,
    remotesdp,
    %
    localuri,
    localtag,
    localcontact,
    %
    refer_callid,
    %
    starttime,
    answertime,
    finaltime
  }).

% ----------------------
-record(side_fork, {
    callid,
    rhandle,
    dhandle,
    %
    remoteuri,
    remotetag,
    remotecontacts,
    remotesdp,
    requesturi,
    %
    localuri,
    localtag,
    localcontact,
    %
    cmnopts,
    inviteopts,
    %
    rule_timeout,
    rule_timer_ref,
    last_response_code,
    resp_timer_ref,
    %
    starttime,
    finaltime,
    res
  }).

% ----------------------
-record(reinvite, {
    timestart,
    timer_ref,
    rhandle,
    lsdp,
    offer_opts,
    funresult,
    pending_ref
  }).


% ----------------------
-record(state, {
    auto_check
}).


%% ====================================================================
%% Define modules
%% ====================================================================

-define(SIPAPP, r_sip_ivr).

-define(APP_MIXER, era_mixer).
%%-define(APP_SCRIPT, era_script).

-define(IVR, r_sip_ivr_cb).
-define(IVR_SUPV, r_sip_ivr_supv).
-define(IVR_UTILS, r_sip_ivr_utils).

-define(IVR_ROUTER, r_sip_ivr_invite_router).
-define(IVR_MODE_ROUTER, r_sip_ivr_mode_router).
-define(IVR_MEDIA, r_sip_ivr_media).
-define(IVR_METADATA, r_sip_ivr_script_metadata).

-define(EVENT, r_sip_ivr_eventing_log).
-define(EVENT_CDR, r_sip_ivr_eventing).

-define(IVR_SRV, r_sip_ivr_fsm).

-define(ACTIVE, r_sip_ivr_fsm_active).
-define(ACTIVE_MIGRATING, r_sip_ivr_fsm_active_migrating).
-define(ACTIVE_SM, r_sip_ivr_fsm_active_scriptmachine).
-define(ACTIVE_UTILS, r_sip_ivr_fsm_active_utils).
-define(ACTIVE_REFER, r_sip_ivr_fsm_active_refer).
-define(ACTIVE_REINVITE, r_sip_ivr_fsm_active_reinvite).

-define(DIALING, r_sip_ivr_fsm_dialing).
-define(DIALING_CALL,r_sip_ivr_fsm_dialing_call).
-define(DIALING_UTILS,r_sip_ivr_fsm_dialing_utils).
-define(DIALING_MIGRATING, r_sip_ivr_fsm_dialing_migrating).

-define(IVR_SCRIPT_UTILS, r_sip_ivr_script_utils).

-define(ScriptMachine, r_script_srv). % left for r_sip_ivr_script_metadata:apply_script_state()
-define(ScriptExpressions, r_script_expressions). % left for ~

-define(QUEUE, r_sip_ivr_queue).
-define(QUEUE_SCRIPT, r_sip_ivr_queue_script).

-define(MRCP_ADPT, r_sip_mrcp_adapter4ivr).

%% ====================================================================
%% Define other
%% ====================================================================

-define(ACTIVE_STATE, active).
-define(DIALING_STATE, dialing).
-define(STOPPING_STATE, stopping).

-define(STORE_TIMEOUT, 300000).
-define(STORE_REFRESH, 200000).
-define(REFER_TIMEOUT, 60000).
-define(STOPPING_TIMEOUT, 2000).

-define(REINVITE_TIMEOUT, 16000).

-define(FORK_TIMEOUT_MSG, 'fork_timeout').

-define(IVR_TIMEOUT_MSG, {ivr_timeout}).
-define(IVR_TIMEOUT, 5 * 3600 * 1000).

