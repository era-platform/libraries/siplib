%% ====================================================================

-include("../include/r_sip_ivr.hrl").
-include("../include/r_sip.hrl").
-include("../include/r_sip_headers.hrl").

%% ====================================================================

-record(state_queue,
{
    state=link :: link | enqueue | waitqueue | call | play | refer,
    huntq_id = <<>> :: binary(),
    domain = <<>> :: binary(),
    from_user = <<>> :: binary(),
    from_dn = <<>> :: binary(),
    site = <<>> :: binary(),
    ivr_fsm_pid = undefined :: undefined | pid(),
    scm_pid = undefined :: undefined | pid(),
    resourses = [], % [{UserId :: binary(), {Number,DialerPid,IvrNode}}]
    reput_huntq_ref = undefined :: undefined | reference(),
    current_queue_id = <<>> :: binary(),
    remote_ivr_script_code = <<>> : binary(),
    maxattemptstrannumber = <<>>,
    timeouttrannumber = <<>>,
    resdialtimeout = 0,
    hunt_block = false,
    hunt_block_code = <<>>,
    xinfo = #{},
    parent_functions = [] % RP-1468
}).

%% ====================================================================

-define(QUEUE_SRV, r_sip_ivr_queue_srv).
-define(QUEUE_SM, r_sip_ivr_queue_sm).
-define(QUEUE_DIAL, r_sip_ivr_queue_dial).
-define(QUEUE_HUNTQ, r_sip_ivr_queue_huntq).
-define(QUEUE_CB, r_sip_ivr_queue_callbacks).

-define(ENVMON, r_env_monitor_srv).
-define(SCR_COMP, r_script_components).

%% ====================================================================

-define(Q_1_R_REG, router_registered).
-define(Q_2_FSM_BYE, fsm_bye).
-define(Q_3_FSM_ERROR, fsm_error).
-define(Q_4_SCR_START, scriptmachine_start).
-define(Q_5_SCR_STOP, scriptmachine_stop).
-define(Q_6_SCR_REFER, scriptmachine_refer).
-define(Q_7_HUNT_REPORT, huntq_report).
-define(Q_8_HUNT_ERROR, huntq_error).
-define(Q_9_HUNT_RESERVED, apply_new_pair).
-define(Q_10_11_DIAL_RESPONSE, dial_response).
-define(Q_12_DIAL_SUCCESSFUL, dial_call_successful).
-define(Q_13_DIAL_CALL_ERROR, dial_call_error).
-define(Q_14_DIAL_ERROR, dial_error).
-define(Q_15_HUNT_PUT, huntq_put).
-define(Q_16_HUNT_EVENT, huntq_event).

-define(Q_GET_STATE, get_state).

%% ====================================================================

-define(Q_STYPE_HuntId, huntid).
-define(Q_STYPE_HuntObjId, huntobjid).
-define(Q_STYPE_Priority, queuepriority).
-define(Q_STYPE_Position, queueposition).
-define(Q_STYPE_Estimated, queueestimatedsec).
-define(Q_STYPE_Count, queuerescount).
-define(Q_STYPE_State, queuestate).

%% ====================================================================
