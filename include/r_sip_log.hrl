
-ifdef(TEST).
-define(BLlogdummy, basiclib_log_dummy).
-else.
-define(BLlog, basiclib_log).
-endif.

%% ======================================================

-define(LOGFILE, {sip,erl}).

-define(LOG(LogLevel,Fmt,Args), ?BLlog:write(LogLevel, ?LOGFILE, {Fmt,Args})).
-define(LOG(Fmt,Args), ?BLlog:write(?LOGFILE, {Fmt,Args})).
-define(LOG(Text), ?BLlog:write(?LOGFILE, Text)).

-define(OUT(Fmt,Args), ?BLlog:writeout(?LOGFILE, {Fmt,Args})).
-define(OUT(Text), ?BLlog:writeout(?LOGFILE, Text)).

-define(NOOUT(Fmt,Args), io_lib:format(Fmt++"~n",Args)).
-define(NOOUT(Text), io_lib:format(Text++"~n",[])).

-define(DLOG(Fmt,Args), ?LOG('$trace',Fmt,Args)).

-define(DOLOG(Level,LogFile,Data), case application:get_env(?APP,LogFile,true) of false -> ok; _ -> ?BLlog:write(Level,LogFile,Data) end).
-define(DOLOG(LogFile,Data), case application:get_env(?APP,LogFile,true) of false -> ok; _ -> ?BLlog:write(LogFile,Data) end).

-define(LOGFILESIP, {sip,sip}).
-define(LOGFILETRN, {sip,trn}).
-define(LOGFILECDR, {sip,cdr}).
-define(LOGFILEMEDIA, {sip,media}).
-define(LOGFILEMGCT, {sip,mgct}).

-define(LOGSIP(Level, Fmt,Args), ?DOLOG(Level,?LOGFILESIP,{Fmt,Args})).
-define(LOGSIP(Fmt,Args), ?DOLOG(?LOGFILESIP,{Fmt,Args})).
-define(LOGSIP(Text), ?DOLOG(?LOGFILESIP,Text)).

-define(LOGTRN(Level,Fmt,Args), ?DOLOG(Level,?LOGFILETRN,{Fmt,Args})).
-define(LOGTRN(Fmt,Args), ?DOLOG(?LOGFILETRN,{Fmt,Args})).
-define(LOGTRN(Text), ?DOLOG(?LOGFILETRN, Text)).

-define(LOGCDR(Fmt,Args), ?DOLOG(?LOGFILECDR,{Fmt,Args})).

-define(LOGMEDIA(Level,Fmt,Args), ?DOLOG(Level,?LOGFILEMEDIA,{Fmt,Args})).
-define(LOGMEDIA(Fmt,Args), ?DOLOG(?LOGFILEMEDIA,{Fmt,Args})).
-define(LOGMEDIA(Text), ?DOLOG(?LOGFILEMEDIA,Text)).

-define(LOGMGC(Level,Fmt,Args), ?DOLOG(Level,?LOGFILEMGCT,{Fmt,Args})).
-define(LOGMGC(Fmt,Args), ?DOLOG(?LOGFILEMGCT,{Fmt,Args})).
-define(LOGMGC(Text), ?DOLOG(?LOGFILEMGCT,Text)).