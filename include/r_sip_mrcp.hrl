
% ----------------------
-record(state_mrcp, {
    opts :: map(),
    state :: dialing | active | terminating,
    map :: map(), % fromdomain, to, callerid, callername, headers, routedomain, callid, lsdp
    %
    dlgid :: binary(),
    app :: binary(),
    domain :: binary(),
    ownerpid :: pid(),
    scriptmachine :: map(), % pid, id, monitorref
    parent_callid :: binary(),
    starttime :: integer(),
    %
    a, % side{}
    forks = [] :: list(), % {CallId::binary(), ForkCall::map(type,zcallid,fork,startgs)}
    acallid :: binary(),
    %
    media, % #media{}
    %
    timer_ref :: reference(),
    stopreason = undefined :: term(),
    mrcpreason = undefined :: term()
}).

%% ====================================================================
%% Define modules
%% ====================================================================

-define(MRCP_FSM_DIALING_STATE, dialing).
-define(MRCP_FSM_ACTIVE_STATE, active).
-define(MRCP_FSM_TERMINATING_STATE, terminating).

-define(MRCP_FSM, r_sip_mrcp_fsm).
-define(MRCP_FSM_DIALING, r_sip_mrcp_fsm_dialing).
-define(MRCP_FSM_ACTIVE, r_sip_mrcp_fsm_active).
-define(MRCP_FSM_TERMINATING, r_sip_mrcp_fsm_terminating).

-define(MRCP_FSM_UTILS, r_sip_mrcp_fsm_utils).

-define(MRCP_DIALING_CALL, r_sip_mrcp_fsm_dialing_call).
-define(MRCP_DIALING_UTILS, r_sip_mrcp_fsm_dialing_utils).

-define(MRCP_MEDIA, r_sip_mrcp_media).
-define(MRCP_CONN, r_sip_mrcp_conn).
-define(MRCP_PROTO, r_sip_mrcp_proto).
