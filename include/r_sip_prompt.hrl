
-record(prompt, {
    dlgid :: binary(),
    mode :: binary(),
    side :: binary(),
    site :: binary() | undefined,
    node :: node() | undefined,
    pid :: pid() | undefined,
    monref :: reference() | undefined,
    termid :: binary()
}).

% ----------------------
-record(side, {
    rhandle,
    dhandle,
    callid,
    dir, % in | out
    req, % #sipmsg
    %
    remoteuri,
    remotetag,
    remotecontacts,
    remotesdp,
    remoteparty,
    %
    localuri,
    localtag,
    localcontact,
    %
    refer_callid,
    %
    starttime,
    answertime,
    finaltime
}).

% ----------------------
-record(side_fork, {
    callid,
    rhandle,
    dhandle,
    %
    remoteuri,
    remotetag,
    remotecontacts,
    remotesdp,
    requesturi,
    %
    localuri,
    localtag,
    localcontact,
    %
    cmnopts,
    inviteopts,
    %
    rule_timeout,
    rule_timer_ref,
    last_response_code,
    resp_timer_ref,
    %
    starttime,
    finaltime,
    res
}).

% ----------------------
-record(state_prompt, {
    version=1,
    pid :: pid(),
    dlgid :: binary(),
    dlgidhash :: integer(),
    req :: nksip:sipmsg(),
    aor :: tuple(),
    %
    prompt :: #prompt{},
    %
    a :: #side{},
    acallid :: binary(),
    %
    invite_ts :: integer(), % id/timestamp
    map, % #{app, dlgnum, init_callid, opts, store_data, last_out_callid_idx, timer_migration_ref}
    lstate, % map #{} for every sub state (migration, etc)| undefined
    timer_ref,
    ref, % unique internal reference
    usedtags = [], % used local/remote tags not to generate already existing
    video = false,
    %
    starttime,
    finaltime,
    %
    forks = [], % list of [{CallId,#{}}]
    %
    stopreason
  }).

% ----------------------
-record(state, {
    auto_check
}).

%% ====================================================================
%% Define modules
%% ====================================================================

-define(SIPAPP, r_sip_prompt).

-define(PROMPT, r_sip_prompt_cb).
-define(PROMPT_SUPV, r_sip_prompt_supv).
-define(PROMPT_UTILS, r_sip_prompt_utils).

-define(PROMPT_ROUTER, r_sip_prompt_invite_router).
-define(PROMPT_MEDIA, r_sip_prompt_media).

-define(PROMPT_SRV, r_sip_prompt_fsm).

-define(ACTIVE, r_sip_prompt_fsm_active).

-define(ACTIVE_CALL, r_sip_prompt_fsm_active_call).
-define(ACTIVE_MIGRATING, r_sip_prompt_fsm_active_migrating).
-define(ACTIVE_REFER, r_sip_prompt_fsm_active_refer).
-define(ACTIVE_UTILS, r_sip_prompt_fsm_active_utils).

-define(PROMPT_STORE, r_sip_prompt_store_srv).

%% ====================================================================
%% Define other
%% ====================================================================

-define(ACTIVE_STATE, active).
-define(STOPPING_STATE, stopping).

-define(STORE_TIMEOUT, 300000).
-define(STORE_REFRESH, 200000).
-define(REFER_TIMEOUT, 60000).
-define(STOPPING_TIMEOUT, 2000).

-define(FORK_TIMEOUT_MSG, 'fork_timeout').

-define(PROMPT_TIMEOUT_MSG, {prompt_timeout}).
-define(PROMPT_TIMEOUT, 5 * 3600 * 1000).
