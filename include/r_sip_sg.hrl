
%% ====================================================================
%% Define modules
%% ====================================================================

-define(SIPAPP, r_sip_sg).

-define(SG_SUPV, r_sip_sg_supv).

-define(SG_REREGMON_SUPV, r_sip_sg_rereg_monitor_supv).
-define(SG_REREGMON_OPTS, r_sip_sg_rereg_opts).

-define(SG_REFRESHER, r_sip_sg_refresher_srv).

-define(SG_TRANSLIT,r_sip_sg_translit).
-define(SG_Substitute,era_sip_sg_substitute).

-define(BG_CB, r_sip_sg_bg_cb).

-define(M_SDP, r_sip_sdp_media_routines).