
%% ====================================================================
%% Types
%% ====================================================================

%% state::[empty,offered,answered,error,dialog]
-record(state,{state=empty,
                no_sdp_invite=false,
                media,
                %prevsdp,
                contacts,
                totag,
                fromtag,
                t1alias,
                t2alias}).

-record(gatemedia, {mgc,
                    msid,
                    mgid,
                    ctx,
                    start_opts,
                    term_type,
                    t1alias,
                    t2alias,
                    t1,
                    t2,
                    t1_opts,
                    t2_opts,
                    t1_prevsdp,
                    t2_prevsdp}).

%% ====================================================================
%% Define modules
%% ====================================================================

-define(BG_MEDIA, r_sip_sg_bg_media).