
-include("../include/r_sip.hrl").
-include("../include/r_sip_sg.hrl").

%----------------------------------------------------------------------
% Modules
-define(SYNC,r_env_sync_strategy_srv).
-define(REREGSRV,r_sip_sg_rereg_monitor_srv).
-define(REREGCHECKDOWN,r_sip_sg_rereg_strategy_cd).
-define(REREGPERF,r_sip_sg_rereg_monitor_perf).
-define(REREGWORK,r_sip_sg_rereg_monitor_work).
-define(REREGSENDER,r_sip_sg_rereg_strategy_sn).

%----------------------------------------------------------------------
% Records
-record(rereg_cache, {
    all_gates = [] :: [{binary(), binary(), non_neg_integer()}],
    active_gates = [] :: [{binary(), binary(), non_neg_integer()}],
    zombie_gates = [] :: [{binary(), binary(), non_neg_integer(), non_neg_integer()}]
}).

-record(rereg_state, {
    cache = undefined :: undefined | #rereg_cache{},
    timer_ref = undefined :: undefined | reference(),
    monitoring = [] :: [{reference(),pid()}],
    %
    counter = 0,
    domains_cache = [] :: [{Domain::binary(),TS::non_neg_integer()}],
    skip = 0
}).

%----------------------------------------------------------------------
% Variables
-define(TaskRegInfo, <<"REGINFO">>).
-define(PerfRef, <<"PERFREF">>).