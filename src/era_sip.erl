%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc
%%% @todo
%%% -------------------------------------------------------------------
%%% Required Opts:
%%%    udp (-), 5060
%%% Optional Opts:
%%%    tcp (-), 5060
%%%    tls (-), 5061
%%%    ws (-), 5062
%%%    wss (-), 5063
%%%    log_sip (+), true/false
%%%    log_trn (+), true/false
%%%    log_media (+), true/false
%%%    log_mgct (+), true/false
%%%    log_cdr (+), true/false
%%%    use_srtp (-), true/false
%%%    payloads_audio_offer (-),
%%%            [<<"PCMA/8000">>,<<"PCMU/8000">>,<<"CN/8000">>,<<"GSM/8000">>,<<"G722/8000">>,<<"G723/8000">>,<<"G729/8000">>,
%%%             <<"G726-16/8000">>,<<"G726-24/8000">>,<<"G726-32/8000">>,<<"G726-40/8000">>,
%%%             <<"speex/8000">>,<<"speex/16000">>,<<"speex/32000">>,<<"iLBC/8000">>,
%%%             <<"telephone-event/8000">>,<<"opus/48000/2">>]
%%%            NOTE! should be same case of letters.
%%%            NOTE! PCMA, PCMU, CN, telephone-event are always statically used.
%%%    payloads_video_offer (-),
%%%            [<<"H263/90000">>,<<"H263-1998/90000">>,<<"H264/90000">>,<<"VP8/80000">>,<<"VP9/90000">>,
%%%             <<"red/90000">>,<<"rtx/90000">>]
%%%    use_video_transcoding (-) (false/true)
%%%    tls_ciphers (-), []
%%%            [<<"*">>, <<"TLS_RSA_WITH_AES_256_GCM_SHA384">>, <<"AES256-GCM-SHA384">>, ...] rfc or openssl names for tls1.1, tls1.2, tls1.3, ...
%%%
%%% ==== b2bua ====
%%%    allow_ac_set (-), true/false
%%%    check_by_options_on_limit (-), true/false
%%%    send_reinvite_on_ack (-), true/false
%%%    mg_postfix (-), "def"
%%%    record_ivr (-), false/true
%%%    record_conf (-), false/true
%%%    record_prompt (-), false/true
%%%
%%% ==== sg ====
%%%    aliases (-), [{<<"Local">>,<<"192.168.0.12">>},{<<"Inet">>,<<"87.12.151.52">>}]
%%%    b2bmedia (-), true/false
%%%    max_udp_size (-) 6000 (bytes)
%%%    reregister (-), false/true
%%%    stateless (-), false/true
%%%    translit (-), false/true
%%%    sip_alg (-), [#{<<"gray">> => GrayAddr,<<"white">> => WhiteAddr}]
%%%    substitute_domains (-), [#{<<"addr">> => Addr,<<"domain">> => Domain}]
%%%
%%% ==== esg ====
%%%    -loadsrvid ([CurrentServerIdx()])
%%%    -domains ([<<"rootdomain">>])
%%%    stunserver ([<<"sip:stun.sipnet.ru:3478">>, <<"sip:demo.era.ru:5060">>"])
%%%    extaddrs ([<<"188.227.75.174">>, <<"10.0.0.10">>])
%%%    b2bmedia (-), true/false
%%%    max_udp_size (-) 6000 (bytes),
%%%    sip_alg (-), [{GrayAddr1,WhiteAddr1},{GrayAddr2,WhiteAddr2}]
%%%
%%% ==== redirect ====
%%%    sipcode (-), 305/302/301/300/3xx
%%%    multicontact (-), false/true % NOTE! multicontact do not use gate check and choice of proto.
%%%    expires (-), 0/integer
%%%    gates (-),
%%%
%%% ==== ivr ====
%%%    b2bmedia (-), true/false #350
%%%    mg_postfix (-), "def"
%%%    usemedia (-), true/false #350
%%%
%%% ==== conf ====
%%%    mg_postfix (-), "def"
%%%
%%% ==== prompt ====
%%%
%%% -------------------------------------------------------------------

-module(era_sip).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(application).

-export([prepare_start/1,
         start/1,
         update_opts/1,
         update_cfg/0,
         stop/0,
         get_opts/1,
         get_os_pids/0]).

-export([start/2, stop/1]).
-export([add_deps_paths/0]). % RP-646
-export([get_module_by_role/1]).

%% ------------------
%% Facade functions
%% ------------------

%% r_sip_utils
-export([u_get_host_interfaces_ipv4/0,
         u_get_host_addr/1]).

%% RP-1344
%% r_sip_dlg_store_srv
-export([ua_pull_dlg/1]).

%% r_sip_b2bua_notify
-export([b2b_notify_add_task/1]).

%% RP-1297, RP-1331
%% r_sip_b2bua_fsm
-export([b2b_dialog_stop/2,
         b2b_dialog_getinfo/2,
         b2b_dialog_break_bye_transmit/3,
         b2b_dialog_send_notify_3265/2,
         b2b_dialog_send_dtmf/2,
         b2b_setup_record/3,
         b2b_setup_asr/3,
         b2b_external_term_attach/2,
         b2b_external_term_detach/2,
         b2b_external_term_setup_topology/2]).

%% r_sip_b2bua_fsm_utils_bindings
-export([b2b_dialog_bindings_add/2,
         b2b_dialog_bindings_remove/2,
         b2b_dialog_bindings_setx/2,
         b2b_dialog_bindings_cleanup/1,
         b2b_dialog_bindings_contains/2,
         b2b_dialog_bindings_get/1]).

%% r_sip_ivr_cb
-export([ivr_cb_start_by_mode/1]).

%% r_sip_ivr_fsm
-export([ivr_fsm_stop/2,
         ivr_dialog_getinfo/2, % RP-1344
         ivr_script_send_ext_event/2]).

%% r_sip_conf_fsm
-export([conf_start_empty/3,
         conf_stop/2,
         conf_sync_get_topology/2,
         conf_sync_set_topology/2,
         conf_sync_start_play/4,
         conf_sync_call_start/4,
         conf_sync_call_stop/2,
         conf_sync_call_set_mode/3,
         conf_get_info/2,
         conf_get_participants/2,
         conf_find_participant/2,
         conf_get_participant_info/3]).

%% r_sip_stat
-export([stat_test/0,
         stat_b2bua/2,
         stat_esg/2,
         stat_conf/2,
         stat_ivr/2,
         stat_sg/2,
         stat_prompt/2,
         stat_sip_trace/2]).

%% r_sip_esg
-export([esg_get_provider_addr/1]).

%% r_sip_sg
-export([clear_cert_cache/0]).

-export([dbg_start/0]).

%% ================================================================================
%% Types
%% ================================================================================

-include("../include/r_sip.hrl").

%% ===================================================================
%% Private
%% ===================================================================

%% -------------------------------------
%% Preparing application to start
%%
-spec prepare_start(Opts :: list()) ->
          ok | {error, Reason :: term()} | {retry_after, Ms :: integer(), Reason :: term()}.

prepare_start(Opts) when is_list(Opts) ->
    check_opts(Opts).

dbg_start() ->
    Opts = [{role,b2bua},
        {<<"allow_ac_set">>,false},
        {<<"check_by_options_on_limit">>,false},
        {<<"log_cdr">>,true},
        {<<"log_media">>,false},
        {<<"log_mgct">>,true},
        {<<"log_sip">>,true},
        {<<"log_trn">>,true},
        {<<"payloads_audio_offer">>,
            [<<"PCMA/8000">>,<<"PCMU/8000">>,<<"G729/8000">>]},
        {<<"payloads_video_offer">>,[]},
        {<<"record_conf">>,false},
        {<<"record_ivr">>,false},
        {<<"sipid">>,11240},
        {<<"send_reinvite_on_ack">>,false},
        {<<"tcp">>,5099},
        {<<"udp">>,5099},
        {<<"rolename">>,<<"b2b1">>}],
    AppDeps = ["basiclib","lager","goldrush","ranch",
               "cowlib","cowboy","enotify","jsone","yamerl",
               "eper","recon","mimerl","rfc3339","parsexml",
               "gun", "iconverl","jsx",
               "nklib","nksip","nkserver","nkpacket"],
    lists:foreach(fun(App) -> true=code:add_patha("../../"++App++"/ebin") end, AppDeps),
    io:format("start basiclib: ~120p~n", [application:ensure_all_started(basiclib)]),
    start(Opts).

%% -------------------------------------
%% Starts application (no service)
%%
start(Opts) when is_list(Opts) ->
    case get_role_module(Opts) of
        {error,_}=Err -> Err;
        {ok, M} ->
            add_deps_paths(Opts),
            case nklib_util:ensure_all_started(?APP, permanent) of
                {ok, _Started} ->
                    case start_role_module(M, Opts) of
                        ok ->
                            %loglevel(notice),
                            %trace(false),
                            ok;
                        Error -> Error
                    end;
                Error -> Error
            end end.

%% -------------------------------------
%% Updates opts of application
%%
update_opts(Opts) ->
    ?LOG('$info', "Application ~p update_opts", [?APP]),
    case get_role_module(Opts) of
        {error,_}=Err -> Err;
        {ok, M} ->
            case ?ENV:get_env(M) of
                {ok,_} ->
                    set_opts(M, Opts),
                    case ?EU:function_exported(M, update_opts, 1) of
                        true ->
                            case M:update_opts(Opts) of
                                restart ->
                                    ?OUT("Application ~p stop/start on update opts (env_store)", [?APP]),
                                    %?MODULE:stop(),
                                    %?MODULE:start(Opts);
                                    ?EU:stop_node(1000);
                                T -> T
                            end;
                        false -> ok
                    end;
                undefined ->
                    ?OUT("Application ~p stop/start on update_opts (no env_store)", [?APP]),
                    %?MODULE:stop(),
                    %?MODULE:start(Opts)
                    ?EU:stop_node(1000)
            end end.

%% -------------------------------------
%% Notify about configuration changes
%%
update_cfg() ->
    ?LOG('$info', "Application ~p update_cfg", [?APP]),
    case ?ENV:get_env(siprole) of
        {ok,[Module]} ->
            case ?EU:function_exported(Module,update_cfg,0) of
                true -> Module:update_cfg();
                false -> ok
            end;
        undefined -> ok
    end.

%% -------------------------------------
%% Work pids
%%
get_os_pids() -> [os:getpid()].

%% -------------------------------------
%% Stops application (no service)
%%
stop() ->
    ?LOG('$info', "Application ~p stop", [?APP]),
    case ?ENV:get_env(siprole) of
        undefined -> ok;
        {ok,M} when is_list(M) ->
            ?ENV:unset_env(siprole),
            stop_role_modules(M)
    end,
    application:stop(?APP).

%% -------------------------------------
%% Return opts of active module
%%
get_opts(Module) ->
    ?ENV:get_env(Module).

%% ====================================================================
%% Callback
%% ====================================================================

start(_Mode, State) ->
    start_apps(),
    spawn(fun() -> check_copy_common_files() end),
    erlang:append_element(?SUPV:start_link(), State).

stop(_State) ->
    ok.

%% ====================================================================
%% Facade functions
%% ====================================================================

%% ------------------------------------------
%% r_sip_utils
%% ------------------------------------------

%% --------------------
-spec u_get_host_interfaces_ipv4() -> [binary()].
%% --------------------
u_get_host_interfaces_ipv4() ->
    ?U:get_host_interfaces_ipv4().

%% --------------------
-type ip4_address() :: {0..255, 0..255, 0..255, 0..255}.
%% --------------------
-spec u_get_host_addr(RemoteIp :: binary() | string() | ip4_address()) -> ip4_address().
%% --------------------
u_get_host_addr(RemoteIp) ->
    ?U:get_host_addr(RemoteIp).

%% ------------------------------------------
%% r_sip_dlg_store_srv RP-1344
%% ------------------------------------------

%% --------------------
-spec ua_pull_dlg(CallId::binary()) -> false | {DialogId::binary(), Pid::pid()}.
%% --------------------
ua_pull_dlg(CallId) ->
    ?DLG_STORE:pull_dlg(CallId).

%% ------------------------------------------
%% r_sip_b2bua_notify
%% ------------------------------------------

%% --------------------
-spec b2b_notify_add_task(Opts :: map()) -> pid().
%% --------------------
b2b_notify_add_task(Opts) ->
    ?SIP_B2B_NOTIFY:add_task(Opts).

%% ------------------------------------------
%% r_sip_b2bua_fsm RP-1297, RP-1331
%% ------------------------------------------

%% --------------------
-spec b2b_dialog_stop(Dlg::binary(),Reason::binary() | map()) -> ok | {error,not_found}.
%% --------------------
b2b_dialog_stop(Dlg,Reason) ->
    ?SIP_B2B_FSM:stop(Dlg,Reason).

%% --------------------
-spec b2b_dialog_getinfo(Dlg::binary(),Mode::term()) -> false | {ok,Info::map()}.
%% --------------------
b2b_dialog_getinfo(Dlg,Mode) ->
    ?SIP_B2B_FSM:get_current_info(Dlg,Mode).

%% --------------------
-spec b2b_dialog_break_bye_transmit(Dlg::binary(),Pause::boolean(),Opts::list()) ->
          false | ok | {ok,Ref::reference()} | {false,another} | {false,undefined}.
%% --------------------
b2b_dialog_break_bye_transmit(Dlg,Pause,Opts) ->
    ?SIP_B2B_FSM:break_bye_transmit(Dlg,Pause,Opts).

%% --------------------
-spec b2b_dialog_send_notify_3265(Dlg::binary(),Opts::list()) ->
          ok | false | undefined | {error,Reason::term()}.
%% --------------------
b2b_dialog_send_notify_3265(Dlg,Opts) ->
    ?SIP_B2B_FSM:send_notify_3265(Dlg,Opts).

%% --------------------
-spec b2b_dialog_send_dtmf(Dlg::binary(),Opts::map()) ->
    ok | false | undefined | {error,Reason::term()}.
%% --------------------
b2b_dialog_send_dtmf(Dlg,Opts) ->
    ?SIP_B2B_FSM:send_dtmf(Dlg,Opts).

%% ------------------
-spec b2b_setup_record(DlgId::binary(), Cmd::add | remove, Opts::map()) -> ok | false | {error,Reason::binary()}.
%% ------------------
b2b_setup_record(Dlg,Cmd,Opts) ->
    ?SIP_B2B_FSM:setup_record(Dlg,Cmd,Opts).

%% ------------------
-spec b2b_setup_asr(DlgId::binary(), Cmd::add | remove, Opts::map()) -> ok | false | {error,Reason::binary()}.
%% ------------------
b2b_setup_asr(Dlg,Cmd,Opts) ->
    ?SIP_B2B_FSM:setup_asr(Dlg,Cmd,Opts).

%% ------------------
-spec b2b_external_term_attach(CallOrDlgId::binary(), Opts::map())
      -> ok | {ok,LSdp::binary()} | false | {error,Reason::binary()}.
%% Opts:: id::binary(), termtype::play|rec|rtp, ...
%%    termtype==play -> mode,file,files,dir,random,loop,startat,stopat,volume
%%    termtype==rec -> file,codec_name,type,buffer_duration
%%    termtype==rtp -> dir, topology (mesh {default} | monitor | {prompt,TERM})
%%        dir==in -> sdp
%%        dir==out -> -
%% ------------------
b2b_external_term_attach(Dlg,Opts) ->
    ?SIP_B2B_FSM:external_term_attach(Dlg,Opts).

%% ------------------
-spec b2b_external_term_detach(CallOrDlgId::binary(), Opts::map()) -> ok | false | {error,Reason::binary()}.
%% Opts:: id::binary()
%% ------------------
b2b_external_term_detach(Dlg,Opts) ->
    ?SIP_B2B_FSM:external_term_detach(Dlg,Opts).

%% ------------------
-spec b2b_external_term_setup_topology(CallOrDlgId::binary(), Opts::map()) -> ok | false | {error,Reason::binary()}.
%% Opts:: id::binary(),
%%          topology::[{a|b|t, a|b|t, bothway|oneway|isolate}]
%%            or
%%          oneway_sides::[a|b]
%% ------------------
b2b_external_term_setup_topology(Dlg,Opts) ->
    ?SIP_B2B_FSM:external_term_setup_topology(Dlg,Opts).

%% ------------------------------------------
%% r_sip_b2bua_dialog_bindings interface
%% ------------------------------------------

%% --------------------
-spec b2b_dialog_bindings_add(CallOrDlgId :: binary(), BindingLabels :: binary() | [binary()])
      -> ok | {error, Reason::term()}.
%% --------------------
b2b_dialog_bindings_add(CallOrDlgId, BindingLabels) ->
    ?B2BUA_BINDS:dialog_bindings({add, CallOrDlgId, BindingLabels}).

%% --------------------
-spec b2b_dialog_bindings_remove(CallOrDlgId :: binary(), BindingLabel :: binary() | [binary()])
      -> ok | {error, Reason::term()}.
%% --------------------
b2b_dialog_bindings_remove(CallOrDlgId, BindingLabels) ->
    ?B2BUA_BINDS:dialog_bindings({remove, CallOrDlgId, BindingLabels}).

%% ------------------
-spec b2b_dialog_bindings_setx(CallOrDlgId :: binary(), BindingLabel :: binary()) ->
          ok | {error, Reason::term()}.
%% ------------------
b2b_dialog_bindings_setx(CallOrDlgId, BindingLabel) ->
    ?B2BUA_BINDS:dialog_bindings({setx, CallOrDlgId, BindingLabel}).

%% ------------------
-spec b2b_dialog_bindings_cleanup(CallOrDlgId :: binary()) ->
          ok | {error, Reason::term()}.
%% ------------------
b2b_dialog_bindings_cleanup(CallOrDlgId) ->
    ?B2BUA_BINDS:dialog_bindings({cleanup, CallOrDlgId, <<>>}).

%% ------------------
-spec b2b_dialog_bindings_contains(CallOrDlgId :: binary(), BindLabel :: binary()) ->
          boolean() | {error, Reason::term()}.
%% ------------------
b2b_dialog_bindings_contains(CallOrDlgId, BindingLabel) ->
    ?B2BUA_BINDS:dialog_bindings({contains, CallOrDlgId, BindingLabel}).
%% ------------------
-spec b2b_dialog_bindings_get(CallOrDlgId::binary()) ->
          list() | {error, Reason::term()}.
%% ------------------
b2b_dialog_bindings_get(CallOrDlgId) ->
    ?B2BUA_BINDS:dialog_bindings({get, CallOrDlgId, <<>>}).

%% ------------------------------------------
%% r_sip_ivr_cb
%% ------------------------------------------
-type child() :: undefined | pid().

%% ------------------
-spec ivr_cb_start_by_mode(Opts :: map())
      ->  {ok, Child::child()} | {ok, Child::child(), Info::term()} |
          {error, Reason :: invalid_params | binary() | already_present | {already_started, Child :: child()} | term()}.
%% ------------------
ivr_cb_start_by_mode(Opts) ->
    ?SIP_IVR_CB:start_by_mode(Opts).

%% ------------------------------------------
%% r_sip_ivr_fsm
%% ------------------------------------------

%% ------------------
-spec ivr_fsm_stop(IVR :: pid() | binary() | {IvrId :: binary(), Pid :: pid()}, Reason :: term()) -> ok.
%% ------------------
ivr_fsm_stop(IVR, Reason) ->
    ?SIP_IVR_FSM:stop(IVR, Reason).

%% ------------------
-spec ivr_dialog_getinfo(DialogId :: binary(), Mode :: term()) -> {ok, Info::map()} | false.
%% ------------------
ivr_dialog_getinfo(DialogId, Mode) ->
    ?SIP_IVR_FSM:get_info(DialogId, Mode).

%% ------------------
-spec ivr_script_send_ext_event(DialogId :: binary(), Msg :: term()) -> ok | {error,Reason::binary()}.
%% ------------------
ivr_script_send_ext_event(DialogId, Message) ->
    case ?SIP_IVR_FSM:pull_fsm_by_ivrid(DialogId) of
        {DialogId,Pid} when is_pid(Pid) ->
            Pid ! {scriptmachine,{ext_event,Message}},
            ok;
        _ ->
            {error,<<"IVR dialog not found">>}
    end.

%% ------------------------------------------
%% r_sip_conf_fsm
%% ------------------------------------------

%% ------------------
-spec conf_start_empty({Room::binary(), Domain::binary()}, Owner::pid(), Opts::list())
      -> {ok,Pid::pid(),ConfId::binary()} | {error,Reason::term()}.
%% ------------------
conf_start_empty(RoomDomain, Owner, Opts) ->
    ?SIP_CONF_FSM:start_empty(RoomDomain, Owner, Opts).

%% ------------------
-spec conf_stop(PidOrConfId :: pid() | binary(), Reason :: term()) -> ok | {error, Reason :: {not_found,binary()}}.
%% ------------------
conf_stop(PidOrConfId, Reason) ->
    ?SIP_CONF_FSM:stop(PidOrConfId, Reason).

%% ------------------
-spec conf_sync_get_topology(ConfId :: binary(), Opts::list())
      -> ok | {error, Reason :: {not_found,binary()}}.
%% ------------------
conf_sync_get_topology(ConfId, Opts) ->
    ?SIP_CONF_FSM:sync_get_topology(ConfId, Opts).

%% ------------------
-spec conf_sync_set_topology(ConfId :: binary(), Opts :: [{{A::binary(),B::binary()},Enabled::boolean()}])
      -> ok | {error, Reason :: {not_found,binary()}}.
%% ------------------
conf_sync_set_topology(ConfId, Opts) ->
    ?SIP_CONF_FSM:sync_set_topology(ConfId, Opts).

%% ------------------
%% TODO PlayerId :: binary(), File :: string() | binary() -- spec is not actually checked
%% ------------------
-spec conf_sync_start_play(ConfId :: binary(), PlayerId :: binary(), File :: string()|binary(), Opts :: list())
      -> ok | {error, Reason :: {not_found,binary()}}.
%% ------------------
conf_sync_start_play(ConfId, PlayerId, File, Opts) ->
    ?SIP_CONF_FSM:sync_start_play(ConfId, PlayerId, File, Opts).

%% ------------------
%% TODO PartcpId :: binary(), To :: term() -- spec is not actually checked
%% ------------------
-spec conf_sync_call_start(ConfId :: binary(), PartcpId :: binary(), To :: term(), Opts :: list())
      -> ok | {error, Reason :: {not_found,binary()}}.
%% ------------------
conf_sync_call_start(ConfId, PartcpId, To, Opts) ->
    ?SIP_CONF_FSM:sync_call_start(ConfId, PartcpId, To, Opts).

%% ------------------
%% TODO PartcpId :: binary() -- spec is not actually checked
%% ------------------
-spec conf_sync_call_stop(ConfId :: binary(), PartcpId :: binary()) -> ok | {error, Reason :: {not_found,binary()}}.
%% ------------------
conf_sync_call_stop(ConfId, PartcpId) ->
    ?SIP_CONF_FSM:sync_call_stop(ConfId, PartcpId).

%% ------------------
%% TODO PartcpId :: binary() -- spec is not actually checked
%% ------------------
-spec conf_sync_call_set_mode(ConfId :: binary(), PartcpId :: binary(), Opts :: list())
      -> ok | {error, Reason :: {not_found,binary()}}.
%% ------------------
conf_sync_call_set_mode(ConfId, PartcpId, Opts) ->
    ?SIP_CONF_FSM:sync_call_set_mode(ConfId, PartcpId, Opts).

%% ------------------
-spec conf_get_info(ConfId :: binary(), Opts::list()) -> {ok, Info::map()} | {error, Reason::term()}.
%% ------------------
conf_get_info(ConfId,Opts) ->
    ?SIP_CONF_FSM:get_info(ConfId,Opts).

%% ------------------
-spec conf_get_participants(ConfId :: binary(), Opts :: list())
      -> {ok, Participants::[map()]} | {error, Reason::term()}.
%% ------------------
conf_get_participants(ConfId,Opts) ->
    ?SIP_CONF_FSM:get_participants(ConfId, Opts).

%% ------------------
-spec conf_find_participant(ConfId :: binary(), Key :: binary())
      -> {ok, PartcpId::binary()} | {error, Reason::term()}.
%% ------------------
conf_find_participant(ConfId,Key) ->
    ?SIP_CONF_FSM:find_participant(ConfId, Key).

%% ------------------
-spec conf_get_participant_info(ConfId :: binary(), PartcpId :: binary(), Opts::list())
      -> {ok, Map::map()} | {error, Reason::term()}.
%% ------------------
conf_get_participant_info(ConfId,PartcpId,Opts) ->
    ?SIP_CONF_FSM:get_participant_info(ConfId, PartcpId, Opts).

%% ------------------------------------------
%% r_sip_stat
%% ------------------------------------------

%% ------------------
-spec stat_test() -> ok.
%% ------------------
stat_test() -> ?STAT:test().

%% ------------------
-spec stat_b2bua(Mode :: binary(), Map :: map()) -> {ok, Result :: map()}.
%% ------------------
stat_b2bua(Mode, Map) -> ?STAT:b2bua(Mode, Map).

%% ------------------
-spec stat_esg(Mode :: binary(), Map :: map()) -> {ok, Result :: map()}.
%% ------------------
stat_esg(Mode, Map) -> ?STAT:esg(Mode, Map).

%% ------------------
-spec stat_conf(Mode :: binary(), Map :: map()) -> {ok, Result :: map()}.
%% ------------------
stat_conf(Mode, Map) -> ?STAT:conf(Mode, Map).

%% ------------------
-spec stat_ivr(Mode :: binary(), Map :: map()) -> {ok, Result :: map()}.
%% ------------------
stat_ivr(Mode, Map) -> ?STAT:ivr(Mode, Map).

%% ------------------
-spec stat_sg(Mode :: binary(), Map :: map()) -> {ok, Result :: map()}.
%% ------------------
stat_sg(Mode, Map) -> ?STAT:sg(Mode, Map).

%% ------------------
-spec stat_prompt(Mode :: binary(), Map :: map()) -> {ok, Result :: map()}.
%% ------------------
stat_prompt(Mode, Map) -> ?STAT:prompt(Mode, Map).

%% ------------------
-spec stat_sip_trace(Mode :: binary(), Map :: map()) -> {ok, Result :: map()}.
%% ------------------
stat_sip_trace(Mode, Map) -> ?STAT:sip_trace(Mode, Map).

%% ------------------------------------------
%% r_sip_esg
%% ------------------------------------------

%% ------------------
-spec esg_get_provider_addr(Provider::map()) -> {ok,Addr::binary()} | {error,Reason::binary()}.
%% ------------------
esg_get_provider_addr(Provider) ->
    ?EXT_REGSRV:rpc_get_provider_addr(Provider).

%% ---
-spec clear_cert_cache() -> ok | {error, Reason::binary()}.
%% ---
clear_cert_cache() ->
    case application:get_env(?APP,role) of
        undefined -> {error, <<"Missed 'role' option value">>};
        {ok, Role} ->
            case get_module_by_role(Role) of
                invalid -> {error, <<"Unknown 'role' option value">>};
                M -> M:clear_cert_cache()
            end
    end.

%% ====================================================================
%% Internal
%% ====================================================================

% @private
start_apps() ->
    case os:type() of
        {unix,_} ->
            pg:start_link(), % for nkserver
            application:ensure_all_started(gen_netlink);
        _ -> ok
    end.

%% %% @doc Enables SIP trace messages to console.
%% -spec trace(Start::boolean()) -> ok.
%%
%% trace(true) ->
%%     nksip_trace:start();
%% trace(false) ->
%%     nksip_trace:stop().
%%
%%
%% %% @doc Changes console log level.
%% %% Availanle options are `debug' (maximum), `info' (medium) and `notice' (minimum).
%% -spec loglevel(debug|info|notice) -> ok.
%%
%% loglevel(Level) ->
%%     lager:set_loglevel(lager_console_backend, Level),
%%     {ok, _} = nksip:update(?SIPAPP, [{log_level, Level}]).


%% ====================================================================
%% API functions
%% ====================================================================


%% ====================================================================
%% Internal functions
%% ====================================================================

% check opts correctness
check_opts(Opts) ->
    case get_role_module(Opts) of
        {ok, _M} -> ok;
        {error, _R}=Error -> Error
    end.

set_opts(Module,Opts) ->
    ?ENV:set_env(Module,Opts),
    Logs = [{<<"log_sip">>,?LOGFILESIP,true},
            {<<"log_trn">>,?LOGFILETRN,true},
            {<<"log_cdr">>,?LOGFILECDR,true},
            {<<"log_media">>,?LOGFILEMEDIA,true},
            {<<"log_mgct">>,?LOGFILEMGCT,true}],
    lists:map(fun({OptKey,FileKey,Default}) -> application:set_env(?APP,FileKey,?EU:to_bool(?EU:get_by_key(OptKey,Opts,Default))) end, Logs).

% parse role from opts
get_role_module(Opts) ->
    case lists:keyfind(role, 1, Opts) of
        false -> {error, "Expected 'role' option"};
        {_, Role} ->
            application:set_env(?APP,role,Role),
            case get_module_by_role(Role) of
                invalid -> {error, "Unknown 'role' option value"};
                M -> {ok, M}
            end
    end.

% maps role code to module
get_module_by_role('redirect') -> r_sip_redirect_cb;
get_module_by_role('sg') -> r_sip_sg_cb;
get_module_by_role('b2bua') -> r_sip_b2bua_cb;
get_module_by_role('esg') -> r_sip_esg_cb;
get_module_by_role('conf') -> r_sip_conf_cb;
get_module_by_role('ivr') -> r_sip_ivr_cb;
get_module_by_role('prompt') -> r_sip_prompt_cb;
get_module_by_role(_) -> invalid.


% starts role
start_role_module(Module, Opts) ->
    ?ENV:append_env(siprole, Module),
    set_opts(Module, Opts),
    case Module:start() of
        {ok, _AppId} -> ok;
        {error, _}=Error ->
            ?ENV:unset_env(Module),
            ?ENV:dropvalue_env(siprole, Module),
            Error
    end.

% stops roles
stop_role_modules([]) -> ok;
stop_role_modules([Module|Rest]) ->
    ?ENV:unset_env(Module),
    ?OUT("SIP ~s stopping...", [Module]),
    Module:stop(),
    stop_role_modules(Rest).

% adds app's dependencies paths to code
add_deps_paths(Opts) ->
    add_deps_paths(),
    Path = ?EU:drop_last_subdir(code:which(?MODULE), 2),
    add_subapp_path(Path, Opts).

add_deps_paths() ->
    Path = ?EU:drop_last_subdir(code:which(?MODULE), 2),
    RPath = ?EU:drop_last_subdir(code:which(?MODULE), 3),
    Deps = [%RPath ++ "/era_env/deps/goldrush/ebin",
            %RPath ++ "/era_env/deps/lager/ebin",
            %Path ++ "/deps/gun/ebin",
%%            Path ++ "/_build/default/lib/ranch/ebin",
%%            Path ++ "/_build/default/lib/cowboy/ebin",
%%            Path ++ "/_build/default/lib/cowlib/ebin",
%%            Path ++ "/_build/default/lib/mimetypes/ebin",
%%            Path ++ "/_build/default/lib/nklib/ebin",
%%            Path ++ "/_build/default/lib/nkpacket/ebin",
%%            Path ++ "/_build/default/lib/nkserver/ebin",
%%            Path ++ "/_build/default/lib/nksip/ebin"
           ],
    code:add_pathsa(Deps).

add_subapp_path(Path, Opts) ->
    case lists:keyfind(role, 1, Opts) of
        {role, ivr} -> ok;
%%            Mixer = filename:join([filename:dirname(Path), ?EU:to_list(?EnvRoles:get_app_name(mixer)), "ebin"]),
%%            Script = filename:join([filename:dirname(Path), ?EU:to_list(?EnvRoles:get_app_name(script)), "ebin"]),
%%            code:add_pathsa([Mixer, Script]);
        _ -> ok
    end.

%% ----------------------------------------------

check_copy_common_files() -> ok.
%%    Priv = code:priv_dir(?APP),
%%    Src = filename:join([Priv,"sync","common"]),
%%    Dst = ?ENVNAMING:get_sync_common_folder(),
%%    case ?Rfilelib:is_dir(Src) of
%%        false -> ok;
%%        true ->
%%            Paths = ?Rfilelib:wildcard(Src ++ "/**"),
%%            {_, Files} = lists:partition(fun ?Rfilelib:is_dir/1, Paths),
%%            lists:foreach(fun(FileSrc) ->
%%                                  FileDst = filename:join([Dst,filename:join(lists:nthtail(length(filename:split(Src)), filename:split(FileSrc)))]),
%%                                  case ?Rfilelib:is_regular(FileDst) of
%%                                      false ->
%%                                          ?OUT("copy(1) ~ts to ~ts", [FileSrc,FileDst]),
%%                                          ?EU:file_copy(FileSrc,FileDst);
%%                                      true ->
%%                                          case ?EU:file_md5(FileSrc)==?EU:file_md5(FileDst) of
%%                                              false ->
%%                                                  ?OUT("copy(2) ~ts to ~ts", [FileSrc,FileDst]),
%%                                                  ?EU:file_copy(FileSrc,FileDst);
%%                                              true -> ok
%%                                          end
%%                                  end end, Files)
%%    end.
