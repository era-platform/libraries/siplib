%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 30.10.2015
%%% @doc Facade
%%%      Incapsulates logic of interaction of server with Media Gateway Controller.
%%%      Service prepares Context Option, which then saved outside.
%%%      Then every event, concerned to MGC-interaction, is passing to service with Context Option.
%%%      Supports two-side dialogs:
%%%        - Forwarding incoming invite to several forks, finalizing forking state with 2xx response;
%%%        - Update caller's MG with 1xx SDPs;
%%%        - Forwarding reinvites;
%%%        - Updating local SDPs by known audio formats (applying audio transcoding if need);
%%%        - Migrating whole dialog on another MG/MGC.

-module(r_sip_media_lib_b2b).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([fwd_start_media/3,
         re_start_media/2, re_start_media/3,
         stop_media/1,
         check_media_mg/2,
         %
         fwd_invite_request_to_b/3,
         fwd_invite_response_to_a/2,
         update_term_by_remote/3,
         apply_invite_pickup_from_b/2,
         fwd_reinvite_request_to_y/2,
         fwd_reinvite_response_to_x/2,
         rollback_reinvite/1,
         mirror_reinvite_request/2,
         %
         re_reinvite_request/2,
         re_reinvite_response/3,
         ivr_attach/1,
         ivr_detach/1,
         modify_term_events/3,
         external_term_attach/4,
         external_term_detach/2,
         external_term_setup_topology_prompter/3,
         external_term_setup_topology/3,
         %
         onrefer_z_request/4,
         onrefer_z_response/2,
         onrefer_y_request/3,
         onrefer_y_response/2,
         %
         onreplace_x_subtract/2,
         onreplace_y_request/3,
         onreplace_y_response/2,
         onreplace_z_add/4,
         onreplace_z_add_mirror/4]).

%% ================================================================================
%% Define
%% ================================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_mgc.hrl").

%% ================================================================================
%% API functions
%% ================================================================================

%% ==============================================================
%% Start / stop
%% ==============================================================

%% ----------------------------------------------
%% Preparing media server to handle forwarding call
%%  forming SDP
%%  find responsible mediaserver
%%  create media context
%%  setup first termination (b-callee)
%%  registers current proc in MGC Pinger
%%  MOpts could contain : transcoding::on|off, rec::true|false
%%  returns media option
%% ----------------------------------------------
-spec fwd_start_media(MSID::term(), Invite::#sipmsg{}, MOpts::map()) ->
                             {ok, {SdpModif::function()}} |
                             {reply, {unsupported_media_type, Accept::binary()}} |
                             {reply, {unsupported_media_encoding, Encod::binary()}} |
                             {reply, Reply::atom()} |
                             {reply, {Reply::atom(), Reason::list()}}.
%% ----------------------------------------------
fwd_start_media(MSID, AReq, MOpts) ->
    #sipmsg{contacts=ARContacts,
            from={_FromUri, FromTag},
            cseq=_CSeq}=AReq,
    ?LOGMEDIA("-> start_media (fwd, cseq=~140p)", [_CSeq]),
    % mg opts (mgaddr, mgsfx)
    case maps:get(mg_opts, MOpts, undefined) of
        undefined -> MGOpts = [];
        MGOpts when is_list(MGOpts) -> ok
    end,
    % record
    ITS = maps:get(timestamp, MOpts, undefined),
    case Rec=maps:get(rec, MOpts, true) of
        true ->
            {Timestamp,RecPath} = ?U:get_media_context_rec_opts(), % ITS
            RecTS = ?EU:to_int(Timestamp),
            Frec = fun(_MGC,MG,CtxId) -> [{MG,CtxId,Timestamp,RecPath}] end,
            CtxOpts = [{'contextProp', [{"timestamp",Timestamp},
                                        {"recpath", RecPath}]}];
        false ->
            RecTS = ITS,
            Frec = fun(_,_,_) -> [] end,
            CtxOpts = []
    end,
    % create only same type as caller, others by forwarding request
    ASdp = ?U:extract_sdp(AReq),
    ATermType = ?MGC_TERM:build_term_type(ARContacts, ASdp), % handle error? create context uses a term type for b, every fork should add new term if not.
    Addr = ?U:get_host_addr(),
    SdpSessId = (10000 + erlang:phash2(MSID) rem 90000) * 2,
    Media = #media{start_opts=MOpts,
                   allow_transcoding=maps:get(transcoding, MOpts, true),
                   is_rec=Rec,
                   timestamp=RecTS,
                   caller=undefined,
                   caller_tag=FromTag,
                   caller_opts=[{'term_type', ATermType},
                                {'sdp_r', ASdp},
                                {'sdp_o', {"-", SdpSessId, SdpSessId, "IN", "IP4", Addr}}]},
    {ok,BaseSdp,BTermTemplate} = ?M_B2B_OA:b_term(ATermType,<<>>,Media),
    case ?MGC:mg_create_context({MSID}, [BTermTemplate], CtxOpts ++ MGOpts) of
        {ok,{MGC,MG,CtxId,BTerms}}
          when is_list(BTerms) andalso BTerms=/=[] andalso is_integer(CtxId) andalso CtxId=/=0 ->
            Media1 = Media#media{mgc=MGC,
                                 msid=MSID,
                                 mgid=MG,
                                 ctx=CtxId,
                                 rec_links=Frec(MGC,MG,CtxId),
                                 callee=BTerms, % list
                                 callee_opts=[{'sdp_o', {"-", SdpSessId+1, SdpSessId+1, "IN", "IP4", Addr}},
                                              {'sdp_l_base', BaseSdp}]},
            ?MGC_PINGER:add({MGC,MSID,MG,CtxId},self()),
            ?LOGMEDIA("<- start_media: ok, ~p", [CtxId]),
            {ok, Media1};
        {error, mg_all_busy} ->
            ?LOGMEDIA("<- start_media: Service Unavailable (Too Many Calls)"),
            {reply, ?ReplyOpts(503,"Too Many Calls","MG busy")};
        {error, mg_not_found} ->
            ?LOGMEDIA("<- start_media: Internal Server Error (MG connect error)"),
            {reply, ?InternalError("MG connect error")};
        {error, mgc_not_found} ->
            ?LOGMEDIA("<- start_media: Internal Server Error (MGC not found)"),
            {reply, ?InternalError("MGC not found")};
        {error, mgc_overload} ->
            ?LOGMEDIA("<- start_media: Internal Server Error (MGC overload)"),
            {reply, ?InternalError("MGC overload")};
        {error, mgc_connect_error} ->
            ?LOGMEDIA("<- start_media: Internal Server Error (MGC connect error)"),
            {reply, ?InternalError("MGC connect error")};
        {error, Reason} ->
            ?LOGMEDIA("<- start_media: Internal Server Error (~140p)", [Reason]),
            {reply, ?InternalError("MGC response error")};
        Res ->
            ?LOGMEDIA("<- start_media: Internal Server Error (MGC response error ~140p)", [Res]),
            {reply, ?InternalError("MGC invalid response")}
    end.

%% ----------------------------------------------
%% Preparing media server to handle migration of active call
%% ----------------------------------------------
%% /2
re_start_media(MSID, OldMedia) ->
    re_start_media(MSID, {undefined,undefined}, OldMedia).

%% /3
re_start_media(MSID, {AHost,BHost}, OldMedia) ->
    #media{}=OldMedia,
    ?LOGMEDIA("-> start_media (migration)"),
    % oldmedia
    #media{allow_transcoding=AllowTranscoding,
           caller=OldTermA,
           callee=OldTermB,
           caller_opts=OldOptsA,
           callee_opts=OldOptsB,
           is_rec=OldIsRec,
           timestamp=OldTS,
           start_opts=MOpts,
           rec_links=OldRecLinks}=OldMedia,
    % mg opts (mgaddr, mgsfx)
    case maps:get(mg_opts, MOpts, undefined) of
        undefined -> MGOpts = [];
        MGOpts when is_list(MGOpts) -> ok
    end,
    % record
    case OldIsRec of
        true ->
            {Timestamp,RecPath} = ?U:get_media_context_rec_opts(), % OldTS
            RecTS = ?EU:to_int(Timestamp),
            Frec = fun(_MGC,MG,CtxId,PrevRec) -> [{MG,CtxId,Timestamp,RecPath}|PrevRec] end,
            CtxOpts = [{'contextProp', [{"timestamp",Timestamp},
                                        {"recpath",RecPath}]}];
        false ->
            RecTS = OldTS,
            Frec = fun(_,_,_,_) -> [] end,
            CtxOpts = []
    end,
    %
    {TermATemplate,OptsA} = ?MGC_TERM:prepare_term_template_by_previous(OldTermA, OldOptsA, AllowTranscoding),
    {TermBTemplate,OptsB} = ?MGC_TERM:prepare_term_template_by_previous(OldTermB, OldOptsB, AllowTranscoding),
    % sequence the same as in initial invite to make joined mixed files on same stereo side
    case ?MGC:mg_create_context({MSID}, [TermBTemplate,TermATemplate], CtxOpts ++ MGOpts) of
        {ok,{MGC,MG,CtxId,[TermB,TermA]}}
          when is_integer(CtxId) andalso CtxId=/=0 ->
            Media = OldMedia#media{mgc=MGC,
                                   msid=MSID,
                                   mgid=MG,
                                   ctx=CtxId,
                                   timestamp=RecTS,
                                   rec_links=Frec(MGC,MG,CtxId,OldRecLinks),
                                   caller=TermA,
                                   callee=TermB,
                                   caller_opts=OptsA,
                                   callee_opts=OptsB},
            case ensure_aliased_terms([{a,AHost},{b,BHost}],Media,[]) of
                {ok,Media1} ->
                    ?MGC_PINGER:add({MGC,MSID,MG,CtxId},self()),
                    ?LOGMEDIA("<- start_media: ok, ~p", [CtxId]),
                    {ok, Media1};
                {error,_} ->
                    ?LOGMEDIA("<- start_media: Internal Server Error (Ensure aliased terms error)"),
                    {reply, ?InternalError("Ensure aliased terms error")}
            end;
        {error, mg_all_busy} ->
            ?LOGMEDIA("<- start_media: Service Unavailable (Too Many Calls)"),
            {reply, {503, "Too Many Calls"}};
        {error, mg_not_found} ->
            ?LOGMEDIA("<- start_media: Internal Server Error (MG connect error)"),
            {reply, {500, "Internal Error"}};
        {error, mgc_not_found} ->
            ?LOGMEDIA("<- start_media: Internal Server Error (MGC not found)"),
            {reply, ?InternalError("MGC not found")};
        {error, mgc_overload} ->
            ?LOGMEDIA("<- start_media: Internal Server Error (MGC overload)"),
            {reply, ?InternalError("MGC overload")};
        {error, mgc_connect_error} ->
            ?LOGMEDIA("<- start_media: Internal Server Error (MGC connect error)"),
            {reply, ?InternalError("MGC connect error")};
        {error, Reason} ->
            ?LOGMEDIA("<- start_media: Internal Server Error (~140p)", [Reason]),
            {reply, ?InternalError("MGC response error")};
        Res ->
            ?LOGMEDIA("<- start_media: Internal Server Error (MGC response error ~140p)", [Res]),
            {reply, ?InternalError("MGC invalid response")}
    end.

%% @private
ensure_aliased_terms([],Media,[]) ->
    {ok,Media};
ensure_aliased_terms([],Media,SubtractTerms) ->
    #media{mgc=MGC,
           msid=MSID,
           mgid=MG,
           ctx=CtxId}=Media,
    TermsIds = ?MGC_TERM:get_terms_id(SubtractTerms),
    ?MGC:mg_subtract_terms({MGC,MSID,MG,CtxId},TermsIds),
    {ok,Media};
ensure_aliased_terms([{_,undefined}|T],Media,SubtractTerms) ->
    ensure_aliased_terms(T,Media,SubtractTerms);
ensure_aliased_terms([{Side,HostX}|T],#media{mgid=MG}=Media,SubtractTerms) ->
    MgAliasRaw = ?U:get_mg_alias_for_addr(MG,HostX),
    case ?U:finalize_mg_alias(MgAliasRaw) of
        {error,_}=Err -> Err;
        {ok,MgAlias} ->
            case ?U:is_default_alias(MgAlias) of
                true -> ensure_aliased_terms(T,Media,SubtractTerms);
                false ->
                    #media{mgc=MGC,
                           msid=MSID,
                           mgid=MG,
                           ctx=CtxId,
                           allow_transcoding=AllowTranscoding}=Media,
                    [OldTerm,OldOpts] = parse_side_term_opts(Side,Media),
                    {_,_,OldTOpts} = OldTerm,
                    TOpts = lists:keystore(iface,1,OldTOpts,{iface,MgAlias}),
                    Term = erlang:setelement(3,OldTerm,TOpts),
                    {TermTemplate,Opts} = ?MGC_TERM:prepare_term_template_by_previous(Term, OldOpts, AllowTranscoding),
                    case ?MGC:mg_add_terms({MGC,MSID,MG,CtxId},[TermTemplate]) of % add callee's term to mg
                        {error,_}=Err -> Err;
                        {ok, [NewTerm]} ->
                            Media1 = set_term(Side,NewTerm,Opts,Media),
                            ensure_aliased_terms(T,Media1,[OldTerm|SubtractTerms])
                    end
            end
    end.

%% @private
parse_side_term_opts(a,#media{caller=OldTermA,caller_opts=OldOptsA}) ->
    [OldTermA,OldOptsA];
parse_side_term_opts(b,#media{callee=OldTermB,callee_opts=OldOptsB}) ->
    [OldTermB,OldOptsB].

%% @private
set_term(a,NewTerm,Opts,Media) ->
    Media#media{caller=NewTerm,caller_opts=Opts};
set_term(b,NewTerm,Opts,Media) ->
    Media#media{callee=NewTerm,callee_opts=Opts}.

%% ----------------------------------------------
%% Stops media-context on call final
%% ----------------------------------------------
-spec stop_media(Media:: undefined | #media{}) -> ok.

stop_media(undefined) -> ok;
stop_media(Media) ->
    ?LOGMEDIA("-> stop_media"),
    #media{mgc=MGC,msid=MSID,mgid=MG,ctx=CtxId}=Media,
    ?MGC_PINGER:del({MGC,MSID,MG,CtxId}),
    ?MGC:mg_delete_context({MGC,MSID,MG,CtxId}),
    ?LOGMEDIA("<- stop_media ~p", [CtxId]),
    ok.

%% ----------------------------------------------
%% Checks if media-context is linked to appropriate MG
%% ----------------------------------------------
check_media_mg(#media{msid=_MSID, mgid=_MG}=_Media, [_MSID,_MG,_Mode]) -> true;
check_media_mg(_,_) -> false.

%% ==============================================================
%% Modify sdp on forwarded INVITE - RESPONSE (with sdp)
%% ==============================================================

%% ----------------------------------------------
%% when fork invite requested (a -> b)
%% ----------------------------------------------
fwd_invite_request_to_b(#media{}=Media, BRUri, BCallId) ->
    ?M_B2B_FWD:fwd_invite_request_to_b(Media, BRUri, BCallId).

%% ----------------------------------------------
%% when fork invite 1xx, 2xx responsed with sdp (a <- b)
%% ----------------------------------------------
fwd_invite_response_to_a(#media{}=Media, #sipmsg{}=BResp) ->
    ?M_B2B_FWD:fwd_invite_response_to_a(Media, BResp).

%% ----------------------------------------------
%% when ack with sdp
%% ----------------------------------------------
update_term_by_remote(Media, Side, RSdp) ->
    ?M_B2B_FWD:update_term_by_remote(Media, Side, RSdp).

%% ----------------------------------------------
%% when pickup invite call intercepts forking
%% ----------------------------------------------
apply_invite_pickup_from_b(#media{}=Media, #sipmsg{}=BReq) ->
    ?M_B2B_FWD:apply_invite_pickup_from_b(Media, BReq).

%% ----------------------------------------------
%% when dialog reinvite requested (x -> y)
%% ----------------------------------------------
fwd_reinvite_request_to_y(#media{}=Media, #sipmsg{}=XReq) ->
    ?M_B2B_FWD:fwd_reinvite_request_to_y(Media, XReq).

%% ----------------------------------------------
%% when dialog reinvite responsed (x <- y)
%% ----------------------------------------------
fwd_reinvite_response_to_x(#media{}=Media, #sipmsg{}=YResp) ->
    ?M_B2B_FWD:fwd_reinvite_response_to_x(Media, YResp).

%% ----------------------------------------------
%% when dialog reinvite responsed 4xx-6xx (x <- y)
%% ----------------------------------------------
rollback_reinvite(#media{}=Media) ->
    ?M_B2B_FWD:rollback_reinvite(Media).

%% ----------------------------------------------
%% when dialog reinvite request and shoudl be auto answered
%% ----------------------------------------------
mirror_reinvite_request(#media{}=Media, #sipmsg{}=XReq) ->
    ?M_B2B_FWD:mirror_reinvite_request(Media, XReq).

%% ==============================================================
%% Attach/detach IVR player
%% ==============================================================

%% ----------------------------------------------
ivr_attach(Media) ->
    #media{mgc=MGC,msid=MSID,mgid=MG,ctx=CtxId}=Media,
    IvrTT = ?MGC_TERM:prepare_term_template('ivrp'), % prepare ivr term template for mg
    IvrTT1 = ?MGC_TERM:set_term_ivr_play(IvrTT, #{%file => filename:join([":SYNC", ?ENVNAMING:get_subpath_common_waitonhold(), "hold.wav"]),
                                           %files => ["/files/wait/snd_knock.wav","/files/wait/snd_trash.wav"],
                                           %dir => "/files/1",
                                           dir => filename:join([":SYNC", get_waitonhold_dir()]),
                                           random => true,
                                           loop => true,
                                           %startat => 5000,
                                           %stopat => 6000,
                                           volume => 2
                                          }),
    case ?MGC:mg_add_terms({MGC,MSID,MG,CtxId},[IvrTT1]) of % add ivr term to mg
        {error,_}=Err -> Err;
        {ok, [IvrTerm]} ->
            Media1 = Media#media{ivr=IvrTerm},
            {ok,Media1}
    end.

%% ----------------------------------------------
ivr_detach(#media{ivr=undefined}=Media) -> {ok,Media};
ivr_detach(#media{ivr=IvrTerm}=Media) ->
    #media{mgc=MGC,msid=MSID,mgid=MG,ctx=CtxId}=Media,
    TermsId = ?MGC_TERM:get_term_id(IvrTerm),
    ?MGC:mg_subtract_terms({MGC,MSID,MG,CtxId},[TermsId]),
    Media1 = Media#media{ivr=undefined},
    {ok,Media1}.

%% ----------------------------------------------
%% Modify only term events descriptor (ex. send dtmf)
%% ----------------------------------------------
modify_term_events(Media, SideLitera, {RequestId,Events}=Ev) when is_integer(RequestId), is_list(Events) ->
    #media{mgc=MGC, msid=MSID, mgid=MG, ctx=CtxId}=Media,
    ?LOGMEDIA("-> modify_term (ctx=~p, callid=~140p)", [CtxId, SideLitera]),
    Term = case SideLitera of
               a -> Media#media.caller;
               b -> Media#media.callee
           end,
    {Type,Id,_Opts}=_Term1 = ?MGC_TERM:set_term_events(Term, Ev), % prepare term for #media
    Term2 = ?MGC_TERM:set_term_events({Type,Id,[]}, Ev), % prepare term for mg
    case ?MGC:mg_modify_terms({MGC,MSID,MG,CtxId},[Term2]) of % modify term in context
        {error,Reason}=Err ->
            ?LOGMEDIA("<- modify_term: Error (reason=~140p)", [Reason]),
            Err;
        {ok,[_Term3]} ->
            {ok,Media}
    end.

%% ==============================================================
%% Attach/detach external terminations
%% ==============================================================

%% ----------------------------------------------
external_term_attach(TId,TType,Opts,Media) ->
    ?M_B2B_PROMPT:external_term_attach(TId,TType,Opts,Media).

%% ----------------------------------------------
external_term_detach(TId,Media) ->
    ?M_B2B_PROMPT:external_term_detach(TId,Media).

%% ----------------------------------------------
external_term_setup_topology_prompter(TId,OnewaySides,Media) ->
    ?M_B2B_PROMPT:external_term_setup_topology_prompter(TId,OnewaySides,Media).

external_term_setup_topology(TId,Topology,Media) ->
    ?M_B2B_PROMPT:external_term_setup_topology(TId,Topology,Media).

%% ==============================================================
%% Modify sdp on migrating REINVITE - RESPONSE
%% ==============================================================

%% ----------------------------------------------
%% when server's migrating reinvite request (SRV -> z)
%% ----------------------------------------------
re_reinvite_request(#media{}=Media, RTag) ->
    ?M_B2B_MIGRATE:reinvite_request(Media, RTag).

%% ----------------------------------------------
%% when server's migrating reinvite responsed (SRV <- z)
%% ----------------------------------------------
re_reinvite_response(#media{}=Media, RTag, #sipmsg{}=Resp) ->
    ?M_B2B_MIGRATE:reinvite_response(Media, RTag, Resp).

%% ==============================================================
%% Modify sdp on refer
%% ==============================================================

%% ----------------------------------------------
%% when server's after-refer invite request (SRV -> z)
%% ----------------------------------------------
onrefer_z_request(Media,XTag,ZIfaceAlias,SendReinvite) ->
    ?M_B2B_REFER:z_request(Media,XTag,ZIfaceAlias,SendReinvite).

%% ----------------------------------------------
%% when server's after-refer invite response (SRV <- z)
%% ----------------------------------------------
onrefer_z_response(Media,Resp) ->
    ?M_B2B_REFER:z_response(Media,Resp).

%% ----------------------------------------------
%% when server's after-refer reinvite request (SRV -> y)
%% ----------------------------------------------
onrefer_y_request(Media,YTag,ZRSdp) ->
    ?M_B2B_REFER:y_request(Media,YTag,ZRSdp).

%% ----------------------------------------------
%% when server's after-refer reinvite response (SRV <- y)
%% ----------------------------------------------
onrefer_y_response(Media,Resp) ->
    ?M_B2B_REFER:y_response(Media,Resp).

%% ==============================================================
%% Modify sdp on replaces
%% ==============================================================

%% ----------------------------------------------
%% subtract x side when server got replaces invite
%% ----------------------------------------------
onreplace_x_subtract(Media,XTag) ->
    ?M_B2B_REPLACES:x_subtract(Media,XTag).

%% ----------------------------------------------
%% prepare local sdp for y reinvite (SRV -> y)
%% ----------------------------------------------
onreplace_y_request(Media,YTag,ZRSdp) ->
    ?M_B2B_REPLACES:y_request(Media,YTag,ZRSdp).

%% ----------------------------------------------
%% updates y by local and remote sdps (SRV <- y)
%% ----------------------------------------------
onreplace_y_response(Media,Resp) ->
    ?M_B2B_REPLACES:y_response(Media,Resp).

%% ----------------------------------------------
%% add z by remote and local
%% ----------------------------------------------
onreplace_z_add(Media, {ZReq,YResp}, {ZTag,XTag}, ZIfaceAlias) ->
    ?M_B2B_REPLACES:z_add(Media, {ZReq,YResp}, {ZTag,XTag}, ZIfaceAlias).

%% ----------------------------------------------
%% add z by remote and auto mirror local
%% ----------------------------------------------
onreplace_z_add_mirror(Media, ZReq, {ZTag,XTag}, ZIfaceAlias) ->
    ?M_B2B_REPLACES:z_add_mirror(Media, ZReq, {ZTag,XTag}, ZIfaceAlias).

%% ================================================================================
%% Internal functions
%% ================================================================================

%% ----------------------------------------------
%% @private
%% return link to folder where music_on_hold files are located.
%%  prefers custom directory where admin put files over webserver api if there are sound files (wav & mp3), and system if there is no one.
%% ----------------------------------------------
get_waitonhold_dir() ->
    ?ENVSTORE:lazy_t('waitonholddir', fun() -> get_waitonhold_dir_1() end).
%%
get_waitonhold_dir_1() ->
    Custom = ?ENVNAMING:get_subpath_common_waitonholdcustom(),
    System = ?ENVNAMING:get_subpath_common_waitonhold(),
    CustomF = filename:join([filename:join(lists:droplast(filename:split(?ENVNAMING:get_sync_common_folder()))),Custom]),
    case ?Rfilelib:is_dir(CustomF) of
        false -> System;
        true ->
            Paths = ?Rfilelib:wildcard(CustomF ++ "/**"),
            {_, Files} = lists:partition(fun ?Rfilelib:is_dir/1, Paths),
            SoundFiles = lists:filter(fun(FN) -> case ?EU:lowercase(filename:extension(FN)) of
                                                     ".wav" -> true;
                                                     ".mp3" -> true;
                                                     _ -> false
                                                 end end, Files),
            case SoundFiles of
                [] -> System;
                _ -> Custom
            end
    end.
