%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Modify sdp on two-side dialog's forwarded (request to mgc)
%%%        first INVITE–RESPONSE (+forking),
%%%        re-INVITE–RESPONSE

-module(r_sip_media_lib_b2b_fwd).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([fwd_invite_request_to_b/3,
         fwd_invite_response_to_a/2,

         update_term_by_remote/3,

         apply_invite_pickup_from_b/2,

         fwd_reinvite_request_to_y/2,
         fwd_reinvite_response_to_x/2,

         rollback_reinvite/1,
         mirror_reinvite_request/2]).

%% ================================================================================
%% Define
%% ================================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_mgc.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ==============================================================
%% Initial INVITE - RESPONSE
%% ==============================================================

%% ----------------------------------------------
%% When new forking request is ought to be send
%%   MGC already reserved, Context and B-term of default type are created. Base SDP is already in MEDIA option.
%% Returns Local SDP for B-Request from B-term. If type is not agreed to RUri, then creates another B-term.
%% ----------------------------------------------
-spec fwd_invite_request_to_b(Media::#media{}, BRUri::#uri{}, BCallId::binary()) -> {Media1::#media{}, LocalSdp::#sdp{}} | {error, Reason::term()}.
%% ----------------------------------------------
fwd_invite_request_to_b(Media, BRUri, BCallId) ->
    ?LOGMEDIA("   media_request_to_b in first invite response, create term A"),
    % first invite request forward (could be forked)
    #media{callee=Callees,
           callee_opts=CalleeOpts,
           start_opts=StartOpts} = Media,
    {_,BLSdpO} = lists:keyfind('sdp_o', 1, CalleeOpts),
    % find (already created) term by term type, make local callee's sdp
    BTermType = ?MGC_TERM:build_term_type([BRUri]),
    % iface alias not used, because forking is b2bua function and b2bua now uses only one interface. esg has single fork and term is already created.
    ToAlias = get_alias_to(BCallId,StartOpts),
    case ?MGC_TERM:find_term_by_type({BTermType, ToAlias, BCallId}, Callees) of
        false ->
            % if term not found by type, adding another term to context
            #media{mgc=MGC,msid=MSID,mgid=MG,ctx=CtxId}=Media,
            % iface alias
            {ok,_BaseSdp,BTermTemplate} = ?M_B2B_OA:b_term(BTermType,BCallId,Media),
            case ?MGC:mg_add_terms({MGC,MSID,MG,CtxId},[BTermTemplate]) of % add callee's term to mg
                {error,_}=Err -> Err;
                {ok, [BTerm]} ->
                    BTerm1 = ?MGC_TERM:set_term_callid(BTerm, BCallId),
                    fwd_b_1({BTerm, BLSdpO}, Media#media{callee=[BTerm1|Callees]})
            end;
        T ->
            fwd_b_1({T, BLSdpO}, Media)
    end.

% @private ---
fwd_b_1({BTerm, BLSdpO}, Media) ->
    case ?D_SDP:make_sdp(?MGC_TERM:get_term_localsdp(BTerm), BLSdpO) of
        #sdp{}=BLSdp ->
            % push callee's local sdp to sip request
            {ok, Media, ?M_SDP:set_sess(BLSdp)};
        _ ->
            {error, "MGC sdp error"}
    end.

%% ----------------------------------------------
%% When response from B-fork has come, it should be resend to A
%%   First response creates A-Term in existing Context, another modifies Local SDP
%% Returns Local SDP for A-Response from A-Term.
%% ----------------------------------------------
-spec fwd_invite_response_to_a(Media::#media{}, Resp::#sipmsg{}) -> {ok, Media1::#media{}, LocalSdp::#sdp{}} | {error, Reason::term()}.
%% ----------------------------------------------

%% first response+sdp (from forked callee)
fwd_invite_response_to_a(#media{caller=undefined}=Media, BResp) ->
    % first invite response: caller term not created yet
    ?LOGMEDIA("   media_response_to_a in first invite response, create term A"),
    #sdp{}=BRSdp=?U:extract_sdp(BResp),
    #media{mgc=MGC, msid=MSID, mgid=MG, ctx=CtxId,
           caller_opts=AOpts,
           callee_opts=BOpts}=Media,
    % add caller term to MG
    [ALSdpO] = ?EU:extract_required_props(['sdp_o'], AOpts), % take saved caller's term type (rtp,srtp,webrtc) and remote sdp,
    {ok,_BaseSdp,ATermTemplate} = ?M_B2B_OA:a_term(BRSdp,Media), % prepare term template for mg
    case ?MGC:mg_add_terms({MGC,MSID,MG,CtxId},[ATermTemplate]) of % add caller's term to mg
        {error,_}=Err -> Err;
        {ok, [ATerm]} ->
            Media1 = Media#media{caller_opts=lists:keystore('sdp_l_base',1,AOpts,{'sdp_l_base',BRSdp})},
            fwd_a_1({ATerm, ALSdpO}, {BResp, BOpts}, Media1)
    end;

%% another response+sdp (from forked callee)
fwd_invite_response_to_a(#media{}=Media, BResp) ->
    % first invite response: caller term already created
    ?LOGMEDIA("   media_response_to_a in first invite response, modify term A"),
    #sipmsg{contacts=BRContacts,
            call_id=BCallId}=BResp,
    #sdp{}=BRSdp=?U:extract_sdp(BResp),
    #media{mgc=MGC, msid=MSID, mgid=MG, ctx=CtxId,
           caller=ATerm,
           caller_opts=AOpts,
           callee=BTerms,
           callee_opts=BOpts,
           start_opts=StartOpts}=Media,
    % modify terms in MG
    BTermType = ?MGC_TERM:build_term_type(BRContacts, BRSdp), % who responsed
    ToAlias = get_alias_to(BCallId,StartOpts),
    case ?MGC_TERM:find_term_by_type({BTermType, ToAlias, BCallId}, BTerms) of
        false -> {error,<<"term type error">>};
        BTerm ->
            case ?MEDIA_SESSCHANGE:update_sesschange_sdp(Media, {BRSdp,AOpts,ATerm}) of
                {error,_}=Err -> Err;
                {ok, ALSdp, AOpts1} ->
                    ALSdp1 = ?M_B2B_OA:a_lsdp(ALSdp,Media#media{caller_opts=AOpts1}),
                    ATerm1 = ?MGC_TERM:set_term_localsdp(ATerm, ?D_SDP:prepare_descr_direct(ALSdp1)),
                    BTerm1 = ?MGC_TERM:set_term_remotesdp(BTerm, ?D_SDP:prepare_descr_direct(BRSdp)),
                    case ?MGC:mg_modify_terms_ex({MGC,MSID,MG,CtxId},[{ATerm1,[localsdp]},{BTerm1,[remotesdp]}]) of
                        {error,_}=Err -> Err;
                        {ok, [ATerm2, BTerm2]} ->
                            % update media state
                            Media1 = Media#media{caller=ATerm2,
                                                 caller_opts=lists:keystore('sdp_l_base',1,AOpts1,{'sdp_l_base',BRSdp}),
                                                 callee=?MGC_TERM:update_term_by_type(BTerms, BTerm2),
                                                 callee_opts=lists:keystore('sdp_r',1,BOpts,{'sdp_r',BRSdp})},
                            case finalize_callee_terms(false, BResp, Media1) of
                                {error,_}=Err -> Err;
                                {ok, Media2} ->
                                    % push caller's local sdp to sip answer
                                    {ok, Media2, ?M_SDP:set_sess(ALSdp1)}
                            end end end end.

%x(#sdp{}=Sdp) -> lists:keyfind("m",1,?D_SDP:prepare_descr_direct(Sdp));
%x(Sdp) when is_list(Sdp) -> lists:keyfind("m",1,Sdp).

% @private ---
% finishing of fun fwd_invite_response_to_a
fwd_a_1({ATerm, ALSdpO}, {BResp, BOpts}, Media) ->
    #sdp{}=BRSdp=?U:extract_sdp(BResp),
    % take local caller's sdp
    case ?D_SDP:make_sdp(?MGC_TERM:get_term_localsdp(ATerm), ALSdpO) of
        #sdp{}=ALSdp ->
            Media1 = Media#media{caller=ATerm,
                                 callee_opts=lists:keystore('sdp_r',1,BOpts,{'sdp_r',BRSdp})},
            % try finalize callee terms (on 2xx)
            case finalize_callee_terms(true, BResp, Media1) of
                {error,_}=Err -> Err;
                {ok, Media2} ->
                    % push caller's local sdp to sip answer
                    {ok, Media2, ?M_SDP:set_sess(ALSdp)}
            end;
        _ ->
            {error, "MGC sdp error"}
    end.

%% ==============================================================
%% Ack's remote sdp
%% ==============================================================

update_term_by_remote(#media{}=Media, a, #sdp{}=RSdp) ->
    #media{caller=Term,caller_opts=Opts}=Media,
    FApply = fun(TermX) -> Media#media{caller=TermX,caller_opts=lists:keystore('sdp_r',1,Opts,{'sdp_r',RSdp})}  end,
    do_update_term_by_remote(Media, Term, RSdp, FApply);

update_term_by_remote(#media{}=Media, b, #sdp{}=RSdp) ->
    #media{callee=Term,callee_opts=Opts}=Media,
    FApply = fun(TermX) -> Media#media{callee=TermX,callee_opts=lists:keystore('sdp_r',1,Opts,{'sdp_r',RSdp})}  end,
    do_update_term_by_remote(Media, Term, RSdp, FApply).

%% @private
do_update_term_by_remote(Media, Term, RSdp, FApply) ->
    #media{mgc=MGC,msid=MSID,mgid=MG,ctx=CtxId}=Media,
    % modify z term by remote sdp
    Term1 = ?MGC_TERM:set_term_remotesdp(Term, ?D_SDP:prepare_descr_direct(RSdp)),
    case ?MGC:mg_modify_terms({MGC,MSID,MG,CtxId},[Term1]) of
        {error,_}=Err -> Err;
        {ok, [Term2]} ->
            {ok, FApply(Term2)}
    end.

%% ==============================================================
%% Pickup invite
%% ==============================================================

% @TODO
apply_invite_pickup_from_b(#media{}=Media, BReq) ->
    % pickup invite request
    ?LOGMEDIA("   media_pickup, create term B"),
    #sipmsg{contacts=BRContacts,
            call_id=BCallId,
            from={BFrom,_}}=BReq,
    BRSdp=?U:extract_sdp(BReq),
    #media{mgc=MGC, msid=MSID, mgid=MG, ctx=CtxId,
           caller_opts=AOpts,
           callee_opts=BOpts,
           start_opts=StartOpts}=Media,
    % extract props
    [ALSdpO] = ?EU:extract_required_props(['sdp_o'], AOpts),
    [BLSdpO] = ?EU:extract_required_props(['sdp_o'], BOpts),
    % iface alias not used, because pickup is b2bua function and b2bua now uses only one interface
    BTermType = ?MGC_TERM:build_term_type(BRContacts, BRSdp),
    % mg with several ifaces. found media ifaces RP-2078
    BSource = ?U:get_call_source(BFrom, BReq),
    BHost = ?U:get_caller_address(BSource,BReq),
    BForkAddressRaw = ?U:get_mg_alias_for_addr(MG,BHost),
    case ?U:finalize_mg_alias(BForkAddressRaw) of
        {error,_}=Err -> Err;
        {ok,BMgAlias} ->
            {FrAlias,_} = maps:get(aliases,StartOpts),
            Media1 = Media#media{start_opts=StartOpts#{aliases=>{FrAlias,BMgAlias}}},
            % pickup term to MG as callee
            {ok,BSdpLBase,BTermTemplate} = ?M_B2B_OA:ib_term(BTermType,BRSdp,BCallId,Media1),
            % caller term
            {ok,ASdpLBase,ATermTemplate} = ?M_B2B_OA:ia_term(BRSdp,Media1),
            % add terms to MG
            case ?MGC:mg_add_terms({MGC,MSID,MG,CtxId},[ATermTemplate, BTermTemplate]) of % add pickuper's term to mg
                {error,_}=Err -> Err;
                {ok, [ATerm, BTerm]} ->
                    Media2 = Media1#media{caller_opts=lists:keystore('sdp_l_base',1,AOpts,{'sdp_l_base',ASdpLBase}),
                                          callee_opts=lists:keystore('sdp_l_base',1,BOpts,{'sdp_l_base',BSdpLBase})},
                    fin_pickup_1({ATerm, ALSdpO}, {BTerm, BLSdpO}, {BReq, BOpts}, Media2)
            end
    end.

% @private ---
% finishing of fun apply_pickup_from_b
fin_pickup_1({ATerm, ALSdpO}, {BTerm, BLSdpO}, {BReq, BOpts}, Media) ->
    #sipmsg{from={_,BRTag}}=BReq,
    BRSdp=?U:extract_sdp(BReq),
    % delete terms
    case Media of
        #media{caller=undefined,callee=BTerms} -> subtract_terms(BTerms, Media);
        #media{caller=ATerm,callee=BTerms} -> subtract_terms([ATerm|BTerms], Media)
    end,
    % take local caller's sdp
    case {?D_SDP:make_sdp(?MGC_TERM:get_term_localsdp(ATerm), ALSdpO),
          ?D_SDP:make_sdp(?MGC_TERM:get_term_localsdp(BTerm), BLSdpO)} of
        {#sdp{}=ALSdp, #sdp{}=BLSdp} ->
            Media1 = Media#media{stage=dlg,
                                 caller=ATerm,
                                 callee=BTerm,
                                 callee_tag=BRTag,
                                 callee_opts=lists:keystore('sdp_r',1,BOpts,{'sdp_r',BRSdp})},
            {ok, Media1, ?M_SDP:set_sess(ALSdp), ?M_SDP:set_sess(BLSdp)};
        _ ->
            {error, "MGC sdp error"}
    end.

%% ==============================================================
%% Modify sdp on forwarded REINVITE - RESPONSE
%% ==============================================================

%% ----------------------------------------------
%% When reINVITE request from X has come, it should be resend to Y
%% ----------------------------------------------
-spec fwd_reinvite_request_to_y(Media::#media{}, XReq::#sipmsg{}) -> {ok, Media1::#media{}, LocalSdp::#sdp{}} | {error,Reason::term()}.
%% ----------------------------------------------
fwd_reinvite_request_to_y(#media{}=Media, #sipmsg{}=XReq) ->
    % reinvite request foward (not forked), could be pending
    #sipmsg{cseq={CSeq,_M}, from={_,FTag}}=XReq,
    #sdp{}=XRemoteSdp=?U:extract_sdp(XReq),
    ?LOGMEDIA("   media_request_to_y in reinvite"),
    % update local sdp by request sdp, update media option
    case ?M_B2B_RE:update_reinvite_req_sdp(Media, {CSeq,FTag}, XRemoteSdp) of
        {error,_}=Err -> Err;
        {ok, Media1, #sdp{}=YLocalSdp} ->
            {ok, Media1, ?M_SDP:set_sess(YLocalSdp)}
    end.

%% ----------------------------------------------
%% When reINVITE response from Y has come, it should be resend to X
%% ----------------------------------------------
-spec fwd_reinvite_response_to_x(Media::#media{}, YResp::#sipmsg{}) -> {ok, Media1::#media{}, LocalSdp::#sdp{}} | {error,Reason::term()}.
%% ----------------------------------------------
fwd_reinvite_response_to_x(#media{}=Media, #sipmsg{}=YResp) ->
    % reinvite response
    #sipmsg{cseq=CSeq, to={_,TTag}}=YResp,
    #sdp{}=YRemoteSdp=?U:extract_sdp(YResp),
    ?LOGMEDIA("   media_response_to_x in reinvite"),
    % update local sdp by response sdp, update media opt
    case ?M_B2B_RE:update_reinvite_resp_sdp(Media, {CSeq,TTag}, YRemoteSdp) of
        {error,_}=Err -> Err;
        {ok, Media1, #sdp{}=XLocalSdp} ->
            % modify both terms in MG, update media opt
            case ?M_B2B_RE:update_reinvite_terms(Media1, {XLocalSdp, TTag}) of
                {error,_}=Err -> Err;
                {ok, #sdp{}=XLocalSdp1, Media2} ->
                    {ok, Media2, ?M_SDP:set_sess(XLocalSdp1)}
            end
    end.

%% ----------------------------------------------
%% When reINVITE response 4xx-6xx
%% ----------------------------------------------
-spec rollback_reinvite(Media::#media{}) -> {ok, Media1::#media{}} | {error,Reason::term()}.
%% ----------------------------------------------
rollback_reinvite(#media{}=Media) ->
    ?M_B2B_RE:rollback(Media).

%% ----------------------------------------------
%% When reINVITE request from X has come, and should be auto answered
%% ----------------------------------------------
mirror_reinvite_request(#media{}=Media, #sipmsg{}=XReq) ->
    % reinvite request foward (not forked), could be pending
    #sipmsg{cseq={CSeq,_M}, from={_,FTag}}=XReq,
    #sdp{}=XRemoteSdp=?U:extract_sdp(XReq),
    ?LOGMEDIA("   mirror_reinvite_request in reinvite"),
    % update local sdp by request sdp, update media option
    case ?M_B2B_RE:mirror_reinvite_req_sdp(Media, {CSeq,FTag}, XRemoteSdp) of
        {error,_}=Err -> Err;
        {ok, Media1, #sdp{}=XLocalSdp} ->
            {ok, Media1, ?M_SDP:set_sess(XLocalSdp)}
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ----------------------------------------------
%% finalize callee terms from forked.
%% on 2xx - modify callee term by remote sdp, drop other forked terms
%% ----------------------------------------------
finalize_callee_terms(ModifyBTerm,
                      #sipmsg{class={resp,SipCode,_R},
                              to={_,TTag},
                              contacts=BRContacts,
                              call_id=BCallId}=BResp,
                      #media{callee=BTerms,
                             callee_opts=BOpts,
                             start_opts=StartOpts}=Media)
  when SipCode>=200, SipCode<300, is_list(BTerms) ->
    #sdp{}=BRSdp=?U:extract_sdp(BResp),
    BTermType = ?MGC_TERM:build_term_type(BRContacts, BRSdp),
    ToAlias = get_alias_to(BCallId,StartOpts),
%%    ?OUT("=============="),
%%    ?OUT("ModifyBTerm: ~120p",[ModifyBTerm]),
%%    ?OUT("StartOpts: ~120p", [StartOpts]),
%%    ?OUT("BTerms: ~120p",[BTerms]),
%%    ?OUT("BTermType: ~120p",[BTermType]),
%%    ?OUT("ToAlias: ~120p",[ToAlias]),
%%    ?OUT("BCallId: ~120p",[BCallId]),
%%    ?OUT("BRSdp: ~120p",[BRSdp]),
%%    ?OUT("BRContacts: ~120p",[BRContacts]),
%%    ?OUT("=============="),
    case ?MGC_TERM:find_term_by_type({BTermType, ToAlias, BCallId}, BTerms) of
        false -> {error,<<"term type error">>};
        BTerm ->
            subtract_terms(BTerms -- [BTerm], Media),
            ModifyRes = case ModifyBTerm of % @todo {error,_}
                            true -> modify_callee_on_answer(BTerm,BResp,Media);
                            false -> {ok,BTerm}
                        end,
            case ModifyRes of
                {error,_}=Err -> Err;
                {ok,BTerm1} ->
                    {ok,Media#media{stage=dlg,
                                    callee=BTerm1,
                                    callee_tag=TTag,
                                    callee_opts=lists:keystore('sdp_r',1,BOpts,{'sdp_r',BRSdp})}}
            end end;
%% on 1xx - modify callee term by remote sdp, keep other forked terms
finalize_callee_terms(ModifyBTerm,
                      #sipmsg{contacts=BRContacts,
                              call_id=BCallId}=BResp,
                      #media{callee=BTerms,
                             start_opts=StartOpts}=Media)
  when is_list(BTerms) ->
    #sdp{}=BRSdp=?U:extract_sdp(BResp),
    BTermType = ?MGC_TERM:build_term_type(BRContacts, BRSdp),
    ToAlias = get_alias_to(BCallId,StartOpts),
    case ?MGC_TERM:find_term_by_type({BTermType, ToAlias, BCallId}, BTerms) of
        false -> {error,<<"term type error">>};
        BTerm ->
            ModifyRes = case ModifyBTerm of % @todo {error,_}
                            true -> modify_callee_on_answer(BTerm,BResp,Media);
                            false -> {ok,BTerm}
                        end,
            case ModifyRes of
                {error,_}=Err -> Err;
                {ok,BTerm1} ->
                    {ok, Media#media{stage=dlg,
                                     callee=?MGC_TERM:update_term_by_type(BTerms, BTerm1)}}
            end end;
%% else
finalize_callee_terms(_ModifyBTerm,_SipMsg, Media) -> {ok,Media}.

%% @private
modify_callee_on_answer(BTerm, BResp, Media) ->
    #sdp{}=BRSdp=?U:extract_sdp(BResp),
    #media{mgc=MGC,msid=MSID,mgid=MG,ctx=CtxId}=Media,
    BTerm1 = ?MGC_TERM:set_term_remotesdp(BTerm, ?D_SDP:prepare_descr_direct(BRSdp)),
    case ?MGC:mg_modify_terms_ex({MGC,MSID,MG,CtxId},[{BTerm1,[remotesdp]}]) of
        {error,_}=Err -> Err;
        {ok, [BTerm2]} -> {ok, BTerm2}
    end.

%% ----------------------------------------------
subtract_terms([],_) -> ok;
subtract_terms(Terms, Media) ->
    #media{mgc=MGC,msid=MSID,mgid=MG,ctx=CtxId}=Media,
    TermsIds = ?MGC_TERM:get_terms_id(Terms),
    ?MGC:mg_subtract_terms({MGC,MSID,MG,CtxId},TermsIds).

%% ----------------------------------------------
get_alias_to(BCallId,StartOpts) ->
    case maps:get(aliases,StartOpts,undefined) of
        undefined -> undefined;
        {_,BMgAliasMap} when is_map(BMgAliasMap) -> maps:get(BCallId,BMgAliasMap,undefined);
        {_,BMgAlias} -> BMgAlias
    end.