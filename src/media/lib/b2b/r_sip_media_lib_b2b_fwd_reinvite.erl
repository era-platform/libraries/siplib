%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Provides service to handle and update SDP of re-INVITE requests and responses
%%%        Applied to dialog state's media, calls MGC

-module(r_sip_media_lib_b2b_fwd_reinvite).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([update_reinvite_req_sdp/3,
         update_reinvite_resp_sdp/3,
         update_reinvite_terms/2,

         rollback/1,
         mirror_reinvite_req_sdp/3]).

%% ================================================================================
%% Define
%% ================================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_mgc.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

% -------------------------------
% when reinvite request
% -------------------------------
update_reinvite_req_sdp(#media{caller_tag=FTag}=Media, {CSeq,FTag}, FrRemoteSdp) ->
    %?LOGMEDIA("   reInvite. Request from caller. update sdp"),
    #media{callee_opts=ToOpts, callee=ToTerm}=Media,
    FunOk = fun(Media1,ToOpts1,ToLocalSdp1) -> {ok, Media1#media{callee_opts=ToOpts1}, ToLocalSdp1} end,
    update_reinvite_req_sdp_1(Media, {CSeq,FTag,'b',ToOpts,ToTerm}, FrRemoteSdp, FunOk);

update_reinvite_req_sdp(#media{callee_tag=FTag}=Media, {CSeq,FTag}, FrRemoteSdp) ->
    %?LOGMEDIA("   reInvite. Request from callee. update sdp"),
    #media{caller_opts=ToOpts, caller=ToTerm}=Media,
    FunOk = fun(Media1,ToOpts1,ToLocalSdp1) -> {ok, Media1#media{caller_opts=ToOpts1}, ToLocalSdp1} end,
    update_reinvite_req_sdp_1(Media, {CSeq,FTag,'a',ToOpts,ToTerm}, FrRemoteSdp, FunOk).

% ----
update_reinvite_req_sdp_1(Media, {CSeq,FTag,ToSide,ToOpts,ToTerm}, FrRemoteSdp, FunOk) ->
    ToLSdpPrev = ?MGC_TERM:get_term_localsdp(ToTerm),
    [XRemoteSdpPrev] = ?EU:extract_optional_props(['sdp_l_base'], ToOpts),
    case ?MEDIA_SESSCHANGE:update_sesschange_sdp(Media, {FrRemoteSdp,ToOpts,ToTerm}) of
        {error,_}=Err -> Err;
        {ok, ToLocalSdp1, ToOpts1} ->
            ToLocalSdp2 = ?M_SDP:transcode_offer(Media#media.allow_transcoding, ToLocalSdp1),
            ReOpts = #reopts{cseq=CSeq, ftag=FTag, fsdp_r=FrRemoteSdp, tsdp_l=ToLocalSdp2,
                             toside=ToSide, tsdpd_l_prev=ToLSdpPrev, tsdpd_base_prev=XRemoteSdpPrev},
            FunOk(Media#media{re_opts=ReOpts},ToOpts1,ToLocalSdp2)
    end.

% -------------------------------
% when reinvite response
% -------------------------------
update_reinvite_resp_sdp(#media{callee_tag=TTag}=Media, {CSeq,TTag}, ToRemoteSdp) ->
    %?LOGMEDIA("   reInvite. Ok response from callee. update sdp"),
    #media{caller_opts=FOpts, caller=FTerm}=Media, % opposite
    FunOk = fun(Media1, FrOpts1, FrLocalSdp) -> {ok, Media1#media{caller_opts=FrOpts1}, FrLocalSdp} end,
    update_reinvite_resp_sdp_1(Media, {CSeq,TTag,FOpts,FTerm}, ToRemoteSdp, FunOk);

update_reinvite_resp_sdp(#media{caller_tag=TTag}=Media, {CSeq,TTag}, ToRemoteSdp) ->
    %?LOGMEDIA("   reInvite. Ok response from caller. update sdp"),
    #media{callee_opts=FOpts, callee=FTerm}=Media, % opposite
    FunOk = fun(Media1, FrOpts1, FrLocalSdp) -> {ok, Media1#media{callee_opts=FrOpts1}, FrLocalSdp} end,
    update_reinvite_resp_sdp_1(Media, {CSeq,TTag,FOpts,FTerm}, ToRemoteSdp, FunOk).

% ----
update_reinvite_resp_sdp_1(#media{re_opts=ReOpts}=Media, {_CSeq,TTag,FOpts,FTerm}, ToRemoteSdp, FunOk) ->
    case ?MEDIA_SESSCHANGE:update_sesschange_sdp(Media, {ToRemoteSdp,FOpts,FTerm}) of
        {error,_}=Err -> Err;
        {ok, FrLocalSdp1, FOpts1} ->
            #reopts{fsdp_r=FrRemoteSdp}=ReOpts,
            FrLocalSdp2 = ?M_SDP:transcode_answer(Media#media.allow_transcoding,FrLocalSdp1,FrRemoteSdp), % After matching brsdp and alsdp. Choose ?LO_REDUCE(FrLocalSdp1,FrRemoteSdp) vs ?LO(FrLocalSdp1,FrRemoteSdp)
            ReOpts1 = ReOpts#reopts{ttag=TTag, tsdp_r=ToRemoteSdp, fsdp_l=FrLocalSdp2},
            FunOk(Media#media{re_opts=ReOpts1}, FOpts1, FrLocalSdp2)
    end.

% -------------------------------
% on successfull response modifying terms
% -------------------------------
update_reinvite_terms(#media{callee_tag=TTag, caller=Caller, callee=Callee}=Media, {XLocalSdp,TTag}) ->
    %?LOGMEDIA("  reInvite. Apply terms. to callee (~140p). update sdp", [TTag]),
    update_reinvite_terms_1(Media, {XLocalSdp,TTag,Caller,Callee});
update_reinvite_terms(#media{caller_tag=TTag, caller=Caller, callee=Callee}=Media, {XLocalSdp,TTag}) ->
    %?LOGMEDIA("  reInvite. Apply terms. to caller (~140p). update sdp", [TTag]),
    update_reinvite_terms_1(Media, {XLocalSdp,TTag,Callee,Caller}).

update_reinvite_terms_1(Media, {XLocalSdp,TTag,FromTerm,ToTerm}) ->
    #media{mgc=MGC, msid=MSID, mgid=MG, ctx=CtxId,
           re_opts=ReOpts,
           callee_tag=CalleeTag,
           caller_opts=CallerOpts,
           callee_opts=CalleeOpts}=Media,
    #reopts{fsdp_r=FSdpR, fsdp_l=FSdpL, tsdp_r=TSdpR, tsdp_l=TSdpL}=ReOpts,
    FTerm = ?MGC_TERM:set_term_sdp(FromTerm, ?D_SDP:prepare_descr_direct(FSdpL), ?D_SDP:prepare_descr_direct(FSdpR)),
    TTerm = ?MGC_TERM:set_term_sdp(ToTerm, ?D_SDP:prepare_descr_direct(TSdpL), ?D_SDP:prepare_descr_direct(TSdpR)),
    %?LOGMEDIA("  DEBUG reInvite. Media on TERM UP (1): ~n\t~140p~n\t~140p", [FTerm, TTerm]),
    case ?MGC:mg_modify_terms({MGC,MSID,MG,CtxId},[FTerm,TTerm]) of
        {error,_}=Err -> Err;
        {ok, [FTerm1,TTerm1]} ->
            %?LOGMEDIA("  DEBUG reInvite. Media on TERM UP (2): ~n\t~140p~n\t~140p", [FTerm1, TTerm1]),
            % RP-1268 return xlocalsdp
            #sdp{user=U, id=Id, vsn=Vsn, address={OType, OAddrType, OAddress}}=XLocalSdp,
            XLSdpD1 = ?MGC_TERM:get_term_localsdp(FTerm1),
            #sdp{}=XLocalSdp1 = ?D_SDP:make_sdp(XLSdpD1,{U,Id,Vsn,OType,OAddrType,OAddress}),
            %
            Media1 = Media#media{re_opts=undefined},
            F = fun(TCaller, TCallee, RSdpCaller, RSdpCallee) ->
                        {ok, XLocalSdp1, Media1#media{caller=TCaller,
                                                      callee=TCallee,
                                                      caller_opts=lists:keystore('sdp_r',1,CallerOpts,{'sdp_r',RSdpCaller}),
                                                      callee_opts=lists:keystore('sdp_r',1,CalleeOpts,{'sdp_r',RSdpCallee})}}
                end,
            case TTag==CalleeTag of
                true -> F(FTerm1, TTerm1, FSdpR, TSdpR);
                false -> F(TTerm1, FTerm1, TSdpR, FSdpR)
            end
    end.

% -------------------------------
% when reinvite response 4xx-6xx
%
rollback(#media{}=Media) ->
    #media{mgc=MGC, msid=MSID, mgid=MG, ctx=CtxId,
           re_opts=ReOpts}=Media,
    #reopts{toside=ToSide,
            tsdpd_l_prev=LSdpPrevD,
            tsdpd_base_prev=BaseSdpPrev}=ReOpts,
    case ToSide of
        'a' -> #media{caller=ToTerm, caller_opts=ToOpts}=Media;
        'b' -> #media{callee=ToTerm, callee_opts=ToOpts}=Media
    end,
    LSdpDT = LSdpPrevD, %?D_SDP:prepare_descr_direct(LSdpPrev),
    ToTerm1 = ?MGC_TERM:set_term_localsdp(?MGC_TERM:prepare_term_opts(ToTerm, [localsdp, remotesdp]), LSdpDT),
    case ?MGC:mg_modify_terms({MGC,MSID,MG,CtxId},[ToTerm1]) of
        {error,_}=Err -> Err;
        {ok,[ToTerm2]} ->
            ?LOGMEDIA("  DEBUG reInvite. rollback: ~n  SdpOut ~140p", [ToTerm2]),
            ToOpts2 = lists:keystore('sdp_l_base', 1, ToOpts, {'sdp_l_base', BaseSdpPrev}),
            case ToSide of
                'a' -> {ok,Media#media{caller=ToTerm2,caller_opts=ToOpts2}};
                'b' -> {ok,Media#media{callee=ToTerm2,callee_opts=ToOpts2}}
            end
    end.

% -------------------------------
% when reinvite request should be mirrored (no forward)
%
mirror_reinvite_req_sdp(#media{caller_tag=FTag}=Media, {_CSeq,FTag}, FrRemoteSdp) ->
    %?LOGMEDIA("   reInvite. Request from caller. mirror"),
    #media{caller_opts=ToOpts, caller=ToTerm}=Media, % self
    FunSetOpts = fun(Media1,ToOpts1) -> Media1#media{caller_opts=ToOpts1} end,
    FunSetTerm = fun(Media1,Term1) -> Media1#media{caller=Term1} end,
    mirror_reinvite_req_sdp_1(Media, {ToOpts,ToTerm}, FrRemoteSdp, FunSetOpts, FunSetTerm);

mirror_reinvite_req_sdp(#media{callee_tag=FTag}=Media, {_CSeq,FTag}, FrRemoteSdp) ->
    %?LOGMEDIA("   reInvite. Request from callee. mirror"),
    #media{callee_opts=ToOpts, callee=ToTerm}=Media, % self
    FunSetOpts = fun(Media1,ToOpts1) -> Media1#media{callee_opts=ToOpts1} end,
    FunSetTerm = fun(Media1,Term1) -> Media1#media{callee=Term1} end,
    mirror_reinvite_req_sdp_1(Media, {ToOpts,ToTerm}, FrRemoteSdp, FunSetOpts, FunSetTerm).

% ----
mirror_reinvite_req_sdp_1(Media, {ToOpts,ToTerm}, FrRemoteSdp, FunSetOpts, FunSetTerm) ->
    [FrPrev] = ?EU:extract_optional_props(['sdp_l_base'], ToOpts),
    % #272
    %FrRemoteSdp1 = ?M_SDP:setmode(FrPrev, ?M_SDP:reverse_stream_mode(?M_SDP:get_audio_mode(FrRemoteSdp))),
    FrRemoteSdp1 = ?M_SDP:setmode(FrPrev, ?M_SDP:get_audio_mode(FrRemoteSdp)),
    case ?MEDIA_SESSCHANGE:update_sesschange_sdp(Media, {FrRemoteSdp1,ToOpts,ToTerm}) of
        {error,_}=Err -> Err;
        {ok, ToLocalSdp1, ToOpts1} ->
            ToLocalSdp2 = ?M_SDP:transcode_offer(Media#media.allow_transcoding, ToLocalSdp1),
            #media{mgc=MGC, msid=MSID, mgid=MG, ctx=CtxId} = Media,
            ToLocalSdp3 = ?M_SDP:filter_by_local_media_fmts(?M_SDP:reverse_stream_mode(ToLocalSdp2), undefined),
            ToTerm1 = ?MGC_TERM:set_term_sdp(ToTerm, ?D_SDP:prepare_descr_direct(ToLocalSdp3), ?D_SDP:prepare_descr_direct(FrRemoteSdp)),
            case ?MGC:mg_modify_terms({MGC,MSID,MG,CtxId},[ToTerm1]) of
                {error,_}=Err -> Err;
                {ok, [ToTerm2]} ->
                    Media1 = FunSetTerm(Media,ToTerm2),
                    Media2 = FunSetOpts(Media1,lists:keystore('sdp_r',1,ToOpts1,{'sdp_r',FrRemoteSdp})),
                    {ok, Media2, ToLocalSdp3}
            end
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

