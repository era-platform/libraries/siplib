%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Operates mg context/term recreation
%%% @doc Modify sdp on two-side dialog migration from one mg to another
%%%         Create another context, request to mgc

-module(r_sip_media_lib_b2b_migrate).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([reinvite_request/2,
         reinvite_response/3]).

%% ================================================================================
%% Define
%% ================================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_mgc.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

% -----------------------------
% when server's migrating reinvite request (SRV -> z)
% Simply get LocalSdp from term info for sending in INVITE,
% it's already prepared there while creating context, even 'o=' item was incremented
%
reinvite_request(#media{caller_tag=RTag}=Media, RTag) ->
    #media{caller=Term,
           caller_opts=Opts}=Media,
    LSdp = extract_localsdp({Term, Opts}),
    {ok, Media, ?M_SDP:set_sess(LSdp)};

reinvite_request(#media{callee_tag=RTag}=Media, RTag) ->
    #media{callee=Term,
           callee_opts=Opts}=Media,
    LSdp = extract_localsdp({Term, Opts}),
    {ok, Media, ?M_SDP:set_sess(LSdp)}.

% -----------------------------
% when server's migrating reinvite responsed (SRV <- z)
% Put remote SDP in opts of media, Push to MGC to update remote side
%
reinvite_response(#media{caller_tag=RTag}=Media, RTag, #sipmsg{}=Resp) ->
    #media{caller=Term,
           caller_opts=Opts}=Media,
    Fun = fun({Term1,Opts1}, Media1) -> Media1#media{caller=Term1, caller_opts=Opts1} end,
    apply_remotesdp({Term, Opts, Fun}, Media, Resp);

reinvite_response(#media{callee_tag=RTag}=Media, RTag, #sipmsg{}=Resp) ->
    #media{callee=Term,
           callee_opts=Opts}=Media,
    Fun = fun({Term1,Opts1}, Media1) -> Media1#media{callee=Term1, callee_opts=Opts1} end,
    apply_remotesdp({Term, Opts, Fun}, Media, Resp).

%% ====================================================================
%% Internal functions
%% ====================================================================

% ----
extract_localsdp({Term, Opts}) ->
    {_,LSdpO} = lists:keyfind('sdp_o', 1, Opts),
    #sdp{}=LSdp = ?D_SDP:make_sdp(?MGC_TERM:get_term_localsdp(Term), LSdpO),
    LSdp.

% ----
apply_remotesdp({Term, Opts, Fun}, Media, Resp) ->
    #media{mgc=MGC,msid=MSID,mgid=MG,ctx=CtxId}=Media,
    #sdp{}=RSdp=?U:extract_sdp(Resp),
    Term1 = ?MGC_TERM:set_term_remotesdp(Term, ?D_SDP:prepare_descr_direct(RSdp)),
    Opts1 = lists:keystore('sdp_r', 1, Opts, {'sdp_r',RSdp}),
    case ?MGC:mg_modify_terms_ex({MGC,MSID,MG,CtxId},[{Term1,[remotesdp]}]) of
        {error,_}=Err -> Err;
        {ok, [Term2]} ->
            Media1 = Fun({Term2, Opts1}, Media),
            {ok, Media1}
    end.
