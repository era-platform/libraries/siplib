%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 02.10.2018
%%% @doc RP-956 no-sdp-invite
%%%      Sdp Offer-Answer build routines

-module(r_sip_media_lib_b2b_oa).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([b_term/3,
         b_sdp_offer/1,
         a_term/2,
         a_lsdp/2,
         ib_term/4,
         ia_term/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_mgc.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% FORKING TO B
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% ----------------------------------
%% Prepare 'b' term template for outgoing INVITE request. Contains sdp offer.
%%   Either based on a side remote sdp or not instead of no-sdp-INVITE.
%% ----------------------------------
-spec b_term(TermType::rtp|webrtc|srtp,BCallId::binary(),#media{}) -> {ok,BaseSdp::#sdp{},Term::tuple()}.
%% ----------------------------------
b_term(BTermType,BCallId,#media{start_opts=MOpts}=Media) ->
    BIfaceAlias = get_alias_to(BCallId,MOpts),
    BaseSdp = b_sdp_offer(Media),
    TermTemplate = ?MGC_TERM:prepare_term_template({BIfaceAlias,BTermType}, BaseSdp),
    {ok,BaseSdp,TermTemplate}.

%% ----------------------------------
%% Prepare outgoing offer sdp
%% ----------------------------------
-spec b_sdp_offer(#media{}) -> #sdp{}.
%% ----------------------------------
b_sdp_offer(#media{caller_opts=CallerOpts}=Media) ->
    case ?EU:get_by_key('sdp_r', CallerOpts, false) of
        #sdp{}=ARSdp -> ?M_SDP:transcode_offer(Media#media.allow_transcoding, ARSdp);
        _ -> ?SENDRECV(?LO(?M_SDP:make_offer_sdp()))
    end.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ANSWER TO A
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% ----------------------------------
%% Prepare 'a' term template for outgoing 200 OK response. outgoing offer sdp
%% TODO ARSdp is undefined
%% ----------------------------------
-spec a_term(BRSdp::#sdp{}, #media{}) -> {ok,BaseSdp::#sdp{},Term::tuple()}.
%% ----------------------------------
a_term(BRSdp,#media{start_opts=MOpts,
                    caller_opts=AOpts}=Media) ->
    ATermType = ?EU:get_by_key('term_type', AOpts),
    AIfaceAlias = get_alias_from(MOpts),
    case ?EU:get_by_key('sdp_r', AOpts, false) of
        #sdp{}=ARSdp ->
            BaseSdp = ?M_SDP:transcode_offer(Media#media.allow_transcoding, BRSdp, ARSdp),
            TermTemplate = ?MGC_TERM:prepare_term_template({AIfaceAlias,ATermType}, BaseSdp, ARSdp),
            {ok,BaseSdp,TermTemplate};
        _ ->
            BaseSdp = ?M_SDP:transcode_offer(Media#media.allow_transcoding, BRSdp),
            TermTemplate = ?MGC_TERM:prepare_term_template({AIfaceAlias,ATermType}, BaseSdp),
            {ok,BaseSdp,TermTemplate}
    end.

a_lsdp(ALSdp,#media{caller_opts=AOpts}=Media) ->
    {_,ARSdp} = lists:keyfind('sdp_r',1,AOpts),
    % After matching brsdp and alsdp. Choose ?LO_REDUCE(ALSdp,ARSdp) vs ?LO(ALSdp,ARSdp)
    ?M_SDP:transcode_answer(Media#media.allow_transcoding,ALSdp,ARSdp).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INTERCEPT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% ----------------------------------
%% Prepare intercepting 'b' term template for incoming INVITE response.
%% TODO ARSdp is undefined
%% TODO BRSdp is undefined
%% ----------------------------------
-spec ib_term(BTermType::rtp|webrtc|srtp, BRSdp::#sdp{}, BCallId::binary(), #media{}) -> {ok,BaseSdp::#sdp{},Term::tuple()}.
%% ----------------------------------
ib_term(BTermType,BRSdp,BCallId,#media{caller_opts=AOpts,start_opts=MOpts}=Media) ->
    BIfaceAlias = get_alias_to(BCallId,MOpts),
    ARSdp = ?EU:get_by_key('sdp_r', AOpts, false), % take saved caller's remote sdp,
    BaseSdp = ?M_SDP:transcode_answer(Media#media.allow_transcoding,ARSdp,BRSdp),
    TermTemplate = ?MGC_TERM:prepare_term_template({BIfaceAlias,BTermType}, BaseSdp, BRSdp),
    {ok,BaseSdp,TermTemplate}.

%% ----------------------------------
%% Prepare intercepted 'a' term template for outgoing 200 OK response.
%% TODO ARSdp is undefined
%% TODO BRSdp is undefined
%% ----------------------------------
-spec ia_term(BRSdp::#sdp{}, #media{}) -> {ok,BaseSdp::#sdp{},Term::tuple()}.
%% ----------------------------------
ia_term(BRSdp,#media{caller_opts=AOpts,start_opts=MOpts}=Media) ->
    AIfaceAlias = get_alias_from(MOpts),
    [ATermType,ARSdp] = ?EU:extract_required_props(['term_type','sdp_r'], AOpts), % take saved caller's remote sdp,
    BaseSdp = ?M_SDP:transcode_answer(Media#media.allow_transcoding,BRSdp,ARSdp),
    TermTemplate = ?MGC_TERM:prepare_term_template({AIfaceAlias,ATermType}, BaseSdp, ARSdp),
    {ok,BaseSdp,TermTemplate}.

%% -----------------------------------
get_alias_from(StartOpts) ->
    case maps:get(aliases,StartOpts,undefined) of
        undefined -> undefined;
        {FrAlias,_} -> FrAlias
    end.

get_alias_to(BCallId,StartOpts) ->
    case maps:get(aliases,StartOpts,undefined) of
        undefined -> undefined;
        {_,BMgAliasMap} when is_map(BMgAliasMap) -> maps:get(BCallId,BMgAliasMap,undefined);
        {_,BMgAlias} -> BMgAlias
    end.