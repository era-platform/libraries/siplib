%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 14.06.2022
%%% @doc

-module(r_sip_media_lib_b2b_prompt).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    external_term_attach/4,
    external_term_detach/2,
    external_term_setup_topology_prompter/3,
    external_term_setup_topology/3
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_mgc.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ----------------------------------------------
external_term_attach(TId,TType,Opts,Media) ->
    case TType of
        'play' ->
            ExTerm0 = ?MGC_TERM:prepare_term_template('ivrp'), % prepare ivr term template for mg
            ExTerm1 = set_ivrterm(TType, ExTerm0, get_opts(TType, Opts)),
            do_add_external_term_playrec(Media, {TId,TType,ExTerm1});
        'rec' ->
            ExTerm0 = ?MGC_TERM:prepare_term_template('ivrr'), % prepare ivr term template for mg
            ExTerm1 = set_ivrterm(TType, ExTerm0, get_opts(TType, Opts)),
            do_add_external_term_playrec(Media, {TId,TType,ExTerm1});
        'rtp' ->
            case maps:get('dir',Opts,'out') of
                'in' ->
                    RSdp = maps:get('sdp',Opts),
                    {ok, ExTermTempl, XLSdpO} = prepare_in_term(TId,RSdp),
                    do_add_external_term_rtp(Media, {TId,TType,ExTermTempl}, XLSdpO);
                'out' ->
                    case maps:get('sdp',Opts,undefined) of
                        undefined ->
                            {ok, ExTermTempl, XLSdpO} = prepare_out_term(TId),
                            do_add_external_term_rtp(Media, {TId,TType,ExTermTempl}, XLSdpO);
                        RSdp ->
                            do_update_external_term_rtp(Media, TId, RSdp)
                    end
            end
    end.

%% ----------------------------------------------
external_term_detach(TId,Media) ->
    #media{external_terms=ExTerms,external_topology=ExTopology}=Media,
    case lists:keytake(TId,1,ExTerms) of
        false -> {ok,Media};
        {value,{_,_,ExTerm},Rest} ->
            TermId = ?MGC_TERM:get_term_id(ExTerm),
            ?MGC:mg_subtract_terms(get_ctxlnk(Media),[TermId]),
            Media1 = Media#media{external_terms=Rest,external_topology=lists:keydelete(TId,1,ExTopology)},
            {ok,Media1}
    end.

%% ----------------------------------------------
%% Modifies topology in context
%% ----------------------------------------------
-spec external_term_setup_topology_prompter(TId::term(), OnewaySides::[a|b], Media::#media{})
      -> {ok, NewMedia::#media{}} | {error, R::term()}.
%% ----------------------------------------------
external_term_setup_topology_prompter(TId,OnewaySides,#media{external_topology=ExTopology}=Media) ->
    BothwaySides = [a,b]--OnewaySides,
    O = lists:map(fun(X) -> {X,t,oneway} end, OnewaySides),
    B = lists:map(fun(X) -> {X,t,bothway} end, BothwaySides),
    case external_term_setup_topology(TId, O ++ B, Media) of
        {ok, Media1} -> {ok, Media1#media{external_topology=lists:keystore(TId,1,ExTopology,{TId,OnewaySides})}};
        Res -> Res
    end.

%% ----------------------------------------------
%% Modifies topology in context
%% ----------------------------------------------
-spec external_term_setup_topology(TId::term(), Topology::[{a|b|t, a|b|t, bothway|oneway|isolate}], Media::#media{})
        -> {ok, NewMedia::#media{}} | {error, R::term()}.
%% ----------------------------------------------
external_term_setup_topology(TId,Topology,Media) ->
    #media{ctx=CtxId,caller=Caller,callee=Callee,external_terms=ExTerms}=Media,
    case lists:keyfind(TId,1,ExTerms) of
        false -> {error,not_found};
        {_,_,ExTerm} ->
            F = fun(a) -> Caller; (b) -> Callee; (t) -> ExTerm end,
            Items = lists:map(fun({SideX,SideY,Mode}) -> {?MGC_TERM:get_term_id(F(SideX)),?MGC_TERM:get_term_id(F(SideY)),Mode} end, Topology),
            ?LOGMEDIA("-> modify_topology prompt (ctx=~p, topology=~140p)", [CtxId, Items]),
            Opts = [{'topology', Items}],
            case ?MGC:mg_modify_topology(get_ctxlnk(Media), Opts) of
                {error,Reason}=Err ->
                    ?LOGMEDIA("<- modify_topology prompt: Error (reason=~140p)", [Reason]),
                    Err;
                ok ->
                    {ok, Media}
            end
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
do_add_external_term_playrec(Media, {TId,TType,ExTerm}) ->
    #media{external_terms=ExTerms}=Media,
    case ?MGC:mg_add_terms(get_ctxlnk(Media),[ExTerm]) of % add ivr term to mg
        {error,_}=Err -> Err;
        {ok, [ExTerm1]} ->
            Media1 = Media#media{external_terms=[{TId,TType,ExTerm1}|ExTerms]},
            {ok,Media1}
    end.

%% @private
%% add new ex term
do_add_external_term_rtp(Media, {TId,TType,ExTerm}, XLSdpO) ->
    case ?MGC:mg_add_terms(get_ctxlnk(Media),[ExTerm]) of % add r+l term to mg
        {error,_}=Err -> Err;
        {ok, [XTerm]} ->
            #media{external_terms=ExTerms}=Media,
            Media1 = Media#media{external_terms=[{TId,TType,XTerm}|ExTerms]},
            case ?D_SDP:make_sdp(?MGC_TERM:get_term_localsdp(XTerm), XLSdpO) of
                #sdp{}=XLSdp ->
                    {ok,Media2} = apply_topology(TId,Media1),
                    {ok, ?M_SDP:set_sess(XLSdp), Media2};
                _ ->
                    ?LOGMEDIA("<- external_term_attach (rtp): Internal Server Error (MGC out SDP error)"),
                    {error, <<"MGC error on sdp">>}
            end
    end.

%% @private
%% update existing ex term by remote sdp
do_update_external_term_rtp(Media, TId, RSdp) ->
    #media{external_terms=ExTerms}=Media,
    case lists:keytake(TId,1,ExTerms) of
        false -> {error,<<"ExTerm not exists">>};
        {value,{_,TType,ExTerm},Rest} ->
            ExTerm1 = ?MGC_TERM:set_term_remotesdp(ExTerm, ?D_SDP:prepare_descr_direct(RSdp)),
            case ?MGC:mg_modify_terms(get_ctxlnk(Media),[ExTerm1]) of
                {error,_}=Err -> Err;
                {ok, [ExTerm2]} ->
                    Media1 = Media#media{external_terms=[{TId,TType,ExTerm2}|Rest]},
                    {ok,_Media2} = apply_topology(TId,Media1)
            end
    end.

%% @private
apply_topology(TId,#media{external_topology=ExTopology}=Media) ->
    #media{external_topology=ExTopology}=Media,
    case lists:keyfind(TId,1,ExTopology) of
        false -> {ok,Media};
        {_,OnewaySides} ->
            {ok,_Media1} = external_term_setup_topology(TId,OnewaySides,Media)
    end.

%% @private
prepare_in_term(TId,RSdp) ->
    Addr = ?U:get_host_addr(),
    TermType = ?MGC_TERM:build_term_type('rtp',RSdp), % handle error?
    SdpSessId = (10000 + erlang:phash2(TId) rem 90000) * 2,
    LSdpO = {"-", SdpSessId, SdpSessId, "IN", "IP4", Addr},
    LSdpBase = ?M_SDP:filter_by_local_media_fmts(?M_SDP:reverse_stream_mode(RSdp), undefined),
    TermTemplate = ?MGC_TERM:prepare_term_template(TermType, LSdpBase, RSdp), % prepare term template for mg
    {ok, TermTemplate, LSdpO}.

%% @private
prepare_out_term(TId) ->
    Addr = ?U:get_host_addr(),
    TermType = ?MGC_TERM:build_term_type(),
    SdpSessId = (10000 + erlang:phash2(TId) rem 90000) * 2,
    LSdpO = {"-", SdpSessId, SdpSessId, "IN", "IP4", Addr},
    Sdp = ?M_SDP:make_offer_sdp(),
    LSdpBase = ?SENDRECV(?LO(Sdp)),
    TermTemplate = ?MGC_TERM:prepare_term_template(TermType, LSdpBase), % prepare term template for mg
    {ok, TermTemplate, LSdpO}.

%% ------------
%% @private
get_ctxlnk(#media{mgc=MGC,msid=MSID,mgid=MG,ctx=CtxId}) -> {MGC,MSID,MG,CtxId}.

%% @private
get_opts(T, Opts) when is_list(Opts) -> get_opts(T, maps:from_list(Opts));
get_opts(play, Opts) -> maps:with([mode,file,files,dir,random,loop,startat,stopat,volume],Opts);
get_opts(rec, Opts) -> maps:with([file,codec_name,type,buffer_duration],Opts).

%% @private
set_ivrterm(T, IvrTerm, Opts) when is_list(Opts) -> set_ivrterm(T, IvrTerm, maps:from_list(Opts));
set_ivrterm(play, IvrTerm, Opts) -> ?MGC_TERM:set_term_ivr_play(IvrTerm, Opts);
set_ivrterm(rec, IvrTerm, Opts) -> ?MGC_TERM:set_term_ivr_rec(IvrTerm, Opts).
