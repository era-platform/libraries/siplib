%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Operates mg context/term modifications on sip refer

-module(r_sip_media_lib_b2b_refer).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([z_request/4,
         z_response/2,
         y_request/3,
         y_response/2]).

%% ================================================================================
%% Define
%% ================================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_mgc.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ---
%% when invite request to refer-to side
z_request(Media,XTag,ZIfaceAlias,SendReinvite) ->
    case x_inactivate(Media,XTag,not SendReinvite) of
        {error,_}=Err -> Err;
        {ok, Media1} ->
            z_add_term(Media1,XTag,ZIfaceAlias)
    end.

%% ---
%% when invite response from refer-to side
z_response(Media, #sipmsg{class={resp,SipCode,_}}=Resp) when SipCode<200 ->
    z_update_by_response(Media, Resp);
z_response(Media, #sipmsg{class={resp,SipCode,_}}=Resp) when SipCode>=200, SipCode<300 ->
    % update by remote sdp from response
    case z_update_by_response(Media, Resp) of
        {error,_}=Err -> Err;
        {ok,Media1} ->
            z_invite_response_2xx(Media1)
    end.

%% ---
%% when reinvite request to referred side
y_request(Media,YTag,ZRSdp) ->
    y_modify_term(Media,YTag,ZRSdp).

%% ---
%% when reinvite response from referred side
y_response(Media, #sipmsg{class={resp,SipCode,_}}=Resp) when SipCode>=200, SipCode<300 ->
    y_update_by_response(Media, Resp).

%% ====================================================================
%% Internal functions (Z-side refer-to operations)
%% ====================================================================

%% --------------------------------------
%% Switch x to inactive
%%
x_inactivate(Media,_XTag,true) -> {ok,Media};
x_inactivate(Media,XTag,false) ->
    #media{mgc=MGC,msid=MSID,mgid=MG,ctx=CtxId}=Media,
    Fup = case Media of
              #media{caller=XTerm,caller_tag=XTag,caller_opts=XOpts} -> fun(Med,Term,Opts) -> Med#media{caller=Term,caller_opts=Opts} end;
              #media{callee=XTerm,callee_tag=XTag,callee_opts=XOpts} -> fun(Med,Term,Opts) -> Med#media{callee=Term,callee_opts=Opts} end
          end,
    [XLSdpO, XRSdp, XBaseSdp] = ?EU:extract_required_props(['sdp_o', 'sdp_r','sdp_l_base'], XOpts),
    XRSdp1 = ?INACTIVE(XRSdp),
    XBaseSdp1 = ?INACTIVE(XBaseSdp), % may be recvonly for music-on-hold (when 'reinvite' option is false)
    XTerm1 = ?MGC_TERM:set_term_sdp(XTerm, ?D_SDP:prepare_descr_direct(XBaseSdp1), ?D_SDP:prepare_descr_direct(XRSdp1)),
    case ?MGC:mg_modify_terms({MGC,MSID,MG,CtxId},[XTerm1]) of
        {error,_}=Err -> Err;
        {ok, [XTerm2]} ->
            XLSdp2 = ?D_SDP:make_sdp(?MGC_TERM:get_term_localsdp(XTerm2), XLSdpO),
            XOpts1 = lists:keystore('sdp_r', 1, XOpts, {'sdp_r',XRSdp1}),
            XOpts2 = lists:keystore('sdp_l_base', 1, XOpts1, {'sdp_l_base',XLSdp2}),
            {ok, Fup(Media, XTerm2, XOpts2)}
    end.

%% --------------------------------------
%% Adding z term to context
%%
z_add_term(Media,XTag,ZIfaceAlias) ->
    #media{mgc=MGC,msid=MSID,mgid=MG,ctx=CtxId}=Media,
    case Media of
        #media{caller_tag=XTag,callee_tag=YTag,caller_opts=XOpts} -> ok;
        #media{callee_tag=XTag,caller_tag=YTag,callee_opts=XOpts} -> ok
    end,
    {_,ZLSdpO} = lists:keyfind('sdp_o', 1, XOpts), % base sdp session for invite
    ZLSdpO1 = erlang:setelement(3,ZLSdpO, erlang:element(3,ZLSdpO)+1),
    {_,ZBaseSdp} = lists:keyfind('sdp_l_base', 1, XOpts), % base opposite sdp formats for invite
    ZBaseSdp1 = ?SENDRECV(?M_SDP:transcode_offer(Media#media.allow_transcoding, ZBaseSdp)),
    ZTermType = ?MGC_TERM:build_term_type(),
    TermTemplate = ?MGC_TERM:prepare_term_template({ZIfaceAlias,ZTermType}, ZBaseSdp1),
    case ?MGC:mg_add_terms({MGC,MSID,MG,CtxId},[TermTemplate]) of % add z callee's term to mg
        {error,_}=Err -> Err;
        {ok, [ZTerm]} ->
            z_after_add({ZTerm, ZLSdpO1, ZBaseSdp, ZTermType, XTag, YTag}, Media)
    end.

% @private ---
z_after_add({ZTerm, ZLSdpO, ZBaseSdp, ZTermType, XTag, YTag}, Media) ->
    case ?D_SDP:make_sdp(?MGC_TERM:get_term_localsdp(ZTerm), ZLSdpO) of
        #sdp{}=ZLSdp ->
            % push callee's local sdp to sip request
            ZLSdp1 = ?M_SDP:set_sess(ZLSdp),
            Refer = #{term => ZTerm,
                      termtype => ZTermType,
                      remotetag => undefined,
                      sdp_o => ZLSdpO,
                      sdp_l => ZLSdp1,
                      sdp_r => undefined,
                      sdp_l_base => ZBaseSdp,
                      xtag => XTag,
                      ytag => YTag},
            {ok, Media#media{refer=Refer}, ZLSdp1};
        _ ->
            {error, "MGC sdp error"}
    end.

%% --------------------------------------
%% Update z refer-to term remote
%%
z_update_by_response(Media, Resp) ->
    #sipmsg{to={_,ZRTag}}=Resp,
    #sdp{}=ZRSdp=?U:extract_sdp(Resp),
    #media{mgc=MGC,msid=MSID,mgid=MG,ctx=CtxId,
           refer=#{term:=ZTerm}=Refer}=Media,
    % modify z term by remote sdp
    ZTerm1 = ?MGC_TERM:set_term_remotesdp(ZTerm, ?D_SDP:prepare_descr_direct(ZRSdp)),
    case ?MGC:mg_modify_terms({MGC,MSID,MG,CtxId},[ZTerm1]) of
        {error,_}=Err -> Err;
        {ok, [ZTerm2]} ->
            {ok, Media#media{refer=Refer#{term := ZTerm2,
                                          remotetag := ZRTag,
                                          sdp_r := ZRSdp}}}
    end.

%% --------------------------------------
%% Finalizing context refer works on 2xx answer from Z side
%%
z_invite_response_2xx(Media) ->
    % delete x term
    #media{refer=#{xtag:=XTag}}=Media,
    case delete_term(Media, XTag) of
        {error,_}=Err -> Err;
        {ok,Media1} ->
            % move refer to x in media
            #media{refer=#{term:=ZTerm,
                           termtype:=ZTermType,
                             remotetag:=ZRTag,
                           sdp_r:=ZRSdp,
                                sdp_o:=ZLSdpO,
                                sdp_l:=_ZLSdp,
                           sdp_l_base:=ZBaseSdp}}=Media1,
            ZTermOpts = [{termtype,ZTermType},
                         {'sdp_r',ZRSdp},
                         {'sdp_o',ZLSdpO},
                         {'sdp_l_base',ZBaseSdp}],
            case Media1#media{refer=undefined} of
                #media{caller_tag=XTag}=M ->
                    {ok, M#media{caller=ZTerm,
                                  caller_tag=ZRTag,
                                 caller_opts=ZTermOpts}};
                #media{callee_tag=XTag}=M ->
                    {ok, M#media{callee=ZTerm,
                                 callee_tag=ZRTag,
                                 callee_opts=ZTermOpts}}
            end
    end.

%% --------------------------------------
%% Deleting refering x term from context
%%
delete_term(Media,XTag) ->
    #media{mgc=MGC,msid=MSID,mgid=MG,ctx=CtxId}=Media,
    case Media of
        #media{caller_tag=XTag,caller=XTerm,callee=_YTerm} -> ok;
        #media{callee_tag=XTag,callee=XTerm,caller=_YTerm} -> ok
    end,
    TermIds = ?MGC_TERM:get_terms_id([XTerm]),
    case ?MGC:mg_subtract_terms({MGC,MSID,MG,CtxId},TermIds) of
        {error,_}=Err -> Err;
        {ok, _} -> {ok, Media}
    end.

%% ====================================================================
%% Internal functions (Y-side referred operations)
%% ====================================================================

%% -------------------------------------
%% We should make only one call to media. So make sdp by changes of previous sent. Last remote should be kept in local opts
%%
y_modify_term(Media,YTag,BaseSdp) ->
    case Media of
        #media{callee_tag=YTag,callee=YTerm,callee_opts=YOpts} -> F = fun(Med,Term,Opts) -> Med#media{callee=Term,callee_opts=Opts} end;
        #media{caller_tag=YTag,caller=YTerm,caller_opts=YOpts} -> F = fun(Med,Term,Opts) -> Med#media{caller=Term,caller_opts=Opts} end
    end,
    case ?MEDIA_SESSCHANGE:update_sesschange_sdp(Media, {BaseSdp,YOpts,YTerm}) of
        {error,_}=Err -> Err;
        {ok, YLocalSdp1, YOpts1} ->
            YLocalSdp2 = ?M_SDP:transcode_offer(Media#media.allow_transcoding, YLocalSdp1),
            YLocalSdp3 = ?M_SDP:set_sess(YLocalSdp2),
            YOpts2 = lists:keystore('sdp_l',1,YOpts1,{'sdp_l',YLocalSdp3}),
            {ok, F(Media,YTerm,YOpts2), YLocalSdp3}
    end.

%% -------------------------------------
%% Update y refering term remote
%%
y_update_by_response(Media, Resp) ->
    %{error,"Not Implemented"}.
    #sipmsg{to={_,YRTag}}=Resp,
    #sdp{}=YRSdp=?U:extract_sdp(Resp),
    #media{mgc=MGC,msid=MSID,mgid=MG,ctx=CtxId}=Media,
    case Media of
        #media{callee_tag=YRTag,callee=YTerm,callee_opts=YOpts} -> F = fun(Med,Term,Opts) -> Med#media{callee=Term,callee_opts=Opts} end;
        #media{caller_tag=YRTag,caller=YTerm,caller_opts=YOpts} -> F = fun(Med,Term,Opts) -> Med#media{caller=Term,caller_opts=Opts} end
    end,
    % modify z term by remote sdp
    {_,YLSdp} = lists:keyfind('sdp_l', 1, YOpts),
    YTerm1 = ?MGC_TERM:set_term_sdp(YTerm, ?D_SDP:prepare_descr_direct(YLSdp), ?D_SDP:prepare_descr_direct(YRSdp)),
    case ?MGC:mg_modify_terms({MGC,MSID,MG,CtxId},[YTerm1]) of
        {error,_}=Err -> Err;
        {ok, [YTerm2]} ->
            YOpts1 = lists:keystore('sdp_r',1,YOpts,{'sdp_r',YRSdp}),
            {ok, F(Media,YTerm2,YOpts1)}
    end.
