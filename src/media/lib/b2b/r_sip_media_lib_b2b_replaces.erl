%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Operates mg context/term modifications on sip replacing invite received

-module(r_sip_media_lib_b2b_replaces).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([x_subtract/2,
         y_request/3,
         y_response/2,
         z_add/4,
         z_add_mirror/4]).

%% ================================================================================
%% Define
%% ================================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_mgc.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

x_subtract(Media, XTag) ->
    x_delete_term(Media, XTag).
    %{error, "Not implemented"}.

y_request(Media,YTag,ZRSdp) ->
    y_modify_term(Media,YTag,ZRSdp).
    %{error, "Not implemented"}.

y_response(Media,Resp) ->
    y_update_by_response(Media, Resp).
    %{error, "Not implemented"}.

z_add(Media, {ZReq,YResp}, {ZTag,XTag}, ZIfaceAlias) ->
    #sdp{}=YRSdp=?U:extract_sdp(YResp),
    z_add_term(Media, ZReq, YRSdp, {ZTag,XTag}, ZIfaceAlias).
    %{error, "Not implemented"}.

z_add_mirror(Media, ZReq, {ZTag,XTag}, ZIfaceAlias) ->
    case Media of
        #media{caller_tag=XTag,caller_opts=XOpts} -> ok;
        #media{callee_tag=XTag,callee_opts=XOpts} -> ok
    end,
    #sdp{}=ZRSdp=?U:extract_sdp(ZReq),
    [FrPrev] = ?EU:extract_optional_props(['sdp_l_base'], XOpts),
    AutoSdp = ?M_SDP:setmode(FrPrev, ?M_SDP:reverse_stream_mode(?M_SDP:get_audio_mode(ZRSdp))),
    z_add_term(Media, ZReq, AutoSdp, {ZTag,XTag}, ZIfaceAlias).

%% ====================================================================
%% Internal functions (X-side replaced)
%% ====================================================================

%% --------------------------------------
%% Deleting replaced x term from context
%%
x_delete_term(Media,XTag) ->
    #media{mgc=MGC,msid=MSID,mgid=MG,ctx=CtxId}=Media,
    case Media of
        #media{caller_tag=XTag,caller=XTerm,callee=_YTerm} -> ok;
        #media{callee_tag=XTag,callee=XTerm,caller=_YTerm} -> ok
    end,
    TermIds = ?MGC_TERM:get_terms_id([XTerm]),
    case ?MGC:mg_subtract_terms({MGC,MSID,MG,CtxId},TermIds) of
        {error,_}=Err -> Err;
        {ok, _} -> {ok, Media}
    end.

%% ====================================================================
%% Internal functions (Y-side referred operations)
%% ====================================================================

%% ---------------------------------------
%% We should make only one call to media. So make sdp by changes of previous sent. Last remote should be kept in local opts
%%
y_modify_term(Media,YTag,BaseSdp) ->
    case Media of
        #media{callee_tag=YTag,callee=YTerm,callee_opts=YOpts} -> F = fun(Med,Term,Opts) -> Med#media{callee=Term,callee_opts=Opts} end;
        #media{caller_tag=YTag,caller=YTerm,caller_opts=YOpts} -> F = fun(Med,Term,Opts) -> Med#media{caller=Term,caller_opts=Opts} end
    end,
    case ?MEDIA_SESSCHANGE:update_sesschange_sdp(Media, {BaseSdp,YOpts,YTerm}) of
        {error,_}=Err -> Err;
        {ok, YLocalSdp1, YOpts1} ->
            YLocalSdp2 = ?M_SDP:transcode_offer(Media#media.allow_transcoding, YLocalSdp1),
            YLocalSdp3 = ?M_SDP:set_sess(YLocalSdp2),
            YOpts2 = lists:keystore('sdp_l',1,YOpts1,{'sdp_l',YLocalSdp3}),
            {ok, F(Media,YTerm,YOpts2), YLocalSdp3}
    end.

%% -------------------------------------
%% Update y refering term remote
%%
y_update_by_response(Media, Resp) ->
    #sipmsg{to={_,YRTag}}=Resp,
    #sdp{}=YRSdp=?U:extract_sdp(Resp),
    #media{mgc=MGC,msid=MSID,mgid=MG,ctx=CtxId}=Media,
    case Media of
        #media{callee_tag=YRTag,callee=YTerm,callee_opts=YOpts} -> F = fun(Med,Term,Opts) -> Med#media{callee=Term,callee_opts=Opts} end;
        #media{caller_tag=YRTag,caller=YTerm,caller_opts=YOpts} -> F = fun(Med,Term,Opts) -> Med#media{caller=Term,caller_opts=Opts} end
    end,
    % modify z term by remote sdp
    {_,YLSdp} = lists:keyfind('sdp_l', 1, YOpts),
    YTerm1 = ?MGC_TERM:set_term_sdp(YTerm, ?D_SDP:prepare_descr_direct(YLSdp), ?D_SDP:prepare_descr_direct(YRSdp)),
    case ?MGC:mg_modify_terms({MGC,MSID,MG,CtxId},[YTerm1]) of
        {error,_}=Err -> Err;
        {ok, [YTerm2]} ->
            YOpts1 = lists:keystore('sdp_r',1,YOpts,{'sdp_r',YRSdp}),
            {ok, F(Media,YTerm2,YOpts1)}
    end.

%% ====================================================================
%% Internal functions (Z-side refering operations)
%% ====================================================================

%% --------------------------------------
%% Adding z term to context (y response sdp)
%%
z_add_term(Media,ZReq,YRSdp,{ZTag,XTag},ZIfaceAlias) ->
    #media{mgc=MGC,msid=MSID,mgid=MG,ctx=CtxId}=Media,
    case Media of
        #media{caller_tag=XTag,caller_opts=XOpts} -> ok;
        #media{callee_tag=XTag,callee_opts=XOpts} -> ok
    end,
    %
    #sipmsg{contacts=ZRContacts}=ZReq,
    #sdp{}=ZRSdp=?U:extract_sdp(ZReq),
    ZTermType = ?MGC_TERM:build_term_type(ZRContacts, ZRSdp),
    ZBaseSdp = ?SENDRECV(?M_SDP:transcode_offer(Media#media.allow_transcoding, YRSdp)),
    TermTemplate = ?MGC_TERM:prepare_term_template({ZIfaceAlias,ZTermType}, ZBaseSdp),
    TermTemplate1 = ?MGC_TERM:set_term_remotesdp(TermTemplate, ?D_SDP:prepare_descr_direct(ZRSdp)),
    case ?MGC:mg_add_terms({MGC,MSID,MG,CtxId},[TermTemplate1]) of % add z callee's term to mg
        {error,_}=Err -> Err;
        {ok, [ZTerm]} ->
            {_,ZLSdpO} = lists:keyfind('sdp_o', 1, XOpts), % base sdp session for invite
            ZLSdpO1 = erlang:setelement(3,ZLSdpO, erlang:element(3,ZLSdpO)+1),
            z_after_add({ZTerm, ZLSdpO1, ZRSdp, YRSdp, ZTermType, XTag, ZTag}, Media)
    end.

% @private ---
z_after_add({ZTerm, ZLSdpO, ZRSdp, ZBaseSdp, ZTermType, XTag, ZTag}, Media) ->
    case ?D_SDP:make_sdp(?MGC_TERM:get_term_localsdp(ZTerm), ZLSdpO) of
        #sdp{}=ZLSdp ->
            % push callee's local sdp to sip request
            ZLSdp1 = ?M_SDP:set_sess(ZLSdp),
            ZTermOpts = [{termtype,ZTermType},
                         {'sdp_r',ZRSdp},
                         {'sdp_o',ZLSdpO},
                         {'sdp_l_base',ZBaseSdp}],
            Media1 = case Media of
                #media{caller_tag=XTag}=M ->
                    M#media{caller=ZTerm,
                             caller_tag=ZTag,
                            caller_opts=ZTermOpts};
                #media{callee_tag=XTag}=M ->
                    M#media{callee=ZTerm,
                            callee_tag=ZTag,
                            callee_opts=ZTermOpts}
            end,
            {ok, Media1, ZLSdp1};
        _ ->
            {error, "MGC sdp error"}
    end.
