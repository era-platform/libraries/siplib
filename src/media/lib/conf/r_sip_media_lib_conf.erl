%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 30.10.2015
%%% @doc Provides media service functions to support CONF role
%%%      Incapsulates logic of interaction of server with Media Gateway Controller.
%%%      Service prepares Context Option, which then saved outside.
%%%      Then every event concerned to MGC-interaction is being passed to service with Context Option.

-module(r_sip_media_lib_conf).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([start_media/2,
         stop_media/1,
         check_media_mg/2,

         conf_attach_player/2,
         conf_attach_recorder/2,
         conf_detach_player/2,
         conf_detach_recorder/2,
         conf_modify_player/2,
         conf_modify_recorder/2,
         conf_find_player_id/2,
         conf_find_recorder_id/2,

         modify_topology/2,
         modify_context_props/2,

         attach_term/2,
         attach_term/3,
         detach_term/2,
         modify_term/2,
         modify_term/3,
         modify_term_mode/3,

         add_outgoing_term/2,
         add_outgoing_term/3,
         update_outgoing_term_by_remote/3,
         update_outgoing_term_by_remote/4]).

%-compile(export_all).

%% ================================================================================
%% Types
%% ================================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_mgc.hrl").

%% ================================================================================
%% API functions
%% ================================================================================

%% ==============================================================
%% Start / stop
%% ==============================================================

%% --------
%% Preparing media server to handle conference call
%%    MOpts could contain : rec::true|false
%%
start_media(MSID, MOpts) ->
    ?LOGMEDIA("-> start_media conf"),
    % mg opts (mgaddr, mgsfx)
    case maps:get(mg_opts, MOpts, undefined) of
        undefined -> MGOpts = [];
        MGOpts when is_list(MGOpts) -> ok
    end,
    % record
    case Rec=maps:get(rec, MOpts, true) of
        true ->
            {Timestamp,RecPath} = ?U:get_media_context_rec_opts(),
            Frec = fun(_MGC,MG,CtxId) -> [{MG,CtxId,Timestamp,RecPath}] end,
            CtxOpts = [{"timestamp",Timestamp},
                       {"recpath",RecPath}];
        false ->
            Frec = fun(_,_,_) -> [] end,
            CtxOpts = []
    end,
    % videoconference options. Screen schema in JSON.
    {IsVideo,VSS,CtxOpts1} = case {maps:get(video,MOpts,false), maps:get('videoScreenSchema',MOpts,undefined)}  of
                                 {true,VSS1} when is_map(VSS1)->
                                     {true,VSS1,[{"conf/video",true},
                                                 {"conf/video/schema",serialize_video_screen_schema(VSS1)}
                                                 |CtxOpts]};
                                 {_,_} -> {false, undefined, CtxOpts}
                             end,
    % freq
    CtxProp1 = [{'contextProp', [{"mode","conf"},
                                 {"conf/freq","8000"}
                                | CtxOpts1]}],
    CtxProp2 = CtxProp1,
    % term of player
    TP = 'play',
    TermPlayT = set_ivrterm(TP, prepare_ivrterm(TP), get_opts(TP,maps:get(play,MOpts))),
    % megaco
    case ?MGC:mg_create_context({MSID}, [TermPlayT], CtxProp2 ++ MGOpts) of
        {ok,{MGC,MG,CtxId,[TermPlay]}}
          when is_integer(CtxId), CtxId=/=0 ->
            Media = #media_conf{mgc=MGC,
                                msid=MSID,
                                mgid=MG,
                                ctx=CtxId,
                                is_rec=Rec,
                                rec_links=Frec(MGC,MG,CtxId),
                                is_video=IsVideo,
                                video_screen_schema=VSS,
                                terms=[],
                                players=[{'$init',TermPlay}]},
            ?MGC_PINGER:add({MGC,MSID,MG,CtxId},self()),
            ?LOGMEDIA("<- start_media: ok, ~p", [CtxId]),
            {ok, Media};
        {error, mg_all_busy} ->
            ?LOGMEDIA("<- start_media conf: Service Unavailable (Too Many Calls)"),
            {reply, ?ReplyOpts(503,"Too Many Calls","MG busy")};
        {error, mg_not_found} ->
            ?LOGMEDIA("<- start_media conf: Internal Server Error (MG connect error)"),
            {reply, ?InternalError("MG connect error")};
        {error, mgc_not_found} ->
            ?LOGMEDIA("<- start_media conf: Internal Server Error (MGC not found)"),
            {reply, ?InternalError("MGC not found")};
        {error, mgc_overload} ->
            ?LOGMEDIA("<- start_media: Internal Server Error (MGC overload)"),
            {reply, ?InternalError("MGC overload")};
        {error, mgc_connect_error} ->
            ?LOGMEDIA("<- start_media conf: Internal Server Error (MGC connect error)"),
            {reply, ?InternalError("MGC connect error")};
        {error, Reason} ->
            ?LOGMEDIA("<- start_media conf: Internal Server Error (~140p)", [Reason]),
            {reply, ?InternalError("MGC response error")};
        Res ->
            ?LOGMEDIA("<- start_media conf: Internal Server Error (MGC response error ~140p)", [Res]),
            {reply, ?InternalError("MGC invalid response")}
    end.

%% ----------------------------------------------
%% Stops media-context on call final
%%
-spec stop_media(Media:: undefined | #media_conf{}) -> ok.

stop_media(undefined) -> ok;
stop_media(Media) ->
    ?LOGMEDIA("-> stop_media conf"),
    #media_conf{mgc=MGC,msid=MSID,mgid=MG,ctx=CtxId}=Media,
    ?MGC_PINGER:del({MGC,MSID,MG,CtxId}),
    ?MGC:mg_delete_context({MGC,MSID,MG,CtxId}),
    ?LOGMEDIA("<- stop_media conf ~p", [CtxId]),
    ok.

%% ----------------------------------------------
%% Checks if media-context is linked to appropriate MG
check_media_mg(#media_conf{msid=_MSID, mgid=_MG}=_Media, [_MSID,_MG,_Mode]) -> true;
check_media_mg(_,_) -> false.

%% ==============================================================
%% Attach/detach IVR player/recorder
%% ==============================================================

%% attach player/recorder termination to context
conf_attach_player(Media, Opts) -> conf_attach_pr(play, Media, Opts).
conf_attach_recorder(Media, Opts) -> conf_attach_pr(rec, Media, Opts).
% @private
conf_attach_pr(T, Media, Opts) ->
    Media1 = conf_detach_pr_if_exists(T, Media, Opts),
    TId = get_id(T, Opts),
    TL = get_list(T, Media1),
    CtxLnk = get_ctxlnk(Media),
    IvrTT = set_ivrterm(T, prepare_ivrterm(T), get_opts(T, Opts)),
    case ?MGC:mg_add_terms(CtxLnk,[IvrTT]) of % add ivr term to mg
        {error,_}=Err -> Err;
        {ok, [IvrTerm]} ->
            Media2 = set_list(T, [{TId,IvrTerm}|TL], Media1),
            {ok,Media2}
    end.
% @private
conf_detach_pr_if_exists(T, Media, Opts) ->
    case conf_detach_pr(T, Media, Opts) of
        {error,_} -> Media;
        {ok,Media1} -> Media1
    end.

%% detach player/recorder termination to context
conf_detach_player(Media, Opts) -> conf_detach_pr(play, Media, Opts).
conf_detach_recorder(Media, Opts) -> conf_detach_pr(rec, Media, Opts).
% @private
conf_detach_pr(T, Media, Opts) ->
    TId = get_id(T, Opts),
    TL = get_list(T, Media),
    CtxLnk = get_ctxlnk(Media),
    case lists:keytake(TId,1,TL) of
        {value, {_,IvrTerm}, TL1} ->
            TermsId = ?MGC_TERM:get_term_id(IvrTerm),
            ?MGC:mg_subtract_terms(CtxLnk,[TermsId]),
            Media1 = set_list(T, TL1, Media),
            {ok,Media1};
        false ->
            {error,{not_found,<<"not found">>}}
    end.

%% modify player/recorder termination in context
conf_modify_player(Media, Opts) -> conf_modify_pr(play, Media, Opts).
conf_modify_recorder(Media, Opts) -> conf_modify_pr(rec, Media, Opts).
% @private
conf_modify_pr(T, Media, Opts) ->
    TId = get_id(T, Opts),
    TL = get_list(T, Media),
    CtxLnk = get_ctxlnk(Media),
    case lists:keytake(TId,1,TL) of
        false -> {error,{not_found,<<"not found">>}};
        {value,{_,IvrTerm},TL1} ->
            IvrTerm1 = set_ivrterm(T, IvrTerm, get_opts(T, Opts)),
            case ?MGC:mg_modify_terms(CtxLnk,[IvrTerm1]) of % add ivr term to mg
                {error,_}=Err -> Err;
                {ok, [IvrTerm2]} ->
                    Media1 = set_list(T,[{TId,IvrTerm2}|TL1],Media),
                    {ok,Media1}
            end
    end.

%% return player/recorder id by termination id
conf_find_player_id(TermId,Media) -> conf_find_pr(play, TermId, Media).
conf_find_recorder_id(TermId,Media) -> conf_find_pr(rec, TermId, Media).
% @private
conf_find_pr(T, TermId, Media) ->
    TL = get_list(T, Media),
    lists:foldl(fun({TId,IvrTerm}, undefined) ->
                        case ?MGC_TERM:get_term_id(IvrTerm) of
                            TermId -> {ok,TId};
                            _ -> undefined
                        end;
                   (_, Acc) -> Acc
                end, undefined, TL).

% ------------
% @private
get_ctxlnk(#media_conf{mgc=MGC,msid=MSID,mgid=MG,ctx=CtxId}) -> {MGC,MSID,MG,CtxId}.

% @private
get_id(play, Opts) -> maps:get(playerid,Opts,default);
get_id(rec, Opts) -> maps:get(recorderid,Opts,default).

% @private
get_list(play, Media) -> Media#media_conf.players;
get_list(rec, Media) -> Media#media_conf.recorders.

% @private
set_list(play, Players, Media) -> Media#media_conf{players=Players};
set_list(rec, Recorders, Media) -> Media#media_conf{recorders=Recorders}.

% @private
get_opts(play, Opts) -> maps:with([mode,file,files,dir,random,loop,startat,stopat,volume],Opts);
get_opts(rec, Opts) -> maps:with([mode,file],Opts).

% @private
prepare_ivrterm(play) -> ?MGC_TERM:prepare_term_template('ivrp');
prepare_ivrterm(rec) -> ?MGC_TERM:prepare_term_template('ivrr').

% @private
set_ivrterm(play, IvrTerm, Opts) -> ?MGC_TERM:set_term_ivr_play(IvrTerm, Opts);
set_ivrterm(rec, IvrTerm, Opts) -> ?MGC_TERM:set_term_ivr_rec(IvrTerm, Opts).

%% ----------------------------------------------
%% Modifies topology in context
%% ----------------------------------------------
-spec modify_topology(Media::#media_conf{}, Topology::list()) -> {ok, NewMedia::#media_conf{}} | {error, R::term()}.

modify_topology(Media, Topology) ->
    #media_conf{mgc=MGC, msid=MSID, mgid=MG, ctx=CtxId,
                terms=Terms}=Media,
    ?LOGMEDIA("-> modify_topology conf (ctx=~p, topology=~140p)", [CtxId, Topology]),
    Items = lists:map(fun({A,B,Dir}) ->
                              {_,#term_conf{term=TermA}} = lists:keyfind(A,1,Terms),
                              {_,#term_conf{term=TermB}} = lists:keyfind(B,1,Terms),
                              {?MGC_TERM:get_term_id(TermA), ?MGC_TERM:get_term_id(TermB), Dir}
                        end, Topology),
    Opts = [{'topology', Items}],
    case ?MGC:mg_modify_topology({MGC,MSID,MG,CtxId}, Opts) of
        {error,Reason}=Err ->
            ?LOGMEDIA("<- modify_topology conf: Error (reason=~140p)", [Reason]),
            Err;
        ok ->
            {ok, Media}
    end.

%% ----------------------------------------------
%% Modifies context props
%% ----------------------------------------------
-spec modify_context_props(Media::#media_conf{}, ContextProps::list()) -> {ok, NewMedia::#media_conf{}} | {error, R::term()}.

modify_context_props(Media, ContextProps) ->
    #media_conf{mgc=MGC, msid=MSID, mgid=MG, ctx=CtxId}=Media,
    ?LOGMEDIA("-> modify_context_props conf (ctx=~p, contextprops=~140p)", [CtxId, ContextProps]),
    % videoconference options. Screen schema in JSON. {'videoScreenSchema',Map}
    VSS = ?EU:get_by_key('videoScreenSchema',ContextProps,#{}),
    KeyValList = [{"conf/video/schema",serialize_video_screen_schema(VSS)}],
    Opts = [{'contextProp', KeyValList}],
    Media1 = Media#media_conf{video_screen_schema=VSS},
    case ?MGC:mg_modify_context_props({MGC,MSID,MG,CtxId}, Opts) of
        {error,Reason}=Err ->
            ?LOGMEDIA("<- modify_context_props conf: Error (reason=~140p)", [Reason]),
            Err;
        ok ->
            {ok, Media1}
    end.

%% @private
serialize_video_screen_schema(VSS) when is_map(VSS) ->
    % TODO: map -> json -> base64
    ?EU:to_base64_ex(jsx:encode(VSS)).

%% ----------------------------------------------
%% attach term by incoming invite
%% ----------------------------------------------
attach_term(Media, Req) ->
    attach_term(Media, Req, undefined).

attach_term(Media, Req, Mode) ->
    #media_conf{mgc=MGC, msid=MSID, mgid=MG, ctx=CtxId,
                terms=Terms}=Media,
    {XTermTemplate, #term_conf{sdp_o=XLSdpO, callid=XCallId}=TermRec} = prepare_in_term(Media, Req, #{}),
    ?LOGMEDIA("-> attach_term conf (ctx=~p, callid=~140p)", [CtxId, XCallId]),
    %
    case ?MGC:mg_add_terms({MGC,MSID,MG,CtxId},[set_mode(XTermTemplate,Mode)]) of % add new term to context
        {error,Reason}=Err ->
            ?LOGMEDIA("<- attach_term conf: Error (reason=~140p)", [Reason]),
            Err;
        {ok, [XTerm]} ->
            TermRec1 = TermRec#term_conf{term=XTerm,
                                         term_id=?MGC_TERM:get_term_id(XTerm)},
            Media1 = Media#media_conf{terms=[{XCallId, TermRec1}|Terms]},
            % take local caller's sdp
            case ?D_SDP:make_sdp(?MGC_TERM:get_term_localsdp(XTerm), XLSdpO) of
                #sdp{}=XLSdp ->
                    ?LOGMEDIA("<- attach_term conf: ok"),
                    {ok, Media1, ?M_SDP:set_sess(XLSdp)};
                _ ->
                    ?LOGMEDIA("<- attach_term conf: Error MGC out SDP"),
                    {error, "MGC response error"}
            end
    end.

%% ----------------------------------------------
%% detach term by callid
%% ----------------------------------------------
detach_term(Media, CallId) ->
    #media_conf{mgc=MGC, msid=MSID, mgid=MG, ctx=CtxId,
                terms=Terms}=Media,
    ?LOGMEDIA("-> detach_term conf (ctx=~p, callid=~140p)", [CtxId, CallId]),
    case lists:keyfind(CallId,1,Terms) of
        false -> {error, "Term not found by callid"};
        {_,#term_conf{term=Term}} ->
            case ?MGC:mg_subtract_terms({MGC,MSID,MG,CtxId},?MGC_TERM:get_terms_id([Term])) of % delete term from context
                {error,Reason}=Err ->
                    ?LOGMEDIA("<- detach_term conf: Error (reason=~140p)", [Reason]),
                    Err;
                {ok,_} ->
                    ?LOGMEDIA("<- detach_term conf: ok"),
                    Media1 = Media#media_conf{terms=lists:keydelete(CallId,1,Terms)},
                    {ok,Media1}
            end
    end.

%% ----------------------------------------------
%% modify session of term
%% ----------------------------------------------
modify_term(Media, #sipmsg{class={req,_}}=Req) ->
    modify_term(Media,Req,undefined).

% when reinvite
modify_term(Media, #sipmsg{class={req,_}}=Req, Mode) ->
    #media_conf{mgc=MGC, msid=MSID, mgid=MG, ctx=CtxId,
                terms=Terms}=Media,
    #sipmsg{call_id=CallId}=Req,
    #sdp{}=RSdp=?U:extract_sdp(Req),
    ?LOGMEDIA("-> modify_term conf (ctx=~p, callid=~140p)", [CtxId, CallId]),
    case lists:keyfind(CallId,1,Terms) of
        false -> {error, "Term not found by callid"};
        {_,TermRec} ->
            #term_conf{term=Term,
                       sdp_r=RSdpPrev,
                       sdp_o=SdpO}=TermRec,
            SdpO1 = erlang:setelement(3,SdpO, erlang:element(3,SdpO)+1),
            % update local sdp by (now_r, prev_r, prev_l)
            case ?MEDIA_SESSCHANGE:update_sesschange_sdp({MGC,MSID,MG,CtxId}, {RSdp,RSdpPrev,SdpO1,Term}) of
                {error,Reason}=Err ->
                    ?LOGMEDIA("<- modify_term conf: Error (reason=~140p)", [Reason]),
                    Err;
                {ok,LSdp} ->
                    % filter media formats
                    LSdp1 = ?M_SDP:filter_by_local_media_fmts(?M_SDP:reverse_stream_mode(LSdp), undefined),
                    LSdp2 = case ?M_SDP:get_audio_mode(LSdp1) of
                                <<"sendrecv">> -> LSdp1;
                                _ -> ?M_SDP:inactive(LSdp1) % #243
                            end,
                    Term1 = ?MGC_TERM:set_term_sdp(Term, ?D_SDP:prepare_descr_direct(LSdp2), ?D_SDP:prepare_descr_direct(RSdp)), % prepare term for mg
                    case ?MGC:mg_modify_terms({MGC,MSID,MG,CtxId},[set_mode(Term1, Mode)]) of % modify term in context
                        {error,Reason}=Err ->
                            ?LOGMEDIA("<- modify_term conf: Error (reason=~140p)", [Reason]),
                            Err;
                        {ok,[Term2]} ->
                            TermRec1 = TermRec#term_conf{term=Term2,
                                                         sdp_o=SdpO1,
                                                         sdp_r=RSdp,
                                                         sdp_l_base=LSdp2},
                            Media1 = Media#media_conf{terms=lists:keyreplace(CallId,1,Terms,{CallId,TermRec1})},
                            % take local caller's sdp
                            case ?D_SDP:make_sdp(?MGC_TERM:get_term_localsdp(Term2), SdpO1) of
                                #sdp{}=LSdp3 ->
                                    ?LOGMEDIA("<- modify_term conf: ok"),
                                    {ok, Media1, ?M_SDP:set_sess(LSdp3)};
                                _ ->
                                    ?LOGMEDIA("<- modify_term conf: Error MGC out SDP"),
                                    {error,"MGC response error"}
                            end end end end.

% when selector managing
modify_term_mode(Media, CallId, Mode) ->
    #media_conf{mgc=MGC, msid=MSID, mgid=MG, ctx=CtxId,
                terms=Terms}=Media,
    ?LOGMEDIA("-> modify_term_mode conf (ctx=~p, callid=~140p, Mode=~140p)", [CtxId, CallId, Mode]),
    case lists:keyfind(CallId,1,Terms) of
        false -> {error, "Term not found by callid"};
        {_,TermRec} ->
            #term_conf{term=Term}=TermRec,
            case ?MGC:mg_modify_terms({MGC,MSID,MG,CtxId},[set_mode(Term, Mode)]) of % delete term from context
                {error,Reason}=Err ->
                    ?LOGMEDIA("<- modify_term conf: Error (reason=~140p)", [Reason]),
                    Err;
                {ok,[_Term1]} ->
                    {ok, Media}
            end
    end.

%% ----------------------------------------------
%% create new term (local side)
%% ----------------------------------------------
add_outgoing_term(Media, CallId) ->
    add_outgoing_term(Media, CallId, undefined).

add_outgoing_term(Media, CallId, Mode) ->
    %{error, "Not implemented"}.
    #media_conf{mgc=MGC,msid=MSID,mgid=MG,ctx=CtxId}=Media,
    Addr = ?U:get_host_addr(),
    % create back sdp
    TermType = ?MGC_TERM:build_term_type(),
    SdpSessId = (10000 + erlang:phash2(CallId) rem 90000) * 2,
    LSdpO = {"-", SdpSessId, SdpSessId, "IN", "IP4", Addr},
    Sdp = ?M_SDP:make_offer_sdp(),
    LSdpBase = ?SENDRECV(?LO(Sdp)),
    TermTemplate = setup_video_props(
                        Media,
                        set_mode(?MGC_TERM:prepare_term_template(TermType, LSdpBase), Mode), % prepare term template for mg
                        {CallId,<<"unknown">>}),
    case ?MGC:mg_add_terms({MGC,MSID,MG,CtxId},[TermTemplate]) of % add z callee's term to mg
        {error,_}=Err -> ?OUT("Err: ~p, ~p", [CtxId, Err]), Err;
        {ok, [Term]} ->
            after_add(Media, {Term, LSdpO, LSdpBase, TermType, CallId})
    end.

% @private ---
after_add(Media, {Term, LSdpO, LSdpBase, TermType, CallId}) ->
    case ?D_SDP:make_sdp(?MGC_TERM:get_term_localsdp(Term), LSdpO) of
        #sdp{}=LSdp ->
            % push callee's local sdp to sip request
            LSdp1 = ?M_SDP:set_sess(LSdp),
            TermRec = #term_conf{dir='out',
                                 callid=CallId,
                                 term_id=?MGC_TERM:get_term_id(Term),
                                 term=Term,
                                 term_type=TermType,
                                 sdp_o=LSdpO,
                                 sdp_l_base=LSdpBase},
            #media_conf{terms=Terms}=Media,
            {ok, Media#media_conf{terms=[{CallId,TermRec}|Terms]}, LSdp1};
        _ ->
            {error, "MGC sdp error"}
    end.

%% ----------------------------------------------
%% modify existing term by remote
%% ----------------------------------------------
update_outgoing_term_by_remote(Media, CallId, RSdp) ->
    update_outgoing_term_by_remote(Media, CallId, RSdp, undefined).

update_outgoing_term_by_remote(Media, CallId, RSdp, Mode) ->
    %{error, "Not implemented"}.
    #media_conf{mgc=MGC,msid=MSID,mgid=MG,ctx=CtxId,
                   terms=Terms}=Media,
    case lists:keytake(CallId, 1, Terms) of
        false -> {error, "MGC invalid callid, term not found"};
        {value,{_,#term_conf{term=Term}=TermRec},Terms1} ->
            % modify z term by remote sdp
            Term1 = ?MGC_TERM:set_term_remotesdp(Term, ?D_SDP:prepare_descr_direct(RSdp)),
            case ?MGC:mg_modify_terms({MGC,MSID,MG,CtxId},[set_mode(Term1,Mode)]) of
                {error,_}=Err -> Err;
                {ok, [Term2]} ->
                    TermRec1 = TermRec#term_conf{term=Term2,
                                                 sdp_r=RSdp},
                    {ok, Media#media_conf{terms=[{CallId,TermRec1}|Terms1]}}
            end
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

prepare_in_term(Media, Req, _MOpts) ->
    #sipmsg{contacts=XRContacts,
            from={FromUri, FromTag},
            cseq=_CSeq,
            call_id=XCallId}=Req,
    #sdp{}=XRSdp=?U:extract_sdp(Req),
    Addr = ?U:get_host_addr(),
    % create back sdp
    Dir='in',
    XTermType = ?MGC_TERM:build_term_type(XRContacts,XRSdp), % handle error?
    SdpSessId = (10000 + erlang:phash2(XCallId) rem 90000) * 2,
    XLSdpO = {"-", SdpSessId, SdpSessId, "IN", "IP4", Addr},
    XLSdpBase = ?M_SDP:filter_by_local_media_fmts(?M_SDP:reverse_stream_mode(XRSdp), undefined),
    XTermTemplate = setup_video_props(
                        Media,
                        ?MGC_TERM:prepare_term_template(XTermType, XLSdpBase, XRSdp), % prepare term template for mg
                        {XCallId,element(3,FromUri)}),
    TermRec = #term_conf{dir=Dir,
                         callid=XCallId,
                         tag=FromTag,
                         term_type=XTermType,
                         sdp_r=XRSdp,
                         sdp_o= XLSdpO,
                         sdp_l_base=XLSdpBase},
    {XTermTemplate, TermRec}.

%% @private
set_mode(Term, undefined) -> Term;
set_mode(Term, Mode) -> ?MGC_TERM:set_term_mode(Term, Mode, false).

%% @private
setup_video_props(#media_conf{is_video = false},Term, _) -> Term;
setup_video_props(_Media, Term, {CallId,Name}) ->
    ?MGC_TERM:set_term_state_props(Term,[{"v-id",?EU:to_list(erlang:phash2(CallId))},
                                         {"v-title",?EU:to_unicode_list(Name)}]).