%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Anton Makarov <anton@mastermak.ru>, Peter Bukashin <tbotc@yandex.ru>
%%% @doc @todo Add description to r_sip_media_lib_gate.

-module(r_sip_media_lib_gate).
-author(['Anton Makarov <anton@mastermak.ru>','Peter Bukashin <tbotc@yandex.ru>']).

-export([fwd_start_media/1,
         fwd_invite_response_to_a/1,
         fwd_update_rex_to_x/1,
         stop_media/1]).

%% ================================================================================
%% Types
%% ================================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_mgc.hrl").

-record(gatemedia, {mgc,
                    msid,
                    mgid,
                    ctx,
                    start_opts,
                    term_type,
                    t1alias,
                    t2alias,
                    t1,
                    t2,
                    t1_opts,
                    t2_opts,
                    t1_prevsdp,
                    t2_prevsdp}).

%% ================================================================================
%% API functions
%% ================================================================================

%% ==============================================================
%% Start / stop
%% ==============================================================

-spec fwd_start_media({MSID::term(), ARSdp::#sdp{}, MGOpts::term(), Contacts::term(), XAlias::binary(), TermPos::atom(), T1Alias::binary(), T2Alias::binary()}) ->
          {ok, Media::#gatemedia{},SDP::#sdp{}} | {error, Reason::term()}.

fwd_start_media({MSID, ARSdp, MGOpts, Contacts, XAlias, TermPos,T1Alias,T2Alias}) ->
    % mg opts (mgaddr, mgsfx)
    % record = false
    % Transcoding = false
    BaseSdp = ARSdp,
    %
    TermType = ?MGC_TERM:build_term_type(Contacts,ARSdp), % handle error?
    BTermTemplates = [?MGC_TERM:prepare_term_template({XAlias,TermType}, BaseSdp)],
    case ?MGC:mg_create_context({MSID}, BTermTemplates, MGOpts) of
        {ok,{MGC,MG,CtxId,[BTerm]}=Res}
          when is_integer(CtxId) andalso CtxId=/=0 ->
            Addr = ?U:get_host_addr(),
            SdpSessId = (10000 + erlang:phash2(Res) rem 90000) * 2,
            BLSdpO = {"-", SdpSessId+1, SdpSessId+1, "IN", "IP4", Addr},
            PrepMedia = #gatemedia{mgc=MGC,
                           msid=MSID,
                           mgid=MG,
                           ctx=CtxId,
                           start_opts=MGOpts,
                           term_type=TermType},
            Media = save_offer_media(PrepMedia,TermPos,{BTerm,TermType,ARSdp,SdpSessId,Addr,BLSdpO,BaseSdp,T1Alias,T2Alias}),

            ?MGC_PINGER:add({MGC,MSID,MG,CtxId},self()),
            ?LOGMEDIA("<- start_media: ok, ~p", [CtxId]),
            Resp =
                case ?D_SDP:make_sdp(?MGC_TERM:get_term_localsdp(BTerm), BLSdpO) of
                    #sdp{}=BLSdp ->
                        % push tX's local sdp to sip request
                        {ok, Media, ?M_SDP:set_sess(BLSdp)};
                    _ ->
                        {error, "MGC sdp error"}
                end,
            Resp;
        {error,_}=Err -> Err
    end.

%% ====================================================================
% first response+sdp (from forked callee)

-spec fwd_invite_response_to_a({Media::#gatemedia{},BRSdp::#sdp{}, TermPos::atom()}) ->
          {ok, NewMedia::#gatemedia{},NewSDP::#sdp{}} | {error, Reason::term()}.

fwd_invite_response_to_a({#gatemedia{mgc=MGC, msid=MSID, mgid=MG, ctx=CtxId}=Media, BRSdp, TermPos}) ->
    % first invite response: caller term not created yet
        ?LOGMEDIA("   media_response_to_a in first invite response, create term A"),
        {AOpts,BOpts,BTerm, XAlias} = get_answer_media(Media,TermPos),
        [ATermType,ALSdpO,ARSdp] = ?EU:extract_required_props(['term_type','sdp_o','sdp_r'], AOpts), % take saved caller's term type (rtp,srtp,webrtc) and remote sdp,
        %% prepare_term_template 1 - new term params, 2 - local sdp based on remote, 3 - remote sdp
        ATermTemplate = ?MGC_TERM:prepare_term_template({XAlias, ATermType}, _ALSdp=BRSdp, ARSdp), % prepare term template for mg
        case ?MGC:mg_add_terms({MGC,MSID,MG,CtxId},[ATermTemplate]) of % add caller's term to mg
            {ok, [ATerm]} ->
                BTerm1 = ?MGC_TERM:set_term_remotesdp(BTerm, ?D_SDP:prepare_descr_direct(BRSdp)),
                case ?MGC:mg_modify_terms_ex({MGC,MSID,MG,CtxId},[{BTerm1,[remotesdp]}]) of
                    {error,_}=Err -> Err;
                    {ok, [BTerm2]} ->
                        AOpts1 = lists:keystore('sdp_l_base',1,AOpts,{'sdp_l_base',BRSdp}),
                        BOpts1 = lists:keystore('sdp_l_base',1,BOpts,{'sdp_l_base',ARSdp}),
                        BOpts2 = lists:keystore('sdp_r',1,BOpts1,{'sdp_r',BRSdp}),
                        %
                        Media1 = save_answer_media(Media,TermPos,ATerm,BTerm2,AOpts1,BOpts2,BRSdp),
                        case ?D_SDP:make_sdp(?MGC_TERM:get_term_localsdp(ATerm), ALSdpO) of
                            #sdp{}=ALSdp1 ->
                                % push t2's local sdp to sip request
                                {ok, Media1, ?M_SDP:set_sess(ALSdp1)};
                            _ ->
                                {error, "MGC sdp error"}
                        end end;
            {error,_}=Err -> Err
        end.

%%-----------------------------------------------------------------------------
%% Media save media
%%-----------------------------------------------------------------------------
get_answer_media(#gatemedia{t1alias=T1Alias,t2alias=T2Alias,t1=ATerm,t2=BTerm,t1_opts=T1Opts,t2_opts=T2Opts}=_Media,TermPos) ->
    case TermPos of
        t2a -> {T1Opts,T2Opts,BTerm,T1Alias};
        t1a -> {T2Opts,T1Opts,ATerm,T2Alias}
    end.

%--------
save_answer_media(Media,TermPos,ATerm,BTerm2,T1Opts1,T2Opts2,BRSdp) ->
    case TermPos of
        t2a ->
            Media#gatemedia{t1=ATerm,
                            t2=BTerm2, % list
                            t1_opts=T1Opts1,
                            t2_opts=T2Opts2,
                            t2_prevsdp=BRSdp};
        t1a ->
            Media#gatemedia{t2=ATerm,
                            t1=BTerm2, % list
                            t2_opts=T1Opts1,
                            t1_opts=T2Opts2,
                            t1_prevsdp=BRSdp}
    end.

%%-----------------------------------------------------------------------------
save_offer_media(Media,TermPos,{BTerm,TermType,ARSdp,SdpSessId,Addr,BLSdpO,BaseSdp,T1Alias,T2Alias}) ->
    case TermPos of
        t2 ->
            Media#gatemedia{t1alias=T1Alias,
                            t2alias=T2Alias,
                            t1=undefined,
                            t2=BTerm, % list
                            t1_opts=[{'term_type', TermType},
                                     {'sdp_r', ARSdp},
                                     {'sdp_o', {"-", SdpSessId, SdpSessId, "IN", "IP4", Addr}}],
                            t2_opts=[{'sdp_o', BLSdpO},
                                      {'sdp_l_base', BaseSdp}],
                            t1_prevsdp=ARSdp,
                            t2_prevsdp=undefined};
        t1 ->
            Media#gatemedia{t2alias=T1Alias,
                               t1alias=T2Alias,
                               t2=undefined,
                               t1=BTerm, % list
                               t2_opts=[{'term_type', TermType},
                                     {'sdp_r', ARSdp},
                                     {'sdp_o', {"-", SdpSessId, SdpSessId, "IN", "IP4", Addr}}],
                               t1_opts=[{'sdp_o', BLSdpO},
                                     {'sdp_l_base', BaseSdp}],
                               t2_prevsdp=ARSdp,
                               t1_prevsdp=undefined}
    end.

%%-----------------------------------------------------------------------------
-spec fwd_update_rex_to_x({Media::#gatemedia{},BRSdp::#sdp{}, TermPos::atom()}) ->
          {ok, NewMedia::#gatemedia{},NewSDP::#sdp{}} | {error, Reason::term()}.

fwd_update_rex_to_x({#gatemedia{mgc=MGC, msid=MSID, mgid=MG, ctx=CtxId}=Media, BRSdp, TermPos}) ->
    % first invite response: caller term already created
        ?LOGMEDIA("   media_response_to_a in first invite response, modify term A"),
        {AOpts,BOpts,ATerm,BTerm} = get_update_media(Media,TermPos),
    % modify terms in MG
        BTerm1 = ?MGC_TERM:set_term_remotesdp(BTerm, ?D_SDP:prepare_descr_direct(BRSdp)),
        ATerm1 = ?MGC_TERM:set_term_localsdp(ATerm, ?D_SDP:prepare_descr_template(_ALSdp=BRSdp)),
        case ?MGC:mg_modify_terms_ex({MGC,MSID,MG,CtxId},[{ATerm1,[localsdp]}, {BTerm1,[remotesdp]}]) of
            {error,_}=Err -> Err;
            {ok, [ATerm2, BTerm2]} ->
                [ALSdpO] = ?EU:extract_required_props(['sdp_o'], AOpts),
                ALSdpO1 = erlang:setelement(3,ALSdpO, erlang:element(3,ALSdpO)+1),
                AOpts1 = lists:keystore('sdp_l_base',1,AOpts,{'sdp_l_base',BRSdp}),
                AOpts2 = lists:keystore('sdp_o',1,AOpts1,{'sdp_o',ALSdpO1}),
                BOpts1 = lists:keystore('sdp_r',1,BOpts,{'sdp_r',BRSdp}),
                %
                Media1 = save_update_media(Media,TermPos,ATerm2,BTerm2,AOpts2,BOpts1,BRSdp),
                case ?D_SDP:make_sdp(?MGC_TERM:get_term_localsdp(ATerm2), ALSdpO1) of
                    #sdp{}=ALSdp1 ->
                    % push t2's local sdp to sip request
                        {ok, Media1, ?M_SDP:set_sess(ALSdp1)};
                    _ ->
                        {error, "MGC sdp error"}
                end end.

%%----------------------------
get_update_media(#gatemedia{t1=ATerm,t2=BTerm,t1_opts=T1Opts,t2_opts=T2Opts}=_Media,TermPos) ->
    case TermPos of
        t2 -> {T1Opts,T2Opts,ATerm,BTerm};
        t1 -> {T2Opts,T1Opts,BTerm,ATerm}
    end.

%%----------------------------
save_update_media(Media,TermPos,ATerm,BTerm,T1Opts,T2Opts,BRSdp) ->
    case TermPos of
        t2 ->
            Media#gatemedia{t1=ATerm,
                            t2=BTerm, % list
                            t1_opts=T1Opts,
                            t2_opts=T2Opts,
                            t2_prevsdp=BRSdp};
        t1 ->
            Media#gatemedia{t2=ATerm,
                            t1=BTerm, % list
                            t2_opts=T1Opts,
                            t1_opts=T2Opts,
                            t1_prevsdp=BRSdp}
    end.

%% ====================================================================
-spec stop_media(Media:: undefined | #gatemedia{}) -> ok.

stop_media(undefined) -> ok;
stop_media(#gatemedia{mgc=MGC,msid=MSID,mgid=MG,ctx=CtxId}=_Media) ->
    ?LOGMEDIA("-> stop_media"),
    ?MGC_PINGER:del({MGC,MSID,MG,CtxId}),
    ?MGC:mg_delete_context({MGC,MSID,MG,CtxId}),
    ?LOGMEDIA("<- stop_media ~p", [CtxId]),
    ok.

%% ====================================================================
%% Internal functions
%% ====================================================================
