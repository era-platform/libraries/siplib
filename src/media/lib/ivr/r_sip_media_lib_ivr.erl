%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.04.2016
%%% @doc Provides media service functions to support IVR role
%%%      Incapsulates logic of interaction of server with Media Gateway Controller.
%%%      Service prepares Context Option, which then saved outside.
%%%      Then every event concerned to MGC-interaction is being passed to service with Context Option.

-module(r_sip_media_lib_ivr).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([start_media/3,
         start_media/2,
         stop_media/1,
         check_media_mg/2,

         modify_term/2,
         modify_term/3,
         del_term/2,
         add_term/2,
    
         modify_topology/2,
         
         get_local_sdp/3,
         set_local_sdp/3,

         add_outgoing_term/2,
         update_outgoing_term_by_remote/3,
    
         ivr_attach_player/2,
         ivr_attach_recorder/2,
         ivr_detach_player/2,
         ivr_detach_recorder/2,
         ivr_modify_player/2,
         ivr_modify_recorder/2,
         ivr_find_player_id/2,
         ivr_find_recorder_id/2,
         ivr_check_no_playrec/1,

         ivr_attach_fax/2,
         ivr_detach_fax/2,
         ivr_check_fax_id/2,
     
         modify_term_events/3]).

%-compile(export_all).

%% ================================================================================
%% Types
%% ================================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_mgc.hrl").

%% ================================================================================
%% API functions
%% ================================================================================

%% --------
%% Preparing media server to handle conference call
%%    MOpts could contain : rec::true|false
%%
start_media(MSID, MOpts) ->
    ?LOGMEDIA("-> start_media conf (for ivr)"),
    % mg opts (mgaddr, mgsfx)
    case maps:get(mg_opts, MOpts, undefined) of
        undefined -> MGOpts = [];
        MGOpts when is_list(MGOpts) -> ok
    end,
    % freq
    CtxOpts1 = [{noping_alive_time, 30}, % #304 ivr hard link to mgc/mg
                {'contextProp', [%{"mode","conf"},
                                 {"conf/freq","8000"}
                                 | []]}],
    CtxOpts2 = CtxOpts1,
    % term of player
    TP = 'play',
    TermPlayT = set_ivrterm(TP, prepare_ivrterm(TP), get_opts(TP,maps:get(play,MOpts))),
    % megaco
    case ?MGC:mg_create_context({MSID}, [TermPlayT], CtxOpts2 ++ MGOpts) of
        {ok,{MGC,MG,CtxId,[TermPlay]}}
          when is_integer(CtxId), CtxId=/=0 ->
            Media = #media_ivr{mgc=MGC,
                               msid=MSID,
                               mgid=MG,
                               ctx=CtxId,
                               terms=[],
                               players=[{'$init',TermPlay}]},
            ?MGC_PINGER:add({MGC,MSID,MG,CtxId},self()),
            ?LOGMEDIA("<- start_media: ok, ~p", [CtxId]),
            {ok, Media};
        {error, mg_all_busy} ->
            ?LOGMEDIA("<- start_media conf: Service Unavailable (Too Many Calls)"),
            {reply, ?ReplyOpts(503,"Too Many Calls","MG busy")};
        {error, mg_not_found} ->
            ?LOGMEDIA("<- start_media conf: Internal Server Error (MG connect error)"),
            {reply, ?InternalError("MG connect error")};
        {error, mgc_not_found} ->
            ?LOGMEDIA("<- start_media conf: Internal Server Error (MGC not found)"),
            {reply, ?InternalError("MGC not found")};
        {error, mgc_overload} ->
            ?LOGMEDIA("<- start_media: Internal Server Error (MGC overload)"),
            {reply, ?InternalError("MGC overload")};
        {error, mgc_connect_error} ->
            ?LOGMEDIA("<- start_media conf: Internal Server Error (MGC connect error)"),
            {reply, ?InternalError("MGC connect_error")};
        {error, Reason} ->
            ?LOGMEDIA("<- start_media conf: Internal Server Error (~140p)", [Reason]),
            {reply, ?InternalError("MGC response error")};
        Res ->
            ?LOGMEDIA("<- start_media conf: Internal Server Error (MGC response error ~140p)", [Res]),
            {reply, ?InternalError("MGC invalid response")}
    end.

%% --------
%% Preparing media server to handle migration of active call
%%    MOpts could contain : rec::true|false
%%
start_media(MSID, Req, MOpts) ->
    {XTermTemplate, #term_ivr{sdp_o=XLSdpO, callid=XCallId}=TermRec} = prepare_in_term(Req, MOpts),
    ?LOGMEDIA("-> start_media ivr (callid=~140p)", [XCallId]),
    % mg opts (mgaddr, mgsfx)
    case maps:get(mg_opts, MOpts, undefined) of
        undefined -> MGOpts = [];
        MGOpts when is_list(MGOpts) -> ok
    end,
    %% @todo @ivr @conf
    CtxOpts = [{noping_alive_time, 30}, % #304 ivr hard link to mgc/mg
               {'contextProp', [{"mode","conf"},
                                {"conf/freq","8000"}]}],
    %%CtxOpts = [{noping_alive_time, 30}, % #304 ivr hard link to mgc/mg
    %%          {'contextProp', []}],
    % megaco
    case ?MGC:mg_create_context({MSID}, [XTermTemplate], CtxOpts ++ MGOpts) of
        {ok,{MGC,MG,CtxId,[XTerm]}}
          when is_integer(CtxId), CtxId=/=0 ->
            TermRec1 = TermRec#term_ivr{term=XTerm,
                                        term_id=?MGC_TERM:get_term_id(XTerm)},
            Media = #media_ivr{mgc=MGC,
                               msid=MSID,
                               mgid=MG,
                               ctx=CtxId,
                               terms=[{XCallId,TermRec1}]},
            % take local caller's sdp
            case ?D_SDP:make_sdp(?MGC_TERM:get_term_localsdp(XTerm), XLSdpO) of
                #sdp{}=XLSdp ->
                    ?MGC_PINGER:add({MGC,MSID,MG,CtxId},self()),
                    ?LOGMEDIA("<- start_media ivr: ok, ~p", [CtxId]),
                    {ok, Media, ?M_SDP:set_sess(XLSdp)};
                _ ->
                    ?LOGMEDIA("<- start_media ivr: Internal Server Error (MGC out SDP error)"),
                    {reply, ?InternalError("MGC response error")}
            end;
        {error, mg_all_busy} ->
            ?LOGMEDIA("<- start_media ivr: Service Unavailable (Too Many Calls)"),
            {reply, ?ReplyOpts(503,"Too Many Calls","MG busy")};
        {error, mg_not_found} ->
            ?LOGMEDIA("<- start_media ivr: Internal Server Error (MG connect error)"),
            {reply, ?InternalError("MG connect error")};
        {error, mgc_not_found} ->
            ?LOGMEDIA("<- start_media ivr: Internal Server Error (MGC not found)"),
            {reply, ?InternalError("MGC not found")};
        {error, mgc_overload} ->
            ?LOGMEDIA("<- start_media: Internal Server Error (MGC overload)"),
            {reply, ?InternalError("MGC overload")};
        {error, mgc_connect_error} ->
            ?LOGMEDIA("<- start_media ivr: Internal Server Error (MGC connect error)"),
            {reply, ?InternalError("MGC connect error")};
        {error, Reason} ->
            ?LOGMEDIA("<- start_media ivr: Internal Server Error (~140p)", [Reason]),
            {reply, ?InternalError("MGC response error")};
        Res ->
            ?LOGMEDIA("<- start_media ivr: Internal Server Error (MGC response error ~140p)", [Res]),
            {reply, ?InternalError("MGC invalid response")}
    end.


%% ----------------------------------------------
%% Stops media-context on call final
%%
-spec stop_media(Media:: undefined | #media_ivr{}) -> ok.

stop_media(undefined) -> ok;
stop_media(Media) ->
    ?LOGMEDIA("-> stop_media ivr"),
    #media_ivr{mgc=MGC,msid=MSID,mgid=MG,ctx=CtxId}=Media,
    ?MGC_PINGER:del({MGC,MSID,MG,CtxId}),
    ?MGC:mg_delete_context({MGC,MSID,MG,CtxId}),
    ?LOGMEDIA("<- stop_media ivr ~p", [CtxId]),
    ok.

%% ----------------------------------------------
%% Checks if media-context is linked to appropriate MG
%% ----------------------------------------------
check_media_mg(#media_ivr{msid=_MSID, mgid=_MG}=_Media, [_MSID,_MG,_Mode]) -> true;
check_media_mg(_,_) -> false.

%% ----------------------------------------------
%% When incoming reinvite or second response should be sent
%% ----------------------------------------------
modify_term(Media, #sipmsg{class={req,_},call_id=CallId}=Req) ->
    #media_ivr{mgc=MGC, msid=MSID, mgid=MG, ctx=CtxId,
               terms=Terms}=Media,
    ?LOGMEDIA("-> modify_term ivr (ctx=~p, callid=~140p)", [CtxId, CallId]),
    case lists:keytake(CallId,1,Terms) of
        false ->
            ?LOGMEDIA("<- modify_term ivr: Error (callid not found)"),
            {error,<<"MGC error. CallId not found">>};
        {value,{_,TermRec},Terms1} ->
            #sdp{}=RSdp=?U:extract_sdp(Req),
            #term_ivr{term=Term,
                      sdp_r=RSdpPrev,
                      sdp_o=SdpO}=TermRec,
            SdpO1 = erlang:setelement(3,SdpO, erlang:element(3,SdpO)+1),
            % update local sdp by (now_r, prev_r, prev_l)
            case ?MEDIA_SESSCHANGE:update_sesschange_sdp({MGC,MSID,MG,CtxId}, {RSdp,RSdpPrev,SdpO1,Term}) of
                {error,Reason}=Err ->
                    ?LOGMEDIA("<- modify_term ivr: Error (reason=~140p)", [Reason]),
                    Err;
                {ok,LSdp} ->
                    % filter media formats
                    LSdp1 = ?M_SDP:filter_by_local_media_fmts(?M_SDP:reverse_stream_mode(LSdp), undefined),
                    Term1 = ?MGC_TERM:set_term_sdp(Term, ?D_SDP:prepare_descr_direct(LSdp1), ?D_SDP:prepare_descr_direct(RSdp)), % prepare term for mg
                    case ?MGC:mg_modify_terms({MGC,MSID,MG,CtxId},[Term1]) of % modify term in context
                        {error,Reason}=Err ->
                            ?LOGMEDIA("<- modify_term ivr: Error (reason=~140p)", [Reason]),
                            Err;
                        {ok,[Term2]} ->
                            TermRec1 = TermRec#term_ivr{term=Term2,
                                                        sdp_o=SdpO1,
                                                        sdp_r=RSdp,
                                                        sdp_l_base=LSdp1},
                            Media1 = Media#media_ivr{terms=[{CallId,TermRec1}|Terms1]},
                            % take local caller's sdp
                            case ?D_SDP:make_sdp(?MGC_TERM:get_term_localsdp(Term2), SdpO1) of
                                #sdp{}=LSdp2 ->
                                    ?LOGMEDIA("<- modify_term ivr: ok"),
                                    {ok, Media1, ?M_SDP:set_sess(LSdp2)};
                                _ ->
                                    ?LOGMEDIA("<- modify_term ivr: Error MGC out SDP)"),
                                    {error,"MGC response error"}
                            end end end end.

%% ----------------------------------------------
%% When outgoing reinvite was responsed
%% ----------------------------------------------

% returns last local sdp (could be updated by next session id in o=)
get_local_sdp(Media, CallId, SetNextO) ->
    #media_ivr{terms=Terms}=Media,
    Fmakesdp = fun(FMedia,Term,SdpO) ->
                       case ?D_SDP:make_sdp(?MGC_TERM:get_term_localsdp(Term), SdpO) of
                           #sdp{}=LSdp -> {ok, FMedia, LSdp};
                           _ -> {error,"MGC response error"}
                       end end,
    case lists:keyfind(CallId,1,Terms) of
        false ->
            ?LOGMEDIA("<- modify_term ivr: Error (callid not found)"),
            {error,<<"MGC error. CallId not found">>};
        {_,#term_ivr{term=Term, sdp_o=SdpO}=TermIvr} when SetNextO==true ->
            SdpO1 = erlang:setelement(3,SdpO, erlang:element(3,SdpO)+1),
            Media1 = Media#media_ivr{terms=lists:keyreplace(CallId,1,Terms,{CallId,TermIvr#term_ivr{sdp_o=SdpO1}})},
            Fmakesdp(Media1, Term, SdpO);
        {_,#term_ivr{term=Term, sdp_o=SdpO}} ->
            Fmakesdp(Media, Term, SdpO)
    end.

% apply updated local sdp to term (create new stream, change codec)
set_local_sdp(Media, CallId, LSdp) ->
    #media_ivr{mgc=MGC, msid=MSID, mgid=MG, ctx=CtxId,
               terms=Terms}=Media,
    Fapply = fun(FMedia,#term_ivr{term=Term,sdp_o=SdpO}=TermRec) ->
                     Term1 = ?MGC_TERM:set_term_localsdp(Term, ?D_SDP:prepare_descr_template(LSdp)), % prepare term for mg
                     case ?MGC:mg_modify_terms_ex({MGC,MSID,MG,CtxId},[{Term1,[localsdp]}]) of % modify term in context
                         {error,Reason}=Err ->
                             ?LOGMEDIA("<- modify_term ivr: Error (reason=~140p)", [Reason]),
                             Err;
                         {ok,[Term2]} ->
                             TermRec1 = TermRec#term_ivr{term=Term2,
                                                         sdp_l_base=LSdp},
                             Media1 = FMedia#media_ivr{terms=lists:keyreplace(CallId,1,Terms,{CallId,TermRec1})},
                             % take local caller's sdp
                             case ?D_SDP:make_sdp(?MGC_TERM:get_term_localsdp(Term2), SdpO) of
                                 #sdp{}=LSdp2 ->
                                     ?LOGMEDIA("<- modify_term ivr: ok"),
                                     {ok, Media1, ?M_SDP:set_sess(LSdp2)};
                                 _ ->
                                     ?LOGMEDIA("<- modify_term ivr: Error MGC out SDP)"),
                                     {error,"MGC response error"}
                             end end end,
    case lists:keyfind(CallId,1,Terms) of
        false ->
            ?LOGMEDIA("<- modify_term ivr: Error (callid not found)"),
            {error,<<"MGC error. CallId not found">>};
        {_,#term_ivr{sdp_o=SdpO}=TermRec} ->
            SdpO1 = erlang:setelement(3,SdpO, erlang:element(3,SdpO)+1),
            TermRec1 = TermRec#term_ivr{sdp_o=SdpO1},
            Media1 = Media#media_ivr{terms=lists:keyreplace(CallId,1,Terms,{CallId,TermRec1})},
            Fapply(Media1, TermRec1)
    end.

% after response got
modify_term(Media, {RSdp, LSdp}, CallId) ->
    #media_ivr{mgc=MGC, msid=MSID, mgid=MG, ctx=CtxId,
               terms=Terms}=Media,
    ?LOGMEDIA("-> modify_term ivr (ctx=~p, callid=~140p)", [CtxId, CallId]),
    case lists:keytake(CallId,1,Terms) of
        false ->
            ?LOGMEDIA("<- modify_term ivr: Error (callid not found)"),
            {error,<<"MGC error. CallId not found">>};
        {value,{_,TermRec},Terms1} ->
            #term_ivr{term=Term}=TermRec,
            % update local sdp by (now_r, prev_r, prev_l)
            Term1 = ?MGC_TERM:set_term_sdp(Term, ?D_SDP:prepare_descr_direct(LSdp), ?D_SDP:prepare_descr_direct(RSdp)), % prepare term for mg
            case ?MGC:mg_modify_terms({MGC,MSID,MG,CtxId},[Term1]) of % modify term in context
                {error,Reason}=Err ->
                    ?LOGMEDIA("<- modify_term ivr: Error (reason=~140p)", [Reason]),
                    Err;
                {ok,[Term2]} ->
                    TermRec1 = TermRec#term_ivr{term=Term2,
                                                sdp_r=RSdp,
                                                sdp_l_base=LSdp},
                    Media1 = Media#media_ivr{terms=[{CallId,TermRec1}|Terms1]},
                    {ok, Media1}
            end end.

%% ----------------------------------------------
%% When replacing, previous term should be deleted
%% ----------------------------------------------
del_term(Media, CallId) ->
    #media_ivr{mgc=MGC, msid=MSID, mgid=MG, ctx=CtxId,
               terms=Terms}=Media,
    ?LOGMEDIA("-> del_term ivr (ctx=~p)", [CtxId]),
    case lists:keytake(CallId,1,Terms) of
        false ->
            ?LOGMEDIA("<- del_term ivr: Error (callid not found)"),
            {error,<<"MGC error. CallId not found">>};
        {value,{_,#term_ivr{term=Term}},Terms1} ->
            TermIds = ?MGC_TERM:get_terms_id([Term]),
            case ?MGC:mg_subtract_terms({MGC,MSID,MG,CtxId},TermIds) of
                {error,Reason} ->
                    ?LOGMEDIA("<-  del_term ivr: Internal Server Error (~140p)", [Reason]),
                    {error, ?InternalError("MGC error on remove")};
                {ok, _} ->
                    ?LOGMEDIA("<- del_term ivr (ctx=~p)", [CtxId]),
                    {ok, Media#media_ivr{terms=Terms1}}
            end end.

%% ----------------------------------------------
%% When replacing, new term should be added
%% ----------------------------------------------
add_term(Media, Req) ->
    {XTermTemplate, #term_ivr{sdp_o=XLSdpO, callid=XCallId}=TermRec} = prepare_in_term(Req, #{}),
    #media_ivr{mgc=MGC, msid=MSID, mgid=MG, ctx=CtxId,
               terms=Terms}=Media,
    ?LOGMEDIA("-> add_term ivr (ctx=~p, callid=~140p)", [CtxId, XCallId]),
    case ?MGC:mg_add_terms({MGC,MSID,MG,CtxId},[XTermTemplate]) of % add x callee's term to mg
        {error,Reason} ->
            ?LOGMEDIA("<-  add_term ivr: Internal Server Error (~140p)", [Reason]),
            {error, ?InternalError("MGC error on add")};
        {ok, [XTerm]} ->
            % take local caller's sdp
            case ?D_SDP:make_sdp(?MGC_TERM:get_term_localsdp(XTerm), XLSdpO) of
                #sdp{}=XLSdp ->
                    ?LOGMEDIA("<- add_term ivr: ok, ~p", [CtxId]),
                    TermRec1 = TermRec#term_ivr{term=XTerm,
                                                term_id=?MGC_TERM:get_term_id(XTerm)},
                    Media1 = Media#media_ivr{terms=[{XCallId,TermRec1}|Terms]},
                    {ok, Media1, ?M_SDP:set_sess(XLSdp)};
                _ ->
                    ?LOGMEDIA("<- add_term ivr: Internal Server Error (MGC out SDP error)"),
                    {error, ?InternalError("MGC error on sdp")}
            end
    end.

%% ----------------------------------------------
%% create new term (local side)
%% ----------------------------------------------
add_outgoing_term(Media, CallId) ->
    %{error, "Not implemented"}.
    #media_ivr{mgc=MGC,msid=MSID,mgid=MG,ctx=CtxId}=Media,
    Addr = ?U:get_host_addr(),
    % create back sdp
    TermType = ?MGC_TERM:build_term_type(),
    SdpSessId = (10000 + erlang:phash2(CallId) rem 90000) * 2,
    LSdpO = {"-", SdpSessId, SdpSessId, "IN", "IP4", Addr},
    Sdp = ?M_SDP:make_offer_sdp(),
    LSdpBase = ?SENDRECV(?LO(Sdp)),
    TermTemplate = ?MGC_TERM:prepare_term_template(TermType, LSdpBase), % prepare term template for mg
    case ?MGC:mg_add_terms({MGC,MSID,MG,CtxId},[TermTemplate]) of % add z callee's term to mg
        {error,_}=Err -> Err;
        {ok, [Term]} ->
            after_add(Media, {Term, LSdpO, LSdpBase, TermType, CallId})
    end.

% @private ---
after_add(Media, {Term, LSdpO, LSdpBase, TermType, CallId}) ->
    case ?D_SDP:make_sdp(?MGC_TERM:get_term_localsdp(Term), LSdpO) of
        #sdp{}=LSdp ->
            % push callee's local sdp to sip request
            LSdp1 = ?M_SDP:set_sess(LSdp),
            TermRec = #term_ivr{dir='out',
                                callid=CallId,
                                term=Term,
                                term_type=TermType,
                                sdp_o=LSdpO,
                                sdp_l_base=LSdpBase},
            #media_ivr{terms=Terms}=Media,
            Media1 = Media#media_ivr{terms=[{CallId,TermRec}|Terms]},
            % delete initial player
            Opts = #{playerid => '$init'},
            case ivr_detach_player(Media1,Opts) of
                {ok,Media2} -> {ok, Media2, LSdp1};
                _ -> {ok,Media1,LSdp1}
            end;
        _ ->
            {error, "MGC sdp error"}
    end.

%% ----------------------------------------------
%% modify existing term by remote
%% ----------------------------------------------
update_outgoing_term_by_remote(Media, CallId, RSdp) ->
    %{error, "Not implemented"}.
    #media_ivr{mgc=MGC,msid=MSID,mgid=MG,ctx=CtxId,
               terms=Terms}=Media,
    case lists:keytake(CallId, 1, Terms) of
        false -> {error, "MGC invalid callid, term not found"};
        {value,{_,#term_ivr{term=Term}=TermRec},Terms1} ->
            % modify z term by remote sdp
            Term1 = ?MGC_TERM:set_term_remotesdp(Term, ?D_SDP:prepare_descr_direct(RSdp)),
            case ?MGC:mg_modify_terms({MGC,MSID,MG,CtxId},[Term1]) of
                {error,_}=Err -> Err;
                {ok, [Term2]} ->
                    TermRec1 = TermRec#term_ivr{term=Term2,
                                                sdp_r=RSdp},
                    {ok, Media#media_ivr{terms=[{CallId,TermRec1}|Terms1]}}
            end
    end.

%% ----------------------------------------------
%% Modifies topology in context (by callid and player/recorder layer-identificators
%% ----------------------------------------------
-spec modify_topology(Media::#media_ivr{}, Topology::list()) -> {ok, NewMedia::#media_ivr{}} | {error, R::term()}.
%% ----------------------------------------------
modify_topology(Media, Topology) ->
    #media_ivr{mgc=MGC, msid=MSID, mgid=MG, ctx=CtxId,
               terms=Terms,players=Ps,recorders=Rs}=Media,
    ?LOGMEDIA("-> modify_topology ivr (ctx=~p, topology=~140p)", [CtxId, Topology]),
    Items = lists:filtermap(fun({A,B,Dir}) ->
                                      FunFind = fun(X) ->
                                                    case ?EU:get_by_key(X,Terms,undefined) of
                                                        #term_ivr{term=TermX} -> TermX;
                                                        _ ->
                                                            case ?EU:get_by_key(X,Ps,undefined) of
                                                                undefined -> ?EU:get_by_key(X,Rs,undefined);
                                                                TermX -> TermX
                                                            end end end,
                                      case {FunFind(A),FunFind(B)} of
                                          {TermA,TermB} when TermA/=undefined,TermB/=undefined ->
                                              Item = {?MGC_TERM:get_term_id(TermA), ?MGC_TERM:get_term_id(TermB), Dir},
                                              {true,Item};
                                          _ -> false
                                      end end, Topology),
    Opts = [{topology, Items}],
    case ?MGC:mg_modify_topology({MGC,MSID,MG,CtxId}, Opts) of
        {error,Reason}=Err ->
            ?LOGMEDIA("<- modify_topology ivr: Error (reason=~140p)", [Reason]),
            Err;
        ok ->
            {ok, Media}
    end.

%% ==============================================================
%% Attach/detach IVR player/recorder
%% ==============================================================

%% attach player/recorder termination to context
ivr_attach_player(Media, Opts) -> ivr_attach_pr(play, Media, Opts).
ivr_attach_recorder(Media, Opts) -> ivr_attach_pr(rec, Media, Opts).
% @private
ivr_attach_pr(T, Media, Opts) ->
    Media1 = ivr_detach_pr_if_exists(T, Media, Opts),
    TId = get_id(T, Opts),
    TL = get_list(T, Media1),
    CtxLnk = get_ctxlnk(Media),
    IvrTT = set_ivrterm(T, prepare_ivrterm(T), get_opts(T, Opts)),
    case ?MGC:mg_add_terms(CtxLnk,[IvrTT]) of % add ivr term to mg
        {error,_}=Err -> Err;
        {ok, [IvrTerm]} ->
            Media2 = set_list(T, [{TId,IvrTerm}|TL], Media1),
            {ok,Media2}
    end.
% @private
ivr_detach_pr_if_exists(T, Media, Opts) ->
    case ivr_detach_pr(T, Media, Opts) of
        {error,_} -> Media;
        {ok,Media1} -> Media1
    end.

%% detach player/recorder termination to context
ivr_detach_player(Media, Opts) -> ivr_detach_pr(play, Media, Opts).
ivr_detach_recorder(Media, Opts) -> ivr_detach_pr(rec, Media, Opts).
% @private
ivr_detach_pr(T, Media, Opts) ->
    TId = get_id(T, Opts),
    TL = get_list(T, Media),
    CtxLnk = get_ctxlnk(Media),
    case lists:keytake(TId,1,TL) of
        {value, {_,IvrTerm}, TL1} ->
            TermsId = ?MGC_TERM:get_term_id(IvrTerm),
            ?MGC:mg_subtract_terms(CtxLnk,[TermsId]),
            Media1 = set_list(T, TL1, Media),
            {ok,Media1};
        false ->
            {error,{not_found,<<"not found">>}}
    end.

%% modify player/recorder termination in context
ivr_modify_player(Media, Opts) -> ivr_modify_pr(play, Media, Opts).
ivr_modify_recorder(Media, Opts) -> ivr_modify_pr(rec, Media, Opts).
% @private
ivr_modify_pr(T, Media, Opts) ->
    TId = get_id(T, Opts),
    TL = get_list(T, Media),
    CtxLnk = get_ctxlnk(Media),
    case lists:keytake(TId,1,TL) of
        false -> {error,{not_found,<<"not found">>}};
        {value,{_,IvrTerm},TL1} ->
            IvrTerm1 = set_ivrterm(T, IvrTerm, get_opts(T, Opts)),
            case ?MGC:mg_modify_terms(CtxLnk,[IvrTerm1]) of % add ivr term to mg
                {error,_}=Err -> Err;
                {ok, [IvrTerm2]} ->
                    Media1 = set_list(T,[{TId,IvrTerm2}|TL1],Media),
                    {ok,Media1}
            end
    end.

%% return player/recorder id by termination id
ivr_find_player_id(TermId,Media) -> ivr_find_pr(play, TermId, Media).
ivr_find_recorder_id(TermId,Media) -> ivr_find_pr(rec, TermId, Media).
% @private
ivr_find_pr(T, TermId, Media) ->
    TL = get_list(T, Media),
    lists:foldl(fun({TId,IvrTerm}, undefined) ->
                        case ?MGC_TERM:get_term_id(IvrTerm) of
                            TermId -> {ok,TId};
                            _ -> undefined
                        end;
                   (_, Acc) -> Acc
                end, undefined, TL).

%% return true if no players/recorders exist in context
ivr_check_no_playrec(#media_ivr{players=[],recorders=[]}) -> true;
ivr_check_no_playrec(#media_ivr{}) -> false.

% ------------
%% @private
get_ctxlnk(#media_ivr{mgc=MGC,msid=MSID,mgid=MG,ctx=CtxId}) -> {MGC,MSID,MG,CtxId}.

%% @private
get_id(play, Opts) -> maps:get(playerid,Opts,default);
get_id(rec, Opts) -> maps:get(recorderid,Opts,default).

%% @private
get_list(play, Media) -> Media#media_ivr.players;
get_list(rec, Media) -> Media#media_ivr.recorders.

%% @private
set_list(play, Players, Media) -> Media#media_ivr{players=Players};
set_list(rec, Recorders, Media) -> Media#media_ivr{recorders=Recorders}.

%% @private
get_opts(play, Opts) -> maps:with([mode,file,files,dir,random,loop,startat,stopat,volume],Opts);
get_opts(rec, Opts) -> maps:with([file,codec_name,type,buffer_duration],Opts).

%% @private
prepare_ivrterm(play) -> ?MGC_TERM:prepare_term_template('ivrp');
prepare_ivrterm(rec) -> ?MGC_TERM:prepare_term_template('ivrr').

%% @private
set_ivrterm(play, IvrTerm, Opts) -> ?MGC_TERM:set_term_ivr_play(IvrTerm, Opts);
set_ivrterm(rec, IvrTerm, Opts) ->
    ?MGC_TERM:set_term_ivr_rec(IvrTerm, Opts).

%% ==============================================================
%% Attach/detach IVR fax transmitter/receiver
%% ==============================================================

%% attach fax termination to context
ivr_attach_fax(Media, Opts) ->
    Media1 = ivr_detach_fax_if_exists(Media, Opts),
    #media_ivr{mgc=MGC,msid=MSID,mgid=MG,ctx=CtxId}=Media1,
    T = case maps:get(mode,Opts) of
            "transmit" -> 't';
            "receive" -> 'r';
            A when A=='t'; A=='r' -> A
        end,
    FaxTT = set_faxterm(T, prepare_faxterm(T,Opts), get_fax_opts(T, Opts)),
    case ?MGC:mg_add_terms({MGC,MSID,MG,CtxId},[FaxTT]) of % add ivr term to mg
        {error,_}=Err -> Err;
        {ok, [FaxTerm]} ->
            Media2 = Media1#media_ivr{fax=FaxTerm},
            {ok,Media2}
    end.
% @private
ivr_detach_fax_if_exists(Media, Opts) ->
    case ivr_detach_fax(Media, Opts) of
        {error,_} -> Media;
        {ok,Media1} -> Media1
    end.

%% detach fax termination from context
ivr_detach_fax(Media, _Opts) ->
    case Media of
        #media_ivr{fax=undefined} -> {error,{not_found,<<"fax not found">>}};
        #media_ivr{fax=FaxTerm} ->
            #media_ivr{mgc=MGC,msid=MSID,mgid=MG,ctx=CtxId}=Media,
            TermsId = ?MGC_TERM:get_term_id(FaxTerm),
            ?MGC:mg_subtract_terms({MGC,MSID,MG,CtxId},[TermsId]),
            Media1 = Media#media_ivr{fax=undefined},
            {ok,Media1}
    end.

%% return player/recorder id by termination id
ivr_check_fax_id(TermId, #media_ivr{fax=FaxTerm}) ->
    case ?MGC_TERM:get_term_id(FaxTerm) of
        TermId -> ok;
        _ -> undefined
    end.

% @private
get_fax_opts('t',Opts) -> maps:with([path,mode],Opts);
get_fax_opts('r',Opts) -> maps:with([path,mode],Opts).

% @private
prepare_faxterm(_,Opts) ->
    case maps:get(proto,Opts) of
        P when P=='t38';P=='t30' -> ?MGC_TERM:prepare_term_template(P)
    end.

% @private path, proto (t38, t30)
set_faxterm(_, FaxTerm, Opts) -> ?MGC_TERM:set_term_fax(FaxTerm, Opts).

%% ----------------------------------------------
%% RP-1466
%% Modify only term events descriptor (ex. enable/disable VAD)
%% ----------------------------------------------
modify_term_events(Media, CallId, {RequestId,Events}=Ev) when is_integer(RequestId), is_list(Events) ->
    #media_ivr{mgc=MGC, msid=MSID, mgid=MG, ctx=CtxId,
               terms=Terms}=Media,
    ?LOGMEDIA("-> modify_term ivr (ctx=~p, callid=~140p)", [CtxId, CallId]),
    case lists:keytake(CallId,1,Terms) of
        false ->
            ?LOGMEDIA("<- modify_term ivr: Error (callid not found)"),
            {error,<<"MGC error. CallId not found">>};
        {value,{_,TermRec},Terms1} ->
            #term_ivr{term=Term}=TermRec,
            {Type,Id,_Opts}=Term1 = ?MGC_TERM:set_term_events(Term, Ev), % prepare term for #media
            Term2 = ?MGC_TERM:set_term_events({Type,Id,[]}, Ev), % prepare term for mg
            case ?MGC:mg_modify_terms({MGC,MSID,MG,CtxId},[Term2]) of % modify term in context
                {error,Reason}=Err ->
                    ?LOGMEDIA("<- modify_term ivr: Error (reason=~140p)", [Reason]),
                    Err;
                {ok,[_Term3]} ->
                    TermRec1 = TermRec#term_ivr{term=Term1,
                                                term_events=Ev},
                    {ok,Media#media_ivr{terms=[{CallId,TermRec1}|Terms1]}}
            end
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

prepare_in_term(Req, _MOpts) ->
    #sipmsg{contacts=XRContacts,
            from={_FromUri, FromTag},
            cseq=_CSeq,
            call_id=XCallId}=Req,
    #sdp{}=XRSdp=?U:extract_sdp(Req),
    Addr = ?U:get_host_addr(),
    % create back sdp
    Dir='in',
    XTermType = ?MGC_TERM:build_term_type(XRContacts,XRSdp), % handle error?
    SdpSessId = (10000 + erlang:phash2(XCallId) rem 90000) * 2,
    XLSdpO = {"-", SdpSessId, SdpSessId, "IN", "IP4", Addr},
    XLSdpBase = ?M_SDP:filter_by_local_media_fmts(?M_SDP:reverse_stream_mode(XRSdp), undefined),
    XTermTemplate = ?MGC_TERM:prepare_term_template(XTermType, XLSdpBase, XRSdp), % prepare term template for mg
    TermRec = #term_ivr{dir=Dir,
                        callid=XCallId,
                        tag=FromTag,
                        term_type=XTermType,
                        sdp_r=XRSdp,
                        sdp_o= XLSdpO,
                        sdp_l_base=XLSdpBase},
    {XTermTemplate, TermRec}.
