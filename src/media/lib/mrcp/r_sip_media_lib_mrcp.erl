%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 10.07.2019
%%% @doc RP-1470
%%%      Implements interaction processing to MGC (Media Gate Controller) in case of MRCP media-termination.
%%%      Builds SDP, modify context and term.
%%%      Uses the same media-context as basic dialog.
%%%      Based on IVR (r_sip_media_lib_ivr)

-module(r_sip_media_lib_mrcp).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([add_outgoing_term/4,
         update_outgoing_term_by_remote/3,
         del_term/2]).

%% ================================================================================
%% Types
%% ================================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_mgc.hrl").
-include("../include/r_sip_mrcp.hrl").

%% ================================================================================
%% API functions
%% ================================================================================

%% ----------------------------------------------
%% create new term (local side)
%% ----------------------------------------------
-spec add_outgoing_term(Media::#media_mrcp{}, Resource::binary(), Mode::sendrecv|sendonly|recvonly, CallId::binary()) ->
                               {ok, Media::#media_mrcp{}, LSdp::#sdp{}} | {error,Reason::term()}.
%% ----------------------------------------------
add_outgoing_term(Media, Resource, Mode, CallId) ->
    #media_mrcp{mgc=MGC,msid=MSID,mgid=MG,ctx=CtxId}=Media,
    Addr = ?U:get_host_addr(),
    % create back sdp
    TermType = ?MGC_TERM:build_term_type(),
    SdpSessId = (10000 + erlang:phash2(CallId) rem 90000) * 2,
    LSdpO = {"-", SdpSessId, SdpSessId, "IN", "IP4", Addr},
    Sdp = ?M_SDP:make_offer_sdp([audio,{filter_codecs,[<<"PCMU/8000">>,<<"PCMA/8000">>]}]),
    LSdpD_Base = case Mode of
                     sendrecv -> ?SENDRECV(Sdp);
                     sendonly -> ?SENDONLY(Sdp);
                     recvonly -> ?RECVONLY(Sdp)
                 end,
    TermTemplate = ?MGC_TERM:prepare_term_template(TermType, LSdpD_Base), % prepare term template for mg
    case ?MGC:mg_add_terms({MGC,MSID,MG,CtxId},[TermTemplate]) of % add z callee's term to mg
        {error,_}=Err -> Err;
        {ok, [Term]} ->
            after_add(Media, {Term, LSdpO, LSdpD_Base, TermType, Resource, Mode, CallId})
    end.

%% @private
after_add(Media, {Term, LSdpO, LSdpD_Base, TermType, Resource, _Mode, CallId}) ->
    LSdpD_MEGACO = ?MGC_TERM:get_term_localsdp(Term),
    case ?D_SDP:make_sdp(LSdpD_MEGACO, LSdpO) of
        #sdp{}=LSdp_MEGACO ->
            LSdp_MRCP = add_mrcp_stream(Resource, LSdp_MEGACO),
            LSdp1 = ?M_SDP:set_sess(LSdp_MRCP),
            TermRec = #term_mrcp{dir='out',
                                 callid=CallId,
                                 term=Term,
                                 term_type=TermType,
                                 sdp_o=LSdpO,
                                 sdp_l_descr_base=LSdpD_Base,
                                 sdp_l_descr_megaco=LSdpD_MEGACO,
                                 sdp_l_megaco=LSdp_MEGACO,
                                 sdp_l_mrcp=LSdp_MRCP},
            #media_mrcp{terms=Terms}=Media,
            {ok, Media#media_mrcp{terms=[{CallId,TermRec}|Terms]}, LSdp1};
        _ ->
            {error, "MGC sdp error"}
    end.

%% @private
add_mrcp_stream(Resource, #sdp{medias=[#sdp_m{attributes=AudioAttrs}=Audio]}=SdpMEGACO) ->
    MrcpAttrs=[{<<"setup">>,[<<"active">>]},
               {<<"connection">>,[<<"new">>]},
               {<<"resource">>,[Resource]},
               {<<"cmid">>,[<<"1">>]}],
    MediaMRCP=#sdp_m{media = <<"application">>,
                     port = 9,
                     proto = <<"TCP/MRCPv2">>,
                     fmt = [<<"1">>],
                     attributes = MrcpAttrs},
    Audio1=Audio#sdp_m{attributes = lists:keystore(<<"mid">>,1,AudioAttrs,{<<"mid">>,[<<"1">>]})},
    SdpMEGACO#sdp{medias=[MediaMRCP,Audio1]}.

%% ----------------------------------------------
%% modify existing term by remote
%% ----------------------------------------------
-spec update_outgoing_term_by_remote(Media::#media_mrcp{}, CallId::binary(), RSdp::#sdp{}) ->
                                            {ok, Media::#media_mrcp{}} | {error,Reason::term()}.
%% ----------------------------------------------
update_outgoing_term_by_remote(Media, CallId, RSdp) ->
    #media_mrcp{mgc=MGC,msid=MSID,mgid=MG,ctx=CtxId,
                terms=Terms}=Media,
    case lists:keytake(CallId, 1, Terms) of
        false -> {error, "MGC invalid callid, term not found"};
        {value,{_,#term_mrcp{term=Term}=TermRec},Terms1} ->
            RSdp_MEGACO = remove_mrcp_stream(RSdp),
            RSdpD_MEGACO = ?D_SDP:prepare_descr_direct(RSdp_MEGACO),
            Term1 = ?MGC_TERM:set_term_remotesdp(Term, RSdpD_MEGACO),
            case ?MGC:mg_modify_terms({MGC,MSID,MG,CtxId},[Term1]) of
                {error,_}=Err -> Err;
                {ok, [Term2]} ->
                    TermRec1 = TermRec#term_mrcp{term=Term2,
                                                 sdp_r_mrcp=RSdp,
                                                 sdp_r_megaco=RSdp_MEGACO,
                                                 sdp_r_descr_megaco=RSdpD_MEGACO},
                    {ok, Media#media_mrcp{terms=[{CallId,TermRec1}|Terms1]}}
            end end.

%% @private
remove_mrcp_stream(#sdp{medias=Medias}=SdpMRCP) ->
    Medias1=lists:filter(fun(#sdp_m{media= <<"application">>}) -> false; (_) -> true end, Medias),
    SdpMRCP#sdp{medias=Medias1}.

%% ----------------------------------------------
%% When replacing, previous term should be deleted
%% ----------------------------------------------
-spec del_term(Media::#media_mrcp{}, CallId::binary()) ->
                      {ok, Media::#media_mrcp{}} | {error,Reason::term()}.
%% ----------------------------------------------
del_term(#media_mrcp{terms=[]}=Media, _CallId) -> {ok,Media};
del_term(Media, CallId) ->
    #media_mrcp{mgc=MGC, msid=MSID, mgid=MG, ctx=CtxId,
                terms=Terms}=Media,
    ?LOGMEDIA("-> del_term mrcp (ctx=~p)", [CtxId]),
    case lists:keytake(CallId,1,Terms) of
        false ->
            ?LOGMEDIA("<- del_term mrcp: Error (callid not found)"),
            {error,<<"MGC error. CallId not found">>};
        {value,{_,#term_mrcp{term=Term}},Terms1} ->
            TermIds = ?MGC_TERM:get_terms_id([Term]),
            case ?MGC:mg_subtract_terms({MGC,MSID,MG,CtxId},TermIds) of
                {error,Reason} ->
                    ?LOGMEDIA("<-  del_term mrcp: Internal Server Error (~140p)", [Reason]),
                    {error, ?InternalError("MGC error on remove")};
                {ok, _} ->
                    ?LOGMEDIA("<- del_term mrcp (ctx=~p)", [CtxId]),
                    {ok, Media#media_mrcp{terms=Terms1}}
            end end.