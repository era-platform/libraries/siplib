%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Provides service to change session properties (reinvite, double sdp, ...).
%%%         Applies new SDP to existing term, difference

-module(r_sip_media_sesschange).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([update_sesschange_sdp/2]).

%% ================================================================================
%% Types
%% ================================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_mgc.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% -----------------------------------------------
%% common update local sdp (both for request and response)
%% -----------------------------------------------
-spec update_sesschange_sdp(Media::#media{} |
                                    {MGC::term(),MSID::binary(),MG::term(),CtxId::integer()},
                                    {SourceRemoteSdp::#sdp{}, DestinationOpts::list(), DestinationTerm::tuple()} |
                                    {SourceRemoteSdp::#sdp{}, SourcePreviousRemoteSdp::#sdp{}, DestinationSdpOProperty::tuple(), DestinationTerm::tuple()}) ->
          {ok, SDP::#sdp{}, Opts::list()}.
%% -----------------------------------------------
update_sesschange_sdp(#media{}=Media, {XRemoteSdp,YOpts,YTerm}) ->
    #media{mgc=MGC,msid=MSID,mgid=MG,ctx=CtxId}=Media,
    update_sesschange_sdp({MGC,MSID,MG,CtxId}, {XRemoteSdp,YOpts,YTerm});
%%
update_sesschange_sdp({MGC,MSID,MG,CtxId}, {XRemoteSdp,YOpts,YTerm}) ->
    [XRemoteSdpPrev,YSdpO] = ?EU:extract_optional_props(['sdp_l_base','sdp_o'], YOpts),
    YSdpO1 = erlang:setelement(3,YSdpO, erlang:element(3,YSdpO)+1),
    YOpts1 = lists:keystore('sdp_o',1,YOpts,{'sdp_o',YSdpO1}),
    Extra1 = #{mgcinfo => {MGC,MSID,MG,CtxId}},
    case update_sesschange_sdp(Extra1, {XRemoteSdp,XRemoteSdpPrev,YSdpO1,YTerm}) of
        {error,_}=Err -> Err;
        {ok,YLocalSdp1} ->
            YOpts2 = lists:keystore('sdp_l_base',1,YOpts1,{'sdp_l_base',XRemoteSdp}),
            {ok,YLocalSdp1,YOpts2}
    end;
%%
update_sesschange_sdp({MGC,MSID,MG,CtxId}, {XRemoteSdp,XRemoteSdpPrev,YSdpO,YTerm}) ->
    Extra1 = #{mgcinfo => {MGC,MSID,MG,CtxId}},
    update_sesschange_sdp(Extra1, {XRemoteSdp,XRemoteSdpPrev,YSdpO,YTerm});
%%
update_sesschange_sdp(Extra1, {XRemoteSdp,XRemoteSdpPrev,YSdpO,YTerm}) ->
    YLocalSdpD = ?MGC_TERM:get_term_localsdp(YTerm),
    YLocalSdp = ?D_SDP:make_sdp(YLocalSdpD, YSdpO),
    case update_sesschange_sdp_1(YLocalSdp, XRemoteSdpPrev, XRemoteSdp, Extra1#{yterm => YTerm}) of
        {error,_}=Err -> Err;
        {ok,YLocalSdp1} ->
            {ok,YLocalSdp1}
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -----------------------------------------------
%% detect if build forwarded sdp automatically by difference with previous
%%  or to exchange with mgc/mg
%% -----------------------------------------------
update_sesschange_sdp_1(#sdp{}=LocalSdp, #sdp{}=RemoteSdpPrev, #sdp{}=RemoteSdp, #{}=Extra2) ->
    YTerm = maps:get(yterm,Extra2),
    case ?MGC_TERM:get_term_type(YTerm) of
        webrtc ->
            update_sesschange_sdp_mgc(LocalSdp, RemoteSdpPrev, RemoteSdp, Extra2);
        _ ->
            update_sesschange_sdp_auto(LocalSdp, RemoteSdpPrev, RemoteSdp, Extra2)
    end.

%% -----
%% @private
%% change sdp by mgc call with $
%% -----
update_sesschange_sdp_mgc(#sdp{}=LocalSdp, #sdp{}=_RemoteSdpPrev, #sdp{}=RemoteSdp, #{}=Extra2) ->
    % ?OUT("DEBUG. REINVITE WEBRTC"),
    [MgcInfo,YTerm] = ?EU:maps_get([mgcinfo,yterm],Extra2),
    #sdp{user=U, id=Id, vsn=Vsn, address={OType, OAddrType, OAddress}}=LocalSdp,
    LSdpDT = ?D_SDP:prepare_descr_template(?LO(RemoteSdp)),
    ToTerm1 = ?MGC_TERM:set_term_localsdp(?MGC_TERM:prepare_term_opts(YTerm, [localsdp]), LSdpDT),
    case ?MGC:mg_modify_terms(MgcInfo,[ToTerm1]) of
        {error,_}=Err -> Err;
        {ok,[ToTerm2]} ->
            LSdpD1 = ?MGC_TERM:get_term_localsdp(ToTerm2),
            case ?D_SDP:make_sdp(LSdpD1,{U,Id,Vsn,OType,OAddrType,OAddress}) of
                #sdp{}=LSdp1 -> {ok,LSdp1};
                _ -> {error, "MGC sdp error"}
            end
    end.

%% -----
%% @private
%% change sdp by current local, prev opposite remote, current opposite remote
%% -----
update_sesschange_sdp_auto(#sdp{}=LocalSdp, #sdp{}=RemoteSdpPrev, #sdp{}=RemoteSdp, Extra2) ->
    % ?OUT("DEBUG. REINVITE AUTO"),
    Up = cmp_sdps(LocalSdp, RemoteSdp), % Up = cmp_sdps(RemoteSdpPrev, RemoteSdp),
    ?LOGMEDIA("  DEBUG reInvite. update_sesschange_sdp_2 (1): ~n  LocalSDPD: ~140p~n  RemoteSDPPrev: ~140p~n  RemoteSDP: ~140p~n  Up: ~140p", [LocalSdp, RemoteSdpPrev, RemoteSdp, Up]),
    #sdp{}=LocalSdp1 =
               case lists:keyfind(del, 1, Up) of
                   false -> LocalSdp;
                   {_,Del} -> del_streams(LocalSdp, Del)
               end,
    ?LOGMEDIA("  DEBUG reInvite. update_sesschange_sdp_2 (2): ~n  LocalSDPD: ~140p", [LocalSdp1]),
    #sdp{}=LocalSdp2 =
               case lists:keyfind(mod, 1, Up) of
                   false -> LocalSdp1;
                   {_,Mod} -> mod_streams(LocalSdp1, RemoteSdp, Mod)
               end,
    ?LOGMEDIA("  DEBUG reInvite. update_sesschange_sdp_2 (3): ~n  LocalSDPD: ~140p", [LocalSdp2]),
    #sdp{}=LocalSdp3 = % @todo {error,_}
               case lists:keyfind(add, 1, Up) of
                   false -> LocalSdp2;
                   {_,Add} -> add_streams(LocalSdp2, RemoteSdp, Add, Extra2)
               end,
    ?LOGMEDIA("  DEBUG reInvite. update_sesschange_sdp_2 (4): ~n  LocalSDPD: ~140p", [LocalSdp3]),
    {ok, LocalSdp3}.

%% ================================================================

%% -----------------------------------------------
%% compare previous and new R sdp
%%   RP-1261 used only on auto update (rtp), non mgc.
%%           webrtc is modified over mgc, noticed that only webrtc has multiple audio streams   
%% -----------------------------------------------
cmp_sdps(Sdp1, Sdp2) ->
    #sdp{medias=M1}=Sdp1,
    #sdp{medias=M2}=Sdp2,
    ML1 = lists:usort(lists:map(fun(#sdp_m{media=M}) -> M end, M1)), % RP-1261 sort->usort
    ML2 = lists:usort(lists:map(fun(#sdp_m{media=M}) -> M end, M2)), % RP-1261 sort->usort
    DelAdd = cmp_lists(ML1, ML2),
    Fcmp = fun(#sdp_m{media=M}=SdpM1, Acc) ->
                   case lists:keyfind(M, #sdp_m.media, M2) of
                       false -> Acc;
                       SdpM2 -> [cmp_mediastreams(SdpM1, SdpM2) | Acc]
                   end
           end,
    case lists:flatten(lists:foldl(Fcmp, [], M1)) of
        [] -> DelAdd;
        Mod -> lists:reverse([{mod, Mod} | DelAdd])
    end.

%% @private
cmp_mediastreams(#sdp_m{media=M}=SdpM1, #sdp_m{media=M}=SdpM2) ->
    #sdp_m{fmt=Fmt1, attributes=A1}=SdpM1,
    #sdp_m{fmt=Fmt2, attributes=A2}=SdpM2,
    {Modes1, Modes2} = {defaultize_mediamodes(extract_mediamodes(A1)),
                        defaultize_mediamodes(extract_mediamodes(A2))},
    Fmt = cmp_lists(Fmt1, Fmt2),
    FmtX = lists:keydelete(del,1,Fmt), % #305.b
    Mod = cmp_mediamodes(Modes1, Modes2),
    case {Fmt,Mod} of
        {[],[]} -> [];
        {_,[]} -> {M, [{fmt,FmtX}]};
        {[],_} -> {M, [{mode,Mod}]};
        {_,_} -> {M, [{fmt,FmtX}, {mode,Mod}]}
    end.

%% @private
cmp_lists(L1, L2) ->
    case {L1--L2, L2--L1} of
        {[], []} -> [];
        {Dels, []} -> [{del, Dels}];
        {[], Adds} -> [{add, Adds}];
        {Dels, Adds} -> [{del, Dels}, {add, Adds}]
    end.

%% @private
extract_mediamodes(Attrs) ->
    lists:foldl(fun({M,[]}, Acc)
                     when M == <<"sendrecv">>;
                          M == <<"sendonly">>;
                          M == <<"recvonly">>;
                          M == <<"inactive">> ->
                        [M|Acc];
                   (_, Acc) -> Acc
                end, [], Attrs).

%% @private
defaultize_mediamodes([]) -> [<<"sendrecv">>];
defaultize_mediamodes(MMs) -> MMs.

%% @private
cmp_mediamodes(Modes1, Modes2) ->
    case Modes1 == Modes2 of
        true -> [];
        false -> Modes2
    end.

%% -----------------------------------------------
%% modify local sdp description by changes in reinvite sdp
%% -----------------------------------------------

%% ------------------------------------
%% del streams from local sdp
%% ------------------------------------
del_streams(LSdp, MNames) ->
    %?LOGMEDIA("  DEBUG reInvite. Del streams: ~n  Sdp ~140p~n Names: ~140p", [LSdp, MNames]),
    #sdp{medias=Ms}=LSdp,
    Ms1 = lists:filter(fun(#sdp_m{media=MName}) -> not lists:member(MName, MNames) end, Ms),
    LSdp#sdp{medias=Ms1}.

%% ------------------------------------
%% modify streams in local sdp
%% ------------------------------------
mod_streams(LSdp, BaseSdp, MChanges) ->
    %?LOGMEDIA("  DEBUG reInvite. Mod streams: ~n  LSdp: ~140p~n  BSdp: ~140p~n  Changes: ~140p", [LSdp, BaseSdp, MChanges]),
    #sdp{medias=Ms}=LSdp,
    #sdp{medias=BaseMs}=BaseSdp,
    F = fun(#sdp_m{media=MName, fmt=MFmt, attributes=MAttrs}=M) ->
                case lists:keyfind(MName,1,MChanges) of
                    false -> M;
                    {_,P} ->
                        [Fmt,Modes] = ?EU:extract_optional_props([fmt,mode],P),
                        {MFmt1,MAttrs1} = case Fmt of
                                              undefined -> {MFmt, MAttrs};
                                              _ ->
                                                  [Del,Add] = ?EU:extract_optional_default([del,add],Fmt,[[],[]]),
                                                  % del
                                                  MF1 = MFmt -- Del,
                                                  MA1 = lists:filter(fun({<<"rtpmap">>,[Payload|_]}) -> not lists:member(Payload, Del);
                                                                        ({<<"fmtp">>,[Payload|_]}) -> not lists:member(Payload, Del);
                                                                        (_) -> true
                                                                     end, MAttrs),
                                                  % add
                                                  #sdp_m{attributes=BaseMAttrs} = lists:keyfind(MName,#sdp_m.media,BaseMs),
                                                  MF2 = MF1 ++ Add,
                                                  MA2 = MA1 ++ lists:filter(fun({<<"rtpmap">>,[Payload|_]}) -> lists:member(Payload, Add);
                                                                               ({<<"fmtp">>,[Payload|_]}) -> lists:member(Payload, Add);
                                                                               (_) -> false
                                                                            end, BaseMAttrs),
                                                  {MF2, MA2}
                                          end,
                        MAttrs2 = case Modes of
                                      undefined -> MAttrs1;
                                      _ ->
                                          lists:filter(fun({<<"sendrecv">>,[]}) -> false;
                                                          ({<<"sendonly">>,[]}) -> false;
                                                          ({<<"recvonly">>,[]}) -> false;
                                                          ({<<"inactive">>,[]}) -> false;
                                                          (_) -> true
                                                       end, MAttrs1) ++ [{Mode,[]} || Mode <- Modes]
                                  end,
                        M#sdp_m{fmt=MFmt1, attributes=MAttrs2}
                end
        end,
    LSdp#sdp{medias=lists:map(F, Ms)}.

%% ------------------------------------
%% add streams to local sdp
%% ------------------------------------
add_streams(LSdp, BaseSdp, MNames, #{}=Extra2) ->
    [MgcInfo,ToTerm] = ?EU:maps_get([mgcinfo,yterm],Extra2),
    #sdp{medias=Ms,
         user=U, id=Id, vsn=Vsn, address={OType, OAddrType, OAddress}}=LSdp,
    #sdp{medias=BaseMs}=BaseSdp,
    LSdpT = BaseSdp#sdp{medias=lists:filter(fun(#sdp_m{media=MName}) -> lists:member(MName, MNames) end, BaseMs)},
    LSdpDT = ?D_SDP:prepare_descr_template(LSdpT),
    ToTerm1 = ?MGC_TERM:set_term_localsdp(?MGC_TERM:prepare_term_opts(ToTerm, [localsdp]), LSdpDT),
    case ?MGC:mg_modify_terms(MgcInfo,[ToTerm1]) of
        {error,_}=Err -> Err;
        {ok,[ToTerm2]} ->
            LSdpD1 = ?MGC_TERM:get_term_localsdp(ToTerm2),
            ?LOGMEDIA("  DEBUG reInvite. Add streams: ~n  SdpOut ~140p~n  SdpO: ~140p~n  Names: ~140p", [LSdpD1, {U,Id,Vsn,OType,OAddrType,OAddress}, MNames]),
            case ?D_SDP:make_sdp(LSdpD1,{U,Id,Vsn,OType,OAddrType,OAddress}) of
                #sdp{medias=Ms1} ->
                    ?LOGMEDIA("  DEBUG reInvite. Mix streams ~140p", [LSdp#sdp{medias=Ms++Ms1}]),
                    LSdp#sdp{medias=Ms++Ms1};
                _ -> {error, "MGC sdp error"}
            end
    end.
