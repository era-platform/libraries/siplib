%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Adapter/Delegate/Proxy/Facade pattern
%%%         Provides access to mgc-battery of domain to manage mg, context and term entities
%%% @todo

-module(r_sip_mgc_adapter).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([mg_check_any_available/1,
         mg_check_available_slots/1,

         mg_create_context/3,
         mg_delete_context/1,
         mg_modify_topology/2,
         mg_modify_context_props/2,
         mg_add_terms/2,
         mg_modify_terms/2,
         mg_modify_terms_ex/2,
         mg_subtract_terms/2,

         mgc_ping_ctx/1
        ]).

%-compile(export_all).

%% ================================================================================
%% Types
%% ================================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_mgc.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

% --------------------------------------------
% checks if there is any available mg
%
-spec mg_check_any_available(Opts::list()) ->
          {ok, true} | {ok, false} | {error, Reason::term()}.

mg_check_any_available(Opts) ->
    Groups = get_mgc_groups(),
    try
        F = fun(GroupIdx,Acc) ->
                    MGC = ?ENVNAMING:get_mgc_name(GroupIdx),
                    case mg_check_any_available(MGC, Opts) of
                        {ok,true}=R -> throw(R);
                        {ok,false}=R -> R;
                        R -> case Acc of {ok,_} -> Acc; _ -> R end
                    end end,
        lists:foldl(F, {error, mgc_not_found}, Groups)
    catch
        throw:R -> R
    end.

% @private
mg_check_any_available(MGC, Opts) ->
    ?LOGMGC("   MGC. -> check_any_available"),
    F = fun() -> {check_any_available, [Opts], make_ref()} end,
    Reply = case ?U:call_media_controller(MGC, F, undefined) of
                {ok,Res}=R when is_boolean(Res) -> R;
                undefined -> {error, mgc_connect_error};
                R -> R
            end,
    ?LOGMGC("   MGC. <- check_any_available ~n\t~140tp", [Reply]),
    Reply.

% --------------------------------------------
% checks if there is any available mg and returns total available context slot count
%
-spec mg_check_available_slots(Opts::list()) ->
          {ok, AvMgCnt::integer(), AvSlotCnt::integer()} | {error, Reason::term()}.

mg_check_available_slots(Opts) ->
    Groups = get_mgc_groups(),
    try
        F = fun(_,{ok,M,C}=Acc) when M>0,C>300 -> throw(Acc);
               (GroupIdx,Acc) ->
                    MGC = ?ENVNAMING:get_mgc_name(GroupIdx),
                    case mg_check_available_slots(MGC, Opts) of
                        {ok,M,C}=R -> case Acc of {ok,M1,C1} -> {ok,M+M1,C+C1}; _ -> R end;
                        R -> case Acc of {ok,_,_} -> Acc; _ -> R end
                    end end,
        lists:foldl(F, {error, mgc_not_found}, Groups)
    catch
        throw:R -> R
    end.

% @private
mg_check_available_slots(MGC, Opts) ->
    ?LOGMGC("   MGC. -> check_available_slots"),
    F = fun() -> {check_available_slots, [Opts], make_ref()} end,
    Reply = case ?U:call_media_controller(MGC, F, undefined) of
                {ok,A1,A2}=R when is_integer(A1), is_integer(A2) -> R;
                undefined -> {error, mgc_connect_error};
                R -> R
            end,
    ?LOGMGC("   MGC. <- check_available_slots ~n\t~140tp", [Reply]),
    Reply.

% --------------------------------------------
% create context and outgoing terms
%
-spec mg_create_context(Id::term(), TermTemplates::list(), Opts::list()) ->
          'undefined' | {error, busy} | {error, not_found} | {MG::term(), ContextId::term(), Terms::list()}.

mg_create_context({MSID}, TermTemplates, Opts) ->
    Groups = get_mgc_groups(),
    try
        F = fun(GroupIdx,Acc) ->
                    MGC = ?ENVNAMING:get_mgc_name(GroupIdx),
                    case mg_create_context(MGC, {MSID}, TermTemplates, Opts) of
                        {ok,_}=R -> throw(R);
                        R -> case Acc of {ok,_,_} -> Acc; _ -> R end
                    end end,
        lists:foldl(F, {error, mgc_not_found}, Groups)
    catch
        throw:R -> R
    end.

% ---
mg_create_context(MGC, {MSID}, TermTemplates, Opts) ->
    ?LOGMGC("   MGC. -> create_context ~n\tMSID: ~140tp, ~n\tT: ~140tp~n\tOpts: ~120tp", [MSID, TermTemplates, Opts]),
    F = fun() -> {create_context, [MSID,self(),filter(TermTemplates),Opts], make_ref()} end,
    Reply = case ?U:call_media_controller(MGC, F, undefined) of
                {ok,{MG,CtxId,Terms1}} ->
                    Terms2 = ?MGC_TERM:merge_terms_sdp_opts(Terms1, TermTemplates),
                    Terms3 = ?MGC_TERM:merge_terms_opts_add(Terms2, TermTemplates),
                    {ok,{MGC,MG,CtxId,Terms3}};
                undefined ->
                    ?LOG('$warning', "MGC. create_context ~1000p timeout. Flushed", [MSID]),
                    ?U:cast_media_controller(MGC, {flush_ctx,MSID}),
                    {error, mgc_connect_error};
                R -> R
            end,
    ?LOGMGC("   MGC. <- create_context ~n\t~140tp", [Reply]),
    Reply.

% --------------------------------------------
% deletes context with all terms and topology
%
-spec mg_delete_context({Id::term(), MG::term(), ContextId::term()}) ->
          'undefined' | ok.

mg_delete_context({MGC,MSID,MG,CtxId}) ->
    ?LOGMGC("   MGC. -> delete_context ~tp ~n\tMSID:  ~140tp, ~n\tMG:  ~140tp", [CtxId, MSID, MG]),
    F = fun() -> {delete_context, [{MSID,MG,CtxId}], make_ref()} end,
    Reply = ?U:cast_media_controller(MGC, F),
    ?LOGMGC("   MGC. <- delete_context ~tp (casted)", [CtxId]),
    Reply.

% --------------------------------------------
% modifies topology of context
%
-spec mg_modify_topology({Id::term(), MG::term(), ContextId::term()}, Opts::list()) ->
          'undefined' | ok | {error, R::term()}.

mg_modify_topology({MGC,MSID,MG,CtxId}, Opts) ->
    ?LOGMGC("   MGC. -> modify_topology ~tp ~n\tMSID:  ~140tp, ~n\tMG:  ~140tp", [CtxId, MSID, MG]),
    F = fun() -> {modify_topology, [{MSID,MG,CtxId}, Opts], make_ref()} end,
    Reply = ?U:cast_media_controller(MGC, F),
    ?LOGMGC("   MGC. <- modify_topology ~tp (casted)", [CtxId]),
    Reply.

% --------------------------------------------
% modifies context props
%
-spec mg_modify_context_props({Id::term(), MG::term(), ContextId::term()}, Opts::list()) ->
          'undefined' | ok | {error, R::term()}.

mg_modify_context_props({MGC,MSID,MG,CtxId}, Opts) ->
    ?LOGMGC("   MGC. -> modify_context_props ~tp ~n\tMSID:  ~140tp, ~n\tMG:  ~140tp", [CtxId, MSID, MG]),
    F = fun() -> {modify_context_props, [{MSID,MG,CtxId}, Opts], make_ref()} end,
    Reply = ?U:cast_media_controller(MGC, F),
    ?LOGMGC("   MGC. <- _modify_context_props ~tp (casted)", [CtxId]),
    Reply.

% --------------------------------------------
% adds term to context
%
mg_add_terms({MGC,MSID,MG,CtxId}, TermTemplates) ->
    ?LOGMGC("   MGC. -> add_term ~tp, ~n\tMG: ~140tp, ~n\tT: ~140tp", [CtxId, MG, TermTemplates]),
    F = fun() -> {add_terms, [{MSID,MG,CtxId},filter(TermTemplates)], make_ref()} end,
    Reply = case ?U:call_media_controller(MGC, F, undefined) of
                {ok, Terms1} ->
                    Terms2 = ?MGC_TERM:merge_terms_sdp_opts(Terms1, TermTemplates),
                    {ok, ?MGC_TERM:merge_terms_opts_add(Terms2, TermTemplates)};
                undefined -> {error, mgc_connect_error};
                R -> R
            end,
    ?LOGMGC("   MGC. <- add_terms ~tp ~n\t~140tp", [CtxId, Reply]),
    Reply.

% --------------------------------------------
% modifies existing term in context
%
mg_modify_terms({MGC,MSID,MG,CtxId}, Terms) ->
    ?LOGMGC("   MGC. -> modify_terms ~tp, ~n\tMG: ~140tp, ~n\tT: ~140tp", [CtxId, MG, Terms]),
    F = fun() -> {modify_terms, [{MSID,MG,CtxId},filter(Terms)], make_ref()} end,
    Reply = case ?U:call_media_controller(MGC, F, undefined) of
                {ok, Terms1} ->
                    Terms2 = ?MGC_TERM:merge_terms_sdp_opts(Terms1, Terms),
                    Terms3 = ?MGC_TERM:merge_terms_opts_mod(Terms2, Terms),
                    {ok, Terms3};
                undefined -> {error, mgc_connect_error};
                R -> R
            end,
    ?LOGMGC("   MGC. <- modify_terms ~tp ~n\t~140tp", [CtxId, Reply]),
    Reply.

% TermsEx :: [{Term, OptList}]
mg_modify_terms_ex({MGC,MSID,MG,CtxId}, TermsEx) ->
    Terms = [Term || {Term, _Opts} <- TermsEx],
    TSub = [?MGC_TERM:prepare_term_opts(Term, Opts) || {Term, Opts} <- TermsEx],
    ?LOGMGC("   MGC. -> modify_terms ~tp, ~n\tMG: ~140tp, ~n\tT: ~140tp, ~n\tTSub: ~140tp", [CtxId, MG, TermsEx,TSub]),
    F = fun() -> {modify_terms, [{MSID,MG,CtxId},filter(TSub)], make_ref()} end,
    Reply = case ?U:call_media_controller(MGC, F, undefined) of
                {ok, Terms1} ->
                    Terms2 = ?MGC_TERM:merge_terms_sdp_opts(Terms1, Terms),
                    {ok, ?MGC_TERM:merge_terms_opts_mod(Terms2, Terms)};
                undefined -> {error, mgc_connect_error};
                R -> R
            end,
    ?LOGMGC("   MGC. <- modify_terms ~tp ~n\t~140tp", [CtxId, Reply]),
    Reply.

% --------------------------------------------
% subtract term/terms from context
%
mg_subtract_terms({MGC,MSID,MG,CtxId}, TermIds) ->
    ?LOGMGC("   MGC. -> subtract_terms ~tp ~n\tMG: ~140tp, ~n\tT: ~140tp", [CtxId, MG, TermIds]),
    F = fun() -> {subtract_terms, [{MSID,MG,CtxId},TermIds], make_ref()} end,
    Reply = case ?U:call_media_controller(MGC, F, undefined) of
                undefined -> {error, mgc_connect_error};
                R -> R
            end,
    ?LOGMGC("   MGC. <- subtract_terms ~tp ~n\t~140tp", [CtxId, Reply]),
    Reply.

% --------------------------------------------
% pings context (context should be auto deleted after ping timeout)
%
mgc_ping_ctx(Pinged) ->
    ?LOGMGC("   MGC. -> ping_ctx (cnt=~tp))", [length(Pinged)]),
    MGCs = lists:foldl(fun(#{mgc:=MGC,msid:=MSID}, Acc) ->
                               case lists:keyfind(MGC,1,Acc) of
                                   false -> lists:keystore(MGC,1,Acc,{MGC,[MSID]});
                                   {_,MSIDs} -> lists:keyreplace(MGC,1,Acc,{MGC,[MSID|MSIDs]})
                               end end, [], Pinged),
    lists:foreach(fun({MGC,MSIDs}) ->
                          F = fun() -> {ping_ctx, MSIDs} end,
                          ?U:cast_media_controller(MGC, F)
                  end, MGCs),
    ?LOGMGC("   MGC. <- ping_ctx"),
    ok.


%% ====================================================================
%% Internal functions
%% ====================================================================

% ---
get_mgc_groups() ->
    %?EU:randomize(?CFG:get_site_mgc_groups()).
    case ?ENVSTORE:lazy_t('mgc_groups', fun() -> cache_groups() end, {3000,2000,500}, {true,true}) of
        {R,L} -> lists:append(?EU:randomize(R),?EU:randomize(L));
        R -> ?EU:randomize(R) % TODO delete, left for patched versions
    end.

% @private @lazy
cache_groups() ->
    case ?CFG:get_site_mgc_groups() of
        [] -> [];
        [G] -> [G];
        Groups ->
            MyAddrs = [?EU:node_addr()], % TODO all interfaces of current srv
            F = fun(GIdx) ->
                        GName = ?ENVNAMING:get_mgc_name(GIdx),
                        case ?ENVGLOBAL:global_whereis_name(GName) of
                            undefined -> false;
                            GPid ->
                                GNode = node(GPid),
                                GAddrs = [?EU:node_addr(GNode)], % TODO all addrs of all servers in group
                                {true,{GIdx,ordsets:intersection(MyAddrs,GAddrs)==[]}}
                        end end,
            % filter inactive
            Groups1 = lists:filtermap(F, Groups),
            % partition remote/local mgc addrs
            {Remote,Local} = lists:partition(fun({_,Remote}) -> Remote end, Groups1),
            Remote1 = lists:map(fun({GIdx,_}) -> GIdx end, Remote),
            Local1 = lists:map(fun({GIdx,_}) -> GIdx end, Local),
            {Remote1,Local1}
    end.

% ---
filter(Terms) ->
    filter_local(filter_remote(Terms)).

filter_local(Terms) ->
    filter_e({get_term_localsdp, set_term_localsdp}, Terms).

filter_remote(Terms) ->
    filter_e({get_term_remotesdp, set_term_remotesdp}, Terms).

filter_e({GetF, SetF}, Terms) ->
    [case ?MGC_TERM:GetF(Term) of
         undefined -> Term;
         SdpD ->
             ?MGC_TERM:SetF(Term,
                   ?D_SDP:filter_lines_before_mg(SdpD))
     end || Term <- Terms].
