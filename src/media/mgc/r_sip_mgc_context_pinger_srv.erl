%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Pinger is process which allow MGC to autoclear lost contexts and free resources.
%%%         If SIP NODE goes down, then PINGER interrups to confirm reserved contexts to MGC,
%%%         so MGC can drop lost contexts by ping timeout and free resources.
%%%         Also if DIALOG PROCESS which owns MGC context is down, then PINGER automatically
%%%         detects it and drops from subscriptions, so the same result on MGC
%%%         Every context is registered to MGC Pinger and unregister when finished.
%%% @todo

-module(r_sip_mgc_context_pinger_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

% interface
-export([start_link/1,

         add/2,
         del/1]).

% callback interface
-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).

%% ================================================================================
%% Types
%% ================================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_mgc.hrl").

-define(PINGTIMEOUT, 10000).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, Opts, []).

add({MGC,Key,MG,CtxId}, PidCallSrv) ->
    gen_server:cast(?MODULE, {add_ctx, [MGC,Key,MG,CtxId,PidCallSrv]}).

del({MGC,Key,MG,CtxId}) ->
    gen_server:cast(?MODULE, {del_ctx, [MGC,Key,MG,CtxId]}).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------------------------------------
%% Init
%% ------------------------------------------------------------
init(Opts) ->
    [ETS] = ?EU:extract_required_props([ets], Opts),
    State = #pingstate{ctx=ETS},
    erlang:send_after(?PINGTIMEOUT, self(), {ping_ctx}),
    ?OUT(" MGC pinger. inited (~140p)", [Opts]),
    {ok, State}.

%% ------------------------------------------------------------
%% Call
%% ------------------------------------------------------------

handle_call({print}, _From, State) ->
    ?OUT("MGC CONTEXT PINGER. State: ~140p", [State]),
    {reply, ok, State};

%% other
handle_call(_Request, _From, State) ->
    Reply = ok,
    {reply, Reply, State}.

%% ------------------------------------------------------------
%% Cast
%% ------------------------------------------------------------

handle_cast({add_ctx, [MGC,Key,MG,CtxId, PidCallSrv]}, State) ->
    #pingstate{ctx=ETS}=State,
    ets:insert(ETS, {Key,#{mgc=>MGC,
                            mg=>MG,
                            ctx=>CtxId,
                            pid=>PidCallSrv}}),
    {noreply, State};

handle_cast({del_ctx, [_MGC,Key,_MG,_CtxId]}, State) ->
    #pingstate{ctx=ETS}=State,
    ets:delete(ETS,Key),
    {noreply, State};

%% other
handle_cast(Request, State) ->
    ?LOG(" MGC pinger. handle_cast(~140p)", [Request]),
    {noreply, State}.

%% ------------------------------------------------------------
%% Info
%% ------------------------------------------------------------

handle_info({ping_ctx}, State) ->
    State1 = ping(State),
    erlang:send_after(?PINGTIMEOUT, self(), {ping_ctx}),
    {noreply, State1};

%% other
handle_info(Info, State) ->
    ?LOG(" MGC pinger. handle_info(~140p)", [Info]),
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

ping(#pingstate{ctx=ETS}=State) ->
    % check first if pid of callsrv still exists
    F = fun({Key,#{}=Data}, {E,N}) ->
                MGC = maps:get(mgc,Data),
                PidCallSrv = maps:get(pid,Data),
                case process_info(PidCallSrv) of
                    undefined -> {E,[Key|N]};
                    _ -> {[#{mgc=>MGC,msid=>Key}|E],N}
                end
        end,
    {Exist,NotExist} = ets:foldl(F,{[],[]},ETS),
    % del abnormally destroyed
    lists:foreach(fun(Key) -> ets:delete(ETS,Key) end, NotExist),
    % ping mgc on existing
    case Exist of
        [] -> ok;
        _ -> ?MGC:mgc_ping_ctx(Exist)
    end,
    State.
