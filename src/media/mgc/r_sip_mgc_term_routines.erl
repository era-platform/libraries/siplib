%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Provides services to operate with term mapping (prior to request to mgc)

-module(r_sip_mgc_term_routines).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([build_term_type/0,
         build_term_type/1,
         build_term_type/2,

         prepare_term_template/1,
         prepare_term_template/2,
         prepare_term_template/3,
         prepare_term_template_by_previous/3,

         get_term_type/1,
         find_term_by_type/2,
         update_term_by_type/2,

         prepare_term_opts/2,

         merge_terms_opts_mod/2,
         merge_terms_opts_add/2,
         merge_terms_sdp_opts/2,

         get_terms_id/1,
         get_term_id/1,
         get_term_localsdp/1,
         set_term_localsdp/2,
         get_term_remotesdp/1,
         set_term_remotesdp/2,
         set_term_sdp/3,

         set_term_state_props/2,
         set_term_mode/3,
         set_term_callid/2,

         set_term_ivr_play/2,
         set_term_ivr_rec/2,
         set_term_fax/2,

         set_term_events/2]).

%-compile(export_all).

%% ================================================================================
%% Types
%% ================================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_mgc.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

% builds term type by contact
build_term_type() -> rtp.
build_term_type([]) -> {error, invalid_contact_scheme};
build_term_type([#uri{scheme=Scheme, opts=Opts}=_Uri|Rest]) ->
    Type0 = case lists:keyfind(<<"transport">>, 1, Opts) of
                {_,<<"ws">>} -> webrtc;
                {_,<<"WS">>} -> webrtc;
                {_,<<"wss">>} -> webrtc;
                {_,<<"WSS">>} -> webrtc;
                {_,<<"tls">>} -> srtp;
                {_,<<"TLS">>} -> srtp;
                _ when Scheme==sip -> rtp;
                _ when Scheme==sips -> srtp;
                _ -> build_term_type(Rest)
            end,
    case Type0 of
        srtp ->
            % for TLS uris could be used both rtp(AVP) and srtp(SAVP). Russia restricts using srtp, so use only rtp by default
            case ?SIPSTORE:find_u('use_srtp') of
                {_,true} -> srtp;
                _ -> rtp
            end;
        T -> T
    end;
build_term_type(#sdp{medias=Ms}) ->
    case lists:foldl(fun(#sdp_m{media= <<"audio">>}=M, _) -> M;
                        (_, Acc) -> Acc end, false, Ms) of
        false -> {error, invalid_sdp_audio};
        #sdp_m{proto= <<"RTP/AVP">>} -> rtp;
        #sdp_m{proto= <<"RTP/SAVP">>} -> srtp;
        #sdp_m{proto= <<"RTP/SAVPF">>} -> webrtc;
        #sdp_m{proto= <<"UDP/TLS/RTP/SAVPF">>} -> webrtc
    end.
build_term_type(Contacts, undefined) -> build_term_type(Contacts);
build_term_type([], #sdp{}=Sdp) -> build_term_type(Sdp);
build_term_type(Contacts, Sdp) ->
    case build_term_type(Sdp) of
        {error,_} -> build_term_type(Contacts);
        T when is_atom(T) -> T
    end.

% ---------------------------------------
% prepare term template for megaco query.

% makes ivr term with no mediastreams
% iface/rtp/$, iface/webrtc/$,
prepare_term_template(X)
  when X==ivr; X==ivrp; X==ivrr; X==t38; X==t30 ->
    prepare_term_template({"tgw", X});
prepare_term_template({undefined,X})
  when X==ivr; X==ivrp; X==ivrr; X==t38; X==t30 ->
    prepare_term_template(X);
prepare_term_template({IFACE,X})
  when X==ivr; X==ivrp; X==ivrr; X==t38; X==t30 ->
    IF = ?EU:to_list(IFACE),
    {X, IF ++ "/" ++ atom_to_list(X) ++ "/$",
     [{iface, IF}]}.

% makes SDP local template by SDP (Opposite remote incoming or generated by B2BUA).
prepare_term_template(X, #sdp{}=BaseSdp)
  when X==rtp; X==webrtc; X==srtp -> prepare_term_template({"tgw",X}, BaseSdp);
prepare_term_template({undefined,X}, #sdp{}=BaseSdp)
  when X==rtp; X==webrtc; X==srtp -> prepare_term_template(X, BaseSdp);
prepare_term_template({IFACE,X}, #sdp{}=BaseSdp)
  when X==rtp; X==webrtc; X==srtp ->
    IF = ?EU:to_list(IFACE),
    {X, IF ++ "/" ++ atom_to_list(X) ++ "/$",
     [{iface, IF},
      {localsdp, ?D_SDP:prepare_descr_template(sdp_change_proto(BaseSdp, get_term_proto(X)))}]}.

% makes SDP local template by Base term's local sdp
prepare_term_template(X, #sdp{}=BaseSdp, #sdp{}=RSdp)
  when X==rtp; X==webrtc; X==srtp -> prepare_term_template({"tgw",X}, BaseSdp, RSdp);
prepare_term_template({undefined,X}, #sdp{}=BaseSdp, #sdp{}=RSdp)
  when X==rtp; X==webrtc; X==srtp -> prepare_term_template(X, BaseSdp, RSdp);
prepare_term_template({IFACE,X}, #sdp{}=BaseSdp, #sdp{}=RSdp)
  when X==rtp; X==webrtc; X==srtp ->
    IF = ?EU:to_list(IFACE),
    {X, IF ++ "/" ++ atom_to_list(X) ++ "/$",
     [{iface, IF},
      {localsdp, ?D_SDP:prepare_descr_template(sdp_change_proto(BaseSdp, get_term_proto(X)))},
      {remotesdp, ?D_SDP:prepare_descr_direct(RSdp)}]}.

% makes SDP local template by Base term's local sdp
% used only in migration (fsm state - dialog, migration).
%   No forking, so no need to be ready to find term in callee list.
%   So no need to insert into options 'iface' and 'callid'.
prepare_term_template_by_previous({X,_,TOpts}=BaseTerm, [_|_]=BaseOpts, AllowTranscoding)
  when X==rtp; X==webrtc; X==srtp; X==ivrp; X==ivrr; X==t38; X==t30 ->
    IF = case lists:keyfind(iface, 1, TOpts) of
             false -> "tgw";
             {_,IFACE} -> ?EU:to_list(IFACE)
         end,
    {RSdpD, LSdpD} = {get_term_remotesdp(BaseTerm), get_term_localsdp(BaseTerm)},
    [LSdpO,RSdp,LBaseSdp] = ?EU:extract_required_props(['sdp_o','sdp_r','sdp_l_base'], BaseOpts),
    LSdpO1 = erlang:setelement(3, LSdpO, erlang:element(3,LSdpO)+1),
    LSdp = ?D_SDP:make_sdp(LSdpD, LSdpO1),
    LSdp1 = ?M_SDP:transcode_offer(AllowTranscoding, sdp_change_proto(LSdp, get_term_proto(X))),
    TermTemplate =
        {X, IF ++ "/" ++ atom_to_list(X) ++ "/$",
           [{localsdp, ?D_SDP:prepare_descr_template(LSdp1)},
            {remotesdp, RSdpD}]},
    Opts = [{'term_type', X},
            {'sdp_r', RSdp},
            {'sdp_o', LSdpO1},
            {'sdp_l_base', LBaseSdp}],
    {TermTemplate, Opts}.

% -------------------------------
% returns type of term
get_term_type(Term) ->
    erlang:element(1,Term).

%% --------------------------------------------------
%% returns term from list of corresponding type (used in callee's answer)
%% --------------------------------------------------
-spec find_term_by_type({TermType::rtp|webrtc|srtp, Alias::binary()|string(), CallId::binary()}, Terms::[tuple()]) -> tuple() | false.
%% --------------------------------------------------
find_term_by_type(_, [])-> false;
find_term_by_type({TermType,_Alias,_CallId}=K, [{Type,_,_}=Term|Rest]) when Type==TermType ->
    case is_term_equal(K,Term) of
        true -> Term;
        false -> find_term_by_type(K, Rest)
    end;
find_term_by_type(K, [_|Rest])->
    find_term_by_type(K, Rest).

%% --------------------------------------------------
%% store term to list of corresponding type (used in callee's answer)
%% --------------------------------------------------
-spec update_term_by_type(Terms::[tuple()], Term::tuple) -> [tuple()].
%% --------------------------------------------------
update_term_by_type(Terms, {TermType,_,Opts}=Term) ->
    case {lists:keyfind(iface,1,Opts), lists:keyfind(callid,1,Opts)} of
        {R1,R2} when R1==false; R2==false -> [Term|Terms];
        {{iface,Alias},{callid,CallId}} ->
            case find_term_by_type({TermType,Alias,CallId}, Terms) of
                false -> [Term|Terms];
                XTerm -> [Term|lists:delete(XTerm,Terms)]
            end end.

%% ------------
%% @private
%% ------------
-spec is_term_equal({TermType::rtp|webrtc|srtp, Alias::binary()|string(), CallId::binary()}, Term::tuple()) -> boolean().
%% ------------
is_term_equal({TermType, Alias, CallId}, {TermType,_,Opts}) ->
    CombineForks = combine_forks_in_term(),
    AliasL = ?EU:to_list(Alias),
    case {lists:keyfind(iface,1,Opts), lists:keyfind(callid,1,Opts)} of
        {{iface,AliasL},_} when CombineForks -> true;
        {{iface,AliasL},{callid,CallId}} -> true;
        _ -> false
    end;
is_term_equal(_,_) -> false.

%% @private
%% when true - then one term could correspond to many forks at the same time.
%% when false - then every fork has its own term in context.
combine_forks_in_term() -> false.

% -------------------------------
% make term filtered option (when no need to modify full props)
prepare_term_opts({Type,Id,Opts}=_Term, ROpts) ->
    F = fun({K,_V}) -> lists:member(K,ROpts) end,
    {Type,Id,lists:filter(F, Opts)}.

% -------------------------------
% make merged term after mg response of modify req (update opts, cause request was not full optioned)
%  for example on adding previous remotesdp to only localsdp result (term id of request is defined)
merge_terms_opts_mod(TermsUp, TermsFull) ->
    [case lists:keyfind(Id,2,TermsUp) of
         false -> Term;
         {_Type,_Id,OptsUp} ->
             MergedOpts = lists:ukeymerge(1, lists:sort(OptsUp), lists:sort(OptsFull)),
             {Type,Id, MergedOpts}
     end || {Type,Id,OptsFull}=Term <- TermsFull].

% make merged term after mg response (update opts, cause mg not response immutable remote sdp)
%  when adding terms with remote sdp, MG doesn't pull it back
merge_terms_opts_add(TermsUp, TermsFull) ->
    merge_terms_opts_add_1(TermsUp, TermsFull, []).

% @private
merge_terms_opts_add_1([], [], Res) -> lists:reverse(Res);
merge_terms_opts_add_1([{Type,Id,OptsUp}|RestUp], [{Type,_,OptsFull}|RestFull], Res) ->
    MergedOpts = lists:ukeymerge(1, lists:sort(OptsUp), lists:sort(OptsFull)),
    merge_terms_opts_add_1(RestUp, RestFull, [{Type,Id, MergedOpts}|Res]).


% mg response could skip some important sdp parameters, such as "a=sendrecv", "t=0 0"
%    so after mg we need to fill response sdp with skipped.
merge_terms_sdp_opts(TermsOut, TermsIn) ->
    merge_terms_sdp_opts(TermsOut, TermsIn, []).

% @private
merge_terms_sdp_opts([],_,Done) -> lists:reverse(Done);
merge_terms_sdp_opts([TOut|RestOut], [TIn|RestIn], Done) ->
    TOut1 = merge_terms_sdp_opts_1(TOut,TIn, localsdp, fun set_term_localsdp/2),
    TOut2 = merge_terms_sdp_opts_1(TOut1,TIn, remotesdp, fun set_term_remotesdp/2),
    merge_terms_sdp_opts(RestOut, RestIn, [TOut2|Done]).
% @private
merge_terms_sdp_opts_1({_,_,OOut}=TOut, {_,_,OIn}=_TIn, OptName, FSet) ->
    case lists:keyfind(OptName, 1, OOut) of
        false -> TOut;
        {_,SdpOutD} ->
            {_,SdpInD} = lists:keyfind(OptName, 1, OIn),
            SdpOutD1 = ?D_SDP:merge_sdp_after_mg(SdpOutD, SdpInD),
            FSet(TOut,SdpOutD1)
    end.

% -------------------------------
% returns list of termid by list of terms
%
get_terms_id(Terms) when is_list(Terms) ->
    lists:map(fun(Term) -> get_term_id(Term) end, Terms).

% returns termid of term
get_term_id({_Key,TermId,_}=_Term) -> TermId.

% -------------------------------
% get/set term's properties
% -------------------------------
get_term_localsdp({_,_,Opts}=_Term) ->
    case lists:keyfind(localsdp,1,Opts) of
        false -> undefined;
        {_, LocalSdp} -> LocalSdp
    end.

set_term_localsdp({Type,Id,Opts}=_Term, Sdp) ->
    Opts1 = lists:keystore(localsdp, 1, Opts, {localsdp,Sdp}),
    {Type,Id,Opts1}.

%% ----------------------------------------------
get_term_remotesdp({_,_,Opts}=_Term) ->
    case lists:keyfind(remotesdp,1,Opts) of
        false -> undefined;
        {_, RemoteSdp} -> RemoteSdp
    end.

set_term_remotesdp({Type,Id,Opts}=_Term, Sdp) ->
    Opts1 = lists:keystore(remotesdp, 1, Opts, {remotesdp,Sdp}),
    {Type,Id,Opts1}.

%% ----------------------------------------------
set_term_sdp(Term, LSdp, RSdp) ->
    set_term_localsdp(set_term_remotesdp(Term,RSdp),LSdp).

%% ----------------------------------------------
set_term_state_props({Type,Id,Opts}=_Term, TermProps) when is_list(TermProps) ->
    Opts1 = lists:keystore(termstate, 1, Opts, {termstate, [{props, TermProps}]}),
    {Type,Id,Opts1}.

%% ----------------------------------------------
set_term_mode({Type,Id,Opts}=_Term, Mode, ModeOnly) when is_binary(Mode), is_boolean(ModeOnly) ->
    Opts1 = lists:keystore(mode, 1, Opts, {mode,Mode}),
    Opts2 = lists:keystore(modeonly, 1, Opts1, {modeonly,ModeOnly}),
    {Type,Id,Opts2}.

%% ----------------------------------------------
%% to juxtapose forks and terms
set_term_callid({Type,Id,Opts}=Term, CallId) ->
    case combine_forks_in_term() of
        true -> Term;
        false ->
            Opts1 = lists:keystore(callid, 1, Opts, {callid,CallId}),
            {Type,Id,Opts1}
    end.

%% ----------------------------------------------
%% set opts for ivr player (file/files/dir, startat,stopat, loop,random, volume)
%% ----------------------------------------------
set_term_ivr_play({ivrp,_,_}=Term, #{}=P) ->
    Opts = [],
    Opts0 = case ?EU:get_by_key(mode,P,undefined) of
                Mode when is_list(Mode); is_binary(Mode) -> [{"mode", ?EU:to_list(Mode)} | Opts];
                _ -> Opts end,
    Opts1 = case ?EU:get_by_key(file,P,undefined) of
                File when is_list(File); is_binary(File) -> [{"file", ?EU:to_unicode_list(File)} | Opts0];
                _ -> Opts0 end,
    Opts2 = case ?EU:get_by_key(files,P,undefined) of
                Files when is_list(Files) -> [{"files", string:join(lists:map(fun(File) -> ?EU:to_unicode_list(File) end, Files), ";")} | Opts1];
                _ -> Opts1 end,
    Opts3 = case ?EU:get_by_key(dir,P,undefined) of
                Dir when is_list(Dir); is_binary(Dir) -> [{"dir", ?EU:to_unicode_list(Dir)} | Opts2];
                _ -> Opts2 end,
    Opts4 = case ?EU:get_by_key(startat,P,undefined) of
                I1 when is_integer(I1), I1 > 0 -> [{"startat", I1} | Opts3];
                _ -> Opts3 end,
    Opts5 = case ?EU:get_by_key(stopat,P,undefined) of
                I2 when is_integer(I2), I2 > 0 -> [{"stopat", I2} | Opts4];
                _ -> Opts4 end,
    Opts6 = case ?EU:get_by_key(loop,P,undefined) of
                B3 when is_boolean(B3) -> [{"loop", ?EU:to_int(B3)} | Opts5];
                B3 when is_integer(B3), B3 > 0 -> [{"loop", ?EU:to_int(?EU:to_bool(B3))} | Opts5];
                _ -> Opts5 end,
    Opts7 = case ?EU:get_by_key(random,P,undefined) of
                B4 when is_boolean(B4) -> [{"random", ?EU:to_int(B4)} | Opts6];
                B4 when is_integer(B4), B4 > 0 -> [{"random", ?EU:to_int(?EU:to_bool(B4))} | Opts6];
                _ -> Opts6 end,
    Opts8 = case ?EU:get_by_key(volume,P,undefined) of
                I3 when is_integer(I3), I3 > 0 -> [{"volume", I3} | Opts7];
                _ -> Opts7 end,
    set_term_state_props(Term, lists:reverse(Opts8)).

%% ----------------------------------------------
%% set opts for ivr recorder (file, length)
%%   type : rtp | raw
%%   buffer_duration : in raw mode setup period in ms of writing to disk
%%   codec_name : L16/8000 | PCMU/8000 | ...
%% ----------------------------------------------
set_term_ivr_rec({ivrr,_,_}=Term, #{}=P) ->
    Term1 = case ?EU:get_by_key(codec_name,P,undefined) of
                undefined -> Term;
                CodecName ->
                    % emulation of sdp to setup codec
                    case ?M_SDP:get_payload(<<"audio">>, ?EU:to_binary(CodecName)) of
                        {ok,Payload} ->
                            LSdp = [{"m",?EU:str("audio 0 RTP/AVP ~s", [Payload])},
                                    {"a",?EU:str("rtpmap:~s ~s", [Payload, CodecName])},
                                    {"a","sendonly"}],
                            set_term_localsdp(Term,LSdp);
                        false -> Term
                    end end,
    Opts = [{"file", ?EU:to_list(?EU:get_by_key(file,P))}],
    Opts0 = case ?EU:get_by_key(type,P,undefined) of
                Type when is_list(Type); is_binary(Type) -> [{"type", ?EU:to_list(Type)} | Opts];
                _ -> Opts end,
    Opts1 = case ?EU:get_by_key(buffer_duration,P,undefined) of
                BDur when is_integer(BDur), BDur > 0 -> [{"buffer_duration", BDur} | Opts0];
                _ -> Opts0
            end,
    set_term_state_props(Term1, lists:reverse(Opts1)).

%% ----------------------------------------------
%% set opts for fax (path, mode)
%% ----------------------------------------------
set_term_fax({Type,_,_}=Term, #{}=P) when Type==t38; Type==t30 ->
    Opts0 = [{"mode", ?EU:to_list(?EU:get_by_key(mode,P))}],
    Opts1 = [{"path", ?EU:to_unicode_list(?EU:get_by_key(path,P))} | Opts0],
    set_term_state_props(Term, lists:reverse(Opts1)).

%% ----------------------------------------------
%% set opts for any term, rtp and others ()
%% Events :: [{RequestId::integer(), [{PkgName::string(),[{ParamKey::string(),Value::term()}] }] }]
%% Ex. add [{1234, [{"vdp/vad", [{"vthres",40}] }] }]
%% Ex. del [{1234, []}]
%% TODO could be merged with already saved
%% ----------------------------------------------
set_term_events({Type,Id,Opts}=_Term, {RequestId,Events}=Ev) when is_integer(RequestId), is_list(Events) ->
    Opts1 = lists:keystore(events, 1, Opts, {events, Ev}),
    {Type,Id,Opts1}.

%% ====================================================================
%% Internal functions
%% ====================================================================

% gets sdp proto by term type
get_term_proto(rtp) -> <<"RTP/AVP">>;
get_term_proto(srtp) -> <<"RTP/SAVP">>;
get_term_proto(webrtc) -> <<"RTP/SAVPF">>.

% changes proto in m= of sdp
sdp_change_proto(#sdp{medias=Ms}=Sdp, NewProto) ->
    Sdp#sdp{medias=[M#sdp_m{proto=NewProto} || #sdp_m{}=M <- Ms]}.
