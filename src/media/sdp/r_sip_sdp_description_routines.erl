%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Provides service to transform sip sdp to megaco sdp description and back

-module(r_sip_sdp_description_routines).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([prepare_descr_template/1,
         prepare_descr_direct/1,

         make_sdp/2,
         get_stream_name/0,

         filter_lines_before_mg/1,
         merge_sdp_after_mg/2]).

%-compile(export_all).

%% ====================================================================
%% Types
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_mgc.hrl").

-define(L(A), ?EU:to_list(A)).
-define(FLAT(A), lists:flatten(A)).

%% ====================================================================
%% API functions
%% ====================================================================

% makes sdp megaco template description list by #sdp{}
prepare_descr_template(#sdp{}=SDP) ->
    FWildA = fun(_) -> "$" end,
    FWildP = fun([$0]=X) -> X;
                ([$0,$/|_]=X) -> X;
                (_) -> "$"
             end,
    FFilterAttrs = fun(Attr) -> filter_template_media_attrs(Attr) == true end,
    prepare_descr(#sdp{}=SDP, FWildA, FWildP, FFilterAttrs).

% makes sdp description list by #sdp{}
prepare_descr_direct(#sdp{}=SDP) ->
    FNone = fun(X) -> X end,
    FFilterAttrs = fun(Attr) ->
                           case filter_template_media_attrs(Attr) of
                               true -> true;
                               remove -> true;
                               false -> false
                           end end,
    prepare_descr(#sdp{}=SDP, FNone, FNone, FFilterAttrs).

% build #sdp{} by sdp description list and Options tuple for "o="
make_sdp(SdpDescr, SdpO) when is_list(SdpDescr) ->
    FO = fun({_U,SID,SVSN,NTP,ATP,AD}) ->
                 SIDB = ?EU:to_binary(SID), SVSNB = ?EU:to_binary(SVSN),
                 NTPB = ?EU:to_binary(NTP), ATPB = ?EU:to_binary(ATP), ADB = ?U:addr_to_binary(AD),
                 <<(get_stream_name())/binary, 32, SIDB/binary, 32, SVSNB/binary, 32, NTPB/binary, 32, ATPB/binary, 32, ADB/binary>>
         end,
    F = fun({K,V}) ->
                KB = ?EU:to_binary(K), VB = ?EU:to_binary(V),
                case K of
                    "v" -> OB = FO(SdpO), <<KB/binary, "=", VB/binary, "\r\n", "o=", OB/binary, "\r\n">>;
                    _ -> <<KB/binary, "=", VB/binary, "\r\n">>
                end end,
    ?U:parse_sdp(list_to_binary([F(KvTup) || KvTup <- SdpDescr])).

% return stream name for sdp by current application
get_stream_name() ->
    case application:get_env(?APP,role) of
        undefined -> <<"era">>;
        {ok,sg} -> <<"era-g">>;
        {ok,b2bua} -> <<"era-b">>;
        {ok,esg} -> <<"era-e">>;
        {ok,conf} -> <<"era-c">>;
        {ok,ivr} -> <<"era-i">>;
        {ok,prompt} -> <<"era-p">>;
        _ -> <<"era">>
    end.

% filter useless sdp lines
filter_lines_before_mg(SdpDescr) ->
    lists:filter(fun({"s",_}) -> false;
                    ({"t",_}) -> false;
                    ({"o",_}) -> false;
                    (_) -> true
                 end, SdpDescr).

% merge out sdp with in sdp props (mg could skip some important values: "a=sendrecv", "t=0 0", ...)
merge_sdp_after_mg(SdpOutD, SdpInD) ->
    MsIn = parse_media_blocs(SdpInD),
    Fmix = fun({"m",V}=I, {M,L,Acc}) ->
                   Acc1 = case lists:keyfind(M,1,MsIn) of
                              false -> Acc;
                              {_,ItemsIn} ->
                                  FIn = fun({"c",_}, AccIn) -> AccIn;
                                           ({"m",_}, AccIn) -> AccIn;
                                           ({"a"=KIn,VIn}=IIn, AccIn) ->
                                                FW = ?EU:first_word(VIn),
                                                case lists:member({KIn,FW}, L) of
                                                    true -> AccIn;
                                                    false ->
                                                        [AKey|_] = binary:split(?EU:to_binary(FW), <<":">>),
                                                        case filter_template_media_attrs(AKey) of
                                                            false -> [IIn|AccIn];
                                                            true -> AccIn;
                                                            remove -> AccIn
                                                        end
                                                end;
                                           (IIn, AccIn) ->
                                                case lists:member(IIn,L) of
                                                    true -> AccIn;
                                                    false -> [IIn|AccIn]
                                                end
                                        end,
                                  lists:foldl(FIn, [], ItemsIn) ++ Acc
                          end,
                   M1 = ?EU:first_word(V),
                   {M1,[I,{"m",?EU:first_word(V)}],[I|Acc1]};
              ({K,V}=I, {M,L,Acc}) when K=="a" -> {M,[I,{K,?EU:first_word(V)}|L],[I|Acc]};
              (I, {M,L,Acc}) -> {M,[I|L],[I|Acc]}
           end,
    {_,_,Res} = lists:foldl(Fmix,{common,[],[]},SdpOutD++[{"m","fin"}]),
    Fsort = fun({"m",_}=I,{ToSort,Acc}) -> {[I],[lists:sort(fun sort_sdp_props/2, lists:reverse(ToSort))|Acc]};
               (I,{ToSort,Acc}) -> {[I|ToSort],Acc}
            end,
    {_,Res1} = lists:foldl(Fsort, {[],[]}, lists:reverse(Res)),
    lists:flatten(lists:reverse(Res1)).

%% ====================================================================
%% Internal functions
%% ====================================================================

parse_media_blocs(SdpD) ->
    FMs = fun({"m",V}=I, {CurM,CurOpts,Acc}) ->
                  M = ?EU:first_word(V),
                  {M,[I],[{CurM,lists:reverse(CurOpts)}|Acc]};
             (I, {CurM,CurOpts,Acc}) -> {CurM, [I|CurOpts], Acc}
          end,
    {LastM,LastOpts,Res} = lists:foldl(FMs, {common,[],[]}, SdpD),
    [{LastM,lists:reverse(LastOpts)}|Res].


%% --------------------------------------------------
%% @private
%% FWildA - function of change Address strings into template mask
%% FWildP - function of change Port strings into template mask
%% FWildX - function of change
%% --------------------------------------------------
prepare_descr(#sdp{}=SDP, FWildA, FWildP, FFilterAttrs) ->
    ?FLAT([
         % v=
             {"v", ?L(SDP#sdp.sdp_vsn)},
         % s=
             {"s", ?L(SDP#sdp.session)},
        % i=
            case SDP#sdp.info of
                undefined -> [];
                MI -> {"i", MI}
            end,
        % c=
             case SDP#sdp.connect of
                undefined -> [];
                {CType, CAddrType, CAddress} -> {"c", ?FLAT([?L(CType), 32, ?L(CAddrType), 32, FWildA(?L(CAddress))])}
            end,
        % b=
            [{"b", ?L(B)} || B <- SDP#sdp.bandwidth],
         % t=
             [[
                 {"t", ?FLAT([?L(integer_to_list(Start)), 32, ?L(integer_to_list(Stop))])},
                 [{"r", R} || R <- Reps]
              ] || {Start, Stop, Reps} <- SDP#sdp.time],
        % a=
            [
                case Values of
                            [] -> {"a", ?L(Attr)};
                            _ -> {"a", ?FLAT([?L(Attr), $:, join_list(Values)])}
                        end
                || {Attr, Values} <- SDP#sdp.attributes, FFilterAttrs(Attr)
            ],
        % group m=, a=
            [[
                  % m=
                {"m", ?FLAT([?L(Media#sdp_m.media), 32, FWildP(?FLAT([integer_to_list(Media#sdp_m.port),
                                                                     case Media#sdp_m.nports of
                                                                        1 -> [];
                                                                        NPorts -> [$/, integer_to_list(NPorts)]
                                                                     end])),
                            32, ?L(Media#sdp_m.proto),
                            case Media#sdp_m.fmt of
                                [] -> [];
                                Fmt -> [32, join_list(Fmt)]
                            end])},
                % i=
                    case Media#sdp_m.info of
                        undefined -> [];
                        MI -> {"i", MI}
                    end,
                % c=
                    case Media#sdp_m.connect of
                        undefined -> [];
                        {MType, MAddrType, MAddress} -> {"c", ?FLAT([?L(MType), 32, ?L(MAddrType), 32, FWildA(?L(MAddress))])}
                    end,
                % b=
                    [{"b", ?L(MB)} || MB <- Media#sdp_m.bandwidth],
                % a=
                    [
                         case MValues of
                            [] -> {"a", ?L(MAttr)};
                            _ -> {"a", ?FLAT([?L(MAttr), $:, join_list(MValues)])}
                        end
                        || {MAttr, MValues} <- defaultize_mediamode(Media#sdp_m.attributes), FFilterAttrs(MAttr)
                    ]
             ] || Media <- SDP#sdp.medias]
    ]).

%% @private
join_list([Term]) -> ?L(Term);
join_list([First|Rest]) -> join_list(Rest, [?L(First)]).

%% @private
join_list([Next|Rest], Acc) -> join_list(Rest, [?L(Next), 32 | Acc]);
join_list([], Acc) -> lists:reverse(Acc).

%% @private
defaultize_mediamode(MAttrs) ->
    F = fun(_, true) -> true;
           ({<<"sendrecv">>,_}, false) -> true;
           ({<<"sendonly">>,_}, false) -> true;
           ({<<"recvonly">>,_}, false) -> true;
           ({<<"inactive">>,_}, false) -> true;
           (_, false) -> false
        end,
    case lists:foldl(F, false, MAttrs) of
        true -> MAttrs;
        false -> lists:reverse([{<<"sendrecv">>,[]}|lists:reverse(MAttrs)])
    end.

%% @private
%% when preparing sdp for mg - filter attributes if false
%% when mg returns sdp - append attribute from original sdp to response sdp if false
%% ---------------------------------------------
-spec filter_template_media_attrs(Attr::binary()) -> true | false | remove.
%% ---------------------------------------------
filter_template_media_attrs(<<"sendrecv">>) -> true;
filter_template_media_attrs(<<"sendonly">>) -> true;
filter_template_media_attrs(<<"recvonly">>) -> true;
filter_template_media_attrs(<<"inactive">>) -> true;
filter_template_media_attrs(<<"crypto">>) -> true;
%%
filter_template_media_attrs(<<"rtpmap">>) -> true;
filter_template_media_attrs(<<"fmtp">>) -> true;
filter_template_media_attrs(<<"maxptime">>) -> true;
filter_template_media_attrs(<<"ptime">>) -> true;

%% bgmg's RP-1058
filter_template_media_attrs(<<"rtcp">>) -> remove;
filter_template_media_attrs(<<"rtcp-mux">>) -> remove;
filter_template_media_attrs(<<"rtcp-fb">>) -> remove;
filter_template_media_attrs(<<"rtcp-rsize">>) -> remove;

%% custom
filter_template_media_attrs(<<"no_transcode">>) -> true;

%% for webrtc
filter_template_media_attrs(<<"ssrc">>) -> true;
filter_template_media_attrs(<<"ice-options">>) -> true;
filter_template_media_attrs(<<"candidate">>) -> true;
filter_template_media_attrs(<<"fingerprint">>) -> true;
filter_template_media_attrs(<<"ice-ufrag">>) -> true;
filter_template_media_attrs(<<"ice-pwd">>) -> true;
%% filter_template_media_attrs(<<"ice-fingerprint">>) -> false; % @george
%% filter_template_media_attrs(<<"ice-setup">>) -> false; % @george
filter_template_media_attrs(<<"mid">>) -> true;
filter_template_media_attrs(<<"setup">>) -> true;
filter_template_media_attrs(<<"extmap">>) -> true;
filter_template_media_attrs(<<"group">>) -> true;
filter_template_media_attrs(<<"msid-semantic">>) -> true;
%%
filter_template_media_attrs(<<"orient">>) -> true;
filter_template_media_attrs(<<"type">>) -> true;
filter_template_media_attrs(<<"charset">>) -> true;
filter_template_media_attrs(<<"cat">>) -> true;
filter_template_media_attrs(<<"lang">>) -> true;
filter_template_media_attrs(<<"sdplang">>) -> true;
filter_template_media_attrs(<<"keywds">>) -> true;
filter_template_media_attrs(<<"tool">>) -> true;
filter_template_media_attrs(<<"framerate">>) -> true;
filter_template_media_attrs(<<"quality">>) -> true;
%%
filter_template_media_attrs(<<"T38",_/bitstring>>) -> true;
%%
filter_template_media_attrs(_) -> false.


%% @private
%% sort sdp props by priority
sort_sdp_props({X,_},{Y,_}) -> sdp_idx(X) =< sdp_idx(Y).

%% @private
%% sdp props priority
sdp_idx("m") -> 100;
sdp_idx("v") -> 200;
sdp_idx("o") -> 300;
sdp_idx("s") -> 400;
sdp_idx("i") -> 500;
sdp_idx("u") -> 600;
sdp_idx("e") -> 700;
sdp_idx("p") -> 800;
sdp_idx("c") -> 900;
sdp_idx("b") -> 1000;
sdp_idx("t") -> 1100;
sdp_idx("z") -> 1200;
sdp_idx("k") -> 1300;
sdp_idx("a") -> 1400;
sdp_idx(_) -> 1500.
