%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Provides services to operate with media of sip sdp.
%%%         Modifies local sdp (template). enlarges by known audio formats, reduces by unknown formats and offer formats.
%%%         So b2bua can connect by transcoding.
%%%         Added formats are placed in the end of format list (to save priority expectations and secondly prioritize media without transcoding).
%%%         If some dynamic payload doesn't match, then known payload is beeing modified to satisfy offer. Response sdp should satisfy modified itself.
%%%         So, SDP:A -> B2BUA -> SDP:A++known--unknown, SDP:B -> B2BUA -> SDP:B++known--(SDP:A-SDP:B)

-module(r_sip_sdp_media_routines).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([check_known_fmt/1,

         transcode_offer/2, transcode_offer/3,
         transcode_answer/3,
         lo/1, lo/2, lo_reduce/2,
         update_by_local_media_fmts/2,
         filter_by_local_media_fmts/2,
    
         get_payload/2,

         setmode/2,
         sendrecv/1,
         sendonly/1,
         recvonly/1,
         inactive/1,
         reverse_stream_mode/1,
         get_audio_mode/1,
         get_video_mode/1,
         get_image_mode/1,
         get_stream_mode/2,
         build_media_mode/1,
         build_media_mode/2,
         limit_media_mode/2,
         limit_media_mode_nochange/2,
         check_sendrecv/1,

         check_t38/1,
         check_t30/1,
         check_audio/1,
         update_by_t38/1,
         update_by_t30/1,

         unzero_ip/1,

         check_rfc2833/1,
         dtmf_rfc2833_to_info/1,

         make_options_sdp/0,
         make_offer_sdp/0,
         set_sess/1]).

%% ====================================================================
%% Test
%% ====================================================================

-compile([export_all,nowarn_export_all]).

-ifdef(TEST).
-compile([export_all,nowarn_export_all]).
-define(T_SIPSTORE, (list_to_atom(?MODULE_STRING++"_test"))).
-define(T_GetPayloads(Subj,MediaN), ((list_to_atom(?MODULE_STRING++"_test")):get_payloads(Subj,MediaN))).
-define(T_UseVideoTranscoding, ((list_to_atom(?MODULE_STRING++"_test")):is_use_video_transcoding())).
-define(T_GetIpAddr, <<"1.1.1.1">>).
-define(T_GetStreamName, <<"TestStream">>).

-else.
-define(T_SIPSTORE, ?SIPSTORE).
-define(T_GetPayloads(Subj,MediaN), ?U:get_payloads(Subj,MediaN)).
%-define(T_GetPayloads(Subj,MediaN), []).
-define(T_UseVideoTranscoding, ?U:is_use_video_transcoding()).
-define(T_GetIpAddr, ?U:addr_to_binary(?U:get_host_addr())).
-define(T_GetStreamName, ?D_SDP:get_stream_name()).
-endif.

%% ====================================================================
%% Types
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_mgc.hrl").

-type fmt() :: {PL::binary(),Name::binary()}.
-type fmtdescr() :: {PL::binary(),Name::binary(),Fmt::binary(),Fun::function()}.
-type attr() :: {AttrName::binary(),[AttrOpt::binary()]}.

-define(DynamicPL, 77). % unassigned
-define(DynamicFrom, 96). % dynamic
-define(DynamicTo, 127). % dynamic
% -define(DynamicPL, 96). % dynamic

-define(AttrNamesPL(AN), AN == <<"fmtp">>; AN == <<"rtpmap">>; AN == <<"rtcp-fb">>).

%% ====================================================================
%% API functions
%% ====================================================================

%% --------------
%% checks if incoming remote sdp contains known formats (audio).
%%   makes satisfaction by dynamic payloads, then search for known formats.
%% --------------
check_known_fmt(#sdp{}=Sdp) ->
    check_known_fmt(<<"audio">>, Sdp).

%% --------------
%% check if sdp has known formats in selected media
%%   makes satisfaction by dynamic payloads, then search for known formats.
%% --------------
check_known_fmt(MediaN, #sdp{}=Sdp) ->
    case extract_media(MediaN, Sdp) of
        false -> true;
        #sdp_m{fmt=Fmt, attributes=Attr}=Media ->
            Known = get_known_formats_for(Media),
            Same = get_same(Fmt, Known),
            Fcheck = fun(P,false) ->
                             {_,R,_,Fun} = lists:keyfind(P, 1, Known),
                             IsGen = case ?EU:to_int(P)>=?DynamicPL of
                                         true -> not lists:member(R, known_no_general(dynamic,MediaN));
                                         false -> not lists:member(P, known_no_general(static,MediaN))
                                     end,
                             case IsGen of
                                 false -> false;
                                 true -> Fun(get_attr(<<"fmtp">>,Attr,P))
                             end;
                        (_,true) -> true
                     end,
            lists:foldl(Fcheck, false, Same)
    end.

%% --------------
%% return payload for codec name
%% --------------
get_payload(Media,Name) ->
    Formats = known_extra(static,Media)++known_extra(dynamic,Media),
    case lists:keyfind(Name,2,Formats) of
        {Payload,_} -> {ok,Payload};
        false ->
            case lists:keyfind(?EU:uppercase(Name),2,[{K,?EU:uppercase(N)} || {K,N} <- Formats]) of
                {Payload,_} -> {ok,Payload};
                false -> false
            end end.

%% --------------
%% prepare transcoding offer sets
%% --------------
transcode_offer(false, #sdp{}=Sdp) -> append_attr(notranscode,[<<"audio">>,<<"video">>],Sdp);
transcode_offer(true, #sdp{medias=[]}=Sdp) -> Sdp;
transcode_offer(true, #sdp{medias=[#sdp_m{media= <<"audio">>}]}=Sdp) -> lo(Sdp);
transcode_offer(true, #sdp{medias=Ms}=Sdp) ->
    case lists:keyfind(<<"video">>, #sdp_m.media, Ms) of
        false -> transcode_offer(audio, Sdp);
        #sdp_m{} ->
            case ?T_UseVideoTranscoding andalso check_known_fmt(<<"video">>, Sdp) of
                false -> transcode_offer(audio, append_attr(notranscode, [<<"video">>], Sdp));
                true -> lo(Sdp)
            end end;
transcode_offer(audio, #sdp{medias=Ms}=Sdp) ->
    case lists:keytake(<<"audio">>, #sdp_m.media, Ms) of
        false -> Sdp;
        {value,#sdp_m{}=Audio,MsRest} ->
            #sdp{medias=[Audio1]}=Sdp1 = lo(Sdp#sdp{medias=[Audio]}),
            Sdp1#sdp{medias=[Audio1|MsRest]}
    end.

%%
transcode_offer(M,SdpRFrom,SdpRTo) -> transcode_answer(M,SdpRFrom,SdpRTo).

%% --------------
%% prepare transcoding answer sets
%% --------------
transcode_answer(false, ResponseSdp, _OfferSdp) -> append_attr(notranscode,[<<"audio">>,<<"video">>],ResponseSdp);
transcode_answer(true, #sdp{medias=[]}=ResponseSdp, _OfferSdp) -> ResponseSdp;
transcode_answer(true, #sdp{medias=[#sdp_m{media= <<"audio">>}]}=ResponseSdp, OfferSdp) -> lo_reduce(ResponseSdp, OfferSdp);
transcode_answer(true, #sdp{medias=Ms}=ResponseSdp, OfferSdp) ->
    case lists:keyfind(<<"video">>, #sdp_m.media, Ms) of
        false -> transcode_answer(audio, ResponseSdp, OfferSdp);
        #sdp_m{} ->
            case ?T_UseVideoTranscoding andalso check_known_fmt(<<"video">>, ResponseSdp) of
                false -> transcode_answer(audio, append_attr(notranscode,[<<"video">>],ResponseSdp), OfferSdp);
                true -> lo_reduce(ResponseSdp, OfferSdp)
            end end;
transcode_answer(audio, #sdp{medias=Ms}=ResponseSdp, OfferSdp) ->
    case lists:keytake(<<"audio">>, #sdp_m.media, Ms) of
        false -> ResponseSdp;
        {value,#sdp_m{}=Audio,MsRest} ->
            #sdp{medias=[Audio1]}=Sdp1 = lo_reduce(ResponseSdp#sdp{medias=[Audio]}, OfferSdp),
            Sdp1#sdp{medias=[Audio1|MsRest]}
    end.

%% --------------
%% @private
%% append to media attributes of selected streams option of no transcoding
%% --------------
append_attr(notranscode, MediaNames, #sdp{medias=Ms}=Sdp) ->
    Sdp#sdp{medias=lists:map(fun(#sdp_m{media=M,attributes=As}=SdpM) ->
                                     case not lists:member(M,MediaNames) orelse lists:keyfind(<<"no_transcode">>,1,As) of
                                         false -> SdpM#sdp_m{attributes=As++[{<<"no_transcode">>,[]}]};
                                         _ -> SdpM
                                     end end, Ms)}.

%% ------------------------------------------------------
%% localizes sdp by known formats (audio)
%%

%% --------------
%% extend offer sdp to known formats
%% --------------
lo(OfferSdp) ->
    update_by_local_media_fmts(OfferSdp, undefined).

%% --------------
%% extend response sdp to known, reduce to offer sdp (for answer)
%%  multi codec set
%% --------------
lo(ResponseSdp, OfferSdp) ->
    update_by_local_media_fmts(ResponseSdp, OfferSdp).

%% --------------
%% extend response sdp to known, reduce to offer sdp (for answer),
%%  choose only one for response, preferred top same in response and offer
%% --------------
lo_reduce(ResponseSdp, OfferSdp) ->
    % @todo
    Sdp = update_by_local_media_fmts(ResponseSdp, OfferSdp),
    Key = {allow_ac_set,self()},
    case ?T_SIPSTORE:find_t(Key) of
        false -> Sdp;
        {_,true} -> Sdp;
        {_,false} -> reduce_answer_to_one_general_fmt(Sdp,ResponseSdp,OfferSdp)
    end.

%% --------------
%% updates local sdp by known audio formats (should be enlarged by known formats and filtered by formats in OfferSdp)
%% --------------
update_by_local_media_fmts(#sdp{}=Sdp, OfferSdp) ->
    update_by_local_media_fmts(Sdp, OfferSdp, [add,del,up]).

%% --------------
%% filters local sdp by known audio formats and by formats in OfferSdp
%% --------------
filter_by_local_media_fmts(#sdp{}=Sdp, OfferSdp) ->
    update_by_local_media_fmts(Sdp, OfferSdp, [del,up]).

%% --------------
%% set up Sdp to contain a=sendrecv (deleting a=sendonly,recvonly,inactive)
%% --------------
setmode(#sdp{}=Sdp, Mode)
  when Mode == <<"sendrecv">>; Mode == <<"inactive">>; Mode == <<"sendonly">>; Mode == <<"recvonly">> ->
    do_ensure_stream_mode(Mode, Sdp).

%% --------------
sendrecv(#sdp{}=Sdp) ->
    do_ensure_stream_mode(<<"sendrecv">>, Sdp).

sendonly(#sdp{}=Sdp) ->
    do_ensure_stream_mode(<<"sendonly">>, Sdp).

recvonly(#sdp{}=Sdp) ->
    do_ensure_stream_mode(<<"recvonly">>, Sdp).

inactive(#sdp{}=Sdp) ->
    do_ensure_stream_mode(<<"inactive">>, Sdp).

%% --------------
reverse_stream_mode(#sdp{medias=Ms}=Sdp) ->
    do_reverse_stream_mode(Sdp, Ms, []);
reverse_stream_mode(Mode) when is_binary(Mode) ->
    do_reverse_stream_mode(Mode).

%% --------------
%% return stream mode from sdp
%% --------------
get_audio_mode(#sdp{}=Sdp) -> get_stream_mode(<<"audio">>,Sdp).
get_video_mode(#sdp{}=Sdp) -> get_stream_mode(<<"video">>,Sdp).
get_image_mode(#sdp{}=Sdp) -> get_stream_mode(<<"image">>,Sdp).

%%
get_stream_mode(MediaN, #sdp{medias=Ms}=_Sdp) ->
    case lists:keyfind(MediaN, #sdp_m.media, Ms) of
        false -> undefined;
        #sdp_m{attributes=As} ->
            F = fun({<<"sendrecv">> = M,_}, undefined) -> M;
                   ({<<"sendonly">> = M,_}, undefined) -> M;
                   ({<<"recvonly">> = M,_}, undefined) -> M;
                   ({<<"inactive">> = M,_}, undefined) -> M;
                   (_,Acc) -> Acc
                end,
            case lists:foldl(F, undefined, As) of
                undefined -> <<"sendrecv">>;
                M -> M
            end
    end.

%% --------------
%% build media stream's mode by limiting max (ex. if inactive(max.sendonly) -> inactive, if sendrecv(max recvonly) -> recvonly)
%% --------------
build_media_mode([Spk,Mic]) -> build_media_mode(Spk, Mic).
build_media_mode(Spk, Mic) ->
    case {?EU:to_bool(Spk),?EU:to_bool(Mic)} of
        {true,true} -> <<"sendrecv">>;
        {true,false} -> <<"recvonly">>;
        _ -> <<"inactive">>
    end.

%% --------------
%% build media stream's mode by limiting max (ex. if inactive(max.sendonly) -> inactive, if sendrecv(max recvonly) -> recvonly)
%% --------------
limit_media_mode(X,Max) ->
    case mode_idx(X) =< mode_idx(Max) of
        true -> X;
        _ -> Max
    end.

%% --------------
limit_media_mode_nochange(X,Max) ->
    case mode_idx(X) =< mode_idx(Max) of
        true -> undefined;
        _ -> Max
    end.

%% --------------
%% returns true if audio stream mode is sendrecv, otherwise false
%% --------------
check_sendrecv(#sdp{}=Sdp) ->
    get_audio_mode(Sdp) == <<"sendrecv">>.

%% --------------
%% @fax t38
%% returns true if image stream presents and port /= 0, false otherwise
%% --------------
check_t38(#sdp{medias=Ms}) ->
    case lists:keyfind(<<"image">>, #sdp_m.media, Ms) of
        #sdp_m{port=ImageP} -> ok;
        _ -> ImageP = 0
    end,
    case lists:keyfind(<<"audio">>, #sdp_m.media, Ms) of
        #sdp_m{port=AudioP} -> ok;
        _ -> AudioP = 0
    end,
    ImageP > 0 andalso AudioP == 0.

%% --------------
%% returns true if audio stream presents, port /= 0, and first codec is PCMU|PCMA, false otherwise
%% --------------
check_t30(#sdp{medias=Ms}) ->
    case lists:keyfind(<<"audio">>, #sdp_m.media, Ms) of
        #sdp_m{port=Port,fmt=[<<"0">>|_]} when Port>0 -> true;
        #sdp_m{port=Port,fmt=[<<"8">>|_]} when Port>0 -> true;
        _ -> false
    end.

%% --------------
%% returns true if audio stream presents and port /= 0, otherwise false
%% --------------
check_audio(#sdp{medias=Ms}) ->
    case lists:keyfind(<<"audio">>, #sdp_m.media, Ms) of
        #sdp_m{port=MPort}=_M -> MPort /= 0;
        _ -> false
    end.

%% --------------
%% @fax t38
%% update sdp by setting t38 image media stream and remove other streams (for reinvite)
%% --------------
update_by_t38(#sdp{}=Sdp) ->
    Sdp#sdp{medias=[#sdp_m{media= <<"image">>,
                           port= 99999,
                           proto= <<"udptl">>,
                           fmt= [<<"t38">>]}]}.

%% --------------
%% update sdp by adding g711 audio fmt payloads to top (for reinvite)
%% --------------
update_by_t30(#sdp{medias=Ms}=Sdp) ->
    case lists:keyfind(<<"audio">>, #sdp_m.media, Ms) of
        false -> Sdp#sdp{medias=[#sdp_m{media= <<"audio">>,
                                        port= 99999,
                                        proto= <<"RTP/AVP">>, % @todo
                                        fmt= [<<"0">>, <<"8">>]} ]}; %|Ms]};

        #sdp_m{fmt=Fmt}=A -> Sdp#sdp{medias=lists:keyreplace(<<"audio">>, #sdp_m.media, Ms, A#sdp_m{fmt=[<<"0">>,<<"8">>|Fmt--[<<"0">>,<<"8">>]]})}
    end.

% ------------------------------------------------------

%% --------------
%% RP-25
%% localize to local dummy ip/port if "IN IP4 0.0.0.0" found
%% --------------
unzero_ip(#sdp{address=A,connect=C,medias=Ms}=Sdp) ->
    Sdp#sdp{connect=localize_zero_ip(C),
            address=localize_zero_ip(A),
            medias=lists:map(fun(#sdp_m{connect={<<"IN">>,<<"IP4">>,<<"0.0.0.0">>}=MC}=M) ->
                                     M#sdp_m{connect=localize_zero_ip(MC),
                                             port=dummy_port()};
                                (M) -> M
                             end, Ms)}.
% @private
localize_zero_ip({<<"IN">>,<<"IP4">>,<<"0.0.0.0">>}) -> {<<"IN">>,<<"IP4">>,?EU:to_binary(inet:ntoa(?U:get_host_addr()))};
localize_zero_ip(C) -> C.
% @private
dummy_port() -> 65535.

%% --------------
%% returns true if audio stream has telephone-event fmt
%% --------------
check_rfc2833(#sdp{medias=Ms}) ->
    case lists:keyfind(<<"audio">>, #sdp_m.media, Ms) of
        #sdp_m{fmt=Fmt,attributes=Attrs} ->
            P = try lists:foldl(fun({<<"rtpmap">>, [P1,<<"telephone-event/8000">>|_]}, _) -> throw(P1); (_,Acc) -> Acc end, undefined, Attrs) catch throw:R -> R end,
            lists:member(P,Fmt);
        _ -> false
    end.

%% --------------
%% modify sign of dtmf from rfc2833(integer) to sip-info value(integer)
%% --------------
dtmf_rfc2833_to_info($*) -> "10";
dtmf_rfc2833_to_info($#) -> "11";
dtmf_rfc2833_to_info($A) -> "12";
dtmf_rfc2833_to_info($B) -> "13";
dtmf_rfc2833_to_info($C) -> "14";
dtmf_rfc2833_to_info($D) -> "15";
dtmf_rfc2833_to_info(A) when A>=$0, A=<$9 -> ?EU:to_list(?EU:to_int([A])).

%% ------------------------------------------------------
%% makes sdp for options
%% --------------
make_options_sdp() ->
    SDP = ?U:parse_sdp(<<"v=0\r\no=- 1000 1000 IN IP4 0.0.0.0\r\ns=-\r\nt=0 0\r\nm=audio 1 RTP/AVP 8\r\nc=IN IP4 0.0.0.0\r\na=rtpmap:0 PCMA/8000">>),
    #sdp{address={Pr1,Pr2,_},
         medias=[#sdp_m{}=M|_]}=SDP,
    % audio
    AuS = known_formats(static,<<"audio">>),
    AuD = known_formats(dynamic,<<"audio">>),
    Au = AuS++AuD,
    % video
    ViS = known_formats(static,<<"video">>),
    ViD = known_formats(dynamic,<<"video">>),
    Vi = ViS++ViD,
    %
    LocalAddr = {Pr1,Pr2,?T_GetIpAddr},
    MA = M#sdp_m{media= <<"audio">>,
                 fmt= [P || {P,_,_,_} <- Au],
                 connect= LocalAddr,
                 attributes= [{<<"rtpmap">>,[P,F]} || {P,F,_,_} <- Au]},
    MV = M#sdp_m{media= <<"video">>,
                 fmt= [P || {P,_,_,_} <- Vi],
                 connect= LocalAddr,
                 attributes= [{<<"rtpmap">>,[P,F]} || {P,F,_,_} <- Vi]},
    SDP#sdp{user= ?T_GetStreamName,
            address= LocalAddr,
            %connect= LocalAddr,
            medias= [MA,MV]}.

%% --------------
%% makes sdp for offer call
%% --------------
make_offer_sdp() -> make_offer_sdp([audio]).
make_offer_sdp(Opts) ->
    SDP = ?U:parse_sdp(<<"v=0\r\no=- 1000 1000 IN IP4 0.0.0.0\r\ns=-\r\nt=0 0\r\nm=audio 1 RTP/AVP 8\r\nc=IN IP4 0.0.0.0\r\na=rtpmap:0 PCMA/8000">>),
    #sdp{address={Pr1,Pr2,_},
         medias=[#sdp_m{}=M|_]}=SDP,
    LocalAddr = {Pr1,Pr2,?T_GetIpAddr},
    Ms0 = [],
    FilterCodecs = ?EU:get_by_key(filter_codecs, Opts, []), % RP-1470
    Fmakemedia = fun(MediaN) ->
                         MSta = known_formats(static,MediaN,FilterCodecs),
                         MDyn = known_formats(dynamic,MediaN,FilterCodecs),
                         KFmts = filter_formats(MediaN,MSta++MDyn,[]),
                         M#sdp_m{media= MediaN,
                                 fmt= [P || {P,_,_,_} <- KFmts],
                                 connect= LocalAddr,
                                 attributes= [{<<"rtpmap">>,[P,F]} || {P,F,_,_} <- KFmts]}
                 end,
    % audio
    Ms1 = case lists:member(audio,Opts) of
              false -> Ms0;
              true -> [Fmakemedia(<<"audio">>)|Ms0]
          end,
    % video
    Ms2 = case lists:member(video,Opts) of
              false -> Ms1;
              true -> [Fmakemedia(<<"video">>)|Ms1]
          end,
    % res
    SDP#sdp{user= ?T_GetStreamName,
            address= LocalAddr,
            %connect= LocalAddr,
            medias = lists:reverse(Ms2)}.

%% --------------
%% modifies session name of local SDP
%% --------------
set_sess(#sdp{}=Sdp) ->
    Sdp#sdp{session = ?T_GetStreamName}.

%% ====================================================================
%% Internal functions
%% ====================================================================

% =======================================================
% known media formats
% =======================================================

%% --------------
% @private #305.a
basic_formats(<<"audio">>) -> [<<"PCMU/8000">>,<<"PCMA/8000">>,<<"CN/8000">>,<<"telephone-event/8000">>];
basic_formats(<<"video">>) -> [].

%% --------------
%% RP-1470 filter codecs by options
known_formats(T, MN, []) -> known_formats(T, MN);
known_formats(T, MN, FilterCodecs) ->
    FilterCodecsUp = lists:map(fun(N) -> ?EU:uppercase(N) end, FilterCodecs),
    lists:filter(fun({_,N,_,_}) -> lists:member(?EU:uppercase(N),FilterCodecsUp) end, known_formats(T, MN)).

% static audio payloads
known_formats(static, <<"audio">>) ->
    FunTrue = fun(_) -> true end,
    [{<<"0">>, <<"PCMU/8000">>, [], FunTrue},
     {<<"3">>, <<"GSM/8000">>, [], FunTrue},
     % {<<"4">>, <<"G723/8000">>, [<<"annexa=no">>], FunTrue}, % packet-size:30
     {<<"8">>, <<"PCMA/8000">>, [], FunTrue},
     {<<"9">>, <<"G722/8000">>, [], FunTrue},
     % {<<"10">>, <<"L16/8000/2">>, [], FunTrue}, % stereo
     % {<<"11">>, <<"L16/8000">>, [], FunTrue}, % mono
     {<<"13">>, <<"CN/8000">>, [], FunTrue},
     % {<<"15">>, <<"G728/8000">>, [], FunTrue},
     {<<"18">>, <<"G729/8000">>, [], fun([<<"annexb=no">>]) -> true; (_) -> true end}
    ];

% dynamic audio payloads
% payloads could be changed to satisfy offer
known_formats(dynamic, <<"audio">>) ->
    FunTrue = fun(_) -> true end,
    [{<<"97">>, <<"speex/8000">>, [], FunTrue},
     {<<"98">>, <<"speex/16000">>, [], FunTrue},
     {<<"99">>, <<"speex/32000">>, [], FunTrue},
     %
     {<<"101">>, <<"telephone-event/8000">>, [<<"0-15">>], FunTrue},
     %
     {<<"102">>, <<"G726-16/8000">>, [], FunTrue},
     {<<"103">>, <<"G726-24/8000">>, [], FunTrue},
     {<<"104">>, <<"G726-32/8000">>, [], FunTrue},
     {<<"105">>, <<"G726-40/8000">>, [], FunTrue},
     %
     {<<"106">>, <<"iLBC/8000">>, [<<"mode=20">>], FunTrue},
     %
     {<<"107">>, <<"opus/48000/2">>, [<<"useinbandfec=1;usedtx=1;maxaveragebitrate=64000">>], FunTrue}
    ];

% static video payloads
known_formats(static, <<"video">>) ->
    FunTrue = fun(_) -> true end,
    [{<<"34">>, <<"H263/90000">>, [<<"CIF=2;QCIF=2;VGA=2;CIF4=2">>], FunTrue}];

% dynamic video payloads
% payloads could be changed to satisfy offer
known_formats(dynamic, <<"video">>) ->
    FunTrue = fun(_) -> true end,
    [{<<"97">>, <<"H263-1998/90000">>, [<<"VGA=2;CIF=1;QCIF=1;CIF4=2;I=1;J=1;T=1">>], FunTrue},
     {<<"98">>, <<"H264/90000">>, [<<"packetization-mode=1;profile-level-id=42801e">>], FunTrue},
     {<<"99">>, <<"VP8/90000">>, [], FunTrue},
     {<<"100">>, <<"VP9/90000">>, [], FunTrue}
     %%      {<<"101">>, <<"rtx/90000">>, [], FunTrue},
     %%      {<<"102">>, <<"red/90000">>, [], FunTrue}
    ].

%% --------------
% return payloads of static codecs and names of dynamic codecs which could not be used as general.
% static audio (returns payloads)
known_no_general(static, <<"audio">>) ->
    [<<"13">>];

% dynamic audio (returns names)
known_no_general(dynamic, <<"audio">>) ->
    [<<"telephone-event/8000">>];

% static video (returns payloads)
known_no_general(static, <<"video">>) -> [];

% dynamic video (returns names)
known_no_general(dynamic, <<"video">>) -> [].

%% -------------
% known extra static audio
known_extra(static, <<"audio">>) ->
    [{<<"0">>, <<"PCMU/8000">>},
     {<<"3">>, <<"GSM/8000">>},
     % {<<"4">>, <<"G723/8000">>},
     {<<"8">>, <<"PCMA/8000">>},
     {<<"9">>, <<"G722/8000">>},
     {<<"10">>, <<"L16/8000/2">>}, % stereo
     {<<"11">>, <<"L16/8000">>}, % mono
     {<<"13">>, <<"CN/8000">>},
     % {<<"15">>, <<"G728/8000">>},
     {<<"18">>, <<"G729/8000">>}
    ];

% known extra dynamic audio
known_extra(dynamic, <<"audio">>) ->
    [{<<"97">>, <<"speex/8000">>},
     {<<"98">>, <<"speex/16000">>},
     {<<"99">>, <<"speex/32000">>},
     {<<"101">>, <<"telephone-event/8000">>},
     {<<"102">>, <<"G726-16/8000">>},
     {<<"103">>, <<"G726-24/8000">>},
     {<<"104">>, <<"G726-32/8000">>},
     {<<"105">>, <<"G726-40/8000">>},
     {<<"106">>, <<"iLBC/8000">>},
     {<<"107">>, <<"opus/48000/2">>}
    ];

% known extra static video
known_extra(static, <<"video">>) ->
    [{<<"34">>, <<"H263/90000">>}];

% known extra dynamic video
known_extra(dynamic, <<"video">>) ->
    [{<<"97">>, <<"H263-1998/90000">>},
     {<<"98">>, <<"H264/90000">>},
     {<<"99">>, <<"VP8/90000">>},
     {<<"100">>, <<"VP9/90000">>}].

% =======================================================
% enlarge/reduce by local
% =======================================================

%% --------------
-spec update_by_local_media_fmts(Sdp::#sdp{}, OfferSdp::#sdp{}|undefined, Operations::[add|del|up]) -> NewSdp::#sdp{}.
%% --------------
update_by_local_media_fmts(#sdp{}=Sdp, OfferSdp, Operations) ->
    %update_by_local_media_fmts_1(filter_duplicate_dyn_names(Sdp), filter_duplicate_dyn_names(OfferSdp), Operations).
    update_by_local_media_fmts_1(Sdp, OfferSdp, Operations). % RP-786 rollback #486

%% --------------
update_by_local_media_fmts_1(#sdp{}=Sdp, OfferSdp, Operations) ->
    Sdp1 = update_by_local_media_fmts_1(<<"audio">>, Sdp, OfferSdp, Operations),
    update_by_local_media_fmts_1(<<"video">>, Sdp1, OfferSdp, Operations).

%% --------------
update_by_local_media_fmts_1(MediaN, #sdp{}=Sdp, OfferSdp, Operations)
  when MediaN== <<"audio">>; MediaN== <<"video">> ->
    case extract_media(MediaN, Sdp) of
        false -> Sdp;
        #sdp_m{}=M ->
            M1 = update_by_locals(M, OfferSdp, Operations),
            M2 = reduce_by_offer(M1, OfferSdp),
            replace_media(MediaN, Sdp, M2)
    end.

%% --------------
%% ---- add known / del unknown / update same (by selected operations)
%% --------------
-spec update_by_locals(M::#sdp_m{}, OfferSdp::#sdp{} | undefined, Operations::[add|del|up]) -> NewM::#sdp_m{}.
%% --------------
update_by_locals(#sdp_m{}=M, undefined, Operations) ->
    Known = get_known_formats_for(M),
    update_by_locals_differ_by_fmtp(M, Known, Operations);

update_by_locals(#sdp_m{media=MediaN}=M, #sdp{}=SdpOffer, Operations) ->
    Known = case extract_media(MediaN, SdpOffer) of
                false -> get_known_formats_for(M);
                #sdp_m{}=MOffer -> get_known_formats_for(M,MOffer)
            end,
    update_by_locals_differ_by_fmtp(M, Known, Operations).

%% --------------
-spec update_by_locals_differ_by_fmtp(M::#sdp_m{}, KnownFmts::[fmtdescr()], Operations::[add|del|up]) -> NewM::#sdp_m{}.
%% --------------
update_by_locals_differ_by_fmtp(#sdp_m{media=MediaN, fmt=Fmt, attributes=A}=M, Known, Operations)
  when MediaN== <<"audio">>; MediaN== <<"video">> ->
    % cmp fmt lists
    Diff = get_diff(Fmt, Known),
    % del unknown
    {MFmt1, A1} = case lists:member(del, Operations) of
                      true ->
                          case lists:keyfind(del, 1, Diff) of
                              {_,Dels} -> {Fmt -- Dels, filter_attrs_by_fmts(del,A,Dels)};
                              false -> {Fmt, A}
                          end;
                      false -> {Fmt, A}
                  end,
    % add known
    {MFmt2, A2} = case lists:member(add, Operations) of
                      true ->
                          case lists:keyfind(add, 1, Diff) of
                              {_,Adds} ->
                                  FAdd = fun(P) -> case lists:keyfind(P,1,Known) of
                                                       {P,R,[],_} -> [{<<"rtpmap">>,[P,R]}];
                                                       {P,R,F,_} -> [{<<"rtpmap">>,[P,R]}, {<<"fmtp">>,lists:flatten([P,F])}]
                                                   end end,
                                  {MFmt1 ++ Adds, A1 ++ lists:flatten(lists:map(FAdd, Adds))};
                              false -> {MFmt1, A1}
                          end;
                      false -> {MFmt1, A1}
                  end,
    % modify same by formats
    {MFmt3, A3} = case lists:member(up, Operations) of
                      true -> update_by_locals_same_by_fmtp(M, {MFmt2, A2, Known});
                      false -> {MFmt2, A2}
                  end,
    M#sdp_m{fmt=MFmt3, attributes=A3}.

%% --------------
%% ---- del format not match
%% --------------
-spec update_by_locals_same_by_fmtp(M::#sdp_m{}, {Formats::[binary()], Attributes::[attr()], KnownFmts::[fmtdescr()]}) ->
                                           {NewFormats::[binary()], NewAttributes::[attr()]}.
%% --------------
update_by_locals_same_by_fmtp(#sdp_m{media=MediaN, fmt=Fmt, attributes=A}, {MFmt2, A2, Known})
  when MediaN== <<"audio">>; MediaN== <<"video">> ->
    % find same
    Same = get_same(Fmt, Known),
    % extract payload fmtp of same
    Fun1 = fun({AN,[P|FmtpOpts]}) when AN == <<"fmtp">> ->
                   case lists:member(P,Same) of
                       false -> false;
                       true -> {true, {P,FmtpOpts}}
                   end;
              (_) -> false end,
    FmtpOfSame = lists:filtermap(Fun1, A), % from sdp
    % delete if format check fun of known payloads doesn't satisfy
    Fun2 = fun({P,_,_,Fun}, Acc) ->
                   case lists:member(P,Same) of
                       false -> Acc;
                       true ->
                           PFmtpOpts = case lists:keyfind(P,1,FmtpOfSame) of
                                           false -> [];
                                           {_,FmtpOpts} -> FmtpOpts
                                       end,
                           case Fun(PFmtpOpts) of
                               true -> Acc;
                               false -> [P|Acc]
                           end
                   end end,
    {MFmt3, A3} = case lists:foldl(Fun2, [], Known) of
                      [] -> {MFmt2, A2};
                      DelByFmt -> {MFmt2 -- DelByFmt, filter_attrs_by_fmts(del,A2,DelByFmt)}
                  end,
    {MFmt3, A3}.

%% --------------
get_known_formats_for(#sdp_m{media=MediaN, fmt=Fmt, attributes=A}=_M)
  when MediaN== <<"audio">>; MediaN== <<"video">> ->
    KnownStatic = known_formats(static,MediaN),
    KnownDynamic = modify_known_dynamic_payloads({Fmt,A}, known_formats(dynamic,MediaN)),
    filter_formats(MediaN, KnownStatic ++ KnownDynamic, Fmt). % #305.a
get_known_formats_for(#sdp_m{media=MediaN, fmt=Fmt, attributes=A}=_M, #sdp_m{fmt=Fmt2, attributes=A2}=_OfferM)
  when MediaN== <<"audio">>; MediaN== <<"video">> ->
    KnownStatic = known_formats(static,MediaN),
    KnownDynamic = modify_known_dynamic_payloads({Fmt,A}, modify_known_dynamic_payloads({Fmt2,A2}, known_formats(dynamic,MediaN))), % #485 without adapt to Offer
    KnownStatic ++ KnownDynamic. % #485 without filter by Offer

%% --------------
% #305.a
%% --------------
filter_formats(MediaN, KnownFormats, ForFmt)
  when MediaN== <<"audio">>; MediaN== <<"video">> ->
    case ?T_GetPayloads('offer',MediaN) of
        [] -> lists:filter(fun({PL,_,_,_}) -> lists:member(PL,ForFmt) end, KnownFormats); % #475
        A -> lists:filtermap(fun({PT,PN,_,_}=P) ->
                                     case lists:member(PN,basic_formats(MediaN)) of
                                         true -> {true,P};
                                         false ->
                                             case lists:member(PN,A) of
                                                 true -> {true,P};
                                                 false ->
                                                     case lists:member(PT,ForFmt) of
                                                         true -> {true,P};
                                                         false -> false
                                                     end end end end, KnownFormats)
    end.

%% --------------
get_diff(Fmts, Known) ->
    KnownFmt = [P || {P,_,_,_} <- Known],
    cmp_lists_diff(Fmts, KnownFmt).

%% --------------
get_same(Fmts, Known) ->
    KnownFmt = [P || {P,_,_,_} <- Known],
    cmp_lists_same(Fmts, KnownFmt).

%% --------------
max_payload(Ps1,Ps2) ->
    FP = fun(P) when is_binary(P) -> ?EU:to_int(P);
            ({P,_}) when is_binary(P) -> ?EU:to_int(P);
            ({P,_,_,_}) when is_binary(P) -> ?EU:to_int(P)
         end,
    P1 = lists:max([-1 | lists:map(FP, Ps1)]),
    P2 = lists:max([-1 | lists:map(FP, Ps2)]),
    lists:max([?DynamicPL, P1, P2]).

%% --------------
nonused_payloads(Ps1,Ps2) ->
    FP = fun(P) when is_binary(P) -> ?EU:to_int(P);
            ({P,_}) when is_binary(P) -> ?EU:to_int(P);
            ({P,_,_,_}) when is_binary(P) -> ?EU:to_int(P)
         end,
    Ps11 = lists:filter(fun(P) -> P >= ?DynamicPL end, lists:map(FP, Ps1)),
    Ps21 = lists:filter(fun(P) -> P >= ?DynamicPL end, lists:map(FP, Ps2)),
    (lists:seq(?DynamicFrom,?DynamicTo) -- Ps11) -- Ps21.

%% --------------
get_attr(Name, Attr, P) when Name == <<"fmtp">>; Name == <<"rtpmap">> ->
    lists:foldl(fun({_Name, [_P|Val]}, []) when _Name==Name, _P==P -> Val; (_,Acc) -> Acc end, [], Attr).

%% --------------
%% modifies known dynamic payloads to satisfy offer.
%%  if intersects with different rtpmaps then moves known payloads to free zone
%% --------------
modify_known_dynamic_payloads({Fmt,Att}, KnownDynamic) ->
    MFmtDyn = lists:filter(fun(P) -> ?EU:to_int(P)>=?DynamicPL end, Fmt),
    Basic = lists:filtermap(fun({<<"rtpmap">>,[P,R|_]}) -> case lists:member(P,MFmtDyn) of false -> false; true -> {true, {P,R}} end; (_) -> false end, Att),
    Target = [{P,N} || {P,N,_,_} <- KnownDynamic],
    Result = adapt_dynamic_payloads(Target, Basic),
    Map = lists:foldr(fun({P,N}, Acc) -> maps:put(N,case maps:find(N,Acc) of error -> [P]; {ok,Ps} -> [P|Ps] end,Acc) end, maps:new(), Result),
    KnownDynamicUniqueSeq = KnownDynamic -- KnownDynamic -- lists:ukeysort(2,KnownDynamic),
    lists:flatten(lists:map(fun({P,N,_,_}=Item) ->
                                    case maps:find(N,Map) of
                                        error -> Item;
                                        {ok,[P]} -> Item;
                                        {ok,[P1]} -> setelement(1,Item,P1);
                                        {ok,Ps} -> lists:map(fun(Px) -> setelement(1,Item,Px) end, Ps)
                                    end end, KnownDynamicUniqueSeq)).

% =======================================================
% reduce by offer
% (1. prepare already matched subset)
% (2. prepare opposite subset by names)
% =======================================================

%% --------------
reduce_by_offer(M, undefined) -> M;
reduce_by_offer(#sdp_m{media=MediaN}=M, #sdp{}=SdpOffer)
  when MediaN== <<"audio">>; MediaN== <<"video">> ->
    case extract_media(MediaN, SdpOffer) of
        false -> M; % error, but forward through server
        #sdp_m{}=MO -> reduce_by_offer_1(M, MO)
    end.

%% --------------
%% leaves only intersecting formats (static formats by payload, dynamic formats by names)
%% --------------
reduce_by_offer_1(#sdp_m{fmt=Fmt, attributes=Attr}=AnswerM, #sdp_m{}=OfferM) ->
    % payloads should correspond offer (static by payloads, dynamic by names!),
    %   even if differ basic known payloads (and dynamic payloads not match!)
    #sdp_m{fmt=OFmt1} = adapt_media_offer_by_dyn(AnswerM,OfferM), % #473
    Diff = cmp_lists_diff(Fmt, OFmt1),
    % del
    {Fmt1, Attr1} = case lists:keyfind(del, 1, Diff) of
                        {_,Dels} -> {Fmt -- Dels, filter_attrs_by_fmts(del,Attr,Dels)};
                        false -> {Fmt, Attr}
                    end,
    AnswerM#sdp_m{fmt=Fmt1, attributes=Attr1}.

%% --------------
%% #473
%% makes mapping of offer dynamic payloads into answer dynamic payloads (intersect dynamic formats by names and map payloads)
%% --------------
adapt_media_offer_by_dyn(#sdp_m{fmt=AFmt, attributes=AAttr}=_AnswerM, #sdp_m{fmt=OFmt, attributes=OAttr}=OfferM) ->
    case lists:filter(fun(P) -> ?EU:to_int(P)>=?DynamicPL end, OFmt) of
        [] -> OfferM;
        OFmtDyn ->
            OTarget = capitalize_names(extract_rtpmaps_as_fmt(OFmtDyn,OAttr)), % #483
            ABasic = capitalize_names(extract_rtpmaps_as_fmt(AFmt,AAttr)),
            ODynAdapted = adapt_dynamic_payloads(OTarget, ABasic),
            Mapping = get_mapping(OTarget,ODynAdapted),
            Fmap = fun(PL) -> case lists:keyfind(PL,1,Mapping) of false -> PL; {_,PL2} -> PL2 end end,
            OFmt1 = lists:map(fun(PL) -> Fmap(PL) end, OFmt),
            OAttr1 = lists:map(fun({AN,[PL|Rest]}) when ?AttrNamesPL(AN) -> {AN,[Fmap(PL)|Rest]}; (T) -> T end, OAttr),
            OfferM#sdp_m{fmt=OFmt1,attributes=OAttr1}
    end.

% =======================================================
% reduce to 1 preferred audio format
% (Prefer direct no transcoding, then top codec. Uses only general codecs, no CN=13,TE=101)
% =======================================================

%% --------------
%% after preparing response sdp makes reduce to only one media format in each stream (preferred existing in remote response or first known in remote offer)
%%  should not exclude no alternative formats as static CN and dynamic TE.
%% --------------
-spec reduce_answer_to_one_general_fmt(SdpPreparedForAnswerToA::#sdp{}, SdpAnswerFromB::#sdp{}, SdpOfferFromA::#sdp{}) -> Sdp::#sdp{}.
%% --------------
reduce_answer_to_one_general_fmt(#sdp{}=Sdp, #sdp{}=SdpAnswer, #sdp{}=SdpOffer) ->
    Sdp1 = reduce_answer_to_one_general_fmt(<<"audio">>, Sdp, SdpAnswer, SdpOffer),
    reduce_answer_to_one_general_fmt(<<"video">>, Sdp1, SdpAnswer, SdpOffer).

%% --------------
%% reduce selected media stream to only one general format, not excluding no alternative formats as CN,TE
%% --------------
reduce_answer_to_one_general_fmt(MediaN, #sdp{}=Sdp, #sdp{}=SdpA, #sdp{}=SdpO)
  when MediaN== <<"audio">>; MediaN== <<"video">> ->
    M = extract_media(MediaN, Sdp),
    MA = extract_media(MediaN, SdpA),
    MO = extract_media(MediaN, SdpO),
    case {M,MO,MA} of
        {false,_,_} -> Sdp; % no medianame in result => no reduce
        {_,false,_} -> Sdp; % no medianame in offer => no reduce
        {_,_,false} -> Sdp; % no medianame in answer => error, unbeleivable
        {#sdp_m{attributes=Attrs},#sdp_m{},#sdp_m{}} ->
            FmtsRes = reduce_media_fmts_to_single(MediaN, M, MO, MA),
            replace_media(MediaN, Sdp, M#sdp_m{fmt=FmtsRes,attributes=filter_attrs_by_fmts(leave,Attrs,FmtsRes)})
    end.

%% --------------
%% choose only one fmt from alternatives, choose top for answer to side a
%% expected Sdp has corresponding dynamic payload codes as SdpA, but SdpO dynamic payloads could differ
%% --------------
reduce_media_fmts_to_single(MediaN,#sdp_m{fmt=Fmts,attributes=Attrs},#sdp_m{fmt=FmtsO,attributes=AttrsO},#sdp_m{fmt=FmtsA}) ->
    D = make_fmt(MediaN,Fmts,Attrs),
    DO = make_fmt(MediaN,FmtsO,AttrsO),
    AdaptedFmtsO = [P || {P,_} <- adapt_dynamic_payloads(DO, D)],
    FGen = fun(P) -> case ?EU:to_int(P)>=?DynamicPL of
                         true ->
                             RtpMap = case get_attr(<<"rtpmap">>,Attrs,P) of
                                          [N|_] -> N;
                                          [] -> unknown
                                      end,
                             not lists:member(RtpMap, known_no_general(dynamic,MediaN));
                         false -> not lists:member(P, known_no_general(static,MediaN))
                     end end,
    {_,FmtP,FmtsNG} = lists:foldl(fun(P,{found=AccR,AccF,AccNG}=Acc) ->
                                          case FGen(P) of
                                              false -> {AccR,AccF,[P|AccNG]};
                                              true -> Acc
                                          end;
                                     (P,{AccR,AccF,AccNG}=Acc) ->
                                          case FGen(P) of
                                              false -> {AccR,AccF,[P|AccNG]};
                                              true ->
                                                  case lists:member(P,FmtsA) of
                                                      true -> {found,P,AccNG};
                                                      false when AccR==undefined -> {first,P,AccNG};
                                                      false -> Acc
                                                  end end end, {undefined,undefined,[]}, AdaptedFmtsO),
    [FmtP|lists:reverse(FmtsNG)].

% =======================================================
% services
% =======================================================

%% %% --------------
%% %% #486
%% %% --------------
%% filter_duplicate_dyn_names(undefined) -> undefined;
%% filter_duplicate_dyn_names(#sdp{medias=Ms}=Sdp) ->
%%     F = fun(#sdp_m{fmt=Fmt,attributes=Attrs}=M) ->
%%                 FmtNames = extract_rtpmaps_as_fmt(Fmt,Attrs),
%%                 Unique = lists:ukeysort(2,FmtNames),
%%                 case length(Unique)==length(FmtNames) of
%%                     true -> M;
%%                     false ->
%%                         M#sdp_m{fmt=lists:filter(fun(P) -> lists:keymember(P,1,Unique) end, Fmt),
%%                                 attributes=lists:filter(fun({AN,[P,_]}) when ?AttrNamesPL(AN) -> lists:keymember(P,1,Unique);
%%                                                                (_) -> true
%%                                                           end, Attrs)}
%%                 end end,
%%     Sdp#sdp{medias=lists:map(F, Ms)}.

%% --------------
%% #485
%% make dynamic payloads in target same as in basic by same names (case insensitive), leave other unchanged.
%% save order of unchanged, others to the end.
%% NOTE! webrtc could use same video codecs with different properties. This should be handled by expanding target list.
%% --------------
-spec adapt_dynamic_payloads(Target::[fmt()], Basic::[fmt()]) -> Result::[fmt()].
%% --------------
adapt_dynamic_payloads(Target, Basic) ->
    MultiName = true,
    %MaxP0 = max_payload(Target,Basic),
    NonUsedP0 = nonused_payloads(Target,Basic),
    BasicDyn = lists:filter(fun({P,_N}) -> ?EU:to_int(P)>=?DynamicPL end, Basic),
    {Res,_,_} = lists:foldl(fun({P,N}, {Acc,NonUsedP,Cache}) ->
                                    X = lists:keyfind(N, 2, Acc),
                                    Y = case binary:split(N,<<"/">>,[global]) of
                                            [_,_] -> false;
                                            [NA,NB,<<"1">>] -> lists:keyfind(<<NA/binary,"/",NB/binary>>, 2, Acc);
                                            _ -> false
                                        end,
                                    case {X, Y} of
                                        {{P,_},_} -> % rtpmap found the same payload
                                            {Acc,NonUsedP,[N|Cache]};
                                        {_,{P,_}} -> % rtpmap found the same payload
                                            {Acc,NonUsedP,[N|Cache]};
                                        {false,false} -> % rtpmap not found
                                            case lists:keyfind(P, 1, Acc) of
                                                false -> % payload not intersect
                                                    {Acc,NonUsedP,Cache};
                                                {_,_}=CInfo -> % payload intersect
                                                    %PNew = MaxP + 1,
                                                    [PNew|NonUsedP1] = NonUsedP,
                                                    PNewB = ?EU:to_binary(PNew),
                                                    Acc1 = lists:keystore(PNewB,1,lists:keydelete(P,1,Acc),setelement(1,CInfo,PNewB)),
                                                    {Acc1,NonUsedP1,Cache}
                                            end;
                                        _ -> % rtpmap found another payload
                                            {P1,CInfo1} = case X of {P0,_} -> {P0,X}; false -> {P0,_}=Y, {P0,Y} end,
                                            case lists:keyfind(P, 1, Acc) of
                                                false -> % this payload not intersect
                                                    case MultiName andalso lists:member(N,Cache) of
                                                        false ->
                                                            Acc1 = lists:keystore(P,1,lists:keydelete(P1,1,Acc),setelement(1,CInfo1,P)),
                                                            {Acc1,NonUsedP,[N|Cache]};
                                                        true ->
                                                            Acc1 = lists:keystore(P,1,Acc,setelement(1,CInfo1,P)),
                                                            {Acc1,NonUsedP,Cache}
                                                    end;
                                                {_,N} -> % rtpmap found the same payload
                                                    {Acc,NonUsedP,[N|Cache]};
                                                {_,_}=CInfo2 -> % this payload intersect
                                                    %PNew = MaxP + 1,
                                                    [PNew|NonUsedP1] = NonUsedP,
                                                    PNewB = ?EU:to_binary(PNew),
                                                    Acc1 = lists:keystore(PNewB,1,lists:keydelete(P,1,Acc),setelement(1,CInfo2,PNewB)),
                                                    case MultiName andalso lists:member(N,Cache) of
                                                        false ->
                                                            Acc2 = lists:keystore(P,1,lists:keydelete(P1,1,Acc1),setelement(1,CInfo1,P)),
                                                            {Acc2,NonUsedP1,[N|Cache]};
                                                        true ->
                                                            Acc2 = lists:keystore(P,1,Acc1,setelement(1,CInfo1,P)),
                                                            {Acc2,NonUsedP1,Cache}
                                                    end end
                                    end end, {capitalize_names(Target),NonUsedP0,[]}, capitalize_names(BasicDyn)), % #483
    restore_orig_case(Res, Target).

%% --------------
%% #483 restores case of names of all codecs to known original formats
%% --------------
-spec restore_known_case(MediaN::binary(), CapitalizedFmt::[fmt()] | [fmtdescr()]) -> Res::[fmt()] | [fmtdescr()].
%% --------------
restore_known_case(MediaN, CapitalizedFmts) ->
    Known = known_formats(static,MediaN) ++ known_formats(dynamic,MediaN),
    restore_orig_case(CapitalizedFmts, Known).

%% --------------
%% restores case of names of all codecs to selected original formats
%% --------------
-spec restore_orig_case(CapitalizedFmt::[fmt()] | [fmtdescr()], OrigCaseFmts::[fmt()] | [fmtdescr()]) -> Res::[fmt()] | [fmtdescr()].
%% --------------
restore_orig_case(CapitalizedFmts, OrigCaseFmts) ->
    Fmap = fun(N,Map) -> maps:put(capitalize(N),N,Map) end,
    Map = lists:foldl(fun({_,N,_,_},Acc) -> Fmap(N,Acc);
                         ({_,N},Acc) -> Fmap(N,Acc)
                      end, maps:new(), OrigCaseFmts),
    Frestore = fun(N,ItemTuple) ->
                       case maps:find(capitalize(N),Map) of
                           error -> ItemTuple;
                           {_,OrigN} -> setelement(2,ItemTuple,OrigN)
                       end end,
    lists:map(fun({_,N,_,_}=Item) -> Frestore(N,Item);
                 ({_,N}=Item) -> Frestore(N,Item)
              end, CapitalizedFmts).

%% --------------
%% #473
%% makes [{x,x},{y,z}] list of payloads mappings by comparing of two [{PL,Name,Descr,Fun}] by names.
%% RP-786 NOTE that could be duplicated names of dynamic codecs, ex. H264/90000 by webrtc
%% --------------
-spec get_mapping([fmt() | fmtdescr()],[fmt() | fmtdescr()]) -> [{_,_}].
%% --------------
get_mapping(FmtsDescr1, FmtsDescr2) ->
    % first intersection directly
    FDx1 = lists:sort(lists:map(fun({P,N,_,_}) -> {P,N}; ({P,N}) -> {P,N} end, FmtsDescr1)),
    FDx2 = lists:sort(lists:map(fun({P,N,_,_}) -> {P,N}; ({P,N}) -> {P,N} end, FmtsDescr2)),
    Intersection = ordsets:intersection(FDx1,FDx2),
    Direct = lists:map(fun({P,_}) -> {P,P} end, Intersection),
    % then others sequently extract
    FDy1 = ordsets:subtract(FDx1, Intersection),
    FDy2 = ordsets:subtract(FDx2, Intersection),
    element(2,lists:foldl(fun({PL1,Name},{Rest,Acc}) ->
                                  {value,{PL2,_},Rest1} = lists:keytake(Name,2,Rest),
                                  {Rest1,[{PL1,PL2}|Acc]}
                          end, {FDy2,Direct}, FDy1)).

%% --------------
%% #473
%% makes list of formats by rtpmap attributes in Attrs filtered by selected Fmts.
%% --------------
-spec extract_rtpmaps_as_fmt([PL::integer()],[Attr::{AttrName::binary(),[AttrParam::binary()]}]) -> [fmt()].
%% --------------
extract_rtpmaps_as_fmt(Fmts,Attrs) ->
    Fadapt = fun({_,[PL,Name|_]}) -> {PL,Name} end,
    lists:filtermap(fun({<<"rtpmap">>,[PL|_]}=Item) ->
                            case lists:member(PL,Fmts) of
                                true -> {true,Fadapt(Item)};
                                false -> false
                            end;
                       (_) -> false
                    end, Attrs).

%% --------------
%% #485 @private
%% makes full list of formats by fmt & rtpmap attributes in Attrs filtered by selected Fmts. Missed attrs filled by known static formats.
%% --------------
make_fmt(MediaN,Fmts,Attrs) ->
    RtpMaps = extract_rtpmaps_as_fmt(Fmts,Attrs),
    KnownStatic = known_formats(static,MediaN),
    lists:filtermap(fun(PL) ->
                            case lists:keyfind(PL,1,RtpMaps) of
                                {PL,Name} -> {true,{PL,Name}};
                                false ->
                                    case lists:keyfind(PL,1,KnownStatic) of
                                        {PL,Name,_,_} -> {true,{PL,Name}};
                                        false -> false
                                    end end end, Fmts).

%% --------------
%% #483
capitalize_names(Formats) ->
    lists:map(fun({_,N,_,_}=Item) -> setelement(2,Item,capitalize(N));
                 ({_,N}=Item) -> setelement(2,Item,capitalize(N))
              end, Formats).

%% --------------
capitalize(X) -> ?EU:to_upper(X).

%% --------------
expand_names(Formats) ->
    lists:usort(Formats ++
                lists:filtermap(fun({_,N,_,_}=Item) -> {true, setelement(2,Item,<<N/binary,"/1">>)};
                                   ({_,N}=Item) -> {true, setelement(2,Item,<<N/binary,"/1">>)}
                                end, Formats)).

%% --------------
extract_media(MediaN, #sdp{medias=Ms})
  when MediaN== <<"audio">>; MediaN== <<"video">> ->
    case lists:keyfind(MediaN, #sdp_m.media, Ms) of
        false -> false;
        #sdp_m{}=M -> M
    end.

%% --------------
replace_media(MediaN, #sdp{medias=Ms}=Sdp, #sdp_m{}=M) ->
    Sdp#sdp{medias=lists:keyreplace(MediaN, #sdp_m.media, Ms, M)}.

%% --------------
cmp_lists_diff(L1, L2) ->
    case {L1--L2, L2--L1} of
        {[], []} -> [];
        {Dels, []} -> [{del, Dels}];
        {[], Adds} -> [{add, Adds}];
        {Dels, Adds} -> [{del, Dels}, {add, Adds}]
    end.

%% --------------
cmp_lists_same(L1, L2) ->
    ordsets:intersection(ordsets:from_list(L1), ordsets:from_list(L2)).

%% --------------
filter_attrs_by_fmts(Op,Attrs,Fmts) when Op==del; Op==leave ->
    F = fun(P) when Op==leave -> lists:member(P,Fmts);
           (P) when Op==del -> not lists:member(P,Fmts)
        end,
    FDel = fun({AN,[P|_]}) when ?AttrNamesPL(AN) -> F(P);
              (_) -> true
           end,
    lists:filter(FDel, Attrs).

% =======================================================
% sdp attribute sendrecv
% =======================================================

%% --------------
do_ensure_stream_mode(Mode, #sdp{medias=Ms}=Sdp)
  when Mode== <<"sendrecv">>; Mode== <<"inactive">>; Mode== <<"sendonly">>; Mode== <<"recvonly">> ->
    Sdp#sdp{medias=lists:foldr(fun(M,Acc) -> [do_ensure_stream_mode(Mode,M) | Acc] end, [], Ms)};

do_ensure_stream_mode(Mode, #sdp_m{attributes=As}=M) ->
    {Found,As1} = lists:foldl(fun({A,P},{_,Acc})
                                    when A == <<"sendrecv">>;
                                         A == <<"sendonly">>;
                                         A == <<"recvonly">>;
                                         A == <<"inactive">> ->
                                      {true,[{Mode,P}|Acc]};
                                 (A,{X,Acc}) -> {X,[A|Acc]} end, {false,[]}, As),
    As2 = case Found of
              true -> As1;
              false -> [{Mode,[]} | As1]
          end,
    M#sdp_m{attributes=lists:reverse(As2)}.

%% --------------
do_reverse_stream_mode(#sdp{}=Sdp, [], Ms) -> Sdp#sdp{medias=lists:reverse(Ms)};
do_reverse_stream_mode(#sdp{}=Sdp, [#sdp_m{attributes=As}=M|Rest], Acc) ->
    M1 = do_reverse_stream_mode(M, As, []),
    do_reverse_stream_mode(Sdp, Rest, [M1|Acc]);

do_reverse_stream_mode(#sdp_m{}=Media, [], Attributes) -> Media#sdp_m{attributes=lists:reverse(Attributes)};
do_reverse_stream_mode(#sdp_m{}=Media, [A|Rest], Acc) ->
    A1 = case A of
             {<<"sendrecv">>,_}=M -> M;
             {<<"inactive">>,_}=M -> M;
             {<<"sendonly">>,P} -> {<<"recvonly">>,P};
             {<<"recvonly">>,P} -> {<<"sendonly">>,P};
             _ -> A
         end,
    do_reverse_stream_mode(Media, Rest, [A1|Acc]).

%% --------------
do_reverse_stream_mode(<<"sendonly">>) -> <<"recvonly">>;
do_reverse_stream_mode(<<"recvonly">>) -> <<"sendonly">>;
do_reverse_stream_mode(M) when is_binary(M) -> M.

%% --------------
% @private
mode_idx(<<"sendrecv">>) -> 3;
mode_idx(<<"sendonly">>) -> 2;
mode_idx(<<"recvonly">>) -> 1;
mode_idx(<<"inactive">>) -> 0.