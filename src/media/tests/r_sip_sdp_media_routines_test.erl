%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 27.10.2017
%%% @doc Unit test module (#474)

-module(r_sip_sdp_media_routines_test).
-author('Peter Bukashin <tbotc@yandex.ru>').

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_mgc.hrl").
-include_lib("eunit/include/eunit.hrl").

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Init functions
%% ====================================================================

% init function, load environment
start_test_() ->
    include_nksip(),
    include_environment(),
    [].

% @private attach env project
include_environment() ->
    {ok,CWD} = file:get_cwd(),
    RPath = filename:dirname(filename:dirname(CWD)),
    Path = filename:join([RPath, "era_env", "ebin"]),
    code:add_patha(Path).

% @private attach nksip
include_nksip() ->
    {ok,CWD} = file:get_cwd(),
    RPath = filename:dirname(filename:dirname(CWD)),
    code:add_patha(filename:join([RPath, "era_sip", "deps", "nksip", "ebin"])),
    code:add_patha(filename:join([RPath, "era_sip", "deps", "nklib", "ebin"])),
    code:add_patha(filename:join([RPath, "era_sip", "deps", "nkpacket", "ebin"])),
    code:add_patha(filename:join([RPath, "era_sip", "deps", "nkserver", "ebin"])),
    %code:add_patha(filename:join([RPath, "era_sip", "deps", "nkservice", "ebin"])), old_nksip
    code:add_patha(filename:join([RPath, "era_sip", "deps", "nkdocker", "ebin"])).

%% ====================================================================
%% Autotest functions
%% ====================================================================

% ok
parse_sdp_test_() ->
    ?_test(#sdp{} = ?U:parse_sdp(<<"v=0\r\no=- 1000 1000 IN IP4 0.0.0.0\r\ns=-\r\nt=0 0\r\nm=audio 1 RTP/AVP 8\r\nc=IN IP4 0.0.0.0\r\na=rtpmap:0 PCMA/8000">>)).

% -----
% ok
mode_idx_test_() ->
    {"SDP mode_idx test",
     [?_assertEqual(0,?M_SDP:mode_idx(<<"inactive">>)),
      ?_assertEqual(1,?M_SDP:mode_idx(<<"recvonly">>)),
      ?_assertEqual(2,?M_SDP:mode_idx(<<"sendonly">>)),
      ?_assertEqual(3,?M_SDP:mode_idx(<<"sendrecv">>))
      ]}.

% ok
get_audio_mode_test_() ->
    {"SDP get_audio_mode test",
     [?_assertEqual(<<"inactive">>,?M_SDP:get_audio_mode(construct_sdp([{mode,<<"inactive">>}]))),
      ?_assertEqual(<<"sendrecv">>,?M_SDP:get_audio_mode(construct_sdp([{mode,<<"sendrecv">>}]))),
      ?_assertEqual(<<"sendonly">>,?M_SDP:get_audio_mode(construct_sdp([{mode,<<"sendonly">>}]))),
      ?_assertEqual(<<"recvonly">>,?M_SDP:get_audio_mode(construct_sdp([{mode,<<"recvonly">>}]))),
      ?_assertEqual(<<"sendrecv">>,?M_SDP:get_audio_mode(construct_sdp([{mode,undefined},{video,false}]))),
      ?_assertEqual(undefined,?M_SDP:get_audio_mode(construct_sdp([{mode,<<"recvonly">>},{audio,false}]))),
      ?_assertEqual(undefined,?M_SDP:get_audio_mode(construct_sdp([{mode,<<"recvonly">>},{audio,false},{video,false}])))
      ]}.

% ok
reverse_stream_mode_test_() ->
    {"SDP reverse_stream_mode test",
     [?_assertEqual(<<"inactive">>,?M_SDP:reverse_stream_mode(<<"inactive">>)),
      ?_assertEqual(<<"sendrecv">>,?M_SDP:reverse_stream_mode(<<"sendrecv">>)),
      ?_assertEqual(<<"sendonly">>,?M_SDP:reverse_stream_mode(<<"recvonly">>)),
      ?_assertEqual(<<"recvonly">>,?M_SDP:reverse_stream_mode(<<"sendonly">>)),
      ?_assertEqual(<<"inactive">>,?M_SDP:get_audio_mode(?M_SDP:reverse_stream_mode(construct_sdp([{mode,<<"inactive">>}])))),
      ?_assertEqual(<<"sendonly">>,?M_SDP:get_audio_mode(?M_SDP:reverse_stream_mode(construct_sdp([{mode,<<"recvonly">>},{video,false}])))),
      ?_assertEqual(<<"recvonly">>,?M_SDP:get_audio_mode(?M_SDP:reverse_stream_mode(construct_sdp([{mode,<<"sendonly">>}])))),
      ?_assertEqual(<<"sendrecv">>,?M_SDP:get_audio_mode(?M_SDP:reverse_stream_mode(construct_sdp([{mode,<<"sendrecv">>}]))))
      ]}.

% ok
ensure_stream_mode_test_() ->
    {"SDP ensure_stream_mode test",
     [?_assertEqual(<<"sendonly">>,?M_SDP:get_audio_mode(?M_SDP:sendonly(construct_sdp([{mode,<<"inactive">>}])))),
      ?_assertEqual(<<"sendrecv">>,?M_SDP:get_audio_mode(?M_SDP:sendrecv(construct_sdp([{mode,<<"inactive">>}])))),
      ?_assertEqual(<<"inactive">>,?M_SDP:get_audio_mode(?M_SDP:inactive(construct_sdp([{mode,<<"recvonly">>}])))),
      ?_assertEqual(<<"inactive">>,?M_SDP:get_audio_mode(?M_SDP:setmode(construct_sdp([{mode,<<"sendrecv">>}]),<<"inactive">>))),
      ?_assertEqual(<<"sendonly">>,?M_SDP:get_audio_mode(?M_SDP:setmode(construct_sdp([{mode,undefined}]),<<"sendonly">>)))
      ]}.

% ok
build_media_mode_test_() ->
    {"SDP ensure_stream_mode test",
     [?_assertEqual(<<"sendrecv">>,?M_SDP:build_media_mode(true,true)),
      ?_assertEqual(<<"recvonly">>,?M_SDP:build_media_mode(true,false)),
      ?_assertEqual(<<"inactive">>,?M_SDP:build_media_mode(false,true)),
      ?_assertEqual(<<"inactive">>,?M_SDP:build_media_mode(false,false)),
      ?_assertEqual(<<"sendrecv">>,?M_SDP:build_media_mode([true,true])),
      ?_assertEqual(<<"inactive">>,?M_SDP:build_media_mode([false,true]))
      ]}.

% ok
limit_media_mode_test_() ->
    {"SDP limit_media_mode test",
     [?_assertEqual(<<"sendonly">>,?M_SDP:limit_media_mode(<<"sendrecv">>,<<"sendonly">>)),
      ?_assertEqual(<<"sendrecv">>,?M_SDP:limit_media_mode(<<"sendrecv">>,<<"sendrecv">>)),
      ?_assertEqual(<<"recvonly">>,?M_SDP:limit_media_mode(<<"recvonly">>,<<"sendonly">>)),
      ?_assertEqual(<<"inactive">>,?M_SDP:limit_media_mode(<<"inactive">>,<<"sendonly">>)),
      ?_assertEqual(<<"inactive">>,?M_SDP:limit_media_mode(<<"sendonly">>,<<"inactive">>)),
      ?_assertEqual(<<"recvonly">>,?M_SDP:limit_media_mode(<<"recvonly">>,<<"sendonly">>)),
      ?_assertEqual(<<"recvonly">>,?M_SDP:limit_media_mode(<<"sendonly">>,<<"recvonly">>)),
      ?_assertEqual(<<"sendonly">>,?M_SDP:limit_media_mode(<<"sendonly">>,<<"sendonly">>)),
      %
      ?_assertEqual(<<"sendonly">>,?M_SDP:limit_media_mode_nochange(<<"sendrecv">>,<<"sendonly">>)),
      ?_assertEqual(undefined,?M_SDP:limit_media_mode_nochange(<<"sendrecv">>,<<"sendrecv">>)),
      ?_assertEqual(undefined,?M_SDP:limit_media_mode_nochange(<<"recvonly">>,<<"sendonly">>)),
      ?_assertEqual(undefined,?M_SDP:limit_media_mode_nochange(<<"inactive">>,<<"sendonly">>)),
      ?_assertEqual(<<"inactive">>,?M_SDP:limit_media_mode_nochange(<<"sendonly">>,<<"inactive">>)),
      ?_assertEqual(undefined,?M_SDP:limit_media_mode_nochange(<<"recvonly">>,<<"sendonly">>)),
      ?_assertEqual(<<"recvonly">>,?M_SDP:limit_media_mode_nochange(<<"sendonly">>,<<"recvonly">>)),
      ?_assertEqual(undefined,?M_SDP:limit_media_mode_nochange(<<"sendonly">>,<<"sendonly">>))
      ]}.

% ok
check_sendrecv_test_() ->
    Sdp1 = #sdp{medias=[#sdp_m{media= <<"audio">>,fmt=[<<"0">>],attributes=[{<<"sendrecv">>,[]}]}]},
    Sdp2 = #sdp{medias=[#sdp_m{media= <<"audio">>,fmt=[<<"0">>],attributes=[{<<"sendonly">>,[]}]}]},
    Sdp3 = #sdp{medias=[#sdp_m{media= <<"audio">>,fmt=[<<"0">>],attributes=[{<<"inactive">>,[]}]}]},
    Sdp4 = #sdp{medias=[#sdp_m{media= <<"audio">>,fmt=[<<"0">>],attributes=[{<<"rtpmap">>,[<<"0">>,<<"PCMU/8000">>]}]}]},
    Sdp5 = #sdp{medias=[#sdp_m{media= <<"video">>,fmt=[<<"100">>],attributes=[{<<"rtpmap">>,[<<"100">>,<<"H264/90000">>]}]}]},
    Sdp6 = #sdp{medias=[]},
    {"SDP check_sendrecv test",
     [?_assertEqual(true,?M_SDP:check_sendrecv(Sdp1)),
      ?_assertEqual(false,?M_SDP:check_sendrecv(Sdp2)),
      ?_assertEqual(false,?M_SDP:check_sendrecv(Sdp3)),
      ?_assertEqual(true,?M_SDP:check_sendrecv(Sdp4)),
      ?_assertEqual(false,?M_SDP:check_sendrecv(Sdp5)), % no audio - no sendrecv
      ?_assertEqual(false,?M_SDP:check_sendrecv(Sdp6))
      ]}.

% ok
check_update_t38_t30_rfc2833_audio_test_() ->
    Sdp1 = #sdp{medias=[#sdp_m{media= <<"audio">>,port=0,fmt=[<<"0">>],attributes=[{<<"sendrecv">>,[]}]},
                        #sdp_m{media= <<"image">>,port=1001}]},
    Sdp2 = #sdp{medias=[#sdp_m{media= <<"image">>,port=1001}]},
    Sdp3 = #sdp{medias=[#sdp_m{media= <<"audio">>,port=1002,fmt=[<<"0">>],attributes=[{<<"sendrecv">>,[]}]},
                        #sdp_m{media= <<"image">>,port=0}]},
    Sdp4 = #sdp{medias=[#sdp_m{media= <<"audio">>,port=1002,fmt=[<<"0">>,<<"101">>],attributes=[{<<"rtpmap">>,[<<"101">>,<<"telephone-event/8000">>]},{<<"sendrecv">>,[]}]}]},
    Sdp5 = #sdp{medias=[#sdp_m{media= <<"audio">>,port=0,fmt=[<<"0">>,<<"101">>],attributes=[{<<"rtpmap">>,[<<"101">>,<<"telephone-event/8000">>]},{<<"sendrecv">>,[]}]}]},
    Sdp6 = #sdp{medias=[#sdp_m{media= <<"audio">>,port=0,fmt=[<<"0">>],attributes=[{<<"sendrecv">>,[]}]},
                        #sdp_m{media= <<"image">>,port=0}]},
    Sdp7 = #sdp{medias=[#sdp_m{media= <<"audio">>,port=1002,fmt=[<<"8">>],attributes=[{<<"sendrecv">>,[]}]},
                        #sdp_m{media= <<"image">>,port=1001}]},
    Sdp8 = #sdp{medias=[#sdp_m{media= <<"video">>,port=1003,fmt=[<<"100">>],attributes=[{<<"rtpmap">>,[<<"100">>,<<"H264/90000">>]}]},
                        #sdp_m{media= <<"image">>,port=1001}]},
    Sdp9 = #sdp{medias=[]},
    Sdp10 = #sdp{medias=[#sdp_m{media= <<"audio">>,port=1002,fmt=[<<"18">>,<<"0">>],attributes=[{<<"rtpmap">>,[<<"18">>,<<"G729/8000">>]}]}]},
    {"SDP check_update_t38_t30_rfc2833_audio test",
     [% check t38
      ?_assertEqual(true,?M_SDP:check_t38(Sdp1)),
      ?_assertEqual(true,?M_SDP:check_t38(Sdp2)),
      ?_assertEqual(false,?M_SDP:check_t38(Sdp3)),
      ?_assertEqual(false,?M_SDP:check_t38(Sdp4)),
      ?_assertEqual(false,?M_SDP:check_t38(Sdp5)),
      ?_assertEqual(false,?M_SDP:check_t38(Sdp6)),
      ?_assertEqual(false,?M_SDP:check_t38(Sdp7)),
      ?_assertEqual(true,?M_SDP:check_t38(Sdp8)),
      ?_assertEqual(false,?M_SDP:check_t38(Sdp9)),
      % check t30
      ?_assertEqual(false,?M_SDP:check_t30(Sdp1)),
      ?_assertEqual(false,?M_SDP:check_t30(Sdp2)),
      ?_assertEqual(true,?M_SDP:check_t30(Sdp3)),
      ?_assertEqual(true,?M_SDP:check_t30(Sdp4)),
      ?_assertEqual(false,?M_SDP:check_t30(Sdp5)),
      ?_assertEqual(true,?M_SDP:check_t30(Sdp7)),
      ?_assertEqual(false,?M_SDP:check_t30(Sdp8)),
      ?_assertEqual(false,?M_SDP:check_t30(Sdp9)),
      ?_assertEqual(false,?M_SDP:check_t30(Sdp10)),
      % check audio
      ?_assertEqual(false,?M_SDP:check_audio(Sdp1)),
      ?_assertEqual(false,?M_SDP:check_audio(Sdp2)),
      ?_assertEqual(true,?M_SDP:check_audio(Sdp3)),
      ?_assertEqual(true,?M_SDP:check_audio(Sdp4)),
      ?_assertEqual(false,?M_SDP:check_audio(Sdp5)),
      ?_assertEqual(false,?M_SDP:check_audio(Sdp8)),
      ?_assertEqual(false,?M_SDP:check_audio(Sdp9)),
      ?_assertEqual(true,?M_SDP:check_audio(Sdp10)),
      % check rfc2833
      ?_assertEqual(false,?M_SDP:check_rfc2833(Sdp1)),
      ?_assertEqual(true,?M_SDP:check_rfc2833(Sdp4)),
      ?_assertEqual(true,?M_SDP:check_rfc2833(Sdp5)), % @ port no check
      ?_assertEqual(false,?M_SDP:check_rfc2833(Sdp7)),
      ?_assertEqual(false,?M_SDP:check_rfc2833(Sdp8)),
      ?_assertEqual(false,?M_SDP:check_rfc2833(Sdp9)),
      % update t38
      ?_assertEqual(true,?M_SDP:check_t38(?M_SDP:update_by_t38(Sdp1))),
      ?_assertEqual(true,?M_SDP:check_t38(?M_SDP:update_by_t38(Sdp2))),
      ?_assertEqual(true,?M_SDP:check_t38(?M_SDP:update_by_t38(Sdp3))),
      ?_assertEqual(true,?M_SDP:check_t38(?M_SDP:update_by_t38(Sdp4))),
      ?_assertEqual(true,?M_SDP:check_t38(?M_SDP:update_by_t38(Sdp5))),
      ?_assertEqual(true,?M_SDP:check_t38(?M_SDP:update_by_t38(Sdp6))),
      ?_assertEqual(true,?M_SDP:check_t38(?M_SDP:update_by_t38(Sdp7))),
      ?_assertEqual(true,?M_SDP:check_t38(?M_SDP:update_by_t38(Sdp8))),
      ?_assertEqual(true,?M_SDP:check_t38(?M_SDP:update_by_t38(Sdp9))),
      % update t30
      ?_assertEqual(false,?M_SDP:check_t30(?M_SDP:update_by_t30(Sdp1))), % @port 0
      ?_assertEqual(true,?M_SDP:check_t30(?M_SDP:update_by_t30(Sdp2))),
      ?_assertEqual(true,?M_SDP:check_t30(?M_SDP:update_by_t30(Sdp3))),
      ?_assertEqual(true,?M_SDP:check_t30(?M_SDP:update_by_t30(Sdp4))),
      ?_assertEqual(false,?M_SDP:check_t30(?M_SDP:update_by_t30(Sdp5))), % @port 0
      ?_assertEqual(true,?M_SDP:check_t30(?M_SDP:update_by_t30(Sdp8))),
      ?_assertEqual(true,?M_SDP:check_t30(?M_SDP:update_by_t30(Sdp9))),
      ?_assertEqual(true,?M_SDP:check_t30(?M_SDP:update_by_t30(Sdp10)))
      ]}.

% ok
dtmf_rfc2833_to_info_test_() ->
    {"SDP dtmf_rfc2833_to_info test",
     [?_assertEqual("10",?M_SDP:dtmf_rfc2833_to_info($*)),
      ?_assertEqual("11",?M_SDP:dtmf_rfc2833_to_info($#)),
      ?_assertEqual("12",?M_SDP:dtmf_rfc2833_to_info($A)),
      ?_assertEqual("13",?M_SDP:dtmf_rfc2833_to_info($B)),
      ?_assertEqual("14",?M_SDP:dtmf_rfc2833_to_info($C)),
      ?_assertEqual("15",?M_SDP:dtmf_rfc2833_to_info($D)),
      ?_assertEqual("7",?M_SDP:dtmf_rfc2833_to_info($7))
      ]}.

set_sess_test_() ->
    {"SDP set_sess test",
     [?_assertMatch(#sdp{session= <<"TestStream">>},?M_SDP:set_sess(construct_sdp([])))]}.

% ok
make_sdp_test_() ->
    F = fun(#sdp{medias=Ms}) -> lists:foldl(fun(#sdp_m{media=M},Acc) -> ordsets:add_element(M,Acc) end, [], Ms) end,
    {"SDP make_sdp test",
     [?_assertMatch(true,lists:member(<<"audio">>,F(?M_SDP:make_options_sdp()))),
      ?_assertMatch(true,lists:member(<<"audio">>,F(?M_SDP:make_offer_sdp()))),
      ?_assertMatch([<<"audio">>,<<"video">>],F(?M_SDP:make_offer_sdp([audio,video])))
      ]}.

% -----
% ok
cmp_lists_diff_test_() ->
    {"SDP cmp_lists_diff test",
     [?_assertEqual([],?M_SDP:cmp_lists_diff([],[])),
      ?_assertEqual([],?M_SDP:cmp_lists_diff([a,b],[b,a])),
      ?_assertEqual([{del,[a,b]}],?M_SDP:cmp_lists_diff([a,b],[])),
      ?_assertEqual([{add,[a,b]}],?M_SDP:cmp_lists_diff([],[a,b])),
      ?_assertEqual([{del,[a,d]}],?M_SDP:cmp_lists_diff([a,b,c,d],[c,b])),
      ?_assertEqual([{del,[b]},{add,[d,c]}],?M_SDP:cmp_lists_diff([a,b],[d,a,c])),
      ?_assertEqual([{del,[a,d]},{add,[x,y]}],?M_SDP:cmp_lists_diff([a,b,c,d],[x,b,c,y])),
      ?_assertEqual([{del,[a,b,c]},{add,[x,y,z]}],?M_SDP:cmp_lists_diff([a,b,c],[x,y,z]))
      ]}.

% ok
cmp_lists_same_test_() ->
    {"SDP cmp_lists_same test",
     [?_assertEqual([],?M_SDP:cmp_lists_same([],[])),
      ?_assertEqual([a,b],?M_SDP:cmp_lists_same([a,b],[b,a])),
      ?_assertEqual([],?M_SDP:cmp_lists_same([a,b],[])),
      ?_assertEqual([],?M_SDP:cmp_lists_same([],[a,b])),
      ?_assertEqual([b,c],?M_SDP:cmp_lists_same([a,b,c,d],[c,b])),
      ?_assertEqual([a],?M_SDP:cmp_lists_same([a,b],[d,a,c])),
      ?_assertEqual([b,c],?M_SDP:cmp_lists_same([a,b,c,d],[x,b,c,y])),
      ?_assertEqual([],?M_SDP:cmp_lists_same([a,b,c],[x,y,z]))
      ]}.

% ok
filter_attrs_by_fmts_test_() ->
    Attrs = [{<<"x">>,[<<"9">>,<<"test">>]},
             {<<"fmtp">>,[<<"9">>,<<"test">>]},
             {<<"rtpmap">>,[<<"9">>,<<"test">>]},
             {<<"rtcp-fb">>,[<<"9">>,<<"test">>]},
             {<<"y">>,[<<"18">>,<<"test">>]},
             {<<"fmtp">>,[<<"18">>,<<"test">>]},
             {<<"rtpmap">>,[<<"18">>,<<"test">>]},
             {<<"rtcp-fb">>,[<<"18">>,<<"test">>]},
             {<<"fmtp">>,[<<"97">>,<<"test">>]}],
    R1 = lists:reverse(lists:nthtail(4,lists:reverse(Attrs))),
    R2 = [lists:nth(1,Attrs),lists:nth(5,Attrs),lists:nth(9,Attrs)],
    R4 = [lists:nth(1,Attrs),lists:nth(5,Attrs)],
    {"SDP filter_attrs_by_fmts test",
     [?_assertEqual(R1,?M_SDP:filter_attrs_by_fmts(leave,Attrs,[<<"9">>])),
      ?_assertEqual(R2,?M_SDP:filter_attrs_by_fmts(del,Attrs,[<<"9">>,<<"18">>])),
      ?_assertEqual(Attrs,?M_SDP:filter_attrs_by_fmts(leave,Attrs,[<<"9">>,<<"18">>,<<"97">>])),
      ?_assertEqual(R4,?M_SDP:filter_attrs_by_fmts(leave,Attrs,[])),
      ?_assertEqual(Attrs,?M_SDP:filter_attrs_by_fmts(del,Attrs,[]))
      ]}.

% -----


% ok
extract_rtpmaps_as_fmt_test_() ->
    {Fmts1,Attrs1} = {[<<"0">>,<<"98">>,<<"101">>],
                      [{<<"rtpmap">>,[<<"98">>,<<"speex/8000">>]},{<<"rtpmap">>,[<<"101">>,<<"Telephone-Event/8000">>]}]},
    {Fmts2,Attrs2} = {[<<"0">>,<<"98">>,<<"101">>],
                      [{<<"rtpmap">>,[<<"0">>,<<"PCMU/8000">>]},{<<"rtpmap">>,[<<"98">>,<<"speex/8000">>]},{<<"rtpmap">>,[<<"101">>,<<"Telephone-Event/8000">>]}]},
    {"SDP extract_rtpmaps_as_fmt test",
     [?_assertEqual([{<<"98">>,<<"speex/8000">>},{<<"101">>,<<"Telephone-Event/8000">>}],?M_SDP:extract_rtpmaps_as_fmt(Fmts1,Attrs1)),
      ?_assertEqual([{<<"0">>,<<"PCMU/8000">>},{<<"98">>,<<"speex/8000">>},{<<"101">>,<<"Telephone-Event/8000">>}],?M_SDP:extract_rtpmaps_as_fmt(Fmts2,Attrs2))
      ]}.

% ok
restore_orig_case_test_() ->
    {Cap1,Orig1} = {[{<<"0">>,<<"ABCD">>},{<<"99">>,<<"QWER">>}], [{<<"0">>,<<"abcd">>},{<<"99">>,<<"qwer">>}]},
    {Cap2,Orig2} = {[{<<"0">>,<<"ABCD">>},{<<"99">>,<<"QWER">>}], [{<<"0">>,<<"abcd">>},{<<"98">>,<<"zxcv">>},{<<"99">>,<<"qwer">>}]},
    {Cap3,Orig3} = {[{<<"0">>,<<"Abcd">>},{<<"99">>,<<"qwer">>}], [{<<"0">>,<<"abcd">>},{<<"98">>,<<"ZXCV">>},{<<"99">>,<<"QWER">>}]},
    {Cap4,Orig4} = {[{<<"0">>,<<"Abcd">>},{<<"99">>,<<"QWER">>}], [{<<"0">>,<<"abcd">>},{<<"98">>,<<"ZXCV">>}]},
    Cap5 = [{<<"0">>,<<"pCmU/8000">>},{<<"500">>,<<"SPeex/8000">>},{<<"501">>,<<"TeSt">>}],
    {"SDP restore_orig_case test",
     [?_assertEqual([{<<"0">>,<<"abcd">>},{<<"99">>,<<"qwer">>}],?M_SDP:restore_orig_case(Cap1,Orig1)),
      ?_assertEqual([{<<"0">>,<<"abcd">>},{<<"99">>,<<"qwer">>}],?M_SDP:restore_orig_case(Cap2,Orig2)),
      ?_assertEqual([{<<"0">>,<<"abcd">>},{<<"99">>,<<"QWER">>}],?M_SDP:restore_orig_case(Cap3,Orig3)),
      ?_assertEqual([{<<"0">>,<<"abcd">>},{<<"99">>,<<"QWER">>}],?M_SDP:restore_orig_case(Cap4,Orig4)),
      ?_assertEqual([{<<"0">>,<<"PCMU/8000">>},{<<"500">>,<<"speex/8000">>},{<<"501">>,<<"TeSt">>}],?M_SDP:restore_known_case(<<"audio">>,Cap5))
      ]}.

% ok
max_payload_test_() ->
    T1 = [{<<"76">>, <<"test">>, [], xfun},
          {<<"104">>, <<"G726-32/8000">>, [], xfun},
          {<<"105">>, <<"G726-40/8000">>, [], xfun}],
    T2 = [{<<"76">>, <<"test">>},
          {<<"104">>, <<"G726-32/8000">>},
          {<<"105">>, <<"G726-40/8000">>}],
    T3 = [<<"76">>,<<"104">>,<<"105">>],
    {"SDP max_payload test",
     [?_assertEqual(505,?M_SDP:max_payload([<<"8">>,<<"101">>,<<"500">>,<<"505">>], T1)),
      ?_assertEqual(77,?M_SDP:max_payload([<<"0">>,<<"8">>], [lists:nth(1,T1)])),
      ?_assertEqual(105,?M_SDP:max_payload([<<"0">>,<<"101">>], T1)),
      %
      ?_assertEqual(505,?M_SDP:max_payload([{<<"8">>,<<"PCMU/8000">>},{<<"505">>,<<"test">>}], T2)),
      ?_assertEqual(77,?M_SDP:max_payload([<<"0">>,{<<"8">>,<<"PCMA/8000">>}], [lists:nth(1,T2)])),
      ?_assertEqual(105,?M_SDP:max_payload([{<<"0">>,<<"PCMU/8000">>,[],xfun},<<"101">>], T3))
      ]}.

%
capitalize_test_() ->
    T1 = [{<<"76">>, <<"test">>, [], xfun},
          {<<"104">>, <<"g726-32/8000">>, [], xfun}],
    T2 = [{<<"76">>, <<"test">>},
          {<<"104">>, <<"g726-32/8000">>},
          {<<"105">>, <<"G726-40/8000">>}],
    {"SDP capitalize test",
     [?_assertEqual(<<"ABCD/9000">>, ?M_SDP:capitalize(<<"aBcD/9000">>)),
      ?_assertEqual(<<"ABCD/9000">>, ?M_SDP:capitalize(<<"ABCD/9000">>)),
      ?_assertEqual([{<<"76">>,<<"TEST">>,[],xfun},{<<"104">>,<<"G726-32/8000">>,[],xfun}], ?M_SDP:capitalize_names(T1)),
      ?_assertEqual([{<<"76">>,<<"TEST">>},{<<"104">>,<<"G726-32/8000">>},{<<"105">>,<<"G726-40/8000">>}], ?M_SDP:capitalize_names(T2))
      ]}.

% ok
adapt_dynamic_payloads_test_() ->
    {T1,B1} = {[{<<"99">>,<<"abc">>}], [{<<"99">>,<<"abc">>}]},
    {T2,B2} = {[{<<"99">>,<<"abc">>}], [{<<"100">>,<<"abc">>}]},
    {T3,B3} = {[{<<"99">>,<<"abc">>}], [{<<"100">>,<<"abc">>},{<<"101">>,<<"zxc">>}]},
    {T4,B4} = {[{<<"99">>,<<"abc">>}], [{<<"100">>,<<"abc">>},{<<"99">>,<<"zxc">>}]},
    {T5,B5} = {[{<<"99">>,<<"aBc">>}], [{<<"100">>,<<"ABC">>}]},
    {T6,B6} = {[{<<"99">>,<<"aBc">>}], [{<<"100">>,<<"AbC">>}]},
    {T7,B7} = {[{<<"0">>,<<"ZxC">>},{<<"99">>,<<"AbC">>}], [{<<"0">>,<<"zXc">>},{<<"99">>,<<"aBc">>}]},
    {T8,B8} = {[{<<"98">>,<<"ZxC">>},{<<"99">>,<<"AbC">>}], [{<<"99">>,<<"zXc">>},{<<"98">>,<<"aBc">>},{<<"105">>,<<"test">>}]},
    {T9,B9} = {[{<<"0">>,<<"zxc">>},{<<"97">>,<<"abc">>},{<<"98">>,<<"QwE">>},{<<"99">>,<<"ASD">>}],
               [{<<"96">>,<<"qwe">>},{<<"97">>,<<"AbC">>},{<<"98">>,<<"ASD">>},{<<"99">>,<<"ttt">>},{<<"105">>,<<"qqq">>}]},
    {T10,B10} = {[{<<"98">>,<<"xxx">>},{<<"99">>,<<"ZxC">>},{<<"100">>,<<"AbC">>}], [{<<"98">>,<<"zXc">>},{<<"99">>,<<"aBc">>},{<<"105">>,<<"test">>}]},
    {"SDP adapt_dynamic_payloads test",
     [?_assertEqual([{<<"99">>,<<"abc">>}], ?M_SDP:adapt_dynamic_payloads(T1,B1)),
      ?_assertEqual([{<<"100">>,<<"abc">>}], ?M_SDP:adapt_dynamic_payloads(T2,B2)),
      ?_assertEqual([{<<"100">>,<<"abc">>}], ?M_SDP:adapt_dynamic_payloads(T3,B3)),
      ?_assertEqual([{<<"100">>,<<"abc">>}], ?M_SDP:adapt_dynamic_payloads(T4,B4)),
      ?_assertEqual([{<<"100">>,<<"aBc">>}], ?M_SDP:adapt_dynamic_payloads(T5,B5)),
      ?_assertEqual([{<<"100">>,<<"aBc">>}], ?M_SDP:adapt_dynamic_payloads(T6,B6)),
      ?_assertEqual([{<<"0">>,<<"ZxC">>},{<<"99">>,<<"AbC">>}], ?M_SDP:adapt_dynamic_payloads(T7,B7)),
      ?_assertEqual([{<<"99">>,<<"ZxC">>},{<<"98">>,<<"AbC">>}], ?M_SDP:adapt_dynamic_payloads(T8,B8)),
      ?_assertEqual([{<<"0">>,<<"zxc">>},{<<"97">>,<<"abc">>},{<<"96">>,<<"QwE">>},{<<"98">>,<<"ASD">>}], ?M_SDP:adapt_dynamic_payloads(T9,B9)),
      %?_assertEqual([{<<"106">>,<<"xxx">>},{<<"98">>,<<"ZxC">>},{<<"99">>,<<"AbC">>}], ?M_SDP:adapt_dynamic_payloads(T10,B10))
      ?_assertEqual([{<<"96">>,<<"xxx">>},{<<"98">>,<<"ZxC">>},{<<"99">>,<<"AbC">>}], ?M_SDP:adapt_dynamic_payloads(T10,B10))
      ]}.

% -----
% ok
reduce_media_fmts_to_single_test_() ->
    Attrs = [{<<"rtpmap">>,[<<"0">>,<<"PCMU/8000">>]},
             {<<"rtpmap">>,[<<"8">>,<<"PCMA/8000">>]},
             {<<"rtpmap">>,[<<"9">>,<<"G722/8000">>]},
             {<<"rtpmap">>,[<<"18">>,<<"G729/8000">>]},
             {<<"rtpmap">>,[<<"97">>,<<"SPEEX/8000">>]},
             {<<"rtpmap">>,[<<"13">>,<<"CN/8000">>]},
             {<<"rtpmap">>,[<<"101">>,<<"telephone-event/8000">>]}],
    {FmtsA1,FmtsO1} = {[<<"0">>,<<"8">>,<<"13">>,<<"18">>,<<"97">>,<<"101">>], [<<"13">>,<<"18">>,<<"8">>,<<"101">>]},
    {FmtsA2,FmtsO2} = {[<<"0">>,<<"13">>,<<"18">>,<<"97">>,<<"101">>], [<<"8">>,<<"18">>,<<"101">>]},
    {FmtsA3,FmtsO3} = {[<<"0">>,<<"13">>,<<"97">>,<<"9">>,<<"101">>], [<<"8">>,<<"18">>,<<"101">>]},
    {FmtsA4,FmtsO4} = {[<<"0">>,<<"8">>,<<"18">>,<<"9">>,<<"101">>], [<<"8">>,<<"18">>]},
    {FmtsA5,FmtsO5} = {[<<"0">>,<<"8">>,<<"18">>,<<"9">>,<<"101">>], [<<"200">>,<<"8">>,<<"18">>]},
    %
    MediaN = <<"audio">>,
    Known = ?M_SDP:known_formats(static,MediaN) ++ ?M_SDP:known_formats(dynamic,MediaN),
    Fsdpm1 = fun(Fmts) -> #sdp_m{media=MediaN,fmt=Fmts,attributes=[{<<"rtpmap">>,[P,N,<<>>]} || {P,N,_,_} <- Known, lists:member(P,Fmts)]} end,
    SdpM = #sdp_m{media=MediaN,fmt=[<<"0">>,<<"8">>,<<"9">>,<<"18">>,<<"97">>,<<"13">>,<<"101">>],attributes=Attrs},
    %
    {"SDP reduce_audio_fmts_to_single test",
     [?_assertEqual([<<"18">>,<<"13">>,<<"101">>],?M_SDP:reduce_media_fmts_to_single(MediaN,SdpM,Fsdpm1(FmtsO1),Fsdpm1(FmtsA1))),
      ?_assertEqual([<<"18">>,<<"101">>],?M_SDP:reduce_media_fmts_to_single(MediaN,SdpM,Fsdpm1(FmtsO2),Fsdpm1(FmtsA2))),
      ?_assertEqual([<<"8">>,<<"101">>],?M_SDP:reduce_media_fmts_to_single(MediaN,SdpM,Fsdpm1(FmtsO3),Fsdpm1(FmtsA3))),
      ?_assertEqual([<<"8">>],?M_SDP:reduce_media_fmts_to_single(MediaN,SdpM,Fsdpm1(FmtsO4),Fsdpm1(FmtsA4))),
      ?_assertEqual([<<"8">>],?M_SDP:reduce_media_fmts_to_single(MediaN,SdpM,Fsdpm1(FmtsO5),Fsdpm1(FmtsA5)))
      ]}.

% ok
reduce_answer_to_one_general_fmt_test_() ->
    SdpAudio = construct_sdp([]),
    SdpNoAudio = construct_sdp([{audio,false},{video,false}]),
    {Sdp1,Sdp1A,Sdp1O} = {SdpNoAudio,SdpAudio,SdpAudio},
    {Sdp2,Sdp2A,Sdp2O} = {SdpAudio,SdpAudio,SdpNoAudio},
    {Sdp3,Sdp3A,Sdp3O} = {SdpAudio,SdpNoAudio,SdpAudio},
    {Sdp4,Sdp4A,Sdp4O} = {SdpAudio,SdpAudio,SdpAudio},
    {"SDP reduce_answer_to_one_audio_fmt test",
     [?_assertEqual(Sdp1,?M_SDP:reduce_answer_to_one_general_fmt(Sdp1,Sdp1A,Sdp1O)),
      ?_assertEqual(Sdp2,?M_SDP:reduce_answer_to_one_general_fmt(Sdp2,Sdp2A,Sdp2O)),
      ?_assertEqual(Sdp3,?M_SDP:reduce_answer_to_one_general_fmt(Sdp3,Sdp3A,Sdp3O)),
      ?_assertEqual([<<"9">>,<<"101">>,<<"13">>],(lists:nth(1,(?M_SDP:reduce_answer_to_one_general_fmt(Sdp4,Sdp4A,Sdp4O))#sdp.medias))#sdp_m.fmt)
      ]}.

% -----

% ok
known_formats_test_() ->
    DynA = ?M_SDP:known_formats(dynamic,<<"audio">>),
    {"SDP check_known_fmt test",
     [?_assertMatch({_,_,_,_},lists:keyfind(<<"speex/8000">>,2,DynA)),
      ?_assertMatch({_,_,_,_},lists:keyfind(<<"speex/16000">>,2,DynA)),
      ?_assertMatch({_,_,_,_},lists:keyfind(<<"speex/32000">>,2,DynA)),
      ?_assertMatch({_,_,[_|_],_},lists:keyfind(<<"telephone-event/8000">>,2,DynA)),
      ?_assertMatch({_,_,[_|_],_},lists:keyfind(<<"iLBC/8000">>,2,DynA)),
      ?_assertMatch({_,_,[_|_],_},lists:keyfind(<<"opus/48000/2">>,2,DynA)),
      ?_assertMatch({_,_,_,_},lists:keyfind(<<"G726-16/8000">>,2,DynA)),
      ?_assertMatch({_,_,_,_},lists:keyfind(<<"G726-24/8000">>,2,DynA)),
      ?_assertMatch({_,_,_,_},lists:keyfind(<<"G726-32/8000">>,2,DynA)),
      ?_assertMatch({_,_,_,_},lists:keyfind(<<"G726-40/8000">>,2,DynA)),
      ?_assertEqual(ok,lists:foldl(fun({A,B,C,D},ok) when is_binary(A),is_binary(B),is_list(C),is_function(D) -> ok; (_,_) -> false end,ok,DynA))
      ]}.

% ok
modify_known_dynamic_payloads_test_() ->
    KnownDynA = ?M_SDP:known_formats(dynamic,<<"audio">>),
    KnownDynV = ?M_SDP:known_formats(dynamic,<<"video">>),
    {PLte,_,_,_}=Ete = lists:keyfind(<<"telephone-event/8000">>,2,KnownDynA),
    {PLspeex8,_,_,_}=Espeex8 = lists:keyfind(<<"speex/8000">>,2,KnownDynA),
    {PLtest1,PLtest2} = {<<"500">>,<<"501">>},
    {Fmt1,Att1} = {[PLte],[{<<"rtpmap">>,[PLte,<<"telephone-event/8000">>]}]},
    {Fmt2,Att2} = {[PLspeex8,PLte],[{<<"rtpmap">>,[PLspeex8,<<"speex/8000">>]},{<<"rtpmap">>,[PLte,<<"Telephone-event/8000">>]}]},
    {Fmt3,Att3} = {[PLtest1,PLte],[{<<"rtpmap">>,[PLtest1,<<"SPEEX/8000">>]},{<<"rtpmap">>,[PLte,<<"telephone-event/8000">>]}]},
    {Fmt4,Att4} = {[PLtest1,PLtest2],[{<<"rtpmap">>,[PLtest1,<<"speex/8000">>]},{<<"rtpmap">>,[PLtest2,<<"Telephone-Event/8000">>]}]},
    {Fmt5,Att5} = {[PLte,PLspeex8],[{<<"rtpmap">>,[PLte,<<"speex/8000">>]},{<<"rtpmap">>,[PLspeex8,<<"telephone-event/8000">>]}]},
    {Fmt6,Att6} = {[<<"96">>, <<"97">>, <<"98">>, <<"99">>, <<"100">>, <<"101">>, <<"102">>, <<"123">>, <<"127">>, <<"122">>, <<"125">>, <<"107">>, <<"108">>, <<"109">>, <<"124">>],
                   [{<<"rtpmap">>,[<<"96">>,<<"VP8/90000">>]},
                    {<<"rtpmap">>,[<<"97">>,<<"rtx/90000">>]},
                    {<<"rtpmap">>,[<<"98">>,<<"VP9/90000">>]},
                    {<<"rtpmap">>,[<<"99">>,<<"rtx/90000">>]},
                    {<<"rtpmap">>,[<<"100">>,<<"H264/90000">>]},
                    {<<"rtpmap">>,[<<"101">>,<<"rtx/90000">>]},
                    {<<"rtpmap">>,[<<"102">>,<<"H264/90000">>]},
                    {<<"rtpmap">>,[<<"123">>,<<"rtx/90000">>]},
                    {<<"rtpmap">>,[<<"127">>,<<"H264/90000">>]},
                    {<<"rtpmap">>,[<<"122">>,<<"rtx/90000">>]},
                    {<<"rtpmap">>,[<<"125">>,<<"H264/90000">>]},
                    {<<"rtpmap">>,[<<"107">>,<<"rtx/90000">>]},
                    {<<"rtpmap">>,[<<"108">>,<<"red/90000">>]},
                    {<<"rtpmap">>,[<<"109">>,<<"rtx/90000">>]},
                    {<<"rtpmap">>,[<<"124">>,<<"ulpfec/90000">>]}]},
    {"SDP modify_known_dynamic_payloads test",
     [?_assertEqual(lists:sort(KnownDynA),lists:sort(?M_SDP:modify_known_dynamic_payloads({Fmt1,Att1}, KnownDynA))),
      ?_assertEqual([Espeex8,Ete],?M_SDP:modify_known_dynamic_payloads({Fmt1,Att1}, [Espeex8,Ete])),
      ?_assertEqual([Espeex8,Ete],?M_SDP:modify_known_dynamic_payloads({Fmt2,Att2}, [Espeex8,Ete])),
      ?_assertEqual(lists:sort([Ete,setelement(1,Espeex8,PLtest1)]),lists:sort(?M_SDP:modify_known_dynamic_payloads({Fmt3,Att3}, [Espeex8,Ete]))),
      ?_assertEqual(lists:sort([setelement(1,Espeex8,PLtest1),setelement(1,Ete,PLtest2)]),lists:sort(?M_SDP:modify_known_dynamic_payloads({Fmt4,Att4}, [Espeex8,Ete]))),
      ?_assertEqual(lists:sort([setelement(1,Espeex8,PLte),setelement(1,Ete,PLspeex8)]),lists:sort(?M_SDP:modify_known_dynamic_payloads({Fmt5,Att5}, [Espeex8,Ete]))),
      %?_assertEqual([<<"128">>,<<"100">>,<<"102">>,<<"127">>,<<"125">>,<<"96">>,<<"98">>],lists:map(fun({P,_,_,_}) -> P end, ?M_SDP:modify_known_dynamic_payloads({Fmt6,Att6}, KnownDynV)))
      ?_assertEqual([<<"103">>,<<"100">>,<<"102">>,<<"127">>,<<"125">>,<<"96">>,<<"98">>],lists:map(fun({P,_,_,_}) -> P end, ?M_SDP:modify_known_dynamic_payloads({Fmt6,Att6}, KnownDynV)))
      ]}.

% ok
check_known_fmt_test_() ->
    KnownDyn = ?M_SDP:known_formats(dynamic,<<"audio">>),
    {PLte,_,_,_} = lists:keyfind(<<"telephone-event/8000">>,2,KnownDyn),
    Sdp_101 = #sdp{medias=[#sdp_m{media= <<"audio">>,fmt=[PLte],attributes=[{<<"rtpmap">>,[PLte,<<"Telephone-event/8000">>]}]}]},
    Sdp_13_101 = #sdp{medias=[#sdp_m{media= <<"audio">>,fmt=[<<"13">>,PLte],attributes=[{<<"rtpmap">>,[PLte,<<"telephone-event/8000">>]}, {<<"rtpmap">>,[<<"13">>,<<"CN/8000">>]}]}]},
    Sdp_8_0_101 = #sdp{medias=[#sdp_m{media= <<"audio">>,fmt=[<<"8">>,<<"0">>,PLte],attributes=[{<<"rtpmap">>,[PLte,<<"telephone-event/8000">>]}]}]},
    Sdp_noaudio = #sdp{medias=[#sdp_m{media= <<"video">>,fmt=[<<"100">>],attributes=[{<<"rtpmap">>,[<<"100">>,<<"H264/90000">>]}]}]},
    Sdp_empty = #sdp{medias=[]},
    Sdp_av = #sdp{medias=[#sdp{medias=[#sdp_m{media= <<"audio">>,fmt=[<<"8">>,PLte],attributes=[{<<"rtpmap">>,[PLte,<<"telephone-event/8000">>]}]}]},#sdp_m{media= <<"video">>,fmt=[<<"100">>],attributes=[{<<"rtpmap">>,[<<"100">>,<<"H264/90000">>]}]}]},
    {PLspeex8000,_,_,_} = lists:keyfind(<<"speex/8000">>,2,?M_SDP:known_formats(dynamic,<<"audio">>)),
    Sdp_dyn = #sdp{medias=[#sdp_m{media= <<"audio">>,fmt=[PLspeex8000,PLte],attributes=[{<<"rtpmap">>,[PLspeex8000,<<"SPEEX/8000">>]},{<<"rtpmap">>,[PLte,<<"telephone-event/8000">>]}]}]},
    Sdp_dyn_unknown = #sdp{medias=[#sdp_m{media= <<"audio">>,fmt=[<<"200">>,PLte],attributes=[{<<"rtpmap">>,[<<"200">>,<<"test">>]},{<<"rtpmap">>,[PLte,<<"telephone-event/8000">>]}]}]},
    Sdp_dyn_unknown_used = #sdp{medias=[#sdp_m{media= <<"audio">>,fmt=[PLspeex8000,PLte],attributes=[{<<"rtpmap">>,[PLspeex8000,<<"test">>]},{<<"rtpmap">>,[PLte,<<"telephone-event/8000">>]}]}]},
    Sdp_unknownvideo = #sdp{medias=[#sdp_m{media= <<"video">>,fmt=[<<"100">>],attributes=[{<<"rtpmap">>,[<<"100">>,<<"unknown">>]}]}]},
    Sdp_emptyvideo = #sdp{medias=[#sdp_m{media= <<"video">>,fmt=[],attributes=[]}]},
    {"SDP check_known_fmt test",
     [?_assertEqual(false,?M_SDP:check_known_fmt(Sdp_101)),
      ?_assertEqual(false,?M_SDP:check_known_fmt(Sdp_13_101)),
      ?_assertEqual(true,?M_SDP:check_known_fmt(Sdp_8_0_101)),
      ?_assertEqual(true,?M_SDP:check_known_fmt(Sdp_noaudio)),
      ?_assertEqual(true,?M_SDP:check_known_fmt(Sdp_empty)),
      ?_assertEqual(true,?M_SDP:check_known_fmt(Sdp_av)),
      ?_assertEqual(true,?M_SDP:check_known_fmt(Sdp_dyn)),
      ?_assertEqual(false,?M_SDP:check_known_fmt(Sdp_dyn_unknown)),
      ?_assertEqual(false,?M_SDP:check_known_fmt(Sdp_dyn_unknown_used)),
      %
      ?_assertEqual(true,?M_SDP:check_known_fmt(<<"video">>,Sdp_noaudio)),
      ?_assertEqual(true,?M_SDP:check_known_fmt(<<"video">>,Sdp_101)),
      ?_assertEqual(false,?M_SDP:check_known_fmt(<<"video">>,Sdp_unknownvideo)),
      ?_assertEqual(false,?M_SDP:check_known_fmt(<<"video">>,Sdp_emptyvideo))
      ]}.

% ok
update_by_local_media_fmts_test_() ->
    KnownSta = ?M_SDP:known_formats(static,<<"audio">>),
    KnownDyn = ?M_SDP:known_formats(dynamic,<<"audio">>),
    {PLte,_,_,_} = lists:keyfind(<<"telephone-event/8000">>,2,KnownDyn),
    {PLspeex8,_,_,_} = lists:keyfind(<<"speex/8000">>,2,KnownDyn),
    {PLspeex16,_,_,_} = lists:keyfind(<<"speex/16000">>,2,KnownDyn),
    PLtest = <<"500">>,
    Sdp0 = #sdp{medias=[#sdp_m{media= <<"video">>,fmt=[PLtest],
                               attributes=[{<<"rtpmap">>,[PLtest,<<"H264/90000">>]}]}]},
    Sdp1 = #sdp{medias=[#sdp_m{media= <<"audio">>,fmt=[<<"8">>,PLte],
                               attributes=[{<<"rtpmap">>,[PLte,<<"telephone-event/8000">>]}]}]},
    Sdp2 = #sdp{medias=[#sdp_m{media= <<"audio">>,fmt=[PLspeex8,PLte],
                               attributes=[{<<"rtpmap">>,[PLte,<<"telephone-event/8000">>]},
                                           {<<"rtpmap">>,[PLspeex8,<<"SpEeX/8000">>]}]}]},
    Sdp3 = #sdp{medias=[#sdp_m{media= <<"audio">>,fmt=[<<"8">>,<<"0">>,<<"13">>,PLspeex8,PLspeex16,PLte],
                               attributes=[{<<"rtpmap">>,[PLte,<<"telephone-event/8000">>]},
                                           {<<"rtpmap">>,[PLspeex8,<<"speex/8000">>]},
                                           {<<"rtpmap">>,[PLspeex16,<<"speex/16000">>]}]}]},
    Sdp4 = #sdp{medias=[#sdp_m{media= <<"audio">>,fmt=[<<"8">>,PLspeex8,PLte],
                               attributes=[{<<"rtpmap">>,[PLte,<<"telephone-event/8000">>]},
                                           {<<"rtpmap">>,[PLspeex8,<<"test">>]}]}]},
    Sdp5 = #sdp{medias=[#sdp_m{media= <<"audio">>,fmt=[<<"8">>,PLtest,PLte],
                               attributes=[{<<"rtpmap">>,[PLte,<<"telephone-event/8000">>]},
                                           {<<"rtpmap">>,[PLspeex8,<<"test">>]}]}]},
    Sdp6 = #sdp{medias=[#sdp_m{media= <<"audio">>,fmt=[<<"8">>,PLtest,PLte],
                               attributes=[{<<"rtpmap">>,[PLte,<<"telephone-event/8000">>]},
                                           {<<"rtpmap">>,[PLspeex8,<<"SpEeX/8000">>]}]}]},
    Sdp7 = #sdp{medias=[#sdp_m{media= <<"audio">>,fmt=[<<"8">>,PLtest,PLte],
                               attributes=[{<<"rtpmap">>,[PLte,<<"telephone-event/8000">>]},
                                           {<<"rtpmap">>,[PLtest,<<"speex/8000">>]}]}]},
    R14 = lists:filtermap(fun({P,_,_,_}) when P==<<"8">>;P==PLspeex8;P==PLte-> false; ({P,_,_,_}) -> {true,P} end, KnownSta++KnownDyn),
    Fextract = fun(#sdp{medias=[#sdp_m{fmt=Fmts}]}) -> Fmts end,
    {"SDP update_by_local_media_fmts test",
     [?_assertEqual([PLtest],Fextract(?M_SDP:update_by_local_media_fmts(Sdp0, undefined, [del]))),
      ?_assertEqual([<<"8">>,PLte],Fextract(?M_SDP:update_by_local_media_fmts(Sdp1, undefined, [del]))),
      ?_assertEqual([<<"8">>,PLte],Fextract(?M_SDP:update_by_local_media_fmts(Sdp1, undefined, [del,up]))),
      ?_assertEqual([PLspeex8,PLte],Fextract(?M_SDP:update_by_local_media_fmts(Sdp2, undefined, [del]))),
      ?_assertEqual([PLspeex8,PLte],Fextract(?M_SDP:update_by_local_media_fmts(Sdp2, undefined, [del,up]))),
      ?_assertEqual([<<"8">>,<<"0">>,<<"13">>,PLspeex8,PLspeex16,PLte],Fextract(?M_SDP:update_by_local_media_fmts(Sdp3, undefined, [del]))),
      ?_assertEqual([<<"8">>,<<"0">>,<<"13">>,PLspeex8,PLspeex16,PLte],Fextract(?M_SDP:update_by_local_media_fmts(Sdp3, undefined, [del,up]))),
      ?_assertEqual([<<"8">>,PLte],Fextract(?M_SDP:update_by_local_media_fmts(Sdp4, undefined, [del]))),
      ?_assertEqual([<<"8">>,PLte],Fextract(?M_SDP:update_by_local_media_fmts(Sdp4, undefined, [del,up]))),
      ?_assertEqual([<<"8">>,PLte],Fextract(?M_SDP:update_by_local_media_fmts(Sdp5, undefined, [del]))),
      ?_assertEqual([<<"8">>,PLte],Fextract(?M_SDP:update_by_local_media_fmts(Sdp5, undefined, [del,up]))),
      ?_assertEqual([<<"8">>,PLte],Fextract(?M_SDP:update_by_local_media_fmts(Sdp6, undefined, [del]))),
      ?_assertEqual([<<"8">>,PLte],Fextract(?M_SDP:update_by_local_media_fmts(Sdp6, undefined, [del,up]))),
      ?_assertEqual([<<"8">>,PLtest,PLte],Fextract(?M_SDP:update_by_local_media_fmts(Sdp7, undefined, [del,up]))),
      ?_assertEqual([<<"8">>,PLtest,PLte]++R14,Fextract(?M_SDP:update_by_local_media_fmts(Sdp7, undefined, [add,del,up]))),
      ?_assertEqual([<<"8">>,PLtest,PLte],Fextract(?M_SDP:update_by_local_media_fmts(Sdp7, undefined, []))),
      %
      ?_assertEqual([PLspeex8,PLte],Fextract(?M_SDP:filter_by_local_media_fmts(Sdp2, undefined))),
      ?_assertEqual([<<"8">>,PLte],Fextract(?M_SDP:filter_by_local_media_fmts(Sdp4, undefined))),
      ?_assertEqual([<<"8">>,PLte],Fextract(?M_SDP:filter_by_local_media_fmts(Sdp5, undefined))),
      ?_assertEqual([<<"8">>,PLte],Fextract(?M_SDP:filter_by_local_media_fmts(Sdp6, undefined))),
      %
      ?_assertEqual([<<"8">>,PLtest,PLte]++R14,Fextract(?M_SDP:lo(Sdp7)))
      ]}.

% -----

% ok
reduce_by_offer_test_() ->
    {SdpO1,SdpM1} = {#sdp{medias=[#sdp_m{media= <<"audio">>,fmt=[<<"0">>,<<"8">>,<<"18">>],
                                           attributes=[{<<"rtpmap">>,[<<"8">>,<<"PCMA/8000">>]},{<<"rtpmap">>,[<<"18">>,<<"G729/8000">>]}]}]},
                     #sdp_m{media= <<"audio">>, fmt=[<<"0">>,<<"8">>,<<"18">>],attributes=[{<<"rtpmap">>,[<<"8">>,<<"PCMA/8000">>]},{<<"rtpmap">>,[<<"18">>,<<"G729/8000">>]}]}},
    {SdpO2,SdpM2} = {#sdp{medias=[#sdp_m{media= <<"audio">>,fmt=[<<"0">>,<<"8">>],attributes=[]}]},
                     #sdp_m{media= <<"audio">>, fmt=[<<"0">>,<<"8">>,<<"18">>],attributes=[{<<"rtpmap">>,[<<"18">>,<<"G729/8000">>]}]}},
    {SdpO3,SdpM3} = {#sdp{medias=[#sdp_m{media= <<"audio">>,fmt=[<<"97">>],attributes=[{<<"rtpmap">>,[<<"97">>,<<"speex/8000">>]}]}]},
                     #sdp_m{media= <<"audio">>, fmt=[<<"0">>,<<"8">>,<<"18">>],attributes=[{<<"rtpmap">>,[<<"18">>,<<"G729/8000">>]}]}},
    {SdpO4,SdpM4} = {#sdp{medias=[#sdp_m{media= <<"audio">>,fmt=[<<"99">>],attributes=[{<<"rtpmap">>,[<<"99">>,<<"speex/8000">>]}]}]},
                     #sdp_m{media= <<"audio">>, fmt=[<<"0">>,<<"8">>,<<"18">>],attributes=[{<<"rtpmap">>,[<<"18">>,<<"G729/8000">>]}]}},
    {SdpO5,SdpM5} = {#sdp{medias=[#sdp_m{media= <<"audio">>,fmt=[<<"99">>],attributes=[{<<"rtpmap">>,[<<"99">>,<<"speex/8000">>]}]}]},
                     #sdp_m{media= <<"video">>, fmt=[<<"100">>],attributes=[{<<"rtpmap">>,[<<"100">>,<<"H264/90000">>]}]}},
    {SdpO6,SdpM6} = {#sdp{medias=[#sdp_m{media= <<"image">>}]},
                     #sdp_m{media= <<"image">>}},
    {SdpO7,SdpM7} = {#sdp{medias=[#sdp_m{media= <<"audio">>,fmt=[<<"97">>,<<"98">>],
                                         attributes=[{<<"rtpmap">>,[<<"97">>,<<"speex/8000">>]},
                                                     {<<"rtpmap">>,[<<"98">>,<<"speex/16000">>]}]}]},
                     #sdp_m{media= <<"audio">>, fmt=[<<"0">>,<<"18">>,<<"97">>,<<"98">>],
                            attributes=[{<<"rtpmap">>,[<<"18">>,<<"G729/8000">>]},
                                        {<<"rtpmap">>,[<<"98">>,<<"speex/8000">>]},
                                        {<<"rtpmap">>,[<<"97">>,<<"speex/16000">>]}]}},
    {SdpO8,SdpM8} = {#sdp{medias=[#sdp_m{media= <<"audio">>,fmt=[<<"96">>,<<"97">>,<<"98">>],
                                         attributes=[{<<"rtpmap">>,[<<"96">>,<<"speex/16000">>]},
                                                     {<<"rtpmap">>,[<<"97">>,<<"speex/8000">>]},
                                                     {<<"rtpmap">>,[<<"98">>,<<"telephone-event/8000">>]}]}]},
                     #sdp_m{media= <<"audio">>, fmt=[<<"110">>,<<"101">>],
                            attributes=[{<<"rtpmap">>,[<<"110">>,<<"speex/16000">>]},
                                        {<<"rtpmap">>,[<<"101">>,<<"telephone-event/8000">>]}]}},
    {SdpO9,SdpM9} = {#sdp{medias=[#sdp_m{media= <<"audio">>,fmt=[<<"99">>],attributes=[{<<"rtpmap">>,[<<"99">>,<<"speex/8000">>]}]}]},
                     #sdp_m{media= <<"video">>, fmt=[<<"500">>],attributes=[{<<"rtpmap">>,[<<"500">>,<<"test">>]}]}},
    {SdpO10,SdpM10} = {#sdp{medias=[#sdp_m{media= <<"video">>,fmt=[<<"116">>],attributes=[{<<"rtpmap">>,[<<"116">>,<<"VP8/90000">>]}]}]},
                       #sdp_m{media= <<"video">>, fmt=[<<"116">>],attributes=[{<<"rtpmap">>,[<<"116">>,<<"VP8/90000">>]}]}},
    {SdpO11,SdpM11} = {#sdp{medias=[#sdp_m{media= <<"audio">>,fmt=[<<"96">>,<<"97">>,<<"101">>,<<"98">>],
                                           attributes=[{<<"rtpmap">>,[<<"96">>,<<"speex/16000">>]},
                                                       {<<"rtpmap">>,[<<"97">>,<<"speex/8000">>]},
                                                       {<<"rtpmap">>,[<<"101">>,<<"telephone-event/16000">>]},
                                                       {<<"rtpmap">>,[<<"98">>,<<"telephone-event/8000">>]}]}]},
                       #sdp_m{media= <<"audio">>, fmt=[<<"110">>,<<"98">>],
                              attributes=[{<<"rtpmap">>,[<<"110">>,<<"speex/16000">>]},
                                          {<<"rtpmap">>,[<<"98">>,<<"telephone-event/8000">>]}]}},
    Fextract = fun(#sdp_m{fmt=Fmts}) -> Fmts;
                  (#sdp{medias=[#sdp_m{media= <<"audio">>,fmt=Fmts}|_]}) -> Fmts;
                  (#sdp{}) -> []
               end,
    FextractV = fun(#sdp{medias=[#sdp_m{media= <<"video">>,fmt=Fmts}|_]}) -> Fmts end,
    Frtpmap = fun(#sdp_m{attributes=Attrs}) -> lists:filtermap(fun({<<"rtpmap">>,X}) -> {true,X}; (_) -> false end, Attrs) end,
    {"SDP reduce_by_offer test",
     [?_assertEqual(SdpM1,?M_SDP:reduce_by_offer(SdpM1, undefined)),
      ?_assertEqual([<<"0">>,<<"8">>,<<"18">>],Fextract(?M_SDP:reduce_by_offer(SdpM1, SdpO1))),
      ?_assertEqual([<<"0">>,<<"8">>],Fextract(?M_SDP:reduce_by_offer(SdpM2, SdpO2))),
      ?_assertEqual([],Fextract(?M_SDP:reduce_by_offer(SdpM3, SdpO3))),
      ?_assertEqual([],Fextract(?M_SDP:reduce_by_offer(SdpM4, SdpO4))),
      ?_assertEqual(SdpM5,?M_SDP:reduce_by_offer(SdpM5, SdpO5)),
      % mix of payload
      ?_assertEqual([<<"97">>,<<"98">>],Fextract(?M_SDP:reduce_by_offer(SdpM7, SdpO7))),
      ?_assertEqual([[<<"98">>,<<"speex/8000">>],[<<"97">>,<<"speex/16000">>]],Frtpmap(?M_SDP:reduce_by_offer(SdpM7, SdpO7))),
      ?_assertEqual([[<<"110">>,<<"speex/16000">>],[<<"101">>,<<"telephone-event/8000">>]],Frtpmap(?M_SDP:reduce_by_offer(SdpM8, SdpO8))),
      %
      ?_assertEqual([<<"0">>,<<"8">>,<<"18">>],Fextract(?M_SDP:lo(#sdp{medias=[SdpM1]},SdpO1))),
      ?_assertEqual([<<"0">>,<<"8">>],Fextract(?M_SDP:lo(#sdp{medias=[SdpM2]},SdpO2))),
      ?_assertEqual([<<"97">>],Fextract(?M_SDP:lo(#sdp{medias=[SdpM3]},SdpO3))),
      ?_assertEqual([<<"99">>],Fextract(?M_SDP:lo(#sdp{medias=[SdpM4]},SdpO4))),
      ?_assertEqual([],Fextract(?M_SDP:lo(#sdp{medias=[SdpM5]},SdpO5))),
      %
      ?_assertEqual([<<"0">>,<<"8">>,<<"18">>],Fextract(?M_SDP:lo_reduce(#sdp{medias=[SdpM1]},SdpO1))),
      ?_assertEqual([<<"97">>],Fextract(?M_SDP:lo_reduce(#sdp{medias=[SdpM3]},SdpO3))),
      ?_assertEqual([<<"99">>],Fextract(?M_SDP:lo_reduce(#sdp{medias=[SdpM4]},SdpO4))),
      ?_assertEqual([],Fextract(?M_SDP:lo_reduce(#sdp{medias=[SdpM6]},SdpO6))),
      {setup,
       fun() -> start_param_test_proc() end,
       fun(_) -> stop_param_test_proc() end,
       [?_assertEqual([<<"0">>,<<"8">>,<<"18">>],Fextract(?M_SDP:lo_reduce(#sdp{medias=[SdpM1]},SdpO1))),
        ?_assertEqual([<<"99">>],Fextract(?M_SDP:lo_reduce(#sdp{medias=[SdpM4]},SdpO4)))
        ]},
      {setup,
       fun() -> start_param_test_proc(),
                set_param('allow_ac_set',true),
                set_param({'offer',video},[]),
                set_param({'offer',audio},[<<"PCMA/8000">>,<<"PCMU/8000">>]) end,
       fun(_) -> stop_param_test_proc() end,
       [?_assertEqual([<<"0">>,<<"8">>,<<"18">>],Fextract(?M_SDP:lo_reduce(#sdp{medias=[SdpM1]},SdpO1))),
        ?_assertEqual([<<"99">>],Fextract(?M_SDP:lo_reduce(#sdp{medias=[SdpM4]},SdpO4))),
        ?_assertEqual([<<"100">>],FextractV(?M_SDP:lo_reduce(#sdp{medias=[SdpM5]},SdpO5))),
        ?_assertEqual([],FextractV(?M_SDP:lo_reduce(#sdp{medias=[SdpM9]},SdpO9)))
        ]},
      {setup,
       fun() -> start_param_test_proc(), set_param('allow_ac_set',false) end,
       fun(_) -> stop_param_test_proc() end,
       [?_assertEqual([<<"0">>],Fextract(?M_SDP:lo_reduce(#sdp{medias=[SdpM1]},SdpO1))),
        ?_assertEqual([<<"0">>],Fextract(?M_SDP:lo_reduce(#sdp{medias=[SdpM2]},SdpO2))),
        ?_assertEqual([<<"97">>],Fextract(?M_SDP:lo_reduce(#sdp{medias=[SdpM3]},SdpO3))),
        ?_assertEqual([<<"99">>],Fextract(?M_SDP:lo_reduce(#sdp{medias=[SdpM4]},SdpO4))),
        ?_assertEqual([],Fextract(?M_SDP:lo_reduce(#sdp{medias=[SdpM5]},SdpO5))),
        ?_assertEqual([<<"110">>,<<"98">>],Fextract(?M_SDP:lo_reduce(#sdp{medias=[SdpM11]},SdpO11)))
        ]},
      %
      {setup,
       fun() -> start_param_test_proc(),
                set_param('allow_ac_set',true),
                set_param('use_video_transcoding',true),
                set_param({'offer',video},[<<"H263/90000">>,<<"H264/90000">>,<<"vp8/90000">>]),
                set_param({'offer',audio},[<<"PCMA/8000">>,<<"PCMU/8000">>]) end,
       fun(_) -> stop_param_test_proc() end,
       [?_assertEqual([<<"116">>],FextractV(?M_SDP:lo_reduce(#sdp{medias=[SdpM10]},SdpO10)))]},
        ?_assertEqual([[<<"110">>,<<"speex/16000">>],[<<"98">>,<<"telephone-event/8000">>]],Frtpmap(?M_SDP:reduce_by_offer(SdpM11,SdpO11))),
        ?_assertEqual([<<"110">>,<<"98">>,<<"97">>],Fextract(?M_SDP:lo_reduce(#sdp{medias=[SdpM11]},SdpO11))) % #485
        ]}.

transcode_test_() ->
    Sdp1 = #sdp{medias=Sdp1Ms=[#sdp_m{media= <<"audio">>,fmt=[<<"97">>],attributes=[{<<"rtpmap">>,[<<"97">>,<<"speex/8000">>]}]},
                                 #sdp_m{media= <<"video">>, fmt=[<<"100">>],attributes=[{<<"rtpmap">>,[<<"100">>,<<"VP9/90000">>]}]}]},
    Sdp1n = Sdp1#sdp{medias=lists:map(fun(#sdp_m{attributes=As}=Sdp1M) -> Sdp1M#sdp_m{attributes=As++[{<<"no_transcode">>,[]}]} end, Sdp1Ms)},
    Sdp2 = #sdp{medias=[#sdp_m{media= <<"audio">>,fmt=[<<"97">>],attributes=[{<<"rtpmap">>,[<<"97">>,<<"speex/8000">>]}]},
                          #sdp_m{media= <<"video">>, fmt=[<<"105">>],attributes=[{<<"rtpmap">>,[<<"105">>,<<"test">>]}]}]},
    Sdp3 = #sdp{medias=[#sdp_m{media= <<"audio">>,fmt=[<<"97">>],attributes=[{<<"rtpmap">>,[<<"97">>,<<"speex/8000">>]}]},
                          #sdp_m{media= <<"video">>, fmt=[<<"105">>],attributes=[{<<"rtpmap">>,[<<"100">>,<<"test">>]}]}]},
    Sdp4 = #sdp{medias=[#sdp_m{media= <<"audio">>,fmt=[<<"97">>],attributes=[{<<"rtpmap">>,[<<"97">>,<<"speex/8000">>]}]}]},
    Sdp5 = #sdp{medias=[#sdp_m{media= <<"video">>, fmt=[<<"100">>],attributes=[{<<"rtpmap">>,[<<"100">>,<<"VP9/90000">>]}]}]},
    Sdp6 = #sdp{medias=[#sdp_m{media= <<"image">>, fmt=[<<"udptl">>],attributes=[]}]},
    Sdp7 = #sdp{medias=[#sdp_m{media= <<"audio">>,fmt=[<<"97">>],attributes=[{<<"rtpmap">>,[<<"97">>,<<"speex/8000">>]}]},
                          #sdp_m{media= <<"video">>, fmt=[<<"100">>],attributes=[{<<"rtpmap">>,[<<"100">>,<<"VP9/90000">>]}]},
                         #sdp_m{media= <<"image">>, fmt=[<<"udptl">>],attributes=[]}]},
    Sdp8 = #sdp{medias=[#sdp_m{media= <<"audio">>,fmt=[<<"97">>],attributes=[{<<"rtpmap">>,[<<"97">>,<<"speex/8000">>]}]},
                          #sdp_m{media= <<"video">>, fmt=[<<"100">>,<<"97">>],attributes=[{<<"rtpmap">>,[<<"100">>,<<"VP9/90000">>]},{<<"rtpmap">>,[<<"97">>,<<"rtx/90000">>]}]}]},
    Sdp9 = #sdp{medias=[#sdp_m{media= <<"video">>, fmt=[<<"96">>, <<"97">>, <<"98">>, <<"99">>, <<"100">>, <<"101">>, <<"102">>, <<"123">>, <<"127">>, <<"122">>, <<"125">>, <<"107">>, <<"108">>, <<"109">>, <<"124">>],
                                attributes=[{<<"rtpmap">>,[<<"96">>,<<"VP8/90000">>]},
                                            {<<"rtpmap">>,[<<"97">>,<<"rtx/90000">>]},
                                            {<<"rtpmap">>,[<<"98">>,<<"VP9/90000">>]},
                                            {<<"rtpmap">>,[<<"99">>,<<"rtx/90000">>]},
                                            {<<"rtpmap">>,[<<"100">>,<<"H264/90000">>]},
                                            {<<"rtpmap">>,[<<"101">>,<<"rtx/90000">>]},
                                            {<<"rtpmap">>,[<<"102">>,<<"H264/90000">>]},
                                            {<<"rtpmap">>,[<<"123">>,<<"rtx/90000">>]},
                                            {<<"rtpmap">>,[<<"127">>,<<"H264/90000">>]},
                                            {<<"rtpmap">>,[<<"122">>,<<"rtx/90000">>]},
                                            {<<"rtpmap">>,[<<"125">>,<<"H264/90000">>]},
                                            {<<"rtpmap">>,[<<"107">>,<<"rtx/90000">>]},
                                            {<<"rtpmap">>,[<<"108">>,<<"red/90000">>]},
                                            {<<"rtpmap">>,[<<"109">>,<<"rtx/90000">>]},
                                            {<<"rtpmap">>,[<<"124">>,<<"ulpfec/90000">>]}]}]},
    Fextract = fun(MN,#sdp{medias=Ms}) ->
                       case lists:keyfind(MN,#sdp_m.media,Ms) of
                           false -> undefined;
                           #sdp_m{fmt=Fmt} -> Fmt
                       end end,
    FullA = lists:map(fun({P,_,_,_}) -> P end, ?M_SDP:known_formats(static,<<"audio">>) ++ ?M_SDP:known_formats(dynamic,<<"audio">>)),
    FullV = lists:map(fun({P,_,_,_}) -> P end, ?M_SDP:known_formats(static,<<"video">>) ++ ?M_SDP:known_formats(dynamic,<<"video">>)),
    {"SDP reduce_by_offer test",
     [?_assertEqual(Sdp1n,?M_SDP:transcode_offer(false,Sdp1)),
      ?_assertEqual([<<"97">>|FullA--[<<"97">>]],Fextract(<<"audio">>,?M_SDP:transcode_offer(audio,Sdp1))),
      ?_assertEqual([<<"100">>],Fextract(<<"video">>,?M_SDP:transcode_offer(audio,Sdp1))),
      ?_assertEqual([<<"97">>|FullA--[<<"97">>]],Fextract(<<"audio">>,?M_SDP:transcode_offer(true,Sdp1))),
      ?_assertEqual([<<"100">>|FullV--[<<"100">>]],Fextract(<<"video">>,?M_SDP:transcode_offer(true,Sdp1))),
      %
      ?_assertEqual([<<"105">>],Fextract(<<"video">>,?M_SDP:transcode_offer(true,Sdp2))),
      ?_assertEqual([<<"97">>|FullA--[<<"97">>]],Fextract(<<"audio">>,?M_SDP:transcode_offer(true,Sdp3))),
      ?_assertEqual([<<"105">>],Fextract(<<"video">>,?M_SDP:transcode_offer(true,Sdp3))),
      ?_assertEqual([<<"97">>|FullA--[<<"97">>]],Fextract(<<"audio">>,?M_SDP:transcode_offer(true,Sdp4))),
      ?_assertEqual(undefined,Fextract(<<"video">>,?M_SDP:transcode_offer(true,Sdp4))),
      ?_assertEqual(undefined,Fextract(<<"audio">>,?M_SDP:transcode_offer(true,Sdp5))),
      ?_assertEqual([<<"100">>|FullV--[<<"100">>]],Fextract(<<"video">>,?M_SDP:transcode_offer(true,Sdp5))),
      ?_assertEqual(undefined,Fextract(<<"audio">>,?M_SDP:transcode_offer(true,Sdp6))),
      ?_assertEqual(undefined,Fextract(<<"video">>,?M_SDP:transcode_offer(true,Sdp6))),
      ?_assertEqual([<<"97">>|FullA--[<<"97">>]],Fextract(<<"audio">>,?M_SDP:transcode_offer(true,Sdp7))),
      ?_assertEqual([<<"100">>|FullV--[<<"100">>]],Fextract(<<"video">>,?M_SDP:transcode_offer(true,Sdp7))),
      ?_assertEqual([<<"udptl">>],Fextract(<<"image">>,?M_SDP:transcode_offer(true,Sdp7))),
      %
      ?_assertEqual([<<"100">>,<<"97">>],Fextract(<<"video">>,?M_SDP:transcode_offer(audio,Sdp8))),
      %?_assertEqual([<<"96">>,<<"98">>,<<"100">>,<<"102">>,<<"127">>,<<"125">>,<<"34">>,<<"128">>],Fextract(<<"video">>,?M_SDP:transcode_offer(true,Sdp9)))
      ?_assertEqual([<<"96">>,<<"98">>,<<"100">>,<<"102">>,<<"127">>,<<"125">>,<<"34">>,<<"103">>],Fextract(<<"video">>,?M_SDP:transcode_offer(true,Sdp9)))
     ]}.

get_mapping_test_() ->
    {AFmts1,BFmts1} = {[{<<"110">>,<<"A">>}],[{<<"110">>,<<"A">>}]},
    {AFmts2,BFmts2} = {[{<<"110">>,<<"A">>}],[{<<"112">>,<<"A">>}]},
    {AFmts3,BFmts3} = {[{<<"110">>,<<"A">>},{<<"111">>,<<"B">>},{<<"112">>,<<"C">>}],[{<<"110">>,<<"A">>},{<<"111">>,<<"B">>},{<<"112">>,<<"C">>}]},
    {AFmts4,BFmts4} = {[{<<"110">>,<<"A">>},{<<"111">>,<<"B">>},{<<"112">>,<<"C">>}],[{<<"113">>,<<"A">>},{<<"111">>,<<"B">>},{<<"112">>,<<"C">>}]},
    {AFmts5,BFmts5} = {[{<<"110">>,<<"A">>},{<<"111">>,<<"B">>},{<<"112">>,<<"C">>}],[{<<"116">>,<<"A">>},{<<"112">>,<<"B">>},{<<"124">>,<<"C">>}]},
    {AFmts6,BFmts6} = {[{<<"110">>,<<"A">>},{<<"111">>,<<"A">>},{<<"112">>,<<"C">>}],[{<<"110">>,<<"A">>},{<<"111">>,<<"A">>},{<<"112">>,<<"C">>}]},
    {AFmts7,BFmts7} = {[{<<"110">>,<<"A">>},{<<"111">>,<<"A">>},{<<"112">>,<<"A">>}],[{<<"110">>,<<"A">>},{<<"111">>,<<"A">>},{<<"112">>,<<"A">>}]},
    {AFmts8,BFmts8} = {[{<<"110">>,<<"A">>},{<<"111">>,<<"A">>},{<<"112">>,<<"A">>}],[{<<"115">>,<<"A">>},{<<"112">>,<<"A">>},{<<"113">>,<<"A">>}]},
    {"SDP get_mapping test",
     [?_assertEqual([{<<"110">>,<<"110">>}], lists:sort(?M_SDP:get_mapping(AFmts1,BFmts1))),
      ?_assertEqual([{<<"110">>,<<"112">>}], lists:sort(?M_SDP:get_mapping(AFmts2,BFmts2))),
      ?_assertEqual([{<<"110">>,<<"110">>},{<<"111">>,<<"111">>},{<<"112">>,<<"112">>}], lists:sort(?M_SDP:get_mapping(AFmts3,BFmts3))),
      ?_assertEqual([{<<"110">>,<<"113">>},{<<"111">>,<<"111">>},{<<"112">>,<<"112">>}], lists:sort(?M_SDP:get_mapping(AFmts4,BFmts4))),
      ?_assertEqual([{<<"110">>,<<"116">>},{<<"111">>,<<"112">>},{<<"112">>,<<"124">>}], lists:sort(?M_SDP:get_mapping(AFmts5,BFmts5))),
      ?_assertEqual([{<<"110">>,<<"110">>},{<<"111">>,<<"111">>},{<<"112">>,<<"112">>}], lists:sort(?M_SDP:get_mapping(AFmts6,BFmts6))),
      ?_assertEqual([{<<"110">>,<<"110">>},{<<"111">>,<<"111">>},{<<"112">>,<<"112">>}], lists:sort(?M_SDP:get_mapping(AFmts7,BFmts7))),
      ?_assertEqual([{<<"110">>,<<"113">>},{<<"111">>,<<"115">>},{<<"112">>,<<"112">>}], lists:sort(?M_SDP:get_mapping(AFmts8,BFmts8)))
      ]}.

multi_name_test_() ->
    SdpO = #sdp{medias=[#sdp_m{media= <<"video">>, fmt=[<<"113">>,<<"112">>],
                               attributes = [{<<"rtpmap">>,[<<"113">>,<<"H264/90000">>]},
                                             {<<"fmtp">>,[<<"113">>,<<"max-fs=6336;max-mbps=190080;profile-level-id=42801e">>]},
                                             {<<"rtpmap">>,[<<"112">>,<<"H264/90000">>]},
                                             {<<"fmtp">>,[<<"112">>,<<"packetization-mode=1;max-fs=6336;max-mbps=190080;profile-level-id=42801e">>]}]}]},
    SdpR = #sdp{medias=[#sdp_m{media= <<"video">>, fmt=[<<"112">>],
                               attributes = [{<<"rtpmap">>,[<<"112">>,<<"H264/90000">>]},
                                             {<<"fmtp">>,[<<"112">>,<<"profile-level-id=42801F">>]}]}]},
    Fextract = fun(#sdp{medias=[#sdp_m{fmt=Fmt}]}) -> Fmt end,
    {"SDP multi_name test",
     [?_assertEqual([<<"112">>,<<"113">>], Fextract(?M_SDP:lo(SdpR, SdpO)))]}.

%% ====================================================================
%% Internal functions
%% ====================================================================

% ===================================
% SDP construction
% ===================================

% make sdp record
construct_sdp(Opts) ->
    ?U:parse_sdp(sdp_binary(Opts)).

% make binary sdp
sdp_binary(Opts) ->
    [Mode,Audio,Video] = ?EU:extract_optional_default([{mode,<<"sendrecv">>},{audio,true},{video,true}],Opts),
    Fmode = fun() when Mode==undefined -> <<>>;
               () -> <<"a=",Mode/binary,"\r\n">>
            end,
    Sdp = <<"v=0","\r\n",
            "o=r-b 172453 172453 IN IP4 172.20.47.3","\r\n",
            "s=r-b","\r\n",
            "c=IN IP4 172.20.47.3",
            (case Audio of
                 true ->
                     <<"\r\n",
                       "m=audio 10004 RTP/AVP 9 8 0 18 101 13","\r\n",
                       "a=rtcp:10005 IN IP4 172.20.47.3","\r\n",
                       "a=rtpmap:9 G722/8000","\r\n",
                       "a=rtpmap:8 PCMA/8000","\r\n",
                       "a=rtpmap:0 PCMU/8000","\r\n",
                       "a=rtpmap:18 G729/8000","\r\n",
                       "a=fmtp:18 annexb=no","\r\n",
                       "a=rtpmap:101 telephone-event/8000","\r\n",
                       "a=fmtp:101 0-15","\r\n",
                       "a=rtcp-fb:9 test","\r\n",
                       "a=rtcp-fb:8 test","\r\n",
                       "a=rtpmap:13 CN/8000","\r\n",
                       (Fmode())/binary,
                        "a=ptime:20">>;
                 _ -> <<>>
             end)/binary,
            (case Video of
                 true ->
                    <<"\r\n",
                      "m=video 10006 RTP/AVP 97 98 99 34 117","\r\n",
                      "b=TIAS:1024000","\r\n",
                      "a=rtcp:10007 IN IP4 172.20.47.3","\r\n",
                      "a=rtpmap:97 H264/90000","\r\n",
                      "a=fmtp:97 profile-level-id=640028; packetization-mode=1","\r\n",
                      "a=rtpmap:98 H264/90000","\r\n",
                      "a=fmtp:98 profile-level-id=428028","\r\n",
                      "a=rtpmap:99 H264/90000","\r\n",
                      "a=fmtp:99 profile-level-id=428028; packetization-mode=1","\r\n",
                      "a=rtpmap:34 H263/90000","\r\n",
                      "a=fmtp:34 CIF4=1; CIF=1; QCIF=1","\r\n",
                      "a=rtpmap:117 YL-FPR/90000","\r\n",
                      "a=fmtp:117 yl-capset=7;yl-ver=1;yl-ext=19","\r\n",
                      (Fmode())/binary,
                      "a=ptime:20">>;
                 _ -> <<>>
             end)/binary>>,
    Sdp.

% ===================================
% Mock objects
% ===================================

% SIPSTORE
% allow audio codes set in answer (from header or config)
find_t({'allow_ac_set',_}=Key) ->
    Fdefault = fun() -> undefined end,
    case get_param('allow_ac_set',Fdefault) of
        true -> {Key,true};
        false -> {Key,false};
        undefined -> false
    end.

% UTILS
% offer_payloads from config
get_payloads('offer',<<"audio">>) ->
    Default = [<<"PCMA/8000">>,<<"PCMU/8000">>,<<"CN/8000">>,<<"GSM/8000">>,
               <<"G722/8000">>,<<"G723/8000">>,<<"G729/8000">>,
               <<"G726-16/8000">>,<<"G726-24/8000">>,<<"G726-32/8000">>,<<"G726-40/8000">>,
               <<"speex/8000">>,<<"speex/16000">>,<<"speex/32000">>,<<"iLBC/8000">>,
               <<"telephone-event/8000">>,<<"opus/48000/2">>],
    get_param({offer,audio},Default);
get_payloads('offer',<<"video">>) ->
    Default = [<<"H263/90000">>,
                 <<"H263-1998/90000">>,<<"H264/90000">>,<<"VP8/90000">>,<<"VP9/90000">>],
    get_param({offer,video},Default).

% UTILS
% use_video_transcoding from config
is_use_video_transcoding() ->
    get_param('use_video_transcoding',true).

% ===================================
% Param test process
% ===================================

% get param from test proc
get_param(Key,Default) ->
    case whereis(param_test_proc) of
        undefined when is_function(Default,0) -> Default();
        undefined -> Default;
        Pid ->
            Pid ! {get,Key,Default,self(),Ref=make_ref()},
            receive {ok,Result,Ref} -> Result
            after 1000 -> throw("params test process error")
            end end.

% set param to test proc
set_param(Key,Value) ->
    case whereis(param_test_proc) of
        undefined -> false;
        Pid ->
            Pid ! {set,Key,Value},
            true
    end.

% start param proc
start_param_test_proc() ->
    register(param_test_proc, spawn_link(fun() -> params_loop(#{}) end)).

% stop param proc
stop_param_test_proc() ->
    case whereis(param_test_proc) of
        undefined -> ok;
        Pid ->
            unregister(param_test_proc),
            Pid ! stop
    end.

% param process loop
params_loop(M) ->
    receive
        stop -> ok;
        {get,Key,FunDefault,Pid,Ref} when is_function(FunDefault,0) ->
            case maps:get(Key,M,XRef=make_ref()) of
                XRef -> Result = FunDefault();
                 Result -> ok
            end,
            Pid ! {ok,Result,Ref},
            params_loop(M);
        {get,Key,Default,Pid,Ref} ->
            Pid ! {ok,maps:get(Key,M,Default),Ref},
            params_loop(M);
        {set,Key,Value} ->
            params_loop(M#{Key => Value})
    end.