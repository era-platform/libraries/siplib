%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc
%%% @todo

-module(r_sip_config).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([]).

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("../include/r_sip.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% returns current site
get_current_site() ->
    ?ENVCFG:get_current_site().

%% returns current server ipaddr
get_current_server_addr() ->
    ?ENVCFG:get_current_server_addr().

%% return current server ipaddrs list
get_current_server_addrs() ->
    ?ENVCFG:get_current_server_addrs().

%% return general domain addr
get_general_domain() ->
    ?ENVCFG:get_general_domain().

%% return general setting value or default if not found
get_general_setting(Key,Default) ->
    ?ENVCFG:get_general_setting(Key, Default).

%% -------------------
%% returns nearest site for domain (where is registrar and domain center could be found)
get_domain_site(Domain) ->
    ?ENVCFGU:get_domain_site(Domain).

%% returns all worker sites for domain
get_domain_sites(Domain) ->
    ?ENVCFGU:get_domain_sites(Domain).

%% -------------------
%% returns mgc group idx-s on current site
get_site_mgc_groups() ->
    ?ENVCFG:get_site_mgc_groups().

%% -------------------
%% returns ip addrs of all sg nodes on site -> [{Node, [AddrPort::binary()]}]
get_site_sg_nodes() ->
    Res = ?ENVCFG:get_sipgate_server_nodes_addrs_on_site(),
    Dests = ?ENVCFG:get_nodes_containing_role_site('sg'),
    lists:filter(fun({Node,_}) -> lists:keymember(Node,2,Dests) end, Res).

%% -------------------
%% returns ip addrs of all b2bua on site -> [{Node, [AddrPort::binary()]}]
get_site_b2bua() ->
    Res = ?ENVCFG:get_sipb2bua_server_nodes_addrs_on_site(),
    Dests = ?ENVCFG:get_nodes_containing_role_site('b2bua'),
    lists:filter(fun({Node,_}) -> lists:keymember(Node,2,Dests) end, Res).

%% returns ip addrs of all b2bua on all sites -> [{Node::atom(), [AddrPort::binary()]}]
get_all_b2bua() ->
    ?ENVCFG:get_all_sipb2bua_server_nodes_addrs().

%% returns ip addrs of b2bua server by idx -> {Node, [AddrPort]} | false
get_b2bua_by_index(Key) when is_integer(Key) ->
    ?ENVCFG:get_sipb2bua_server_node_addr_by_index(Key).

%% -------------------
%% returns ip addrs of all sipconf on site -> [{Node, AddrPort::binary()}]
get_site_conf() ->
    Res = ?ENVCFG:get_sipconf_server_nodes_addrs_on_site(),
    Dests = ?ENVCFG:get_nodes_containing_role_site('conf'),
    lists:filter(fun({Node,_}) -> lists:keymember(Node,2,Dests) end, Res).

get_site_conf(Site) ->
    Res = ?ENVCFG:get_sipconf_server_nodes_addrs_on_site(Site),
    Dests = ?ENVCFG:get_nodes_containing_role_site('conf',Site),
    lists:filter(fun({Node,_}) -> lists:keymember(Node,2,Dests) end, Res).

%% returns ip addrs of all sipconf on all sites -> [{Node::atom(), AddrPort::binary()}]
get_all_conf() ->
    ?ENVCFG:get_all_sipconf_server_nodes_addrs().

%% -------------------
%% returns ip addrs of all sipivr on site -> [{Node, AddrPort::binary()}]
get_site_ivr() ->
    Res = ?ENVCFG:get_sipivr_server_nodes_addrs_on_site(),
    Dests = ?ENVCFG:get_nodes_containing_role_site('ivr'),
    lists:filter(fun({Node,_}) -> lists:keymember(Node,2,Dests) end, Res).

get_site_ivr(Site) ->
    Res = ?ENVCFG:get_sipivr_server_nodes_addrs_on_site(Site),
    Dests = ?ENVCFG:get_nodes_containing_role_site('ivr',Site),
    Debug = lists:filter(fun({Node,_}) -> lists:keymember(Node,2,Dests) end, Res),
    case Debug of
        [] ->
            RegisteredNames = era_rpci:cli_registered_names(),
            IvrNodes = era_rpci:cli_get_nodes('ivr'),
            CurrentSite = ?ENVCFG:get_current_site(),
            ?OUT("get_site_ivr(~120tp) -> []. ~n\tCurrentSite: ~120tp, ~n\tRes: ~120tp,~n\tDests: ~120tp~n\tRegNames: ~120tp~n\tIvrNodes: ~120tp",
                 [Site,CurrentSite,Res,Dests,RegisteredNames,IvrNodes]);
        _ -> ok
    end,
    Debug.

%% returns ip addrs of all sipivr on all sites -> [{Node::atom(), AddrPort::binary()}]
get_all_ivr() ->
    ?ENVCFG:get_all_sipivr_server_nodes_addrs().

%% -------------------
%% returns ip addrs of all sipprompt on site -> [{Node, AddrPort::binary()}]
get_site_prompt() ->
    Res = ?ENVCFG:get_sipprompt_server_nodes_addrs_on_site(),
    Dests = ?ENVCFG:get_nodes_containing_role_site('prompt'),
    lists:filter(fun({Node,_}) -> lists:keymember(Node,2,Dests) end, Res).

get_site_prompt(Site) ->
    Res = ?ENVCFG:get_sipprompt_server_nodes_addrs_on_site(Site),
    Dests = ?ENVCFG:get_nodes_containing_role_site('prompt',Site),
    lists:filter(fun({Node,_}) -> lists:keymember(Node,2,Dests) end, Res).

%% returns ip addrs of all sipprompt on all sites -> [{Node::atom(), AddrPort::binary()}]
get_all_prompt() ->
    ?ENVCFG:get_all_sipprompt_server_nodes_addrs().

%% -------------------
% returns ip addrs of all sipesg on all sites -> [{Node::atom(), AddrPort::binary()}]
get_all_esg() ->
    ?ENVCFG:get_all_sipesg_server_nodes_addrs().

%% -------------------
%% returns ip addrs of sip server by idx -> {Node, AddrPort} | false
get_sipsrv_by_index(Key) when is_integer(Key) ->
    ?ENVCFG:get_sip_server_node_addr_by_index(Key).

% returns ip addr of current server (used to generate contact for sipmsg)
get_my_sipserver_addr() ->
    ?EU:node_addr().

%% returns ip addrs of all sip servers in configuration -> [Addr::binary()]
get_all_sipserver_addrs() ->
    ?ENVCFG:get_all_sipserver_addrs().

%% -------------------
%% returns global system index of current sipserver -> integer()
get_my_sipserver_index() ->
    ?ENVCFG:get_current_node_index().

%% returns global system index of selected node -> integer()
get_node_index(Node) ->
    ?ENVCFG:get_node_index(Node).

%%
get_node_by_index(Index) ->
    ?ENVCFG:get_node_index(Index).

%% returns node of sip server (by Key from callid, tags, x-era-identity) -> {Site::binary(), Node::atom()}
get_sipserver_by_index(Key) when is_integer(Key) ->
    ?ENVCFG:get_node_by_index(Key).

%% returns ip addrs of sipserver by idx -> [Addr::binary()]
get_sipserver_addrs_by_index(Key) ->
    ?ENVCFG:get_server_addrs_by_index(Key).

%% -------------------
%% returns role opts of sipserver by idx -> Opts::list()
%% -------------------
get_sipserver_opts_by_index(Key, Role) ->
    ?ENVCFG:get_server_role_opts_by_index(Key, Role).

%% -------------------
%% checks if domain_master is accessible
%% -------------------
check_domain_master() ->
    ?ENVCFGU:check_domain_master().

%% -------------------
%% return transport opts for contacts and routes inside of system
%% -------------------
get_transport_opts() ->
    % []. % UDP
    [{<<"transport">>, <<"tcp">>}]. % TCP

%% -------------------
-spec get_static_sg_nodes_addrs() -> Result::[{Site::binary(), Node::node(), Address::binary(), Port::non_neg_integer()}].
%% -------------------
get_static_sg_nodes_addrs() ->
    ?ENVCFG:get_static_all_sg_nodes_addrs_udpport().

%% -------------------
get_domains_in_site() ->
    All = case ?ENVCFGU:get_domain_tree([{mode,isowner}]) of
              undefined -> [];
              R when is_list(R) -> R
          end,
    lists:filtermap(fun({D,true}) -> {true, D}; (_)-> false end, All).

%% -------------------
%% RP-1017
%% -------------------
get_server_roleopts(Role) ->
    ?ENVCFG:get_server_roleopts_by_role(Role).

get_server_roleopts_by_role(Site,Server,Role) ->
    ?ENVCFG:get_server_roleopts_by_role(Site,Server,Role).

%% -------------------
get_servers_by_param(Key, Value) ->
    ?ENVCFG:get_servers_by_param(Key, Value).

get_server_param(Node, Keys, Default) ->
    ?ENVCFG:get_server_param(Node, Keys, Default).

get_server_role_opts_by_index(Key, Role) ->
    ?ENVCFG:get_server_role_opts_by_index(Key, Role).

get_parent_node() ->
    ?ENVCFG:get_parent_node().

get_mg_node_by_addr_postfix(Addr,Postfix) ->
    ?ENVCFG:get_mg_node_by_addr_postfix(Addr,Postfix).

get_record_path(Addr,Postfix) ->
    ?ENVCFG:get_record_path(Addr,Postfix).

get_nodes_containing_role_dyncfg_check_free(Role) ->
    ?ENVCFG:get_nodes_containing_role_dyncfg_check_free(Role).

%% ====================================================================
%% Internal functions
%% ====================================================================


