%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc
%%% @todo

-module(r_sip_supv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(supervisor).

-export([init/1]).
-export([start_link/0]).
-export([start_child/1, terminate_child/1, delete_child/1, restart_child/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_mgc.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).


start_child(ChildSpec) ->
    supervisor:start_child(?MODULE, ChildSpec).

restart_child(Id) ->
    supervisor:restart_child(?MODULE, Id).

terminate_child(Id) ->
    supervisor:terminate_child(?MODULE, Id).

delete_child(Id) ->
    supervisor:delete_child(?MODULE, Id).

%% ====================================================================
%% OTP callbacks
%% ====================================================================

init(_State) ->
    PingerOpts = [{ets, ets:new(ctxping,[public,set])}],
    DlgStoreOpts = [{ets_c, ets:new(ets_c,[public,set])},
                    {ets_d, ets:new(ets_d,[public,set])},
                    {supv, ?SUPV}],
    ChildSpec = [{?STAT_SUPV, {?STAT_SUPV, start_link, []}, permanent, 1000, supervisor, [?STAT_SUPV]},
                 {?MGC_PINGER, {?MGC_PINGER, start_link, [PingerOpts]}, permanent, 1000, worker, [?MGC_PINGER]},
                 %{?WATCHER, {?WATCHER, start_link, [[]]}, permanent, 1000, worker, [?WATCHER]},
                 {?DLG_STORE, {?DLG_STORE, start_link, [DlgStoreOpts]}, permanent, 1000, worker, [?DLG_STORE]},
                 {?SIPSTORE, {?SIPSTORE, start_link, [[{ets_u, ets:new(ets_u,[public,set])},
                                                       {ets_t, ets:new(ets_t,[public,set])}]]}, permanent, 1000, worker, [?SIPSTORE]},
                 {?SIP_SRV, {?SIP_SRV, start_link, [#{}]}, permanent, 1000, worker, [?SIP_SRV]},
                 {?SipWorkerSupv, {?SipWorkerSupv, start_link, [[]]}, permanent, 1000, supervisor, [?SipWorkerSupv]}
                ],
    {ok, {{one_for_one, 10, 2}, ChildSpec}}.


