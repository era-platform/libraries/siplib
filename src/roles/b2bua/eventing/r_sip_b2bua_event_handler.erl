%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>, Peter Bukashin <tbotc@yandex.ru>
%%% @doc

-module(r_sip_b2bua_event_handler).

-export([prepare_event/3,
         prepare_event/4]).

%% ==========================================================================
%% Defines
%% ==========================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_mgc.hrl").

-define(CallPattern, <<"callevents">>).
-define(CommonType,common).


%% ====================================================================
%% Public functions
%% ====================================================================

%%-----------------------------------------------
%% Prepare Event common part
%%-----------------------------------------------
prepare_event(EventType,State,Opts) ->
    Event = make_event(EventType,{State,Opts}),
    prepare_event_header(EventType,Event).

prepare_event(EventType,XState,State,Opts) ->
    Event = make_event(EventType,{XState,State,Opts}),
    prepare_event_header(EventType,Event).

%% @private
prepare_event_header(EventType,Event) ->
    Pattern = {?CallPattern,?CommonType,undefined},
    EventHeader = #{<<"class">> => ?CallPattern,
                    <<"type">> => ?EU:to_binary(EventType),
                    <<"data">> => Event,
                    <<"eventts">> => ?EU:timestamp()},
    From = self(),
    {Pattern,EventHeader,From}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ----------------------------------
make_event(EventType,Params) ->
    Meta = ?EVENTMETA:get_meta(EventType),
    F = fun({Key,generate,new_uuid},Acc) ->
                Acc#{Key => ?EU:uuid_srvidx()};
           ({Key,StateType,ValueIdx},Acc) ->
                case extract_value(Params, StateType,ValueIdx) of
                    {ok, Value} -> Acc#{Key => Value};
                    _ -> Acc
                end;
           ({Key,StateType,ValueIdx,Mod},Acc) ->
                case extract_value(Params, StateType,ValueIdx) of
                    {ok, Value} -> Acc#{Key => transform_value(Value, Mod)};
                    _ -> Acc
                end
        end,
    lists:foldl(F,#{},Meta).

%% state type 1.1 [{{acallid,acallidhash}, {iid,its}}=State, #{}=Opts]
extract_value({{{ACallId,_ACallIdH},{_IID,_ITS}},_Opts},state,acallid) -> {ok,ACallId};
extract_value({{{_ACallId,ACallIdH},{_IID,_ITS}},_Opts},state,acallidhash) -> {ok,ACallIdH};
extract_value({{{_ACallId,_ACallIdH},{IID,_ITS}},_Opts},state,iid) -> {ok,IID};
extract_value({{{_ACallId,_ACallIdH},{_IID,ITS}},_Opts},state,its) -> its(ITS);
extract_value({{{_ACallId,_ACallIdH},{_IID,_ITS}},Opts},opts,Key) -> maps:find(Key, Opts);

%% state type 1.2 [{{dialogid,dialogidhash},{acallid,acallidhash}, {iid,its}}=State,#{}=Opts]
extract_value({{{DialogId,_DialogIdH},{_ACallId,_ACallIdH},{_IID,_ITS}},_Opts},state,dialogid) -> {ok,DialogId};
extract_value({{{_DialogId,DialogIdH},{_ACallId,_ACallIdH},{_IID,_ITS}},_Opts},state,dialogidhash) -> {ok,DialogIdH};
extract_value({{{_DialogId,_DialogIdH},{ACallId,_ACallIdH},{_IID,_ITS}},_Opts},state,acallid) -> {ok,ACallId};
extract_value({{{_DialogId,_DialogIdH},{_ACallId,ACallIdH},{_IID,_ITS}},_Opts},state,acallidhash) -> {ok,ACallIdH};
extract_value({{{_DialogId,_DialogIdH},{_ACallId,_ACallIdH},{IID,_ITS}},_Opts},state,iid) -> {ok,IID};
extract_value({{{_DialogId,_DialogIdH},{_ACallId,_ACallIdH},{_IID,ITS}},_Opts},state,its) -> its(ITS);
extract_value({{{_DialogId,_DialogIdH},{_ACallId,_ACallIdH},{_IID,_ITS}},Opts},opts,Key) -> maps:find(Key, Opts);

%% state type 1.3.1 [#state_dlg{}=State,#{}=Opts]
extract_value({#state_dlg{dialogid=DialogId},_Opts},state,dialogid) -> {ok,DialogId};
extract_value({#state_dlg{dialogidhash=DialogIdH},_Opts},state,dialogidhash) -> {ok,DialogIdH};
extract_value({#state_dlg{acallid=ACallId},_Opts},state,acallid) -> {ok,ACallId};
extract_value({#state_dlg{acallidhash=ACallIdH},_Opts},state,acallidhash) -> {ok,ACallIdH};
extract_value({#state_dlg{invite_id=IID},_Opts},state,iid) -> {ok,IID};
extract_value({#state_dlg{invite_ts=ITS},_Opts},state,its) -> its(ITS);
extract_value({#state_dlg{},Opts},opts,Key) -> maps:find(Key,Opts);
extract_value({#state_dlg{a=ASide},_Opts},aside,Key) -> get_side(Key,ASide);
extract_value({#state_dlg{b=BSide},_Opts},bside,Key) -> get_side(Key,BSide);
extract_value({#state_dlg{media=Media},_Opts},media,Key) -> get_media(Key,Media);
extract_value({#state_dlg{stopreason=Reason},_Opts},stopreason,Key) -> get_stopreason(Key, Reason);
extract_value({#state_dlg{map=Map},_Opts},map,Key) -> get_map(Key, Map);

%% state type 1.3.2 [#state_forking{}=State,#{}=Opts]
extract_value({#state_forking{dialogid=DialogId},_Opts},state,dialogid) -> {ok,DialogId};
extract_value({#state_forking{dialogidhash=DialogIdH},_Opts},state,dialogidhash) -> {ok,DialogIdH};
extract_value({#state_forking{acallid=ACallId},_Opts},state,acallid) -> {ok,ACallId};
extract_value({#state_forking{acallidhash=ACallIdH},_Opts},state,acallidhash) -> {ok,ACallIdH};
extract_value({#state_forking{invite_id=IID},_Opts},state,iid) -> {ok,IID};
extract_value({#state_forking{invite_ts=ITS},_Opts},state,its) -> its(ITS);
extract_value({#state_forking{},Opts},opts,Key) -> maps:find(Key,Opts);
extract_value({#state_forking{a=ASide},_Opts},aside,Key) -> get_side(Key,ASide);
extract_value({#state_forking{b=BSide},_Opts},bside,Key) -> get_side(Key,BSide);
extract_value({#state_forking{media=Media},_Opts},media,Key) -> get_media(Key,Media);
extract_value({#state_forking{stopreason=Reason},_Opts},stopreason,Key) -> get_stopreason(Key, Reason);
extract_value({#state_forking{map=Map},_Opts},map,Key) -> get_map(Key, Map);

%% state type 2.1 [{{acallid,acallidhash}, {iid,its}}=Owner, #state_xxx=State, #{}=Opts]
extract_value({{{ACallId,_ACallIdH},{_IID,_ITS}}=_Owner,_State,_Opts},owner,acallid) -> {ok,ACallId};
extract_value({{{_ACallId,ACallIdH},{_IID,_ITS}}=_Owner,_State,_Opts},owner,acallidhash) -> {ok,ACallIdH};
extract_value({{{_ACallId,_ACallIdH},{IID,_ITS}}=_Owner,_State,_Opts},owner,iid) -> {ok,IID};
extract_value({{{_ACallId,_ACallIdH},{_IID,ITS}}=_Owner,_State,_Opts},owner,its) -> its(ITS);
extract_value({{{_ACallId,_ACallIdH},{_IID,_ITS}}=_Owner,State,Opts},state,Key) -> extract_value({State,Opts},state,Key);
extract_value({{{_ACallId,_ACallIdH},{_IID,_ITS}}=_Owner,_State,Opts},opts,Key) -> maps:find(Key, Opts);

%% state type 2.2 [{{acallid,acallidhash}, {iid,its}}=Owner, #state_xxx=State, #{}=Opts]
extract_value({{{DialogId,_DialogIdH},{_CallId,_CallIdH},{_IID,_ITS}}=_Owner,_State,_Opts},owner,dialogid) -> {ok,DialogId};
extract_value({{{_DialogId,DialogIdH},{_CallId,_CallIdH},{_IID,_ITS}}=_Owner,_State,_Opts},owner,dialogidhash) -> {ok,DialogIdH};
extract_value({{{_DialogId,_DialogIdH},{CallId,_CallIdH},{_IID,_ITS}}=_Owner,_State,_Opts},owner,acallid) -> {ok,CallId};
extract_value({{{_DialogId,_DialogIdH},{_CallId,CallIdH},{_IID,_ITS}}=_Owner,_State,_Opts},owner,acallidhash) -> {ok,CallIdH};
extract_value({{{_DialogId,_DialogIdH},{_CallId,_CallIdH},{IID,_ITS}}=_Owner,_State,_Opts},owner,iid) -> {ok,IID};
extract_value({{{_DialogId,_DialogIdH},{_CallId,_CallIdH},{_IID,ITS}}=_Owner,_State,_Opts},owner,its) -> its(ITS);
extract_value({{{_DialogId,_DialogIdH},{_CallId,_CallIdH},{_IID,_ITS}}=_Owner,State,Opts},state,Key) -> extract_value({State,Opts},state,Key);
extract_value({{{_DialogId,_DialogIdH},{_CallId,_CallIdH},{_IID,_ITS}}=_Owner,_State,Opts},opts,Key) -> maps:find(Key, Opts);
%% state type 2.3.1 [#side_fork{}=Fork,#state_xxx{}=State,#{}=Opts]
%% state type 2.3.2 [#side{}=Side,#state_xxx{}=State,#{}=Opts]
extract_value({#side_fork{}=Fork,_State,_Opts},fork,Key) -> get_side_fork(Key, Fork);
extract_value({#side{}=Side,_State,_Opts},xside,Key) -> get_side(Key, Side);
extract_value({_XState,#state_dlg{}=State,Opts},StateType,Key) -> extract_value({State,Opts}, StateType, Key);
extract_value({_XState,#state_forking{}=State,Opts},StateType,Key) -> extract_value({State,Opts}, StateType, Key);
extract_value(_, _, _) -> false.

%% transform to string
transform_value({Protocol, Number, Domain}, uri) -> ?EU:to_binary("<"++?EU:to_list(Protocol)++":"++?EU:to_list(Number)++"@"++?EU:to_list(Domain)++">");
transform_value(#uri{scheme=Protocol,user=User,domain=Domain}, uri) -> transform_value({Protocol,User,Domain}, uri);
transform_value(_Other, uri) -> "NULL";
transform_value(Value, aor) -> transform_value(Value, uri);
transform_value(Value, any) -> ?EU:to_binary(?EU:str("~tp",[Value]));
transform_value(Value, datetime) -> ?EU:to_binary(?B2BUA_UTILS:erlang_ts_concat(Value));
transform_value(Value, relates) ->
    try jsx:encode([{Domain,[{Type,ObjKey}]} || {Domain,{Type,ObjKey}} <- Value]) of
        Result -> Result
    catch
        Err:R -> {error, <<"Invalid JSON text ",Err/binary,":",R/binary>>}
    end.

get_side(callid, #side{callid=CallId}) -> {ok,CallId};
get_side(localuri, #side{localuri=LURI}) -> extract_uri(LURI);
get_side(localtag, #side{localtag=LTAG}) -> {ok,LTAG};
get_side(remoteuri, #side{remoteuri=RURI}) -> extract_uri(RURI);
get_side(last_remote_party, #side{last_remote_party=LRP}) -> extract_uri(LRP);
get_side(remotecontact, #side{remotecontacts=[RContact|_]}) -> extract_uri(RContact);
get_side(is_rec, #side{is_rec=IsRec}) -> {ok,IsRec};
get_side(_, _) -> false.

%% @private
extract_uri(#uri{}=URI) -> {ok,?U:unparse_uri(?U:clear_uri(URI))};
extract_uri(_) -> false.

get_side_fork(requesturi,#side_fork{requesturi=RequestURI}) -> {ok,?U:unparse_uri(RequestURI)};
get_side_fork(remoteuri,#side_fork{remoteuri=RURI}) -> extract_uri(RURI);
get_side_fork(localtag,#side_fork{localtag=LocalTag}) -> {ok,LocalTag};
get_side_fork(callid,#side_fork{callid=CallId}) -> {ok,CallId};
get_side_fork(callednum,#side_fork{callednum=CalledNum}) -> {ok,CalledNum};
get_side_fork(dir,#side_fork{dir=Dir}) -> {ok,Dir};
get_side_fork(extaccountcode,#side_fork{opts=Opts}) when is_list(Opts) ->
    case lists:keyfind(extaccount,1,Opts) of
        false -> false;
        {_,AMap} -> maps:find(code,AMap)
    end;
get_side_fork(sipcode, #side_fork{res={timeout}}) -> {ok,0};
get_side_fork(sipcode, #side_fork{res={bye}}) -> {ok,0};
get_side_fork(sipcode, #side_fork{res={resp,RSipCode,_}}) -> {ok,RSipCode};
get_side_fork(sipreason, #side_fork{res={timeout}}) -> {ok,<<"timeout">>};
get_side_fork(sipreason, #side_fork{res={bye}}) -> {ok,<<"bye">>};
get_side_fork(sipreason, #side_fork{res={resp,_,RSipReason}}) -> {ok,RSipReason};
get_side_fork(_,#side_fork{}) -> false.

get_media(mgc, #media{mgc=Mgc}) -> {ok,Mgc};
get_media(rec_links, #media{rec_links=RecLinks}) -> {ok, ?EU:to_unicode_binary(?B2BUA_UTILS:rec_links_handler(RecLinks,[]))};
get_media(is_rec, #media{is_rec=IsRec}) -> {ok,IsRec};
get_media(_, _) -> false.

get_stopreason(Key,#{}=Reason) -> maps:find(Key,Reason).
get_map(Key,#{}=Map) -> maps:find(Key,Map).

%%-----------------------------------
%% ITS
%%-----------------------------------
its({A,B,C}) ->
    {ok,?EU:to_int(?EU:to_list(A)++?EU:to_list(B)++?EU:to_list(C))};
its([_ITSunit|_Tail]=ITS) ->
    {ok,?EU:to_int(lists:foldl(fun(A,Acc) -> Acc ++ ?EU:to_list(A) end,[],ITS))};
its(ITS) ->
    {ok, ?EU:to_int(?EU:to_list(ITS))}.
