%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author George Makarov <georgemkrv@gmail.com>, Peter Bukashin <tbotc@yandex.ru>
%%% @doc @todo Add description to r_sip_b2bua_event_meta.


-module(r_sip_b2bua_event_meta).

-export([get_meta/1]).

%% ==========================================================================
%% Defines
%% ==========================================================================

%% ====================================================================
%% API functions
%% ====================================================================

%% acallid,acallidhash,urifrom,urito,anumber,fromusername,fromdomain,
%% fromouter,fromprovidercode,callednum,networkaddr,urireferedby,
%% isrefered,isreplacing,invitets,eventts,invitedt,cid,ghost
get_meta(invite) ->
    [{<<"acallid">>, state, acallid},
     {<<"acallidhash">>, state, acallidhash},
     {<<"urifrom">>, opts, from, uri},
     {<<"urito">>, opts, to, uri},
     {<<"anumber">>, opts, fromnumber},
     {<<"fromusername">>, opts, fromusername},
     {<<"fromdomain">>, opts, fromdomain},
     {<<"fromouter">>, opts, fromouter},
     {<<"fromprovidercode">>, opts, fromprovidercode},
     {<<"callednum">>, opts, callednum},
     {<<"networkaddr">>, opts, networkaddr},
     {<<"urireferedby">>, opts, referred_by, uri},
     {<<"isrefered">>, opts, is_referred},
     {<<"isreplacing">>, opts, is_replacing},
     {<<"esgdlg">>, opts, esgdlg},
     {<<"invitets">>, state, its},
     {<<"invitedt">>, state, its, datetime},
     {<<"cid">>, state, iid},
     {<<"ghost">>, opts, ghost_mode}];
%% acallid,acallidhash,routefinaction,sipcode,reason,srvidx,invitets,eventts,invitedt,cid
get_meta(route_fin) ->
    [{<<"acallid">>, state, acallid},
     {<<"acallidhash">>, state, acallidhash},
     {<<"routefinaction">>, opts, action},
     {<<"sipcode">>, opts, sipcode},
     {<<"reason">>, opts, reason, any},
     {<<"srvidx">>, opts, srvidx},
     {<<"invitets">>, state, its},
     {<<"invitedt">>, state, its, datetime},
     {<<"cid">>, state, iid}];
%% acallid,acallidhash,urito,replacedcallid,replaceddlg,replaceddlghash,
%% replacedacallid,replacedacallidhash,replacedinvitets,replacedcid,
%% invitets,eventts,eventid,invitedt,cid
get_meta(route_replacing) ->
    [{<<"acallid">>, state, acallid},
     {<<"acallidhash">>, state, acallidhash},
     {<<"urito">>, opts, ruri, uri},
     {<<"replacedcallid">>, opts, replaced_callid},
     {<<"replaceddlg">>, opts, replaced_dialogid},
     {<<"replaceddlghash">>, opts, replaced_dialogidhash},
     {<<"replacedacallid">>, opts, replaced_acallid},
     {<<"replacedacallidhash">>, opts, replaced_acallidhash},
     {<<"replacedinvitets">>, opts, replaced_its},
     {<<"replacedcid">>, opts, replaced_iid},
     {<<"invitets">>, state, its},
     {<<"invitedt">>, state, its, datetime},
     {<<"eventid">>, generate, new_uuid},
     {<<"cid">>, state, iid}];
%% acallid,acallidhash,routedomain,ruleid,routeaction,vector,routeto,routefrom,
%% account,invitets,eventts,eventid,invitedt,cid
get_meta(route) ->
    [{<<"acallid">>, state, acallid},
     {<<"acallidhash">>, state, acallidhash},
     {<<"routedomain">>, opts, domain},
     {<<"ruleid">>, opts, ruleid},
     {<<"routeaction">>, opts, action},
     {<<"vector">>, opts, vector},
     {<<"routeto">>, opts, to, aor},
     {<<"routefrom">>, opts, from, aor},
     {<<"account">>, opts, account},
     {<<"invitets">>, state, its},
     {<<"invitedt">>, state, its, datetime},
     {<<"eventid">>, generate, new_uuid},
     {<<"cid">>, state, iid}];
%% acallid,acallidhash,redirectto,redirectowner,reason,invitets,
%% eventts,eventid,invitedt,cid
get_meta(route_redirect) ->
    [{<<"acallid">>, state, acallid},
     {<<"acallidhash">>, state, acallidhash},
     {<<"redirectto">>, opts, redirect_to, aor},
     {<<"redirectowner">>, opts, owner, aor},
     {<<"reason">>, opts, reason, any},
     {<<"invitets">>, state, its},
     {<<"invitedt">>, state, its, datetime},
     {<<"eventid">>, generate, new_uuid},
     {<<"cid">>, state, iid}];
%% acallid,acallidhash,featurecodetype,featurecodeid,featurecodeext,
%% featurenumber,invitets,eventts,eventid,invitedt,cid
get_meta(route_feature) ->
    [{<<"acallid">>, state, acallid},
     {<<"acallidhash">>, state, acallidhash},
     {<<"featurecodetype">>, opts, featurecode_type},
     {<<"featurecodeid">>, opts, featurecode_id},
     {<<"featurecodeextension">>, opts, featurecode_extension},
     {<<"featurenumber">>, opts, number},
     {<<"invitets">>, state, its},
     {<<"invitedt">>, state, its, datetime},
     {<<"eventid">>, generate, new_uuid},
     {<<"cid">>, state, iid}];
%% acallid,acallidhash,isreferred,byuri,linkreferred,linkreplaces,linkcid,linkits,linkside,invitets,eventts,eventid,invitedt,cid
get_meta(route_referred) ->
    [{<<"acallid">>, state, acallid},
     {<<"acallidhash">>, state, acallidhash},
     {<<"isreferred">>, opts, is_referred},
     {<<"byuri">>, opts, by_uri, uri},
     {<<"linkreferred">>, opts, link_referred},
     {<<"linkreplaces">>, opts, link_replaces},
     {<<"linkcid">>, opts, link_iid},
     {<<"linkits">>, opts, link_its},
     {<<"linkdlgid">>, opts, link_dlgid},
     {<<"linkside">>, opts, link_side},
     {<<"invitets">>, state, its},
     {<<"invitedt">>, state, its, datetime},
     {<<"eventid">>, generate, new_uuid},
     {<<"cid">>, state, iid}];
%% acallid,acallidhash,dialogid,dialogidhash,invitets,eventts,invitedt,cid,ghost
get_meta(dlg_init) ->
    [{<<"acallid">>, state, acallid},
     {<<"acallidhash">>, state, acallidhash},
     {<<"dialogid">>, state, dialogid},
     {<<"dialogidhash">>, state, dialogidhash},
     {<<"ghost">>, opts, ghost_mode},
     {<<"invitets">>, state, its},
     {<<"invitedt">>, state, its, datetime},
     {<<"cid">>, state, iid}];
%% acallid,acallidhash,dialogid,dialogidhash,localuri,localtag,remoteuri,
%% invitets,eventts,invitedt,cid,ghost
get_meta(dlg_call) ->
    [{<<"acallid">>, state, acallid},
     {<<"acallidhash">>, state, acallidhash},
     {<<"dialogid">>, state, dialogid},
     {<<"dialogidhash">>, state, dialogidhash},
     {<<"localuri">>, aside, localuri},
     {<<"localtag">>, aside, localtag},
     {<<"remoteuri">>, aside, remoteuri},
     {<<"anumber">>,opts,anumber},
     {<<"ausername">>,opts,ausername},
     {<<"adomain">>,opts,adomain},
     {<<"anetworkaddr">>,opts,anetworkaddr},
     {<<"acallednum">>,opts,acallednum},
     {<<"aouter">>,opts,aouter},
     {<<"aprovidercode">>,opts,aprovidercode},
     {<<"ghost">>,map,ghost_mode},
     {<<"invitets">>, state, its},
     {<<"invitedt">>, state, its, datetime},
     {<<"cid">>, state, iid}];
%% acallid,acallidhash,dialogid,dialogidhash,localuri,localtag,remoteuri,
%% forkrequesturi,forkremoteuri,forklocaltag,forkcallid,forkcallednum,
%% forkdir,forkextaccountcode,
%% "++ExtFields++" busername,bdomain,bnetworkaddr,bouter,bprovidercode,eventid
%% invitets,eventts,invitedt,cid
get_meta(fork_start) ->
    [{<<"acallid">>, state, acallid},
     {<<"acallidhash">>, state, acallidhash},
     {<<"dialogid">>, state, dialogid},
     {<<"dialogidhash">>, state, dialogidhash},
     {<<"localuri">>, aside, localuri},
     {<<"localtag">>, aside, localtag},
     {<<"remoteuri">>, aside,remoteuri},
     {<<"forkrequesturi">>,fork,requesturi},
     {<<"forkremoteuri">>,fork,remoteuri},
     {<<"forklocaltag">>,fork,localtag},
     {<<"forkcallid">>,fork,callid},
     {<<"forkcallednum">>,fork,callednum, aor},
     {<<"forkdir">>,fork,dir},
     {<<"forkextaccountcode">>,fork,extaccountcode},
     {<<"busername">>,opts,busername},
     {<<"bdomain">>,opts,bdomain},
     {<<"bnetworkaddr">>,opts,bnetworkaddr},
     {<<"bouter">>,opts,bouter},
     {<<"bprovidercode">>,opts,bprovidercode},
     {<<"invitets">>,state,its},
     {<<"invitedt">>, state, its, datetime},
     {<<"eventid">>, generate, new_uuid},
     {<<"cid">>,state,iid}
    ];
%% acallid,acallidhash,dialogid,dialogidhash,localuri,localtag,remoteuri,
%% forkrequesturi,forkremoteuri,forklocaltag,forkcallid,forkcallednum,
%% forkdir,forkextaccountcode,
%% "++ExtFields++" sipcode,reason,?eventid?,
%% invitets,eventts,invitedt,cid
get_meta(fork_stop)->
    [{<<"acallid">>, state, acallid},
     {<<"acallidhash">>, state, acallidhash},
     {<<"dialogid">>, state, dialogid},
     {<<"dialogidhash">>, state, dialogidhash},
     {<<"localuri">>, aside, localuri},
     {<<"localtag">>, aside, localtag},
     {<<"remoteuri">>, aside, remoteuri},
     {<<"forkrequesturi">>,fork,requesturi},
     {<<"forkremoteuri">>,fork,remoteuri},
     {<<"forklocaltag">>,fork,localtag},
     {<<"forkcallid">>,fork,callid},
     {<<"forkcallednum">>,fork,callednum, aor},
     {<<"forkdir">>,fork,dir},
     {<<"forkextaccountcode">>,fork,extaccountcode},
     {<<"sipcode">>,fork,sipcode},
     {<<"reason">>,fork,sipreason},
     {<<"eventid">>, generate, new_uuid},
     {<<"invitets">>,state,its},
     {<<"invitedt">>, state, its, datetime},
     {<<"cid">>,state,iid}
    ];
%% acallid,acallidhash,dialogid,dialogidhash,localuri,localtag,remoteuri,
%% forkrequesturi,forkremoteuri,forklocaltag,forkcallid,forkcallednum,
%% forkdir,forkextaccountcode,
%% "++ExtFields++" ---
%% invitets,eventts,invitedt,cid
get_meta(EventType) when EventType=:=fork_preanswer;EventType=:=fork_cancel ->
    [{<<"acallid">>, state, acallid},
     {<<"acallidhash">>, state, acallidhash},
     {<<"dialogid">>, state, dialogid},
     {<<"dialogidhash">>, state, dialogidhash},
     {<<"localuri">>, aside, localuri},
     {<<"localtag">>, aside, localtag},
     {<<"remoteuri">>, aside, remoteuri},
     {<<"forkrequesturi">>,fork,requesturi},
     {<<"forkremoteuri">>,fork,remoteuri},
     {<<"forklocaltag">>,fork,localtag},
     {<<"forkcallid">>,fork,callid},
     {<<"forkcallednum">>,fork,callednum, aor},
     {<<"forkdir">>,fork,dir},
     {<<"forkextaccountcode">>,fork,extaccountcode},
     {<<"invitets">>,state,its},
     {<<"invitedt">>, state, its, datetime},
     {<<"eventid">>, generate, new_uuid},
     {<<"cid">>,state,iid}
    ];
%% acallid,acallidhash,dialogid,dialogidhash,localuri,localtag,remoteuri,
%% forkrequesturi,forkremoteuri,forklocaltag,forkcallid,forkcallednum,
%% forkdir,forkextaccountcode,
%% "++ExtFields++" ---
%% invitets,eventts,invitedt,cid
get_meta(fork_answer)  ->
    [{<<"acallid">>, state, acallid},
     {<<"acallidhash">>, state, acallidhash},
     {<<"dialogid">>, state, dialogid},
     {<<"dialogidhash">>, state, dialogidhash},
     {<<"localuri">>, aside, localuri},
     {<<"localtag">>, aside, localtag},
     {<<"remoteuri">>, aside, remoteuri},
     {<<"forkrequesturi">>,fork,requesturi},
     {<<"forkremoteuri">>,fork,remoteuri},
     {<<"forklocaltag">>,fork,localtag},
     {<<"forkcallid">>,fork,callid},
     {<<"forkcallednum">>,fork,callednum,aor},
     {<<"forkdir">>,fork,dir},
     {<<"forkextaccountcode">>,fork,extaccountcode},
     {<<"esgdlg">>,opts,esgdlg},
     {<<"invitets">>,state,its},
     {<<"invitedt">>, state, its, datetime},
     {<<"cid">>,state,iid}
    ];
%% acallid,acallidhash,dialogid,dialogidhash,localuri,localtag,remoteuri,
%% forkrequesturi,forkremoteuri,forklocaltag,forkcallid,forkcallednum,
%% forkdir,forkextaccountcode,
%% "++ExtFields++" ---
%% invitets,eventts,invitedt,cid
get_meta(dlg_pickup)  ->
    [{<<"acallid">>, state, acallid},
     {<<"acallidhash">>, state, acallidhash},
     {<<"dialogid">>, state, dialogid},
     {<<"dialogidhash">>, state, dialogidhash},
     {<<"localuri">>, aside, localuri},
     {<<"localtag">>, aside, localtag},
     {<<"remoteuri">>, aside, remoteuri},
     {<<"forkrequesturi">>,fork,requesturi},
     {<<"forkremoteuri">>,fork,remoteuri},
     {<<"forklocaltag">>,fork,localtag},
     {<<"forkcallid">>,fork,callid},
     {<<"forkcallednum">>,fork,callednum},
     {<<"forknumber">>,opts,forknumber},
     {<<"forkdir">>,fork,dir},
     {<<"forkextaccountcode">>,fork,extaccountcode},
     {<<"invitets">>,state,its},
     {<<"invitedt">>, state, its, datetime},
     {<<"cid">>,state,iid}
    ];
%% acallid,acallidhash,dialogid,dialogidhash,localuri,localtag,remoteuri,
%% redirectto,redirectreason,forkrequesturi,forkremoteuri,forklocaltag,
%% forkcallid,forkcallednum,forkdir,forkextaccountcode,invitets,eventts,
%% eventid,invitedt,cid
get_meta(fork_redirect) ->
    [{<<"acallid">>,state,acallid},
     {<<"acallidhash">>,state,acallidhash},
     {<<"dialogid">>,state,dialogid},
     {<<"dialogidhash">>,state,dialogidhash},
     {<<"localuri">>,aside,localuri},
     {<<"localtag">>,aside,localtag},
     {<<"remoteuri">>,aside,remoteuri},
     {<<"redirectto">>,opts,redirect_to, aor},
     {<<"redirectreason">>,opts,reason, any},
     {<<"forkrequesturi">>,fork,requesturi},
     {<<"forkremoteuri">>,fork,remoteuri},
     {<<"forklocaltag">>,fork,localtag},
     {<<"forkcallid">>,fork,callid},
     {<<"forkcallednum">>,fork,callednum, aor},
     {<<"forkdir">>,fork,dir},
     {<<"forkextaccountcode">>,fork,extaccountcode},
     {<<"invitets">>,state,its},
     {<<"invitedt">>, state, its, datetime},
     {<<"eventid">>, generate, new_uuid},
     {<<"cid">>,state,iid}
    ];
%% acallid,acallidhash,dialogid,dialogidhash,localuri,localtag,remoteuri,
%% redirectto,redirectowner,redirectreason,invitets,eventts,eventid,invitedt,cid
get_meta(user_redirect) ->
    [{<<"acallid">>, state, acallid},
     {<<"acallidhash">>, state, acallidhash},
     {<<"dialogid">>, state, dialogid},
     {<<"dialogidhash">>, state, dialogidhash},
     {<<"localuri">>, aside,localuri},
     {<<"localtag">>, aside,localtag},
     {<<"remoteuri">>, aside,remoteuri},
     {<<"redirectto">>,opts,redirect_to, aor},
     {<<"redirectowner">>,opts,owner, aor},
     {<<"redirectreason">>,opts,reason, any},
     {<<"invitets">>,state,its},
     {<<"invitedt">>, state, its, datetime},
     {<<"eventid">>, generate, new_uuid},
     {<<"cid">>,state,iid}
    ];
%% acallid,acallidhash,dialogid,dialogidhash,alocaluri,alocaltag,aremoteuri,aremoteparty,aremotecontact,
%% bcallid,blocaluri,blocaltag,bremoteuri,bremotecontact,mgc,reclinks,isrec,invitets,eventts,invitedt,cid,
%% anumber,ausername,adomain,adisplayname,anetworkaddr,acallednum,arepresentative,aouter,aprovidercode,
%% bnumber,busername,bdomain,bdisplayname,bnetworkaddr,bouter,bprovidercode,ghost
get_meta(dlg_start) ->
    [{<<"acallid">>, state, acallid},
     {<<"acallidhash">>, state, acallidhash},
     {<<"dialogid">>, state, dialogid},
     {<<"dialogidhash">>, state, dialogidhash},
     {<<"alocaluri">>, aside,localuri},
     {<<"alocaltag">>, aside,localtag},
     {<<"aremoteuri">>, aside,remoteuri},
     {<<"aremoteparty">>,aside,last_remote_party},
     {<<"aremotecontact">>,aside,remotecontact},
     {<<"bcallid">>, bside, callid},
     {<<"blocaluri">>,bside,localuri},
     {<<"blocaltag">>,bside,localtag},
     {<<"bremoteuri">>,bside,remoteuri},
     {<<"bremotecontact">>,bside,remotecontact},
     {<<"mgc">>,media,mgc},
     {<<"reclinks">>,media,rec_links},
     {<<"isrec">>,media,is_rec},
     {<<"ghost">>,map,ghost_mode},
     {<<"invitets">>,state,its},
     {<<"invitedt">>, state, its, datetime},
     {<<"cid">>,state,iid},
     {<<"anumber">>,opts,anumber},
     {<<"ausername">>,opts,ausername},
     {<<"adomain">>,opts,adomain},
     {<<"adisplayname">>,opts,adisplayname},
     {<<"anetworkaddr">>,opts,anetworkaddr},
     {<<"acallednum">>,opts,acallednum},
     {<<"arepresentative">>,opts,arepresentative},
     {<<"aouter">>,opts,aouter},
     {<<"aprovidercode">>,opts,aprovidercode},
     {<<"bnumber">>,opts,bnumber},
     {<<"busername">>,opts,busername},
     {<<"bdomain">>,opts,bdomain},
     {<<"bdisplayname">>,opts,bdisplayname},
     {<<"bnetworkaddr">>,opts,bnetworkaddr},
     {<<"bouter">>,opts,bouter},
     {<<"bprovidercode">>,opts,bprovidercode},
     {<<"ctxscridm">>,map,ctx_scrid_master},
     {<<"ctxscridd">>,map,ctx_scrid_domain},
     {<<"relates">>,opts,relates,relates}
    ];
%% acallid,acallidhash,dialogid,dialogidhash,invitets,invitedt,domain,dir,
%% fromnum,tonum,crossdomain,isrec,recordruleid,storageruleid,eventts,cid
get_meta(rec_info) ->
    [{<<"acallid">>,state,acallid},
     {<<"acallidhash">>,state,acallidhash},
     {<<"dialogid">>,state,dialogid},
     {<<"dialogidhash">>,state,dialogidhash},
     {<<"invitets">>,state,its},
     {<<"invitedt">>, state, its, datetime},
     {<<"domain">>,opts,domain},
     {<<"dir">>,opts,dir},
     {<<"fromnum">>,opts,from_num},
     {<<"tonum">>,opts,to_num},
     {<<"crossdomain">>,opts,crossdomain},
     {<<"isrec">>,opts,rec},
     {<<"recordruleid">>,opts,recordruleid},
     {<<"storageruleid">>,opts,storageruleid},
     {<<"cid">>,state,iid}
    ];
%% acallid,acallidhash,dialogid,dialogidhash,alocaluri,alocaltag,aremoteuri,
%% bcallid,blocaluri,blocaltag,bremoteuri,mgc,reclinks,isrec,invitets,
%% eventts,eventid,invitedt,cid
get_meta(media_migrate_done) ->
    [{<<"acallid">>,state,acallid},
     {<<"acallidhash">>,state,acallidhash},
     {<<"dialogid">>,state,dialogid},
     {<<"dialogidhash">>,state,dialogidhash},
     {<<"alocaluri">>, aside,localuri},
     {<<"alocaltag">>, aside,localtag},
     {<<"aremoteuri">>, aside,remoteuri},
     {<<"bcallid">>, bside,callid},
     {<<"blocaluri">>,bside,localuri},
     {<<"blocaltag">>,bside,localtag},
     {<<"bremoteuri">>,bside,remoteuri},
     {<<"mgc">>,media,mgc},
     {<<"reclinks">>,media,rec_links},
     {<<"isrec">>,media,is_rec},
     {<<"invitets">>,state,its},
     {<<"invitedt">>, state, its, datetime},
     {<<"eventid">>, generate, new_uuid},
     {<<"cid">>,state,iid}
    ];
%% acallid,acallidhash,dialogid,dialogidhash,alocaluri,alocaltag,aremoteuri,
%% bcallid,blocaluri,blocaltag,bremoteuri,
%% xcallid,xlocaluri,xlocaltag,xremoteuri,invitets,eventts,eventid,invitedt,cid
get_meta(EType) when EType=:=hold;EType=:=unhold ->
    [{<<"acallid">>,state,acallid},
     {<<"acallidhash">>,state,acallidhash},
     {<<"dialogid">>,state,dialogid},
     {<<"dialogidhash">>,state,dialogidhash},
     {<<"alocaluri">>, aside,localuri},
     {<<"alocaltag">>, aside,localtag},
     {<<"aremoteuri">>, aside,remoteuri},
     {<<"bcallid">>, bside,callid},
     {<<"blocaluri">>,bside,localuri},
     {<<"blocaltag">>,bside,localtag},
     {<<"bremoteuri">>,bside,remoteuri},
     {<<"xcallid">>, xside,callid},
     {<<"xlocaluri">>,xside,localuri},
     {<<"xlocaltag">>,xside,localtag},
     {<<"xremoteuri">>,xside,remoteuri},
     {<<"invitets">>,state,its},
     {<<"invitedt">>, state, its, datetime},
     {<<"eventid">>, generate, new_uuid},
     {<<"cid">>,state,iid}
    ];
%% acallid,acallidhash,dialogid,dialogidhash,alocaluri,alocaltag,aremoteuri,
%% bcallid,blocaluri,blocaltag,bremoteuri,
%% xcallid,xlocaluri,xlocaltag,xremoteuri,invitets,eventts,eventid,invitedt,cid
%% dtmf,proto
get_meta(EType) when EType=:=dtmf ->
    [{<<"acallid">>,state,acallid},
     {<<"acallidhash">>,state,acallidhash},
     {<<"dialogid">>,state,dialogid},
     {<<"dialogidhash">>,state,dialogidhash},
     {<<"alocaluri">>, aside,localuri},
     {<<"alocaltag">>, aside,localtag},
     {<<"aremoteuri">>, aside,remoteuri},
     {<<"bcallid">>, bside,callid},
     {<<"blocaluri">>,bside,localuri},
     {<<"blocaltag">>,bside,localtag},
     {<<"bremoteuri">>,bside,remoteuri},
     {<<"xcallid">>, xside,callid},
     {<<"xlocaluri">>,xside,localuri},
     {<<"xlocaltag">>,xside,localtag},
     {<<"xremoteuri">>,xside,remoteuri},
     {<<"invitets">>,state,its},
     {<<"invitedt">>, state, its, datetime},
     {<<"eventid">>, generate, new_uuid},
     {<<"dtmf">>,opts,dtmf},
     {<<"proto">>,opts,proto},
     {<<"cid">>,state,iid}
    ];
%% acallid,acallidhash,dialogid,dialogidhash,alocaluri,alocaltag,aremoteuri,
%% bcallid,blocaluri,blocaltag,bremoteuri,referto,referredby,referring,
%% invitets,eventts,eventid,invitedt,cid
get_meta(refer) ->
    [{<<"acallid">>,state,acallid},
     {<<"acallidhash">>,state,acallidhash},
     {<<"dialogid">>,state,dialogid},
     {<<"dialogidhash">>,state,dialogidhash},
     {<<"alocaluri">>,aside,localuri},
     {<<"alocaltag">>,aside,localtag},
     {<<"aremoteuri">>,aside,remoteuri},
     {<<"bcallid">>,bside,callid},
     {<<"blocaluri">>,bside,localuri},
     {<<"blocaltag">>,bside,localtag},
     {<<"bremoteuri">>,bside,remoteuri},
     {<<"referto">>,opts,refer_to, aor},
     {<<"referredby">>,opts,referred_by, aor},
     {<<"referring">>,opts,referring, aor},
     {<<"invitets">>,state,its},
     {<<"invitedt">>, state, its, datetime},
     {<<"eventid">>, generate, new_uuid},
     {<<"cid">>,state,iid}
    ];
%% acallid,acallidhash,adialogid,adialogidhash,ainvitets,
%% bcallid,bdialogid,bdialogidhash,binvitets,eventts,eventid,invitedt,cid
get_meta(replaces_refer) ->
    [{<<"bdialogid">>,state,dialogid},
     {<<"bdialogidhash">>,state,dialogidhash},
     {<<"binvitets">>,state,its},
     {<<"bcallid">>,bside,callid},
     {<<"acallid">>,owner,acallid},
     {<<"acallidhash">>,owner,acallidhash},
     {<<"adialogid">>,owner,dialogid},
     {<<"adialogidhash">>,owner,dialogidhash},
     {<<"acid">>,owner,iid},
     {<<"ainvitets">>,owner,its},
     {<<"invitets">>,state,its},
     {<<"invitedt">>,state,its,datetime},
     {<<"eventid">>, generate, new_uuid},
     {<<"cid">>,state,iid} %! from state nor owner
    ];
%% acallid,acallidhash,dialogid,dialogidhash,alocaluri,alocaltag,aremoteuri,
%% bcallid,blocaluri,blocaltag,bremoteuri,binvitets,
%% owneracallid,owneracallidhash,ownerinvitets,eventts,eventid,invitedt,cid
get_meta(replaces_invite) ->
    [{<<"acallid">>,state,acallid},
     {<<"acallidhash">>,state,acallidhash},
     {<<"dialogid">>,state,dialogid},
     {<<"dialogidhash">>,state,dialogidhash},
     {<<"alocaluri">>,aside,localuri},
     {<<"alocaltag">>,aside,localtag},
     {<<"aremoteuri">>,aside,remoteuri},
     {<<"bcallid">>,bside,callid},
     {<<"blocaluri">>,bside,localuri},
     {<<"blocaltag">>,bside,localtag},
     {<<"bremoteuri">>,bside,remoteuri},
     {<<"binvitets">>,state,its},
     {<<"invitets">>,state,its},
     {<<"invitedt">>,state,its,datetime},
     {<<"owneracallid">>,owner,acallid},
     {<<"owneracallidhash">>,owner,acallidhash},
     {<<"ownerinvitets">>,owner,its},
     {<<"ownercid">>,owner,iid},
     {<<"eventid">>, generate, new_uuid},
     {<<"cid">>,state,iid}
    ];
%% acallid,acallidhash,dialogid,dialogidhash,alocaluri,alocaltag,aremoteuri,
%% bcallid,blocaluri,blocaltag,bremoteuri,binvitets,
%% owneracallid,owneracallidhash,ownerinvitets,eventts,eventid,invitedt,cid
get_meta(referring_invite) ->
    [{<<"acallid">>,state,acallid},
     {<<"acallidhash">>,state,acallidhash},
     {<<"dialogid">>,state,dialogid},
     {<<"dialogidhash">>,state,dialogidhash},
     {<<"alocaluri">>,aside,localuri},
     {<<"alocaltag">>,aside,localtag},
     {<<"aremoteuri">>,aside,remoteuri},
     {<<"bcallid">>,bside,callid},
     {<<"blocaluri">>,bside,localuri},
     {<<"blocaltag">>,bside,localtag},
     {<<"bremoteuri">>,bside,remoteuri},
     {<<"binvitets">>,state,its},
     {<<"invitets">>,state,its},
     {<<"invitedt">>,state,its,datetime},
     {<<"owneracallid">>,owner,acallid},
     {<<"owneracallidhash">>,owner,acallidhash},
     {<<"ownerinvitets">>,owner,its},
     {<<"ownercid">>,owner,iid},
     {<<"eventid">>, generate, new_uuid},
     {<<"cid">>,state,iid}
    ];
%% acallid,acallidhash,dialogid,dialogidhash,alocaluri,alocaltag,aremoteuri,
%% bcallid,blocaluri,blocaltag,bremoteuri,
%% mgc,reclinks,isrec,stoptype,stopreason,
%% stopoperations,stopcallid,stopside,stopsipcode,invitets,eventts,invitedt,cid,ghost
get_meta(EType) when EType==dlg_stop; EType==dlg_end ->
    [{<<"acallid">>,state,acallid},
     {<<"acallidhash">>,state,acallidhash},
     {<<"dialogid">>,state,dialogid},
     {<<"dialogidhash">>,state,dialogidhash},
     {<<"alocaluri">>,aside,localuri},
     {<<"alocaltag">>,aside,localtag},
     {<<"aremoteuri">>,aside,remoteuri},
     {<<"bcallid">>,bside,callid},
     {<<"blocaluri">>,bside,localuri},
     {<<"blocaltag">>,bside,localtag},
     {<<"bremoteuri">>,bside,remoteuri},
     {<<"mgc">>,media,mgc},
     {<<"reclinks">>,media,rec_links},
     {<<"isrec">>,media,is_rec},
     {<<"adomain">>,opts,adomain},
     {<<"bdomain">>,opts,bdomain},
     {<<"aisrec">>,aside,is_rec},
     {<<"bisrec">>,bside,is_rec},
     {<<"stoptype">>,stopreason,type},
     {<<"stopreason">>,stopreason,reason, any},
     {<<"stopoperations">>,stopreason,operations, any},
     {<<"stopcallid">>,stopreason,callid},
     {<<"stopside">>,stopreason,side},
     {<<"stopsipcode">>,stopreason,sipcode},
     {<<"ghost">>,map,ghost_mode},
     {<<"invitets">>,state,its},
     {<<"invitedt">>, state, its, datetime},
     {<<"cid">>,state,iid},
     {<<"ctxscridm">>,map,ctx_scrid_master},
     {<<"ctxscridd">>,map,ctx_scrid_domain},
     {<<"relates">>,opts,relates,relates}
    ];
%% acallid,acallidhash,dialogid,dialogidhash,invitets,eventts,invitedt,cid,ghost
get_meta(dlg_dead) ->
    [{<<"acallid">>,state,acallid},
     {<<"acallidhash">>,state,acallidhash},
     {<<"dialogid">>,state,dialogid},
     {<<"dialogidhash">>,state,dialogidhash},
     {<<"ghost">>,opts,ghost_mode},
     {<<"invitets">>,state,its},
     {<<"invitedt">>, state,its,datetime},
     {<<"cid">>,state,iid}
    ];
%% acallid,acallidhash,invitets,cid,dlgbinding,dlgbindingset
get_meta(dlg_binding) ->
    [{<<"acallid">>,state,acallid},
     {<<"acallidhash">>,state,acallidhash},
     {<<"invitets">>,state,its},
     {<<"invitedt">>, state, its, datetime},
     {<<"cid">>,state,iid},
     {<<"dlgbinding">>, opts, dlgbinding},
     {<<"dlgbindingset">>, opts, dlgbindingset}
    ];
get_meta(_) ->
    [].

%% ====================================================================
%% Internal functions
%% ====================================================================
