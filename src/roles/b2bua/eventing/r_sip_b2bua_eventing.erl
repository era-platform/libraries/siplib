%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc
%%% @todo

-module(r_sip_b2bua_eventing).
-author('Peter Bukashin <tbotc@yandex.ru>').

%% -------------------------------------------------------------------
%%
%% -------------------------------------------------------------------

-compile([export_all,nowarn_export_all]).

%% ==========================================================================
%% Defines
%% ==========================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").

-define(OUTD(Fmt,Args), ok).

-define(MixerEvents, r_sip_b2bua_eventing_mixerevents).

%% ====================================================================
%% API functions
%% ====================================================================

% when incoming invite
% Opts: #{ruri::#uri{}, to::#uri{}, from::#uri{}, fromusername, fromnumber, fromdomain, fromouter, fromprovidercode, callednum, networkaddr, is_referred, [referred_by::#uri{}, ref_dialogid::binary(), ref_acallid::binary(), ref_side::binary], is_replacing}
% ACallIdData::{ACallId::binary(),ACallIdHash::integer()}
% InviteData::{InviteId::uuid(),InviteTS::integer()}
invite({_ACallIdData,{InviteId,_}=_InviteData}=Id, Opts) ->
    Event = ?EVENTHANDLER:prepare_event(invite,Id,Opts),
    send_event(InviteId,Event,Opts).

% when route fails
% Opts: #{action:reply|proxy, [sipcode::integer()], reason::atom()|string(), [srvidx::integer()]}
% ACallIdData::{ACallId::binary(),ACallIdHash::integer()}
% InviteData::{InviteId::uuid(),InviteTS::integer()}
route_fin({_ACallIdData,{InviteId,_}=_InviteData}=Id, Opts) ->
    Event = ?EVENTHANDLER:prepare_event(route_fin,Id,Opts),
    send_event(InviteId, Event,Opts).

% when route direct replaces
% Opts: #{to::#uri{}, replaced_callid::binary(), replaced_ts::integer(), replaced_dlg::binary(), replaced_acallid::binary()}
% ACallIdData::{ACallId::binary(),ACallIdHash::integer()}
% InviteData::{InviteId::uuid(),InviteTS::integer()}
route_replacing({_ACallIdData,{InviteId,_}=_InviteData}=Id, Opts)  ->
    Event = ?EVENTHANDLER:prepare_event(route_replacing,Id,Opts),
    send_event(InviteId,Event,Opts).

% when route iteration complete
% Opts: #{domain::binary(), ruleid::guid(), action::binary(), vector::binary(), isreferred::boolean(), isredirected::boolean(), [to::aor(),from::aor(),extnumber::binary(),account::binary()]}
% ACallIdData::{ACallId::binary(),ACallIdHash::integer()}
% InviteData::{InviteId::uuid(),InviteTS::integer()}
route({_ACallIdData,{InviteId,_}=_InviteData}=Id, Opts) ->
    Event = ?EVENTHANDLER:prepare_event(route,Id,Opts),
    send_event(InviteId,Event,Opts).

% when route iteration got redirect
% Opts: #{redirect_to::aor(), owner::aor(), reason::binary()}
% ACallIdData::{ACallId::binary(),ACallIdHash::integer()}
% InviteData::{InviteId::uuid(),InviteTS::integer()}
route_redirect({_ACallIdData,{InviteId,_}=_InviteData}=Id, Opts) ->
    Event = ?EVENTHANDLER:prepare_event(route_redirect,Id,Opts),
    send_event(InviteId,Event,Opts).

% when route iteration found feature
% Opts: #{featurecode_type::binary(), featurecode_id::guid(), featurecode_extension::binary(), number::binary()}
% ACallIdData::{ACallId::binary(),ACallIdHash::integer()}
% InviteData::{InviteId::uuid(),InviteTS::integer()}
route_feature({_ACallIdData,{InviteId,_}=_InviteData}=Id, Opts) ->
    Event = ?EVENTHANDLER:prepare_event(route_feature,Id,Opts),
    send_event(InviteId,Event,Opts).

% when route refer
% Opts: #{is_referred::bool(), by_uri::uri(), link_referred::binary() [,link_replaces::binary()]}
% ACallIdData::{ACallId::binary(),ACallIdHash::integer()}
% InviteData::{InviteId::uuid(),InviteTS::integer()}
route_referred({_ACallIdData,{InviteId,_}=_InviteData}=Id, Opts) ->
    Event = ?EVENTHANDLER:prepare_event(route_referred,Id,Opts),
    send_event(InviteId,Event,Opts).
%% ==================

% when dialog init (after routing)
% DialogIdData::{DlgId::binary(),DlgIdHash::integer()}, ACallIdData::{ACallId::binary(),ACallIdHash::integer()},
% InviteData::{InviteId::uuid(),InviteTS::integer()}
dlg_init({_DialogIdData,_ACallIdData,{InviteId,_}=_InviteData}=Id, Opts) ->
    Event = ?EVENTHANDLER:prepare_event(dlg_init,Id,Opts),
    send_event(InviteId,Event,Opts).

% when dialog begin call (after init)
% State::#state_forking{}
dlg_call(#state_forking{invite_id=InviteId}=State, Opts) ->
    %%  ?OUT("Event dlg_call ~120tp", [dlgcid(State)]),
    Event = ?EVENTHANDLER:prepare_event(dlg_call,State,Opts),
    send_event(InviteId,Event,Opts).

% when fork start invite
% Fork::#side_fork{}, State::#state_forking{}
% Opts: #{busername, bdomain, bnetworkaddr, bouter::boolean(), bprovidercode::binary() | null}
fork_start(Fork, #state_forking{invite_id=InviteId}=State, Opts) ->
    %%  ?OUT("Event fork_start ~120tp, ~120tp, ~120tp", [fork(Fork),dlgcid(State),Opts]),
    Event = ?EVENTHANDLER:prepare_event(fork_start,Fork,State,Opts),
    send_event(InviteId,Event,Opts).

% when fork got 1xx
% Fork::#side_fork{}, State::#state_forking{}
% Opts: #{resp ::{resp,SipCode,Reason}}
fork_preanswer(Fork, #state_forking{invite_id=InviteId}=State, Opts) ->
    %%  ?OUT("Event fork_preanswer ~120tp, ~120tp", [fork(Fork),dlgcid(State)]),
    Event = ?EVENTHANDLER:prepare_event(fork_preanswer,Fork,State,Opts),
    send_event(InviteId,Event,Opts).

% when fork got 2xx
% Fork::#side_fork{}, State::#state_forking{}
fork_answer(Fork, #state_forking{invite_id=InviteId}=State, Opts) ->
    %%  ?OUT("Event fork_answer ~120tp, ~120tp", [fork(Fork),dlgcid(State)]),
    Event = ?EVENTHANDLER:prepare_event(fork_answer,Fork,State,Opts),
    send_event(InviteId,Event,Opts).

% when fork got 3xx, 4xx-6xx or timeout
% Fork::#side_fork{}, State::#state_forking{}
fork_stop(Fork, #state_forking{invite_id=InviteId}=State, Opts) ->
    %%  ?OUT("Event fork_stop ~120tp, ~120tp", [fork(Fork),dlgcid(State)]),
    Event = ?EVENTHANDLER:prepare_event(fork_stop,Fork,State,Opts),
    send_event(InviteId,Event,Opts).

% when fork cancelled by b-answer or a-cancel
% Fork::#side_fork{}, State::#state_forking{}
fork_cancel(Fork, #state_forking{invite_id=InviteId}=State, Opts) ->
    %%  ?OUT("Event fork_cancel ~120tp, ~120tp", [fork(Fork),dlgcid(State)]),
    Event = ?EVENTHANDLER:prepare_event(fork_cancel,Fork,State,Opts),
    send_event(InviteId,Event,Opts).

% when fork redirected by 3xx
% Opts: #{redirect_to::aor(), reason::binary()}
% Fork::#side_fork{}, State::#state_forking{}
fork_redirect(Fork, #state_forking{invite_id=InviteId}=State, Opts) ->
    %%  ?OUT("Event fork_redirect ~120tp, ~120tp, ~120tp", [fork(Fork),Opts,dlgcid(State)]),
    Event = ?EVENTHANDLER:prepare_event(fork_redirect,Fork,State,Opts),
    send_event(InviteId,Event,Opts).

% when user redirected after fin of his forks
% Opts: #{redirect_to::aor(), owner::aor(), reason::binary()}
% State::#state_forking{}
user_redirect(#state_forking{invite_id=InviteId}=State, Opts) ->
    %%  ?OUT("Event user_redirect ~120tp, ~120tp", [Opts,dlgcid(State)]),
    Event = ?EVENTHANDLER:prepare_event(user_redirect,State,Opts),
    send_event(InviteId,Event,Opts).

% when dialog picked up
% State::#state_forking{}
dlg_pickup(#side_fork{}=Fork, #state_forking{invite_id=InviteId}=State, Opts) ->
    %%  ?OUT("Event dlg_pickup ~120tp, ~120tp", [fork(Fork),dlgcid(State)]),
    Event = ?EVENTHANDLER:prepare_event(dlg_pickup,Fork,State,Opts),
    send_event(InviteId,Event,Opts).

% when dialog starts (after answer).
% Opts: #{ausername,adomain,adisplayname,anetworkaddr,acallednum,arepresentative,aouter,aprovidercode,busername,bdomain,bdisplayname,bnetworkaddr,bouter,bprovidercode}
% State::#state_dlg{}
dlg_start(#state_dlg{invite_id=InviteId}=State, Opts) ->
    %%  ?OUT("Event dlg_start ~120tp ~120tp", [dlgcid(State), Opts]),
    Event = ?EVENTHANDLER:prepare_event(dlg_start,State,Opts),
    send_event(InviteId,Event,Opts).

% before dialog starts when domain recording rule defined.
% Opts: #{domain::binary(), dir::inner|incoming|outgoing, from_num::binary(), to_num::binary(), crossdomain::binary(), recordruleid::uuid()|null, rec::boolean(), storageruleid::uuid()|null}
% State::#state_dlg{}
rec_info(#state_dlg{invite_id=InviteId}=State, Opts) ->
    %%  ?OUT("Event rec_info ~120tp, ~120tp", [Opts,dlgcid(State)]),
    Event = ?EVENTHANDLER:prepare_event(rec_info,State,Opts),
    send_event(InviteId,Event,Opts).

%% ==================

% when dialog's media start migrating to another mg
% State::#state_dlg{}
media_migrate(_State, _Opts) ->
    ok.
%%      ?OUT("Event media_migrate ~120tp", [dlgcid(State)]),
%%      Event = ?EVENTHANDLER:prepare_event(media_migrate,State,Opts),
%%      send_event(Event,Opts).

% when dialog's media finished migrate to another mg
% State::#state_dlg{}
media_migrate_done(#state_dlg{invite_id=InviteId}=State, Opts) ->
    %%  ?OUT("Event media_migrate_done ~120tp", [dlgcid(State)]),
    Event = ?EVENTHANDLER:prepare_event(media_migrate_done,State,Opts),
    send_event(InviteId,Event,Opts).

% when dialog got reinvite
% XSide::#side{}, State::#state_dlg{}
session_change(_XSide, _State, _Opts) ->
    ok.
%%      ?OUT("Event session_change ~120tp, ~120tp", [side(XSide), dlgcid(State)]),
%%      Event = ?EVENTHANDLER:prepare_event(session_change,XSide,State,Opts),
%%      send_event(Event,Opts).

% when call holded (could be double side)
% XSide::#side{}, State::#state_dlg{}
hold(XSide, #state_dlg{invite_id=InviteId}=State, Opts) ->
    %%  ?OUT("Event hold ~120tp, ~120tp", [side(XSide), dlgcid(State)]),
    Event = ?EVENTHANDLER:prepare_event(hold,XSide,State,Opts),
    send_event(InviteId,Event,Opts).

% when call unholded (could be double side)
% XSide::#side{}, State::#state_dlg{}
unhold(XSide, #state_dlg{invite_id=InviteId}=State, Opts) ->
    %%  ?OUT("Event unhold ~120tp, ~120tp", [side(XSide), dlgcid(State)]),
    Event = ?EVENTHANDLER:prepare_event(unhold,XSide,State, Opts),
    send_event(InviteId,Event,Opts).

% when got dtmf (could be double side)
% XSide::#side{}, State::#state_dlg{}
dtmf(XSide, #state_dlg{invite_id=InviteId}=State, Opts) ->
    Event = ?EVENTHANDLER:prepare_event(dtmf,XSide,State,Opts),
    send_event(InviteId,Event,Opts).

% when call forwards refer (A->B)
% Opts: #{refer_to::aor(), referred_by::aor(), referring::aor()}
refer(#state_dlg{invite_id=InviteId}=State, Opts) ->
    %%  ?OUTD("Event refer ~120tp, ~120tp", [id(dlgcid(State)), side(XSide)]),
    Event = ?EVENTHANDLER:prepare_event(refer,State,Opts),
    send_event(InviteId,Event,Opts).

% when dialog got refer's modify_replace command (from referring dlg) (B->C <= A->B)
% DialogIdData::{DlgId::binary(),DlgIdHash::integer()}, ACallIdData::{binary(),integer()}, State::#state_dlg{}|#state_forking{}
% ACallIdData::{ACallId::binary(),ACallIdHash::integer()}, State::#state_dlg{}|#state_forking{}
% InviteData::{InviteId::uuid(),InviteTS::integer()}
replaces_refer({_DialogIdData,_ACallIdData,{InviteId,_}=_InviteData}=Id, State, Opts) ->
    Event = ?EVENTHANDLER:prepare_event(replaces_refer,Id,State,Opts),
    send_event(InviteId,Event,Opts).

% when dialog got replacing link command (from replacing invite routing) (B->C <= A->C)
% ACallIdData::{ACallId::binary(),ACallIdHash::integer()}, State::#state_dlg{}|#state_forking{}
% InviteData::{InviteId::uuid(),InviteTS::integer()}
replaces_invite({_ACallIdData,{InviteId,_}=_InviteData}=Id, State, Opts) ->
    Event = ?EVENTHANDLER:prepare_event(replaces_invite,Id,State,Opts),
    send_event(InviteId,Event,Opts).

% when dialog got referring link command (from referring invite routing) (A->B <= A->C)
% ACallIdData::{ACallId::binary(),ACallIdHash::integer()}, State::#state_dlg{}|#state_forking{}
% InviteData::{InviteId::uuid(),InviteTS::integer()}
referring_invite({_ACallIdData,{InviteId,_}=_InviteData}=Id, State, Opts) ->
    Event = ?EVENTHANDLER:prepare_event(referring_invite,Id,State,Opts),
    send_event(InviteId,Event,Opts).

%% ==================

% when dialog finished correctly (Reason:#{type,reason,[callid,side,operations]})
% State::#state_dlg{}|#state_forking{}
%   stopreason: #{type:atom, reason::binary(), [operations::list(), callid::binary(), sipcode::integer()]}
dlg_stop(#state_dlg{invite_id=InviteId,stopreason=#{}=_Reason,a=A,b=B}=State, Opts) ->
    %%  ?OUT("Event dlg_stop ~120tp, ~120tp", [Reason, dlgcid(State)]),
    Event1 = ?EVENTHANDLER:prepare_event(dlg_end,State,Opts),
    Event2 = ?EVENTHANDLER:prepare_event(dlg_stop,State,Opts),
    send_event(InviteId,Event1,Opts),
    erlang:spawn(fun() ->
                    ?MODULE:send_event_delayed(Event2,Opts),
                    ?MixerEvents:make_mixer_event(Event2,{A,B})
                 end),
    ok;
dlg_stop(#state_forking{invite_id=InviteId,stopreason=#{}=_Reason}=State, Opts) ->
    %%  ?OUT("Event dlg_stop ~120tp, ~120tp", [Reason, dlgcid(State)]),
    Event1 = ?EVENTHANDLER:prepare_event(dlg_end,State,Opts),
    Event2 = ?EVENTHANDLER:prepare_event(dlg_stop,State,Opts),
    send_event(InviteId,Event1,Opts),
    erlang:spawn(?MODULE,send_event_delayed,[Event2,Opts]),
    ok.

% when dialog dead
% DialogIdData::{DlgId::binary(),DlgIdHash::integer()}
% ACallIdData::{ACallId::binary(),ACallIdHash::integer()}
% InviteData::{InviteId::uuid(),InviteTS::integer()}
dlg_dead({_DialogIdData,_ACallIdData,{InviteId,_}=_InviteData}=Id, Opts) ->
    Event = ?EVENTHANDLER:prepare_event(dlg_dead,Id,Opts),
    send_event(InviteId,Event,Opts).

% when dialog labeled by other entities
% ACallIdData::{ACallId::binary(),ACallIdHash::integer()}
% InviteData::{InviteId::uuid(),InviteTS::integer()}
% #{dlgbinding := BindingKey::binary(), dlgbindingset := Op}
dlg_bindings({_ACallIdData,{InviteId,_}=_InviteData}=Id, Opts) ->
    Event = ?EVENTHANDLER:prepare_event(dlg_binding,Id,Opts),
    send_event(InviteId, Event,Opts).

%% ====================================================================
%% Internal functions
%% Send events
%% ====================================================================

send_event(InviteId, Event, Opts) ->
    ?WORKER:cast_workf(InviteId, fun() -> send_event_worker(Event,Opts) end).

%% @private
send_event_worker(Event,Opts) ->
    case is_cdr_allowed() of
        false -> ok; % RP-363
        true -> ?ENVEVENTGATE:send_event(Event,make_destinations(Opts))
    end.

send_event_delayed(Event,Opts) ->
    case is_cdr_allowed() of
        false -> ok; % RP-363
        true ->
            timer:sleep(3000),
            ?ENVEVENTGATE:send_event(Event,make_destinations(Opts))
    end.

is_cdr_allowed() -> true.
%is_cdr_allowed() -> ?ENVLIC:is_cdr_allowed().

%% @private
make_destinations(Opts) ->
    ScriptIdM = maps:get(ctx_scrid_master,Opts),
    ScriptIdD = maps:get(ctx_scrid_domain,Opts),
    Relates = maps:get(relates,Opts,[]),
    Domains = [Domain || {Domain,_} <- Relates],
    ADomain = lists:nth(1,Domains),
    #{kafka => lists:usort(Domains), % RP-1853
      wssubscr => [{Domain,[{Type,ObjKey}]} || {Domain,{Type,ObjKey}} <- Relates],
      svcscript => [{ScriptIdM,?EU:to_binary(?ENVCFG:get_general_domain()),<<"b2b">>},
                    {ScriptIdD,ADomain,<<"b2b">>}],
      sq => false}. % DEBUG 2021-11-25

