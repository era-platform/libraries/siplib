%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc
%%% @todo

-module(r_sip_b2bua_eventing_log).
-author('Peter Bukashin <tbotc@yandex.ru>').

%% -------------------------------------------------------------------
%%
%% -------------------------------------------------------------------

-export([]).

-compile([export_all,nowarn_export_all]).

%% ==========================================================================
%% Defines
%% ==========================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").

%-define(OUTD(Fmt,Args), ok).
-define(OUTD(Fmt,Args), ?LOGCDR(Fmt,Args)).

%% ====================================================================
%% API functions
%% ====================================================================

% write to cdr log
logcdr(Fmt,Args) -> ?OUTD(Fmt,Args).

%% ==================

% when incoming invite
% Opts: #{ruri::#uri{}, to::#uri{}, from::#uri{}, fromusername, fromnumber, fromdomain, fromouter, fromprovidercode, callednum, networkaddr, is_referred, [referred_by::#uri{}, ref_dialogid::binary(), ref_acallid::binary(), ref_side::binary], is_replacing, esgdlg}
% ACallIdData::{ACallId::binary(),ACallIdHash::integer()}
% InviteData::{InviteId::uuid(),InviteTS::integer()}
invite({{ACallId,_}=_ACallIdData,_InviteData}=Id, Opts) ->
    ?OUTD("invite ~1000tp, ~1000tp, ~1000tp", [id(Id), ACallId, Opts]),
    ?EVENT_CDR:invite(Id, Opts).

% when route fails
% Opts: #{action:reply|proxy, [sipcode::integer()], reason::atom()|string(), [srvidx::integer()]}
% ACallIdData::{ACallId::binary(),ACallIdHash::integer()}
% InviteData::{InviteId::uuid(),InviteTS::integer()}
route_fin({_ACallIdData,_InviteData}=Id, Opts) ->
    ?OUTD("route_fin ~1000tp, ~1000tp", [id(Id), Opts]),
    ?EVENT_CDR:route_fin(Id, Opts).

% when route direct replaces
% REFER REPLACES EVENT (dlg 3 <- dlg 2)
% Opts: #{to::#uri{}, replaced_callid::binary(), replaced_ts::integer(), replaced_dlg::binary(), replaced_acallid::binary()}
% ACallIdData::{ACallId::binary(),ACallIdHash::integer()}
% InviteData::{InviteId::uuid(),InviteTS::integer()}
route_replacing({_ACallIdData,_InviteData}=Id, Opts)  ->
    ?OUTD("route_replacing ~1000tp, ~1000tp", [id(Id), Opts]),
    ?EVENT_CDR:route_replacing(Id, Opts).

% when route find operation complete
% Opts: #{findopts::list of params, rules::list of ids}
% ACallIdData::{ACallId::binary(),ACallIdHash::integer()}
% InviteData::{InviteId::uuid(),InviteTS::integer()}
route_find({_ACallIdData,_InviteData}=Id, Opts) ->
    ?OUTD("find_route: ~1000tp, ~1000tp", [id(Id), Opts]).

% when route iteration complete
% Opts: #{domain::binary(), ruleid::guid(), action::binary(), vector::binary(), isreferred::boolean(), isredirected::boolean(), [to::aor(),from::aor(),extnumber::binary(),account::binary()]}
% ACallIdData::{ACallId::binary(),ACallIdHash::integer()}
% InviteData::{InviteId::uuid(),InviteTS::integer()}
route({_ACallIdData,_InviteData}=Id, Opts) ->
    ?OUTD("route ~1000tp, ~1000tp", [id(Id), Opts]),
    ?EVENT_CDR:route(Id, Opts).

% when route iteration got redirect
% Opts: #{redirect_to::aor(), owner::aor(), reason::binary()}
% ACallIdData::{ACallId::binary(),ACallIdHash::integer()}
% InviteData::{InviteId::uuid(),InviteTS::integer()}
route_redirect({_ACallIdData,_InviteData}=Id, Opts) ->
    ?OUTD("route_redirect ~1000tp, ~1000tp", [id(Id), Opts]),
    ?EVENT_CDR:route_redirect(Id, Opts).

% when route iteration found feature
% Opts: #{featurecode_type::binary(), featurecode_id::guid(), featurecode_extension::binary(), number::binary()}
% ACallIdData::{ACallId::binary(),ACallIdHash::integer()}
% InviteData::{InviteId::uuid(),InviteTS::integer()}
route_feature({_ACallIdData,_InviteData}=Id, Opts) ->
    ?OUTD("route_feature ~1000tp, ~1000tp", [id(Id), Opts]),
    ?EVENT_CDR:route_feature(Id, Opts).

% when route refer
% Opts: #{is_referred::bool(), by_uri::uri(), link_referred::binary(), link_replaces::binary()}
% ACallIdData::{ACallId::binary(),ACallIdHash::integer()}
% InviteData::{InviteId::uuid(),InviteTS::integer()}
route_referred({_ACallIdData,_InviteData}=Id, Opts) ->
    ?OUTD("route_referred ~1000tp, ~1000tp", [id(Id), Opts]),
    ?EVENT_CDR:route_referred(Id, Opts).

% when apply representative complete (no need to cdr-db, only to log)
% ACallIdData::{ACallId::binary(),ACallIdHash::integer()}
% InviteData::{InviteId::uuid(),InviteTS::integer()}
representative({_ACallIdData,_InviteData}=Id, Opts) ->
    ?OUTD("representative ~1000tp, ~1000tp", [id(Id), Opts]).

%% ==================

% when dialog init (after routing)
% DialogIdData::{DlgId::binary(),DlgIdHash::integer()}
% ACallIdData::{ACallId::binary(),ACallIdHash::integer()}
% InviteData::{InviteId::uuid(),InviteTS::integer()}
dlg_init({_DialogIdData,_ACallIdData,_InviteData}=Id, Opts) ->
    ?OUTD("dlg_init ~1000tp", [id(Id)]),
    ?EVENT_CDR:dlg_init(Id, Opts).

% when dialog begin call (after init)
% State::#state_forking{}
dlg_call(State, Opts) ->
    ?OUTD("dlg_call ~1000tp", [id(dlgcid(State))]),
    ?EVENT_CDR:dlg_call(State, Opts).

% when fork start invite
% Fork::#side_fork{}, State::#state_forking{}
% Opts: #{busername, bdomain, bnetworkaddr, bouter::boolean(), bprovidercode::binary() | null}
fork_start(Fork, State, Opts) ->
    ?OUTD("fork_start ~1000tp, ~1000tp, ~1000tp", [id(dlgcid(State)),fork(Fork),Opts]),
    ?EVENT_CDR:fork_start(Fork, State, Opts).

% when fork got 1xx
% Fork::#side_fork{}, State::#state_forking{}
% Opts: #{resp::{resp,Code,Reason}}
fork_preanswer(Fork, State, Opts) ->
    SipCodeReason = maps:get(resp,Opts),
    ?OUTD("fork_preanswer ~1000tp, ~1000tp, ~1000tp", [id(dlgcid(State)),fork(Fork),SipCodeReason]),
    ?EVENT_CDR:fork_preanswer(Fork, State, Opts).

% when fork got 2xx
% Fork::#side_fork{}, State::#state_forking{}
% Opts: #{resp,esgdlg,relates,ctx_scrid_master,ctx_scrid_domain}
fork_answer(Fork, State, Opts) ->
    SipCodeReason = maps:get(resp,Opts),
    ?OUTD("fork_answer ~1000tp, ~1000tp, ~1000tp", [id(dlgcid(State)),fork(Fork),SipCodeReason]),
    ?EVENT_CDR:fork_answer(Fork, State, Opts).

% when fork got 3xx, 4xx-6xx or timeout
% Fork::#side_fork{}, State::#state_forking{}
fork_stop(Fork, State, Opts) ->
    Fork1 = update_fork_res(Fork),
    ?OUTD("fork_stop ~1000tp, ~1000tp, ~1000tp", [id(dlgcid(State)),fork(Fork1),Fork1#side_fork.res]),
    ?EVENT_CDR:fork_stop(Fork1, State, Opts).

% when fork cancelled by b-answer or a-cancel
% Fork::#side_fork{}, State::#state_forking{}
fork_cancel(Fork, State, Opts) ->
     ?OUTD("fork_cancel ~1000tp, ~1000tp", [id(dlgcid(State)), fork(Fork)]),
    ?EVENT_CDR:fork_cancel(Fork, State, Opts).

% when fork redirected by 3xx
% Opts: #{redirect_to::aor(), reason::binary()}
% Fork::#side_fork{}, State::#state_forking{}
fork_redirect(Fork, State, Opts) ->
    ?OUTD("fork_redirect ~1000tp, ~1000tp, ~1000tp", [id(dlgcid(State)),fork(Fork),Opts]),
    ?EVENT_CDR:fork_redirect(Fork, State, Opts).


% when user redirected after fin of his forks
% Opts: #{redirect_to::aor(), owner::aor(), reason::binary()}
% State::#state_forking{}
user_redirect(State, Opts) ->
    ?OUTD("user_redirect ~1000tp, ~1000tp", [id(dlgcid(State)),Opts]),
    ?EVENT_CDR:user_redirect(State, Opts).

% when dialog picked up
% State::#state_forking{}
dlg_pickup(#side_fork{}=Fork, State, Opts) ->
    ?OUTD("dlg_pickup ~1000tp, ~1000tp", [id(dlgcid(State)),fork(Fork)]),
    ?EVENT_CDR:dlg_pickup(Fork, State, Opts).

% when dialog starts (after answer).
% Opts: #{ausername,adomain,adisplayname,anetworkaddr,acallednum,arepresentative,aouter::boolean(),aprovidercode::binary()|'null',busername,bdomain,bdisplayname,bnetworkaddr,bouter::boolean(),bprovidercode::binary()|null}
% State::#state_dlg{}
dlg_start(State, Opts) ->
    ?OUTD("dlg_start ~1000tp, ~1000tp", [id(dlgcid(State)), Opts]),
    ?EVENT_CDR:dlg_start(State, Opts).

% before dialog starts when domain recording rule defined.
% Opts: #{domain::binary(), dir::inner|incoming|outgoing, from_num::binary(), to_num::binary(), crossdomain::binary(), recordruleid::uuid()|null, rec::boolean(), storageruleid::uuid()|null}
% State::#state_dlg{}
rec_info(State, Opts) ->
    ?OUTD("rec_info ~1000tp, ~1000tp", [id(dlgcid(State)), Opts]),
    ?EVENT_CDR:rec_info(State, Opts).

%% ==================

% when dialog's media start migrating to another mg
% State::#state_dlg{}
media_migrate(State, Opts) ->
    ?OUTD("media_migrate ~1000tp", [id(dlgcid(State))]),
    ?EVENT_CDR:media_migrate(State, Opts).

% when dialog's media finished migrate to another mg
% State::#state_dlg{}
media_migrate_done(State, Opts) ->
    ?OUTD("media_migrate_done ~1000tp", [id(dlgcid(State))]),
    ?EVENT_CDR:media_migrate_done(State, Opts).

% when dialog got reinvite
% XSide::#side{}, State::#state_dlg{}
session_change(XSide, State, Opts) ->
    ?OUTD("session_change ~1000tp, ~1000tp", [id(dlgcid(State)), side(XSide)]),
    ?EVENT_CDR:session_change(XSide, State, Opts).

% when call holded (could be double side)
% XSide::#side{}, State::#state_dlg{}
hold(XSide, State, Opts) ->
    ?OUTD("hold ~1000tp, ~1000tp", [id(dlgcid(State)), side(XSide)]),
    ?EVENT_CDR:hold(XSide, State, Opts).

% when call unholded (could be double side)
% XSide::#side{}, State::#state_dlg{}
unhold(XSide, State, Opts) ->
    ?OUTD("unhold ~1000tp, ~1000tp", [id(dlgcid(State)), side(XSide)]),
    ?EVENT_CDR:unhold(XSide, State, Opts).

% when call holded (could be double side)
% XSide::#side{}, State::#state_dlg{}
dtmf(XSide, State, Opts) ->
    ?OUTD("dtmf ~1000tp, ~1000tp", [id(dlgcid(State)), side(XSide)]),
    ?EVENT_CDR:dtmf(XSide, State, Opts).

% when call forwards refer
% Opts: #{refer_to::aor(), referred_by::aor(), referring::aor()}
refer(State, Opts) ->
    ?OUTD("refer ~1000tp", [id(dlgcid(State))]),
    ?EVENT_CDR:refer(State, Opts).

% when dialog got refer's modify_replace command (from referring dlg) (B->C <= A->B)
% REFER REPLACES EVENT (dlg 2 <- dlg 1)
% DialogIdData::{DlgId::binary(),DlgIdHash::integer()}, ACallIdData::{binary(),integer()}, State::#state_dlg{}|#state_forking{}
% ACallIdData::{ACallId::binary(),ACallIdHash::integer()}
% InviteData::{InviteId::uuid(),InviteTS::integer()}
replaces_refer({_DialogIdData,_ACallIdData,_InviteData}=Id, State, Opts) ->
    ?OUTD("replaces_refer ~1000tp, ~1000tp", [id(dlgcid(State)), id(Id)]),
    ?EVENT_CDR:replaces_refer(Id, State, Opts).

% when dialog got replacing link command (from replacing invite routing)
% REFER REPLACES EVENT (dlg 2 <- dlg 3)
% ACallIdData::{ACallId::binary(),ACallIdHash::integer()}, State::#state_dlg{}|#state_forking{}
% InviteData::{InviteId::uuid(),InviteTS::integer()}
replaces_invite({_ACallIdData,_InviteData}=Id, State, Opts) ->
    ?OUTD("replaces_invite ~1000tp, ~1000tp", [id(dlgcid(State)), id(Id)]),
    ?EVENT_CDR:replaces_invite(Id, State, Opts).

% when dialog got referring link command (from referring invite routing) (A->B <= A->C)
% REFERRING CALL EVENT (dlg 1 <- dlg 3)
% ACallIdData::{ACallId::binary(),ACallIdHash::integer()}, State::#state_dlg{}|#state_forking{}
% InviteData::{InviteId::uuid(),InviteTS::integer()}
referring_invite({_ACallIdData,_InviteData}=Id, State, Opts) ->
    ?OUTD("referring_invite ~1000tp, ~1000tp", [id(dlgcid(State)), id(Id)]),
    ?EVENT_CDR:referring_invite(Id, State, Opts).

%% ==================

% when dialog finished correctly (Reason:#{type,reason,[callid,side,operations]})
% State::#state_dlg{}|#state_forking{}
%   stopreason: #{type:atom, reason::binary(), [operations::list(), callid::binary(), sipcode::integer()]}
dlg_stop(#state_dlg{stopreason=#{}=Reason}=State, Opts) ->
    ?OUTD("dlg_stop ~1000tp, ~1000tp", [id(dlgcid(State)),Reason]),
    ?EVENT_CDR:dlg_stop(State, Opts);
dlg_stop(#state_forking{stopreason=#{}=Reason}=State, Opts) ->
    ?OUTD("dlg_stop ~1000tp, ~1000tp", [id(dlgcid(State)),Reason]),
    ?EVENT_CDR:dlg_stop(State, Opts).

% when dialog dead
% DialogIdData::{DlgId::binary(),DlgIdHash::integer()}
% ACallIdData::{ACallId::binary(),ACallIdHash::integer()}
% InviteData::{InviteId::uuid(),InviteTS::integer()}
dlg_dead({_DialogIdData,_ACallIdData,_InviteData}=Id, Opts) ->
    ?OUTD("dlg_dead ~1000tp", [id(Id)]),
    ?EVENT_CDR:dlg_dead(Id, Opts).

% when dialog binding ops
% ACallIdData::{ACallId::binary(),ACallIdHash::integer()}
% InviteData::{InviteId::uuid(),InviteTS::integer()}
% Opts#{dlgbinding:=BindingKey::binary(), dlgbindingset::boolean()}
dlg_bindings({_ACallIdData,_InviteData}=Id, Opts) ->
    ?OUTD("dlg_bindings ~1000tp, ~1000tp", [id(Id), Opts]),
    ?EVENT_CDR:dlg_bindings(Id, Opts).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% id({{ACallId,_ACallIdH},{IID,ITS}}) -> {ACallId, IID, ?EU:timestamp(ITS)};
%% id({{DialogId,_DialogIdH},{ACallId,_ACallIdH},{IID,ITS}}) -> {DialogId, ACallId, IID, ?EU:timestamp(ITS)};
%% id({ACallId,{IID,ITS}}) -> {ACallId, IID, ?EU:timestamp(ITS)};
%% id({DialogId,ACallId,{IID,ITS}}) -> {DialogId, ACallId, IID, ?EU:timestamp(ITS)}.
id({{_ACallId,_ACallIdH},{IID,_ITS}}) -> IID;
id({{_DialogId,_DialogIdH},{_ACallId,_ACallIdH},{IID,_ITS}}) -> IID;
id({_ACallId,{IID,_ITS}}) -> IID;
id({_DialogId,_ACallId,{IID,_ITS}}) -> IID;
id(IID) when is_binary(IID) -> IID.

%% dlgcid(#state_forking{dialogid=DialogId,a=#side{callid=ACallId},invite_id=IId,invite_ts=ITS}=_State) -> {DialogId, ACallId, {IId, ITS}};
%% dlgcid(#state_dlg{dialogid=DialogId,a=#side{callid=ACallId},invite_id=IId,invite_ts=ITS}=_State) -> {DialogId, ACallId, {IId, ITS}}.
dlgcid(#state_forking{invite_id=IId}=_State) -> IId;
dlgcid(#state_dlg{invite_id=IId}=_State) -> IId.

fork(#side_fork{callid=CallId,requesturi=RUri,localtag=LTag}) -> {?U:unparse_uri(?U:clear_uri(RUri)),LTag,CallId}.

side(#side{callid=CallId,remoteuri=RUri,localtag=LTag}) -> {?U:unparse_uri(?U:clear_uri(RUri)),LTag,CallId}.

update_fork_res(#side_fork{res={response,#sipmsg{class={resp,_SipCode,_SipReason}=Res}}}=Fork) -> Fork#side_fork{res=Res};
update_fork_res(Fork) -> Fork.
