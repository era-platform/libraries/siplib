%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 25.11.2021
%%% @doc

-module(r_sip_b2bua_eventing_mixerevents).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([make_mixer_event/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").

-define(OUTD(Fmt,Args), ok).

-define(MixPattern,<<"mixerevents">>).
-define(MixTaskEvent,<<"mixer_task">>).

%% ====================================================================
%% Public functions
%% ====================================================================

make_mixer_event(DlgStopEvent,{A,B}) ->
    MapOpts = maps:get(<<"data">>, element(2,DlgStopEvent)),
    case maps:get(<<"isrec">>,MapOpts,false) of
        true ->
            SrcPattern = <<"callevents">>,
            Type = common,
            EDomain = undefined,
            Event = prepare_event_for_mixer(MapOpts,SrcPattern,Type,EDomain,{A,B}),
            ?LOG('$trace', "Send mixer event: ~120tp", [Event]),
            ?ENVEVENTGATE:send_event(Event,#{sq => true});
        false -> ok
    end.

%% ====================================================================
%% Internal functions
%% Mixer event
%% ====================================================================

%% @private
prepare_event_for_mixer(MapOpts,SrcPattern,Type,EDomain,{A,B}) ->
    Pattern = {?MixPattern,Type,EDomain},
    MixEventData = prepare_data_for_mixer(MapOpts,SrcPattern,{A,B}),
    MixEvent = #{<<"class">> => ?MixPattern,
                 <<"type">>  => ?MixTaskEvent,
                 <<"eventts">>  => ?EU:timestamp(),
                 <<"data">> => MixEventData},
    From = self(),
    {Pattern,MixEvent,From}.

%% @private
prepare_data_for_mixer(MapOpts,SrcPattern,{A,B}) ->
    RecList = string:tokens(?EU:to_unicode_list(maps:get(<<"reclinks">>,MapOpts)), ";"),
    get_rec_data(RecList,MapOpts,SrcPattern,{A,B},[]).

%% @private
%% "r_mgc_3,rtx_mg_192.168.0.84_def_804,216742680,1459419256581,Srv-00A/2016-03-31/10-14;"
%%<<"path">>,<<"termid">>,<<"timestamp">>,<<"termnum">>,<<"mgnode">>
get_rec_data([],_MapOpts,_SrcPattern,_Sides,Res) -> Res;
get_rec_data([RecUnit|T],MapOpts,SrcPattern,{A,B},Res) ->
    [DialogId,ACallId,InviteTs,InviteId,CtxScrIdM,CtxScrIdD,ADomain,BDomain,AIsRec,BIsRec] =
        ?EU:maps_get([<<"dialogid">>,<<"acallid">>,<<"invitets">>,<<"cid">>,<<"ctxscridm">>,<<"ctxscridd">>,<<"adomain">>,<<"bdomain">>,<<"aisrec">>,<<"bisrec">>],MapOpts),
    Relates = maps:get(<<"relates">>,MapOpts,[]),
    ASR = asr({A,ADomain,AIsRec},{B,BDomain,BIsRec}),
    [_Mgc,MgStr,TermId,Ts,Path] = string:tokens(RecUnit, ","),
    MgIndex = string:sub_string(MgStr, string:rstr(MgStr, "def_"),erlang:length(MgStr)),
    case ?ENVCFG:get_node_by_index(MgIndex) of
        {_Site,MgNode} ->
            Res1 = Res ++ [{<<"event">>,
                               [{<<"mgnode">>,MgNode},
                                {<<"path">>,Path},
                                {<<"termid">>,TermId},
                                {<<"timestamp">>,Ts},
                                {<<"dlgid">>,DialogId},
                                {<<"acallid">>,ACallId},
                                {<<"srcpattern">>,SrcPattern},
                                {<<"invitets">>,InviteTs},
                                {<<"inviteid">>,InviteId},
                                {<<"adomain">>,ADomain},
                                {<<"bdomain">>,BDomain},
                                {<<"aisrec">>,AIsRec},
                                {<<"bisrec">>,BIsRec},
                                {<<"asr">>,ASR},
                                {<<"ctxscridm">>,CtxScrIdM},
                                {<<"ctxscridd">>,CtxScrIdD},
                                {<<"relates">>,Relates}] }];
        _Oth ->
            ?LOG("Gen Event SRV Get Mg Node ERROR:~p; MgIndex:~p",[_Oth,MgIndex]),
            Res1 = Res
    end,
    get_rec_data(T,MapOpts,SrcPattern,{A,B},Res1).

%% ------------------------------------------
%% Domains for ASR
%% ------------------------------------------
%% @private
asr({_,_,false},{_,_,false}) ->
    #{<<"domains">> => []};
asr({#side{realnumaor={_,NumA,_}}=_A,Domain,_},{#side{realnumaor={_,NumB,_}}=_B,Domain,_}) ->
    {ok,Res,_} = ?DC:find_asr_rule({Domain,'inner',NumA,NumB,<<>>}, []),
    D1 = case is_asr(Res) of
             true -> [Domain];
             false -> []
         end,
    #{<<"domains">> => D1};
asr({A,ADomain,AIsRec},{B,BDomain,BIsRec}) ->
    {_,ANumA,ANumB} = asr_search_params(a,A),
    {_,BNumA,BNumB} = asr_search_params(b,B),
    D = case AIsRec of
            true ->
                {ok,ResA,_} = ?DC:find_asr_rule({ADomain,'outgoing',ANumA,ANumB,BDomain}, []),
                case is_asr(ResA) of
                    true -> [ADomain];
                    false -> []
                end;
            false -> []
        end,
    D2 = case BIsRec of
             true ->
                 {ok,ResB,_} = ?DC:find_asr_rule({BDomain,'incoming',BNumA,BNumB,ADomain}, []),
                 case is_asr(ResB) of
                     true -> [BDomain|D];
                     false -> []
                 end;
             false -> []
         end,
    #{<<"domains">> => D2}.

%% @private
asr_search_params(a,#side{localuri=#uri{user=NumB},realnumaor={_,NumA,Dom}}) -> {Dom,NumA,NumB};
asr_search_params(b,#side{localuri=#uri{user=NumA},realnumaor={_,NumB,Dom}}) -> {Dom,NumA,NumB}.

%% @private
is_asr(false) -> false;
is_asr({ok,Rule}) ->
    ?EU:to_bool(maps:get(asr,Rule,false)).