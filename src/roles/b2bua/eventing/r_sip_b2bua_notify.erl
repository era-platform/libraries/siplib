%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Pavel Abramov
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc 26.07.2017 #289

-module(r_sip_b2bua_notify).
-author(['Pavel Abramov','Peter Bukashin <tbotc@yandex.ru>']).

%% -------------------------------------------------------------------
%%
%% -------------------------------------------------------------------

-export([add_task/1, send/1]).

%% ==========================================================================
%% Defines
%% ==========================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").

% errors
-define(E_PARAM(K), {error, <<K/binary, " not specified.">>}).
-define(E_USER_I, {error, <<"User id not found.">>}).
-define(E_USER_L, {error, <<"User login not found.">>}).
-define(E_DC_U, {error, <<"Domain centre undefined.">>}).
-define(E_DC, {error, <<"Domain centre return error.">>}).
-define(E_REG, {error, <<"No registered.">>}).

% keys
-define(K_Domain, <<"Domain">>).
-define(K_User, <<"sipUser">>).
-define(K_AdditionallyHeaders, <<"sipHeaders">>).
-define(K_ContentType, <<"sipContentType">>).
-define(K_Content, <<"sipContent">>).
-define(K_Event, <<"sipEvent">>).
-define(K_From, <<"sipFrom">>).
-define(K_Method, <<"sipMsg">>).
-define(K_Timeout, <<"Timeout">>).
-define(K_ReturnDest, <<"ReturnDest">>).

%% ====================================================================
%% API functions
%% ====================================================================
-spec add_task(Opts::map()) -> pid().
add_task(Opts) ->
    erlang:spawn(fun() -> send(Opts) end).

% ---------------------------------------------------------------------
-spec send(Opts::map()) -> ok | {error,Reason::binary()}.
send(Opts) ->
    Result = case check_mandatory_params(Opts) of
                 {error,_}=Error1 -> Error1;
                 ok ->
                     case find_contacts(Opts) of
                         {error,_}=Error2 -> Error2;
                         Contacts -> start_workers(Contacts,Opts)
                     end
             end,
    report(Result, Opts).


%% ====================================================================
%% Internal functions
%% ====================================================================
% @private
check_mandatory_params(Opts) ->
    F = fun(_,{error,_}=Error) -> Error;
           (K,_) -> case maps:get(K, Opts, false) of
                      false -> ?E_PARAM(K);
                      _ -> ok
                  end
        end,
     case lists:foldl(F, ok, [?K_Domain,?K_User,?K_Event,?K_Method]) of
        {error,_}=Error2 -> Error2;
        ok -> case maps:get(?K_Content, Opts, false) of
                  false -> ok;
                  _ -> case maps:get(?K_ContentType, Opts, false) of
                           false -> Key = ?K_ContentType, ?E_PARAM(Key);
                           _ -> ok
                       end
              end
    end.

%% ====================================================================
%% Find contacts
%% ====================================================================
% @private
-spec find_contacts(Info::map()) -> ok | {error, Reason::binary()}.
find_contacts(Info) when is_map(Info) ->
    Domain = maps:get(?K_Domain, Info),
    User = maps:get(?K_User, Info),
    case binary:split(User, <<":">>) of
        [<<"tel">>,Number] -> find_user_id(Number,Domain);
        [_,_] -> [prepare_uri(User,Info)];
        _ -> find_user_id(User,Domain)
    end.

% ---------------------------------------------------------------------
% @private
find_user_id(Key,Domain) when is_binary(Key) ->
    case ?ENVCALL:call_domaincenter_local(Domain,{find_sipuser,Key},undefined) of
        undefined -> ?E_DC_U;
        not_found -> ?E_USER_I;
        {error,_}=Error ->
            ?LOG("~p -- find_user_id: ~120p",[?MODULE,Error]),
            ?E_DC;
        {ok,SipuserId} ->
            find_reg_contacts(SipuserId,Domain)
    end.

% ---------------------------------------------------------------------
% @private
find_reg_contacts(SipuserId, Domain) when is_binary(SipuserId) ->
    find_reg_contacts([SipuserId],Domain);
find_reg_contacts(SipuserIdList, Domain) ->
    case ?ENVDC:get_object_nosticky(Domain, 'sipuser', [{ids,SipuserIdList},
                                                        {fields,[login]}], false, auto) of
        {ok,Items,_NHC} ->
            F2 = fun(LoginMap,Acc) ->
                         Login = maps:get(login, LoginMap),
                         AOR = {sip, Login, Domain},
                         RegInfo = ?REGISTRAR:get(AOR),
                         lists:append(Acc,lists:map(fun(Reg) -> {AOR,Reg} end, RegInfo))
                 end,
            case lists:foldl(F2, [], Items) of
                [] -> ?E_REG;
                Contacts -> Contacts
            end;
        _ -> ?E_USER_L
    end.

% ---------------------------------------------------------------------
% @private
prepare_uri(Uri, Opts) when is_binary(Uri) ->
    case binary:split(Uri, <<":">>) of
        [S,O] ->
            case binary:split(O, <<"@">>) of
                [U,D] -> #uri{scheme=?EU:to_atom_new(S),user=U,domain=D};
                _ -> #uri{scheme=?EU:to_atom_new(S),domain=O}
            end;
        _ ->
            case binary:split(Uri, <<"@">>) of
                [U,D] -> #uri{scheme=sip,user=U,domain=D};
                _ ->
                    D = maps:get(?K_Domain, Opts),
                    #uri{scheme=sip,user=Uri,domain=D}
            end
    end.

%% ====================================================================
%% Workers
%% ====================================================================
% @private
start_workers(Contacts,Opts) ->
    Pid = self(),
    Ref = erlang:make_ref(),
    F = fun(Contact,Acc) ->
                Fun = fun() -> send_request(Contact,Opts,Pid,Ref) end,
                R = erlang:spawn_monitor(Fun),
                lists:append(Acc,[R])
        end,
    Pids = lists:foldl(F, [], Contacts),
    case maps:get(?K_Timeout, Opts, false) of
        false -> ok;
        Timeout ->
            TimerRef = erlang:send_after(Timeout, Pid, {timeout, Ref}),
            wait_workers(TimerRef, Ref, Pids, Opts, [])
    end.

% @private
wait_workers(TimerRef,_,[],_,Results) ->
    erlang:cancel_timer(TimerRef),
    Results;
wait_workers(TimerRef,Ref,Pids,Opts,Results) ->
    receive
        {ok,Ref,WorkPid,Result} ->
            case lists:keytake(WorkPid, 1, Pids) of
                false -> wait_workers(TimerRef,Ref,Pids,Opts,Results);
                {value, {_,WorkMonitorRef}, UnPids} ->
                    erlang:demonitor(WorkMonitorRef),
                    wait_workers(TimerRef,Ref,UnPids,Opts,lists:append(Results,[Result]))
            end;
        {'DOWN', _, _Type, WorkPid, Info} ->
            case lists:keytake(WorkPid, 1, Pids) of
                false -> wait_workers(TimerRef,Ref,Pids,Opts,Results);
                {value, _, UnPids} ->
                    ?LOG("~p -- wait_workers: DOWN (~120p)",[?MODULE,Info]),
                    wait_workers(TimerRef,Ref,UnPids,Opts,Results)
            end;
        {timeout, Ref} -> {timeout,Results}
    end.

%% ====================================================================
%% Send report to ivr
%% ====================================================================
% @private
report(Result,Opts) ->
    case maps:get(?K_ReturnDest, Opts, false) of
        false -> ok;
        {Dest, {M, F, [Pid,{res,Ref,<<"sip">>}]}} ->
            Msg = case Result of ok -> {res,Ref, ok}; _ -> {res,Ref,<<"sip">>,Result} end,
            ?ENVCROSS:call_node(Dest, {M,F, [Pid,Msg]}, undefined),
            ok
    end.

%% ====================================================================
%% Prepare and send notify, message, info
%% ====================================================================
% @private
send_request({{S,U,D},RegContact},Opts,Pid,Ref) ->
    SBin = ?EU:to_binary(S),
    Uri = <<SBin/binary, $:, U/binary, $@, D/binary>>,
    Uris = ?CALLEE:prepare_routed_uris_on_reg_contacts([RegContact]),
    F = fun(UriReq,Acc) ->
                Params = update_opts(lists:append(default_opts(),[{to,Uri}]),Opts),
                Result = send_to(UriReq, Params, Opts),
                lists:append(Acc, [Result])
        end,
    Result = lists:foldl(F, [], Uris),
    send_result(Pid,Ref,{Uri,Result});
send_request(#uri{scheme=S,user=U,domain=D}=Uri,Opts,Pid,Ref) ->
    SBin = ?EU:to_binary(S),
    UriStr = <<SBin/binary, $:, U/binary, $@, D/binary>>,
    Params = update_opts(lists:append(default_opts(),[{to,Uri}]),Opts),
    Result = send_to(Uri, Params, Opts),
    send_result(Pid,Ref,{UriStr,Result}).
% @private
send_to(Uri, Params, Opts) ->
    case maps:get(?K_Method, Opts) of
        <<"notify">> ->
            nksip_uac:notify(r_sip_b2bua, Uri, Params);
        <<"info">> ->
            UnParam = case lists:keytake(subscription_state,1,Params) of false -> Params; {value,_,Unp} -> Unp end,
            nksip_uac:info(r_sip_b2bua, Uri, UnParam);
        <<"message">> ->
            UnParam = case lists:keytake(subscription_state,1,Params) of false -> Params; {value,_,Unp} -> Unp end,
            nksip_uac:message(r_sip_b2bua, Uri, UnParam)
    end.
% @private
send_result(Pid,Ref,Report) ->
    case erlang:is_process_alive(Pid) of
        false ->
            ?LOG("~p: request result: ~120p",[?MODULE,Report]),
            ok;
        true ->
            Pid ! {ok, Ref, self(), Report}
    end,
    timer:sleep(200).

% ---------------------------------------------------------------------
% @private
default_opts() ->
    [{cseq_num, 1},
     server,
     {subscription_state, {terminated,deactivated}}].
% @private
update_opts(Params1,Opts) ->
    Params2 = append_from(Params1, Opts),
    Params3 = append_event(Params2, Opts),
    additionally_opts(Opts,append_content(Params3,Opts)).
% @private
append_from(Params,Opts) ->
    case maps:get(?K_From, Opts, false) of
        false -> Params;
        From ->
            UriFrom = prepare_uri(From, Opts),
            case lists:keytake(from, 1, Params) of
                false -> lists:append(Params, [{from,UriFrom}]);
                {value,_,UnParam} -> lists:append(UnParam, [{from,UriFrom}])
            end
    end.
% @private
append_event(Params,Opts) ->
    Event = maps:get(?K_Event, Opts),
    lists:append(Params, [{event,Event}]).
% @private
append_content(Params,Opts) ->
    case maps:get(?K_Content, Opts, <<>>) of
        <<>> -> Params;
        Content ->
            ContentType = maps:get(?K_ContentType, Opts),
            Size = erlang:byte_size(Content),
            lists:append(Params, [{body,Content},
                                  {content_type,ContentType},
                                  {content_length,Size}])
    end.
% @private
additionally_opts(Opts,Acc) ->
    AddHead = maps:get(?K_AdditionallyHeaders, Opts, []),
    additionally_opts_acc(AddHead,Acc,Opts).
additionally_opts_acc([],Acc,_) -> Acc;
additionally_opts_acc([{K,V}|T],Acc,Opts) ->
    additionally_opts_acc(T,add_in_opt(K,V,Acc,Opts),Opts).
% @private
add_in_opt(K,_V,Acc,_Opts) when (K =:= <<"Route">>);(K =:= <<"route">>) -> Acc;
add_in_opt(K,V,Acc,Opts) when (K =:= <<"To">>);(K =:= <<"to">>) ->
    Uri = prepare_uri(V, Opts),
    add_in_opt(to,Uri,Acc,Opts);
add_in_opt(K,V,Acc,Opts) when is_binary(K) ->
    Kb = binary:replace(K, <<"-">>, <<"_">>, [global]),
    Kl = ?EU:to_list(Kb),
    Klo = string:to_lower(Kl),
    Ka = ?EU:to_atom_new(Klo),
    add_in_opt(Ka,V,Acc,Opts);
add_in_opt(K,V,Acc,_Opts) when is_atom(K) ->
    case lists:keytake(K, 1, Acc) of
        false -> lists:append(Acc,[{K,V}]);
        {value,_,UnAcc} -> lists:append(UnAcc,[{K,V}])
    end.
