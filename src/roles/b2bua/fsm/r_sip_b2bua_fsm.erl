%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc

-module(r_sip_b2bua_fsm).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([init/1, terminate/3, code_change/4]).
-export([callback_mode/0, handle_event/4]). % gen_statem
-export([handle_event/3, handle_sync_event/4, handle_info/3]). % gen_fsm

-export([forking/2,
         ackwaiting/2,
         reinviting/2,
         dialog/2,
         sessionchanging/2,
         refering/2,
         migrating/2,
         stopping/2]).

-export([start/1,
         supv_start_link/1,
         stop/1, stop/2,
         stop_forcely/1,
         test/1,
         pull_fsm_by_callid/1,
         pull_fsm_by_dialogid/1]).

-export([%uac_pre_response/2,
         uac_response/2,
         uas_dialog_response/2,
         sip_cancel/2,
         sip_ack/2,
         sip_bye/2,
         sip_reinvite/2,
         sip_invite_pickup/2,
         sip_refer/2,
         sip_notify/2,
         sip_info/2,
         sip_message/2,
         sip_update/2,
         call_terminate/2]).

-export([modify_refer_replaces/3,
         link_replacing_invite/2,
         link_referring_invite/2,
         get_replaced_dlg/2,
         get_replaced_opts/2,
         get_referred_opts/2]).

-export([get_current_media_link/1,
         get_current_info/1, get_current_info/2]).

-export([dialog_bindings/1]).

-export([fix_forks_on_replace/1]). % #184

-export([break_bye_transmit/3]). % RP-1331

-export([send_notify_3265/2,
         send_dtmf/2]).

-export([external_term_attach/2,
         external_term_detach/2,
         external_term_setup_topology/2]).

-export([setup_record/3,
         setup_asr/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_mgc.hrl").
-include("../include/r_sip_headers.hrl").

-define(AllowACSetKey(Self), {allow_ac_set, Self}).

%% ====================================================================
%% API functions
%% ====================================================================

start(Opts) ->
    {DialogId, D, DlgIdHash} = generate_ids(),
    Opts1 = [{dialogid,DialogId}, {dlgnum,D}, {dlgidhash,DlgIdHash} | Opts],
    ChildSpec = {DialogId, {?MODULE, supv_start_link, [Opts1]}, temporary, 1000, worker, [?MODULE]},
    ?B2BUA_SUPV:start_child(ChildSpec).

supv_start_link(Opts) ->
    start_link(?MODULE, Opts, []).

% --
stop(DlgX) ->
    stop(DlgX, "Undefined").

stop({_DlgId,Pid}, ReasonX) when is_pid(Pid) ->
    send_all_state_event(Pid, {stop_external, ReasonX});
stop(Pid, ReasonX) when is_pid(Pid) ->
    send_all_state_event(Pid, {stop_external, ReasonX});
stop(DlgId, ReasonX) when is_binary(DlgId) ->
    to_fsm(DlgId, fun(Pid) -> send_all_state_event(Pid, {stop_external, ReasonX}) end).

% --
stop_forcely({_DlgId,Pid}) when is_pid(Pid) ->
    send_all_state_event(Pid, {stop_forcely});
stop_forcely(DlgId) when is_binary(DlgId) ->
    to_fsm(DlgId, fun(Pid) -> send_all_state_event(Pid, {stop_forcely}) end).

% --
pull_fsm_by_callid(CallId) when is_binary(CallId) ->
    ?DLG_STORE:pull_dlg(CallId).

% --
pull_fsm_by_dialogid(DlgId) when is_binary(DlgId) ->
    case ?DLG_STORE:find_t(DlgId) of
        false -> false;
        {_,#{link:=Dlg}} -> Dlg
    end.

% --
test({_DlgId,Pid})
  when is_pid(Pid) ->
    send_all_state_event(Pid, {test}).

% --------------------

%% uac_pre_response({_DlgId,Pid},[Resp,_UAC,_Call]=Args)
%%   when is_pid(Pid) ->
%%     send_all_state_event(Pid, {uac_pre_response, [Resp]}),
%%     {continue,Args}.

uac_response({_DlgId,Pid},[_Resp]=Args)
  when is_pid(Pid) ->
    send_all_state_event(Pid, {uac_response, Args}),
    continue.

uas_dialog_response({_DlgId,Pid},[_Resp]=Args)
  when is_pid(Pid) ->
    send_all_state_event(Pid, {uas_dialog_response, Args}),
    continue.

sip_bye({_DlgId,Pid},Args)
  when is_pid(Pid) ->
    send_event(Pid, {sip_bye,Args}).

sip_reinvite({_DlgId,Pid},Args)
  when is_pid(Pid) ->
    send_event(Pid, {sip_reinvite,Args}).

sip_refer({_DlgId,Pid},Args)
  when is_pid(Pid) ->
    send_event(Pid, {sip_refer,Args}).

sip_cancel({_DlgId,Pid},Args)
  when is_pid(Pid) ->
    send_all_state_event(Pid, {sip_cancel,Args}).

sip_ack({_DlgId,Pid},Args)
  when is_pid(Pid) ->
    send_all_state_event(Pid, {sip_ack,Args}).

sip_invite_pickup({_DlgId,Pid},Args)
  when is_pid(Pid) ->
    send_all_state_event(Pid, {sip_invite_pickup,Args}).

sip_notify({_DlgId,Pid},Args)
  when is_pid(Pid) ->
    send_all_state_event(Pid, {sip_notify,Args}).

sip_info({_DlgId,Pid},Args)
  when is_pid(Pid) ->
    send_all_state_event(Pid, {sip_info,Args}).

sip_message({_DlgId,Pid},Args)
  when is_pid(Pid) ->
    send_all_state_event(Pid, {sip_message,Args}).

sip_update({_DlgId,Pid},Args)
  when is_pid(Pid) ->
    send_all_state_event(Pid, {sip_update,Args}).

call_terminate({_DlgId,Pid},Args)
  when is_pid(Pid) ->
    send_all_state_event(Pid, {call_terminate,Args}).

% --------------------
modify_refer_replaces({_DlgId,Pid},Args,Timeout) when is_pid(Pid) ->
    sync_send_all_state_event(Pid, {modify_refer_replaces,Args}, Timeout).

link_replacing_invite({_DlgId,Pid},Args) when is_pid(Pid) ->
    sync_send_all_state_event(Pid, {link_replacing_invite,Args}).

link_referring_invite({_DlgId,Pid},Args) when is_pid(Pid) ->
    sync_send_all_state_event(Pid, {link_referring_invite,Args}).

get_replaced_dlg({_DlgId,Pid},Args) when is_pid(Pid) ->
    sync_send_all_state_event(Pid, {get_replaced_dlg,Args}).

get_replaced_opts({_DlgId,Pid},Args) when is_pid(Pid) ->
    sync_send_all_state_event(Pid, {get_replaced_opts,Args}).

get_referred_opts({_DlgId,Pid},Args) when is_pid(Pid) ->
    sync_send_all_state_event(Pid, {get_referred_opts,Args}).

% --------------------
get_current_media_link({_DlgId,Pid}) when is_pid(Pid) ->
    sync_send_all_state_event(Pid, {get_current_media_link}).

%
get_current_info(DlgId) when is_binary(DlgId) -> get_current_info(DlgId,activesides);
get_current_info({_DlgId,Pid}=Dlg) when is_pid(Pid) -> get_current_info(Dlg,activesides).

get_current_info(DlgId,Mode) when is_binary(DlgId) ->
    case pull_fsm_by_dialogid(DlgId) of
        false -> false;
        Dlg -> get_current_info(Dlg,Mode)
    end;
get_current_info({_DlgId,Pid}, Mode) when is_pid(Pid) ->
    sync_send_all_state_event(Pid, {get_current_info, Mode}).

% --------------------
dialog_bindings({Op, CallOrDlgId, BindingKey}=Args) ->
    F = fun(Pid, {X, _, _}=Arg) when X==get;X==contains -> sync_send_all_state_event(Pid, {dialog_bindings_get, Arg});
           (Pid, Arg) -> send_all_state_event(Pid, {dialog_bindings, Arg}) end,
    case ?B2BUA_UTILS:check_callid(CallOrDlgId) of
        {dialogid, DlgId} -> dialog_bindings_by_dlgid({Op, DlgId, BindingKey}, F);
        {b2b_callid, CallIdO} -> dialog_bindings_by_callid({Op, CallIdO, BindingKey}, F);
        {callid, _} -> dialog_bindings_by_callid(Args, F)
    end.

%%  @private
dialog_bindings_by_dlgid({_Op, DlgId, _Label}=Args, F) ->
    case pull_fsm_by_dialogid(DlgId) of
        {_FDlgId, Pid} when is_pid(Pid) -> F(Pid, Args);
        _ -> {error, <<"Dialog not found">>}
    end.

%%  @private
dialog_bindings_by_callid({_Op, CallId, _Label}=Args, F) ->
    case pull_fsm_by_callid(CallId) of
        {_FCallId, Pid} when is_pid(Pid) -> F(Pid, Args);
        _ -> {error, <<"Dialog not found">>}
    end.

%% --------------------
%% #184
fix_forks_on_replace({_DlgId,Pid}) when is_pid(Pid) ->
    sync_send_all_state_event(Pid, {fix_forks_on_replace});

fix_forks_on_replace(DlgId) when is_binary(DlgId) ->
    case pull_fsm_by_dialogid(DlgId) of
        false -> false;
        Dlg -> fix_forks_on_replace(Dlg)
    end.

%% --------------------
%% RP-1331
%% Enable::boolean(), Opts::list() contains {timeout,X} for pause, {ref,Ref} for unpause
%% Returns Enable=true => {ok,Ref} | false | {false,another}
%%         Enable=false => ok | false | {false,another} | {false,undefined}
%% --------------------
break_bye_transmit(DlgId,Enable,Opts) when is_binary(DlgId) ->
    case pull_fsm_by_dialogid(DlgId) of
        false -> false;
        Dlg -> break_bye_transmit(Dlg,Enable,Opts)
    end;
break_bye_transmit({_DlgId,Pid},Enabled,Opts) when is_pid(Pid) ->
    {Self,SyncRef} = {self(),make_ref()},
    FReply = fun(Reply) -> Self ! {ok,SyncRef,Reply} end,
    Pid ! {break_bye_transmit,Enabled,[{replyfun,FReply}|Opts]},
    receive {ok,SyncRef,Reply} -> Reply
    after 5000 -> undefined
    end.

%% --------------------
%%
%% --------------------
send_notify_3265(DlgId,Opts) when is_binary(DlgId) ->
    case pull_fsm_by_dialogid(DlgId) of
        false -> false;
        Dlg -> send_notify_3265(Dlg,Opts)
    end;
send_notify_3265({_DlgId,Pid},Opts) when is_pid(Pid) ->
    {Self,SyncRef} = {self(),make_ref()},
    FReply = fun(Reply) -> Self ! {ok,SyncRef,Reply} end,
    Pid ! {send_notify_3265,[{replyfun,FReply}|Opts]},
    receive {ok,SyncRef,Reply} -> Reply
    after 5000 -> undefined
    end.

%% --------------------
%%
%% --------------------
send_dtmf(DlgId,Opts) when is_binary(DlgId) ->
    case pull_fsm_by_dialogid(DlgId) of
        false -> false;
        Dlg -> send_dtmf(Dlg,Opts)
    end;
send_dtmf({_DlgId,Pid},Opts) when is_pid(Pid) ->
    {Self,SyncRef} = {self(),make_ref()},
    FReply = fun(Reply) -> Self ! {ok,SyncRef,Reply} end,
    Pid ! {send_dtmf,Opts#{replyfun => FReply}},
    receive {ok,SyncRef,Reply} -> Reply
    after 5000 -> undefined
    end.


%% --------------------
%%
%% --------------------
-spec external_term_attach(DlgId::binary(),Opts::map())
      -> ok | {ok,LSdp::#sdp{}} | false | {error,Reason::binary()}.
%% --------------------
external_term_attach(DlgId,Opts) when is_binary(DlgId) ->
    case pull_fsm_by_dialogid(DlgId) of
        false -> false;
        Dlg -> external_term_attach(Dlg,Opts)
    end;
external_term_attach({_DlgId,Pid},Opts) when is_pid(Pid), is_map(Opts) ->
    {Self,SyncRef} = {self(),make_ref()},
    FReply = fun(Reply) -> Self ! {ok,SyncRef,Reply} end,
    Pid ! {external_term_attach,Opts#{replyfun => FReply}},
    receive {ok,SyncRef,Reply} -> Reply
    after 5000 -> false
    end.

%% -------------------
external_term_detach(DlgId,Opts) when is_binary(DlgId) ->
    case pull_fsm_by_dialogid(DlgId) of
        false -> false;
        Dlg -> external_term_detach(Dlg,Opts)
    end;
external_term_detach({_DlgId,Pid},Opts) when is_pid(Pid), is_map(Opts) ->
    {Self,SyncRef} = {self(),make_ref()},
    FReply = fun(Reply) -> Self ! {ok,SyncRef,Reply} end,
    Pid ! {external_term_detach,Opts#{replyfun => FReply}},
    receive {ok,SyncRef,Reply} -> Reply
    after 5000 -> false
    end.

%% -------------------
external_term_setup_topology(DlgId,Opts) when is_binary(DlgId) ->
    case pull_fsm_by_dialogid(DlgId) of
        false -> false;
        Dlg -> external_term_setup_topology(Dlg,Opts)
    end;
external_term_setup_topology({_DlgId,Pid},Opts) when is_pid(Pid), is_map(Opts) ->
    {Self,SyncRef} = {self(),make_ref()},
    FReply = fun(Reply) -> Self ! {ok,SyncRef,Reply} end,
    Pid ! {external_term_setup_topology,Opts#{replyfun => FReply}},
    receive {ok,SyncRef,Reply} -> Reply
    after 5000 -> false
    end.

%% --------------------
%%
%% --------------------
-spec setup_record(DlgId::binary(),Cmd::add|remove,Opts::map())
      -> ok | false | {error,Reason::binary()}.
%% --------------------
setup_record(DlgId,Cmd,Opts) when is_binary(DlgId) ->
    case pull_fsm_by_dialogid(DlgId) of
        false -> false;
        Dlg -> setup_record(Dlg,Cmd,Opts)
    end;
setup_record({_DlgId,Pid},Cmd,Opts) when is_pid(Pid), is_map(Opts) ->
    {Self,SyncRef} = {self(),make_ref()},
    FReply = fun(Reply) -> Self ! {ok,SyncRef,Reply} end,
    Pid ! {setup_record,Cmd,Opts#{replyfun => FReply}},
    receive {ok,SyncRef,Reply} -> Reply
    after 5000 -> false
    end.

%% --------------------
%%
%% --------------------
-spec setup_asr(DlgId::binary(),Cmd::add|remove,Opts::map())
      -> ok | false | {error,Reason::binary()}.
%% --------------------
setup_asr(DlgId,Cmd,Opts) when is_binary(DlgId) ->
    case pull_fsm_by_dialogid(DlgId) of
        false -> false;
        Dlg -> setup_asr(Dlg,Cmd,Opts)
    end;
setup_asr({_DlgId,Pid},Cmd,Opts) when is_pid(Pid), is_map(Opts) ->
    {Self,SyncRef} = {self(),make_ref()},
    FReply = fun(Reply) -> Self ! {ok,SyncRef,Reply} end,
    Pid ! {setup_asr,Cmd,Opts#{replyfun => FReply}},
    receive {ok,SyncRef,Reply} -> Reply
    after 5000 -> false
    end.

%% ====================================================================
%% gen_fsm => gen_statem translation functions
%% ====================================================================

% --------------------
% gen_fsm calls
% --------------------
start_link(Module,Args,Opts) ->
    ?FSMT:start_link(Module,Args,Opts).

send_event(FsmRef,Event) ->
    ?FSMT:send_event(FsmRef,Event).

%%sync_send_event(FsmRef,Event) ->
%%    ?FSMT:sync_send_event(FsmRef,Event).

send_all_state_event(FsmRef,Event) ->
    ?FSMT:send_all_state_event(FsmRef,Event).

sync_send_all_state_event(FsmRef,Event) ->
    ?FSMT:sync_send_all_state_event(FsmRef,Event).

sync_send_all_state_event(FsmRef,Event,Timeout) ->
    ?FSMT:sync_send_all_state_event(FsmRef,Event,Timeout).

% --------------------
% gen_statem callbacks
% --------------------
callback_mode() ->
    ?FSMT:callback_mode(?MODULE).

handle_event(EventType, EventContent, StateName, StateData) ->
    case catch ?FSMT:handle_event(?MODULE,EventType,EventContent,StateName,StateData) of
        {'EXIT',_}=E -> ?OUT("Error: ~120p", [E]), stop;
        T -> T
    end.

%% ====================================================================
%% Callback functions
%% ====================================================================

init(Opts) ->
    self() ! 'do_init',
    [DialogId,Req] = ?EU:extract_required_props(['dialogid','req'], Opts),
    #sipmsg{call_id=ACallId}=Req,
    ?LOGSIP("B2B fsm ~p '~p': ~120p", [DialogId,'initial',ACallId]),
    {ok, 'initial', {'initial',Opts}}.

init_internal(Opts) ->
    Self = self(),
    [App,DialogId,DlgNum,DlgIdHash,Req] = ?EU:extract_required_props(['app','dialogid','dlgnum','dlgidhash','req'], Opts),
    [IID,ITS,B2Bhdr,GhostMode] = ?EU:extract_required_props(['invite_id','invite_ts','b2bhdr','ghost_mode'], Opts),
    {value,{_,Ctx},Opts1} = lists:keytake('ctx', 1, Opts), % RP-640
    #sipmsg{call_id=ACallId}=Req,
    ACallIdHash = erlang:phash2(ACallId),
    % local store (dlg -> data, callid -> dlg)
    StoreData = #{pid => Self,
                  timestart => erlang:time(), % T
                  link => {DialogId,Self},
                  callids => [ACallId], % used only for log
                  fun_cleanup => fun() -> ok end,
                  supv => ?B2BUA_SUPV},
    % state
    StateData = #state_forking{pid=Self,
                               dialogid=DialogId,
                               dialogidhash = DlgIdHash, % #370
                               invite_id=IID, % RP-385
                               invite_ts=ITS,
                               map=#{app => App,
                                     dlgnum => DlgNum,
                                     ghost_mode => GhostMode,
                                     opts => Opts1, % RP-640
                                     init_req => Req,
                                     store_data => StoreData,
                                     b2bhdr => B2Bhdr, % ext manage by header
                                     intercom => ?EU:get_by_key('intercom',Opts,false), % feature
                                     link_referred => ?EU:get_by_key('link_referred',Opts,undefined), % #354
                                     link_replaces => ?EU:get_by_key('link_replaces',Opts,undefined), % #354
                                     last_out_callid_idx => 0,
                                     last_response => undefined,
                                     applied_redirects => [],
                                     relates => Relates = maps:get(relates,Ctx), % RP-640
                                     ctx_scrid_master => CtxScrIdM = maps:get(ctx_scrid_master,Ctx,undefined),
                                     ctx_scrid_domain => CtxScrIdD = maps:get(ctx_scrid_domain,Ctx,undefined),
                                     ctx_reqfromuri => maps:get(req_from_uri,Ctx), % RP-726
                                     dlg_bindings => maps:get(dlg_bindings, Ctx, sets:new()),
                                     ctx_esgdlg => maps:get(esgdlg, Ctx, <<>>),
                                     ctx_dir => maps:get(dir,Ctx,undefined),
                                     dtmf2send => maps:get(dtmf2send,Ctx)
                                    },
                               use_media=?EU:get_by_key('use_media',Opts,true),
                               acallid=ACallId,
                               acallidhash=ACallIdHash},
    d(StateData, 'init', "srv initing. ACallId=~120p",[ACallId]),
    % to store
    ?DLG_STORE:store_t(DialogId, StoreData, ?STORE_TIMEOUT),
    ?DLG_STORE:push_dlg(ACallId, {DialogId,Self}),
    % allow answer by codec set (not only one codec)
    [AllowACSet] = ?EU:extract_optional_props([<<"allow_ac_set">>], B2Bhdr),
    AllowACSet1 = case AllowACSet of
                      <<"0">> -> false; % if codec set is denied by invite request header option
                      _ -> ?U:is_allow_ac_set() % as it set by role option
                  end,
    AllowACSetKey = ?AllowACSetKey(self()),
    ?SIPSTORE:store_t(AllowACSetKey,AllowACSet1,?STORE_TIMEOUT),
    % change owner to context scripts
    NewOwnerInfo = {{?CFG:get_current_site(),node()},self()},
    Fchangeowner = fun(undefined) -> ok;
                      (CtxScrId) ->
                          SrvIdx = ?EU:parse_uuid_srvidx(CtxScrId),
                          Dest = ?ENVCFG:get_node_by_index(SrvIdx),
                          ?ENVCROSS:call_node(Dest, {?APP_SCRIPT, sm_change_owner, [CtxScrId, NewOwnerInfo]}, {error,<<"unavailable">>}, 2000)
                   end,
    Fchangeowner(CtxScrIdM),
    Fchangeowner(CtxScrIdD),
    % event
    EOpts = #{relates => Relates, ctx_scrid_master => CtxScrIdM, ctx_scrid_domain => CtxScrIdD, ghost_mode => GhostMode},
    ?FSM_EVENT:dlg_init({{DialogId,DlgIdHash},{ACallId,ACallIdHash},{IID,ITS}}, EOpts),
    % regular refresh at store
    FStore = fun(StateData1) ->
                     ?DLG_STORE:store_t(get_dialogid(StateData1), get_storedata(StateData1), ?STORE_TIMEOUT),
                     ?SIPSTORE:store_t(AllowACSetKey,AllowACSet1, ?STORE_TIMEOUT)
             end,
    erlang:send_after(?STORE_REFRESH, Self, {refresh_at_store, FStore}),
    % Monitor
    % we can't apriory setup clear usr states clear, because of fork-dependant domains. It would be done by state collector's monitor.
    % we can't also setup clear media, it starts later, it can migrate.
    Flog = fun(StopReason) -> ?OUT("Process ~p terminated (dlg=~p): ~120p", [Self, DlgNum, StopReason]) end,
    Fclear = fun() -> ?DLG_STORE:unlink_dlg(ACallId),
                      ?DLG_STORE:delete_t(DialogId),
                      ?SIPSTORE:delete_t(AllowACSetKey),
                      ?EVENT:dlg_dead({{DialogId,DlgIdHash},{ACallId,ACallIdHash},{IID,ITS}}, EOpts) end,
    ?MONITOR:start_monitor(Self, "Sip b2bua dlg", [Flog, Fclear]),
    % fin
    d(StateData, 'init', "srv start forking. ACallId=~120p",[ACallId]),
    StateData1 = ?FORKING:init(StateData),
    d(StateData, 'init', "srv inited. ACallId=~120p",[ACallId]),
    {ok, ?FORKING_STATE, StateData1}.

%% ----------------------------------------------------
%% handle events directly in states
%% ----------------------------------------------------

%% -------------------------
%% 'forking' events
%% -------------------------

forking({sip_bye, Args}=_Event, StateData) ->
    ?FORKING:sip_bye(Args, StateData).

%% -------------------------
%% 'ackwaiting' events
%% -------------------------

ackwaiting({sip_bye, Args}=_Event, StateData) ->
    ?ACKWAITING:sip_bye(Args, StateData);

ackwaiting({sip_reinvite, [_CallId, Req]}=_Event, StateData) ->
    send_response(?Pending("B2B. Ackwaiting"), Req, {?ACKWAITING_STATE, StateData});

ackwaiting({sip_refer, [_CallId, Req]}=_Event, StateData) ->
    send_response(?Pending("B2B. Ackwaiting"), Req, {?ACKWAITING_STATE, StateData}).

%% ------------------------
%% 'reinviting' events
%% ------------------------

reinviting({sip_bye, Args}=_Event, StateData) ->
    ?REINVITING:sip_bye(Args, StateData);

reinviting({sip_reinvite, [_CallId, Req]=_Args}=_Event, StateData) ->
    send_response(?Pending("B2B. Reinviting"), Req, {?REINVITING_STATE, StateData}); % @@ To check by implementation doc (currently missing?)

reinviting({sip_refer, [_CallId, Req]=_Args}=_Event, StateData) ->
    send_response(?Pending("B2B. Reinviting"), Req, {?REINVITING_STATE, StateData}).

%% ------------------------
%% 'dialog' events
%% ------------------------

dialog({sip_bye, Args}=_Event, StateData) ->
    ?DIALOG:sip_bye(Args, StateData);

dialog({sip_reinvite, Args}=_Event, StateData) ->
    ?SESSIONCHANGING:enter_by_reinvite(Args, StateData);

dialog({sip_refer, Args}=_Event, StateData) ->
    ?REFERING:enter_by_refer(Args, StateData).

%% ------------------------
%% 'sessionchanging' events
%% ------------------------
sessionchanging({sip_bye, Args}=_Event, StateData) ->
    ?SESSIONCHANGING:sip_bye(Args, StateData);

sessionchanging({sip_reinvite, [_CallId, Req]=Args}=_Event, StateData) ->
    StateData1 = ?SESSIONCHANGING:pending_reinvite(Args, StateData),
    send_response(?Pending("B2B. Session changing."), Req, {?SESSIONCHANGING_STATE, StateData1});

sessionchanging({sip_refer, [_CallId, Req]}=_Event, StateData) ->
    send_response(?Pending("B2B. Session changing."), Req, {?SESSIONCHANGING_STATE, StateData}).

%% ------------------------
%% 'migrating' events
%% ------------------------
migrating({sip_bye, Args}=_Event, StateData) ->
    ?MIGRATING:sip_bye(Args, StateData);

migrating({sip_reinvite, Args}=_Event, StateData) ->
    ?MIGRATING:sip_reinvite(Args, StateData);

migrating({sip_refer, [_CallId, Req]}=_Event, StateData) ->
    send_response(?Pending("B2B. Migrating"), Req, {?MIGRATING_STATE, StateData}).

%% ------------------------
%% 'refering' events
%% ------------------------
refering({sip_bye, Args}=_Event, StateData) ->
    ?REFERING:sip_bye(Args, StateData);

refering({sip_reinvite, [_CallId, _Req]=_Args}=_Event, StateData) ->
    ?REFERING:reinvite_while_transfering(_Args, StateData);

refering({sip_refer, [_CallId, _Req]=_Args}=_Event, StateData) ->
    ?REFERING:refer_while_transfering(_Args, StateData).

%% ------------------------
%% 'stopping' events
%% ------------------------
stopping({sip_bye, [_CallId, Req]}=_Event, StateData) ->
    {ok,ReqHandle} = nksip_request:get_handle(Req),
    ?U:send_sip_reply(fun() -> nksip_request:reply(accepted, ReqHandle) end),
    {next_state, ?STOPPING_STATE, StateData};

stopping({sip_reinvite, [_CallId, Req]}=_Event, StateData) ->
    {ok,ReqHandle} = nksip_request:get_handle(Req),
    ?U:send_sip_reply(fun() -> nksip_request:reply(?TemporarilyUnavailable("B2B. Stopping"), ReqHandle) end),
    {next_state, ?STOPPING_STATE, StateData};

stopping({sip_refer, [_CallId, Req]}=_Event, StateData) ->
    {ok,ReqHandle} = nksip_request:get_handle(Req),
    ?U:send_sip_reply(fun() -> nksip_request:reply(?TemporarilyUnavailable("B2B. Stopping"), ReqHandle) end),
    {next_state, ?STOPPING_STATE, StateData};

stopping(Event, StateData) ->
    E = case is_tuple(Event) of
            true -> erlang:element(1, Event);
            false -> Event
        end,
    d(StateData, ?STOPPING_STATE, "event ~120p", [E]),
    {next_state, ?STOPPING_STATE, StateData}.

%% ----------------------------------------------------
%% All states events
%% ----------------------------------------------------

%%
handle_event({sip_notify, [_CallId,Req]=Args}=_Event, StateName, StateData) ->
    case StateName of
        ?STOPPING_STATE -> send_response({forbidden}, Req, {StateName, StateData});
        ?FORKING_STATE -> send_response({forbidden}, Req, {StateName, StateData});
        _ ->
            StateData1 = ?DIALOG:sip_notify(Args, StateName, StateData),
            {next_state, StateName, StateData1}
    end;

%%
handle_event({sip_info, [_CallId,Req]=Args}=_Event, StateName, StateData) ->
    case StateName of
        ?STOPPING_STATE -> send_response({forbidden}, Req, {StateName, StateData});
        ?FORKING_STATE -> send_response({forbidden}, Req, {StateName, StateData});
        _ ->
            StateData1 = ?DIALOG:sip_info(Args, StateName, StateData),
            {next_state, StateName, StateData1}
    end;

%%
handle_event({sip_message, [_CallId,Req]=Args}=_Event, StateName, StateData) ->
    case StateName of
        ?STOPPING_STATE -> send_response({forbidden}, Req, {StateName, StateData});
        ?FORKING_STATE -> send_response({forbidden}, Req, {StateName, StateData});
        _ ->
            StateData1 = ?DIALOG:sip_message(Args, StateName, StateData),
            {next_state, StateName, StateData1}
    end;

%% RP-1367
handle_event({sip_update, [_CallId,Req]=Args}=_Event, StateName, StateData) ->
    Reason = ?EU:str("B2B. ~s",[StateName]),
    case StateName of
        ?DIALOG_STATE -> ?SESSIONCHANGING:enter_by_reinvite(Args, StateData);
        ?FORKING_STATE -> ?FORKING:sip_update(Args, StateData);
        %?FORKING_STATE -> send_response(?MethodNotAllowed(Reason), Req, {StateName, StateData});
        ?STOPPING_STATE -> send_response(?Forbidden(Reason), Req, {StateName, StateData});
        _ -> send_response(?Pending(Reason), Req, {StateName, StateData})
    end;

%%
handle_event({sip_cancel, [_CallId,_Req,_InviteReq]=Args}=_Event, ?FORKING_STATE, StateData) ->
    ?FORKING:sip_cancel(Args, StateData);
handle_event({sip_cancel, [_CallId,Req,_InviteReq]=_Args}=_Event, StateName, StateData) ->
    send_response({ok,[]}, Req, {StateName, StateData}),
    {next_state, StateName, StateData};

%%
handle_event({sip_ack, [_CallId,_Req]=Args}=_Event, ?ACKWAITING_STATE, StateData) ->
    ?ACKWAITING:sip_ack(Args, StateData);
handle_event({sip_ack, [_CallId,_Req]=_Args}=_Event, StateName, StateData) ->
    {next_state, StateName, StateData};

%%
handle_event({sip_invite_pickup, Args}=_Event, ?FORKING_STATE, StateData) ->
    ?FORKING:sip_invite_pickup(Args, StateData);
handle_event({sip_invite_pickup, [_CallId,Req|_]=_Args}=_Event, StateName, StateData) ->
    send_response(?Forbidden("Pickup could not be applied"), Req, {StateName, StateData}),
    {next_state, StateName, StateData};

%%
handle_event({uac_response, _}=Event, ?STOPPING_STATE, StateData) -> stopping(Event,StateData);
handle_event({uac_response, _}=_Event, ?ACKWAITING_STATE, StateData) -> % #346
    {next_state, ?ACKWAITING_STATE, StateData};
handle_event({uac_response, Args}=_Event, StateName, StateData) ->
    M = state_module(StateName),
    M:uac_response(Args,StateData);

%%
handle_event({uas_dialog_response, _}=Event, ?STOPPING_STATE, StateData) -> stopping(Event,StateData);
handle_event({uas_dialog_response, Args}=_Event, StateName, StateData) ->
    M = state_module(StateName),
    M:uas_dialog_response(Args,StateData);

%%
handle_event({call_terminate, _}=Event, ?STOPPING_STATE, StateData) -> stopping(Event,StateData);
handle_event({call_terminate, Args}=_Event, StateName, StateData) ->
    M = state_module(StateName),
    M:call_terminate(Args,StateData);

%%
handle_event({stop_external, _}=Event, ?STOPPING_STATE, StateData) -> stopping(Event,StateData);
handle_event({stop_external, ReasonX}=_Event, StateName, StateData) ->
    ReasonMap = ?B2BUA_UTILS:reason_external(ReasonX),
    Reason = case maps:get(callid,ReasonMap,undefined) of
                 undefined -> ReasonMap;
                 CallId ->
                     case StateData of
                         #state_dlg{a=#side{callid=CallId}} -> ReasonMap#{side => a};
                         #state_dlg{b=#side{callid=CallId}} -> ReasonMap#{side => b};
                         #state_forking{a=#side{callid=CallId}} -> ReasonMap#{side => a};
                         #state_forking{b_act=BAct} ->
                             case lists:any(fun(#side_fork{callid=C}) -> C==CallId end, BAct) of
                                 true -> ReasonMap#{side => b};
                                 false -> ReasonMap
                             end end end,
    M = state_module(StateName),
    M:stop_external(Reason,StateData);

%%
handle_event({test}=Event, ?STOPPING_STATE, StateData) -> stopping(Event,StateData);
handle_event({test}, StateName, StateData) ->
    M = state_module(StateName),
    M:test(StateData);

%%
handle_event({stop_forcely}, _StateName, StateData) ->
    {stop, normal, StateData};

%%
handle_event({dialog_bindings, {Op, _Id, BindingKey}}, StateName, StateData) ->
    StateData1 = ?B2BUA_BINDS:do_dialog_bindings(Op, BindingKey, StateData),
    {next_state, StateName, StateData1};

%%
handle_event(Event, StateName, StateData) ->
    E = case is_tuple(Event) of
            true -> erlang:element(1, Event);
            false -> Event
        end,
    d(StateData, StateName, "handle_event ~120p", [E]),
    {next_state, StateName, StateData}.

%% @private
state_module(?FORKING_STATE) -> ?FORKING;
state_module(?ACKWAITING_STATE) -> ?ACKWAITING;
state_module(?DIALOG_STATE) -> ?DIALOG;
state_module(?SESSIONCHANGING_STATE) -> ?SESSIONCHANGING;
state_module(?MIGRATING_STATE) -> ?MIGRATING;
state_module(?REFERING_STATE) -> ?REFERING;
state_module(?REINVITING_STATE) -> ?REINVITING.

% -------------------------
% 'forking' requests
% 'dialog' requests
% 'sessionchanging' requests
% 'refering' requests
% 'stopping' requests
% -------------------------

% -------------------------
% All states requests
% -------------------------

%%
handle_sync_event({modify_refer_replaces, Args}, _From, StateName, StateData) ->
    Reply = ?REPLACES:modify_refer_replaces(Args, StateData),
    {reply, Reply, StateName, StateData};

%%
handle_sync_event({link_replacing_invite, Args}, _From, StateName, StateData) ->
    case ?REPLACES:link_replacing_invite(Args, StateData) of
        {ok,Reply,StateData1} -> {reply, Reply, StateName, StateData1};
        Reply -> {reply, Reply, StateName, StateData}
    end;

%%
handle_sync_event({link_referring_invite, Args}, _From, StateName, StateData) ->
    case ?REPLACES:link_referring_invite(Args, StateData) of
        {ok,Reply,StateData1} -> {reply, Reply, StateName, StateData1};
        Reply -> {reply, Reply, StateName, StateData}
    end;

%%
handle_sync_event({get_replaced_dlg, Args}, _From, StateName, StateData) ->
    case ?REPLACES:get_replaced_dlg(Args, StateData) of
        {ok,Reply,StateData1} -> {reply, Reply, StateName, StateData1};
        Reply -> {reply, Reply, StateName, StateData}
    end;

%%
handle_sync_event({get_replaced_opts, Args}, _From, StateName, StateData) ->
    Reply = ?REPLACES:get_replaced_opts(Args, StateData),
    {reply, Reply, StateName, StateData};

%%
handle_sync_event({get_referred_opts, Args}, _From, StateName, StateData) ->
    Reply = ?REPLACES:get_referred_opts(Args, StateData),
    {reply, Reply, StateName, StateData};

%%
handle_sync_event({get_current_media_link}, _From, StateName, StateData) ->
    Reply = ?B2BUA_MEDIA:get_current_media_link(StateData),
    {reply, Reply, StateName, StateData};

%%
handle_sync_event({get_current_info, X}, _From, StateName, StateData) ->
    Reply = do_get_current_info(X, StateData),
    {reply, Reply, StateName, StateData};

%% #184
handle_sync_event({fix_forks_on_replace}, _From, ?FORKING_STATE, StateData) ->
    StateData1 = ?FORKING:fix_forks(StateData),
    {reply, ok, ?FORKING_STATE, StateData1};
handle_sync_event({fix_forks_on_replace}, _From, StateName, StateData) ->
    {reply, ok, StateName, StateData};

handle_sync_event({dialog_bindings_get, {Op, _Id, BindingKey}}, _From, StateName, StateData) ->
    {reply, ?B2BUA_BINDS:do_dialog_bindings(Op, BindingKey, StateData), StateName, StateData};

%%
handle_sync_event(Event, _From, StateName, StateData) ->
    E = case is_tuple(Event) of
            true -> erlang:element(1, Event);
            false -> Event
        end,
    d(StateData, StateName, "handle_sync_event ~120p", [E]),
    {reply, b2bua_sync_event_default, StateName, StateData}.

% -------------------------
% Process messages
% -------------------------

%%
handle_info('do_init', 'initial', {'initial',Opts}) ->
    {ok,StateName,StateData} = init_internal(Opts),
    {next_state,StateName,StateData};

%%
handle_info({refresh_at_store, FStore}, StateName, StateData) when is_function(FStore,1) ->
    d(StateData, StateName, "refresh_at_store"),
    FStore(StateData),
    erlang:send_after(?STORE_REFRESH, self(), {refresh_at_store, FStore}),
    {next_state, StateName, StateData};

%%
handle_info({start, Ref}, ?FORKING_STATE, #state_forking{ref=Ref}=StateData) ->
    ?FORKING:start(StateData);

%%
handle_info({fork_timeout, Args}, ?FORKING_STATE, StateData) ->
    ?FORKING:fork_timeout(Args, StateData);

%%
handle_info({dtmf_timeout, Args}, ?DIALOG_STATE, StateData) ->
    ?B2BUA_DTMF:dtmf_timeout(Args, ?DIALOG_STATE, StateData);

%%
handle_info({total_timeout}, ?FORKING_STATE, StateData) ->
    ?FORKING:total_timeout(StateData);

%%
handle_info({ackwaiting_timeout, Args}, ?ACKWAITING_STATE, StateData) ->
    ?LOGSIP("ackwaiting_timeout ~p", [?ACKWAITING_TIMEOUT]),
    ?ACKWAITING:ackwaiting_timeout(Args, StateData);

%%
handle_info({reinviting_timeout, Args}, ?REINVITING_STATE, StateData) ->
    ?LOGSIP("reinviting_timeout ~p", [Args]),
    ?REINVITING:reinviting_timeout(Args, StateData);

%%
handle_info({reinvite_timeout, Args}, ?SESSIONCHANGING_STATE, StateData) ->
    ?SESSIONCHANGING:reinvite_timeout(Args, StateData);

%%
handle_info({refer_timeout, Args}, ?REFERING_STATE, StateData) ->
    ?REFERING:refer_timeout(Args, StateData);

%%
handle_info({referred_dialog_timeout}, StateName, StateData) ->
    d(StateData, StateName, "referred_dialog_timeout"),
    StopReason = ?B2BUA_UTILS:reason_timeout([dialog, wait_replacing], <<>>, <<"Wait replacing timeout">>),
    ?MODULE:stop(self(), StopReason),
    {next_state, StateName, StateData};

%%
handle_info({dialog_timeout}, StateName, StateData) ->
    d(StateData, StateName, "dialog_timeout"),
    StopReason = ?B2BUA_UTILS:reason_timeout([dialog], <<>>, <<"Global timeout">>),
    ?MODULE:stop(self(), StopReason),
    {next_state, StateName, StateData};

%%
handle_info({stopping_timeout}, ?STOPPING_STATE, StateData) ->
    final(StateData),
    {stop, normal, StateData};

%%
handle_info({migration_timeout, Args}, ?MIGRATING_STATE, StateData) ->
    ?MIGRATING:migration_timeout(Args, StateData);

%% #347
handle_info({mg_dtmf, _Args}, StateName, StateData)
  when StateName==?STOPPING_STATE; StateName==?FORKING_STATE ->
    {next_state, StateName, StateData};
handle_info({mg_dtmf, Args}, StateName, StateData) ->
    ?DIALOG:mg_dtmf(Args, StateName, StateData);

%%
handle_info({mg_trunknovoice, _Args}, StateName, StateData) ->
    {next_state, StateName, StateData};

%% ----------
handle_info({mg_disconnected, Args}, StateName, StateData) ->
    %self() ! {mg_migrate_priv, Args, 0, 64},
    erlang:send_after(?EU:random(200), self(), {mg_migrate_priv, Args, 0, 64}),
    {next_state, StateName, StateData};

%% -
handle_info({mg_migrate, Args}, StateName, StateData) ->
    %self() ! {mg_migrate_priv, Args, 0, 64},
    erlang:send_after(?EU:random(200), self(), {mg_migrate_priv, Args, 0, 64}),
    {next_state, StateName, StateData};

%% -
handle_info({mg_migrate_priv,Args,I,T}, StateName, StateData) ->
    d(StateData, StateName, "MG found disconnected priv"),
    case ?B2BUA_MEDIA:media_check_mg(StateData, Args) of
        false -> ok;
        true -> case ?B2BUA_MEDIA:check_available_slots(StateData) of
                    {ok,_MgCnt,CtxCnt} when CtxCnt > 100 -> self() ! {change_mg};
                    _ -> case I > 11 of % 64 + 128 + ... + 1024 = 8 sec
                             false ->
                                 P = {mg_migrate_priv, Args, I+1, case T>=1000 of true->T;false->T*2 end},
                                 erlang:send_after(T, self(), P);
                             true ->
                                 StopReason = ?B2BUA_UTILS:reason_timeout([migrate, media, wait_available], <<>>, <<"Available slots not found for migration">>),
                                 ?MODULE:stop(self(), StopReason)
                         end end end,
    {next_state, StateName, StateData};

%% -
handle_info({change_mg}, ?FORKING_STATE, StateData) ->
    StopReason = ?B2BUA_UTILS:reason_error([forking, migrate], <<>>, <<"MG change on forking">>),
    ?FORKING:stop_external(StopReason, StateData);
handle_info({change_mg}, ?STOPPING_STATE, StateData) ->
    {next_state, ?STOPPING_STATE, StateData};
handle_info({change_mg}, ?DIALOG_STATE, StateData) ->
    ?MIGRATING:start(StateData);
handle_info({change_mg}, StateName, #state_dlg{map=#{timer_migration_ref:=undefined}=Map}=StateData) ->
    Map1 = Map#{timer_migration_ref := erlang:send_after(100, self(), {change_mg_timer})},
    {next_state, StateName, StateData#state_dlg{map=Map1}};
handle_info({change_mg}, StateName, StateData) ->
    {next_state, StateName, StateData};

%% -
handle_info({change_mg_timer}, StateName, #state_dlg{map=#{timer_migration_ref:=undefined}}=StateData) ->
    {next_state, StateName, StateData};
handle_info({change_mg_timer}, StateName, #state_dlg{map=Map}=StateData) ->
    self() ! {change_mg},
    Map1 = Map#{timer_migration_ref := undefined},
    {next_state, StateName, StateData#state_dlg{map=Map1}};

%% RP-1331
handle_info({break_bye_transmit, Enable, Opts}, StateName, #state_dlg{map=Map}=StateData) ->
    FReply = ?EU:get_by_key(replyfun,Opts,fun(_) -> ok end),
    case Enable of
        true ->
            case maps:get(break_bye_transmit,Map,undefined) of
                undefined ->
                    {Self,Ref} = {self(),make_ref()},
                    Timeout = ?EU:get_by_key(timeout,Opts,500),
                    FReply({ok,Ref}),
                    ExpTS = ?EU:timestamp()+Timeout,
                    StateData1 = StateData#state_dlg{map=Map#{break_bye_transmit => {true,Ref,ExpTS}}},
                    erlang:send_after(Timeout, Self, {break_bye_transmit,false,[{ref,Ref}]}),
                    {noreply, StateName, StateData1};
                {true,_Ref,_ExpTick} ->
                    FReply({false,another}),
                    {noreply, StateName, StateData}
            end;
        false ->
            Ref = ?EU:get_by_key(ref,Opts,undefined),
            case maps:get(break_bye_transmit,Map,undefined) of
                {true,Ref,_ExpTS} ->
                    FReply(ok),
                    StateData1 = StateData#state_dlg{map=maps:without([break_bye_transmit],Map)},
                    {noreply, StateName, StateData1};
                {true,_,_} ->
                    FReply({false,another}),
                    {noreply, StateName, StateData};
                undefined ->
                    FReply({false,undefined}),
                    {noreply, StateName, StateData}
            end
    end;

%%
handle_info({send_notify_3265, Opts}, StateName, StateData) ->
    FReply = ?EU:get_by_key(replyfun,Opts,fun(_) -> ok end),
    CallId = ?EU:get_by_key(callid,Opts,<<>>),
    Event = ?EU:get_by_key(event,Opts,<<>>),
    case StateData of
        #state_forking{b_act=B} when Event/=<<>> ->
            case lists:any(fun(#side_fork{callid=FCallId}) -> FCallId==CallId end, B) of
                true -> ?FORKING:send_notify_3265([CallId,Event,FReply,Opts], StateData);
                false -> FReply({error,not_found})
            end;
        #state_dlg{a=#side{callid=ACallId},b=#side{callid=BCallId}} when (ACallId==CallId orelse BCallId==CallId) andalso Event/=<<>> ->
            ?DIALOG:send_notify_3265([CallId,Event,FReply,Opts], StateData);
        _ ->
            FReply({error,not_found})
    end,
    {noreply, StateName, StateData};

%%
handle_info({send_dtmf,Opts}, StateName, #state_forking{}=StateData) ->
    FReply = ?EU:get_by_key(replyfun,Opts,fun(_) -> ok end),
    FReply({error,<<"Invalid state. Expected dialog">>}),
    {noreply, StateName, StateData};
handle_info({send_dtmf, Opts}, StateName, #state_dlg{}=StateData) ->
    FReply = ?EU:get_by_key(replyfun,Opts,fun(_) -> ok end),
    case ?B2BUA_DTMF:send_dtmf(Opts,StateData) of
        {ok,StateData1} ->
            FReply(ok),
            {noreply, StateName, StateData1};
        {error,_}=Err ->
            FReply(Err),
            {noreply, StateName, StateData}
    end;

%% -----------------
%% External terms
%% -----------------
handle_info({external_term_attach, Opts}, StateName, StateData) ->
    FReply = ?EU:get_by_key(replyfun,Opts,fun(_) -> ok end),
    case ?B2BUA_MEDIA:external_term_attach(Opts,StateData) of
        {error,_}=Err ->
            FReply(Err),
            {noreply, StateName, StateData};
        {Reply,StateData1} -> % ok | {ok,LSdp}
            FReply(Reply),
            {noreply, StateName, StateData1}
    end;

%% --------------
handle_info({external_term_detach, Opts}, StateName, StateData) ->
    FReply = ?EU:get_by_key(replyfun,Opts,fun(_) -> ok end),
    case ?B2BUA_MEDIA:external_term_detach(Opts,StateData) of
        {error,_}=Err ->
            FReply(Err),
            {noreply, StateName, StateData};
        {ok=Reply,StateData1} ->
            FReply(Reply),
            {noreply, StateName, StateData1}
    end;

%% --------------
handle_info({external_term_setup_topology, Opts}, StateName, StateData) ->
    FReply = ?EU:get_by_key(replyfun,Opts,fun(_) -> ok end),
    case ?B2BUA_MEDIA:external_term_setup_topology(Opts,StateData) of
        {error,_}=Err ->
            FReply(Err),
            {noreply, StateName, StateData};
        {ok=Reply,StateData1} ->
            FReply(Reply),
            {noreply, StateName, StateData1}
    end;

%% --------------
handle_info({setup_record,_,Opts}, StateName, #state_dlg{media=undefined}=StateData) ->
    FReply = ?EU:get_by_key(replyfun,Opts,fun(_) -> ok end),
    FReply({error,<<"Invalid state. No media">>}),
    {noreply, StateName, StateData};
handle_info({setup_record, Cmd, Opts}, StateName, #state_dlg{}=StateData) ->
    FReply = ?EU:get_by_key(replyfun,Opts,fun(_) -> ok end),
    {ok,Reply,StateData1} = ?B2BUA_REC:setup_record(Cmd,Opts,StateData),
    FReply(Reply),
    {noreply, StateName, StateData1};

%% --------------
handle_info({setup_asr,_,Opts}, StateName, #state_dlg{media=undefined}=StateData) ->
    FReply = ?EU:get_by_key(replyfun,Opts,fun(_) -> ok end),
    FReply({error,<<"Invalid state. No media">>}),
    {noreply, StateName, StateData};
handle_info({setup_asr, Cmd, Opts}, StateName, #state_dlg{media=_Media}=StateData) ->
    StateData2 =
        case ?B2BUA_ASR:setup_asr(Cmd,Opts,StateData) of
            {ok,Reply,StateData1} ->
                FReply = ?EU:get_by_key(replyfun,Opts,fun(_) -> ok end),
                FReply(Reply),
                StateData1;
            {ok,StateData1} -> StateData1
        end,
    {noreply, StateName, StateData2};

%% -----------------------------
handle_info(Event, StateName, StateData) ->
    E = case is_tuple(Event) of
            true -> erlang:element(1, Event);
            false -> Event
        end,
    d(StateData, StateName, "handle_info ~120p", [E]),
    {next_state, StateName, StateData}.

%% -------------------------
%%
%% -------------------------
terminate(_Reason, _StateName, _StateData) ->
    ok.

code_change(_OldVsn, _StateName, _StateData, _Extra) ->
    ok.

%% ====================================================================
%% Internal functions
%% ====================================================================

% ------
get_dialogid(#state_forking{dialogid=DialogId}=_StateData) -> DialogId;
get_dialogid(#state_dlg{dialogid=DialogId}=_StateData) -> DialogId.

% ------
get_storedata(#state_forking{map=#{store_data:=StoreData}}=_StateData) -> StoreData;
get_storedata(#state_dlg{map=#{store_data:=StoreData}}=_StateData) -> StoreData.

% ------
generate_ids() ->
    <<B6:48/bitstring, _Rest/bitstring>> = ?U:luid(),
    SrvCode = ?U:get_current_srv_textcode(),
    DialogId = <<"rDlg-", SrvCode/bitstring, "-", B6/bitstring>>,
    case ?DLG_STORE:find_t(DialogId) of
        false -> {DialogId, B6, ?U:int64_hash(<<SrvCode/binary,B6/binary>>)}; % #370
        _ -> generate_ids()
    end.

% ------
send_response(SipReply, Req, {StateName, StateData}) ->
    {ok,ReqHandle} = nksip_request:get_handle(Req),
    ?U:send_sip_reply(fun() -> nksip_request:reply(SipReply, ReqHandle) end),
    {next_state, StateName, StateData}.

% ------
to_fsm(DlgId, Fun) ->
    case ?DLG_STORE:find_t(DlgId) of
        false -> {error, not_found};
        {_,Pid} when is_pid(Pid) -> Fun(Pid);
        {_,Map} when is_map(Map) -> Fun(maps:get(pid,Map))
    end.

% ------
final(StateData) ->
    d(StateData, ?STOPPING_STATE, "final"),
    DlgId = get_dialogid(StateData),
    {ok,_} = ?B2BUA_MEDIA:stop(StateData),
    ?SIPSTORE:delete_t(?AllowACSetKey(self())), %
    ?DLG_STORE:delete_t(DlgId), % store
    ?MONITOR:stop_monitor(self()), % monitor
    ?B2BUA_CTX:stop(StateData),
    ?B2BUA_SUPV:drop_child(DlgId). % supv

% ------
do_get_current_info(activesides, State) ->
    FMap = fun(X) -> X end,
    do_get_current_info({activesides,FMap}, State);

%% RP-1297
do_get_current_info({activesides,Opts}, State) when is_list(Opts) ->
    FMap = case lists:member(unparse,Opts) of
               true -> fun(#uri{}=Uri) -> ?U:unparse_uri(Uri);
                          (X) -> X
                       end;
               false -> fun(Uri) -> Uri end
           end,
    do_get_current_info({activesides,FMap}, State);

%% ---
do_get_current_info({activesides,FMap}, #state_dlg{}=State) when is_function(FMap,1) ->
    #state_dlg{a=#side{callid=ACallId,remotetag=ARTag,localtag=ALTag,remoteuri=ARUri,localuri=ALUri},
               b=#side{callid=BCallId,remotetag=BRTag,localtag=BLTag,remoteuri=BRUri,localuri=BLUri},
               invite_id=InviteId,invite_ts=InviteTs, map=Map}=State,
    {ok,#{state => dialog,
          pid => self(),
          invite_id => InviteId,invite_ts=>InviteTs, % RP-417
          dlg_bindings => maps:get(dlg_bindings,Map,undefined), % RP-1121
          acallid=>ACallId,artag=>ARTag,altag=>ALTag,aruri=>FMap(ARUri),aluri=>FMap(ALUri),
          bcallid=>BCallId,brtag=>BRTag,bltag=>BLTag,bruri=>FMap(BRUri),bluri=>FMap(BLUri)}};

do_get_current_info({activesides,FMap}, #state_forking{b_act=BAct}=State) when is_function(FMap,1) ->
    #state_forking{a=#side{callid=ACallId,remotetag=ARTag,localtag=ALTag,remoteuri=ARUri,localuri=ALUri},
                   invite_id=InviteId,invite_ts=InviteTs,map=Map}=State,
    {ok,#{state=>forking,
          pid => self(),
          invite_id=>InviteId,invite_ts=>InviteTs, % RP-417
          acallid=>ACallId,artag=>ARTag,altag=>ALTag,aruri=>FMap(ARUri),aluri=>FMap(ALUri),
          dlg_bindings=>maps:get(dlg_bindings, Map, undefined), % RP-1121
          b=>lists:map(fun(#side_fork{callid=BCallId,remotetag=BRTag,localtag=BLTag,remoteuri=BRUri,localuri=BLUri}) ->
                               #{bcallid=>BCallId,brtag=>BRTag,bltag=>BLTag,bruri=>FMap(BRUri),bluri=>FMap(BLUri)}
                       end, BAct)}}.

% ------
d(StateData, StateName, Text) -> d(StateData, StateName, Text, []).
d(StateData, StateName, Fmt, Args) ->
    ?LOGSIP("B2B fsm ~p '~p':" ++ Fmt, [get_dialogid(StateData),StateName] ++ Args).
