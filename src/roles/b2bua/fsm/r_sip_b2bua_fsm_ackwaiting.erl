%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Anton Makarov <anton@mastermak.ru>, Peter Bukashin <tbotc@yandex.ru>
%%% @doc

-module(r_sip_b2bua_fsm_ackwaiting).
-author(['Anton Makarov <anton@mastermak.ru>','Peter Bukashin <tbotc@yandex.ru>']).

-export([start/1,
         ackwaiting_timeout/2,
         sip_bye/2,
         sip_ack/2,
         call_terminate/2,
         stop_external/2,
         uas_dialog_response/2,
         test/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_mgc.hrl").

-define(CURRSTATE, 'ackwaiting').
-define(TOTAL, 'total').
-define(RETRY, 'retry_side').
-define(KEY, 'ackwating').

%% ====================================================================
%% API functions
%% ====================================================================

% ----
start({State,Args}) ->
    d(State, "start ackwaiting Args=~p", [Args]),
    do_start({State,Args}).

% ----
test(State) ->
    d(State, "test"),
    {next_state, ?CURRSTATE, State}.

% ----
ackwaiting_timeout([?TOTAL], State) ->
    do_on_timeout(State).

% ----
sip_ack([CallId, _Req]=Args, #state_dlg{lstate=#{state:=?KEY}=LState}=State) ->
    AWCallIds = maps:get(ackwaiting_callids, LState),
    d(State, "ack, CallId=~120p", [CallId]),
    case lists:member(CallId, AWCallIds) of
        true ->
            NewAWCallIds = AWCallIds -- [CallId],
            State1 = State#state_dlg{lstate=LState#{ackwaiting_callids:=NewAWCallIds}},
            State2 = apply_ack_sdp(Args, State1),
            case length(NewAWCallIds) of
                0 -> done(State2);
                _ -> {next_state, ?CURRSTATE, State2}
            end;
        false ->
            {next_state, ?CURRSTATE, State}
    end.

% ----
sip_bye([_CallId, _Req]=Args, State) ->
    d(State, "bye, CallId=~120p", [_CallId]),
    go_abort(sip_bye, Args, State).

% ----
stop_external(Reason, State) ->
    d(State, "stop_external(~p)", [Reason]),
    go_abort(stop_external, Reason, State).

% ----
call_terminate([Reason, _Call], State) ->
    #call{call_id=CallId}=_Call,
    d(State, "call_terminate, CallId=~120p, Reason=~120p", [CallId, Reason]),
    StopReason = ?B2BUA_UTILS:reason_callterm(CallId),
    go_error(StopReason, State).

% ----
uas_dialog_response([#sipmsg{class={resp,SipCode,_}, cseq={_,'INVITE'}}=Resp], State)
  when SipCode >= 200, SipCode < 300 ->
    State1  = ?B2BUA_UTILS:handle_uas_dialog_response_2xx([Resp], State, fun d/3),
    {next_state, ?CURRSTATE, State1};
uas_dialog_response(_P, State) ->
    {next_state, ?CURRSTATE, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -----
%% start ackwaiting
%%
do_start({State,Args}) ->
    {_, SidesL} = lists:keyfind(sides, 1, Args),
    CallIds = lists:map(fun(Side) -> case Side of
                                         'a' -> #state_dlg{a=#side{callid=CallId}}=State, CallId;
                                         'b' -> #state_dlg{b=#side{callid=CallId}}=State, CallId
                                     end end, SidesL),
    LState = #{state => ?KEY,
               reinviting_sides => SidesL,
               ackwaiting_callids => CallIds},
    ?LOGSIP("ackwaiting CallIds=~p", [CallIds]),
    State1 = State#state_dlg{lstate=LState#{
                                  timestart => os:timestamp(),
                                  timer_ref => erlang:send_after(?ACKWAITING_TIMEOUT, self(), {ackwaiting_timeout, [?TOTAL]}),
                                  resp_count => 0,
                                  retry_count => 0}},
    {next_state, ?ACKWAITING_STATE, State1}.

%% =============================================================
%% Handling timeout
%% =============================================================

do_on_timeout(State) ->
    d(State, "timeout"),
    StopReason = ?B2BUA_UTILS:reason_timeout([ackwaiting], <<>>, <<"Ackwaiting timeout">>),
    go_error(StopReason, State).

%% =============================================================
%% Services
%% =============================================================

%% -----
apply_ack_sdp([CallId,Req], State) ->
    case ?U:extract_sdp(Req) of
        undefined -> State;
        #sdp{}=RSdp ->
            case ?B2BUA_MEDIA:apply_remote_ext(CallId, RSdp, State) of
                {error,R} ->
                    StopReason = ?B2BUA_UTILS:reason_error([ackwaiting, media, update_term], <<>>, R),
                    go_error(StopReason, State);
                {ok, #state_dlg{a=#side{callid=CallId}=A}=State1} -> State1#state_dlg{a=A#side{remotesdp=RSdp}};
                {ok, #state_dlg{b=#side{callid=CallId}=B}=State1} -> State1#state_dlg{b=B#side{remotesdp=RSdp}}
            end end.

%% -----
cancel_timeout_timer(#state_dlg{lstate=#{state:=?KEY}=LState}=_State) ->
    TimerRef = maps:get(timer_ref, LState),
    case TimerRef of undefined -> ok; _ -> erlang:cancel_timer(TimerRef) end;
cancel_timeout_timer(_State) -> ok.

%% -----
done(State) ->
    cancel_timeout_timer(State),
    State1 = State#state_dlg{lstate=undefined},
    %
    #state_dlg{lstate=#{state:=?KEY}=LState} = State,
    SidesL = maps:get(reinviting_sides, LState),
    Opts = [{sides, SidesL}],
    %
    next_state(State1, Opts).

%% -----
%% switch to dialog or reinviting
%% -----
next_state(State, Opts) ->
    case ?U:is_cfg_send_reinvite_on_ack() of
        true ->
            d(State, " -> switch to '~p'", [?REINVITING_STATE]),
            ?REINVITING:start(State, Opts);
        false ->
            d(State, " -> switch to '~p'", [?DIALOG_STATE]),
            ?DIALOG:start(State)
    end.

%% -----
go_error(Reason, State) ->
    go_abort(stop_error, Reason, State).

%% -----
go_abort(Fun, Arg, State) ->
    cancel_timeout_timer(State),
    State1 = State#state_dlg{lstate=undefined},
    ?DIALOG:Fun(Arg, State1).

%% -----
d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    #state_dlg{dialogid=DlgId}=State,
    ?LOGSIP("B2B fsm ~p '~p':" ++ Fmt, [DlgId,?CURRSTATE] ++ Args).
