%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Presents 'dialog' state of B2BUA Dialog FSM.
%%%      Also handle some events of dialog in other states (sip_info, sip_notify, sip_bye..)
%%%        It's most common state, sometimes switching to other states to handle some events.
%%%        Ends with bye.

-module(r_sip_b2bua_fsm_dialog).
-author('Peter Bukashin <tbotc@yandex.ru>').


-export([start/1,
         finish/1,
         stop_external/2,
         stop_error/2,
         test/1,
         sip_bye/2,
         sip_notify/3,
         sip_info/3,
         sip_message/3,
         uac_response/2,
         uas_dialog_response/2,
         call_terminate/2,
         mg_dtmf/3]).

-export([send_notify_3265/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_mgc.hrl").

-define(CURRSTATE, 'dialog').

%% ====================================================================
%% API functions
%% ====================================================================

%% ----
start(#state_dlg{}=State) ->
    Timeout = ?B2BUA_UTILS:dialog_max_time(State), % #96, #280
    State1 = State#state_dlg{timer_ref=erlang:send_after(Timeout, self(), {dialog_timeout})},
    State2 = ?REC:update_rec(State1),
    ?FSM_EVENT:dlg_start(State2),
    ?CALLSTAT:call_state(State2),
    State3 = ?B2BUA_DTMF:send_on_dlgstart(?CURRSTATE, State2),
    {next_state, ?CURRSTATE, State3}.

%% ----
finish(State) ->
    ?FSM_EVENT:dlg_stop(State),
    {ok,State1} = ?B2BUA_MEDIA:stop(State),
    d(State1, " -> switch to '~p'", [?STOPPING_STATE]),
    erlang:send_after(?STOPPING_TIMEOUT, self(), {stopping_timeout}),
    {next_state, ?STOPPING_STATE, State1}.

%% ----
stop_external(Reason, State) ->
    d(State, ":stop_external(~120p)", [Reason]),
    StopReason = ?B2BUA_UTILS:reason_external(Reason),
    finalize_on_stop(StopReason, State#state_dlg{stopreason=Reason}).

%% ----
stop_error(Reason, State) ->
    d(State, "stop_error(~120p)", [Reason]),
    StopReason = ?B2BUA_UTILS:reason_error([], <<>>, Reason),
    finalize_on_stop(StopReason, State).

%% ----
test(State) ->
    d(State, "test"),
    {next_state, ?CURRSTATE, State}.

%% ----
sip_bye([_CallId, _Req]=P, State) ->
    d(State, "sip_bye, CallId=~120p", [_CallId]),
    do_on_bye(P, State).

%% ----
sip_notify([_CallId, _Req]=P, StateName, State) ->
    d(State, StateName, "sip_notify, CallId=~120p", [_CallId]),
    do_on_notify(P, StateName, State).

%% ----
sip_info([_CallId, _Req]=P, StateName, State) ->
    d(State, StateName, "sip_info, CallId=~120p", [_CallId]),
    do_on_info(P, StateName, State).

%% ----
sip_message([_CallId, _Req]=P, StateName, State) ->
    d(State, StateName, "sip_message, CallId=~120p", [_CallId]),
    do_on_message(P, StateName, State).

%% ----
uac_response([_Resp], State) ->
    #sipmsg{class={resp,SipCode,SipReason},
            cseq={CSeq,Method},
            call_id=CallId}=_Resp,
    d(State, "uac_response ~p:~p -> ~p ~p, CallId=~120p", [Method,CSeq,SipCode,SipReason,CallId]),
    {next_state, ?CURRSTATE, State}.

%% ----
uas_dialog_response([_Resp]=P, State) ->
    #sipmsg{class={resp,SipCode,_},
            cseq={CSeq,Method},
            call_id=CallId}=_Resp,
    d(State, "uas_dialog_response ~p:~p -> ~p, CallId=~120p", [Method,CSeq,SipCode,CallId]),
    do_on_uas_dialog_response(P, State).

%% ----
call_terminate([Reason, _Call], State) ->
    #call{call_id=CallId}=_Call,
    d(State, "call_terminate, CallId=~120p, Reason=~120p", [CallId, Reason]),
    finalize_on_stop(?B2BUA_UTILS:reason_callterm(CallId), State).

%% ================================
%% #347
mg_dtmf([_CtxId, _CtxTermId, _Event]=Args, StateName, StateData) ->
    do_on_mg_dtmf(Args, StateName, StateData).

%% ================================
% -----
send_notify_3265([CallId|_]=Params,#state_dlg{a=#side{callid=CallId}=A}=StateData) ->
    send_notify_3265(A,Params,StateData);
send_notify_3265([CallId|_]=Params,#state_dlg{b=#side{callid=CallId}=B}=StateData) ->
    send_notify_3265(B,Params,StateData);
send_notify_3265([_CallId,_Event,FReply,_Opts],_StateData) ->
    FReply({error,not_found}).

%% @private
send_notify_3265(Side,[_CallId,Event,FReply,Opts],StateData) ->
    #side{dhandle=DHandle,
          localtag=LTag,
          allow_events=AllowEvents}=Side,
    case is_list(AllowEvents) andalso lists:member(Event,AllowEvents) of
        %true ->
        T when T orelse AllowEvents==[] -> % Yealink answers on replaces invite without Allow-Events
            NotifyOpts = [{event, Event},
                          {subscription_state, active},
                          no_dialog],
            NotifyOpts1 = case {?EU:get_by_key(body,Opts,<<>>), ?EU:get_by_key(contenttype,Opts,<<>>)} of
                              {<<>>,_} -> NotifyOpts;
                              {_,<<>>} -> NotifyOpts;
                              {Body,CT} ->
                                  [{content_type, CT},
                                   {body, Body}
                                   |NotifyOpts]
                          end,
            case catch nksip_uac:notify(DHandle, [async|NotifyOpts1]) of
                {'EXIT',Err} ->
                    d(StateData, "notify_fork BLTag=~p, Caught error=~120p", [LTag, Err]),
                    FReply({error,Err});
                {error,_R}=Err ->
                    d(StateData, "notify_fork BLTag=~p, Error=~120p", [LTag, Err]),
                    FReply(Err);
                {async,_ReqHandle} ->
                    d(StateData, "notify_fork BLTag=~p", [LTag]),
                    FReply(ok)
            end;
        false -> FReply({error,not_allowed})
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ============================
%% Notify
%% ============================

do_on_notify([CallId, Req], StateName, State) ->
    case State of
        #state_dlg{a=#side{callid=CallId}, b=BSide} ->
            forward_notify(Req, BSide, StateName, State);
        #state_dlg{a=ASide, b=#side{callid=CallId}} ->
            forward_notify(Req, ASide, StateName, State);
        _ ->
            State
    end.

forward_notify(#sipmsg{event={<<"refer">>,_}}=Req, YSide, ?REFERING_STATE, State) ->
    ?REFERING:forward_notify(Req, YSide, State);
forward_notify(Req, YSide, StateName, State) ->
    #sipmsg{cseq={CSeq,_},
            content_type=ContentType,
            event=Event,
            body=Body}=Req,
    d(State, StateName, "forward_notify. UNKNOWN DIALOG NOTIFY MESSAGE CSeq=~p, Event=~100p, Content-Type=~100p", [CSeq, Event, ContentType]),
    SubS = case nksip_subscription_lib:state(Req) of
               invalid -> active;
               T -> T
           end,
    Opts = [{content_type, ContentType},
            {event, Event},
            {subscription_state, SubS},
            {body, Body},
            user_agent],
    #side{dhandle=YDlgHandle}=YSide,
    catch nksip_uac:notify(YDlgHandle, [async|Opts]),
    send_response({200,[]}, Req),
    State.

%% ============================
%% Info
%% ============================

do_on_info([CallId, Req], StateName, State) ->
    case State of
        #state_dlg{a=#side{callid=CallId}=ASide, b=BSide} ->
            forward_info(Req, {ASide,BSide}, StateName, State);
        #state_dlg{a=ASide, b=#side{callid=CallId}=BSide} ->
            forward_info(Req, {BSide,ASide}, StateName, State);
        _ ->
            State
    end.

%% @private
forward_info(Req, {XSide,YSide}, StateName, State) ->
    #sipmsg{cseq={CSeq,_},
            content_type=ContentType,
            body=Body}=Req,
    d(State, StateName, "forward_info. CSeq=~p, Content-Type=~100p", [CSeq, ContentType]),
    Opts = [{content_type, ContentType},
            {body, Body},
            user_agent],
    #side{dhandle=YDlgHandle}=YSide,
    catch nksip_uac:info(YDlgHandle, [async|Opts]),
    send_response({200,[]}, Req),
    % dtmf event
    ?U:get_dtmf_from_sipinfo(Req, fun(Dtmf) -> ?FSM_EVENT:dtmf(?EU:to_binary(Dtmf),<<"info">>,XSide,State) end, fun() -> ok end),
    State.

%% ------
%% #347
do_on_mg_dtmf([_CtxId, CtxTermId, #{}=Event]=_Args, StateName, StateData) ->
    #state_dlg{media=#media{caller=ATerm, callee=BTerm}, a=ASide, b=BSide}=StateData,
    % @todo check if yterm is not support 101 telephone-event
    case {?MGC_TERM:get_term_id(ATerm), ?MGC_TERM:get_term_id(BTerm)} of
        {CtxTermId,_} -> send_info_dtmf({ASide,BSide},Event,StateName,StateData);
        {_,CtxTermId} -> send_info_dtmf({BSide,ASide},Event,StateName,StateData);
        _ -> {next_state, StateName, StateData}
    end.
%% @private
send_info_dtmf({XSide,#side{remotesdp=RSdp}=YSide},Event,StateName,StateData) ->
    % dtmf event
    Dtmf = lists:flatten(maps:get(dtmf,Event)),
    ?FSM_EVENT:dtmf(?EU:to_binary(Dtmf),<<"rfc2833">>,XSide,StateData),
    %
    case ?M_SDP:check_rfc2833(RSdp) of
        false -> send_info_dtmf_1(YSide,Event,StateName,StateData);
        true -> {next_state, StateName, StateData}
    end.
%% @private
send_info_dtmf_1(#side{dhandle=DlgHandle}=_Side,Event,StateName,StateData) ->
    DTMF = lists:map(fun(Sym) when is_integer(Sym) -> ?M_SDP:dtmf_rfc2833_to_info(Sym) end, lists:flatten(maps:get(dtmf,Event))),
    Opts = [{content_type, <<"application/dtmf-relay">>},
            {body, <<"Signal=",(?EU:to_binary(DTMF))/binary,"\r\nDuration=300">>},
            user_agent],
    catch nksip_uac:info(DlgHandle, [async|Opts]),
    {next_state, StateName, StateData}.

%% ============================
%% Message
%% ============================

do_on_message([CallId, Req], StateName, State) ->
    case State of
        #state_dlg{a=#side{callid=CallId}, b=BSide} ->
            forward_message(Req, BSide, StateName, State);
        #state_dlg{a=ASide, b=#side{callid=CallId}} ->
            forward_message(Req, ASide, StateName, State);
        _ ->
            State
    end.

forward_message(Req, YSide, StateName, State) ->
    #sipmsg{cseq={CSeq,_},
            content_type=ContentType,
            body=Body}=Req,
    d(State, StateName, "forward_message. CSeq=~p, Content-Type=~100p", [CSeq, ContentType]),
    Opts = [{content_type, ContentType},
            {body, Body},
            user_agent],
    #side{dhandle=YDlgHandle}=YSide,
    catch nksip_uac:message(YDlgHandle, [async|Opts]),
    send_response({200,[]}, Req),
    State.

%% ============================
%% Bye
%% ============================

% when dhandle is not set yet
do_on_bye(P, #state_dlg{a=#side{dhandle=ADH},b=#side{dhandle=BDH}}=State)
  when ADH==undefined; BDH==undefined ->
    #state_dlg{dialogid=DlgId}=State,
    Pid = self(),
    spawn(fun() -> timer:sleep(100),
                   ?B2BUA_DLG:sip_bye({DlgId,Pid}, P) end),
    {next_state, ?CURRSTATE, State};
% when dhandles ok
do_on_bye([CallId, Req]=P, State) ->
    case State of
        #state_dlg{a=#side{callid=CallId}=ASide, b=BSide} ->
            StopReason = ?B2BUA_UTILS:reason_bye(CallId, 'a'),
            finalize_on_bye(P, StopReason, {ASide, BSide}, State);
        #state_dlg{a=ASide, b=#side{callid=CallId}=BSide} ->
            StopReason = ?B2BUA_UTILS:reason_bye(CallId, 'b'),
            finalize_on_bye(P, StopReason, {BSide, ASide}, State);
        _ ->
            response_bye(internal_error, Req, State),
            ?DLG_STORE:unlink_dlg(CallId), % it's simple trimming
            {next_state, ?CURRSTATE, State}
    end.

% ----
finalize_on_bye([_CallId, Req], Reason, {XSide,YSide}, #state_dlg{map=Map}=State) ->
    d(State, " -> answer incoming bye ~p",[XSide#side.localtag]),
    response_bye(ok, Req, State),
    %
    case maps:get(break_bye_transmit,Map,undefined) of
        {true,_Ref,ExpTS} -> % RP-1331 breaking bye retransmit  
            spawn(fun() -> timer:sleep(erlang:max(0,ExpTS - ?EU:timestamp())), send_bye(YSide, State) end),
            ok;
        undefined -> % default
            d(State, " -> send bye to opposite ~p",[YSide#side.localtag]),
            send_bye(YSide, State)        
    end,
    cleanup_dialog(State#state_dlg{stopreason=Reason}).

%% ============================
%% Service
%% ============================

% ---
send_response(SipReply, Req) ->
    {ok,Handle} = nksip_request:get_handle(Req),
    ?U:send_sip_reply(fun() -> nksip_request:reply(SipReply, Handle) end).

% ----
finalize_on_stop(Reason, State) ->
    #state_dlg{a=ASide, b=BSide}=State,
    %
    d(State, " -> send bye to A ~p", [ASide#side.localtag]),
    send_bye(ASide, State),
    d(State, " -> send bye to B ~p", [BSide#side.localtag]),
    send_bye(BSide, State),
    %
    cleanup_dialog(State#state_dlg{stopreason=Reason}).

% ----
response_bye(SipReply, ByeReq, _State) ->
    send_response(SipReply, ByeReq).

% ----
send_bye(#side{dhandle=ZDlgHandle}, _State) ->
    Opts = [user_agent],
    catch nksip_uac:bye(ZDlgHandle, [async|Opts]).

% ----
cleanup_dialog(State) ->
    d(State, ":cleanup"),
    Now = os:timestamp(),
    #state_dlg{a=#side{callid=ACallId}=ASide,
               b=#side{callid=BCallId}=BSide}=State,
    %
    State1 = State#state_dlg{a=ASide#side{finaltime=Now},
                             b=BSide#side{finaltime=Now},
                             finaltime=Now},
    %
    State2 = free_users(State1),
    %
    ?DLG_STORE:unlink_dlg(ACallId),
    ?DLG_STORE:unlink_dlg(BCallId),
    % statistics
    return_stopping(State2).

%% @private
%%  free users on dialog stop
free_users(#state_dlg{}=State) ->
    #state_dlg{}=State1 = apply_hunt_strategy(State),
    ?CALLSTAT:call_state(free, a, State1),
    ?CALLSTAT:call_state(free, b, State1),
    State1.

%% @private
%%  RP-896 (set auto huntblock state before usr free). It's strategy
apply_hunt_strategy(#state_dlg{b=#side{sipuser=undefined}}=State) -> State;
apply_hunt_strategy(#state_dlg{map=Map}=State) ->
    Req = ?EU:get_by_key(req,maps:get(opts,Map)),
    case nksip_request:header("replaces", Req) of
        {ok,[HRepl|_]} ->
            [_CallId|Params] = binary:split(HRepl,<<";">>,[global]),
            Params1 = lists:map(fun(A) -> case binary:split(A, <<"=">>, [global]) of [K,V] -> {K,V}; [K] -> {K,undefined} end end, Params),
            case ?EU:get_by_key(<<"hunt">>, Params1, <<>>) of
                <<>> -> State;
                Code -> apply_huntblock(Code,State)
            end;
        _ -> State
    end.

%% @private
apply_huntblock(Code,#state_dlg{b=#side{sipuser=BSipUser}}=State) ->
    Domain = ?EU:get_by_key(domain,BSipUser),
    case ?ENVDC:get_object_sticky(Domain, hunt, [{keys,[Code]},{fields,[huntblock]}], auto) of
        {ok,[Obj|_],_} ->
            HB = maps:get(huntblock,Obj),
            [Val,Reason] = ?EU:maps_get([<<"expires">>,<<"reason">>],HB),
            case catch ?EU:to_int(Val) of
                TTL when is_integer(TTL), TTL > 0 ->
                    Login = ?EU:get_by_key(login,BSipUser),
                    AOR = {sip,Login,Domain},
                    Request = {puttran, [single_maxexpire, #{reserve=>'busy'}, [AOR, 'huntblock', TTL, Reason]]},
                    ?ENVCALL:cast_states_local(Domain, Request),
                    State;
                _ -> State
            end;
        _ -> State
    end.

% ----
return_stopping(State) ->
    #state_dlg{timer_ref=TimerRef}=State,
    case TimerRef of undefined -> ok; _ -> erlang:cancel_timer(TimerRef) end,
    finish(State).

%% ============================
%% UAS response, dialog ready
%% ============================

do_on_uas_dialog_response([#sipmsg{class={resp,SipCode,_}, cseq={_,'INVITE'}}=Resp], State)
  when SipCode >= 200, SipCode < 300 ->
    State1  = ?B2BUA_UTILS:handle_uas_dialog_response_2xx([Resp], State, fun d/3),
    {next_state, ?CURRSTATE, State1};
do_on_uas_dialog_response(_P, State) ->
    {next_state, ?CURRSTATE, State}.

% -----
d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    DlgId = get_dialogid(State),
    ?LOGSIP("B2B fsm ~p '~p':" ++ Fmt, [DlgId,?CURRSTATE] ++ Args).
d(State, StateName, Fmt, Args) ->
    DlgId = get_dialogid(State),
    ?LOGSIP("B2B fsm ~p '~p':" ++ Fmt, [DlgId,StateName] ++ Args).

% -----
get_dialogid(#state_dlg{dialogid=DlgId}) -> DlgId;
get_dialogid(#state_forking{dialogid=DlgId}) -> DlgId.

