%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 7.03.2018
%%% @doc Recording storage cdr info RP-369 (RP-304)

-module(r_sip_b2bua_fsm_dialog_rec).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([update_rec/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_mgc.hrl").

-define(DefaultRec, false).

%% ====================================================================
%% API functions
%% ====================================================================

%% --------------------------------
%% Generate cdr rec_info event for each domain by searching record rule
%%   Return updated state where media.is_rec could be disabled if no rec is need by any domain.
%% --------------------------------
update_rec(#state_dlg{use_media=false}=State) -> State;
update_rec(#state_dlg{media=undefined}=State) -> State;
update_rec(#state_dlg{media=#media{is_rec=false}}=State) -> State; % RP-362
update_rec(#state_dlg{a=A,b=B}=State) ->
    {ADom,ANumA,ANumB} = rec_search_params(a,State),
    {BDom,BNumA,BNumB} = rec_search_params(b,State),
    {Rec,State1} = case {ADom,BDom} of
                       {_D,_D} ->
                           {ok,Res,_} = ?DC:find_record_rule(Param = {ADom,'inner',ANumA,BNumB,<<>>}, []),
                           ABIsRec = rec_info(Res,Param,State),
                           {ABIsRec, State#state_dlg{a=A#side{is_rec=ABIsRec},
                                                     b=B#side{is_rec=ABIsRec}}};
                       _ ->
                           {ok,ResA,_} = ?DC:find_record_rule(ParamA = {ADom,'outgoing',ANumA,ANumB,BDom}, []),
                           {ok,ResB,_} = ?DC:find_record_rule(ParamB = {BDom,'incoming',BNumA,BNumB,ADom}, []),
                           AIsRec = rec_info(ResA,ParamA,State),
                           BIsRec = rec_info(ResB,ParamB,State),
                           {AIsRec or BIsRec, State#state_dlg{a=A#side{is_rec=AIsRec},
                                                              b=B#side{is_rec=BIsRec}}}
                   end,
    do_update_rec(Rec,State1).


%% ====================================================================
%% Internal functions
%% ====================================================================

%% -----------------------------
%% Return NumA, NumB for each domain (for SideA domain, for SideB domain)
%% -----------------------------
rec_search_params(a,#state_dlg{a=A}=_State) ->
    #side{localuri=#uri{user=NumB},
          realnumaor={_,NumA,Dom}}=A,
    {Dom,NumA,NumB};
%%
rec_search_params(b,#state_dlg{b=B}=_State) ->
    #side{localuri=#uri{user=NumA},
          realnumaor={_,NumB,Dom}}=B,
    {Dom,NumA,NumB}.

%% -----------------------------
%% Parse DC result of recordrule search, return true if rec is need, false otherwise.
%%   by the way generate cdr event rec_info for each domain.
%% -----------------------------
rec_info(false,P,State) ->
    NullRule = #{id=>null,storageruleid=>null},
    do_rec_info(?DefaultRec,NullRule,P,State),
    ?DefaultRec;
rec_info({ok,Rule},P,State) ->
    case Rec = ?EU:to_bool(maps:get(rec,Rule)) of
        true -> do_rec_info(true,Rule,P,State);
        false -> do_rec_info(false,Rule,P,State)
    end,
    Rec.

%% -----------------------------
%% Generate cdr event rec_info
%% -----------------------------
%do_rec_info(false,_Rule,_P,_State) -> ok;
do_rec_info(Rec, Rule,{Dom,Dir,NumA,NumB,CrossDom},State) ->
    Opts = #{domain => Dom,
             dir => Dir,
             from_num => NumA,
             to_num => NumB,
             crossdomain => CrossDom,
             rec => Rec,
             recordruleid => maps:get(id,Rule),
             storageruleid => maps:get(storageruleid,Rule)},
    ?FSM_EVENT:rec_info(State,Opts).

%% -----------------------------
%% disable rec options in state if no rec is need
%% -----------------------------
do_update_rec(true,State) -> State;
do_update_rec(false,#state_dlg{media=Media}=State) ->
    State#state_dlg{media=Media#media{is_rec=false,rec_links=[]}}.

