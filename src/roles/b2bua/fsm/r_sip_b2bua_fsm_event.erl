%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 26.05.2018
%%% @doc FSM (init,forking,dialog) CDR eventing funs
%%%         RP-640

-module(r_sip_b2bua_fsm_event).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([dlg_init/2,
         dlg_dead/2]).

-export([dlg_call/1,
         dlg_pickup/2,
         fork_start/2,
         fork_cancel/2,
         fork_stop/2,
         fork_preanswer/3,
         fork_answer/3,
         fork_redirect/3,
         user_redirect/2]).

-export([dlg_start/1,
         dlg_stop/1,
         rec_info/2,
         session_change/2,
         hold/2,
         unhold/2,
         refer/2,
         media_migrate/1,
         media_migrate_done/1,
         dtmf/4]).

-export([replaces_refer/2,
         replaces_invite/2,
         referring_invite/2]).

-export([dlg_bindings/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ========================================
%% FSM initial events
%% ========================================

%% --------------------------
dlg_init(Id, EOpts) ->
    ?EVENT:dlg_init(Id, EOpts).

%% --------------------------
dlg_dead(Id, EOpts) ->
    ?EVENT:dlg_dead(Id, EOpts).

%% ========================================
%% Forking events
%% ========================================

%% --------------------------
dlg_call(#state_forking{a=A}=State) ->
    #side{localuri=ALUri,
          remoteuri=ARUri,
          remotecontacts=[AContact|_],
          sipuser=AUser,
          opts=AOpts}=A,
    Fouter = fun(Opts) ->
                     case ?EU:get_by_key(extaccount,Opts,false) of
                         false -> {false, null};
                         #{}=AMap -> {true, maps:get(code,AMap)}
                     end end,
    Fnumber = fun(undefined,Uri) -> Uri#uri.user;
                   ([_|_]=SipUser,_) ->
                      case ?EU:get_by_key(extension,SipUser,undefined) of
                          undefined -> ?EU:get_by_key(phone,SipUser,<<>>);
                          Ext -> Ext
                      end end,
    {AOuter,AProviderCode} = Fouter(AOpts),
    Opts = #{ausername => ARUri#uri.user,
             anumber => Fnumber(AUser,ARUri),
             adomain => ARUri#uri.domain,
             anetworkaddr => AContact#uri.domain,
             acallednum => ALUri#uri.user,
             aouter => AOuter,
             aprovidercode => AProviderCode},
    EOpts = Opts#{relates => relates(State),
                  ctx_scrid_master => ctx_scrid_master(State),
                  ctx_scrid_domain => ctx_scrid_domain(State)},
    ?EVENT:dlg_call(State, EOpts).

%% --------------------------
dlg_pickup(Fork, State) ->
    EOpts = #{forknumber => element(2,Fork#side_fork.realnumaor),
              relates => relates(Fork,State),
              ctx_scrid_master => ctx_scrid_master(State),
              ctx_scrid_domain => ctx_scrid_domain(State)},
    ?EVENT:dlg_pickup(Fork, State, EOpts).

%% --------------------------
fork_start(Fork, State) ->
    #side_fork{opts=ForkOpts,
               callednum={_,UserName,Domain},
               requesturi=RUri}=Fork,
    {Outer,ProviderCode} = case ?EU:get_by_key(extaccount,ForkOpts,undefined) of
                               undefined -> {false,null};
                               #{}=Map -> {true,maps:get(code,Map)}
                           end,
    NetworkAddr = case Outer of
                      true -> null;
                      false -> RUri#uri.domain
                  end,
    EOpts = #{busername => UserName,
              bdomain => Domain,
              bnetworkaddr => NetworkAddr,
              bouter => Outer,
              bprovidercode => ProviderCode,
              relates => relates(Fork,State),
              ctx_scrid_master => ctx_scrid_master(State),
              ctx_scrid_domain => ctx_scrid_domain(State)},
    ?EVENT:fork_start(Fork, State, EOpts).

%% --------------------------
fork_cancel(Fork, State) ->
    EOpts = #{relates => relates(Fork,State),
              ctx_scrid_master => ctx_scrid_master(State),
              ctx_scrid_domain => ctx_scrid_domain(State)},
    ?EVENT:fork_cancel(Fork, State, EOpts).

%% --------------------------
fork_stop(Fork, State) ->
    EOpts = #{relates => relates(Fork,State),
              ctx_scrid_master => ctx_scrid_master(State),
              ctx_scrid_domain => ctx_scrid_domain(State)},
    ?EVENT:fork_stop(Fork, State, EOpts).

%% --------------------------
fork_preanswer(Fork, State, EOpts) ->
    EOpts1 = EOpts#{relates => relates(Fork,State),
                    ctx_scrid_master => ctx_scrid_master(State),
                    ctx_scrid_domain => ctx_scrid_domain(State)},
    ?EVENT:fork_preanswer(Fork, State, EOpts1).

%% --------------------------
fork_answer(Fork, State, EOpts) ->
    EOpts1 = EOpts#{relates => relates(Fork,State),
                    ctx_scrid_master => ctx_scrid_master(State),
                    ctx_scrid_domain => ctx_scrid_domain(State)},
    ?EVENT:fork_answer(Fork, State, EOpts1).

%% --------------------------
fork_redirect(Fork, State, EOpts) ->
    EOpts1 = EOpts#{relates => relates(Fork,State),
                    ctx_scrid_master => ctx_scrid_master(State),
                    ctx_scrid_domain => ctx_scrid_domain(State)},
    ?EVENT:fork_redirect(Fork, State, EOpts1).

%% --------------------------
user_redirect(State, EOpts) ->
    BAOR = maps:get(owner,EOpts),
    EOpts1 = EOpts#{relates => relates(BAOR,State),
                    ctx_scrid_master => ctx_scrid_master(State),
                    ctx_scrid_domain => ctx_scrid_domain(State)},
    ?EVENT:user_redirect(State, EOpts1).

%% ========================================
%% Dialog events
%% ========================================

%% --------------------------
dlg_start(#state_dlg{a=A,b=B}=State) ->
    #side{localuri=ALUri,
          remoteuri=ARUri,
          remotecontacts=[AContact|_],
          last_remote_party=ARemoteParty,
          sipuser=AUser,
          opts=AOpts}=A,
    #side{localuri=BLUri,
          remoteuri=BRUri,
          remotecontacts=[BContact|_],
          sipuser=BUser,
          opts=BOpts}=B,
    Fouter = fun(Opts) ->
                     case ?EU:get_by_key(extaccount,Opts,false) of
                         false -> {false, null};
                         #{}=AMap -> {true, maps:get(code,AMap)}
                     end end,
    Fnumber = fun(undefined,Uri) -> Uri#uri.user;
                   ([_|_]=SipUser,_) ->
                      case ?EU:get_by_key(extension,SipUser,undefined) of
                          undefined -> ?EU:get_by_key(phone,SipUser,<<>>);
                          Ext -> Ext
                      end end,
    {AOuter,AProviderCode} = Fouter(AOpts),
    {BOuter,BProviderCode} = Fouter(BOpts),
    Opts = #{ausername => ARUri#uri.user,
             anumber => Fnumber(AUser,ARUri),
             adomain => ARUri#uri.domain,
             adisplayname => ?EU:ensure_unquoted(BLUri#uri.disp),
             anetworkaddr => AContact#uri.domain,
             acallednum => ALUri#uri.user,
             arepresentative => BLUri#uri.user,
             aouter => AOuter,
             aprovidercode => AProviderCode,
             busername => BRUri#uri.user,
             bnumber => Fnumber(BUser,BRUri),
             bdomain => BRUri#uri.domain,
             bdisplayname => ?EU:ensure_unquoted(ARemoteParty#uri.disp),
             bnetworkaddr => BContact#uri.domain,
             bouter => BOuter,
             bprovidercode => BProviderCode},
    EOpts = Opts#{relates => relates(State),
                  ctx_scrid_master => ctx_scrid_master(State),
                  ctx_scrid_domain => ctx_scrid_domain(State)},
    ?EVENT:dlg_start(State, EOpts).

%% --------------------------
dlg_stop(#state_dlg{a=A,b=B}=State) ->
    #side{remoteuri=ARUri}=A,
    #side{remoteuri=BRUri}=B,
    Opts = #{adomain => ARUri#uri.domain,
             bdomain => BRUri#uri.domain},
    EOpts = Opts#{relates => relates(State),
                  ctx_scrid_master => ctx_scrid_master(State),
                  ctx_scrid_domain => ctx_scrid_domain(State)},
    ?EVENT:dlg_stop(State, EOpts);
dlg_stop(#state_forking{a=A,b=_B}=State) ->
    #side{remoteuri=ARUri}=A,
    Opts = #{adomain => ARUri#uri.domain,
             bdomain => ARUri#uri.domain},
    EOpts = Opts#{relates => relates(State),
                  ctx_scrid_master => ctx_scrid_master(State),
                  ctx_scrid_domain => ctx_scrid_domain(State)},
    ?EVENT:dlg_stop(State, EOpts).

%% --------------------------
rec_info(State, EOpts) ->
    EOpts1 = EOpts#{relates => relates(State),
                    ctx_scrid_master => ctx_scrid_master(State),
                    ctx_scrid_domain => ctx_scrid_domain(State)},
    ?EVENT:rec_info(State, EOpts1).

%% --------------------------
session_change(Side, State) ->
    EOpts = #{relates => relates(State),
              ctx_scrid_master => ctx_scrid_master(State),
              ctx_scrid_domain => ctx_scrid_domain(State)},
    ?EVENT:session_change(Side, State, EOpts).

%% --------------------------
hold(Side, State) ->
    EOpts = #{relates => relates(State),
              ctx_scrid_master => ctx_scrid_master(State),
              ctx_scrid_domain => ctx_scrid_domain(State)},
    ?EVENT:hold(Side, State, EOpts).

%% --------------------------
unhold(Side, State) ->
    EOpts = #{relates => relates(State),
              ctx_scrid_master => ctx_scrid_master(State),
              ctx_scrid_domain => ctx_scrid_domain(State)},
    ?EVENT:unhold(Side, State, EOpts).

%% --------------------------
refer(State, EOpts) ->
    EOpts1 = EOpts#{relates => relates(State),
                    ctx_scrid_master => ctx_scrid_master(State),
                    ctx_scrid_domain => ctx_scrid_domain(State)},
    ?EVENT:refer(State, EOpts1).

%% --------------------------
media_migrate(State) ->
    EOpts = #{relates => relates(State),
              ctx_scrid_master => ctx_scrid_master(State),
              ctx_scrid_domain => ctx_scrid_domain(State)},
    ?EVENT:media_migrate(State, EOpts).

%% --------------------------
media_migrate_done(State) ->
    EOpts = #{relates => relates(State),
              ctx_scrid_master => ctx_scrid_master(State),
              ctx_scrid_domain => ctx_scrid_domain(State)},
    ?EVENT:media_migrate_done(State, EOpts).

%% --------------------------
dtmf(Dtmf,Proto,Side,State) ->
    EOpts = #{relates => relates(State),
              ctx_scrid_master => ctx_scrid_master(State),
              ctx_scrid_domain => ctx_scrid_domain(State),
              dtmf => ?EU:to_binary(Dtmf),
              proto => Proto},
    ?EVENT:dtmf(Side, State, EOpts).

%% ========================================
%% Common events
%% ========================================

%% --------------------------
replaces_refer(Owner, State) ->
    EOpts = #{relates => relates(State),
              ctx_scrid_master => ctx_scrid_master(State),
              ctx_scrid_domain => ctx_scrid_domain(State)},
    ?EVENT:replaces_refer(Owner, State, EOpts).

%% --------------------------
replaces_invite(Owner, State) ->
    EOpts = #{relates => relates(State),
              ctx_scrid_master => ctx_scrid_master(State),
              ctx_scrid_domain => ctx_scrid_domain(State)},
    ?EVENT:replaces_invite(Owner, State, EOpts).

%% --------------------------
referring_invite(Owner, State) ->
    EOpts = #{relates => relates(State),
              ctx_scrid_master => ctx_scrid_master(State),
              ctx_scrid_domain => ctx_scrid_domain(State)},
    ?EVENT:referring_invite(Owner, State, EOpts).

%% --------------------------
%% added/removed dlg_binding event
dlg_bindings(State, Opts) ->
    [ACallId,ACallIdH,IID,ITS] = extract_ids(State),
    Opts1 = Opts#{relates => relates(State),
                  ctx_scrid_master => ctx_scrid_master(State),
                  ctx_scrid_domain => ctx_scrid_domain(State)},
    ?EVENT:dlg_bindings({{ACallId,ACallIdH},{IID,ITS}}, Opts1).


%% ====================================================================
%% Internal functions
%% ====================================================================

%% ----------------------------
%% RP-640
%% Returns list of relates for event purposes
%%   (ex. [{<<"test.rootdomain.ru",{sipuser,<<"sip1">>}},{<<"test.rootdomain.ru">>,{other,<<"7428913">>}}])
%% ----------------------------
%% when dialog
relates(#state_dlg{map=Map}=_State) ->
    maps:get(relates,Map);
%% when forking
relates(#state_forking{map=Map}=_State) ->
    maps:get(relates,Map);
%% when initial
relates(#{}=Ctx) ->
    maps:get(relates,Ctx,[]).
%% when forking + fork
relates(#side_fork{}=Fork,#state_forking{}=State) ->
    [relate_fork(Fork) | relates(State)];
%% when forking + aor
relates({sip,BU,BD},#state_forking{}=State) ->
    [{BD,{sipuser,BU}} | relates(State)].

%% @private
relate_fork(#side_fork{remoteuri=#uri{user=N,domain=D}}=B) ->
    case B#side_fork.sipuser of
        undefined -> {D,{other,N}};
        SU -> {D,{sipuser,?EU:get_by_key(login,SU,<<>>)}}
    end.

%% ----------------------------
%% RP-682
%% ----------------------------
ctx_scrid_master(#state_dlg{map=Map}) ->
    maps:get(ctx_scrid_master,Map);
ctx_scrid_master(#state_forking{map=Map}) ->
    maps:get(ctx_scrid_master,Map);
%% when initial
ctx_scrid_master(#{}=Ctx) ->
    maps:get(ctx_scrid_master,Ctx,undefined).

%%
ctx_scrid_domain(#state_dlg{map=Map}) ->
    maps:get(ctx_scrid_domain,Map);
ctx_scrid_domain(#state_forking{map=Map}) ->
    maps:get(ctx_scrid_domain,Map);
%% when initial
ctx_scrid_domain(#{}=Ctx) ->
    maps:get(ctx_scrid_domain,Ctx,undefined).

%% ----------------------------
%% RP-1157
%% ----------------------------
extract_ids(#state_forking{acallid=ACallId,acallidhash=ACallIdHash,invite_id=IID,invite_ts=ITS}=_StateData) ->
    [ACallId,ACallIdHash,IID,ITS];
extract_ids(#state_dlg{acallid=ACallId,acallidhash=ACallIdHash,invite_id=IID,invite_ts=ITS}=_StateData) ->
    [ACallId,ACallIdHash,IID,ITS];
extract_ids(Ctx) when is_map(Ctx) ->
    [_ACallId,_ACallIdHash,_IID,_ITS] = ?EU:maps_get([acallid,acallidhash,invite_id,invite_ts],Ctx).
