%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Presents 'forking' state of B2BUA Dialog FSM
%%%        Acts on start to make forward calls to selected uris.
%%%        Ends with final 2xx answer or timeout-4xx-5xx-6xx answers from all forks.

-module(r_sip_b2bua_fsm_forking).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([init/1,
         start/1,
         stop_external/2,
         test/1,
         %
         sip_cancel/2,
         sip_bye/2,
         sip_update/2,
         sip_invite_pickup/2,
         %
         fork_timeout/2,
         total_timeout/1,
         uac_response/2,
         uas_dialog_response/2,
         call_terminate/2,
         %
         forking_internal_prepare_calls/2,
         forking_internal_invite_prepared/2,
         %
         fix_forks/1,
         %
         send_notify_3265/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_mgc.hrl").
-include("../include/r_sip_headers.hrl").

-define(CURRSTATE, 'forking').

%% ====================================================================
%% API functions
%% ====================================================================

init(State) ->
    do_init(State).

start(State) ->
    do_start(State).

% ----
stop_external(Reason, State) ->
    d(State, "stop_external(~p)", [Reason]),
    ?FORKING_UTILS:do_final(?Decline(<<"B2B.F. Call aborted">>), State#state_forking{stopreason=Reason}).

% ----
test(State) ->
    d(State, "test"),
    {next_state, ?CURRSTATE, State}.

% ----
sip_cancel([CallId, _Req, _InviteReq], State) ->
    d(State, "cancel, CallId=~120p", [CallId]),
    StopReason = ?B2BUA_UTILS:reason_cancel(CallId),
    ?FORKING_UTILS:do_final(State#state_forking{stopreason=StopReason}).

% ----
sip_bye([CallId, Req], State) ->
    d(State, "forking bye, CallId=~120p", [CallId]),
    {ok,Handle} = nksip_request:get_handle(Req),
    ?U:send_sip_reply(fun() -> nksip_request:reply(ok, Handle) end),
    %
    case State of
        #state_forking{a=#side{callid=CallId}} ->
            StopReason = ?B2BUA_UTILS:reason_bye(CallId, 'a'),
            ?FORKING_UTILS:do_final(State#state_forking{stopreason=StopReason});
        #state_forking{b_act=BAct} ->
            case lists:keyfind(CallId,#side_fork.callid,BAct) of
                #side_fork{rhandle=ReqHandle,cmnopts=CmnOpts}=Fork ->
                    % This case is when replacing invite acts on fork, before 2xx answered (yealink)
                    %  Send cancel, to block invite leg timer C
                    %   Cancel brings 487 reponse, but we are stopping fork right now
                    catch nksip_uac:cancel(ReqHandle, [async|CmnOpts]),
                    stop_fork(Fork, {bye}, State);
                false -> {next_state, ?CURRSTATE, State}
            end end.

% ----
% RP-1367
% ----
sip_update([CallId, Req], State) ->
    d(State, "forking update, CallId=~120p", [CallId]),
    {ok,Handle} = nksip_request:get_handle(Req),
    case State of
        #state_forking{a=#side{callid=CallId}} ->
            nksip_request:reply(?InternalErrorEx("B2B. fork update unsupported",[{replace,{<<"Retry-After">>,10}}]), Handle);
        #state_forking{b_act=BAct} ->
            case lists:keyfind(CallId,#side_fork.callid,BAct) of
                #side_fork{rhandle=_ReqHandle,cmnopts=_CmnOpts}=_Fork ->
                    nksip_request:reply(?InternalErrorEx("B2B. fork update unsupported",[{replace,{<<"Retry-After">>,10}}]), Handle);
                false ->
                    nksip_request:reply(?InternalError("B2B. fork not found"), Handle)
            end end,
    {next_state, ?CURRSTATE, State}.

% ----
sip_invite_pickup([_CallId|_]=Args, State) ->
    d(State, "pickup, CallId=~120p", [_CallId]),
    ?FORKING_PICKUP:start_pickup(Args, State).

% ----
fork_timeout([CallId, BLTag]=P, State) ->
    d(State, "fork_timeout BLTag=~120p, CallId=~120p", [BLTag, CallId]),
    do_on_uac_timeout(P, State).

% ----
total_timeout(State) ->
    d(State, "total_timeout"),
    StopReason = ?B2BUA_UTILS:reason_timeout([calling], <<>>, <<"Calling timeout">>),
    ?FORKING_UTILS:do_final(?RequestTimeout("B2B.F. Call timeout"), State#state_forking{stopreason=StopReason}).

% ----
uac_response([_Resp]=Args, State) ->
    #sipmsg{class={resp,SipCode,SipReason},
            cseq={CSeq,Method},
            call_id=CallId}=_Resp,
    d(State, "uac_response ~p:~p -> ~p ~p, CallId=~120p", [Method,CSeq,SipCode,SipReason,CallId]),
    do_on_uac_response(Args, State).

% ----
uas_dialog_response([_Resp], State) ->
    #sipmsg{class={resp,SipCode,_},
            cseq={CSeq,Method},
            call_id=CallId}=_Resp,
    d(State, "uas_dialog_response ~p:~p -> ~p, CallId=~120p", [Method,CSeq,SipCode,CallId]),
    {next_state, ?CURRSTATE, State}.

% ----
call_terminate([Reason, Call], State) ->
    #call{call_id=CallId}=Call,
    case State of
        #state_forking{a=#side{callid=CallId}} ->
            d(State, "call_terminate, CallId=~120p, Reason=~120p", [CallId, Reason]),
            ?FORKING_UTILS:do_final(?InternalError("B2B.F. Call terminated"), State#state_forking{stopreason=?B2BUA_UTILS:reason_callterm(CallId)});
        _ ->
            {next_state, ?CURRSTATE, State}
    end.

%% =================

% #184
% when replacing in forking state.
%   it should filter all prepared forks, not tagged active forks, and should skip other redirects.
fix_forks(#state_forking{}=State) ->
    Now = os:timestamp(),
    #state_forking{b_act=BAct,b_fin=BFin}=State,
    StopForks = lists:foldl(fun(#side_fork{remotetag=undefined}=Fork,Acc) -> [Fork|Acc];
                               (_,Acc) -> Acc
                            end, [], BAct),
    BAct1 = lists:filter(fun(#side_fork{callid=CID}) -> not lists:keymember(CID, #side_fork.callid, StopForks) end, BAct),
    ?FORKING_UTILS:cancel_active_forks(StopForks,[],State),
    BCanc = [Fork#side_fork{res={cancelled},
                            finaltime=Now,
                            rule_timer_ref=undefined,
                            resp_timer_ref=undefined} || Fork <- StopForks],
    BFin1 = BFin++BCanc,
    State#state_forking{b_act=BAct1,
                        b_fin=BFin1,
                        b_prep=[],
                        b_rule=[],
                        fixed_forks=true}.

% -----
forking_internal_prepare_calls(Rules, State) ->
    prepare_calls(Rules, State).

% -----
forking_internal_invite_prepared(ForkInfos, State) ->
    invite_prepared(ForkInfos, State).

% -----
send_notify_3265([CallId,Event,FReply,Opts],#state_forking{map=#{app:=App},b_act=BAct}=StateData) ->
    case lists:keyfind(CallId, #side_fork.callid, BAct) of
        #side_fork{requesturi=Uri,
                   localtag=BLTag,
                   remotetag=BRTag,
                   cmnopts=CmnOpts,
                   allow_events=AllowEvents} ->
            case is_list(AllowEvents) andalso lists:member(Event,AllowEvents) of
                %true ->
                T when T orelse AllowEvents==[] -> % Yealink answers on replaces invite without Allow-Events
                    {value,{to,BRToUri},CmnOpts1} = lists:keytake(to,1,CmnOpts),
                    BRTag1 = BRTag,
                    BRToUri1 = BRToUri#uri{ext_opts=[{<<"tag">>,BRTag1}]},
                    NotifyOpts = [{cseq_num,2},
                                  {to,BRToUri1},
                                  {event, Event},
                                  {subscription_state, active},
                                  no_dialog],
                    NotifyOpts1 = case {?EU:get_by_key(body,Opts,<<>>), ?EU:get_by_key(contenttype,Opts,<<>>)} of
                              {<<>>,_} -> NotifyOpts;
                              {_,<<>>} -> NotifyOpts;
                              {Body,CT} ->
                                  [{content_type, CT},
                                   {body, Body}
                                   |NotifyOpts]
                          end,
                    case catch nksip_uac:notify(App, Uri, [async|CmnOpts1]++NotifyOpts1) of
                        {'EXIT',Err} ->
                            d(StateData, "notify_fork BLTag=~p, Caught error=~120p", [BLTag, Err]),
                            FReply({error,Err});
                        {error,_R}=Err ->
                            d(StateData, "notify_fork BLTag=~p, Error=~120p", [BLTag, Err]),
                            FReply(Err);
                        {async,_ReqHandle} ->
                            d(StateData, "notify_fork BLTag=~p", [BLTag]),
                            FReply(ok)
                    end;
                false -> FReply({error,not_allowed})
            end;
        _ -> FReply({error,not_found})
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% =============================================================
%% Initialization
%% =============================================================

% ----
do_init(State) ->
    #state_forking{map=#{opts:=DlgOpts,ctx_dir:=Source}=Map}=State,
    [Req,FromSipUser,UriRules] = ?EU:extract_required_props([req,from_sipuser,urirules], DlgOpts),
    Now = os:timestamp(),
    % -------
    #sipmsg{call_id=ACallId,
            from={ARUri,ARTag}}=Req,
    {ok, AReqHandle} = nksip_request:get_handle(Req),
    #sdp{}=ARSdp=?U:extract_sdp(Req),
    %
    #sipmsg{vias=[#via{domain=_ViaDomain}|_],
            to={#uri{ext_opts=ALExtOpts}=ALUri,_},
            contacts=ARContacts,
            to_tag_candidate=ToTagCandidate}=Req,
    % mg with several ifaces. found media ifaces RP-2078
    AHost = ?U:get_caller_address(Source,Req),
    % a local uri
    ALTag1 = ToTagCandidate,
    State1 = State#state_forking{usedtags=[ARTag, ALTag1]},
    ALUri1 = ALUri#uri{ext_opts = ?U:store_value(<<"tag">>, ALTag1, ALExtOpts)},
    % a local contact
    ALContact = ?U:build_local_contact(ALUri), % @localcontact (cfg=default! | ViaDomain!)
    % @TODO shadow replacement could be done for AL* to introduce B for A specially
    % a side
    AData = #side{callid=ACallId,
                  rhandle=AReqHandle,
                  opts=?B2BUA_UTILS:build_caller_fork_opts(Req,FromSipUser),
                  %
                  remoteuri=ARUri,
                  remotetag=ARTag,
                  remotecontacts=ARContacts,
                  remotesdp=ARSdp,
                  %
                  localuri=ALUri1,
                  localtag=ALTag1,
                  localcontact=ALContact,
                  realnumaor=?U:make_aor(maps:get(ctx_reqfromuri,Map)),
                  %
                  sipuser=FromSipUser,
                  host=AHost,
                  %
                  allow_events=?U:parse_header_params(Req,<<"allow-events">>),
                  %
                  starttime=Now
                 },
    ?STAT_FACADE:link_domain(ACallId,ALUri1#uri.domain), % RP-415
    % @todo move after MG reserve (overloads mnesia by states if automat is working)
    % ?B2BUA_UTILS:usr_state(busy, State#state_forking.dialogid, AData),
    % -------
    TotalTimerRef = case lists:keyfind(forking_timeout, 1, DlgOpts) of
                        {_,TotalTimeout} -> erlang:send_after(TotalTimeout, self(), {total_timeout});
                        false -> undefined
                    end,
    Ref = make_ref(),
    % -------
    #sipmsg{ruri=#uri{scheme=Scheme,user=User,domain=Domain}}=Req,
    % -------
    State2 = State1#state_forking{a=AData,
                                  map=Map#{applied_redirects:=[{Scheme,User,Domain}]},
                                  b_rule=?U:canonize_uri_rules(UriRules),
                                  total_timer_ref=TotalTimerRef,
                                  ref=Ref,
                                  starttime=Now},
    d(State, "init. Forks: ~120p", [[[<<((maps:get(uri,U))#uri.user)/binary,"@",((maps:get(uri,U))#uri.domain)/binary>> || U <- L] || L <- State2#state_forking.b_rule]]),
    % -------
    self() ! {start, Ref},
    State2.

%% =============================================================
%% Starting
%% =============================================================

%%
do_start(State) ->
    case ?B2BUA_MEDIA:check_available(State) of
        {error,mgc_overload} ->
            StopReason = ?B2BUA_UTILS:reason_error([forking, media, check_available], <<>>, <<"MGC overload">>),
            ?FORKING_UTILS:error_mgc(StopReason, State, ?InternalError("B2B.F. MGC overload"));
        {error,_} ->
            StopReason = ?B2BUA_UTILS:reason_error([forking, media, check_available], <<>>, <<"MGC not ready">>),
            ?FORKING_UTILS:error_mgc(StopReason, State, ?InternalError("B2B.F. MGC not ready"));
        {ok,false} ->
            StopReason = ?B2BUA_UTILS:reason_error([forking, media, check_available], <<>>, <<"No media trunk available">>),
            ?FORKING_UTILS:error_mgc(StopReason, State, ?InternalError("B2B.F. No media trunk available"));
        {ok,true} ->
            ?CALLSTAT:call_state(busy, a, State),
            start_prepare_media(State)
    end.

%% --------
%% checks media formats and create media context
%%
start_prepare_media(State) ->
    case ?B2BUA_MEDIA:prepare_start(State) of
        {error, Reply} when is_tuple(Reply) ->
            StopReason = ?B2BUA_UTILS:reason_error([forking, media, create_context], <<>>, <<"MGC error">>),
            ?FORKING_UTILS:do_final(Reply, State#state_forking{stopreason=StopReason});
        {ok, State1} ->
            ?FSM_EVENT:dlg_call(State1),
            State2 = prepare_b_locals_common(State1),
            start_forward_bside(State2)
    end.

%% --------
%% forms b local uris at start (same for all forks)
%%
prepare_b_locals_common(State) ->
    #state_forking{map=#{opts:=DlgOpts},
                   a=#side{remoteuri=AUri,
                           remotecontacts=[AContact|_]}}=State,
    % b from uri
    {_,ReqFromUri} = lists:keyfind(req_from_uri,1,DlgOpts),
    BLUri = (?U:clear_uri(AUri))#uri{disp=ReqFromUri#uri.disp},
    % b local contact
    BLContact = ?U:build_local_contact(AContact), % @localcontact (cfg=default!)
    % shadow replacement
    BLUriOpt = lists:keyfind('bl_uri', 1, DlgOpts),
    BLContactOpt = lists:keyfind('bl_contact', 1, DlgOpts),
    {BLUri1, BLContact1} = prepare_b_locals({BLUri, BLContact}, {BLUriOpt, BLContactOpt}),
    % save
    State#state_forking{b=#side{localuri=BLUri1,
                                localcontact=BLContact1,
                                last_remote_party=BLUri1}}.
%
prepare_b_locals({BLUri, BLContact}, {BLUriOpt, BLContactOpt}) ->
    {BLUri1, BLContact1} =
        try
            case BLUriOpt of
                false -> {BLUri, BLContact};
                {_,#uri{}=Uri1} -> throw(Uri1);
                #uri{}=Uri1 -> throw(Uri1)
            end
        catch
            throw:#uri{disp=Disp,user=User,domain=Domain} ->
                {?U:update_uri(BLUri, lists:filter(fun({_,undefined})->false;(_)->true end,
                                                   lists:zip([disp,user,domain,user_orig], [Disp,User,Domain,<<>>]))),
                 ?U:update_uri(BLContact, lists:filter(fun({_,undefined})->false;(_)->true end,
                                                       lists:zip([disp,user], [Disp,User])))}
        end,
    BLContact2 =
        try
            case BLContactOpt of
                false -> BLContact1;
                {_,#uri{}=Uri2} -> throw(Uri2);
                #uri{}=Uri2 -> throw(Uri2)
            end
        catch
            throw:#uri{disp=ContDisp,user=ContUser} ->
                ?U:update_uri(BLContact1, lists:filter(fun({_,undefined})->false;(_)->true end,
                                                       lists:zip([disp,user], [ContDisp,ContUser])))
        end,
    {BLUri1, BLContact2}.

%% --------
%% starts a portion of parallel forked uris
%%
start_forward_bside(State) ->
    case forward_bside(State) of
        #state_forking{b_act=[],a=#side{callid=ACallId}}=State1 ->
            d(State, "no actual forks, CallId=~120p", [ACallId]),
            StopReason = ?B2BUA_UTILS:reason_error([forking, invite, forward], <<>>, <<"No forks remain">>),
            ?FORKING_UTILS:do_final(?InternalError("B2B.F. Fork error"), State1#state_forking{stopreason=StopReason});
        #state_forking{}=State1 ->
            {next_state, ?CURRSTATE, State1};
        Term -> Term
    end.

%% =============================================================
%% Forking
%% =============================================================

% -----
forward_bside(State) ->
    #state_forking{b_rule=[UriGroup|Rest]}=State,
    case prepare_calls(UriGroup, State#state_forking{b_rule=Rest}) of
        #state_forking{b_prep=Prepared}=State1 ->
            #state_forking{} = invite_prepared(Prepared, State1#state_forking{b_prep=[]});
        Term -> Term
    end.

% -----
prepare_calls([], State) -> State;
prepare_calls([UriRule|Rest], State) ->
    #state_forking{dialogid=DialogId,
                   map=Map,
                   acallid=ACallId,
                   a=#side{localuri=#uri{domain=ADomain}},
                   b=#side{localuri=BLUriCmn,
                           localcontact=BLContactCmn},
                   b_prep=BPrepared}=State,
    [AReq,DlgNum,OutCallIdx] = ?EU:maps_get([init_req,dlgnum,last_out_callid_idx],Map),
    %[ForksReplaces] = ?EU:extract_optional_default([{forks_replaces,[]}],DlgOpts), % @todo #184.de
    % parse rule
    RuleMap = ?U:parse_uri_rule(UriRule, {?DEFAULT_FORK_RULE_TIMEOUT}),
    [AOR,Uri,Timeout,CalledNum,RouteCtx] = ?EU:maps_get(['aor','uri','ftimeout','callednum','routectx'], RuleMap), % @forktimeout
    [ExtAccount,ROpts,Dir] = ?EU:maps_get_default([{'extaccount',undefined},{'opts',[]},{'dir','inside'}], RuleMap),
    % callid
    BCallId = ?B2BUA_UTILS:build_out_callid(DlgNum, OutCallIdx+1, ACallId),
    % from uri, local contact
    {BLUri, BLContact} = prepare_b_locals({BLUriCmn, BLContactCmn}, % @localcontact (cfg=default! | Uri's route!)
                                          {maps:get('bl_uri', RuleMap, false),
                                           maps:get('bl_contact', RuleMap, false)}),
    % From URI (A side)
    {BLFromTag, State1} = ?FORKING_UTILS:generate_tag(State),
    BLFromUri = BLUri#uri{ext_opts=[{<<"tag">>,BLFromTag}]},
    % To URI (B side)
    {AorS,AorU,AorD}=AOR,
    BRToUri = ?U:update_uri(?U:clear_uri(Uri),
                            [{scheme,AorS},{user,AorU},{domain,AorD},{port,0}]), % @todo AOR-domain
    % monitor, links
    ?DLG_STORE:push_dlg(BCallId, {DialogId,self()}),
    ?MONITOR:append_fun(self(), fun() -> ?DLG_STORE:unlink_dlg(BCallId) end),
    ?STAT_FACADE:link_fork(BCallId, ACallId), % @stattrace
    BDomain = BRToUri#uri.domain,
    ?STAT_FACADE:link_domain(BCallId, BDomain), % RP-415
    ?B2BUA_UTILS:log_callid(DlgNum,ACallId,OutCallIdx+1,BCallId,Map,[ADomain,BDomain]), % #307, RP-415
    % mg with several ifaces. found media ifaces RP-2078
    case ?B2BUA_MEDIA:prepare_fork_ifaces(RuleMap,BCallId,State1) of
        {error,R} ->
            StopReason = ?B2BUA_UTILS:reason_error([forking, media, add_term], <<>>, R),
            ?FORKING_UTILS:error_mgc(StopReason, State1);
        {ok,BForkAddress,State2} ->
            % media check modify
            case ?B2BUA_MEDIA:fwd_invite_request_to_b(State2, Uri, BCallId) of
                {error, R} ->
                    StopReason = ?B2BUA_UTILS:reason_error([forking, media, add_term], <<>>, R),
                    ?FORKING_UTILS:error_mgc(StopReason, State2);
                {ok, State3, BLSdp} ->
                    %
                    CmnOpts = [{call_id,BCallId},
                               {from,BLFromUri},
                               {to,BRToUri},
                               {contact,BLContact},
                               user_agent
                               | ROpts],
                    % --- if esg is proxy, then authenticate is handled by b2bua, should include {sip_pass,Pass}
                    InviteOpts = [{body,BLSdp},
                                  auto_2xx_ack,
                                  %record_route, % #244, #248
                                  {cseq_num,1}],
                    % append Referred-By header if incoming is replacing invite (filter internal props).
                    InviteOpts1 = case nksip_request:header("referred-by", AReq) of
                                      {ok,[RefBy|_]} ->
                                          [#uri{ext_opts=RBUO}=RefByUri|_] = ?U:parse_uris(RefBy),
                                          RBUO1 = lists:filter(fun({<<"r-",_/bitstring>>,_}) -> false; (_) -> true end, RBUO),
                                          [{add,{?ReferredBy,?U:unparse_uri(RefByUri#uri{ext_opts=RBUO1})}} | InviteOpts];
                                      _ -> InviteOpts
                                  end,
                    % append Replaces header if incoming is replacing invite.
                    InviteOpts2 = case nksip_request:header("replaces", AReq) of
                                      {ok,[Replaces|_]} ->
                                          % TODO RP-1525 (agat's early-only and so on)
                                          Replaces1 = re:replace(Replaces, <<"^([^;]+).*(;from-tag=[^;]+).*(;to-tag=[^;]+).*">>, <<"\\1\\2\\3">>,[{return,binary}]),
                                          Replaces2 = re:replace(Replaces1, <<"^([^;]+).*(;to-tag=[^;]+).*(;from-tag=[^;]+).*">>, <<"\\1\\2\\3">>,[{return,binary}]),
                                          % @todo #184.de
                                          [{add,{?Replaces,Replaces2}} | InviteOpts1];
                                      _ -> InviteOpts1
                                  end,
                    % append Call-Info header to make intercom when feature is found.
                    InviteOpts3 = case maps:get(intercom,Map,false) of
                                      true -> [{add,{?CallInfo,?U:unparse_uri(BLContact#uri{opts=[],ext_opts=[{<<"answer-after">>,0}]})}} | InviteOpts2];
                                      false -> InviteOpts2
                                  end,
                    % append FeatureCode header to define entity
                    InviteOpts4 = case maps:get(featurecode_id,RouteCtx,<<>>) of
                                      <<>> -> InviteOpts3;
                                      Id -> [{add,{?FCHeader,Id}} | InviteOpts3]
                                  end,
                    % forward custom headers
                    InviteOpts5 = lists:foldl(fun({<<"x-fwd-",N1/binary>>=N,V},Acc) -> [{add,{case Dir of 'inside' -> N1; 'outside' -> N end,V}} | Acc];
                                                 (_,Acc) -> Acc
                                              end, InviteOpts4, AReq#sipmsg.headers),
                    % fork opts
                    SipUser = maps:get('sipuser',RuleMap,undefined),
                    ForkOpts = [{routectx,RouteCtx}],
                    ForkOpts1 = ?B2BUA_UTILS:update_forkopts_ext(ExtAccount,ForkOpts),
                    ForkOpts2 = ?B2BUA_UTILS:update_forkopts_sipuser(SipUser,ForkOpts1),
                    %
                    BSide=#side_fork{callid = BCallId,
                                     localuri = BLFromUri,localtag=BLFromTag,localcontact=BLContact,localsdp=BLSdp,
                                     remoteuri = BRToUri,
                                     requesturi = Uri,
                                     rule_timeout = Timeout,
                                     %
                                     callednum = CalledNum,
                                     realnumaor = CalledNum, % RP-726
                                     sipuser = SipUser, % put in r_sip_callee
                                     %
                                     opts = ForkOpts2,
                                     redircond = maps:get('redircond', RuleMap, []),
                                     dir = Dir,
                                     host = BForkAddress,
                                     cmnopts = CmnOpts,
                                     inviteopts = InviteOpts5},
                    State4 = State3#state_forking{b_prep=[BSide|BPrepared],
                                                  map=Map#{last_out_callid_idx:=OutCallIdx+1,
                                                           applied_redirects:=[AOR|maps:get(applied_redirects,Map)]}},
                    prepare_calls(Rest, State4)
            end
    end.

% -----
invite_prepared([], State) -> State;
invite_prepared([ForkInfo|Rest], State) ->
    State1 = invite_fork(ForkInfo, State),
    invite_prepared(Rest, State1).

% -----
invite_fork(ForkInfo, State) ->
    #state_forking{map=#{app:=App},
                   b_act=BAct}=State,
    #side_fork{callid=BCallId,
               requesturi=Uri,
               localtag=BLTag,
               rule_timeout=Timeout, % @forktimeout
               cmnopts=CmnOpts,
               inviteopts=InviteOpts}=ForkInfo,
    %
    Now = os:timestamp(),
    %
    case catch nksip_uac:invite(App, Uri, [async|CmnOpts++InviteOpts]) of
        {'EXIT',Err} ->
            d(State, "invite_fork BLTag=~p, Caught error=~120p", [BLTag, Err]),
            State#state_forking{b_fin=[ForkInfo#side_fork{res={error,Err}, starttime=Now, finaltime=Now}|BAct]};
        {error,_R}=Err ->
            d(State, "invite_fork BLTag=~p, Error=~120p", [BLTag, Err]),
            State#state_forking{b_fin=[ForkInfo#side_fork{res=Err, starttime=Now, finaltime=Now}|BAct]};
        {async,ReqHandle} ->
            d(State, "invite_fork BLTag=~p", [BLTag]),
            ?FORKING_UTILS:fork_started(ForkInfo, State),
            Fork1 = ForkInfo#side_fork{rhandle=ReqHandle,
                                       starttime=Now,
                                       rule_timer_ref=erlang:send_after(Timeout, self(), {fork_timeout, [BCallId, BLTag]}), % @forktimeout
                                       resp_timer_ref=erlang:send_after(?DEFAULT_FORK_100_TIMEOUT, self(), {fork_timeout, [BCallId, BLTag]})}, % @forktimeout
            ?FSM_EVENT:fork_start(Fork1,State),
            State#state_forking{b_act=[Fork1|BAct]}
    end.

%% =============================================================
%% Handling UAC responses
%% =============================================================

% -----
do_on_uac_timeout([_CallId, BLTag], State) ->
    #state_forking{b_act=BAct, b_fin=BFin}=State,
    case lists:keyfind(BLTag, #side_fork.localtag, BAct) of
        false -> {next_state, ?CURRSTATE, State};
        #side_fork{}=Fork ->
            ?FORKING_UTILS:cancel_active_forks([Fork], [], State),
            Fork1 = Fork#side_fork{res={timeout},
                                   finaltime=os:timestamp(),
                                   rule_timer_ref=undefined,
                                   resp_timer_ref=undefined},
            State1 = State#state_forking{b_act=BAct--[Fork], b_fin=[Fork1|BFin]},
            ?FSM_EVENT:fork_stop(Fork1,State1),
            check_final(State1)
    end.

% -----
do_on_uac_response([#sipmsg{cseq={_,'INVITE'}}=_Resp]=P, State) ->
    on_invite_response(P, State);

do_on_uac_response([#sipmsg{cseq={_,Method}}=_Resp], State) ->
    d(State, "uac_response('~p') skipped", [Method]),
    {next_state, ?CURRSTATE, State}.

% -----
on_invite_response([#sipmsg{class={resp,100,_SipReason}}=_Resp], State) ->
    {next_state, ?CURRSTATE, State};
on_invite_response([#sipmsg{class={resp,SipCode,_SipReason}}=_Resp]=P, State) when SipCode > 100, SipCode < 200 ->
    on_invite_response_1xx(P, State);
on_invite_response([#sipmsg{class={resp,SipCode,_SipReason}}=_Resp]=P, State) when SipCode >= 200, SipCode < 300 ->
    on_invite_response_2xx(P, State);
on_invite_response([#sipmsg{class={resp,SipCode,_SipReason}}=_Resp]=P, State) when SipCode >= 300, SipCode < 400 ->
    on_invite_response_3xx(P, State);
on_invite_response([#sipmsg{class={resp,SipCode,_}}=Resp]=P, State) when SipCode >= 400, SipCode < 500 ->
    on_invite_response_4xx_5xx_6xx(P, {response,Resp}, State);
on_invite_response([#sipmsg{class={resp,SipCode,_SipReason}}=Resp]=P, State) when SipCode >= 500, SipCode < 600 ->
    on_invite_response_4xx_5xx_6xx(P, {response,Resp}, State);
on_invite_response([#sipmsg{class={resp,SipCode,_SipReason}}=Resp]=P, State) when SipCode >= 600, SipCode < 700 ->
    on_invite_response_4xx_5xx_6xx(P, {response,Resp}, State);
on_invite_response([_Resp]=P, State) ->
    on_invite_response_error(P, State).

%% -----------------------
%% 3xx
%% -----------------------
on_invite_response_3xx([Resp]=P, State) ->
    #sipmsg{contacts=[MoveToUri|_],
            from={_,BLTag}}=Resp,
    #state_forking{b_act=BAct}=State,
    case lists:keyfind(BLTag, #side_fork.localtag, BAct) of
        false -> {next_state, ?CURRSTATE, State};
        #side_fork{remoteuri=BRUri, opts=FOpts}=Fork ->
            % if call to extnumber, then 3xx handled by esg
            RedirAOR = ?U:make_aor(MoveToUri),
            ?FSM_EVENT:fork_redirect(Fork, State, #{redirect_to => RedirAOR,
                                                    owner => ?U:make_aor(BRUri),
                                                    reason => <<"3xx">>}),
            % route ctx
            {_,RouteCtx} = lists:keyfind(routectx,1,FOpts),
            RouteCtx1 = RouteCtx#{by_uri:=BRUri,
                                  to_uri:=MoveToUri#uri{disp= <<>>}},
            % redirect start
            {_BoolDone, _SipCode, State1} = ?FORKING_REDIRECT:make_redirect(RedirAOR, RouteCtx1, State),
            on_invite_response_4xx_5xx_6xx(P, {response,Resp}, State1)
    end.

%% ----
%% unknown
%%
on_invite_response_error([_Resp]=P, State) ->
    on_invite_response_4xx_5xx_6xx(P, {error}, State).

%% ===================================
%% Negative UAC response
%% ===================================

%% -----------------------
%% 4xx, 5xx, 6xx
%% -----------------------
on_invite_response_4xx_5xx_6xx([Resp], ForkResult, State) ->
    #sipmsg{from={_,BLTag}}=Resp,
    #state_forking{b_act=BAct}=State,
    case lists:keyfind(BLTag, #side_fork.localtag, BAct) of
        false -> {next_state, ?CURRSTATE, State};
        Fork -> stop_fork(Fork, ForkResult, State)
    end.

% @private
stop_fork(Fork, ForkResult, State) ->
    #state_forking{b_act=BAct, b_fin=BFin}=State,
    %
    ?FORKING_UTILS:fork_stopped(Fork, State),
    Fork1 = ?FORKING_UTILS:cancel_timer(Fork),
    %
    Fork2 = Fork1#side_fork{res=ForkResult,
                            finaltime=os:timestamp()},
    State1 = State#state_forking{b_act=BAct--[Fork], b_fin=[Fork2|BFin]},
    ?FSM_EVENT:fork_stop(Fork2,State1),
    check_final(State1).

% -------------------------
% Checks if there is no one fork remain
%   in case #90 could repeat full fsm cycle
%
check_final(#state_forking{}=State) ->
    case check_final_1(State) of
        {false, State1} -> {next_state, ?CURRSTATE, State1};
        {true, State1} ->
            case check_retry_with_media(State1) of
                false ->
                    State2 = finalize_best_result(State1),
                    ?FORKING_UTILS:return_stopping(State2);
                {true,State2} ->
                    {next_state, ?CURRSTATE, State2}
            end end.

% #90 check if media mode should be set not to use media as default
check_retry_with_media(#state_forking{use_media=true}) -> false;
check_retry_with_media(#state_forking{map=Map}=State) ->
    B2BHdr = maps:get(b2bhdr,Map),
    case lists:keyfind(<<"media">>,1,B2BHdr) of
        false -> false;
        {_,<<"0">>} ->
            #state_forking{b_fin=BFin}=State,
            DoMedia = lists:foldl(fun(_,true) -> true;
                                     (#side_fork{res={response,#sipmsg{class={resp,415,_}}}},_) -> true; % classic
                                     (#side_fork{res={response,#sipmsg{class={resp,488,_}}}},_) -> true; % yealink
                                     (_,Acc) -> Acc
                                  end, false, BFin),
            case DoMedia of
                false -> false;
                true -> % new cycle of fsm using media
                    Opts = maps:get(opts,Map),
                    Opts1 = lists:keystore(use_media,1,Opts,{use_media,true}),
                    ?FORKING_UTILS:cancel_timer(State),
                    State1 = State#state_forking{use_media=true,
                                                 map=Map#{opts => Opts1,
                                                          last_response => undefined,
                                                          applied_redirects => []},
                                                 a=undefined, b=undefined,
                                                 total_timer_ref=undefined,
                                                 b_fin=[], b_act=[], b_rule=[], b_prep=[],
                                                 stopreason=undefined},
                    State2 = ?FORKING:init(State1),
                    {true,State2}
            end end.

% @private
check_final_1(#state_forking{b_act=[], b_rule=[]}=State) ->
    % final
    % redirects
    case ?FORKING_REDIRECT:check_redirect(State) of
        {false, State1} -> {true, State1};
        {true, State1} -> {false, State1}
    end;
check_final_1(#state_forking{b_act=[]}=State) ->
    % next_prep
    % redirects
    case ?FORKING_REDIRECT:check_redirect(State) of
        {false, State1} ->
            case start_next_fork_group(State1) of
                %{ok,#state_forking{b_act=[]}=State2} -> check_final_1(State2); % @todo - no need repeat redirect
                {ok,State2} -> {false, State2}
            end;
        {true, State1} ->
            {false, State1}
    end;
check_final_1(#state_forking{}=State) ->
    % wait active
    % redirects
    {_,State1} = ?FORKING_REDIRECT:check_redirect(State),
    {false, State1}.

% ------------------------
% Enforces next fork group, which is waiting for active forks
%
start_next_fork_group(State) ->
    State1 = forward_bside(State),
    {ok,State1}.

% ------------------------
% Find best negative result, send to caller and finalize dialog
%
finalize_best_result(#state_forking{b_fin=BFin}=State) ->
    Fchoose = fun(X,undefined) -> X;
                 ({Code1,_}=X1,{Code2,_}=X2) ->
                      case ?FORKING_UTILS:choose_best_result({Code1 div 100, Code1}, {Code2 div 100, Code2}) of
                          Code1 -> X1;
                          _ -> X2
                      end end,
    Fbest = fun({response,#sipmsg{class={resp,SipCode,_}}=Resp}, Acc) -> Fchoose({SipCode, Resp},Acc);
               ({timeout}, Acc) -> Fchoose({408,undefined}, Acc);
               ({bye}, Acc) -> Fchoose({487,undefined}, Acc);
               ({cancelled}, Acc) -> Fchoose({487,undefined}, Acc);
               ({error}, undefined) -> {500,undefined};
               ({error}, Acc) -> Acc
            end,
    {BestSipCode,Resp} = lists:foldl(Fbest, undefined, [Res || #side_fork{res=Res} <- BFin]),
    Opts = fwd_response_headers_auto(Resp, []),
    State1 = ?FORKING_UTILS:send_response_to_caller({BestSipCode, Opts}, State),
    State2 = ?FORKING_UTILS:do_finalize_dialog(State1),
    StopReason = ?B2BUA_UTILS:reason_callfinal(BestSipCode),
    State2#state_forking{stopreason=StopReason}.

% make opts to forward some headers (reason, warning)
fwd_response_headers_auto(undefined, Opts) -> Opts;
fwd_response_headers_auto(Resp, Opts) ->
    fwd_response_headers([?Reason,?Warning], Resp, Opts).

% make opts to forward response headers
fwd_response_headers(_, undefined, Opts) -> Opts;
fwd_response_headers([], _Resp, Opts) -> Opts;
fwd_response_headers([H|Rest], Resp, Opts) ->
    case nksip_response:header(?EU:to_binary(string:to_lower(?EU:to_list(H))),Resp) of
        {ok,[_|_]=Vs} -> fwd_response_headers(Rest, Resp, Opts ++ lists:map(fun(V) -> {add, {H,V}} end, Vs));
        _ -> fwd_response_headers(Rest, Resp, Opts)
    end.

%% ===================================
%% Positive UAC response
%% ===================================

%% -----------------------
%% 1xx
%% -----------------------
on_invite_response_1xx([#sipmsg{class={resp,SipCode,_}}=Resp], State) ->
    #state_forking{map=#{last_response:=LastResp}=Map,
                   a=ASide,
                   b_act=BAct}=State,
    {Fork,State1} = update_fork(Resp, State), % @forktimeout
    Opts = fwd_response_headers_auto(Resp, [no_dialog]),
    case {length(BAct), LastResp, ?U:extract_sdp(Resp)} of
        % only 1 fork active (current group)
        {1, _, #sdp{}} ->
            case ?B2BUA_MEDIA:fwd_invite_response_to_a(State1, Resp) of
                % TODO: some error to recall by another rtp/srtp mode
                {error, R} ->
                    StopReason = ?B2BUA_UTILS:reason_error([forking, media, update_term], <<>>, R),
                    ?FORKING_UTILS:error_mgc(StopReason, State1);
                {ok, State2, ALSdp} ->
                    State3 = State2#state_forking{a=ASide#side{localsdp=ALSdp},
                                                  map=Map#{last_response:={SipCode,true}}},
                    forward_response_1xx({SipCode, [{body,ALSdp}|Opts]}, {Fork, Resp}, State3)
            end;
        {1, _, _} ->
            forward_response_1xx({SipCode, Opts}, {Fork, Resp}, State1#state_forking{map=Map#{last_response:={SipCode,false}}});
        % more than 1 fork active
        {_N, undefined, _} when _N > 1 -> % not answered yet
            forward_response_1xx({180, Opts}, {Fork, Resp}, State1#state_forking{map=Map#{last_response:={180,false}}});
        {_N, {_,true}, _} when _N > 1 -> % answered + sdp
            forward_response_1xx({180, Opts}, {Fork, Resp}, State1#state_forking{map=Map#{last_response:={180,false}}});
        {_N, {_,false}, _} when _N > 1 -> % answered no sdp
            {next_state, ?CURRSTATE, State1}
    end.

% --
update_fork(#sipmsg{}=Resp, State) ->
    #sipmsg{call_id=CallId,
            class={resp,SipCode,SipReason},
            from={_,BLTag},
            to={_,BRTag},
            contacts=BRContacts}=Resp,
    #state_forking{b_act=BAct}=State,
    #side_fork{resp_timer_ref=T,
               last_response_code=LastSipCode,
               allow_events=AllowEvents0}=Fork = lists:keyfind(BLTag, #side_fork.localtag, BAct),
    X = case SipCode of
            180 when LastSipCode/=SipCode -> ?DEFAULT_FORK_180_TIMEOUT; % @forktimeout
            182 -> ?DEFAULT_FORK_182_TIMEOUT; % @forktimeout
            183 -> ?DEFAULT_FORK_183_TIMEOUT; % @forktimeout
            _ -> undefined
        end,
    {ok,BDlgHandle} = nksip_dialog:get_handle(Resp),
    AllowEvents = case ?U:parse_header_params(Resp,<<"allow-events">>) of
                      [] -> AllowEvents0;
                      AE -> AE
                  end,
    Fork1 = case X of
                undefined -> Fork;
                Timeout ->
                    ?EU:cancel_timer(T),
                    Fork#side_fork{resp_timer_ref=erlang:send_after(Timeout, self(), {fork_timeout, [CallId, BLTag]})}
            end,
    Fork2 = Fork1#side_fork{dhandle=BDlgHandle,
                            last_response_code=SipCode,
                            remotetag=BRTag,
                            remotecontacts=BRContacts, % RP-1297
                            allow_events=AllowEvents},
    BAct1 = lists:keystore(BLTag, #side_fork.localtag, BAct, Fork2),
    State1 = State#state_forking{b_act=BAct1},
    ?FSM_EVENT:fork_preanswer(Fork2,State1,#{resp => {resp,SipCode,SipReason}}),
    {Fork2,State1}.

% --
forward_response_1xx({SipCode, Opts}, {Fork, Resp}, State) ->
    % @TODO @remoteparty
    Opts1 = case ?B2BUA_UTILS:extract_property(addRemoteParty1xx, false, State) of
                false -> Opts;
                true ->
                    #sipmsg{to={BRUri,_Tag}}=Resp,
                    RemoteParty = ?B2BUA_UTILS:build_remote_party_id(Resp, Fork, BRUri),
                    ?U:remotepartyid_opts(RemoteParty,'1xx') ++ Opts;
                {true,#uri{}=RemoteParty} ->
                    ?U:remotepartyid_opts(RemoteParty,'1xx') ++ Opts
            end,
    % --
    State1 = ?FORKING_UTILS:send_response_to_caller({SipCode, Opts1}, State),
    {next_state, ?CURRSTATE, State1}.

%% ------------------------
%% 2xx
%% ------------------------
on_invite_response_2xx([Resp], State) ->
    Now = os:timestamp(),
    % -----
    #sipmsg{class={resp,SipCode,SipReason},
            from={_,BLTag},
            to={BRUri,_}}=Resp,
    %#sdp{}=?U:extract_sdp(Resp),
    #state_forking{b_act=BAct,
                   b=BSide,
                   total_timer_ref=TotalTimerRef,
                   b_fin=BFin}=State,
    case TotalTimerRef of undefined -> ok; _ -> erlang:cancel_timer(TotalTimerRef) end,
    Fork = lists:keyfind(BLTag, #side_fork.localtag, BAct),
    ?FORKING_UTILS:cancel_timer(Fork),
    ?FSM_EVENT:fork_answer(Fork,State,#{resp => {resp,SipCode,SipReason},
                                        esgdlg => case ?U:parse_header_params(Resp,?EsgDlgHeaderLow) of [] -> <<>>; [EsgDlg|_] -> EsgDlg end}), % RP-1575
    %
    BCancel = BAct--[Fork],
    ?FORKING_UTILS:cancel_active_forks(BCancel, [{reason, {sip, 200, "Call completed elsewhere"}}], State),
    ?FORKING_UTILS:unlink_forks(BCancel++BFin),
    % -----
    case ?B2BUA_MEDIA:fwd_invite_response_to_a(State, Resp) of
        % TODO: some error to recall by another rtp/srtp mode
        {error, R} ->
            StopReason = ?B2BUA_UTILS:reason_error([forking, media, update_term], <<>>, R),
            ?FORKING_UTILS:error_mgc(StopReason, State);
        {ok, State1, ALSdp} ->
            % response to a
            Opts = [{body,ALSdp},
                    user_agent],
            Opts1 = fwd_response_headers_auto(Resp, Opts),
            % @TODO @remoteparty for a on invite response header 'remote-party' (future from when reinvite backside)
            FRemoteParty = fun() -> ?B2BUA_UTILS:build_remote_party_id(Resp, Fork, BRUri) end,
            Opts2 = case ?B2BUA_UTILS:extract_property(addRemoteParty2xx, false, State) of
                        false -> RemoteParty = FRemoteParty(), Opts1;
                        true -> RemoteParty = FRemoteParty(), ?U:remotepartyid_opts(RemoteParty,'2xx') ++ Opts1;
                        {true,#uri{}=RemoteParty} -> ?U:remotepartyid_opts(RemoteParty,'2xx') ++ Opts1
                    end,
            % ----
            State2 = ?FORKING_UTILS:send_response_to_caller({SipCode, Opts2}, State1),
            % -----
            #state_forking{a=ASide}=State2,
            ?CALLSTAT:call_state(busy, a, State2),
            ?FORKING_UTILS:fork_applied(Fork, State2),
            % make sides
            BSide1 = ?FORKING_UTILS:make_dlg_side(Fork, BSide, Now, {Resp}),
            ASide1 = ASide#side{last_remote_party=RemoteParty,
                                answertime=Now},
            % move route ctx from b to a
            #side{opts=BFOpts}=BSide1,
            ASide2 = ASide1#side{opts=lists:keystore('routectx',1,ASide#side.opts,lists:keyfind('routectx',1,BFOpts)),
                                 localsdp=ALSdp},
            BSide2 = BSide1#side{opts=lists:keydelete('routectx',1,BFOpts)},
            % make state
            State3 = ?FORKING_UTILS:make_dlg_state(State2, Now, {ASide2,BSide2}),
            %
            d(State2, " -> switch to '~p'", [?ACKWAITING_STATE]),
            %
            AckWOpts = [{sides,['a']}],
            ?ACKWAITING:start({State3,AckWOpts})
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

% @private ----
d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    #state_forking{dialogid=DlgId}=State,
    ?LOGSIP("B2B fsm ~p '~p':" ++ Fmt, [DlgId,?CURRSTATE] ++ Args).
