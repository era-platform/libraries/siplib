%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Call Pickup sub routines of 'forking' state of B2BUA Dialog FSM
%%%         Acts on incoming pickup invite.

-module(r_sip_b2bua_fsm_forking_pickup).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([start_pickup/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_mgc.hrl").
-include("../include/r_sip_headers.hrl").

-define(CURRSTATE, 'forking').

%% ====================================================================
%% API functions
%% ====================================================================

start_pickup([_CallId, _Req, _FromSipUser, _FromReqUri]=P, State) ->
    do_on_pickup(P, State).

%% ====================================================================
%% Internal functions
%% ====================================================================

do_on_pickup(P, State) ->
    pickup_media(P, State).

%
pickup_media([_, Req|_]=P, State) ->
    % . Setup new media termination
    case ?B2BUA_MEDIA:apply_invite_pickup_from_b(State, Req) of
        {error,R} ->
            pickup_error(R, Req, State);
        {ok, State1, ALSdp, BLSdp} ->
            pickup_send_response({ALSdp,BLSdp}, P, State1)
    end.

%
pickup_send_response({_ALSdp,BLSdp}=P1, [_CallId, Req|_]=P, State) ->
    #state_forking{map=Map,
                   b=BSide,
                   usedtags=UsedTags}=State,
    % get props from request
    #sipmsg{to={BLUri,_},
            to_tag_candidate=Tag}=Req,
    {ok,ReqHandle} = nksip_request:get_handle(Req),
    % ensure tag (candidate already in fine form, but could be dublicated in dlg)
    {BLTag, State1} = case lists:member(Tag, UsedTags) of
                          false -> {Tag, State#state_forking{usedtags=[Tag|UsedTags]}};
                          true -> {?FORKING_UTILS:generate_tag(State), State}
                      end,
    % generate contact
    BLContact = ?U:build_local_contact(BLUri), % @localcontact (cfg=default! | top via's domain!)
    % response to b pickup
    Opts = [{to_tag, BLTag},
            {contact, BLContact},
            {body, BLSdp},
            user_agent],
    % @TODO @remotepartyid remote-party-id option forward (response header, from+display(dc), options, rights)
    {_,ReqFromUri} = lists:keyfind(req_from_uri,1,maps:get(opts,Map)), % 'a' req_from_uri
    RemoteParty = ReqFromUri, % BSide#side.last_remote_party,
    Opts1 = case ?B2BUA_UTILS:extract_property(addRemoteParty2xx, false, State) of
                false -> Opts;
                _ -> ?U:remotepartyid_opts(RemoteParty,'2xx') ++ Opts
            end,
    %
    State2 = pickup_send_response_200(ReqHandle, Opts1, State1),
    State3 = State2#state_forking{b=BSide#side{last_remote_party=RemoteParty}},
    pickup_cancel_forks({BLUri,BLTag,BLContact}, P1, P, State3).

% @private
pickup_send_response_200(ReqHandle, Opts, State) ->
    d(State, "send_response_to_pickuper 200"),
    ?U:send_sip_reply(fun() -> nksip_request:reply({200, Opts}, ReqHandle) end),
    State.

%
pickup_cancel_forks(P2, P1, P, State) ->
    #state_forking{b=#side{callid=BCallId},
                   b_act=BAct,
                   b_fin=BFin,
                   total_timer_ref=TotalTimerRef}=State,
    case TotalTimerRef of undefined -> ok; _ -> erlang:cancel_timer(TotalTimerRef) end,
    ?FORKING_UTILS:cancel_active_forks(BAct, [{reason, {sip, 200, "Call completed elsewhere"}}], State),
    ?DLG_STORE:unlink_dlg(BCallId),
    lists:foldl(fun(#side_fork{callid=FCallId}, Acc) ->
                        case lists:member(FCallId, Acc) of
                            false -> ?DLG_STORE:unlink_dlg(FCallId), [FCallId|Acc];
                            true -> Acc
                        end end, [BCallId], BAct++BFin),
    pickup_update_dialog(P2, P1, P, State).

%
pickup_update_dialog({BLUri, BLTag, BLContact}=P2, P1, [CallId, Req, FromSipUser, FromReqUri|_]=P, State) ->
    #state_forking{dialogid=DialogId}=State,
    #sipmsg{from={BRUri,BRTag},
            to={#uri{user=CalledNum}=BLUri,_},
            contacts=[BRContact|_]}=Req,
    % . Store new CallId to monitor
    ?DLG_STORE:push_dlg(CallId, {DialogId,self()}),
    ?MONITOR:append_fun(self(), fun() -> ?DLG_STORE:unlink_dlg(CallId) end),
    % . Make alternative B-Fork by Pickup-invite
    RUri = BRContact,
    Now = os:timestamp(),
    BFork=#side_fork{callid=CallId,
                     localuri=BLUri,localtag=BLTag,localcontact=BLContact,
                     remoteuri=BRUri,remotetag=BRTag,
                     requesturi=RUri,
                     rule_timeout=0,
                     callednum=CalledNum,
                     realnumaor={sip,FromReqUri#uri.user,FromReqUri#uri.domain}, % RP-726
                     sipuser=FromSipUser,
                     opts=?B2BUA_UTILS:build_caller_fork_opts(Req,FromSipUser),
                     starttime=Now,
                     redircond = [],
                     cmnopts=[],
                     inviteopts=[]},
    State1 = State#state_forking{b_act=[BFork]},
    ?FSM_EVENT:dlg_pickup(BFork, State1),
    % . Make virtual response message by pickup-invite-request
    % . Call invite_response_2xx
    % .   cancel out invites
    % .   response to initiator
    % .   switch to dialog
    pickup_switch_dialog(Now, BFork, P2, P1, P, State1).

%
pickup_switch_dialog(Now, BFork, {BLUri, BLTag, _BLContact}=_P2, {ALSdp,_BLSdp}=_P1, [_,Req,_,FromReqUri]=_P, State) ->
    #state_forking{a=ASide,
                   b=BSide,
                   b_act=[BFork]}=State,
    #sipmsg{from={BRUri,BRTag},
            contacts=BRContacts}=Req,
    #sdp{}=BRSdp=?U:extract_sdp(Req),
    % response to a
    % remote-party-id option forward (response header, from+display(dc), options, rights)
    RemoteParty = FromReqUri,
    %
    Opts = [{body, ALSdp},
            user_agent],
    Opts1 = case ?B2BUA_UTILS:extract_property(addRemoteParty2xx, false, State) of
                false -> Opts;
                _ -> ?U:remotepartyid_opts(RemoteParty,'2xx') ++ Opts
            end,
    State1 = ?FORKING_UTILS:send_response_to_caller({200, Opts1}, State),
    %
    ASide1 = ASide#side{last_remote_party=RemoteParty,
                        answertime=Now},
    %
    {ok,BDlgHandle} = nksip_dialog:get_handle(Req#sipmsg{to={BLUri#uri{ext_opts=[{<<"tag">>,BLTag}]},BLTag}}),
    BLRP = BSide#side.last_remote_party, % #190 handle of 24.08.2016 fix
    BSide1 = ?FORKING_UTILS:make_dlg_side_1(BFork, BSide, Now, {BRUri,BRTag,BLRP,BRContacts,BRSdp,BDlgHandle}),
    %
    State2 = ?FORKING_UTILS:make_dlg_state(State1, Now, {ASide1,BSide1}),
    %
    ?CALLSTAT:call_state(busy, a, State2),
    ?CALLSTAT:call_state(busy, b, State2),
    %
    d(State, " -> switch to '~p'", [?ACKWAITING_STATE]),
    %
    AckWOpts = [{sides,['a','b']}],
    ?ACKWAITING:start({State2,AckWOpts}).

% -----
pickup_error(R, Req, State) ->
    d(State, "error pickup: ~p, send 500 response to pickuper", [R]),
    {ok,ReqHandle} = nksip_request:get_handle(Req),
    ?U:send_sip_reply(fun() -> nksip_request:reply(?InternalError("B2B.F. Pickup media error"), ReqHandle) end),
    {next_state, ?CURRSTATE, State}.


%% ==============================================================

% @private ----
d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    #state_forking{dialogid=DlgId}=State,
    ?LOGSIP("B2B fsm ~p '~p':" ++ Fmt, [DlgId,?CURRSTATE] ++ Args).
