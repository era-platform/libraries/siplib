%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Redirect sub routines of 'forking' state of B2BUA Dialog FSM

-module(r_sip_b2bua_fsm_forking_redirect).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([check_redirect/1,
         make_redirect/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_mgc.hrl").

-define(CURRSTATE, 'forking').

%% ====================================================================
%% API functions
%% ====================================================================

%% -------------
%% check best result of number (if number is finalized on all forks)
%%
check_redirect(#state_forking{fixed_forks=true}=State) -> {false, State};
check_redirect(#state_forking{}=State) ->
    #state_forking{b_act=Cur,
                   b_rule=Fut,
                   b_fin=[#side_fork{localuri=LocalURI,
                                     remoteuri=BRUri,
                                     callednum=CalledNum,
                                     redircond=RedirCond,
                                     sipuser=SipUser,
                                     opts=FOpts}|_]}=State,
    Domain = LocalURI#uri.domain,
    case RedirCond of
        [] -> {false, State};
        _ ->
            case lists:foldl(fun(_,true) -> true;
                                (#side_fork{callednum=C},_) when C==CalledNum -> true;
                                (_,_) -> false
                             end, false, lists:flatten(Cur) ++ lists:flatten(Fut)) of
                true -> {false, State};
                false ->
                    BestResult = get_number_best_result(CalledNum, State, Domain),
                    case lists:keyfind(BestResult, 1, RedirCond) of
                        false -> {false, State};
                        {_, RedirNumber} ->
                            % @todo @redirect apply modifier to FNumExt
                            RedirNumber1 = ?REDIRECT:apply_modifier(RedirNumber, case ?EU:extract_optional_props([extension],SipUser) of
                                                                                     [undefined] -> <<>>;
                                                                                     [FNumExt] -> FNumExt
                                                                                 end, []),
                            MoveToUri = BRUri#uri{disp= <<>>, user=RedirNumber1},
                            RedirAOR = ?U:make_aor(MoveToUri),
                            ?FSM_EVENT:user_redirect(State, #{redirect_to => RedirAOR,
                                                              owner => ?U:make_aor(BRUri),
                                                              reason => BestResult}),
                            % route ctx
                            {_,RouteCtx} = lists:keyfind(routectx,1,FOpts),
                            RouteCtx1 = maps:without([featurecode_id, featurecode_prefix], RouteCtx),
                            RouteCtx2 = RouteCtx1#{by_uri:=BRUri,
                                                   to_uri:=MoveToUri},
                            % redirect start
                            case make_redirect(RedirAOR, RouteCtx2, State) of
                                {false,_,State1} -> {false, State1};
                                {true,_,State1} -> {true, State1}
                            end end end end.

%% -----------------------------------
%% make redirect to aor (if this aor was not called yet, no denial to apply redirects in x-era-b2bua)
%%
make_redirect(RedirAOR, RouteCtx, #state_forking{map=Map}=State) ->
    % 05.02.2017 check if there is no denial of redirect in header opts (x-era-b2bua)
    B2BHdr = maps:get(b2bhdr,Map),
    case lists:keyfind(<<"redirect">>,1,B2BHdr) of
        false -> make_redirect_1(RedirAOR, RouteCtx, State);
        {_,<<"0">>} -> {false, 410, State}
    end.

make_redirect_1(RedirAOR, RouteCtx, State) ->
    #state_forking{map=#{init_req:=Req,
                         applied_redirects:=Redirects}=Map}=State,
    case lists:member(RedirAOR,Redirects) of
        true ->
            d(State, "3xx-MoveTo ~120p, found cycled", [RedirAOR]),
            % loop detected
            {false, 482, State};
        false ->
            SipCode = 410, % gone (408 not acceptable, 488 not acceptable here, 410 gone, 480 temporarily unavailable)
            State1 = State#state_forking{map=Map#{applied_redirects:=[RedirAOR|Redirects]}},
            case ?B2BUA_ROUTER_INVITE:build_route_opts_onredirect(RouteCtx, Req) of
                {return, _} ->
                    {false, SipCode, State1};
                {reply, _Reply} ->
                    %{SipCode1,_Opts} = nksip_reply:parse(Reply),
                    {false, SipCode, State1};
                {ok, [{urirules,[_|_]=UriRules}|_], _Ctx1} ->
                    #state_forking{b_rule=OrigRules}=State1,
                    [UriGroup|Rest] = ?U:canonize_uri_rules(UriRules),
                    #state_forking{b_prep=Prepared}=State2= ?FORKING:forking_internal_prepare_calls(UriGroup, State1#state_forking{b_rule=Rest++OrigRules}),
                    State3 = ?FORKING:forking_internal_invite_prepared(Prepared, State2#state_forking{b_prep=[]}),
                    {true, SipCode, State3}
            end
    end.

%% --------------------------------------
%% defines best aor result (to check redirect rule)
%%
get_number_best_result(CalledNum, State, Domain) ->
    #state_forking{b_fin=Fin}=State,
    Results = lists:foldl(fun(#side_fork{callednum=C, res=Res},Acc) when C==CalledNum ->
                                  case Res of
                                      {response,SipCode} -> [SipCode|Acc];
                                      {bye} -> [487|Acc];
                                      {timeout} -> [408|Acc];
                                      {error,_} -> [0|Acc];
                                      {cancelled} -> Acc
                                  end;
                             (_,Acc) -> Acc
                          end, [], Fin),
    % decline: 603
    % busy: 486 busy
    % timeout: 408 timeout, not respond
    % dnd: 404 not found, 480 temporary unav -> dnd
    % error: 0
    % other: *

    Best = case lists:sort(fun redir_sort_sipcode/2, Results) of
               [] -> undefined;
               [SC1|_] when is_integer(SC1) -> SC1;
               [#sipmsg{class={resp,SC2,_}}|_] -> SC2
           end,
    case get_redirect_codes(Domain, #{}) of
        [] -> redir_map_sipcode(Best);
        {error,_} -> redir_map_sipcode(Best); % RP-1344
        RedirectCodes ->
            Default = <<"other">>,
            F = fun(El) ->
                        case maps:get(sipcode, El, -1) of
                            Best -> {'true', maps:get(mode, El, Default)};
                            _ -> false
                        end
                end,
            case lists:filtermap(F, RedirectCodes) of
                [Mode|_] -> Mode;
                _ -> Default
            end
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
redir_map_sipcode(603) -> <<"decline">>;
redir_map_sipcode(486) -> <<"busy">>;
redir_map_sipcode(408) -> <<"timeout">>;
redir_map_sipcode(404) -> <<"dnd">>;
redir_map_sipcode(480) -> <<"dnd">>;
redir_map_sipcode(0) -> <<"error">>;
redir_map_sipcode(_) -> <<"other">>.

%% @private
% sort sdp props by priority
redir_sort_sipcode({X,_},{Y,_}) -> redir_sortidx(X) =< redir_sortidx(Y);
redir_sort_sipcode(X,Y) when is_integer(X), is_integer(Y) -> redir_sortidx(X) =< redir_sortidx(Y).
% priority
redir_sortidx(603) -> 1;
redir_sortidx(486) -> 2;
redir_sortidx(408) -> 3;
redir_sortidx(404) -> 4;
redir_sortidx(480) -> 4;
redir_sortidx(0) -> 5;
redir_sortidx(_) -> 6.

%% ------------------
%% @private
%% get codes in cache or query in DC.
%% ------------------
-spec get_redirect_codes(Domain::binary(), Opts::map()) -> Codes::[Code::#{sipcode => non_neg_integer(), mode => binary()}].
%% ------------------
get_redirect_codes(_Domain, Opts) ->
    MD = ?EU:to_binary(?CFG:get_general_domain()),
    Key = {MD, <<"redirect_codes">>},
    Fbuild = fun() -> ?DC:get_redirect_codes(MD, Opts) end,
    ?SIPSTORE:lazy_t(Key, Fbuild).

%% ==============================================================

% @private ----
%d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    #state_forking{dialogid=DlgId}=State,
    ?LOGSIP("B2B fsm ~p '~p':" ++ Fmt, [DlgId,?CURRSTATE] ++ Args).
