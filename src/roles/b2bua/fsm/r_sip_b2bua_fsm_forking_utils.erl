%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Service utils of 'forking' state of B2BUA Dialog FSM

-module(r_sip_b2bua_fsm_forking_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_mgc.hrl").

-define(CURRSTATE, 'forking').

%% ====================================================================
%% API functions
%% ====================================================================

%% -------------------
%% Generates non existing tag
%% -------------------
generate_tag(#state_forking{usedtags=Tags}=State) ->
    Tag = ?B2BUA_UTILS:build_tag(),
    case lists:member(Tag, Tags) of
        false -> {Tag, State#state_forking{usedtags=[Tag|Tags]}};
        true -> generate_tag(State)
    end.

%% --------------------
%% handle fork start/stop call to change states
%% -------------------
fork_started(#side_fork{dir='inside'}=ForkInfo, State) ->
    ?CALLSTAT:call_state(early, b, ForkInfo, State);
fork_started(_,_) -> ok.

%%
fork_stopped(#side_fork{dir='inside'}=ForkInfo, State) ->
    ?CALLSTAT:call_state(free, b, ForkInfo, State);
fork_stopped(_,_) -> ok.

%%
fork_applied(#side_fork{dir='inside'}=ForkInfo, State) ->
    ?CALLSTAT:call_state(busy, b, ForkInfo, State);
fork_applied(_,_) -> ok.

%% -------------------
%% Cancel active forks
%% -------------------
cancel_active_forks([],_,_) -> ok;
cancel_active_forks([Fork|Rest], Opts, State) ->
    #side_fork{rhandle=ReqHandle,
               cmnopts=CmnOpts}=Fork,
    cancel_timer(Fork),
    catch nksip_uac:cancel(ReqHandle, [async|Opts++CmnOpts]),
    ?FSM_EVENT:fork_cancel(Fork, State),
    ?FORKING_UTILS:fork_stopped(Fork, State),
    cancel_active_forks(Rest, Opts, State).

%% -------------------
%% Send response to caller
%% -------------------
send_response_to_caller({SipCode, Opts}, State) ->
    #state_forking{a=#side{rhandle=ReqHandle,
                           localtag=ALTag,
                           localcontact=ALContact}}=State,
    d(State, "send_response_to_caller ~p", [SipCode]),
    Opts1 = [{to_tag, ALTag},
             {contact, ALContact}|Opts],
    ?U:send_sip_reply(fun() -> nksip_request:reply({SipCode, Opts1}, ReqHandle) end),
    State.

%% -------------------
%% Cancel timers (fork|state)
%% -------------------
cancel_timer(#side_fork{rule_timer_ref=T1, resp_timer_ref=T2}=Fork) ->
    cancel_timer(T1),
    cancel_timer(T2),
    Fork#side_fork{rule_timer_ref=undefined,
                   resp_timer_ref=undefined};

cancel_timer(#state_forking{total_timer_ref=T}=State) ->
    cancel_timer(T),
    State#state_forking{total_timer_ref=undefined};

cancel_timer(undefined) -> undefined;
cancel_timer(T) when is_reference(T) -> erlang:cancel_timer(T).

%% ------------------------
%% when one abonent is called, then like a proxy
%% -------------------
choose_best_result({G1,Code1}, {G2,Code2}) when G1==6, G2==6 ->
    choose_best_result_of(Code1, Code2, [603,606,600,604]);
choose_best_result({G1,Code1}, {G2,Code2}) when G2==5, G1==5 ->
    choose_best_result_of(Code1, Code2, [500,501,502,503,504,505,513,580]);
choose_best_result({G1,Code1}, {G2,Code2}) when G2==4, G1==4 ->
    choose_best_result_of(Code1, Code2, [401,407,415,420,484,
                                         400,402,403,404,405,406,409,410,411,412,413,414,416,417,
                                         421,422,423,424,428,429,430,433,436,437,438,439,
                                         470,480,481,482,483,485,486,487,488,489,491,493,494,
                                         408]);
choose_best_result({G1,_Code1}, {G2,Code2}) when G2==4, G1/=4 -> Code2;
choose_best_result({G1,Code1}, {G2,_Code2}) when G2/=4, G1==4 -> Code1;
choose_best_result({G1,_Code1}, {G2,Code2}) when G2==5, G1/=5 -> Code2;
choose_best_result({G1,Code1}, {G2,_Code2}) when G2/=5, G1==5 -> Code1;
choose_best_result({G1,_Code1}, {G2,Code2}) when G2==6, G1/=6 -> Code2;
choose_best_result({G1,Code1}, {G2,_Code2}) when G2/=6, G1==6 -> Code1.

choose_best_result_of(Code1, Code2, PriorityList) ->
    case lists:filter(fun(C) when C==Code1; C==Code2 -> true;
                         (_) -> false
                      end, PriorityList) of
        [] -> Code2;
        [Code1|_] -> Code1;
        [Code2|_] -> Code2
    end.

%% -------------------
%% make side record by side_fork when switch to dialog state
%% -------------------
make_dlg_side(#side_fork{}=ForkInfo, BSide, Now, {Resp}) ->
    #sipmsg{to={BRUri,BRTag},
            contacts=BRContacts}=Resp,
    #sdp{}=BRSdp=?U:extract_sdp(Resp),
    %
    {ok,BDlgHandle} = nksip_dialog:get_handle(Resp),
    BLRP = ?U:clear_uri(ForkInfo#side_fork.localuri), % #190 handle of 24.08.2016 fix
    make_dlg_side_1(ForkInfo,BSide,Now,{BRUri,BRTag,BLRP,BRContacts,BRSdp,BDlgHandle}).

make_dlg_side_1(#side_fork{}=ForkInfo, BSide, Now, {BRUri,BRTag,BLRP,BRContacts,BRSdp,BDlgHandle}) ->
    %
    BSide#side{rhandle=ForkInfo#side_fork.rhandle,
               dhandle=BDlgHandle,
               callid=ForkInfo#side_fork.callid,
               opts=ForkInfo#side_fork.opts,
               %
               remoteuri=BRUri,
               remotetag=BRTag,
               remotecontacts=BRContacts,
               remotesdp=BRSdp,
               %
               localuri=ForkInfo#side_fork.localuri,
               localtag=ForkInfo#side_fork.localtag,
               localcontact=ForkInfo#side_fork.localcontact,
               localsdp=ForkInfo#side_fork.localsdp,
               last_remote_party = BLRP, % #190
               %
               realnumaor=ForkInfo#side_fork.realnumaor,
               callednum=ForkInfo#side_fork.callednum,
               sipuser=ForkInfo#side_fork.sipuser,
               host=ForkInfo#side_fork.host,
               %
               allow_events=ForkInfo#side_fork.allow_events,
               %
               starttime=ForkInfo#side_fork.starttime,
               answertime=Now}.

%% -------------------
%% make state_dlg by state_forking when switch to dialog state
%% -------------------
make_dlg_state(State, Now, {ASide,BSide}) ->
    #state_forking{map=Map}=State,
    Map1 = maps:with([app,dlgnum,opts,store_data,last_out_callid_idx,ctx_scrid_master,ctx_scrid_domain,dlg_bindings,ghost_mode,dtmf2send],Map),
    Dtmf2Send = maps:get(dtmf2send,Map),
    #state_dlg{pid= State#state_forking.pid,
               dialogid= State#state_forking.dialogid,
               dialogidhash= State#state_forking.dialogidhash, % #370
               acallid= State#state_forking.acallid, % #370
               acallidhash = State#state_forking.acallidhash, % #370
               invite_id= State#state_forking.invite_id, % RP-385
               invite_ts= State#state_forking.invite_ts,
               map= Map1#{relates => [relates_b(BSide)|maps:get(relates,Map)],
                          init_callid => State#state_forking.acallid,
                          timer_migration_ref => undefined,
                          ctx_esgdlg => maps:get(ctx_esgdlg,Map)},
               use_media= State#state_forking.use_media,
               media= State#state_forking.media,
               %
               a= ASide,
               b= BSide#side{dtmf2send = case maps:get(dtmf2send,Map) of <<>> -> []; Dtmf2Send -> [#{dtmf=>Dtmf2Send}] end},
               %
               starttime= State#state_forking.starttime,
               answertime= Now}.

%% @private RP-640
relates_b(#side{remoteuri=#uri{user=N,domain=D}}=B) ->
    case B#side.sipuser of
        undefined -> {D,{other,N}};
        SU -> {D,{sipuser,?EU:get_by_key(login,SU,<<>>)}}
    end.

%% -------------------
%% Finalize dialog
%% -------------------
do_finalize_dialog(State) ->
    Now = os:timestamp(),
    #state_forking{b_act=BAct,
                   b_fin=BFin,
                   a=#side{callid=ACallId}=ASide}=State,
    State1 = ?FORKING_UTILS:cancel_timer(State),
    ASide1 = ASide#side{finaltime=Now},
    %
    ?FORKING_UTILS:cancel_active_forks(BAct,[],State),
    BCanc = [ForkInfo#side_fork{res={cancelled},
                                finaltime=Now,
                                rule_timer_ref=undefined,
                                resp_timer_ref=undefined} || ForkInfo <- BAct],
    BFin1 = BFin++BCanc,
    State2 = State1#state_forking{a=ASide1,
                                  b_act=[],
                                  b_fin=BFin1,
                                  finaltime=Now},
    %
    ?CALLSTAT:call_state(free, a, State2),
    % stop media context
    {ok,State3} = ?B2BUA_MEDIA:stop(State2),
    % delete links call-id, dlg-id
    ?DLG_STORE:unlink_dlg(ACallId),
    unlink_forks(BFin1),
    State3.

%% -------------------
%% Unlink forks' callids
%% -------------------
unlink_forks(Forks) ->
    lists:foldl(fun(#side_fork{callid=FCallId}, Acc) ->
                        case lists:member(FCallId, Acc) of
                            false -> ?DLG_STORE:unlink_dlg(FCallId), [FCallId|Acc];
                            true -> Acc
                        end end, [], Forks).

%% -------------------
%% Fin with mgc error
%% -------------------
error_mgc(Reason, State) ->
    error_mgc(Reason, State, ?InternalError("B2B.F. MGC error")).
error_mgc(Reason, State, SipReply) ->
    d(State, "MGC error: ~120p", [Reason]),
    StopReason = ?B2BUA_UTILS:reason_error([forking, media], <<>>, Reason),
    State1 = State#state_forking{stopreason=StopReason},
    do_final(SipReply, State1).

% ---------------------
% Finalized dialog and switch fsm to stopping state
%% -------------------
do_final(SipReply, State) ->
    State1 = send_response_to_caller(SipReply, State),
    do_final(State1).

do_final(State) ->
    State1 = do_finalize_dialog(State),
    return_stopping(State1).

% ---------------------
% Switches to stopping state
%% -------------------
return_stopping(State) ->
    ?DIALOG:finish(State).

%% ====================================================================
%% Internal functions
%% ====================================================================

% @private ----
d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    #state_forking{dialogid=DlgId}=State,
    ?LOGSIP("B2B fsm ~p '~p':" ++ Fmt, [DlgId,?CURRSTATE] ++ Args).
