%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Presents 'refering' state of B2BUA Dialog FSM
%%%       Acts forward of REFER sip message.
%%%        Ends with final 4xx,5xx,6xx answer, error, reinvite or timeout.

-module(r_sip_b2bua_fsm_refering).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([enter_by_refer/2,
         refer_timeout/2,
         reinvite_while_transfering/2,
         refer_while_transfering/2,
         sip_bye/2,
         uac_response/2,
         uas_dialog_response/2,
         call_terminate/2,
         stop_external/2,
         test/1]).

-export([forward_notify/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_mgc.hrl").
-include("../include/r_sip_headers.hrl").

-define(CURRSTATE, 'refering').

-define(STAGE_TRYING, 'trying').
-define(STAGE_WAITING, 'waiting').

%% ====================================================================
%% API functions
%% ====================================================================

% ----
enter_by_refer([_CallId, _Req]=Args, State) ->
    d(State, "enter_by_refer, CallId=~120p", [_CallId]),
    do_start_refer(Args, State).

% ----
stop_external(Reason, State) ->
    d(State, "stop_external(~p)", [Reason]),
    State1 = cancel_timeout_timer(State),
    State2 = send_cancel_response(State1),
    ?DIALOG:stop_external(Reason, State2#state_dlg{refer=undefined}).

% ----
test(State) ->
    d(State, "test"),
    {next_state, ?CURRSTATE, State}.

% ----
refer_timeout(Args, State) ->
    do_on_timeout(Args, State).

% ----
reinvite_while_transfering([_CallId, Req]=Args, State) ->
    #state_dlg{refer=#refer{stage=ReferStage}}=State,
    d(State, "reinvite while refering, CallId=~120p", [_CallId]),
    case ReferStage of
        ?STAGE_TRYING ->
            %State1 = cancel_timeout_timer(State),
            %State2 = send_cancel_response(487, State1),
            %?SESSIONCHANGING:enter_by_reinvite(Args, State2);
            send_response(491, Req),
            {next_state, ?CURRSTATE, State};
        ?STAGE_WAITING ->
            State1 = cancel_timeout_timer(State),
            ?SESSIONCHANGING:enter_by_reinvite(Args, State1)
    end.

% ----
refer_while_transfering([_CallId, Req]=Args, State) ->
    #state_dlg{refer=#refer{stage=ReferStage}}=State,
    d(State, "refer while refering, CallId=~120p", [_CallId]),
    case ReferStage of
        ?STAGE_TRYING ->
            send_response(491, Req),
            {next_state, ?CURRSTATE, State};
        ?STAGE_WAITING ->
            do_start_refer(Args, State)
    end.

% ----
sip_bye([_CallId, _Req]=Args, State) ->
    d(State, "bye, CallId=~120p", [_CallId]),
    State1 = cancel_timeout_timer(State),
    State2 = send_cancel_response(State1),
    ?DIALOG:sip_bye(Args, State2#state_dlg{refer=undefined}).

% ----
uac_response([_Resp]=P, State) ->
    #sipmsg{class={resp,SipCode,SipReason},
            cseq={CSeq,Method},
            call_id=CallId}=_Resp,
    d(State, "uac_response ~p:~p -> ~p ~p, CallId=~120p", [Method,CSeq,SipCode,SipReason,CallId]),
    do_on_uac_response(P, State).

% ----
uas_dialog_response([_Resp], State) ->
    {next_state, ?CURRSTATE, State}.

% ----
forward_notify(Req, YSide, State) ->
    do_forward_notify(Req, YSide, State).

% ----
call_terminate([Reason, _Call], State) ->
    #call{call_id=CallId}=_Call,
    d(State, "call_terminate, CallId=~120p, Reason=~120p", [CallId, Reason]),
    State1 = cancel_timeout_timer(State),
    StopReason = ?B2BUA_UTILS:reason_callterm(CallId),
    ?DIALOG:stop_error(StopReason, State1#state_dlg{refer=undefined}).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% =============================================================
%% Start refer
%% =============================================================

do_start_refer([CallId, Req]=P, State) ->
    case State of
        #state_dlg{a=#side{callid=CallId}=A, b=B} -> do_start_refer_1(P, 'b', {A,B}, State);
        #state_dlg{a=A, b=#side{callid=CallId}=B} -> do_start_refer_1(P, 'a', {B,A}, State);
        _ ->
            d(State, " Refer error. Unknown CallId detected ~120p", [CallId]),
            send_response(internal_error, Req),
            {next_state, ?DIALOG_STATE, State}
    end.

% ----
do_start_refer_1([CallId, Req], FwdSide, {X,Y}, #state_dlg{map=Map}=State) ->
    {ok,[XRefToV|_]} = nksip_request:header("refer-to", Req),
    [#uri{}=XRefTo|_] = ?U:parse_uris(XRefToV),
    %
    {ok,_Ev} = nksip_request:header("event", Req),
    YRefBy = ?REPLACES:build_referred_by(Req, State),
    case ?REPLACES:build_refer_to(XRefTo, Req, State) of
        {error, Reason} ->
            d(State, Reason, []),
            send_response(?InternalError(Reason), Req),
            {next_state, ?DIALOG_STATE, State};
        {block, Reason} when is_list(Reason) ->
            d(State, " Refer error. ReferTo cannot be defined (~120p) ~120p", [Reason, CallId]),
            send_response(?Forbidden(Reason), Req),
            {next_state, ?DIALOG_STATE, State};
        {forking} ->
            YRefBy1 = shrink_refby(YRefBy,Map),
            do_start_refer_2([CallId, Req], FwdSide, {XRefTo, YRefBy1}, {X,Y}, State);
        {direct} -> % obsolete?
            YRefBy1 = shrink_refby(YRefBy,Map),
            do_start_refer_2([CallId, Req], FwdSide, {XRefTo, YRefBy1}, {X,Y}, State);
        YRefTo ->
            YRefBy1 = shrink_refby(YRefBy,Map),
            do_start_refer_2([CallId, Req], FwdSide, {YRefTo, YRefBy1}, {X,Y}, State)
    end.

%% @private
shrink_refby(YRefBy,Map) ->
    EO = [{<<"r-leg">>,<<"done">>} | YRefBy#uri.ext_opts],
    % YRefBy#uri{disp= <<>>,ext_opts=EO}}
    SrvIdx = ?U:get_current_srv_textcode(),
    DlgNum = maps:get(dlgnum,Map),
    ?ENVSTORE:store_t({referred_by_info,DlgNum},EO,180000),
    YRefBy#uri{disp= <<>>, ext_opts=[], opts=[{<<"r-key">>,<<SrvIdx/binary,"-",DlgNum/binary>>}]}.

%% % @private #184(k)
%% stop_melody(State) ->
%%     case ?B2BUA_MEDIA:stop_wait_melody(State) of
%%         {ok,State1} -> State1;
%%         {error,_} -> State
%%     end.

%
do_start_refer_2([_CallId, Req], FwdSide, {YRefTo, YRefBy}, {X,Y}, State) ->
    %% d(State, " Refer-To=~p, Referred-By=~p", [YRefTo, YRefBy]),
    #sipmsg{from={XRUri,_}}=Req,
    FromOpts = case FwdSide of
                   'b' ->
                       YSide1 = Y,
                       [];
                   'a' ->
                       YLFromUri = ?U:update_uri_display(?U:clear_uri(XRUri)),
                       YSide1 = Y#side{last_remote_party=YLFromUri}, % @remoteparty
                       %% from's domain is the domain of leg
                       %YDomain = (Y#side.localuri)#uri.domain,
                       %YLFromUri1 = YLFromUri#uri{domain=YDomain},
                       YLFromUri1 = YLFromUri,
                       [{from, YLFromUri1}]
               end,
    %
    ?FSM_EVENT:refer(State, #{refer_to=>?U:make_aor(YRefTo),
                              referred_by=>?U:make_aor(YRefBy),
                              referring=>?U:make_aor(Y#side.remoteuri)}),
    %
    YReferOpts = [{refer_to, YRefTo},
                  {add, {?ReferredBy, ?U:unparse_uri(YRefBy)}},
                  {event, <<"refer">>},
                  user_agent
                 | FromOpts],
    %
    case send_refer_offer(YReferOpts, Y, State) of
        error ->
            send_response(?InternalError("B2B. Send offer error"), Req),
            d(State, " -> switch to '~p'", [?DIALOG_STATE]),
            {next_state, ?DIALOG_STATE, State};
        {ok,YReqHandle} ->
            {ok,XReqHandle} = nksip_request:get_handle(Req),
            StateRefer = #refer{xreq=Req,
                                timestart=os:timestamp(),
                                stage=?STAGE_TRYING,
                                timer_ref=erlang:send_after(?REFER_TRY_TIMEOUT, self(), {refer_timeout, [?STAGE_TRYING, Y#side.callid,Y#side.localtag]}),
                                xrhandle=XReqHandle,
                                xcallid=X#side.callid,
                                xltag=X#side.localtag,
                                yrhandle=YReqHandle,
                                ycallid=Y#side.callid,
                                yltag=Y#side.localtag},
            State1 = State#state_dlg{refer=StateRefer},
            State2 = case FwdSide of
                'a' -> State1#state_dlg{a=YSide1};
                'b' -> State1#state_dlg{b=YSide1}
            end,
            %
            d(State2, " -> switch to '~p'", [?CURRSTATE]),
            {next_state, ?CURRSTATE, State2}
    end.

% ----
send_refer_offer(ReferOpts, Y, State) ->
    #side{dhandle=DlgHandle,
          localtag=LTag}=Y,
    case catch nksip_uac:refer(DlgHandle, [async|ReferOpts]) of
        {'EXIT',Err} ->
            d(State, "send_refer_offer to LTag=~p, Caught error=~120p", [LTag, Err]),
            error;
        {error,_R}=Err ->
            d(State, "send_refer_offer to LTag=~p, Error=~120p", [LTag, Err]),
            error;
        {async,ReqHandle} ->
            d(State, "send_refer_offer to LTag=~p", [LTag]),
            {ok,ReqHandle}
    end.

%% =============================================================
%% Handling UAC responses
%% =============================================================

% ----
do_on_timeout([?STAGE_TRYING, CallId, YLTag], State) ->
    #state_dlg{refer=#refer{xreq=XReq}}=State,
    d(State, "timeout '~p' YLTag=~120p, CallId=~120p", [?STAGE_TRYING, YLTag, CallId]),
    send_response(?ReplyOpts(408, "Timeout", "B2B.R. forward timeout"), XReq),
    StopReason = ?B2BUA_UTILS:reason_timeout([refer, trying], CallId, <<"Refer trying timeout">>),
    ?DIALOG:stop_error(StopReason, State#state_dlg{refer=undefined});
do_on_timeout([?STAGE_WAITING, CallId, YLTag], State) ->
    d(State, "timeout '~p' YLTag=~120p, CallId=~120p", [?STAGE_WAITING, YLTag, CallId]),
    StopReason = ?B2BUA_UTILS:reason_timeout([refer, waiting], CallId, <<"Refer waiting timeout">>),
    ?DIALOG:stop_error(StopReason, State#state_dlg{refer=undefined}).

% -----
do_on_uac_response([#sipmsg{cseq={_,'REFER'}, from={_,Tag}}=Resp]=P, #state_dlg{refer=#refer{yltag=Tag}}=State) ->
    #sipmsg{class={resp,SipCode,_}, from={_,BLTag}}=Resp,
    d(State, "uac_response('REFER') ~p from ~p", [SipCode, BLTag]),
    on_refer_response(P, State);

do_on_uac_response([#sipmsg{cseq={_,Method}}=_Resp], State) ->
    d(State, "uac_response(~p) skipped", [Method]),
    {next_state, ?CURRSTATE, State}.

% -----
on_refer_response([#sipmsg{class={resp,SipCode,_}}=_Resp]=P, State) when SipCode >= 100, SipCode < 200 ->
    on_refer_response_1xx(P,State);

on_refer_response([#sipmsg{class={resp,SipCode,_}}=_Resp]=P, State) when SipCode >= 200, SipCode < 300 ->
    on_refer_response_2xx(P,State);

on_refer_response([#sipmsg{class={resp,SipCode,_}}=_Resp]=P, State) when SipCode >= 300, SipCode < 400 ->
    on_refer_response_error(P, State);

on_refer_response([#sipmsg{class={resp,SipCode,_}}=_Resp]=P, State) when SipCode >= 400, SipCode < 700 ->
    on_refer_response_4xx_5xx_6xx(P,State);

on_refer_response([_Resp]=P, State) ->
    on_refer_response_error(P, State).

%% ===================================
%% Negative UAC response
%% ===================================

on_refer_response_error([#sipmsg{call_id=CallId}=_Resp], State) ->
    State1 = cancel_timeout_timer(State),
    #state_dlg{refer=#refer{xreq=XReq}}=State1,
    send_response(?ReplyOpts(500, "Opposite Invalid Response", "B2B.R. Forward error"), XReq),
    StopReason = ?B2BUA_UTILS:reason_error([refer, response], CallId, <<"Refer unexpected response code">>),
    ?DIALOG:stop_error(StopReason, State1#state_dlg{refer=undefined}).

on_refer_response_4xx_5xx_6xx([#sipmsg{class={resp,SipCode,SipReason}}=_Resp], State) ->
    State1 = cancel_timeout_timer(State),
    #state_dlg{refer=#refer{xreq=XReq}}=State1,
    SipCode1 = SipCode, % @todo map
    SipReason1 = SipReason,
    AnswerOpts = [{reason_phrase, SipReason1},
                  user_agent],
    send_response({SipCode1,AnswerOpts}, XReq),
    {next_state, ?DIALOG_STATE, State1#state_dlg{refer=undefined}}.

%% ===================================
%% Positive UAC response
%% ===================================

on_refer_response_1xx([#sipmsg{class={resp,SipCode,SipReason}}=_Resp], State) ->
    #state_dlg{refer=#refer{xreq=XReq}}=State,
    SipCode1 = SipCode,
    SipReason1 = SipReason,
    AnswerOpts = [{reason_phrase, SipReason1},
                  user_agent],
    send_response({SipCode1,AnswerOpts}, XReq),
    {next_state, ?CURRSTATE, State}.

on_refer_response_2xx([#sipmsg{class={resp,SipCode,SipReason}}=_Resp], State) ->
    State1 = cancel_timeout_timer(State),
    %
    #state_dlg{refer=#refer{xreq=XReq}}=State1,
    SipCode1 = SipCode,
    SipReason1 = SipReason,
    AnswerOpts = [{reason_phrase, SipReason1},
                  {expires, ?REFER_WAIT_TIMEOUT div 1000},
                  user_agent],
    send_response({SipCode1,AnswerOpts}, XReq),
    %
    #state_dlg{refer=#refer{ycallid=YCallId, yltag=YLTag}=Refer}=State1,
    State2 = State1#state_dlg{refer=Refer#refer{stage=?STAGE_WAITING,
                                                timer_ref=erlang:send_after(?REFER_WAIT_TIMEOUT, self(), {refer_timeout, [?STAGE_WAITING, YCallId,YLTag]})}},
    %% State1 = State,
    {next_state, ?CURRSTATE, State2}.

%% =============================================================
%% Services
%% =============================================================

do_forward_notify(Req, YSide, StateData) ->
    #state_dlg{refer=#refer{xreq=#sipmsg{cseq={ReferCSeq,_}},
                            referring_dlg_info=RefDlgInfo}}=StateData,
    #sipmsg{content_type=ContentType,
            body=Body}=Req,
    SubS = case nksip_subscription_lib:state(Req) of
               invalid -> active;
               T -> T
           end,
    Opts = [{content_type, ContentType},
            {event, {<<"refer">>,[{<<"id">>,?EU:to_binary(ReferCSeq)}]}},
            {subscription_state, SubS},
            {body, Body},
            {add,{?ReferringCallId,maps:get(acallid,?EU:unempty(RefDlgInfo,#{}),<<>>)}},
            user_agent],
    #side{dhandle=YDlgHandle}=YSide,
    catch nksip_uac:notify(YDlgHandle, [async|Opts]),
    send_response({200,[]}, Req),
    StateData.

%% =============================================================
%% Services
%% =============================================================

% ---
send_response(SipReply, Req) ->
    {ok,Handle} = nksip_request:get_handle(Req),
    %nksip_request:reply(SipReply, Handle)
    ?U:send_sip_reply(fun() -> nksip_request:reply(SipReply, Handle) end).

% ---
send_cancel_response(State) ->
    send_cancel_response(603, State).

send_cancel_response(SipCode, State) ->
    case State of
        #state_dlg{refer=#refer{stage=?STAGE_TRYING, xreq=XReq}=Refer} ->
            send_response({SipCode,[user_agent]}, XReq),
            State#state_dlg{refer=Refer#refer{stage=?STAGE_WAITING}};
        _ ->
            State
    end.

% ---
cancel_timeout_timer(State) ->
    case State of
        #state_dlg{refer=#refer{timer_ref=TimerRef}=Refer} when TimerRef /= undefined ->
            erlang:cancel_timer(TimerRef),
            State#state_dlg{refer=Refer#refer{timer_ref=undefined}};
        _ -> State
    end.

% -----
d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    #state_dlg{dialogid=DlgId}=State,
    ?LOGSIP("B2B fsm ~p '~p':" ++ Fmt, [DlgId,?CURRSTATE] ++ Args).
