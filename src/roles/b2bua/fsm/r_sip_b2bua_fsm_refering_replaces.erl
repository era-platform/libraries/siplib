%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc

-module(r_sip_b2bua_fsm_refering_replaces).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([build_owner/1,
         %
         build_referred_by/2,
         build_refer_to/3,
         rpc_modify_refer_replaces/1,
         modify_refer_replaces/2,
         %
         link_invite_to_replaced/2,
         rpc_link_replacing_invite/1,
         link_replacing_invite/2,
         %
         link_invite_to_referred/2,
         rpc_link_referring_invite/1,
         link_referring_invite/2,
         %
         find_replaced_dlg/2,
         rpc_get_replaced_dlg/1,
         get_replaced_dlg/2,
         %
         find_replaced_opts/2,
         rpc_get_replaced_opts/1,
         get_replaced_opts/2,
         %
         find_referred_opts/2,
         rpc_get_referred_opts/1,
         get_referred_opts/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_mgc.hrl").
-include("../include/r_sip_headers.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% -----------------
%% facade fun
%% build owner dlg description structure
%% -----------------
%% by state when dialog
build_owner({state,#state_dlg{}=State}) ->
    #state_dlg{dialogid=DlgId, dialogidhash=DlgIdHash,
               acallid=ACallId, acallidhash=ACallIdHash,
               invite_id=IID, invite_ts=ITS,
               a=#side{callid=ACallId}}=State,
    {{DlgId,DlgIdHash},{ACallId,ACallIdHash},{IID,ITS}};
%% by state when forking
build_owner({state,#state_forking{}=State}) ->
    #state_forking{dialogid=DlgId, dialogidhash=DlgIdHash,
                      acallid=ACallId, acallidhash=ACallIdHash,
                      invite_id=IID, invite_ts=ITS,
                   a=#side{callid=ACallId}}=State,
    {{DlgId,DlgIdHash},{ACallId,ACallIdHash},{IID,ITS}};
%% by router context
build_owner({ctx,Ctx}) ->
    [ACallId,ACallIdHash] = ?EU:maps_get([acallid,acallidhash], Ctx),
    InviteData = {maps:get(invite_id, Ctx), maps:get(invite_ts, Ctx)},
    {{ACallId,ACallIdHash},InviteData}.

%%% ==============================================
%%% build_referred_by, build_refer_to
%%% ==============================================

%% -----------------
%% general facade function
%% when refer received. modifies referred-by uri
%% -----------------
build_referred_by(Req, State) ->
    #sipmsg{call_id=CallId}=Req,
    Side = case State of
               #state_dlg{a=#side{callid=CallId}} -> ACallId = CallId, a;
               #state_dlg{b=#side{callid=CallId}, a=#side{callid=ACallId}} -> b
           end,
    Uri1 = case nksip_request:header("referred-by", Req) of
               {ok,[RefH|_]} -> [Uri|_] = ?U:parse_uris(RefH), Uri;
               _ -> find_opposite_referred_by(Req, State)
           end,
    % TODO RefByOpts set
    Uri1#uri{ext_opts=[{<<"r-dlgid">>,State#state_dlg.dialogid},
                       {<<"r-acallid">>,?EU:urlencode(ACallId)},
                       {<<"r-iid">>,State#state_dlg.invite_id},
                       {<<"r-its">>,State#state_dlg.invite_ts},
                       {<<"r-side">>,?EU:to_binary(Side)}]}.

%% -----------------
%% @private build_referred_by root
%% -----------------
find_opposite_referred_by(Req, State) ->
    #sipmsg{ruri=#uri{user=U,domain=D}}=Req,
    case State of
        #state_dlg{a=#side{localcontact=#uri{user=U,domain=D}}, b=#side{localuri=OUri}} -> OUri#uri{opts=[], ext_opts=[], headers=[]};
        #state_dlg{b=#side{localcontact=#uri{user=U,domain=D}}, a=#side{localuri=OUri}} -> OUri#uri{opts=[], ext_opts=[], headers=[]};
        #state_dlg{a=#side{localcontact=#uri{user=U1,domain=D1}}, b=#side{localcontact=#uri{user=U2,domain=D2}}} ->
            d(State, "ReferredBy error. A,B: ~p@~p, ~p@~p", [U1,D1,U2,D2]),
            #uri{scheme=sip, user= <<"unknown">>, domain= <<"unknown">>}
    end.

%% -----------------
%% general facade function
%% when refer received. modifies refer-to uri
%% -----------------
build_refer_to(XRefTo, Req, State) ->
    find_opposite_refer_to(XRefTo, Req, State).

%% -----------------
%% @private build_refer_to
%% -----------------
find_opposite_refer_to(XRefTo, Req, State) ->
    #uri{headers=XRefToH}=XRefTo,
    case lists:keyfind(?ReplacesLow, 1, XRefToH) of
        {_H,V} ->
            XReplaces = ?EU:urldecode(?EU:to_list(V)),
            RepMap = ?U:parse_replaces(XReplaces),
            DlgCallIds = case State of
                             #state_dlg{a=#side{callid=ACallId},b=#side{callid=BCallId}} -> [ACallId,BCallId];
                             #state_forking{a=#side{callid=ACallId},b=Forks} -> [ACallId | [BCallId || #side_fork{callid=BCallId} <- Forks]]
                         end,
            CallId = maps:get(callid,RepMap),
            case lists:member(CallId,DlgCallIds) of
                true -> {error,"B2B. Refer replaces loop"};
                false ->
                    ?STAT_FACADE:link_replaced(CallId, Req#sipmsg.call_id), % @stattrace
                    case catch do_find_b2bua_callid_refer_replaces(RepMap, XRefTo, State) of
                        {'EXIT',_} -> {error,"B2B. Refer replaces translate error"};
                        {block, _Reason}=B -> B;
                        {forking}=R -> R;
                        {error, _Reason} -> XRefTo;
                        {ok, {OUri, ORep}} ->
                            OUri#uri{disp= <<>>, headers=[{?Replaces, ORep}], opts=[], ext_opts=[]}
                    end
            end;
        false -> XRefTo
    end.

%% -----------------
%% @private build_refer_to
%% -----------------
do_find_b2bua_callid_refer_replaces(RepMap, XRefTo, State) ->
    CallId = maps:get(callid,RepMap),
    Owner = build_owner({state,State}),
    ReqArgs = [CallId, Owner, RepMap],
    case ?DLG_STORE:pull_dlg(CallId) of
        false ->
            case find_b2bua_cross_srv(RepMap) of
                {true, SrvIdx} ->
                    do_get_refer_replaces_cross_srv(SrvIdx, ReqArgs, State);
                false ->
                    d(State, "modify_replaces -> auto ~120p", [CallId]),
                    make_auto_replaces_opposite(RepMap, XRefTo)
            end;
        DlgX -> ?B2BUA_DLG:modify_refer_replaces(DlgX, ReqArgs, 5000)
    end.

%% -----------------
%% @private build_refer_to
%% -----------------
do_get_refer_replaces_cross_srv(SrvIdx, ReqArgs, State) ->
    Request = {?MODULE, rpc_modify_refer_replaces, [ReqArgs]},
    Res = case ?CFG:get_sipserver_by_index(SrvIdx) of
              undefined ->
                  d(State, "modify_replaces -> undefined B2BUA id=~120p", [SrvIdx]),
                  undefined;
              {_Site, _Node}=Dest ->
                  d(State, "modify_replaces -> goto ~120p", [Dest]),
                  ?ENVCROSS:call_node(Dest, Request, undefined2)
          end,
    case Res of
        undefined -> {error, "Server not found"};
        undefined2 -> {error, "Server call failure"};
        _ -> Res
    end.

%% -----------------
%% @private build_refer_to
%% When detects that another b2bua is handling call, then we can automatically change call-id (but not tags)
%% Does not work on Yealink. It response 481 Call Leg/Transaction Does Not Exist when no tags in referred invite
%% -----------------
make_auto_replaces_opposite(RepMap, XRefTo) ->
    [CallId,FTag,TTag] = ?EU:maps_get([callid,fromtag,totag],RepMap),
    case {CallId,FTag,TTag} of
        {<<"rB2-", Rest/bitstring>>,_,_} ->
            {ok, {XRefTo#uri{headers=[],opts=[],ext_opts=[]}, ?EU:to_binary(?EU:urlencode(?EU:to_list(Rest)))}};
        {CallId,<<"rB2-",_/bitstring>>,_} ->
            {ok, {XRefTo#uri{headers=[],opts=[],ext_opts=[]}, ?EU:to_binary(?EU:urlencode(?EU:to_list(<<"rB2-", CallId/bitstring>>)))}};
        {CallId,_,<<"rB2-",_/bitstring>>} ->
            {ok, {XRefTo#uri{headers=[],opts=[],ext_opts=[]}, ?EU:to_binary(?EU:urlencode(?EU:to_list(<<"rB2-", CallId/bitstring>>)))}};
        _ ->
            {error, "Unknown CallId and Tags"}
    end.

%% -----------------
%% build_refer_to
%% rpc call from other b2bua server to modify replaces by opposite side (in remote proc)
%% -----------------
rpc_modify_refer_replaces([XCallId,_Owner,_RepMap]=Args) ->
    ?ENV:set_group_leader(),
    ?LOGSIP("B2B fsm : rpc_modify_refer_replaces, CallId=~120p", [XCallId]),
    case ?DLG_STORE:pull_dlg(XCallId) of
        false -> {error, "Unknown CallId"};
        DlgX -> ?B2BUA_DLG:modify_refer_replaces(DlgX, Args, 5000)
    end.

%% -----------------
%% build_refer_to root
%% call from other b2bua server to modify replaces by opposite side (in dlg proc)
%% -----------------
%%  when dialog
modify_refer_replaces([XCallId, Owner, RepMap]=_Args, #state_dlg{}=State) ->
    YSide = case State of
                #state_dlg{a=#side{callid=XCallId}, b=Y} -> Y;
                #state_dlg{a=Y, b=#side{callid=XCallId}} -> Y;
                _ -> error
            end,
    case YSide of
        error ->
            d(State, "modify_refer_replaces, CallId=~120p, Ret=error", [XCallId]),
            {error, "CallId not found"};
        #side{remoteuri=YRUri,callid=YCallId,localtag=YLTag,remotetag=YRTag} ->
            d(State, "modify_refer_replaces, CallId=~120p, Ret=~120p", [XCallId, ?U:unparse_uri(YRUri#uri{headers=[{?Replaces, YCallId}],opts=[],ext_opts=[]})]),
            ?FSM_EVENT:replaces_refer(Owner, State),
            Replaces = ?U:build_replaces_header(#{callid => YCallId, fromtag => YLTag, totag => YRTag, opts => maps:get(opts,RepMap)}),
            {ok, {YRUri, ?EU:to_binary(?EU:urlencode(?EU:to_list(Replaces)))}}
    end;
%%  when forking
modify_refer_replaces([XCallId, Owner, _RepMap]=_Args, #state_forking{}=State) ->
    case State of
        % #184
        #state_forking{a=#side{callid=XCallId}} ->
            d(State, "modify_refer_replaces, CallId=~120p, Forking. Responsibility of invite router", [XCallId]),
            ?FSM_EVENT:replaces_refer(Owner, State),
            {forking};
        _ ->
            d(State, "modify_refer_replaces, CallId=~120p, Ret=error", [XCallId]),
            {error, "CallId not found"}
    end.

%%% ==============================================
%%% link_invite_to_replaced (A->C => B->C)
%%% ==============================================

%% -----------------
%% general facade fun
%% when replacing invite received by router, no state, only functional
%% -----------------
link_invite_to_replaced(RepMap, Owner) when is_map(RepMap) ->
    do_link_invite_to_replaced(RepMap, Owner).

%% -----------------
%% @private link_invite_to_replaced
%% -----------------
do_link_invite_to_replaced(RepMap, Owner) when is_map(RepMap) ->
    YCallId = maps:get(callid,RepMap),
    ReqArgs = [YCallId, Owner, RepMap],
    case ?DLG_STORE:pull_dlg(YCallId) of
        false ->
            case find_b2bua_cross_srv(RepMap) of
                {true, SrvIdx} -> do_link_invite_to_replaced_cross_srv(SrvIdx, ReqArgs);
                false -> {error, <<"Server not defined">>}
            end;
        DlgX -> ?B2BUA_DLG:link_replacing_invite(DlgX, ReqArgs)
    end.

%% -----------------
%% @private link_invite_to_replaced
%% -----------------
do_link_invite_to_replaced_cross_srv(SrvIdx, ReqArgs) ->
    Request = {?MODULE, rpc_link_replacing_invite, [ReqArgs]},
    case ?CFG:get_sipserver_by_index(SrvIdx) of
        undefined -> {error, <<"Server not found">>};
        {_Site, _Node}=Dest ->
              ?ENVCROSS:call_node(Dest, Request, undefined)
    end.

%% -----------------
%% link_invite_to_replaced
%% rpc call from other b2bua server to link  (in remote proc)
%% -----------------
rpc_link_replacing_invite([YCallId,_Owner,RepMap]=Args) when is_binary(YCallId), is_map(RepMap) ->
    ?ENV:set_group_leader(),
    ?LOGSIP("B2B fsm : rpc_link_replacing_route, CallId=~120p", [YCallId]),
    case ?DLG_STORE:pull_dlg(YCallId) of
        false -> {error, "Unknown CallId"};
        DlgX -> ?B2BUA_DLG:link_replacing_invite(DlgX, Args)
    end.

%% -----------------
%% link_invite_to_replaced root
%% call from other b2bua server to get route address for replacing invite (in dlg proc)
%% -----------------
link_replacing_invite([YCallId,Owner,_RepMap]=_Args, #state_dlg{}=State) when is_binary(YCallId), is_map(_RepMap) ->
     case State of
        #state_dlg{a=#side{callid=YCallId}} -> ?FSM_EVENT:replaces_invite(Owner, State), ok;
        #state_dlg{b=#side{callid=YCallId}} -> ?FSM_EVENT:replaces_invite(Owner, State), ok;
        _ -> {error, "CallId not found"}
    end;
link_replacing_invite([YCallId,Owner,_RepMap]=_Args, #state_forking{}=State) when is_binary(YCallId), is_map(_RepMap) ->
    case State of
        #state_forking{a=#side{callid=YCallId}} -> ?FSM_EVENT:replaces_invite(Owner, State), ok;
        #state_forking{b_act=Forks} when is_list(Forks) ->
            case lists:keyfind(YCallId,#side_fork.callid,Forks) of
                #side_fork{} -> ?FSM_EVENT:replaces_invite(Owner, State), ok;
                false -> {error, "CallId not found"}
            end
    end.

%%% ==============================================
%%% link_invite_to_referred (A->C => A->B)
%%% ==============================================

%% -----------------
%% general facade fun
%% when referring invite received by router, no state, only functional
%% -----------------
link_invite_to_referred(RefMap, Owner) when is_map(RefMap) ->
    do_link_invite_to_referred(RefMap, Owner).

%% -----------------
%% @private link_invite_to_referred
%% -----------------
do_link_invite_to_referred(RefMap, Owner) when is_map(RefMap) ->
    YCallId = maps:get(callid,RefMap),
    DlgId = maps:get(dlgid,RefMap),
    ReqArgs = [YCallId, Owner, RefMap],
    case ?DLG_STORE:pull_dlg(YCallId) of
        false ->
            case find_b2bua_cross_srv(DlgId) of
                {true, SrvIdx} -> do_link_invite_to_referred_cross_srv(SrvIdx, ReqArgs);
                false -> {error, <<"Server not defined">>}
            end;
        DlgX -> ?B2BUA_DLG:link_referring_invite(DlgX, ReqArgs)
    end.

%% -----------------
%% @private link_invite_to_referred
%% -----------------
do_link_invite_to_referred_cross_srv(SrvIdx, ReqArgs) ->
    Request = {?MODULE, rpc_link_referring_invite, [ReqArgs]},
    case ?CFG:get_sipserver_by_index(SrvIdx) of
        undefined -> {error, <<"Server not found">>};
        {_Site, _Node}=Dest ->
              ?ENVCROSS:call_node(Dest, Request, undefined)
    end.

%% -----------------
%% link_invite_to_referred
%% rpc call from other b2bua server to link  (in remote proc)
%% -----------------
rpc_link_referring_invite([YCallId,_Owner,RefMap]=Args) when is_binary(YCallId), is_map(RefMap) ->
    ?ENV:set_group_leader(),
    ?LOGSIP("B2B fsm : rpc_link_referring_route, CallId=~120p", [YCallId]),
    case ?DLG_STORE:pull_dlg(YCallId) of
        false -> {error, "Unknown CallId"};
        DlgX -> ?B2BUA_DLG:link_referring_invite(DlgX, Args)
    end.

%% -----------------
%% link_invite_to_referred root
%% Call from other b2bua server to get route address for referring invite (in dlg proc)
%% Only 'dialog' state could be referred.
%% Saves referring dialog info into #state_dlg{refer=#refer{...}}. It is saved and is not cleared by itself.
%% -----------------
link_referring_invite([YCallId,Owner,_RefMap]=_Args, #state_dlg{refer=Refer}=State) when is_binary(YCallId), is_map(_RefMap) ->
    F = fun() ->
            {{ACallId,ACallIdHash},{IID,ITS}} = Owner,
            RefDlgInfo = #{acallid => ACallId,
                           acallid_hash => ACallIdHash,
                           iid => IID,
                           its => ITS},
            Refer#refer{referring_dlg_info=RefDlgInfo}
        end,
    case State of
        #state_dlg{a=#side{callid=YCallId}} ->
            ?FSM_EVENT:referring_invite(Owner, State),
            {ok,ok,State#state_dlg{refer=F()}};
        #state_dlg{b=#side{callid=YCallId}} ->
            ?FSM_EVENT:referring_invite(Owner, State),
            {ok,ok,State#state_dlg{refer=F()}};
        _ -> {error, "CallId not found"}
    end.

%%% ==============================================
%%% find_replaced_dlg
%%% ==============================================

%% -----------------
%% general facade fun
%% when replacing invite needs to take dlgid,acallid of replaced dialog
%% -----------------
find_replaced_dlg(RepMap, Owner) when is_map(RepMap) ->
    do_find_b2bua_callid_replaced_dlg(RepMap, Owner).

%% -----------------
%% @private find_replaced_dlg
%% -----------------
do_find_b2bua_callid_replaced_dlg(RepMap, ZCallId) ->
    YCallId = maps:get(callid,RepMap),
    ReqArgs = [YCallId, ZCallId],
    case ?DLG_STORE:pull_dlg(YCallId) of
        false ->
            case find_b2bua_cross_srv(RepMap) of
                {true, SrvIdx} -> do_find_replaced_dlg_cross_srv(SrvIdx, ReqArgs);
                false -> {error, <<"Server not defined">>}
            end;
        DlgX -> ?B2BUA_DLG:get_replaced_dlg(DlgX, ReqArgs)
    end.

%% -----------------
%% @private find_replaced_dlg
%% -----------------
do_find_replaced_dlg_cross_srv(SrvIdx, ReqArgs) ->
    Request = {?MODULE, rpc_get_replaced_dlg, [ReqArgs]},
    case ?CFG:get_sipserver_by_index(SrvIdx) of
        undefined -> {error, <<"Server not found">>};
        {_Site, _Node}=Dest ->
              ?ENVCROSS:call_node(Dest, Request, undefined)
    end.

%% -----------------
%% find_replaced_dlg
%% rpc call from other b2bua server to get dlgid,acallid of replaced dialog (in remote proc)
%% -----------------
rpc_get_replaced_dlg([YCallId,_Owner]=Args) when is_binary(YCallId) ->
    ?ENV:set_group_leader(),
    ?LOGSIP("B2B fsm : rpc_get_replaced_dlg, CallId=~120p", [YCallId]),
    case ?DLG_STORE:pull_dlg(YCallId) of
        false -> {error, "Unknown CallId"};
        DlgX -> ?B2BUA_DLG:get_replaced_dlg(DlgX, Args)
    end.

get_replaced_dlg([YCallId,_Owner]=_Args, #state_dlg{}=State) when is_binary(YCallId) ->
    case State of
        #state_dlg{a=#side{callid=YCallId}} -> {ok,build_owner({state,State})};
        #state_dlg{b=#side{callid=YCallId}} -> {ok,build_owner({state,State})};
        _ -> {error, "CallId not found"}
    end;
get_replaced_dlg([YCallId,_Owner]=_Args, #state_forking{}=State) when is_binary(YCallId) ->
    case State of
        #state_forking{a=#side{callid=YCallId}} -> {ok,build_owner({state,State})};
        #state_forking{b_act=Forks} when is_list(Forks) ->
            case lists:keyfind(YCallId,#side_fork.callid,Forks) of
                #side_fork{} -> {ok,build_owner({state,State})};
                false -> {error, "CallId not found"}
            end
    end.

%%% ==============================================
%%% find_replaced_opts
%%% ==============================================

%% -----------------
%% general facade fun
%% when replacing invite needs to take opts of YSide opts (for ext account)
%% -----------------
find_replaced_opts(RepMap, Owner) when is_map(RepMap) ->
    do_find_b2bua_callid_replaced_opts(RepMap, Owner).

%% -----------------
%% @private find_replaced_opts
%% -----------------
do_find_b2bua_callid_replaced_opts(RepMap, ZCallId) ->
    YCallId = maps:get(callid,RepMap),
    ReqArgs = [YCallId, ZCallId],
    case ?DLG_STORE:pull_dlg(YCallId) of
        false ->
            case find_b2bua_cross_srv(RepMap) of
                {true, SrvIdx} -> do_find_replaced_opts_cross_srv(SrvIdx, ReqArgs);
                false -> {error, <<"Server not defined">>}
            end;
        DlgX -> ?B2BUA_DLG:get_replaced_opts(DlgX, ReqArgs)
    end.

%% -----------------
%% @private find_replaced_opts
%% -----------------
do_find_replaced_opts_cross_srv(SrvIdx, ReqArgs) ->
    Request = {?MODULE, rpc_get_replaced_opts, [ReqArgs]},
    case ?CFG:get_sipserver_by_index(SrvIdx) of
        undefined -> {error, <<"Server not found">>};
        {_Site, _Node}=Dest ->
              ?ENVCROSS:call_node(Dest, Request, undefined)
    end.

%% -----------------
%% find_replaced_opts
%% rpc call from other b2bua server to get opts of YSide for replacing invite (in remote proc)
%% -----------------
rpc_get_replaced_opts([YCallId,_Owner]=Args) when is_binary(YCallId) ->
    ?ENV:set_group_leader(),
    ?LOGSIP("B2B fsm : rpc_get_replaced_opts, CallId=~120p", [YCallId]),
    case ?DLG_STORE:pull_dlg(YCallId) of
        false -> {error, "Unknown CallId"};
        DlgX -> ?B2BUA_DLG:get_replaced_opts(DlgX, Args)
    end.

%% -----------------
%% find_replaced_opts root
%% call from other b2bua server to get opts of YSide for replacing invite (in dlg proc)
%% -----------------
get_replaced_opts([YCallId,_Owner]=_Args, #state_dlg{}=State) when is_binary(YCallId) ->
     case State of
        #state_dlg{a=#side{callid=YCallId,opts=Opts}} -> {ok,Opts};
        #state_dlg{b=#side{callid=YCallId,opts=Opts}} -> {ok,Opts};
        _ -> {error, "CallId not found"}
    end;
get_replaced_opts([YCallId,_Owner]=_Args, #state_forking{}=State) when is_binary(YCallId) ->
    case State of
        #state_forking{a=#side{callid=YCallId,opts=Opts}} -> {ok,Opts};
        #state_forking{b_act=Forks} when is_list(Forks) ->
            case lists:keyfind(YCallId,#side_fork.callid,Forks) of
                #side_fork{opts=Opts} -> {ok,Opts};
                false -> {error, "CallId not found"}
            end
    end.

%%% ==============================================
%%% find_referred_opts
%%% ==============================================

%% -----------------
%% general facade fun
%% when referred invite needs to take opts of Referred Side opts (routing ctx)
%% -----------------
find_referred_opts({_DlgId,_ACallId}=P, Side) ->
    do_find_b2bua_callid_referred_opts(P, Side).

%% -----------------
%% @private find_referred_opts
%% -----------------
do_find_b2bua_callid_referred_opts({DlgId,ACallId}, Side) ->
    ReqArgs = [Side,ACallId],
    case ?DLG_STORE:pull_dlg(ACallId) of
        false ->
            case find_b2bua_cross_srv(DlgId) of
                {true, SrvIdx} -> do_find_referred_opts_cross_srv(SrvIdx, ReqArgs);
                false -> {error, <<"Server not defined">>}
            end;
        DlgX -> ?B2BUA_DLG:get_referred_opts(DlgX, ReqArgs)
    end.

%% -----------------
%% @private find_referred_opts
%% -----------------
do_find_referred_opts_cross_srv(SrvIdx, ReqArgs) ->
    Request = {?MODULE, rpc_get_referred_opts, [ReqArgs]},
    case ?CFG:get_sipserver_by_index(SrvIdx) of
        undefined -> {error, <<"Server not found">>};
        {_Site, _Node}=Dest ->
              ?ENVCROSS:call_node(Dest, Request, undefined)
    end.

%% -----------------
%% find_referred_opts
%% rpc call from other b2bua server to get opts of YSide for replacing invite (in remote proc)
%% -----------------
rpc_get_referred_opts([Side,ACallId]=Args) when is_atom(Side) ->
    ?ENV:set_group_leader(),
    ?LOGSIP("B2B fsm : rpc_get_referred_opts, Side=~120p, CallId=~120p", [Side,ACallId]),
    case ?DLG_STORE:pull_dlg(ACallId) of
        false -> {error, "Unknown CallId"};
        DlgX -> ?B2BUA_DLG:get_referred_opts(DlgX, Args)
    end.

%% -----------------
%% find_referred_opts root
%% call from other b2bua server to get opts of YSide for replacing invite (in dlg proc)
%% -----------------
get_referred_opts(['a',_]=_Args, #state_dlg{a=#side{opts=Opts}}=_State) -> {ok,Opts};
get_referred_opts(['b',_]=_Args, #state_dlg{b=#side{opts=Opts}}=_State) -> {ok,Opts};
get_referred_opts(_,_) -> {error, "Invalid request"}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -----------------
find_b2bua_cross_srv(RepMap) when is_map(RepMap) ->
    [CallId,FTag,TTag] = ?EU:maps_get([callid,fromtag,totag],RepMap),
    ?B2BUA_UTILS:extract_b2bua_server_index(CallId, FTag, TTag);
find_b2bua_cross_srv(DlgId) when is_binary(DlgId) ->
    ?B2BUA_UTILS:extract_b2bua_server_index(DlgId).

%% -----
%d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    ?LOGSIP("B2B fsm ~p '*':" ++ Fmt, [get_dialogid(State)] ++ Args).

%% @private
get_dialogid(#state_forking{dialogid=DlgId}) -> DlgId;
get_dialogid(#state_dlg{dialogid=DlgId}) -> DlgId.


