%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Anton Makarov <anton@mastermak.ru>, Peter Bukashin <tbotc@yandex.ru>
%%% @doc Sends reinvite to selected side,
%%%      waits for answer,
%%%      modifies media if changed.

-module(r_sip_b2bua_fsm_reinviting).
-author(['Anton Makarov <anton@mastermak.ru>','Peter Bukashin <tbotc@yandex.ru>']).

-export([start/2,
         reinviting_timeout/2,
         sip_bye/2,
         uac_response/2,
         uas_dialog_response/2,
         call_terminate/2,
         stop_external/2,
         test/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_mgc.hrl").

-define(STORE, ?ENVSTORE).

-define(CURRSTATE, 'reinviting').
-define(TOTAL, 'total').
-define(RETRY, 'retry_side').
-define(KEY, 'reinviting').

%% ====================================================================
%% API functions
%% ====================================================================

% ----
start(State, Args) ->
    d(State, "start reinviting Args=~p", [Args]),
    do_start({State, Args}).

% ----
stop_external(Reason, State) ->
    d(State, "stop_external(~p)", [Reason]),
    go_abort(stop_external, Reason, State).

% ----
test(State) ->
    d(State, "test"),
    {next_state, ?CURRSTATE, State}.

% ----
reinviting_timeout([?TOTAL], State) ->
    do_on_timeout(State);
reinviting_timeout([?RETRY, SideIdx], #state_dlg{lstate=#{state:=?KEY}=LState}=State) ->
    TotalTimer = maps:get(total_timer_ref, LState),
    case TotalTimer of
        undefined ->
            {next_state, ?CURRSTATE, State};
        _ ->
            do_retry_side(SideIdx, State)
    end.

% ----
sip_bye([_CallId, _Req]=Args, State) ->
    d(State, "bye, CallId=~120p", [_CallId]),
    go_abort(sip_bye, Args, State).

% ----
uac_response([_Resp]=Args, State) ->
    #sipmsg{class={resp,SipCode,SipReason},
            cseq={CSeq,Method},
            call_id=CallId}=_Resp,
    d(State, "uac_response ~p:~p -> ~p ~p, CallId=~120p", [Method,CSeq,SipCode,SipReason,CallId]),
    do_on_uac_response(Args, State).

% ----
uas_dialog_response([_Resp], State) ->
    {next_state, ?CURRSTATE, State}.

% ----
call_terminate([Reason, _Call], State) ->
    #call{call_id=CallId}=_Call,
    d(State, "call_terminate, CallId=~120p, Reason=~120p", [CallId, Reason]),
    StopReason = ?B2BUA_UTILS:reason_callterm(CallId),
    go_error(StopReason, State).

%% ====================================================================
%% Internal functions
%% ====================================================================

do_start({State,Args}) ->
    case needs_reinvite({State,Args}) of
        {true, Args1} ->
            start_reinvite(State, Args1);
        false ->
            d(State, " -> switch to '~p'", [?DIALOG_STATE]),% !
            ?DIALOG:start(State)
    end.

% ----
needs_reinvite({State, Args}) ->
    {_, SidesL} = lists:keyfind(sides, 1, Args),
    IvrConfAddrs = get_ivr_conf_addrs(),
    SidesL1 = lists:filter(fun(ZIdx) ->
                                    ZSide = case ZIdx of
                                                a -> #state_dlg{a=Side}=State, Side;
                                                b -> #state_dlg{b=Side}=State, Side
                                            end,
                                    #side{remotecontacts=RContacts}=ZSide,
                                    not lists:foldl(fun (_, true) -> true;
                                                        (RCont, _) ->
                                                            #uri{domain=Domain}=RCont,
                                                            lists:member(Domain, IvrConfAddrs)
                                                    end, false, RContacts)
                           end, SidesL),
    case length(SidesL1) of
        0 -> false;
        _ -> {true, #{sidelist => SidesL1}}
    end.

get_ivr_conf_addrs() ->
    Fbuild = fun() -> ivr_conf_addrs() end,
    FunStore = fun(undefined) -> Fbuild(); (R) -> R end,
    FunReply = fun(_,R) -> R end,
    ?STORE:func_t('ivr_conf_addrs', 300000, FunStore,FunReply).

ivr_conf_addrs() ->
    IVR = ?CFG:get_all_ivr(),
    IvrAddrs = ivr_conf_tuples_to_addrs(IVR),
    CONF = ?CFG:get_all_conf(),
    ConfAddrs = ivr_conf_tuples_to_addrs(CONF),
    IvrAddrs ++ ConfAddrs.

ivr_conf_tuples_to_addrs(Tuples) ->
    Addrs = lists:map(fun({_Node, AddrPorts}) ->
            lists:map(fun(AddrPort) ->
                lists:nth(1, binary:split(AddrPort, <<":">>))
            end, AddrPorts)
        end, Tuples),
    lists:flatten(Addrs).

%% ----
%% prepares reinvite to one or both sides
%%
start_reinvite(State,#{}=Args) ->
    SideList = maps:get(sidelist, Args),
    #state_dlg{a=#side{callid=ACallId}=A,
               b=#side{callid=BCallId}=B,
               media=Media}=State,
    State1 = State#state_dlg{media=Media,
                             lstate=#{state => ?KEY,
                                      timestart => os:timestamp(),
                                      total_timer_ref => erlang:send_after(?REINVITING_TOTAL_TIMEOUT, self(), {reinviting_timeout, [?TOTAL]}),
                                      a => #{callid => ACallId, timer => undefined},
                                      b => #{callid => BCallId, timer => undefined},
                                      media => Media,
                                      sidelist => SideList,
                                      resp_count => 0,
                                      retry_count => 0}},
    {Res2, State2} =
        case lists:member('a', SideList) of
            true -> reinvite_side(A, 'a', State1);
            false -> {noreinvite, State1}
        end,
    {Res3, State3} =
        case lists:member('b', SideList) of
            true -> reinvite_side(B, 'b', State2);
            false -> {noreinvite, State2}
        end,
    case {Res2, Res3} of
        {noreinvite, noreinvite} -> done(State3);
        {noreinvite, error} -> done(State3);
        {error, noreinvite} -> done(State3);
        {error, error} -> done(State3);
        _ ->
            {next_state, ?REINVITING_STATE, State3}
    end.

%% ----
%% after 491 pending and timeout
%%
do_retry_side(ZIdx, State) ->
    #state_dlg{lstate=#{state:=?KEY}=LState}=State,
    RetryCount = maps:get(retry_count, LState),
    ZSide = case LState of
                #{a:=#{idx:=ZIdx}} -> State#state_dlg.a;
                #{b:=#{idx:=ZIdx}} -> State#state_dlg.b
            end,
    case reinvite_side(ZSide, ZIdx, State#state_dlg{lstate=LState#{retry_count:=RetryCount+1}}) of
        {error,State1} ->
            StopReason = ?B2BUA_UTILS:reason_error([reinvite, invite, send], ZSide#side.callid, <<"Send invite response">>),
            go_error(StopReason, State1);
        {_,State1} -> {next_state, ?CURRSTATE, State1}
    end.

%% ----
%% reinvite operation
%%
reinvite_side(ZSide, ZIdx, #state_dlg{use_media=false}=State) ->
    #side{localsdp=ZLSdp}=ZSide,
    reinvite_side_1(ZSide, ZIdx, ZLSdp, State);
reinvite_side(#side{remotetag=ZRTag}=ZSide, ZIdx, #state_dlg{media=Media}=State) ->
    % media (make term's local), only gets sdp, no mgc connect
    {ok, Media1, ZLSdp} = ?MEDIA_B2B:re_reinvite_request(Media, ZRTag),
    State1 = State#state_dlg{media=Media1},
    State2 = case State1 of
                 #state_dlg{a=#side{remotetag=ZRTag}=Side} -> State1#state_dlg{a=Side#side{localsdp=ZLSdp}};
                  #state_dlg{b=#side{remotetag=ZRTag}=Side} -> State1#state_dlg{b=Side#side{localsdp=ZLSdp}}
             end,
    reinvite_side_1(ZSide, ZIdx, ZLSdp, State2).

% @private
reinvite_side_1(ZSide, ZIdx, ZLSdp, State) ->
    #state_dlg{lstate=#{ZIdx:=ZMigr}=LState}=State,
    #side{localtag=ZLTag,
          last_remote_party=ZLFrom}=ZSide,
    %% from's domain is the domain of leg
    %ZDomain = (ZSide#side.localuri)#uri.domain,
    %ZLFrom1 = ZLFrom#uri{domain=ZDomain},
    ZLFrom1 = ZLFrom,
    %
    InviteOpts=[{from,?U:set_uri_tag(ZLFrom1,ZLTag)},
                {body,ZLSdp},
                user_agent,
                auto_2xx_ack],
    case send_reinvite_offer(InviteOpts, ZSide, State) of
        error -> {error, State};
        {ok,ReqHandle} ->
            ZMigr1 = ZMigr#{rhandle => ReqHandle,
                            idx => ZIdx,
                            ltag => ZSide#side.localtag,
                            lsdp => ZLSdp,
                            rsdp => undefined,
                            timer => undefined},
            State1 = State#state_dlg{lstate=LState#{ZIdx:=ZMigr1}},
            {ok, State1}
    end.

%% ----
%% send request
%%
send_reinvite_offer(InviteOpts, ZSide, State) ->
    #side{dhandle=DlgHandle,
          localtag=LTag}=ZSide,
    case catch nksip_uac:invite(DlgHandle, [async|InviteOpts]) of
        {'EXIT',Err} ->
            d(State, "send_reinvite_offer to LTag=~p, Caugth error=~120p", [LTag, Err]),
            error;
        {error,_R}=Err ->
            d(State, "send_reinvite_offer to LTag=~p, Error=~120p", [LTag, Err]),
            error;
        {async,ReqHandle} ->
            d(State, "send_reinvite_offer to LTag=~p", [LTag]),
            {ok,ReqHandle}
    end.

%% =============================================================
%% Handling timeout
%% =============================================================

do_on_timeout(State) ->
    d(State, "timeout"),
    case have_reinvited_sides(State) of
        true ->
            State1 = remove_timered_sides(State),
            State2 = cancel_timeout_timers(State1),
            {next_state, ?CURRSTATE, State2};
        false ->
            done(State)
    end.

have_reinvited_sides(#state_dlg{lstate=#{state:=?KEY}=LState}=_State) ->
    SideList = maps:get(sidelist, LState),
    lists:foldl(fun(_, true) -> true;
                   (ZIdx, _) ->
                        case maps:get(timer, maps:get(ZIdx, LState)) of
                            undefined -> true;
                            _ -> false
                        end
                end, false, SideList).

remove_timered_sides(#state_dlg{lstate=#{state:=?KEY}=LState}=State) ->
    SideList = maps:get(sidelist, LState),
    SideList1 = lists:filter(fun (ZIdx) ->
            case maps:get(timer, maps:get(ZIdx, LState)) of
                undefined -> true;
                _ -> false
            end
        end, SideList),
    State#state_dlg{lstate=LState#{sidelist:=SideList1}}.

%% =============================================================
%% Handling UAC responses
%% =============================================================

% -----
do_on_uac_response([#sipmsg{cseq={_,'INVITE'}, call_id=CallId, from={_,LTag}}=Resp]=P,
                   #state_dlg{lstate=#{state:=?KEY,a:=#{callid:=CallId, ltag:=LTag}}}=State) ->
    #sipmsg{class={resp,SipCode,_}}=Resp,
    d(State, "uac_response('INVITE') ~p from A ~p", [SipCode, LTag]),
    on_invite_response(P, State);

do_on_uac_response([#sipmsg{cseq={_,'INVITE'}, call_id=CallId, from={_,LTag}}=Resp]=P,
                   #state_dlg{lstate=#{state:=?KEY,b:=#{callid:=CallId, ltag:=LTag}}}=State) ->
    #sipmsg{class={resp,SipCode,_}, from={_,LTag}}=Resp,
    d(State, "uac_response('INVITE') ~p from B ~p", [SipCode, LTag]),
    on_invite_response(P, State);

do_on_uac_response([#sipmsg{cseq={_,Method}}=Resp], State) ->
    #sipmsg{call_id=CallId, from={_,LTag}}=Resp,
    #state_dlg{lstate=#{state:=?KEY}=LState}=State,
    d(State, "uac_response('~p') skipped ~n\t~140p, ~140p~n\t~140p", [Method, CallId, LTag, LState]),
    {next_state, ?CURRSTATE, State}.

% -----
on_invite_response([#sipmsg{class={resp,SipCode,_}}=_Resp], State) when SipCode >= 100, SipCode < 200 ->
    {next_state, ?CURRSTATE, State};
on_invite_response([#sipmsg{class={resp,SipCode,_}}=_Resp]=P, State) when SipCode >= 200, SipCode < 300 ->
    on_invite_response_2xx(P,State);
on_invite_response([#sipmsg{class={resp,SipCode,_}}=_Resp]=P, State) when SipCode >= 300, SipCode < 400 ->
    on_invite_response_error(P,State);
on_invite_response([#sipmsg{class={resp,SipCode,_}}=_Resp]=P, State) when SipCode == 491 ->
    on_invite_response_491(P,State);
on_invite_response([#sipmsg{class={resp,SipCode,_}}=_Resp]=P, State) when SipCode >= 400, SipCode < 700 ->
    on_invite_response_4xx_5xx_6xx(P,State);
on_invite_response([_Resp]=P, State) ->
    on_invite_response_error(P,State).

%% ===================================
%% Negative UAC response
%% ===================================

on_invite_response_error([#sipmsg{call_id=CallId}=_Resp]=Args, State) ->
    Fun = fun([#sipmsg{call_id=XCallId}=_Resp_]=_XArgs, XState) ->
        StopReason = ?B2BUA_UTILS:reason_error([reinviting, invite], XCallId, <<"Invalid response code">>),
        go_error(StopReason, XState)
        end,
    finish_side_and_do_fun_if_not_reinvited_more(CallId, Fun, Args, State).

on_invite_response_4xx_5xx_6xx([#sipmsg{call_id=CallId}=_Resp]=Args, State) ->
    Fun = fun(_XArgs, XState) ->
        done(XState)
        end,
    finish_side_and_do_fun_if_not_reinvited_more(CallId, Fun, Args, State).

on_invite_response_491([#sipmsg{call_id=CallId}=_Resp],#state_dlg{lstate=#{state:=?KEY}=LState}=State) ->
    TotalTimer = maps:get(total_timer_ref, LState),
    SideList = maps:get(sidelist, LState),
    case TotalTimer of
        undefined ->
            case length(SideList) of
                1 -> done(State); % this was the one we've been waiting for
                _ -> % still have more to wait
                    ZIdx = get_zidx_by_callid(CallId, State),
                    SideList1 = SideList -- [ZIdx],
                    State1 = State#state_dlg{lstate=LState#{sidelist:=SideList1}},
                    {next_state, ?CURRSTATE, State1}
            end;
        _ ->
            #state_dlg{lstate=#{state:=?KEY}=LState}=State,
            Timeout = ?EU:random(300,700),
            Fretry = fun(ZIdx,ZMigr) ->
                            TimerRef = erlang:send_after(Timeout, self(), {reinviting_timeout, [?RETRY, ZIdx]}),
                            State1 = State#state_dlg{lstate=LState#{ZIdx:=ZMigr#{timer:=TimerRef}}},
                            {next_state, ?CURRSTATE, State1}
                     end,
            case LState of
                #{retry_count:=Cnt} when Cnt>5 ->
                    case length(SideList) of
                        1 -> done(State); % this was the only one and could not prime timers any more
                        _ -> % still have more sides (either waiting reply or timered)
                            ZIdx = get_zidx_by_callid(CallId, State),
                            SideList1 = SideList -- [ZIdx],
                            State1 = State#state_dlg{lstate=LState#{sidelist:=SideList1}},
                            %
                            XIdx = case ZIdx of a -> b; b -> a end,
                            XTimer = maps:get(timer, maps:get(XIdx, LState)),
                            case XTimer of
                                undefined -> {next_state, ?CURRSTATE, State1}; % have to wait for reply
                                _ -> done(State1) % could not prime timers any more
                            end
                    end;
                #{a:=#{callid:=CallId, idx:=ZIdx}=ZMigr} -> Fretry(ZIdx,ZMigr);
                #{b:=#{callid:=CallId, idx:=ZIdx}=ZMigr} -> Fretry(ZIdx,ZMigr)
            end
    end.

%% ===================================
%% Positive UAC response
%% ===================================

on_invite_response_2xx([Resp], #state_dlg{use_media=UseMedia}=State) ->
    %
    #state_dlg{a=ASide,
               b=BSide,
               media=Media,
               lstate=#{state:=?KEY,
                        sidelist:=SideList}=LState}=State,
    #sipmsg{call_id=CallId,
            to={_,ZRTag},
            body=#sdp{}=ZRSdp}=Resp,
    % @todo media (update term's remote) [comment came from r_sip_b2bua_fsm_migrating.erl]
    MRes = case UseMedia of
               false -> {ok,Media};
               true -> ?MEDIA_B2B:re_reinvite_response(Media, ZRTag, Resp)
           end,
    case MRes of
        {error, R} ->
            StopReason = ?B2BUA_UTILS:reason_error([reinviting, media, update_term], <<>>, R),
            go_error(StopReason, State);
        {ok, Media1} ->
            State1 = State#state_dlg{media=Media1},
            %
            {ok,DlgHandle} = nksip_dialog:get_handle(Resp),
            State2 = case State1 of
                         #state_dlg{a=#side{callid=CallId}=ASide, b=BSide} ->
                            State1#state_dlg{lstate=LState#{sidelist:=SideList -- [a]},
                                             a=ASide#side{dhandle=DlgHandle,
                                                          remotesdp=ZRSdp}};
                         #state_dlg{a=ASide, b=#side{callid=CallId}=BSide} ->
                            State1#state_dlg{lstate=LState#{sidelist:=SideList -- [b]},
                                             b=BSide#side{dhandle=DlgHandle,
                                                          remotesdp=ZRSdp}}
                     end,
            %
            #state_dlg{lstate=LState1}=State2,
            case length(maps:get(sidelist,LState1)) of
                0 -> done(State2);
                _ -> {next_state, ?CURRSTATE, State2}
            end
    end.

%% =============================================================
%% Services
%% =============================================================

get_zidx_by_callid(CallId, State) ->
    case State of
       #state_dlg{a=#side{callid=CallId}} -> a;
       #state_dlg{b=#side{callid=CallId}} -> a
    end.

% ----
finish_side_and_do_fun_if_not_reinvited_more(CallId, FunDoIfNotReinvited, Args, #state_dlg{lstate=#{state:=?KEY}=LState}=State) ->
    SideList = maps:get(sidelist, LState),
    ZIdx = get_zidx_by_callid(CallId, State),
    SideList1 = SideList -- [ZIdx],
    State1 = State#state_dlg{lstate=LState#{sidelist:=SideList1}},
    case have_reinvited_sides(State1) of
        true -> {next_state, ?CURRSTATE, State1};
        false -> FunDoIfNotReinvited(Args, State1)
    end.

% ----
cancel_timeout_timers(#state_dlg{lstate=#{state:=?KEY}=LState}=State) ->
    TotalTimer = maps:get(total_timer_ref, LState),
    cancel_timer(TotalTimer),
    LState1 = LState#{total_timer_ref:=undefined},
    SideList = maps:get(sidelist, LState),
    State#state_dlg{lstate=
        lists:foldl(fun (ZIdx, AccLState) ->
            #{ZIdx:=#{timer:=Timer}=Side}=AccLState, cancel_timer(Timer), AccLState#{ZIdx:=Side#{timer:=undefined}}
        end, LState1, SideList)};
cancel_timeout_timers(State) -> State.

cancel_timer(Timer) ->
    case Timer of undefined -> ok; _ -> erlang:cancel_timer(Timer) end.

% ----
done(#state_dlg{timer_ref=DlgTimerRef}=State) ->
    cancel_timeout_timers(State),
    State1 = State#state_dlg{lstate=undefined},
    d(State, " -> switch to '~p'", [?DIALOG_STATE]),
    case DlgTimerRef of
        undefined ->
            ?DIALOG:start(State1);
        _ ->
            {next_state, ?DIALOG_STATE, State1}
    end.

% ----
go_error(Reason, State) ->
    ?LOGSIP("go_error(Reason=~p, State)", [Reason]),
    go_abort(stop_error, Reason, State).

% ----
go_abort(Fun, Arg, State) ->
    cancel_timeout_timers(State),
    ?LOGSIP("go_abort() -> DIALOG:~p(Arg, State)", [Fun]),
    State1 = State#state_dlg{lstate=undefined},
    ?DIALOG:Fun(Arg, State1).

% -----
d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    #state_dlg{dialogid=DlgId}=State,
    ?LOGSIP("B2B fsm ~p '~p':" ++ Fmt, [DlgId,?CURRSTATE] ++ Args).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% States of reinviting
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Legend:
%% wX  - wait response from X (reinvite sent)
%% swX - send reinvite, wait response from X -> wX
%% ptX - prime(d) timer for X (after 491)
%% -X  - removed X from sidelist
%% "x" - impossible event
%% "o"/">o" - done
%% sT-X - stop timer and remove X from sidelist
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%           |  A491   | AOK/bad |  B491   | BOK/bad |    TA   |    TB   | TotalT  |
%% ----------+---------+---------+---------+---------+---------+---------+---------+
%%  wA /  wB | ptA/ wB |  -A/ptB |  wA/ptB |  wA/ -B |    x    |    x    |  fA/ fB |
%% ptA /  wB |    x    |    x    | ptA/ptB | ptA/ -B | swA/ wB |    x    |sT-A/ fB |
%%  -A /  wB |    x    |    x    |  -A/ptB |    o    |    x    |    x    |  -A/ fB |
%%  wA / ptB | ptA/ptB |  -A/ptB |    x    |    x    |    x    |  wA/swB |  fA/sT-B|
%%  wA /  -B | ptA/ -B |    o    |    x    |    x    |    x    |    x    |  fA/ -B |
%%  fA /  fB |  -A/ fB |  -A/ fB |  fA/ -B |  fA/ -B |    x    |    x    |    x    |
%% ptA / ptB |    x    |    x    |    x    |    x    | swA/ptB | ptA/swB |sT-AB  >o|
%% ptA /  -B |    x    |    x    |    x    |    x    | swA/ -B |    x    |sT-A/-B>o|
%%  -A / ptB |    x    |    x    |    x    |    x    |    x    |  -A/swB |-A/sT-B>o|
%%  -A /  fB |    x    |    x    |    o    |    o    |    x    |    x    |    x    |
%%  fA /  -B |    o    |    o    |    x    |    x    |    x    |    x    |    x    |
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% impossible states
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  fA /  wB | -A/ fB  | -A/ fB  |  fA/ -B |  fA/ -B |    x    |    x    |    x    |
%%  wA /  fB | -A/ fB  | -A/ fB  |  fA/ -B |  fA/ -B |    x    |    x    |    x    |
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
