%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Presents 'sessionchanging' state of B2BUA Dialog FSM.
%%%      Acts forward of reinvite. Ends with final answer or timeout.

-module(r_sip_b2bua_fsm_sessionchanging).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([enter_by_reinvite/2,
         pending_reinvite/2,
         reinvite_timeout/2,
         sip_bye/2,
         uac_response/2,
         uas_dialog_response/2,
         call_terminate/2,
         stop_external/2,
         test/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_mgc.hrl").

-define(CURRSTATE, 'sessionchanging').

%% ====================================================================
%% API functions
%% ====================================================================

% ----
enter_by_reinvite([_CallId, _Req]=P, State) ->
    d(State, "enter_by_reinvite, CallId=~120p", [_CallId]),
    do_start_reinvite(P, State).

% ----
pending_reinvite([_CallId, _Req], State) ->
    d(State, "pending_reinvite, CallId=~120p", [_CallId]),
    do_on_pending_reinvite(State).

% ----
stop_external(Reason, State) ->
    d(State, "stop_external(~p)", [Reason]),
    #state_dlg{reinvite=#reinvite{xreq=XReq}}=State,
    send_response({603,[]}, XReq),
    ?DIALOG:stop_external(Reason, State#state_dlg{reinvite=undefined}).

% ----
test(State) ->
    d(State, "test"),
    {next_state, ?CURRSTATE, State}.

% ----
reinvite_timeout([<<"total">>|_]=Args, State) ->
    do_on_uac_timeout(Args, State);

reinvite_timeout([<<"pending">>]=Args, State) ->
    do_on_pending_timeout(Args, State).

% ----
sip_bye([_CallId, _Req]=Args, State) ->
    d(State, "bye, CallId=~120p", [_CallId]),
    #state_dlg{reinvite=#reinvite{xreq=XReq}}=State,
    send_response({603,[]}, XReq),
    ?DIALOG:sip_bye(Args, State#state_dlg{reinvite=undefined}).

% ----
uac_response([_Resp]=P, State) ->
    #sipmsg{class={resp,SipCode,SipReason},
            cseq={CSeq,Method},
            call_id=CallId}=_Resp,
    d(State, "uac_response ~p:~p -> ~p ~p, CallId=~120p", [Method,CSeq,SipCode,SipReason,CallId]),
    do_on_uac_response(P, State).

% ----
uas_dialog_response([_Resp]=Args, State) ->
    #sipmsg{class={resp,SipCode,_},
            cseq={CSeq,Method},
            call_id=CallId}=_Resp,
    d(State, "uas_dialog_response ~p:~p -> ~p, CallId=~120p", [Method,CSeq,SipCode,CallId]),
    do_on_uas_dialog_response(Args, State).

% ----
call_terminate([Reason, _Call], State) ->
    #call{call_id=CallId}=_Call,
    d(State, "call_terminate, CallId=~120p, Reason=~120p", [CallId, Reason]),
    #state_dlg{reinvite=#reinvite{xreq=XReq}}=State,
    send_response(?InternalError("B2B.SC. Call terminated"), XReq),
    StopReason = ?B2BUA_UTILS:reason_callterm(CallId),
    ?DIALOG:stop_error(StopReason, State#state_dlg{reinvite=undefined}).

%% ====================================================================
%% Internal functions
%% ====================================================================


%% =============================================================
%% Start reinvite
%% =============================================================

do_start_reinvite([CallId, Req]=P, State) ->
    case State of
        #state_dlg{a=#side{callid=CallId}=A, b=B} -> do_start_reinvite_1(P, 'b', {A,B}, State);
        #state_dlg{a=A, b=#side{callid=CallId}=B} -> do_start_reinvite_1(P, 'a', {B,A}, State);
        _ ->
            send_reinvite_answer_at_start(internal_error, Req, State),
            {next_state, ?DIALOG_STATE, State}
    end.

% ----
do_start_reinvite_1([_CallId, Req], FwdSide, {X,Y}, State) ->
    %
    #sdp{}=XRSdp=?U:extract_sdp(Req),
    % media
    case ?B2BUA_MEDIA:fwd_reinvite_request_to_y(State, Req) of
        {error,R} ->
            StopReason = ?B2BUA_UTILS:reason_error([sessionchange, media, update_term], <<>>, R),
            go_error(StopReason, Req, State, "B2B.SC. Media error");
        {ok, State1, YLSdp} ->
            % ?OUT("DEBUG. REINVITE Y SDP: ~p", [YLSdp]),
            % @TODO @remoteparty for y on reinvite header 'from'
            % update by DC display name if undefined
            % put real abonent name in from display/uri.
            % it could be extension direction, from could change even if direction the same
            FromOpts =
                case build_reinvite_from(Req, FwdSide, {X,Y}, State) of
                    false ->
                        YSide1 = Y,
                        [];
                    #uri{}=YLFromUri ->
                        YLFromUri1 = ?U:clear_uri(YLFromUri),
                        YSide1 = Y#side{last_remote_party=YLFromUri1},
                        %% from's domain is the domain of leg
                        %YDomain = (Y#side.localuri)#uri.domain,
                        %YLFromUri2 = YLFromUri1#uri{domain=YDomain},
                        YLFromUri2 = YLFromUri1,
                        [{from, YLFromUri2}]
                end,
            % update allow-events of X side
            ReqAllowEvents = ?U:parse_header_params(Req,<<"allow-events">>),
            XSide = case ReqAllowEvents of [] -> X; _ -> X#side{allow_events=ReqAllowEvents} end,
            %% ----
            InviteOpts = [{body,YLSdp},
                          user_agent,
                          auto_2xx_ack
                         | FromOpts],
            case send_reinvite_offer(InviteOpts, Y, State1) of
                error ->
                    send_reinvite_answer_at_start(internal_error, Req, State1),
                    d(State1, " -> switch to '~p'", [?DIALOG_STATE]),
                    {next_state, ?DIALOG_STATE, State1};
                {ok,YReqHandle} ->
                    {ok,XReqHandle} = nksip_request:get_handle(Req),
                    StateReinvite = #reinvite{xreq=Req,
                                              fwdside=FwdSide,
                                              timestart=os:timestamp(),
                                              timer_ref=erlang:send_after(?REINVITE_TIMEOUT, self(), {reinvite_timeout, [<<"total">>,Y#side.callid,Y#side.localtag]}),
                                              xrhandle=XReqHandle,
                                              xcallid=X#side.callid,
                                              xltag=X#side.localtag,
                                              xrsdp=XRSdp,
                                              yrhandle=YReqHandle,
                                              ycallid=Y#side.callid,
                                              yltag=Y#side.localtag,
                                              ylsdp=YLSdp,
                                              y=Y,
                                              offer_opts=InviteOpts},
                    State2 = State1#state_dlg{reinvite=StateReinvite},
                    State3 = case FwdSide of
                                 'a' -> State2#state_dlg{a=YSide1,b=XSide};
                                 'b' -> State2#state_dlg{a=XSide,b=YSide1}
                             end,
                    %
                    d(State3, " -> switch to '~p'", [?CURRSTATE]),
                    {next_state, ?CURRSTATE, State3}
            end
    end.

%%%% New
%%     Не из учетки
%%         x1. В ту же сторону что и исходный инвайт
%%             - x11. From.username/From.dn не изменены (в пред хорошем запросе и текущем запросе от удаленной стороны)
%%                 Отправляем без изменений то что было на предыдущем
%%             - x12. From.username/From.dn изменены (в пред хорошем запросе и текущем запросе от удаленной стороны)
%%                 Отправляем новый from насквозь и запоминаем
%%         x2. В обратную сторону
%%             Отправляем новый from насквозь и запоминаем
%%     Из учетки
%%         x3. В ту же сторону что и исходный инвайт
%%             - x31. From.username не изменен
%%                 - x311. Displayname не поменялся
%%                     Мы перенаправляем тоже без изменений то что было на предыдущем вызове.
%%                 - x312. Displayname поменялся
%%                     Мы перенаправляем тоже без изменений то что было на предыдущем вызове, но обновить DisplayName (sipuser.dn over From.dn)
%%             - x32. From.username изменен
%%                 - x321. нет extension у from
%%                     - x3211. Displayname не поменялся
%%                         Мы перенаправляем тоже без изменений то что было на предыдущем вызове.
%%                     - x3212. Displayname поменялся
%%                         Мы перенаправляем тоже без изменений то что было на предыдущем вызове, но обновить DisplayName (sipuser.dn over From.dn)
%%                 - x322. есть extension у from
%%                     Исходная котовасия по детекту фрома, и через репрезентатив тоже
%%         x4. В обратную сторону к исходному инвайту
%%             - x41. From.username совпадает с предыдущим To.username (RemotePartyId)
%%                 - x411. Displayname не поменялся
%%                     Прошлый RemotePartyId подставить во фром (в нем инфа по самой учетке)
%%                 - x412. Displayname поменялся
%%                     Прошлый RemotePartyId подставить во фром, но обновить DisplayName (sipuser.dn over From.dn)
%%             - x42. From.username не совпадает с предыдущим To.username (RemotePartyId),
%%                 - x4211. нет extension у from
%%                     - x4211. Displayname не поменялся
%%                         Прошлый RemotePartyId подставить во фром (в нем инфа по самой учетке).
%%                     - x4212. Displayname поменялся
%%                         Прошлый RemotePartyId подставить во фром (в нем инфа по самой учетке), но обновить DisplayName (sipuser.dn over From.dn)
%%                 - x422. есть extension у from
%%                     Исходная котовасия по детекту фрома, и через репрезентатив тоже
%%         x5. Был внутренний юзер, а теперь не определен
%%             Мы перенаправляем тоже без изменений то что было на предыдущем вызове.
%%         x6. Был один внутренний юзер, а теперь другой
%%             Мы перенаправляем тоже без изменений то что было на предыдущем вызове.
%%         x7. Не было внутреннего юзера, а теперь есть
%%             Мы перенаправляем тоже без изменений то что было на предыдущем вызове.
%%      Для оптимизации пункты x11,x21, x311, x411 проверяются в самом начале как самые частые чтобы сэкономить на расчетах в основной процедуре.
%%   В логи пишет привязку сессии и режима REINVITE

%% could be simpled if nothing changed in from & contacts,
%%  but if something changed - then full routine should be executed
build_reinvite_from(#sipmsg{from={Uri,_}, contacts=C}=_Req, _FwdSide, {#side{remoteuri=Uri,remotecontacts=C}=_X,Y}, State) ->
    d(State, "SESSCHANGE FROM: x11,x21,x311,x411"),
    Y#side.last_remote_party;

build_reinvite_from(Req, FwdSide, {X,Y}, State) ->
%%         %% DEBUG
%%      #sipmsg{from={XFrom,XFT},to={XTo,XTT},headers=XH,contacts=[XC|XCR]}=XReq,
%%      ?OUT("FROM :~120p", [XFrom]),
%%      Req=XReq#sipmsg{from={XFrom#uri{user= <<"730">>,disp= <<"\"Sidorov \"">>},XFT}
%%                     %from={XFrom#uri{user= <<"sip1">>},XFT}
%%                     %from={XFrom#uri{disp= <<"\"TESTDISP\"">>},XFT}
%%                     %from={XFrom#uri{disp= <<"\"TESTDISP\"">>,user= <<"sip1">>},XFT}
%%                     %from={XFrom#uri{user= <<"sip1">>,domain= <<"test.rootdomain.ru">>},XFT}
%%                     %,to={XTo#uri{user= <<"12">>, domain= <<"test.rootdomain.ru">>},XTT},
%%                     %,contacts=[XC#uri{user= <<"sip1">>}|XCR]
%%                      ,headers=[{<<"proxy-authorization">>,<<"Digest username=\"sip1\";">>}|XH]
%%                     },
%%         ?OUT("XLocalUri: ~p", [Y#side.localuri]),
    % -----------------
    #sipmsg{from={XRUri,_}}=Req,
    #side{remoteuri=#uri{domain=XD}=XRemoteUri,
          sipuser=XSipUser}=X,
    #side{remoteuri=#uri{user=YU,domain=YD},
          localuri=YLocalUri}=Y,
    % -----
    {A,_B,C,_D,SipUser,_Abon} = ?FROM:build_from_abc(Req),
    [LoginOld] = ?EU:extract_optional_props([login], case XSipUser of undefined -> []; _ -> XSipUser end),
    [LoginNew] = ?EU:extract_optional_props([login], case SipUser of undefined -> []; _ -> SipUser end),
    % -----
    XRemoteUri = X#side.remoteuri,
    UserChanged = XRemoteUri#uri.user/=XRUri#uri.user,
    DispChanged = XRemoteUri#uri.disp/=XRUri#uri.disp,
    % -----
    Fdefault = fun() -> #uri{scheme=sip,disp=C,user=A,domain=YD} end,
    Flog = fun(Txt) -> d(State, "SESSCHANGE FROM: ~120p", [Txt]) end,
    % -----
    case XSipUser of
        undefined when SipUser/=undefined -> Flog("x7"), false;                              % x7
        undefined ->
              case FwdSide of
                  'b' when not UserChanged, not DispChanged -> Flog("x11"), false;           % x11
                  'b' -> Flog("x12"), Fdefault();                                            % x12
                  'a' -> Flog("x2"), Fdefault()                                              % x2
            end;
        _SipUserOld when SipUser==undefined -> Flog("x5"), false;                            % x5    +
        _SipUserOld when LoginOld/=LoginNew -> Flog("x6"), false;                            % x6    +
        _SipUserOld ->
            [FNumExt1] = ?EU:extract_required_props([extension],SipUser), % current request
            #side{last_remote_party=YLastRP}=Y,
            % ----
            Fdisplay = fun(Uri) -> Uri#uri{disp=C} end,
            FLastRP = fun() -> YLocalUri#uri{disp=YLastRP#uri.disp} end,
            Ffull = fun() ->
                            Flogrepr = fun(#{}=_M) -> ok end, % ?OUT("REPR: ~120p", [M]) end, % ?IR_EVENT:event('represenative',[#{req=>},M]) end,
                            {ok,ResUser,ResDisp} = ?REPRESENTATIVE:apply({sip,A,XD},{sip,YU,YD},SipUser,C,Flogrepr),
                            YLocalUri#uri{disp=ResDisp,user=ResUser}
                    end,
            % ----
            % ?OUT("CASE: ~120p", [{FwdSide,UserChanged,FNumExt1,DispChanged}]),
            case {FwdSide,UserChanged,FNumExt1,DispChanged} of
                {'b',false,_,false} -> Flog("x311"), false;                                  % x311  +
                {'b',false,_,true} -> Flog("x312"), Fdisplay(YLocalUri);                     % x312  +
                {'b',true,undefined,false} -> Flog("x3211"), false;                          % x3211 +
                {'b',true,undefined,true} -> Flog("x3212"), Fdisplay(XRemoteUri);            % x3212 + !
                {'b',true,_,_} -> Flog("x322"), Ffull();                                     % x322  + !
                {'a',false,_,false} -> Flog("x411"), FLastRP();                              % x411  +
                {'a',false,_,true} -> Flog("x412"), Fdisplay(YLocalUri);                     % x412  + ?
                {'a',true,undefined,false} -> Flog("x4211"), FLastRP();                      % x4211 + !
                {'a',true,undefined,true} -> Flog("x4212"), Fdisplay(YLastRP);               % x4212 + !
                {'a',true,_,_} -> Flog("x422"), Ffull()                                      % x422  + !
            end
    end.

% ----
send_reinvite_offer(InviteOpts, Y, State) ->
    #side{dhandle=DlgHandle,
          localtag=LTag}=Y,
    case catch nksip_uac:invite(DlgHandle, [async|InviteOpts]) of
        {'EXIT',Err} ->
            d(State, "send_reinvite_offer to LTag=~p, Caught error=~120p", [LTag, Err]),
            error;
        {error,_R}=Err ->
            d(State, "send_reinvite_offer to LTag=~p, Error=~120p", [LTag, Err]),
            error;
        {async,ReqHandle} ->
            d(State, "send_reinvite_offer to LTag=~p", [LTag]),
            {ok,ReqHandle}
    end.

% ----
send_reinvite_answer_at_start(SipReply, Req, _StateData) ->
    send_response(SipReply, Req).

% ----
% @private nksip's bug, when recv incoming opposite reinvite while offering, offer is not being sent.
%
do_on_pending_reinvite(State) ->
    erlang:send_after(1000, self(), {reinvite_timeout, [<<"pending">>]}),
    State.

do_on_pending_timeout(_Args, State) ->
    #state_dlg{reinvite=#reinvite{offer_opts=InviteOpts, y=Y}=Re}=State,
    case send_reinvite_offer(InviteOpts, Y, State) of
        error ->
            send_reinvite_answer_at_start(internal_error, Re#reinvite.xreq, State),
            d(State, " -> switch to '~p'", [?DIALOG_STATE]),
            {next_state, ?DIALOG_STATE, State};
        {ok,YReqHandle} ->
            State1 = State#state_dlg{reinvite=Re#reinvite{yrhandle=YReqHandle}},
            {next_state, ?CURRSTATE, State1}
    end.

%% =============================================================
%% Handling UAC responses
%% =============================================================

% ----
do_on_uac_timeout([_, CallId, YLTag], State) ->
    d(State, "uac_timeout YLTag=~120p, CallId=~120p", [YLTag, CallId]),
    #state_dlg{reinvite=#reinvite{xreq=XReq}}=State,
    send_response(?RequestTimeout("B2B.SC. Forward timeout"), XReq),
    StopReason = ?B2BUA_UTILS:reason_timeout([sessionchange, invite], CallId, <<"Reinvite timeout">>),
    ?DIALOG:stop_error(StopReason, State#state_dlg{reinvite=undefined}).

% -----
do_on_uac_response([#sipmsg{cseq={_,'INVITE'}, from={_,YLTag}}=Resp]=P, #state_dlg{reinvite=#reinvite{yltag=YLTag}}=State) ->
    #sipmsg{class={resp,SipCode,_}}=Resp,
    d(State, "uac_response('INVITE') ~p from ~p", [SipCode, YLTag]),
    on_invite_response(P, State);

do_on_uac_response([#sipmsg{cseq={_,Method}}=_Resp], State) ->
    d(State, "uac_response('~p') skipped", [Method]),
    {next_state, ?CURRSTATE, State}.

% -----
on_invite_response([#sipmsg{class={resp,SipCode,_}}=_Resp], State) when SipCode >= 100, SipCode < 200 ->
    {next_state, ?CURRSTATE, State};
on_invite_response([#sipmsg{class={resp,SipCode,_}}=_Resp]=P, State) when SipCode >= 200, SipCode < 300 ->
    on_invite_response_2xx(P,State);
on_invite_response([#sipmsg{class={resp,SipCode,_}}=_Resp]=P, State) when SipCode >= 300, SipCode < 400 ->
    on_invite_response_error(P, State);
on_invite_response([#sipmsg{class={resp,SipCode,_}}=_Resp]=P, State) when SipCode >= 400, SipCode < 700 ->
    on_invite_response_4xx_5xx_6xx(P,State);
on_invite_response([_Resp]=P, State) ->
    on_invite_response_error(P, State).

%% ===================================
%% Negative UAC response
%% ===================================

on_invite_response_error([#sipmsg{call_id=CallId}=_Resp], State) ->
    #state_dlg{reinvite=#reinvite{xreq=XReq}}=State,
    cancel_timeout_timer(State),
    send_response(?InternalError("B2B.SC. Forward response error"), XReq),
    StopReason = ?B2BUA_UTILS:reason_error([sessionchange, invite], CallId, <<"Reinvite unexpected response code">>),
    ?DIALOG:stop_error(StopReason, State#state_dlg{reinvite=undefined}).

on_invite_response_4xx_5xx_6xx([#sipmsg{class={resp,SipCode,_SipReason}}=_Resp], State) ->
    #state_dlg{reinvite=#reinvite{xreq=XReq}}=State,
    cancel_timeout_timer(State),
    SipCode1 = SipCode, % @todo map
    send_response({SipCode1,[]}, XReq),
    rollback(State, fun(State1) -> {next_state, ?DIALOG_STATE, State1#state_dlg{reinvite=undefined}} end).

%% ===================================
%% Positive UAC response
%% ===================================

on_invite_response_2xx([#sipmsg{class={resp,SipCode,_}}=Resp], State) ->
    %
    #state_dlg{a=ASide,
               b=BSide,
               reinvite=#reinvite{xreq=XReq,
                                  xcallid=XCallId}=Reinvite}=State,
    #sdp{}=YRSdp=?U:extract_sdp(Resp),
    % media
    case ?B2BUA_MEDIA:fwd_reinvite_response_to_x(State, Resp) of
        {error,R} ->
            StopReason = ?B2BUA_UTILS:reason_error([sessionchange, media, update_term], <<>>, R),
            go_error(StopReason, State, "B2B.SC. Media error");
        {ok, State1, XLSdp} ->
            % response
            % @TODO @remoteparty for x on reinvite response header 'remote-party' (future from when reinvite back side)
            %    displayname is not depend on remote, username not used after session change, so no need to compute username
            SipCode1 = SipCode, % @todo map
            ReplyOpts=[{body,XLSdp},
                       user_agent],
            send_response({SipCode1, ReplyOpts}, XReq),
            % melody
            State2 = case ?B2BUA_MEDIA:update_wait_melody(State1, XReq, Resp) of
                         {ok, StateX} -> StateX;
                         {error,R} ->
                             d(State, "update_wait_melody on ~100p error ~200p", [XCallId, R]),
                             State
                     end,
            % res
            cancel_timeout_timer(State1),
            {ok,YDlgHandle} = nksip_dialog:get_handle(Resp),
            State3 = case State2 of
                         #state_dlg{a=#side{callid=XCallId}=ASide, b=#side{allow_events=AllowEvents0}=BSide} ->
                             State2#state_dlg{a=XSide=ASide#side{remotesdp=Reinvite#reinvite.xrsdp},
                                              b=BSide#side{dhandle=YDlgHandle,
                                                           allow_events=case ?U:parse_header_params(Resp,<<"allow-events">>) of [] -> AllowEvents0; NewAllowEvents -> NewAllowEvents end, % update allow-events of X side
                                                           remotesdp=YRSdp}};
                         #state_dlg{a=#side{allow_events=AllowEvents0}=ASide, b=#side{callid=XCallId}=BSide} ->
                             State2#state_dlg{b=XSide=BSide#side{remotesdp=Reinvite#reinvite.xrsdp},
                                              a=ASide#side{dhandle=YDlgHandle,
                                                           allow_events=case ?U:parse_header_params(Resp,<<"allow-events">>) of [] -> AllowEvents0; NewAllowEvents -> NewAllowEvents end, % update allow-events of X side
                                                           remotesdp=YRSdp}}
                     end,
            event_hold(State2, State3),
            event_sesschange(XSide, State3),
            {next_state, ?CURRSTATE, State3}
    end.

%% ----------
%% Events

% session_change
event_sesschange(XSide, State) ->
    ?FSM_EVENT:session_change(XSide, State).

% hold, unhold
event_hold(StateBefore, StateAfter) ->
    % sides
    #state_dlg{reinvite=#reinvite{xcallid=XCallId}}=StateBefore,
    {{XSide1,YSide1},{XSide2,YSide2}} =
        case StateBefore of
            #state_dlg{a=#side{callid=XCallId}=ASide, b=BSide} -> {{ASide,BSide},{StateAfter#state_dlg.a, StateAfter#state_dlg.b}};
            #state_dlg{a=ASide, b=#side{callid=XCallId}=BSide} -> {{BSide,ASide},{StateAfter#state_dlg.b, StateAfter#state_dlg.a}}
        end,
    % sdps
    {SdpX1,SdpY1} = {XSide1#side.remotesdp, YSide1#side.remotesdp},
    {SdpX2,SdpY2} = {XSide2#side.remotesdp, YSide2#side.remotesdp},
    % modes
    {MX1,MY1} = {?M_SDP:get_audio_mode(SdpX1), ?M_SDP:get_audio_mode(SdpY1)},
    {MX2,MY2} = {?M_SDP:get_audio_mode(SdpX2), ?M_SDP:get_audio_mode(SdpY2)},
    M = case {{MX1,MY1},{MX2,MY2}} of
            %{{<<"inactive">>,<<"inactive">>},_} when MX2 == <<"sendonly">> -> unhold_y;
            {{<<"inactive">>,<<"inactive">>},_} when MY2 == <<"sendonly">> -> unhold_x;
            {{<<"sendrecv">>,<<"sendrecv">>},_} when MX2 == <<"sendonly">> -> hold_x;
            %{{<<"sendrecv">>,<<"sendrecv">>},_} when MY2 == <<"sendonly">> -> hold_y;
            %{_,{<<"inactive">>,<<"inactive">>}} when MX1 == <<"sendonly">> -> hold_y;
            {_,{<<"inactive">>,<<"inactive">>}} when MY1 == <<"sendonly">> -> hold_x;
            {_,{<<"sendrecv">>,<<"sendrecv">>}} when MX1 == <<"sendonly">> -> unhold_x;
            %{_,{<<"sendrecv">>,<<"sendrecv">>}} when MY1 == <<"sendonly">> -> unhold_y;
            _ -> undefined
        end,
    % event
    case M of
        hold_x -> ?FSM_EVENT:hold(XSide2, StateAfter);
        unhold_x -> ?FSM_EVENT:unhold(XSide2, StateAfter);
        undefined -> ok
    end.

%% ===================================
%% UAS response handler
%% ===================================

% ----
do_on_uas_dialog_response([#sipmsg{class={resp,SipCode,_},
                                   cseq={_,Method},
                                   to={_,Tag}}=Resp],
                           #state_dlg{reinvite=#reinvite{xltag=Tag}}=State)
  when (Method=='INVITE' orelse Method=='UPDATE') 
  andalso (SipCode>=200 andalso SipCode<300) ->
    %
    #state_dlg{reinvite=#reinvite{xcallid=XCallId}}=State,
    {ok,XDlgHandle} = nksip_dialog:get_handle(Resp),
    State1 = case State of
                 #state_dlg{a=#side{callid=XCallId}=ASide} ->
                      State#state_dlg{a=ASide#side{dhandle=XDlgHandle},
                                      reinvite=undefined};
                 #state_dlg{b=#side{callid=XCallId}=BSide} ->
                      State#state_dlg{b=BSide#side{dhandle=XDlgHandle},
                                      reinvite=undefined}
             end,
    %
    d(State1, " -> switch to '~p'", [?DIALOG_STATE]),
    {next_state, ?DIALOG_STATE, State1};
do_on_uas_dialog_response(_P, State) ->
    {next_state, ?CURRSTATE, State}.

%% =============================================================
%% Services
%% =============================================================

rollback(State, FOk) ->
    case ?B2BUA_MEDIA:rollback_reinvite(State) of
        {error,R} ->
            StopReason = ?B2BUA_UTILS:reason_error([sessionchange, media, rollback], <<>>, R),
            go_error(StopReason, State, "B2B.SC. Media error rollback");
        {ok,State1} -> FOk(State1)
    end.

% ---
go_error(Reason, #state_dlg{}=State, SipReasonText) ->
    #state_dlg{reinvite=#reinvite{xreq=XReq}}=State,
    go_error(Reason, XReq, State, SipReasonText).
go_error(Reason, #sipmsg{}=XReq, #state_dlg{}=State, SipReasonText) ->
    send_response(?InternalError(SipReasonText), XReq),
    ?DIALOG:stop_error(Reason, State#state_dlg{reinvite=undefined}).

% ---
send_response(SipReply, Req) ->
    {ok,Handle} = nksip_request:get_handle(Req),
    %nksip_request:reply(SipReply, Handle)
    ?U:send_sip_reply(fun() -> nksip_request:reply(SipReply, Handle) end).

% ---
cancel_timeout_timer(State) ->
    #state_dlg{reinvite=#reinvite{timer_ref=TimerRef}}=State,
    erlang:cancel_timer(TimerRef).

% -----
d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    #state_dlg{dialogid=DlgId}=State,
    ?LOGSIP("B2B fsm ~p '~p':" ++ Fmt, [DlgId,?CURRSTATE] ++ Args).
