%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2022 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 20.06.2022
%%% @doc

-module(r_sip_b2bua_fsm_utils_asr).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([setup_asr/3]).

-export([loop/3, loop/4]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_mgc.hrl").

-define(MLSTT, era_env_ml_stt).
-define(ChannelName, 'asr_stream').
-define(ChunkSize, 4000). % in L16/8000, real sent chunk size would be 2X
-define(MaxBytesSentBeforeFirstRecvStartPrhase, 12800000). % in real L16/16000
-define(FinalCounter, 2).
-define(PostTimeout, 3000).

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---------------------------------------------
-spec setup_asr(Cmd::add|remove,Opts::map(),State::#state_dlg{})
      -> {ok,State1::#state_dlg{}} | {ok,Reply::term(),State1::#state_dlg{}} | {error,Reason::term()}.
%% ---------------------------------------------
setup_asr(Cmd,Opts,#state_dlg{invite_ts=ITS,dialogid=DlgId}=StateData) ->
    Layer = ?EU:strbin("asr_~ts", [?EU:get_by_key(layer,Opts,<<"default">>)]),
    case Cmd of
        'add' ->
            {_,RecPath} = ?U:get_media_context_rec_opts(ITS),
            File = filename:join(RecPath,?EU:str("asr_~tp.raw",[?EU:timestamp()])),
            OptsET = #{id => Layer,
                       termtype => 'rec',
                       file => ?EU:to_binary(File),
                       type => <<"raw">>,
                       buffer_duration => 250,
                       codec_name => <<"L16/8000">>},
            case ?B2BUA_MEDIA:external_term_attach(OptsET,StateData) of
                {error,_}=Err -> {ok,Err,StateData};
                {Reply,StateData1} -> % ok | {ok,LSdp}
                    % setup topology
                    case setup_topology(Layer,Opts,StateData1) of
                        {error,_}=Err -> {ok,Err,StateData1};
                        {ok,StateData2} ->
                            Opts1 = Opts#{id => Layer, file => File, dialogid => DlgId},
                            {ok,StateData3} = start_asr_loop(Layer,Opts1,StateData2),
                            {ok, Reply, StateData3}
                    end
            end;
        'remove' ->
            OptsET = #{id => Layer},
            case ?B2BUA_MEDIA:external_term_detach(OptsET,StateData) of
                {error,_}=Err ->
                    {ok,Err,StateData};
                {Reply,StateData1} -> % ok | {ok,LSdp}
                    case stop_asr_loop(Layer,Opts,StateData1) of
                        false -> {ok,{error,<<"Layer not found">>},StateData1};
                        {ok,StateData2} -> {ok,Reply,StateData2};
                        {async,StateData2} -> {ok,StateData2}
                    end
            end
    end.

%% ====================================================================
%% Internal functions
%% Topology
%% ====================================================================

setup_topology(Layer,Opts,#state_dlg{a=#side{callid=ACallId},b=#side{callid=BCallId}}=StateData) ->
    case ?EU:get_by_key(side,Opts,undefined) of
        ACallId -> do_setup_topology(Layer,a,Opts,StateData);
        BCallId -> do_setup_topology(Layer,b,Opts,StateData);
        <<"a">> -> do_setup_topology(Layer,a,Opts,StateData);
        <<"b">> -> do_setup_topology(Layer,b,Opts,StateData);
        _ -> {ok,StateData}
    end.

%% @private
do_setup_topology(Layer,Side,Opts,StateData) ->
    [IsolateSide] = case ?EU:to_bool(?EU:get_by_key(opposite,Opts,false)) of
                        false -> [a,b] -- [Side];
                        true -> [Side]
                    end,
    OptsET = #{id=>Layer,topology=>[{Side,t,oneway},{IsolateSide,t,isolate}]}, % TODO: other prompts?
    ?B2BUA_MEDIA:external_term_setup_topology(OptsET,StateData).

%% ====================================================================
%% Internal functions
%% ASR pid
%% ====================================================================

%% -----
start_asr_loop(Layer,Opts,#state_dlg{media=Media,map=MapData}=StateData) ->
    Self = self(),
    #media{mgid=MG}=Media,
    Opts1 = Opts#{mgid=>MG},
    AsrPid = spawn(fun() -> loop_enter(Self,Opts1) end),
    {ok,StateData#state_dlg{map=MapData#{asr_pids => lists:keystore(layer,1,maps:get(asr_pids,MapData,[]),{Layer,AsrPid})}}}.

%% -----
-spec stop_asr_loop(Layer::binary(),Opts::map(),State::#state_dlg{})
    -> {ok,State::#state_dlg{}} | {async,State::#state_dlg{}} | false.
stop_asr_loop(Layer,Opts,#state_dlg{map=MapData}=StateData) ->
    AsrPids = maps:get(asr_pids,MapData,[]),
    case ?EU:get_by_key(Layer,AsrPids,undefined) of
        undefined -> false;
        Pid when is_pid(Pid) ->
            Pid ! {'$close',Opts},
            StateData1 = StateData#state_dlg{map=MapData#{asr_pids => lists:filter(fun({X,_}) -> X/=Layer end, AsrPids)}},
            case ?EU:to_bool(maps:get(result_text,Opts,true)) of
                false -> {ok,StateData1};
                true ->
                    case process_info(Pid,status) of
                        undefined -> {ok,StateData1};
                        {status,_} -> {async,StateData1}
                    end
            end
    end.

%% ---------------------
%% spawned start
%% ---------------------
loop_enter(OwnerPid,Opts) ->
    % ?OUT("Spawned: ~120tp", [self()]),
    SyncRef = make_ref(),
    case prepare_stream(SyncRef,OwnerPid,Opts) of
        {ok,LState} -> loop(SyncRef,Opts,LState);
        {error,_} ->
            ?B2BUA_DLG:setup_asr(maps:get(dialogid,Opts),remove,Opts),
            error
    end.

prepare_stream(SyncRef,OwnerPid,Opts) ->
    MonRef = erlang:monitor(process,OwnerPid),
    case get_mg_file_info(Opts) of
        false -> {error,mg_file_info};
        {ok,FileInfo} ->
            case refresh_asr() of
                false -> {error,asr_options};
                {ok,ASR} ->
                    LState = #{dialog_pid => OwnerPid,
                               mon_ref => MonRef,
                               fileinfo => FileInfo,
                               asr_options => ASR,
                               chunk_size => ?ChunkSize},
                    LState1 = switch_stage(LState,start_phrase),
                    case start_asr_channel(LState1) of
                        {error,_}=Err -> Err;
                        {ok,LState2} ->
                            start_asr_stream(SyncRef,LState2)
                    end
            end
    end.

%% -----
%% @private
get_mg_file_info(Opts) ->
    MGID = maps:get(mgid,Opts),
    MGFile = maps:get(file,Opts),
    case ?U:parse_mg_devicename(?U:get_mg_devicename(MGID)) of
        undefined -> false;
        {Addr,Postfix} ->
            RecPath = ?CFG:get_record_path(Addr,Postfix),
            SrcPath = filename:join([RecPath|lists:nthtail(1,filename:split(MGFile))]),
            case ?CFG:get_mg_node_by_addr_postfix(Addr,Postfix) of
                undefined -> false;
                {MGSite,MGNode} -> {ok,#{file => MGFile,
                                         site => MGSite,
                                         node => MGNode,
                                         recpath => RecPath,
                                         srcpath => SrcPath}}
            end end.

%% -----
%% @private
refresh_asr() ->
    MD = ?EU:to_binary(?CFG:get_general_domain()),
    case ?ENVDC:get_object_sticky(MD, settings, [{keys,[<<"record_asr_options">>]}], false, auto) of
        {ok,[Item],_} ->
            Value0 = maps:get(value,Item),
            Value = canonize_asr(Value0),
            case is_asr_ready(Value) of
                true -> {ok,Value};
                false -> false
            end;
        _ -> false
    end.

%% @private
canonize_asr(Asr) ->
    case maps:get(<<"type">>,Asr,<<>>) of
        <<"vosk">> ->
            case maps:get(<<"servers">>,Asr,[]) of
                [_|_]=Servers ->
                    F = fun(Srv) ->
                        Host = maps:get(<<"host">>,Srv,<<>>),
                        Port = maps:get(<<"port">>,Srv,0),
                        is_binary(Host) andalso Host/=<<>>
                            andalso is_integer(Port) andalso Port > 0 andalso Port < 65536
                        end,
                    Servers1 = lists:filter(F, Servers),
                    Asr#{<<"servers">> => Servers1};
                _ ->
                    Asr#{<<"servers">> => []}
            end;
        _ -> Asr
    end.

%% @private
is_asr_ready(Asr) ->
    case maps:get(<<"type">>,Asr,<<>>) of
        <<"vosk">> -> [] /= maps:get(<<"servers">>,Asr,[]);
        _ -> false
    end.

%% -----
%% @private
start_asr_channel(LState) ->
    AsrOptions = maps:get(asr_options,LState),
    Servers = maps:get(<<"servers">>,AsrOptions,[]),
    case start_asr_channel(Servers,erlang:phash2(Servers),false) of
        {ok,ChannelName}  -> {ok,LState#{channel_name => ChannelName}};
        {error,_}=Err -> Err
    end.

%% @private
start_asr_channel([],_,LastErr) -> LastErr;
start_asr_channel([Server|Rest],Phash2,_) ->
    ChannelName = ?EU:to_atom_new(?EU:strbin("~ts_~p", [?ChannelName, Phash2])),
    Host = maps:get(<<"host">>,Server,<<>>),
    Port = maps:get(<<"port">>,Server,0),
    %
    MapCh = #{channel_name => ChannelName,
              endpoints => [{http, ?EU:to_list(Host), Port, []}]},
    case ?MLSTT:vosk_start_channel(MapCh) of
        {ok,_} -> {ok,ChannelName};
        {error,{already_started,_}} -> {ok,ChannelName};
        {error,_}=Err -> start_asr_channel(Rest,Phash2,Err)
    end.

%% -----
%% @private
start_asr_stream(SyncRef,LState) ->
    Self = self(),
    FResult = fun(_StreamRef,Result) ->
                    % ?OUT(" ... received: ~120tp", [Result]),
                    Self ! {received,SyncRef,Result}
              end,
    ChannelName = maps:get(channel_name,LState),
    StreamMapOpts = #{channel_name => ChannelName,
                      send_result_cb => FResult,
                      stream_opts => #{sample_rate_hertz => 16000}},
    case ?MLSTT:vosk_start_stream(StreamMapOpts) of
        {ok,StreamRef} -> {ok,LState#{stream_ref => StreamRef}};
        {error,_}=Err -> Err
    end.

%% -----
%% @private
stop_asr_stream(LState) ->
    StreamRef = maps:get(stream_ref,LState),
    ?MLSTT:vosk_stop_stream(StreamRef).

%% ---------------------
%% spawned loop
%% LState :: #{dialog_pid, mon_ref, asr_options, channel_name, stream_ref, sync_ref, fileinfo, chunk_size}
%%   extra keys:
%%      buffer = <<>> ::binary(), % buffer to make chunk
%%      frombyte = 0 :: integer(), % read position in recorded file
%%      stage = start_phrase :: start_phrase|next|close|post|final,
%%          start_phrase - wait for first recv, do send without check ping-pong
%%          next - ping pong by send recv
%%          close - after media trunk removed, read N times for empty data from file and send if non empty.
%%          post - after read from file aborted.
%%          final - after stream_finished
%%      sent_bytes_total = 0 :: integer(), % Counter of totally sent bytes
%%      sent_bytes_start_phrase = 0 :: integer(), % for stage=start_phrase counter of sent bytes in start_phrase mode
%%      start_counter = 0 :: integer(), % on start helps to limit count of chunks sent without recv
%%      turn = send :: send|recv, % for stage=next. after sent chunk wait for receive result from asr service
%%      close_counter = 0 :: integer(), % on close helps to limit read_empty from file
%%      result_text = <<>> :: binary(), % total concatenated text on last phrase final
%%      candidate_text = <<>> :: binary(), % last sent concatenated text based on partials
%%      post_ts = 0 :: integer(), % when entered into post stage
%%      stop_opts :: map(), % Opts from stop command (used: reply_fun, result_text)

%% ---------------------
loop(SyncRef,Opts,LState) ->
    loop(SyncRef,Opts,LState,150).

loop(SyncRef,Opts,LState,Timeout) ->
    Stage = maps:get(stage,LState),
    %?OUT("ASR. loop... '~ts'",[Stage]),
    receive
        {'DOWN',MonRef,process,_,_} ->
            case maps:get(mon_ref,LState) of
                MonRef when Stage==start_phrase, Stage==next ->
                    ?MODULE:loop(SyncRef,Opts,switch_stage(LState,close));
                _ ->
                    ?MODULE:loop(SyncRef,Opts,LState)
            end;
        {received,SyncRef,Result} when Stage==post ->
            {ok,LState1} = apply_result(Result,Opts,LState),
            ?MODULE:loop(SyncRef,Opts,LState1,?PostTimeout + 100);
        {received,SyncRef,Result} ->
            {ok,LState1} = apply_result(Result,Opts,LState),
            ?MODULE:loop(SyncRef,Opts,LState1,0);
        {'$close',StopOpts} ->
            ?MODULE:loop(SyncRef,Opts,switch_stage(LState#{stop_opts => StopOpts},close));
        _ ->
            ?MODULE:loop(SyncRef,Opts,LState)
    after Timeout ->
        LState1 = case {Stage,maps:get(turn,LState,send)} of
                      {final,_} -> LState;
                      {post,_} -> LState;
                      {next,recv} -> LState;
                      {_,_} -> chunk(Opts,LState)
                  end,
        case maps:get(close_counter,LState1,0) of
            T when Stage==close, T >= ?FinalCounter ->
                LState2 = LState1#{close_counter => 0,
                                   post_ts => ?EU:timestamp()},
                stop_asr_stream(LState2),
                ?MODULE:loop(SyncRef,Opts,switch_stage(LState2,post));
            _ when Stage==post ->
                NowTS = ?EU:timestamp(),
                case maps:get(post_ts,LState1,0) of
                    PostTS when NowTS - PostTS >= ?PostTimeout -> send_async_reply(Opts,LState);
                    PostTS -> ?MODULE:loop(SyncRef,Opts,LState1,PostTS+?PostTimeout+100-NowTS)
                end;
            _ when Stage==final -> send_async_reply(Opts,LState);
            _ -> ?MODULE:loop(SyncRef,Opts,LState1)
        end
    end.

%% -----
%% @private
chunk(_Opts,LState) ->
    % ?OUT("ASR. chunk..."),
    case next_part(LState) of
        {ok,Data8,LState1} when size(Data8) == 0 ->
            case maps:get(stage,LState1) of
                close -> LState1#{close_counter => 1 + maps:get(close_counter,LState,0)};
                _ -> LState1
            end;
        {ok,Data8,LState1} ->
            Data16 = resample_8_to_16(Data8),
            {ok,LState2} = send_to_asr(Data16,LState1),
            LState2
    end.

%% -----
%% @private
next_part(LState) ->
    ChunkSize = maps:get(chunk_size,LState,?ChunkSize),
    Buffer = maps:get(buffer,LState,<<>>),
    case size(Buffer) of
        Size when Size >= ChunkSize ->
            Chunk = binary:part(Buffer,0,ChunkSize),
            {ok,Chunk,LState#{buffer => binary:part(Buffer,ChunkSize,Size-ChunkSize)}};
        Size ->
            MGFileInfo = maps:get(fileinfo,LState),
            FromByte = maps:get(frombyte,LState,0),
            Data = case read_file_endpart(MGFileInfo,FromByte) of
                       {ok,D} -> D;
                       _ -> 0
                   end,
            case size(Data) of
                T when Size+T==0 ->
                    {ok,<<>>,LState};
                T when Size+T > ChunkSize ->
                    next_part(LState#{buffer => <<Buffer/binary,Data/binary>>, frombyte => FromByte+T});
                T ->
                    Part = <<Buffer/binary,Data/binary>>,
                    case maps:get(stage,LState) of
                        close -> {ok,Part,LState#{buffer => <<>>, frombyte => FromByte+T}};
                        _ -> {ok,<<>>,LState#{buffer => Part, frombyte => FromByte+T}}
                    end
            end
    end.

%% @private
read_file_endpart(FileInfo,FromByte) -> read_file_endpart(FileInfo,FromByte,1).
read_file_endpart(FileInfo,FromByte,Attempts) ->
    [MGSite,MGNode,MGFilePath] = ?EU:maps_get([site,node,srcpath],FileInfo),
    case ?ENVCROSS:call_node({MGSite,MGNode}, {?EU, file_read_endpart, [MGFilePath,FromByte]}, false) of
        {ok,<<>>} when Attempts>0 -> read_file_endpart(FileInfo,FromByte,Attempts-1);
        T -> T
    end.

%% -----
%% @private
resample_8_to_16(Data) ->
    ?EU:to_binary(lists:reverse(element(2,lists:foldl(fun(B,{u,Acc}) -> {B,Acc}; (B,{A,Acc}) -> {u,[B,A,B,A|Acc]} end, {u,[]}, ?EU:to_list(Data))))).

%% -----
%% @private
send_to_asr(Chunk,LState) ->
    StreamRef = maps:get(stream_ref,LState),
    ?MLSTT:vosk_send_to_stream(StreamRef,Chunk),
    NewSentBytes = maps:get(sent_bytes_total,LState,0) + size(Chunk),
    NewStartPhraseSentBytes = maps:get(sent_bytes_start_phrase,LState,0) + size(Chunk),
    LState1 = LState#{sent_bytes_total => NewSentBytes,
                      sent_bytes_start_phrase => NewStartPhraseSentBytes},
    LState2 = case maps:get(stage,LState1) of
                  start_phrase when NewStartPhraseSentBytes > ?MaxBytesSentBeforeFirstRecvStartPrhase -> switch_stage(LState1,start_phrase); % next
                  _ -> LState1
              end,
    %?OUT("ASR.  --> Sent ~p bytes / ~p bytes total", [size(Chunk), maps:get(sent_bytes_total,LState2)]),
    {ok,switch_turn(LState2,recv)}.

%% -----
%% @private
apply_result(Result,Opts,LState) ->
    on_result(Result,Opts,switch_turn(LState,send)).

%% @private
on_result({ok,stream_finished},_Opts,LState) ->
    %?OUT("ASR.   <-- Rcv stream_finished!"),
    {ok,switch_stage(LState,final)};
on_result({error,_A,_B},_Opts,LState) ->
    ?LOG('$error', "  <-- Asr rcv error: ~120tp; ~1000tp",[_A,_B]),
    {ok,LState};
on_result({partial,ResponseData},Opts,LState) ->
    %?OUT("ASR.   <-- Rcv partial",[]),
    NewText = get_text(ResponseData,LState),
    send_event(NewText, Opts, LState),
    LState1 = LState#{candidate_text => NewText},
%%    LState2 = case maps:get(stage,LState1) of
%%                  start_phrase -> switch_stage(LState1,start_phrase); % next
%%                  _ -> LState1
%%              end,
    {ok,LState1};
on_result({final,ResponseData},Opts,LState) ->
    %?OUT("ASR.   <-- Rcv final", []),
    NewText = get_text(ResponseData,LState),
    send_event(NewText, Opts, LState),
    LState1 = LState#{result_text => NewText, candidate_text => NewText},
%%    LState2 = case maps:get(stage,LState1) of
%%                  start_phrase -> switch_stage(LState1,start_phrase);
%%                  next -> switch_stage(LState1,start_phrase);
%%                  _ -> LState1
%%              end,
    {ok,LState1}.

%% @private
get_text(ResponseData,LState) ->
    Text = maps:get(text,ResponseData),
    case maps:get(result_text,LState,<<>>) of
        <<>> -> Text;
        PrevText -> <<PrevText/binary,"\r\n",Text/binary>>
    end.

%% @private
send_event(Text, Opts, LState) ->
    case maps:get(candidate_text, LState, <<>>) of
        Text -> ok;
        _ ->
            %?OUT("ASR. Event: ~120tp", [Text]),
            case maps:get(event_to,Opts,undefined) of
                undefined -> ok;
                {websocket,[Conn,CapPath,EventName,DataMap]} ->
                    ?ENVCALL:cast_webserver(Conn,{CapPath,{EventName,DataMap#{<<"text">> => Text}}})
            end
    end.

%% @private
send_async_reply(_StartOpts, LState) ->
    StopOpts = maps:get(stop_opts,LState,#{}),
    %?OUT("ASR. send_async_reply: ~120tp", [StopOpts]),
    FReply = ?EU:get_by_key(replyfun,StopOpts,fun(_) -> ok end),
    case ?EU:to_bool(?EU:get_by_key(result_text,StopOpts,true)) of
        true -> FReply({ok,maps:get(result_text, LState, <<>>)});
        false -> ok
    end.

%% -----
%% @private
switch_turn(LState,Turn) when Turn==recv; Turn==send ->
    %?OUT("ASR. Turn to ~ts", [Turn]),
    LState#{turn => Turn}.

%% -----
%% @private
switch_stage(LState,Stage) when Stage==start_phrase; Stage==next; Stage==close; Stage==post; Stage==final ->
    %?OUT("ASR. Stage to ~ts", [Stage]),
    LState#{stage => Stage, sent_bytes_start_phrase => 0, close_counter => 0}.