%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author George Makarov <georgemkrv@gmail.com>, Peter Bukashin <tbotc@yandex.ru>
%%% @date 25.12.2018
%%% @doc Dialog bindings processing functions

-module(r_sip_b2bua_fsm_utils_bindings).
-author(['George Makarov <georgemkrv@gmail.com>','Peter Bukashin <tbotc@yandex.ru>']).

%% public interface. declared in era_sip module
-export([dialog_bindings/1]).

%% internal interface. used by b2bua_fsm module
-export([do_dialog_bindings/3]).

%% internal interface. used by router_invite_referred module
-export([dialog_inherit_bindings/2]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_b2b_invite_router.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------------------------
%% redirection to r_sip_b2bua_fsm_utils_bindings from era_sip
%% modification bindings on dialog
%% ------------------------------------------
-spec dialog_bindings({Op::atom(), CallOrDlgId::binary(), Label::binary()}) ->
                             ok | list() | boolean() | {error, Reason::term()}.
%% ------------------
dialog_bindings(Args) ->
    ?B2BUA_DLG:dialog_bindings(Args).

%% ------------------------------------------
%% binding processing main function for b2bua fsm
%% ------------------------------------------
-spec do_dialog_bindings(Op::atom(), State::#state_forking{} | #state_dlg{}, Label::binary()) ->
                                State::#state_forking{} | #state_dlg{}.
%% ------------------
do_dialog_bindings(add, Label, State) ->
    add_dialog_binding(Label, State);
do_dialog_bindings(setx, Label, State) ->
    setx_dialog_binding(Label, State);
do_dialog_bindings(remove, Label, State) ->
    remove_dialog_binding(Label, State);
do_dialog_bindings(cleanup, _Label, State) ->
    cleanup_dialog_bindings(State);
do_dialog_bindings(contains, Label, State) ->
    contains_dialog_binding(Label, State);
do_dialog_bindings(get, _Label, State) ->
    get_dialog_bindings(State).

%% ------------------------------------------
%% dialog inheritance when referring calls
%% called from r_sip_b2bua_route_invite_referred
%% ------------------------------------------
-spec dialog_inherit_bindings(DialogId::binary(), Ctx::map()) -> CtxN::map().
%% ------------------
dialog_inherit_bindings(DialogId, Ctx) ->
    case ?B2BUA_UTILS:extract_b2bua_server_index(DialogId) of
        false -> Ctx;
        {true, SrvIdx} ->
            case ?CFG:get_sipserver_by_index(SrvIdx) of
                undefined -> Ctx;
                {_Site, _Node}=Dest ->
                    case ?ENVCROSS:call_node(Dest, {?B2BUA_DLG, get_current_info, [DialogId]}, undefined) of
                        {ok,#{dlg_bindings:=undefined}=_Map} -> Ctx;
                        {ok,#{dlg_bindings:=BindingSet}=_Map} -> add_dialog_binding(sets:to_list(BindingSet), Ctx);
                        _ -> Ctx
                    end end end.

%% ====================================================================
%% Internal functions
%% ====================================================================


%% ----------------------------
%% adding binding to dialog from outer entities with unique key
%% all checks made in earlier stages
%% ----------------------------
-spec add_dialog_binding(Label::binary() | [binary()], State::#state_forking{} | #state_dlg{} | map()) ->
                                State::#state_forking{} | #state_dlg{} | map().
%% ------------------
%% for ctx
add_dialog_binding(BindingList, Ctx) when is_list(BindingList) andalso is_map(Ctx) ->
    lists:foreach(fun(E) -> raise_binding_event(Ctx, E, true) end, BindingList),
    update_binding_set(sets:union(get_binding_set(Ctx), sets:from_list(BindingList)), Ctx);
%% for #state_forking{} | #state_dlg{}
add_dialog_binding(BindingList, State) when is_list(BindingList) ->
    lists:foreach(fun(E) -> raise_binding_event(State, E, true) end, BindingList),
    update_binding_set(sets:union(get_binding_set(State), sets:from_list(BindingList)), State);
add_dialog_binding(Label, State) when is_binary(Label) ->
    raise_binding_event(State, Label, true),
    update_binding_set(sets:add_element(Label, get_binding_set(State)), State).

%% ----------------------------
%% setting new binding set to dialog from outer entities with unique exclusive key
%% all other bindings will be removed
%% ----------------------------
-spec setx_dialog_binding(Label::binary()|[binary()], State::#state_forking{} | #state_dlg{}) ->
                                 State::#state_forking{} | #state_dlg{}.
%% ------------------
setx_dialog_binding(Label, State) when is_binary(Label) ->
    setx_dialog_binding([Label], State);
setx_dialog_binding(BindingList, State) when is_list(BindingList) ->
    cleanup_dialog_bindings(State),
    lists:foreach(fun(E) -> raise_binding_event(State, E, true) end, BindingList),
    update_binding_set(sets:from_list(BindingList), State).

%% ----------------------------
%% removing binding from dialog
%% ----------------------------
-spec remove_dialog_binding(Label::binary() | [binary()], State::#state_forking{} | #state_dlg{}) ->
                                   State::#state_forking{} | #state_dlg{}.
%% ------------------
remove_dialog_binding(BindingList, State) when is_list(BindingList) ->
    lists:foreach(fun(E) -> raise_binding_event(State, E, false) end, BindingList),
    update_binding_set(sets:subtract(get_binding_set(State), sets:from_list(BindingList)), State);
remove_dialog_binding(Label, State) when is_binary(Label) ->
    raise_binding_event(State, Label, false),
    update_binding_set(sets:del_element(Label, get_binding_set(State)), State).

%% ----------------------------
%% cleaning all bindings from dialog
%% ----------------------------
-spec cleanup_dialog_bindings(State::#state_forking{} | #state_dlg{}) ->
                                     State::#state_forking{} | #state_dlg{}.
%% ------------------
cleanup_dialog_bindings(State) ->
    lists:foreach(fun(Key) -> raise_binding_event(State, Key, false) end, sets:to_list(get_binding_set(State))),
    update_binding_set(sets:new(), State).

%% ----------------------------
%% has (contains?) binding in dialog
%% ----------------------------
contains_dialog_binding(Label, State) ->
    sets:is_element(Label, get_binding_set(State)).

%% ----------------------------
%% retrieving bindings from dialog
%% ----------------------------
get_dialog_bindings(State) ->
    sets:to_list(get_binding_set(State)).

%% ----------------------------
%% extracting and storing BindSet in the DialogState...
%% ----------------------------

get_binding_set(#state_forking{map=Map}=_State) -> maps:get(dlg_bindings, Map);
get_binding_set(#state_dlg{map=Map}=_State) -> maps:get(dlg_bindings, Map);
get_binding_set(Ctx) when is_map(Ctx) -> maps:get(dlg_bindings, Ctx).

update_binding_set(BindSet, #state_forking{map =Map}=State) -> State#state_forking{map=maps:put(dlg_bindings, BindSet, Map)};
update_binding_set(BindSet, #state_dlg{map=Map}=State) -> State#state_dlg{map=maps:put(dlg_bindings, BindSet, Map)};
update_binding_set(BindSet, Ctx) when is_map(Ctx) -> Ctx#{dlg_bindings => BindSet}.

%% ----------------------------
%% event raising sugar
%% ----------------------------
raise_binding_event(State, Label, Op) ->
    ?FSM_EVENT:dlg_bindings(State, #{dlgbinding => Label, dlgbindingset => Op}).
