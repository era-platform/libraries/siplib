%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2022 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 01.07.2022
%%% @doc

-module(r_sip_b2bua_fsm_utils_dtmf).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    send_dtmf/2,
    dtmf_timeout/3,
    send_on_dlgstart/2
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_mgc.hrl").

-define(SendRfc2833EvtID, 1240).
-define(DtmfDuration,200).
-define(DtmfInterval,500).

%% ====================================================================
%% Public functions
%% ====================================================================

%% ----
%% Add dtmf symbol or sequence to side's dtmf2send queue
%% On
%% ----
send_dtmf(Opts,#state_dlg{a=#side{callid=ACallId}=A,b=#side{callid=BCallId}=B}=StateData) ->
    case ?EU:get_by_key(side,Opts,undefined) of
        ACallId -> send_dtmf(apply_opposite(Opts,{a,A},{a,A},{b,B}),Opts,StateData);
        BCallId -> send_dtmf({b,B},Opts,StateData);
        <<"a">> -> send_dtmf({a,A},Opts,StateData);
        <<"b">> -> send_dtmf({b,B},Opts,StateData);
        _ -> {error, {not_found, <<"Call/leg not found">>}}
    end.

%% ----
%% On timeout after last dtmf.
%% Async operation of send DTMF symbol from queue
%% Check next (first) symbol from queue of side
%% ----
dtmf_timeout([a,Ref],StateName,#state_dlg{a=#side{dtmf2send_ref=Ref}=Side}=StateData) ->
    send_next_dtmf({a,Side},StateName,StateData);
dtmf_timeout([b,Ref],StateName,#state_dlg{b=#side{dtmf2send_ref=Ref}=Side}=StateData) ->
    send_next_dtmf({b,Side},StateName,StateData);
dtmf_timeout(_,StateName,StateData) ->
    {next_state,StateName,StateData}.

%% ----
%% On dialog start. Send requested DTMF to B-side from INVITE's TO number after symbol ','
%% ----
send_on_dlgstart(_StateName,#state_dlg{b=#side{dtmf2send=[]}}=StateData) -> StateData;
send_on_dlgstart(StateName,#state_dlg{b=Side}=StateData) ->
    {next_state,_,StateData1} = send_next_dtmf({b,Side},StateName,StateData),
    StateData1.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ------------------------------------------------
%% @private
%% On append dtmf to send
%% ------------------------------------------------
apply_opposite(Opts,{SideLitera,Side},{a,A},{b,B}) ->
    case ?EU:to_bool(maps:get(opposite,Opts,false)) of
        false -> {SideLitera,Side};
        true when SideLitera==a -> {b,B};
        true when SideLitera==b -> {a,A}
    end.

%% @private
%send_dtmf({SideLitera,Side},Opts,StateData) -> do_send_dtmf({SideLitera,Side},Opts,StateData) % sync direct
send_dtmf({SideLitera,#side{dtmf2send=List,dtmf2send_timerref=TRef}=Side},Opts,#state_dlg{}=StateData) ->
    Side1 = Side#side{dtmf2send=List ++ [Opts]},
    Side2 = case List of
                [_|_] -> Side1;
                [] when is_reference(TRef) -> Side1;
                [] -> start_timer(100,SideLitera,Side1)
            end,
    StateData1 = case SideLitera of
                     a -> StateData#state_dlg{a=Side2};
                     b -> StateData#state_dlg{b=Side2}
                 end,
    {ok,StateData1}.

%% @private
start_timer(Timeout,SideLitera,Side) ->
    Ref = make_ref(),
    Side#side{dtmf2send_ref=Ref,
              dtmf2send_timerref=erlang:send_after(Timeout,self(),{dtmf_timeout,[SideLitera,Ref]})}.

%% -------------------------------------------------
%% @private
%% On dtmf timer (real send)
%% ------------------------------------------------
send_next_dtmf({SideLitera,#side{dtmf2send=[]}=Side},StateName,StateData) ->
    {next_state,StateName,set_side({SideLitera,Side#side{dtmf2send_timerref=undefined}},StateData)};
send_next_dtmf({SideLitera,#side{dtmf2send=[DtmfOpts|Rest],dtmf2send_timerref=TRef}=Side},StateName,StateData) ->
    ?EU:cancel_timer(TRef),
    Proto = maps:get(proto,DtmfOpts,<<"rfc2833">>),
    case maps:get(dtmf,DtmfOpts,undefined) of
        <<C:1/binary>> ->
            Side1 = Side#side{dtmf2send=Rest},
            do_send_next_dtmf({SideLitera,Side1},{C,Proto},StateName,StateData);
        <<C:1/binary,RestB/binary>> ->
            Side1 = Side#side{dtmf2send=[DtmfOpts#{dtmf => RestB}|Rest]},
            do_send_next_dtmf({SideLitera,Side1},{C,Proto},StateName,StateData);
        _ ->
            send_next_dtmf({SideLitera,#side{dtmf2send=Rest}},StateName,set_side({SideLitera,Side},StateData))
    end.

%% @private
do_send_next_dtmf({SideLitera,Side},{C,_}, StateName, StateData) when C== <<",">>; C== <<"'">> ->
    Side1 = start_timer(1000,SideLitera,Side),
    {next_state,StateName,set_side({SideLitera,Side1},StateData)};
do_send_next_dtmf({SideLitera,Side},{C,Proto}, StateName,StateData) ->
    Side1 = start_timer(300,SideLitera,Side),
    case do_send_dtmf({SideLitera,Side1}, {C,Proto}, set_side({SideLitera,Side1},StateData)) of
        {error,_} -> send_next_dtmf({SideLitera,Side1}, StateName, StateData);
        {ok,StateData1} -> {next_state,StateName,StateData1}
    end.

%% @private
set_side({a,Side},StateData) -> StateData#state_dlg{a=Side};
set_side({b,Side},StateData) -> StateData#state_dlg{b=Side}.

%% @private
do_send_dtmf({SideLitera,#side{remotesdp=RSdp}=Side}, {Dtmf,Proto0}, #state_dlg{}=StateData) ->
    case get_code(Dtmf) of
        {error,_}=Err -> Err;
        DtmfCode when is_binary(DtmfCode) ->
            Proto = case ?M_SDP:check_rfc2833(RSdp) of
                        false -> <<"sipinfo">>;
                        true -> Proto0
                    end,
            case Proto of
                <<"rfc2833">> -> send_dtmf_rfc2833({SideLitera,Side},DtmfCode,StateData);
                <<"sipinfo">> -> send_dtmf_sipinfo({SideLitera,Side},DtmfCode,StateData);
                _ -> send_dtmf_rfc2833({SideLitera,Side},DtmfCode,StateData)
            end end.

%% @private
send_dtmf_rfc2833(_,_,#state_dlg{media=undefined}) ->
    {error,{invalid_request,<<"Invalid state. No media.">>}};
send_dtmf_rfc2833({SideLitera,Side},DtmfCode,#state_dlg{}=StateData) ->
    Events = {?SendRfc2833EvtID, [{"rfc2833/send", [{"code",?EU:to_list(DtmfCode)}, {"duration",?DtmfDuration}] }] },
    case ?B2BUA_MEDIA:modify_term_events(SideLitera, Events, StateData) of
        {ok,StateData1} -> {ok,StateData1};
        %{error,_}=Err -> Err
        {error,_} -> send_dtmf_sipinfo({SideLitera,Side},DtmfCode,StateData) % fallback to sipinfo
    end.

%% @private
send_dtmf_sipinfo({_SideLitera,Side},DtmfCode,#state_dlg{}=StateData) ->
    #side{dhandle=DlgHandle}=Side,
    ContentType = <<"application/dtmf-relay">>,
    Body = <<"Signal=",DtmfCode/bitstring,"\r\nDuration=",(?EU:to_binary(?DtmfDuration))/binary>>,
    case catch nksip_uac:info(DlgHandle, [async,{content_type,ContentType},{body,Body}]) of
        {'EXIT',Err} -> {error,Err};
        {error,_R}=Err -> Err;
        {async,_ReqHandle} -> {ok,StateData}
    end.

%% @private
% normalize dtmf symbols
get_code(<<"*">>) -> <<"10">>;
get_code(<<"#">>) -> <<"11">>;
get_code(<<B>> =X) when B>=48, B=<57 -> X;
get_code(_) -> {error,{invalid_params,<<"Invalid DTMF symbol">>}}.