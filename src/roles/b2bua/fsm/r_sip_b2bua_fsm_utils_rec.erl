%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2022 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 20.06.2022
%%% @doc Extra recording

-module(r_sip_b2bua_fsm_utils_rec).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([setup_record/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_mgc.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%%% Start/stop extra recording of dialog
%%%   Cmd: add|remove
%%%   Opts: map()
%%%     layer :: binary() - term id
%%%     [side :: <<"a">> | <<"b">>] - topology
setup_record(Cmd,Opts,#state_dlg{invite_ts=ITS}=StateData) ->
    Layer = ?EU:strbin("rec_~ts", [?EU:get_by_key(layer,Opts,<<"default">>)]),
    case Cmd of
        'add' ->
            {_,RecPath} = ?U:get_media_context_rec_opts(ITS),
            File = filename:join(RecPath,?EU:str("rec_~tp.wav",[?EU:timestamp()])),
            OptsET = #{id => Layer,
                       termtype => 'rec',
                       file => ?EU:to_binary(File),
                       type => <<"raw">>,
                       buffer_duration => 8000,
                       codec_name => <<"L16/8000">>},
            case ?B2BUA_MEDIA:external_term_attach(OptsET,StateData) of
                {error,_}=Err -> {ok,Err,StateData};
                {Reply,StateData1} -> % ok | {ok,LSdp}
                    % setup topology
                    case setup_topology(Layer,Opts,StateData1) of
                        {error,_}=Err -> {ok,Err,StateData1};
                        {ok,#state_dlg{media=Media}=StateData2} ->
                            #media{mgc=MGC,msid=MSID,mgid=MG,ctx=CtxId}=Media,
                            Opts1 = Opts#{id=>Layer,file=>File,mg=>{MGC,MSID,MG,CtxId}},
                            {ok,StateData3} = setup_timer(Opts1,StateData2),
                            {ok, Reply, StateData3}
                    end
            end;
        'remove' ->
            OptsET = #{id => Layer},
            case ?B2BUA_MEDIA:external_term_detach(OptsET,StateData) of
                {error,_}=Err -> {ok,Err,StateData};
                {Reply,StateData1} -> % ok | {ok,LSdp}
                    {ok, Reply, StateData1}
            end
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -------------
setup_topology(Layer,Opts,#state_dlg{a=#side{callid=ACallId},b=#side{callid=BCallId}}=StateData) ->
    case ?EU:get_by_key(side,Opts,undefined) of
        ACallId -> do_setup_topology(Layer,a,Opts,StateData);
        BCallId -> do_setup_topology(Layer,b,Opts,StateData);
        <<"a">> -> do_setup_topology(Layer,a,Opts,StateData);
        <<"b">> -> do_setup_topology(Layer,b,Opts,StateData);
        _ -> {ok,StateData}
    end.

%% @private
do_setup_topology(Layer,Side,_Opts,StateData) ->
    [IsolateSide] = [a,b] -- [Side],
    OptsET = #{id=>Layer,topology=>[{Side,t,oneway},{IsolateSide,t,isolate}]}, % TODO: other prompts?
    ?B2BUA_MEDIA:external_term_setup_topology(OptsET,StateData).

%% ------------
setup_timer(Opts,#state_dlg{dialogid=DlgId}=StateData) ->
    case ?EU:to_int(?EU:get_by_key(timeout,Opts,0),0) of
        I when I > 0, I < 7200000 ->
            spawn(fun() -> timer:sleep(I), stop(DlgId,Opts) end);
        _ -> ok
    end,
    {ok,StateData}.

%% @private
stop(DlgId,Opts) ->
    ?B2BUA_DLG:external_term_detach(DlgId,Opts),
    % TODO: get file,
    % TODO: mix file
    % TODO: send file
    ok.