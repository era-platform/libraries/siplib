%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Authenticate service:
%%%      - skip by request method,
%%%        - skip if dialog or registration found
%%%        - build realm for 407 response,
%%%         - use radius,
%%%        - r esg
%%%        - setup border filter if auth failed

-module(r_sip_b2bua_auth).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([get_password_lazy/2,
         sip_authorize/3,
         extract_gate_rcvfrom/1,
         extract_rcvfrom/1]).

%% ==========================================================================
%% Types
%% ==========================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_headers.hrl").
-include("../include/r_sip_b2b.hrl").

% -include("../include/r_nksip.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ----------------------------------------
%% Obtains password for username to nksip.
%%  NkSip expects <<"PlainPassword">> or <<"HA1!", HA1/binary>>.
%%  Also 'radius' and {lazy_pass, FunLazyPass} are supported (by Peter).
%% --------------------------------------
get_password_lazy(User, Realm)
  when is_binary(User) andalso is_binary(Realm) ->
    FunLazyPass = fun() ->
                          [_|Domain] = binary:split(Realm, <<"__">>),
                          case ?U:call_domaincenter(?EU:to_binary(Domain), fun() -> {get_sipuser_pwd, User} end, undefined) of
                              {ok,_Pass}=Ok -> Ok;
                              false -> {error, <<"B2B. Auth invalid prerequisites (1)">>}; %false;
                              undefined -> {error, <<"B2B. Auth DC not found">>};
                              _ -> {error, <<"B2B. Auth DC response error">>}
                          end end,
    {lazy_pass, FunLazyPass}.

%% -------------------------------------------------
%% - We first check to see if the request is an in-dialog request, coming from
%%   the same ip and port of a previously authorized request.
%% - If not, we check if we have a previous authorized REGISTER request from
%%   the same ip and port.
%% - Next, we check if the request has a valid authentication header with realm=from_uri_domain
%%   If `{{digest, Domain::binary()}, true}' is present, the user has
%%   provided a valid password and it is authorized.
%%   If `{{digest, Domain::binary()}, false}' is present, we have presented
%%   a challenge, but the user has failed it. We send 403.
%% - {{digest, {lazy, Fun}}} could be used to lazy get password (added by Peter)
%% - {{digest, {radius, ListOfAuthParams}}} could be used to external authentication (added by Peter)
%% - If no digest header is present, reply with a 407 response sending
%%   a challenge to the user.
%% -------------------------------------------------
sip_authorize(_Auth, #sipmsg{class={req,'ACK'}}=_Req, _Call) ->
    % Zoiper works over TCP, registration is on port 50416, but invite comes from 60197. Contact and Via = 50416.
    % Authorization is done over 60197. His ACK on 4xx comes from 60197, but ACK on 2xx comes from 50416.
    % This is not authorized. So we need auth, but ACK does not support auth, and ACK lacks.
    % First question is why it sends invite from unregistered port.
    % Second question is why do we answer on remote port but not on Via port. Though it's ack wouldn't be authorized still.
    ok;
sip_authorize(_Auth, #sipmsg{class={req,'BYE'}}=_Req, _Call) ->
    % Zoiper works over TCP, but sends BYE over UDP during REFER,
    % There is no authorized dialog for UDP transport, so Response should be proxy_auth
    % But nksip uses 407 response to stop invite dialog (no filter on 2xx only).
    ok;
sip_authorize(Auth, Req, Call) ->
    case nksip_request:header(?OwnerHeaderLow, Req) of
        {_,[<<"rEG-", SrvTextCode:24/bitstring>>|_]} ->
            sip_authorize_esg(SrvTextCode, Req);
        _ ->
            {ok, Method} = nksip_request:method(Req),
            %Aprior = lists:member(dialog, Auth) orelse (lists:member(register, Auth) andalso Method=/='REGISTER'),
            %?OUT("AUTH ~120p : ~120p => ~120p", [Method, Req#sipmsg.call_id, Auth]),
             %case Method=='INVITE' of
             %    true -> ?OUT("AUTH ~120p : ~120p => ~120p", [Method, Req#sipmsg.call_id, Auth]);
             %    false -> ok
             %end,
            Aprior = lists:member(dialog, Auth) orelse lists:member(register, Auth),
            case Aprior of
                true -> ok;
                false when Method=='OPTIONS' -> ok;
                false ->
                    #sipmsg{from={#uri{domain=Domain},_FromTag}}=Req,
                    Realm = build_realm(Domain),
                    sip_authorize_realm(?U:get_value({digest, Realm}, Auth), Realm, Req, Call)
            end
    end.

%% -------------------------------------------------
%% #149 Extract gate's "x-era-rcvfrom" header value
%%   (used in nksip_registrar_lib and local)
%%
%% return rcv addr from request's header (included by gate)
%% --------------------------------------
-spec extract_gate_rcvfrom(Req::#sipmsg{}) -> GateRcvFrom :: undefined | {Proto::atom(),IpAddr::tuple(),Port::integer()}.
%% --------------------------------------
extract_gate_rcvfrom(Req) ->
    case nksip_request:header(?RcvFromHeaderLow,Req) of
        {ok,[Vs|_]} ->
            [BTransp,BIp,BPort] = binary:split(Vs,[<<"<">>,<<">">>,<<":">>],[global,trim_all]),
            case inet:parse_address(binary_to_list(BIp)) of
                {ok,Ip} -> {binary_to_atom(BTransp,utf8),Ip,binary_to_integer(BPort)};
                _ -> undefined
            end;
        _ -> undefined
    end.

%% --------------------------------------
%% return rcv addr from request
%% --------------------------------------
-spec extract_rcvfrom(Req::#sipmsg{}) -> RcvFrom :: {Proto::atom(),IpAddr::tuple(),Port::integer()}.
%% --------------------------------------
extract_rcvfrom(Req) ->
    #sipmsg{nkport=NkPort}=Req,
     #nkport{transp=Transp, remote_ip=Ip, remote_port=Port}=NkPort,
    {Transp,Ip,Port}.

%% ====================================================================
%% Internal functions
%% ====================================================================

build_realm(Domain) ->
    BinSrvIdx = ?EU:to_binary(?CFG:get_my_sipserver_index()),
    <<"b2b_", BinSrvIdx/bitstring, "__", Domain/bitstring>>.

%% --------------------------------------
%% When authenticate should be done
%%   NOTE! lazy function is defined in nksip_auth, no in accountstore.
%% --------------------------------------
sip_authorize_realm(Res, Realm, Req, Call) ->
    case Res of
        true -> % Password is valid
            ok;
        {lazy_auth, FunLazyAuth} -> % Lazy check (LDAP) optimize
            sip_authorize_realm(FunLazyAuth(), Realm, Req, Call);
        {radius, AuthParams} -> % External RADIUS check
            sip_authorize_radius(AuthParams);
        _ -> % Other (attempt to find noreg account)
            NoRegR = try_noreg(Req),
            sip_authorize_realm_1(NoRegR, Res, Realm, Req, Call)
    end.

%% other results after attempt to find noreg account
sip_authorize_realm_1(ok,_,_,_,_) -> ok;
sip_authorize_realm_1(false, Res, Realm, Req, Call) -> sip_authorize_realm_2(Res, Realm, Req, Call);
sip_authorize_realm_1(Res, _, Realm, Req, Call) -> sip_authorize_realm_2(Res, Realm, Req, Call).

%% other results
sip_authorize_realm_2(Res, Realm, Req, Call) ->
    case Res of
        false -> % User has failed authentication (dc ok, user ok, pwd not match)
            Reason = <<"B2B. Auth invalid prerequisites (2)">>,
            log_failed_reg(Reason, Req), % #308
            ?FILTER:auth_failed(Req, Call),
            {forbidden, Reason};
        {error,Reason} when is_binary(Reason) -> % some error (auth failed, dc absent, dc response error)
            ?FILTER:auth_failed(Req, Call),
            log_failed_reg(Reason, Req), % #308
            {forbidden, Reason};
        undefined -> % No auth header
            {authenticate, Realm}; % #237 authenticate, proxy_authenticate
        invalid -> % Invalid nonce, retry
            {authenticate, Realm} % #237 authenticate, proxy_authenticate
    end.

%% @private #308
log_failed_reg(Reason, #sipmsg{cseq={_,'REGISTER'}, call_id=CallId, from={FUri,_}, to={TUri,_}}=Req) ->
    case extract_gate_rcvfrom(Req) of
        {RcvProto, RcvAddr, RcvPort} -> ok;
        undefined -> {RcvProto, RcvAddr, RcvPort} = extract_rcvfrom(Req)
    end,
    _From = ?U:unparse_uri((?U:clear_uri(FUri))#uri{disp= <<>>}),
    To = ?U:unparse_uri((?U:clear_uri(TUri))#uri{disp= <<>>}),
    Contact = case Req of
                  #sipmsg{contacts=[CUri|_]} -> ?U:unparse_uri(?U:clear_uri(CUri));
                  _ -> undefined
              end,
    UA = try {ok,[XUAV|_]} = nksip_request:header("user-agent", Req),
             [XUA|_] = ?U:parse_uris(XUAV),
             XUA
         catch _:_ -> <<>>
         end,
    ?BLlog:write({sip,rgf}, {"Rcv=~s:~s:~p; To=~ts; Contact=~ts; UA='~ts'; Reason='~ts'; Call-Id='~ts'",[RcvProto,inet:ntoa(RcvAddr),RcvPort,To,Contact,UA,Reason,CallId]});
log_failed_reg(_,_) -> ok.

%% ----------------------------------
%% attempts to find noreg account by recvaddr
%% --------------------------------------
-spec try_noreg(Req::#sipmsg{}) -> ok | false | {error,Reason::binary()}.
%% --------------------------------------
try_noreg(Req) ->
    case extract_gate_rcvfrom(Req) of
        undefined -> false;
        {_,_,_}=GateRcvAddr ->
            case ?NOREG:find(GateRcvAddr) of
                false -> false;
                {ok,[_|_]=G} -> check_noreg(G,Req)
            end end.
%% --------------------------------------
-spec check_noreg(G::[{U::binary(),D::binary()}], Req::#sipmsg{}) -> ok | {error,Reason::binary()}.
%% --------------------------------------
check_noreg(G,Req) ->
    #sipmsg{from={#uri{user=FU,domain=FD},_}}=Req,
    case inet:parse_address(?EU:to_list(FD)) of
        {ok,_} when length(G)>1 -> {error, <<"B2B. RecvAddr is NoReg. Non unique (1)">>};
        {ok,_} -> ok;
        _ ->
            case ?LOCALDOMAIN:is_cluster_domain(FD) of
                false -> {error, <<"B2B. Unknown domain">>};
                true ->
                    case lists:member({FU,FD},G) of
                        true -> ok;
                        false ->
                            case length(lists:filter(fun({_,D}) -> D==FD end, G)) of
                                1 -> ok;
                                0 -> {error, <<"B2B. RecvAddr is NoReg. Domain not found">>};
                                _ -> {error, <<"B2B. RecvAddr is NoReg. Non unique (2)">>}
                            end end end end.

%% --------------------------------------
%% When radius authenticate was defined
%% --------------------------------------
sip_authorize_radius(AuthParams) ->
    ?OUT("sip_authorize_radius ~p ~n", [AuthParams]),
    ok.

%% --------------------------------------
%% When esg-role header detected
%% --------------------------------------
sip_authorize_esg(SrvTextCode, Req) ->
    SrvIdx = ?EU:parse_textcode_to_index(SrvTextCode),
    #sipmsg{call_id=CallId,
            to={#uri{user=User,domain=Domain},_},
            cseq={_,Method}}=Req,
    Request = {?EXT_REGSRV,rpc_check_auth,[User,Domain,CallId]},
    case ?CFG:get_sipserver_by_index(SrvIdx) of
        undefined -> forbidden;
        {_Site, _Node}=Dest ->
            case ?ENVCROSS:call_node(Dest, Request, forbidden) of
                {true,_} -> ok;
                true -> ok;
                false when Method=='NOTIFY' -> ok;
                false -> forbidden;
                forbidden -> forbidden
            end
    end.
