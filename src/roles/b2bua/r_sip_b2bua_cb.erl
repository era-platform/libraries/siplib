%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc
%%% @todo

-module(r_sip_b2bua_cb).
-author('Peter Bukashin <tbotc@yandex.ru>').

%% -------------------------------------------------------------------
%%
%% -------------------------------------------------------------------

-export([start/0, stop/0]).
-export([handle_call/3, handle_cast/2, handle_info/2]).
-export([init/1, sip_get_user_pass/4, sip_authorize/3, sip_route/5,
         %sip_register/2, sip_update/2, sip_publish/2,
         sip_invite/2, sip_reinvite/2, sip_cancel/3, sip_ack/2, sip_bye/2, sip_refer/2,
         sip_subscribe/2, sip_resubscribe/2, sip_notify/2,
         sip_info/2, sip_message/2, sip_options/2,
         sip_update/2,
         sip_dialog_update/3,
         sip_session_update/3]).

-compile([export_all,nowarn_export_all]).

%% ==========================================================================
%% Defines
%% ==========================================================================

%% ==========================================================================
%% Types
%% ==========================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_mgc.hrl").

% -include("../include/r_nksip.hrl").
-include_lib("nkserver/include/nkserver_module.hrl").
-include("../_build/default/lib/nksip/include/nksip_registrar.hrl").

-record(state, {
    auto_check
}).

%% ==========================================================================
%% API functions
%% ==========================================================================

%% --------------------------------
%% @doc Starts a new SipApp, listening on port 5060 for udp and tcp and 5061 for tls,
%% and acting as a registrar.
%%
start() ->
    ?SUPV:start_child({?B2BUA_SUPV, {?B2BUA_SUPV, start_link, []}, permanent, 1000, supervisor, [?B2BUA_SUPV]}),
    %
    {ok,AppOpts} = ?APP:get_opts(?MODULE),
    ?SIPSTORE:store_u('local_contact_port', ?U:parse_contact_port(AppOpts)),
    ?SIPSTORE:store_u('local_ports', ?U:parse_ports(AppOpts)),
    ?SIPSTORE:store_u('use_srtp', ?U:parse_use_srtp(AppOpts)),
    ?SIPSTORE:store_u(?CHECKLIMIT, ?U:is_check_by_options_on_limit()),
    %
    Plugins = [%r_plug_b2bua,
               %r_plug_localdomain,
               %r_plug_bestinterface,
               %r_plug_filter,
               %r_plug_log,
               %
               nksip_uac_auto_auth,
               nksip_registrar
               % nksip_outbound,
               % nksip_timers,
               % nksip_refer,
               % nksip_100rel,
               % nksip_gruu
               ],
    CoreOpts = #{plugins => lists:filter(fun(false) -> false; (_) -> true end, Plugins),
                %certfile => <<"">>, %{certfile, code:priv_dir(era_sip) ++ "/ssl/server.crt"},
                %{keyfile, <<"">>}, %{keyfile, code:priv_dir(era_sip) ++ "/ssl/server.key"},
                %
                sip_allow => "REGISTER, INVITE, ACK, CANCEL, BYE, REFER, NOTIFY, OPTIONS, INFO, SUBSCRIBE, MESSAGE, UPDATE", % UPDATE
                sip_supported => "path",
                sip_events => "dialog, presence, message-summary",
                sip_no_100 => not ?Auto100answer, % test
                sip_listen => ?U:parse_transports(AppOpts)},
    nksip:start_link(?MODULE, CoreOpts).

%% --------------------------------
%% @doc Stops the SipApp.
%%
stop() ->
    ?SUPV:terminate_child(?B2BUA_SUPV),
    ?SUPV:delete_child(?B2BUA_SUPV),
    nksip:stop(?SIPAPP).

%% --------------------------------
%% @doc Updates role configuration opts
%%
update_opts(Opts) ->
    ?SIPSTORE:store_u(?CHECKLIMIT, ?U:is_check_by_options_on_limit()),
    case ?U:check_update_opts(Opts) of
        restart -> restart;
        ok -> ok
    end.

%% ==================================================================================
%% Callback functions
%% ==================================================================================

%% ----------------------------------------------------------------------------------
%% @doc SipApp Callback: Initialization.
%% ----------------------------------------------------------------------------------

init([]) ->
    {ok, #state{auto_check=false}}.

%% ----------------------------------------------------------------------------------
%% @doc SipApp Callback: Called to check user's password.
%% ----------------------------------------------------------------------------------

sip_get_user_pass(User, Realm, _Req, _Call) ->
    ?AUTH:get_password_lazy(User, Realm).

%% ----------------------------------------------------------------------------------
%% @doc SipApp Callback: Called to check if a request should be authorized.
%% ----------------------------------------------------------------------------------

sip_authorize(Auth, Req, Call) ->
    % if request was forwarded from another b2bua server, then no need to authorize
    case check_sender_is_insider(Req) of
        false -> ?AUTH:sip_authorize(Auth, Req, Call);
        true -> ok
    end.

% @private
check_sender_is_insider(Req) ->
    #sipmsg{vias=[#via{domain=ViaD,port=ViaP}|_], nkport=#nkport{remote_ip=RemoteIp}}=Req,
    BViaP = ?EU:to_binary(ViaP),
    Sender = <<ViaD/bitstring, ":", BViaP/bitstring>>,
    ViaD==?EU:to_binary(inet:ntoa(RemoteIp)) andalso check_sender_is_insider_1(Sender).
check_sender_is_insider_1(AddrPort) ->
    Key = <<"all_insider_addrs">>,
    Fun = fun(Items) -> lists:member(AddrPort, Items) end,
    % ESG could be authenticated by direct call, or by direct check destination's ip:port
    Fbuild = fun() -> lists:flatten([AddrPort1 || {_,AddrPort1} <- ?CFG:get_all_b2bua() ++ ?CFG:get_all_conf() ++ ?CFG:get_all_ivr() ++ ?CFG:get_all_esg() ++ ?CFG:get_all_prompt()]) end,
    FunStore = fun(undefined) -> Fbuild(); (V) -> V end,
    FunReply = fun(_,R) -> Fun(R) end,
    ?SIPSTORE:func_t(Key, 3000, FunStore, FunReply).

%% ----------------------------------------------------------------------------------
%% @doc SipApp Callback: Called to decide how to route every new request.
%% ----------------------------------------------------------------------------------

% The request is for one of our users or a SUBSCRIBE
sip_route(_Scheme, _User, <<"224.0.1.75">>, _Req, _Call) -> block; % auto provision broadcast
sip_route(Scheme, User, Domain, Req, Call) ->
    #call{call_id=CallId}=Call,
    #sipmsg{cseq={_,Method}}=Req,
    % -------
    ?U:start_monitor_simple(self(), "Sip b2b call"),
    % -------
    ?LOGSIP("sip_route ~120p, ~120p, ~120p", [Method, self(), CallId]),
    ?STAT_FACADE:incoming_request(Req), % @stattrace
    ?STAT_FACADE:link_domain_filter(Method,Req#sipmsg.call_id,(element(1,Req#sipmsg.to))#uri.domain), % RP-415
    %
    case ?U:is_local_ruri(Req) of
        true -> route_inside_domain(Scheme, User, Domain, Req, Call);
        false ->
            % #140 30.03.2017
            #sipmsg{ruri=#uri{domain=RUriDomain}}=Req,
            case ?SIPSTORE:find_t(?GateIface(RUriDomain)) of
                {_,true} -> route_inside_domain(Scheme, User, Domain, Req, Call);
                false when Method=='INVITE' -> % referred calls
                    case nksip_request:header("replaces", Req) of
                        {ok,[_|_]} -> route_inside_domain(Scheme, User, Domain, Req, Call);
                        _ -> route_outside_domain(Scheme, User, Domain, Req, Call)
                    end;
                false -> route_outside_domain(Scheme, User, Domain, Req, Call)
            end end.

%% ----------------------
route_outside_domain(_Scheme, _UserR, _DomainR, Req, _Call) ->
    Method = nksip_request:method(Req),
    % OUT("ROUTE OUTSIDE ~p", [Method]),
    case Method of
%%         {ok, 'REGISTER'} -> {reply, ?Forbidden("B2B. Router disabled")};
%%         {ok, 'SUBSCRIBE'} -> {reply, ?Forbidden("B2B. Router disabled")};
%%         {ok, 'PUBLISH'} -> {reply, ?Forbidden("B2B. Router disabled")};
%%         %
%%         {ok, 'INVITE'} -> {proxy, ruri, [record_route]};
%%         {ok, 'REFER'} -> {proxy, ruri, []};
%%         {ok, 'NOTIFY'} -> {proxy, ruri, []};
%%         %
%%         {ok, 'INFO'} -> {proxy, ruri, []};
%%         {ok, 'MESSAGE'} -> {proxy, ruri, []};
%%         {ok, 'ACK'} -> {proxy, ruri, []};
%%         {ok, 'BYE'} -> {proxy, ruri, []};
%%         {ok, 'CANCEL'} -> {proxy, ruri, []};
%%         {ok, 'OPTIONS'} -> {proxy, ruri, []};
%%         {ok, 'UPDATE'} -> {proxy, ruri, []};
%%         %
%%         _ -> {proxy, ruri}
        _ -> {reply, ?Forbidden("B2B. Unknown domain, router disabled")}
    end.

%% ----------------------
route_inside_domain(Scheme, User, Domain, Req, Call) ->
    #sipmsg{cseq={_,Method}, call_id=CallId, ruri=RUri}=Req,
    % ?OUT("ROUTE INSIDE ~p, ~p", [Method, ?SIPSTORE:find_t(CallId)]),
    % check prefix numbers (pickup, conference, ...) if found - find responsible b2b, if another - then proxy with no record_route
    case Method /= 'INVITE' andalso ?SIPSTORE:find_t(CallId) of
        {_,{?ProxyToInsider, SrvIdx}} ->
            proxy_to_insider(SrvIdx, RUri, Req);
        _ ->
            route_as_b2bua(Scheme, User, Domain, Req, Call)
    end.

%%
route_as_b2bua(Scheme, User, Domain, Req, _Call) ->
    Method = nksip_request:method(Req),
    case Method of
        {ok, 'REGISTER'} ->
            case check_limit() of true -> ?SIPSTORE:store_t(?CHECKLIMIT_K(self()), Req, 2000); R -> R end,
            process;
        % ----------------
        {ok, 'INVITE'} -> process;
        {ok, 'CANCEL'} -> process;
        {ok, 'ACK'} -> process;
        {ok, 'BYE'} -> process;
        % ----------------
        {ok, 'REFER'} -> process;
        % ----------------
        {ok, 'NOTIFY'} -> process;
        {ok, 'SUBSCRIBE'} -> process;
        {ok, 'PUBLISH'} -> {reply, ?MethodNotAllowed("B2B")};
        % ----------------
        {ok, 'OPTIONS'} -> process;
        {ok, 'INFO'} ->    process;
        {ok, 'MESSAGE'} -> proxy(Scheme, User, Domain, Req);
        % ----------------
        {ok, 'UPDATE'} -> process; % {reply, ?MethodNotAllowed("B2B")};
        % ----------------
        _ -> {proxy, ruri, [record_route]}
    end.

%% ---------------------
proxy(Scheme, User, Domain, Req) ->
    case ?CALLEE:get_callee(map, {Scheme, User, Domain}) of
        {reply, Reason}=Reply ->
            ?OUT("sip_route reply ~p, ~p, ~p, ~p, ~p", [nksip_request:method(Req), Scheme, User, Domain, Reason]),
            Reply;
        [_|_]=Maps ->
            ?OUT("sip_route proxing ~p, ~p, ~p, ~p", [nksip_request:method(Req), Scheme, User, Domain]),
            Uris = lists:foldr(fun F(Item,Acc) when is_map(Item) -> [maps:get(uri,Item)|Acc];
                                   F(List,Acc) when is_list(List) -> lists:foldr(F,Acc,List)
                               end, [], Maps),
            {proxy, Uris, [record_route,follow_redirects]}
    end.

%%
proxy_to_insider(SrvIdx, RequestUri, Req) ->
    #sipmsg{call_id=CallId}=Req,
    case ?SERVERS:find_responsible_sip_srvidx(SrvIdx, CallId) of
        false -> {reply, ?ServiceUnavailable("B2B. Pickup dialog responsible server not found")};
        RouteDomain ->
            %?OUT("PROXY TO INSIDER SRV: ~p ~p -> ~p", [SrvIdx, CallId, RouteDomain]),
            % when proxing to insiders - stored by callid for next aprior routing
            ?SIPSTORE:store_t(CallId, {?ProxyToInsider, SrvIdx}, 3 * 3600 * 1000),
            RouteUri = {route, ?U:unparse_uri(#uri{scheme=sip, domain=RouteDomain, opts=?CFG:get_transport_opts()++[<<"lr">>]})},
            {proxy, RequestUri, [record_route,RouteUri]}
    end.

%% ----------------------------------------------------------------------------------
%% dialog_update
%% ----------------------------------------------------------------------------------

-spec(sip_dialog_update(DialogStatus, Dialog::nksip:dialog(), Call::nksip:call()) ->
        ok
        when DialogStatus :: start | target_update | stop |
                         {invite_status, nksip_dialog:invite_status() | {stop, nksip_dialog:stop_reason()}} |
                         {invite_refresh, SDP::nksip_sdp:sdp()} |
                         {subscription_status, nksip_subscription:status(), nksip:subscription()}).

sip_dialog_update(stop, #dialog{id=_DialogId}=_Dialog, _Call) ->
    {ok, _DialogId1} = nksip_dialog:get_handle(_Dialog),
    %?LOGSIP("   DLG. sip_dialog_update(stop, ~p, ~p)", [DialogId, _DialogId1]),
    ok;

sip_dialog_update({stop, _StopReason}, #dialog{id=_DialogId}=_Dialog, _Call) ->
    {ok, _DialogId1} = nksip_dialog:get_handle(_Dialog),
    %?LOGSIP("   DLG. sip_dialog_update(stop [~p], ~p, ~p)", [_StopReason, DialogId, _DialogId1]),
    ok;

sip_dialog_update(_, _Dialog, _Call) ->
    ok.

%% ----------------------------------------------------------------------------------
%% session_update
%% ----------------------------------------------------------------------------------
-spec(sip_session_update(SessionStatus, Dialog::nksip:dialog(), Call::nksip:call()) ->
        ok
        when SessionStatus :: {start, Local, Remote} | {update, Local, Remote} | stop,
                          Local::nksip_sdp:sdp(), Remote::nksip_sdp:sdp()).

sip_session_update(_, _Dialog, _Call) ->
    ok.

%% ----------------------------------------------------------------------------------
%% INVITE
%% ----------------------------------------------------------------------------------
-spec(sip_invite(Request::nksip:request(), Call::nksip:call()) ->
        {reply, nksip:sipreply()} | noreply).

sip_invite(Req, _Call) ->
    #sipmsg{call_id=CallId}=Req,
    AOR = extract_ruri(Req),
    ?LOGSIP("sip_invite ~1000tp, ~1000tp, ~1000tp", [self(), CallId, AOR]),
    case ?U:extract_sdp(Req) of
        undefined ->
            {reply, ?MediaNotSupported("B2B. No SDP")}; % TODO RP-956
        #sdp{} ->
            ?U:send_100_trying(Req),
            ?B2BUA_ROUTER_INVITE:sip_invite(AOR,Req)
    end.

%% ----------------------------------------------------------------------------------
%% reINVITE
%% ----------------------------------------------------------------------------------
-spec(sip_reinvite(Request::nksip:request(), Call::nksip:call()) ->
        {reply, nksip:sipreply()} | noreply).

sip_reinvite(Req, _Call) ->
    #sipmsg{call_id=CallId}=Req,
    AOR = extract_ruri(Req),
    ?LOGSIP("sip_reinvite ~120p, ~120p, ~120p", [AOR, self(), CallId]),
    case ?U:extract_sdp(Req) of
        undefined -> {reply, ?MediaNotSupported("B2B. No SDP")};
        #sdp{} ->
            case ?B2BUA_DLG:pull_fsm_by_callid(CallId) of
                false -> {reply, ?CallLegTransDoesNotExist("B2B. Unknown Dialog")};
                DlgX ->
                    ?U:send_100_trying(Req),
                    ?B2BUA_DLG:sip_reinvite(DlgX, [CallId, Req]),
                    noreply
            end
    end.

%% ----------------------------------------------------------------------------------
%% CANCEL
%% ----------------------------------------------------------------------------------
-spec(sip_cancel(InviteRequest::nksip:request(),
                 Request::nksip:request(),
                 Call::nksip:call()) -> ok).

sip_cancel(InviteReq, Req, Call) ->
    #call{call_id=CallId}=Call,
    ?LOGSIP("sip_cancel ~120p, ~120p", [self(), CallId]),
    case ?B2BUA_DLG:pull_fsm_by_callid(CallId) of
        false -> ok;
        DlgX -> ?B2BUA_DLG:sip_cancel(DlgX, [CallId, Req, InviteReq]), ok
    end.

%% ----------------------------------------------------------------------------------
%% ACK
%% ----------------------------------------------------------------------------------
-spec(sip_ack(Request::nksip:request(), Call::nksip:call()) -> ok).

sip_ack(Req, Call) ->
    #call{call_id=CallId}=Call,
    ?LOGSIP("sip_ack ~120p, ~120p", [self(), CallId]),
    case ?B2BUA_DLG:pull_fsm_by_callid(CallId) of
        false -> ok;
        DlgX ->
            ?B2BUA_DLG:sip_ack(DlgX, [CallId, Req]),
            ok
    end.

%% ----------------------------------------------------------------------------------
%% BYE
%% ----------------------------------------------------------------------------------
-spec(sip_bye(Request::nksip:request(), Call::nksip:call()) ->
        {reply, nksip:sipreply()} | noreply).

sip_bye(Req, Call) ->
    #call{call_id=CallId}=Call,
    ?LOGSIP("sip_bye ~120p, ~120p", [self(), CallId]),
    case ?B2BUA_DLG:pull_fsm_by_callid(CallId) of
        false -> {reply, ok};
        DlgX ->
            ?B2BUA_DLG:sip_bye(DlgX, [CallId, Req]),
            noreply
    end.

%% ----------------------------------------------------------------------------------
%% REFER
%% ----------------------------------------------------------------------------------
-spec(sip_refer(Request::nksip:request(), Call::nksip:call()) ->
    {reply, nksip:sipreply()} | noreply).

sip_refer(Req, Call) ->
    #call{call_id=CallId}=Call,
    ?LOGSIP("sip_refer ~120p, ~120p", [self(), CallId]),
    case ?B2BUA_DLG:pull_fsm_by_callid(CallId) of
        false -> {reply, ?InternalError("B2B. Unknown dialog")};
        DlgX ->
            ?U:send_100_trying(Req),
            ?B2BUA_DLG:sip_refer(DlgX, [CallId, Req]),
            noreply
    end.

%% ----------------------------------------------------------------------------------
%% UPDATE
%% ----------------------------------------------------------------------------------
%% -spec(sip_update(Req::nksip:request(), Call::nksip:call()) ->
%%     {reply, nksip:sipreply()} | noreply).
%%
%% sip_update(_Req, _Call) ->
%%     ?LOGSIP("sip_update"),
%%     {reply, ?MethodNotAllowed("B2B")}.

%% ----------------------------------------------------------------------------------
%% SUBSCRIBE
%% ----------------------------------------------------------------------------------
-spec(sip_subscribe(Request::nksip:request(), Call::nksip:call()) ->
        {reply, nksip:sipreply()} | noreply).

sip_subscribe(Req, Call) ->
    #call{call_id=CallId}=Call,
    ?LOGSIP("sip_subscribe ~120p, ~120p", [self(), CallId]),
    ?SUBSCRIPTIONS:subscribe(Req, Call, true).

%% ----------------------------------------------------------------------------------
%% reSUBSCRIBE
%% ----------------------------------------------------------------------------------
-spec(sip_resubscribe(Request::nksip:request(), Call::nksip:call()) ->
        {reply, nksip:sipreply()} | noreply).

sip_resubscribe(Req, Call) ->
    #call{call_id=CallId}=Call,
    ?LOGSIP("sip_resubscribe ~120p, ~120p", [self(), CallId]),
    ?SUBSCRIPTIONS:subscribe(Req, Call, false).

%% ----------------------------------------------------------------------------------
%% NOTIFY
%% ----------------------------------------------------------------------------------
-spec(sip_notify(Request::nksip:request(), Call::nksip:call()) ->
        {reply, nksip:sipreply()} | noreply).

sip_notify(Req, Call) ->
    #call{call_id=CallId}=Call,
    ?LOGSIP("sip_notify ~120p, ~120p", [self(), CallId]),
    case ?B2BUA_DLG:pull_fsm_by_callid(CallId) of
        false -> {reply, ?Forbidden("B2B. Unknown dialog")};
        DlgX ->
            ?B2BUA_DLG:sip_notify(DlgX, [CallId, Req]),
            noreply
    end.

%% ----------------------------------------------------------------------------------
%% PUBLISH
%% ----------------------------------------------------------------------------------
%% -spec(sip_publish(Request::nksip:request(), Call::nksip:call()) ->
%%         {reply, nksip:sipreply()} | noreply).
%%
%% sip_publish(_Req, _Call) ->
%%     ?LOGSIP("sip_publish"),
%%     {reply, ?MethodNotAllowed("B2B")}.

%% ----------------------------------------------------------------------------------
%% INFO
%% ----------------------------------------------------------------------------------
-spec(sip_info(Req::nksip:request(), Call::nksip:call()) ->
        {reply, nksip:sipreply()} | noreply).

sip_info(Req, Call) ->
    #call{call_id=CallId}=Call,
    ?LOGSIP("sip_info ~120p, ~120p", [self(), CallId]),
    case ?B2BUA_DLG:pull_fsm_by_callid(CallId) of
        false -> {reply, ?Forbidden("B2B. Unknown dialog")};
        DlgX ->
            ?B2BUA_DLG:sip_info(DlgX, [CallId, Req]),
            noreply
    end.

%% ----------------------------------------------------------------------------------
%% MESSAGE
%% ----------------------------------------------------------------------------------
-spec(sip_message(Request::nksip:request(), Call::nksip:call()) ->
        {reply, nksip:sipreply()} | noreply).

sip_message(Req, Call) ->
    #call{call_id=CallId}=Call,
    ?LOGSIP("sip_message ~120p, ~120p", [self(), CallId]),
    case ?B2BUA_DLG:pull_fsm_by_callid(CallId) of
        false -> {reply, ?Forbidden("B2B. Unknown dialog")};
        DlgX ->
            ?B2BUA_DLG:sip_message(DlgX, [CallId, Req]),
            noreply
    end.

%% ----------------------------------------------------------------------------------
%% OPTIONS
%% ----------------------------------------------------------------------------------
-spec(sip_options(Request::nksip:request(), Call::nksip:call()) ->
        {reply, nksip:sipreply()} | noreply).

sip_options(Req, Call) ->
    #call{call_id=CallId}=Call,
    ?LOGSIP("sip_options ~120p, ~120p", [self(), CallId]),
    PutSdp = case nksip_request:header(<<"accept">>, Req) of
                 {ok, []} -> true;
                 {ok, Accept} -> find_in_binary(Accept, <<"application/sdp">>);
                 {error,_} -> false
             end,
    Opts = [{replace, {<<"Allow">>, <<"REGISTER, INVITE, ACK, CANCEL, BYE, REFER, OPTIONS, NOTIFY, SUBSCRIBE, INFO, MESSAGE">>}}, % <<"PUBLISH">>
            {replace, {<<"Allow-Events">>, <<"refer, presence, dialog">>}}, % <<"kpml(Zoiper) talk,hold,conferencecheck_sync(Yealink)">>
            % {replace, {<<"Accept-Encoding">>, <<"">>}},
            % {replace, {<<"Accept-Language">>, <<"en">>}},
            {replace, {<<"Supported">>, <<"replaces, timer, path">>}} % <<"outbound, norefersub(Zoiper), extended-refer(Zoiper), gruu(JsSIP)">>
            ],
    Opts1 = case PutSdp of
                true ->
                    [{replace, {<<"Accept">>, <<"application/sdp">>}},
                     {replace, {<<"Content-Type">>, <<"application/sdp">>}},
                     {body, ?M_SDP:make_options_sdp()}
                     |Opts];
                false -> Opts
            end,
    {reply, {200,Opts1}}.

% --
find_in_binary([], _Key) -> false;
find_in_binary([Item|Rest], Key) ->
    case binary:match(?EU:to_lower(Item), Key) of
        nomatch -> find_in_binary(Rest, Key);
        {_From,_Count} -> true
    end.

%% ----------------------------------------------------------------------------------
%% UPDATE
%% ----------------------------------------------------------------------------------
-spec(sip_update(Request::nksip:request(), Call::nksip:call()) ->
        {reply, nksip:sipreply()} | noreply).

sip_update(Req, _Call) ->
    #sipmsg{call_id=CallId}=Req,
    AOR = extract_ruri(Req),
    ?LOGSIP("sip_update ~120p, ~120p, ~120p", [AOR, self(), CallId]),
    case ?U:extract_sdp(Req) of
        undefined -> {reply, ?Ok200("B2B. No SDP")};
        #sdp{} ->
            case ?B2BUA_DLG:pull_fsm_by_callid(CallId) of
                false -> {reply, ?CallLegTransDoesNotExist("B2B. Unknown Dialog")};
                DlgX ->
                    ?B2BUA_DLG:sip_update(DlgX, [CallId, Req]),
                    noreply
            end
    end.

%% ----------------------------------------------------------------------------------
%% REGISTER
%% ----------------------------------------------------------------------------------
%-spec(sip_register(Request::nksip:request(), Call::nksip:call()) ->
%        {reply, nksip:sipreply()} | noreply).
%
%sip_register(_Req, _Call) ->
%    ?LOGSIP("sip_register"),
%    {reply, ok}.

%% ----------------------------------------------------------------------------------
%% @doc Registrar Callback: Synchronous user call.
%% ----------------------------------------------------------------------------------

-spec(sip_registrar_store( StoreOp :: {get, AOR} |
                                         {get_minmax, AOR} |
                                         {put, AOR, [RegContact :: nksip_registrar:reg_contact()], TTL :: integer()} |
                                         {del, AOR} |
                                         {del_contacts, AOR, DeletedRegContactIndexes :: [nksip_registrar_lib:index()]} |
                                         del_all,
        AppId::nksip:app_id())    -> {reply, term(), term()} | ok | {error,SipReply::{SipCode::integer(),SipReason::term()}}).

sip_registrar_store({get, AOR}, _AppId) ->
    ?REGISTRAR:get(AOR);

% #191
sip_registrar_store({get_minmax, AOR}, _AppId) ->
    ?REGISTRAR:get_minmax(AOR);

sip_registrar_store({put, AOR, Contacts, TTL}, _AppId) ->
    ?REGISTRAR:put(AOR, Contacts, TTL);

sip_registrar_store({del, AOR}, _AppId) ->
    ?REGISTRAR:del(AOR);

sip_registrar_store({del_contacts, AOR, ContactIndexes}, _AppId) ->
    ?REGISTRAR:del_contacts(AOR, ContactIndexes);

sip_registrar_store(del_all, _AppId) ->
    ?REGISTRAR:del_all().

%% ----------------------------------------------------------------------------------
%% @doc sip_b2bua plugin callback
%% ----------------------------------------------------------------------------------

%% % ------
%% b2bua({uac_pre_response, [#sipmsg{cseq={_,M}}=Resp,_UAC,_Call]=Args}=_Query, _AppId) when M=='INVITE'; M=='REFER' ->
%%     {ok,CallId} = nksip_response:call_id(Resp),
%%     case ?B2BUA_DLG:pull_fsm_by_callid(CallId) of
%%         false -> {continue, Args};
%%         DlgX -> {continue, [Resp,_UAC,_Call1]} = ?B2BUA_DLG:uac_pre_response(DlgX, Args)
%%     end;
%% b2bua({uac_pre_response, Args}, _AppId) ->
%%     {continue, Args};

% ------
b2bua({uac_response, [#sipmsg{cseq={_,M}}=Resp]=Args}=_Query, _AppId) when M=='INVITE'; M=='REFER' ->
    {ok,CallId} = nksip_response:call_id(Resp),
    case ?B2BUA_DLG:pull_fsm_by_callid(CallId) of
        false -> continue;
        DlgX -> ?B2BUA_DLG:uac_response(DlgX, Args)
    end;
b2bua({uac_response, _Args}, _AppId) ->
    continue;

% ------
b2bua({uas_dialog_response, [#sipmsg{cseq={_,M}}=Resp]=Args}=_Query, _AppId) when M=='INVITE'; M=='REFER'; M=='UPDATE' ->
    {ok,CallId} = nksip_response:call_id(Resp),
    case ?B2BUA_DLG:pull_fsm_by_callid(CallId) of
        false -> continue;
        DlgX -> ?B2BUA_DLG:uas_dialog_response(DlgX, Args)
    end;
b2bua({uas_dialog_response, _Args}, _AppId) ->
    continue;

% ------
b2bua({call_terminate, [Reason,Call]=Args}=_Query, _AppId) ->
    #call{call_id=CallId}=Call,
    case ?B2BUA_DLG:pull_fsm_by_callid(CallId) of
        false ->
            % when proxing to insiders - it stored by callid for routing
            ?SIPSTORE:delete_t(CallId),
            ok;
        DlgX ->
            ?LOGSIP("B2BUA. call_terminate: ~p, ~p", [CallId,Reason]),
            ?B2BUA_DLG:call_terminate(DlgX,Args),
            ok
    end,
    % clear from central storage link to this responsible.
    %  only if general CallId, not forwarded.
    case ?B2BUA_UTILS:extract_callid(CallId) of
        CallId -> ?SERVERS:delete_stored_responsible(CallId);
        _ -> ok
    end,
    ok;

% ------
b2bua({uas_totag, [#sipmsg{from={_,FTag}}=_Req]}=Query, AppId) ->
    case ?B2BUA_UTILS:build_tag() of
        FTag -> b2bua(Query, AppId);
        T -> T
    end.

%% ----------------------------------------------------------------------------------
%% @doc sip_log plugin callback
%% ----------------------------------------------------------------------------------

sip_log(Query, AppId) ->
    ?LOGGING:sip_log(Query, AppId).

%% ----------------------------------------------------------------------------------
%% @doc sip_filter plugin callback
%% ----------------------------------------------------------------------------------

sip_filter(Query, AppId) ->
    ?FILTER:transport_filter(internal, Query, AppId).

%% ----------------------------------------------------------------------------------
%% @doc Domain Callback: Synchronous user call.
%% ----------------------------------------------------------------------------------

sip_checkclusterdomain(Domain, _AppId) ->
    ?LOCALDOMAIN:is_cluster_domain(Domain).

%% ----------------------------------------------------------------------------------
%% @doc SipApp Callback: Synchronous user call.
%% ----------------------------------------------------------------------------------

handle_call(_, _, _) ->
    continue.

%% ----------------------------------------------------------------------------------
%% @doc SipApp Callback: Asynchronous user cast.
%% ----------------------------------------------------------------------------------

handle_cast(_,_) ->
    continue.
%% ----------------------------------------------------------------------------------
%% @doc SipApp Callback: External erlang message received.
%% ----------------------------------------------------------------------------------

handle_info(_, _) ->
    continue.

%% ==========================================================================
%% Internal functions
%% ==========================================================================

% -------------------------------
% extracts ruri from request (could be shadowed by route header)
%
extract_ruri(Req) ->
    case ?ROUTE:get_top_route_uri(Req) of
        #uri{scheme=S, user=U, domain=D} ->
            % Real User and Domain from route
            {S, U, D};
        notfound ->
            % RURI User and Domain
            #sipmsg{ruri=#uri{scheme=S, user=U, domain=D}}=Req,
            {S, U, D}
    end.

%% --------------------------------
%% @doc flag 'check_by_options_on_limit' in role configuration opts
%%
-spec check_limit() -> true | false.

check_limit() ->
    case ?SIPSTORE:find_u(?CHECKLIMIT) of
        {_,Value} -> Value;
        _ ->
            CheckLimit = ?U:is_check_by_options_on_limit(),
            ?SIPSTORE:store_u(?CHECKLIMIT, CheckLimit),
            CheckLimit
    end.
