%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>, Peter Bukashin <tbotc@yandex.ru>
%%% @date 07.06.2018
%%% @doc RP-682 context operations for calls

-module(r_sip_b2bua_context).
-author(['Evgeniy Grebenyuk <llceceron@gmail.com>','Peter Bukashin <tbotc@yandex.ru>']).

-export([start_ctx_script/5]).
-export([stop/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ---------------------------------------------
%% Start context svcscript for call
%% ---------------------------------------------
-spec start_ctx_script(DomainType::master|domain,IID::binary(),ScrCode::binary(),Domain::binary(),OtherOpts::map()) -> binary() | undefined.
%% ---------------------------------------------
start_ctx_script(master,IID,ScrCode,_Domain,OtherOpts) ->
    do_start_ctx_script(IID,ScrCode,?EU:to_binary(?CFG:get_general_domain()),OtherOpts);
start_ctx_script(domain,IID,ScrCode,Domain,OtherOpts) ->
    do_start_ctx_script(IID,ScrCode,Domain,OtherOpts).

%% @private
do_start_ctx_script(IID,ScrCode,Domain,OtherOpts) ->
    case ?EnvRoleU:select_role_node(Domain,'script') of
        undefined -> undefined;
        Dest ->
            StartOpts = #{<<"code">> => ScrCode,
                          <<"domain">> => Domain,
                          <<"startparam1">> => IID,
                          <<"startparam2">> => maps:get(callid,OtherOpts),
                          <<"startparam3">> => maps:get(from,OtherOpts),
                          <<"startparam4">> => maps:get(to,OtherOpts),
                          <<"startparam5">> => jsx:encode(OtherOpts),
                          <<"use_owner_down_strategy">> => poststart,
                          %<<"use_owner">> => true,
                          <<"owner_pid">> => self(), % pid of nksip call
                          <<"owner_dest">> => {?CFG:get_current_site(), node()}}, % dest of current node
            case ?ENVCROSS:call_node(Dest, {?APP_SCRIPT, start_svcscript, [StartOpts]}, {error,<<"unavailable">>}, 2000) of
                {ok,StartInfo} -> maps:get(id,StartInfo);
                {error,_} -> undefined
            end end.

%%
stop(#state_forking{map=Map}) -> stop(Map);
stop(#state_dlg{map=Map}) -> stop(Map);
stop(Map) when is_map(Map) ->
    % only domain ctx could be on other site
    case maps:get(ctx_scrid_domain,Map,undefined) of
        undefined -> ok;
        CtxScrIdD ->
            SrvIdx = ?EU:parse_uuid_srvidx(CtxScrIdD),
            Dest = ?ENVCFG:get_node_by_index(SrvIdx),
            ?ENVCROSS:call_node(Dest, {?APP_SCRIPT, sm_stop, [CtxScrIdD, {'stop', 'owner_down'}]}, 5000)
    end.
