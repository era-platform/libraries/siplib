%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Facade to media services
%%%        Handles every request to media (mgc/mg) from b2bua.
%%%         Powered by internal media component (mgc_libs/*) and mgc/mg

-module(r_sip_b2bua_media).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_mgc.hrl").

% -include("../include/r_nksip.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ---------------------------------
%% checks if media is available (mgc-mg pair, or no media is need)
%% ---------------------------------
-spec check_available(State::#state_forking{}) -> {ok, true|false} | {error,Reason::term()}.
%% ---------------------------------
check_available(#state_forking{use_media=false}=_State) -> {ok, true};
check_available(State) ->
    ?MGC:mg_check_any_available(get_mg_opts(State)).

%% ---------------------------------
%% checks if media is available and return count of  (mgc-mg pair, or no media is need)
%% ---------------------------------
-spec check_available_slots(State::#state_forking{}|#state_dlg{}) -> {ok, MGCnt::integer(), CtxCnt::integer()} | {error,Reason::term()}.
%% ---------------------------------
check_available_slots(#state_forking{use_media=false}=_State) -> {ok, 10, 10};
check_available_slots(State) ->
    ?MGC:mg_check_available_slots(get_mg_opts(State)).


%% ---------------------------------
%% returns map with current media properties (mgc/mg/ctxid)
%% ---------------------------------
-spec get_current_media_link(State::#state_forking{}|#state_dlg{}) -> {ok, Map::map()} | undefined.
%% ---------------------------------
get_current_media_link(State) ->
    case State of
        #state_dlg{media=Media} -> ok;
        #state_forking{media=Media} -> ok
    end,
    case Media of
        undefined -> undefined;
        #media{mgc=MGC, msid=MSID, mgid=MG, ctx=CtxId} -> {ok, #{mgc=>MGC, msid=>MSID, mgid=>MG, ctx=>CtxId}}
    end.

%% ---------------------------------
%% prepare media by creating new context (first participant)
%% ---------------------------------
-spec prepare_start(State::#state_forking{}) -> {ok, NewState::#state_forking{}} | {error, SipReply::{SipCode::integer(),SipOpts::list()}}.
%% ---------------------------------
prepare_start(#state_forking{use_media=false}=State) -> {ok, State};

prepare_start(#state_forking{}=State) ->
    #state_forking{dialogid=DialogId,
                   a=#side{remotesdp=RSdp},
                   invite_ts=ITS,
                   map=#{init_req:=Req,
                         opts:=Opts}}=State,
    case ?M_SDP:check_known_fmt(RSdp) of
        false ->
            d(State, "MGC error: Not supported media"),
            {error, ?MediaNotSupported("B2B.F. Unknown audio formats")};
        true ->
            <<B4:32/bitstring, _/bitstring>> = ?U:luid(),
            MSID = <<DialogId/bitstring, "_", B4/bitstring>>,
            UseRecord = case ?ENVLIC:is_rec_allowed() of
                            false -> false; % RP-362
                            true -> ?EU:get_by_key(use_record,Opts,true)
                        end,
            MediaOpts = #{transcoding => true, % transcoding enabled (allowed)
                          rec => UseRecord, % record by opts (default true)
                          timestamp => ITS,
                          mg_opts => get_mg_opts(State)},
            {Mks,Res} = timer:tc(fun() -> ?MEDIA_B2B:fwd_start_media(MSID, Req, MediaOpts) end),
            d(State, " .. prepare_media (~p ms)", [Mks/1000]),
            case Res of
                {reply, SipReply} when is_tuple(SipReply) ->
                    d(State, "MGC error: create media context failure"),
                    {error, SipReply};
                {ok, Media} ->
                    State1 = State#state_forking{media=Media},
                    {ok, State1}
            end
    end.

%% ---------------------------------
%% stops existing media context
%% ---------------------------------
-spec stop(State::#state_forking{}|#state_dlg{}) -> {ok, NewState::#state_forking{}|#state_dlg{}}.
%% ---------------------------------
stop(#state_forking{use_media=false}=State) -> {ok,State};
stop(#state_forking{media=undefined}=State) -> {ok,State};
stop(#state_dlg{media=undefined}=State) -> {ok,State};
stop(#state_forking{media=Media}=State) -> ?MEDIA_B2B:stop_media(Media), {ok,State#state_forking{media=undefined}};
stop(#state_dlg{media=Media}=State) -> ?MEDIA_B2B:stop_media(Media), {ok,State#state_dlg{media=undefined}}.



%% ---------------------------------
%% prepare forward request from a to b, make local sdp for b
%% ---------------------------------
-spec fwd_invite_request_to_b(State::#state_forking{}, BUri::#uri{}, BCallId::binary()) ->
                {ok, NewState::#state_forking{}, LSdp::#sdp{}} | {error, Reason::term()}.
%% ---------------------------------
fwd_invite_request_to_b(#state_forking{use_media=false}=State, _BUri, _BCallId) ->
    #state_forking{a=#side{remotesdp=ARSdp}}=State,
    {ok, State, ARSdp};

fwd_invite_request_to_b(State, BUri, BCallId) ->
    #state_forking{media=Media}=State,
    case ?MEDIA_B2B:fwd_invite_request_to_b(Media, BUri, BCallId) of
        {error,_}=Err -> Err;
        {ok,Media1,LSdp} ->
            {ok, State#state_forking{media=Media1}, LSdp}
    end.

%% ---------------------------------
%% prepare forward response from b to a, make local sdp for a
%% ---------------------------------
-spec fwd_invite_response_to_a(State::#state_forking{}, Resp::#sipmsg{}) -> {ok, NewState::#state_forking{}, LSdp::#sdp{}} | {error, Reason::term()}.
%% ---------------------------------
fwd_invite_response_to_a(#state_forking{use_media=false}=State, #sipmsg{}=Resp) ->
    #sdp{}=BRSdp=?U:extract_sdp(Resp),
    {ok, State, BRSdp};

fwd_invite_response_to_a(State, Resp) ->
    #state_forking{media=Media}=State,
    case ?MEDIA_B2B:fwd_invite_response_to_a(Media, Resp) of
        {error,_}=Err -> Err;
        {ok,Media1,LSdp} ->
            {ok, State#state_forking{media=Media1}, LSdp}
    end.

%% ---------------------------------
%% prepare pickup call, makes local sdp for caller(A) and pickuper(B)
%% ---------------------------------
-spec apply_invite_pickup_from_b(State::#state_forking{}, Req::#sipmsg{}) -> {ok, NewState::#state_forking{}, ALSdp::#sdp{}, BLSdp::#sdp{}} | {error, Reason::term()}.
%% ---------------------------------
apply_invite_pickup_from_b(#state_forking{use_media=false}=State, #sipmsg{}=Req) ->
    BRSdp=?U:extract_sdp(Req),
    #state_forking{a=#side{remotesdp=ARSdp}}=State,
    {ok, State, BRSdp, ARSdp};

apply_invite_pickup_from_b(State, Req) ->
    #state_forking{media=Media}=State,
    case ?MEDIA_B2B:apply_invite_pickup_from_b(Media, Req) of
        {error,_}=Err -> Err;
        {ok,Media1,ALSdp,BLSdp} ->
            {ok, State#state_forking{media=Media1}, ALSdp, BLSdp}
    end.

%% ---------------------------------
%% apply ack's remote sdp
%% ---------------------------------
-spec apply_remote_ext(CallId::binary(), RSdp::#sdp{}, State::#state_dlg{}) -> {ok, NewState::#state_dlg{}} | {error, Reason::term()}.
%% ---------------------------------
apply_remote_ext(_CallId, _RSdp, #state_dlg{use_media=false}=State) ->
    {ok, State};

apply_remote_ext(CallId, RSdp, State) ->
    #state_dlg{media=Media}=State,
    Side = case State of
               #state_dlg{a=#side{callid=CallId}} -> a;
               #state_dlg{b=#side{callid=CallId}} -> b
           end,
    case ?MEDIA_B2B:update_term_by_remote(Media, Side, RSdp) of
        {error, Reason} ->
            d(State, "MGC error: ~120p", [Reason]),
            {error, Reason};
        {ok, Media1} ->
            d(State, " -> modified local term"),
            State1 = State#state_dlg{media=Media1},
            {ok, State1}
    end.

%% ---------------------------------
%% prepare forward reinvite request from x to y, make local sdp for y
%% ---------------------------------
-spec fwd_reinvite_request_to_y(State::#state_dlg{}, Uri::#uri{}) -> {ok, NewState::#state_dlg{}, LSdp::#sdp{}} | {error, Reason::term()}.
%% ---------------------------------
fwd_reinvite_request_to_y(#state_dlg{use_media=false}=State, #sipmsg{}=Req) ->
    #sdp{}=XRSdp=?U:extract_sdp(Req),
    {ok, State, XRSdp};

fwd_reinvite_request_to_y(State, Req) ->
    #state_dlg{media=Media}=State,
    case ?MEDIA_B2B:fwd_reinvite_request_to_y(Media, Req) of
        {error,_}=Err -> Err;
        {ok,Media1,LSdp} ->
            {ok, State#state_dlg{media=Media1}, LSdp}
    end.

%% ---------------------------------
%% prepare forward reinvite response from y to x, make local sdp for x
%% ---------------------------------
-spec fwd_reinvite_response_to_x(State::#state_dlg{}, Uri::#uri{}) -> {ok, NewState::#state_dlg{}, LSdp::#sdp{}} | {error, Reason::term()}.
%% ---------------------------------
fwd_reinvite_response_to_x(#state_dlg{use_media=false}=State, #sipmsg{}=Resp) ->
    #sdp{}=YRSdp=?U:extract_sdp(Resp),
    {ok, State, YRSdp};

fwd_reinvite_response_to_x(State, Resp) ->
    #state_dlg{media=Media}=State,
    case ?MEDIA_B2B:fwd_reinvite_response_to_x(Media, Resp) of
        {error,_}=Err -> Err;
        {ok,Media1,LSdp} ->
            {ok, State#state_dlg{media=Media1}, LSdp}
    end.

%% ---------------------------------
%% rollbacks reinvite response on 4xx-6xx (mg y term could be already modified (local sdp))
%% ---------------------------------
-spec rollback_reinvite(State::#state_dlg{}) -> {ok, NewState::#state_dlg{}} | {error, Reason::term()}.
%% ---------------------------------
rollback_reinvite(#state_dlg{use_media=false}=State) ->
    {ok, State};

rollback_reinvite(State) ->
    #state_dlg{media=Media}=State,
    case ?MEDIA_B2B:rollback_reinvite(Media) of
        {error,_}=Err -> Err;
        {ok,Media1} ->
            {ok, State#state_dlg{media=Media1}}
    end.

%% ---------------------------------
%% checks if MGC/MG pair, linked in current state data corresponds to args
%% ---------------------------------
-spec media_check_mg(State::#state_dlg{}|#state_forking{}, Args::list()) -> true | false.
%% ---------------------------------
media_check_mg(#state_forking{use_media=false}=_State, _Args) -> false;
media_check_mg(#state_dlg{use_media=false}=_State, _Args) -> false;
media_check_mg(State, Args) ->
    ?MEDIA_B2B:check_media_mg(get_media(State), Args).


%% ---------------------------------
%% update Context by adding/removing ivr termination with player
%% ---------------------------------
-spec update_wait_melody(State::#state_dlg{}, Req::#sipmsg{}, Resp::#sipmsg{}) -> {ok, NewState::#state_dlg{}} | {error, Reason::term()}.
%% ---------------------------------
update_wait_melody(#state_dlg{use_media=false}=State, _Req, _Resp) -> {ok,State};
update_wait_melody(#state_dlg{}=State, #sipmsg{}=Req, #sipmsg{}=Resp) ->
    case is_prompt(State) orelse is_conf(State) of
        true -> {ok, State};
        false -> do_update_wait_melody(State, Req, Resp)
    end.

%% @private
do_update_wait_melody(#state_dlg{media=Media}=State, #sipmsg{}=Req, #sipmsg{}=Resp) ->
    #sdp{}=SdpX=?U:extract_sdp(Req),
    #sdp{}=SdpY=?U:extract_sdp(Resp),
    IsEnabled = case Media of
                    #media{ivr=undefined} -> false;
                    _ -> true
                end,
    DoEnable = case {?M_SDP:get_audio_mode(SdpX), ?M_SDP:get_audio_mode(SdpY)} of
                   {<<"sendonly">>, _} -> true;
                   {_, <<"sendonly">>} -> true;
                   _ -> false
               end,
    case {IsEnabled,DoEnable} of
        {A,A} -> {ok, State};
        {_,true} ->
            case ?MEDIA_B2B:ivr_attach(Media) of
                {error,_}=Err -> Err;
                {ok,Media1} ->
                    {ok, State#state_dlg{media=Media1}}
            end;
        {_,false} ->
            case ?MEDIA_B2B:ivr_detach(Media) of
                {error,_}=Err -> Err;
                {ok,Media1} ->
                    {ok, State#state_dlg{media=Media1}}
            end
    end.

%% @private
is_prompt(#state_dlg{a=#side{remoteuri=#uri{user=A}},b=#side{remoteuri=#uri{user=B}}}) ->
    lists:any(fun(U) -> case ?U:parse_prompt_user(U) of false -> false; _ -> true end end, [A,B]).

%% @private
is_conf(#state_dlg{a=#side{remoteuri=#uri{user=A}},b=#side{remoteuri=#uri{user=B}}}) ->
    lists:any(fun(U) -> case ?U:parse_conf_user(U) of false -> false; _ -> true end end, [A,B]).

%% ---------------------------------
-spec stop_wait_melody(State::#state_dlg{}) -> {ok, NewState::#state_dlg{}} | {error, Reason::term()}.
%% ---------------------------------
stop_wait_melody(#state_dlg{media=#media{ivr=undefined}}=State) -> {ok,State};
stop_wait_melody(#state_dlg{media=Media}=State) ->
    case ?MEDIA_B2B:ivr_detach(Media) of
        {error,_}=Err -> Err;
        {ok,Media1} ->
            {ok, State#state_dlg{media=Media1}}
    end.

%% ---------------------------------
%% Modify term's opts (does not concern SDPs, only opts.
%% Used for Events descriptor for termination
%% ---------------------------------
modify_term_events(SideLitera, {RequestId,Events}=Ev, #state_dlg{media=#media{}=Media}=State) when is_integer(RequestId), is_list(Events) ->
    case ?MEDIA_B2B:modify_term_events(Media, SideLitera, Ev) of
        {error, Reason} ->
            d(State, "MGC sessionchange error: ~120p", [Reason]),
            {error, ?InternalError("IVR. MGC error")};
        {ok,Media1} ->
            State1 = State#state_dlg{media=Media1},
            {ok, State1}
    end.

%% ---------------------------------
%% mg with several ifaces. found media ifaces RP-2078, RP-2093
%% ---------------------------------
-spec prepare_fork_ifaces(RuleMap::map(), BCallId::binary(), State::#state_forking{}) ->
    {ok, ForkAddress::inet:ip_address() | undefined, State::#state_forking{}} | {error, Reason::binary()}.
%% ---------------------------------
prepare_fork_ifaces(_,_,#state_forking{use_media=false}=State) -> {ok,undefined,State};
prepare_fork_ifaces(RuleMap,BCallId,State) ->
    #state_forking{media=#media{mgid=MgId,
                                start_opts=MediaStartOpts}=Media,
                   a=#side{host=AHost}}=State,
    % AHost is default (fall-back) host for featurecodes. Normally used address of service
    {AMgAlias,BMgAliasesMap} =
        case maps:get(aliases,MediaStartOpts,undefined) of
            {A,B} -> {A,B};
            undefined ->
                AMgAliasRaw = ?U:get_mg_alias_for_addr(MgId,AHost),
                case ?U:finalize_mg_alias(AMgAliasRaw) of
                    {error,ReasonA}=ErrA ->
                        ?LOGSIP("B2B fsm. Aliased MG. Finalize mg alias for A side (~120p) on mg (~120p) error:~120p",[AHost,MgId,ReasonA]),
                        {ErrA,#{}};
                    {ok,A} -> {A,#{}}
                end end,
    case {AMgAlias, ?U:get_callee_address(RuleMap,AHost)} of
        {{error,_}=Err ,_} -> Err; % if A's alias error
        {_, {error,_}=Err} -> Err; % if B's address error
        {_, {ok,BForkAddress}} ->
            BMgAliasRaw = ?U:get_mg_alias_for_addr(MgId,BForkAddress),
            case ?U:finalize_mg_alias(BMgAliasRaw) of
                {error,ReasonB}=ErrB ->
                    ?LOGSIP("B2B fsm. Aliased MG. Finalize mg alias for B side (~120p) on mg (~120p) error:~120p",[BForkAddress,MgId,ReasonB]),
                    ErrB;
                {ok,BMgAlias} ->
                    State1 = State#state_forking{media=Media#media{start_opts=MediaStartOpts#{aliases=>{AMgAlias,BMgAliasesMap#{BCallId => BMgAlias}}}}},
                    {ok,BForkAddress,State1}
            end
    end.

%% ---------------------------------
%% external terminations (monitor rtp, prompter rtp, record, stenographer, play)...
%% ---------------------------------
-spec external_term_attach(Opts :: map(), State::#state_dlg{}) ->
    {ok, State::#state_dlg{}} | {ok, Response::term(), State::#state_dlg{}} | {error, Reason::binary()}.
%% ---------------------------------
external_term_attach(_, #state_dlg{use_media=false}) -> {error,no_media};
external_term_attach(_, #state_dlg{media=undefined}) -> {error,invalid_state};
external_term_attach(_, #state_dlg{stopreason=SR}) when SR/=undefined -> {error,invalid_state};
external_term_attach(_, #state_forking{}) -> {error,invalid_state};
external_term_attach(#{}=Opts, #state_dlg{media=Media}=State) ->
    TId = ?EU:get_by_key(id, Opts),
    TType = ?EU:get_by_key(termtype, Opts),
    case ?MEDIA_B2B:external_term_attach(TId,TType,Opts,Media) of
        {error,_}=Err -> Err;
        {ok,Media1} ->
            {ok, State#state_dlg{media=Media1}};
        {ok,LSdp,Media1} ->
            {{ok,LSdp}, State#state_dlg{media=Media1}}
    end.

%% ---------------------------------
-spec external_term_detach(Opts :: map(), State::#state_dlg{}) ->
    {ok, State::#state_dlg{}} | {error, Reason::binary()}.
%% ---------------------------------
external_term_detach(_, #state_dlg{use_media=false}) -> {error,no_media};
external_term_detach(_, #state_dlg{media=undefined}) -> {error,invalid_state};
external_term_detach(_, #state_dlg{stopreason=SR}) when SR/=undefined -> {error,invalid_state};
external_term_detach(_, #state_forking{}) -> {error,invalid_state};
external_term_detach(#{}=Opts, #state_dlg{media=Media}=State) ->
    TId = ?EU:get_by_key(id, Opts),
    case ?MEDIA_B2B:external_term_detach(TId,Media) of
        {error,_}=Err -> Err;
        {ok,Media1} ->
            {ok, State#state_dlg{media=Media1}}
    end.

%% ---------------------------------
-spec external_term_setup_topology(Opts :: map(), State::#state_dlg{}) ->
    {ok, State::#state_dlg{}} | {error, Reason::binary()}.
%% ---------------------------------
external_term_setup_topology(_, #state_dlg{use_media=false}) -> {error,no_media};
external_term_setup_topology(_, #state_dlg{media=undefined}) -> {error,invalid_state};
external_term_setup_topology(_, #state_dlg{stopreason=SR}) when SR/=undefined -> {error,invalid_state};
external_term_setup_topology(_, #state_forking{}) -> {error,invalid_state};
external_term_setup_topology(#{}=Opts, #state_dlg{media=Media}=State) ->
    TId = ?EU:get_by_key(id, Opts),
    case ?EU:get_by_key(oneway_sides, Opts, undefined) of
        undefined ->
            case ?EU:get_by_key(topology, Opts, undefined) of
                undefined -> {error,<<"Invalid parameters. Expected oneway_sides or topology">>};
                Topology -> do_external_term_setup_topology({?MEDIA_B2B,external_term_setup_topology,[TId,Topology,Media]},State)
            end;
        OnewaySides -> do_external_term_setup_topology({?MEDIA_B2B,external_term_setup_topology_prompter,[TId,OnewaySides,Media]},State)
    end.

%% @private
do_external_term_setup_topology({M,F,A}, State) ->
    case erlang:apply(M,F,A) of
        {error,Reason}=Err ->
            d(State, "MGC error: ~120p", [Reason]),
            Err;
        {ok,Media1} ->
            d(State, " -> topology modified"),
            {ok, State#state_dlg{media=Media1}}
    end.

%%%% -----------------------------------
%%%% DEBUG DEVELOP
%%%% -----------------------------------
%%get_rec_file_info(#{}=Opts, #state_dlg{media=Media}=State) ->
%%    TId = ?EU:get_by_key(id, Opts),
%%    {TS,RecPath0} = ?U:get_media_context_rec_opts(),
%%    MGFile = filename:join([RecPath0, ?EU:str("ext_~s_~s.raw", [?EU:to_list(TId), ?EU:to_list(TS)])]),
%%    #media{mgid=MGID}=Media,
%%    case ?U:parse_mg_devicename(?U:get_mg_devicename(MGID)) of
%%        undefined -> false;
%%        {Addr,Postfix} ->
%%            RecPath = ?CFG:get_record_path(Addr,Postfix),
%%            SrcPath = filename:join([RecPath|lists:nthtail(1,filename:split(MGFile))]),
%%            case ?CFG:get_mg_node_by_addr_postfix(Addr,Postfix) of
%%                undefined -> false;
%%                {MGSite,MGNode} -> {ok,#{file => MGFile,
%%                                         site => MGSite,
%%                                         node => MGNode,
%%                                         recpath => RecPath,
%%                                         srcpath => SrcPath}}
%%            end end.
%%
%%%% @private
%%read_file_endpart(FileInfo,FromByte) -> read_file_endpart(FileInfo,FromByte,1).
%%read_file_endpart(FileInfo,FromByte,Attempts) ->
%%    [MGSite,MGNode,MGFilePath] = ?EU:maps_get([site,node,srcpath],FileInfo),
%%    case ?ENVCROSS:call_node({MGSite,MGNode}, {?EU, file_read_endpart, [MGFilePath,FromByte]}, false) of
%%        {ok,<<>>} when Attempts>0 -> read_file_endpart(FileInfo,FromByte,Attempts-1);
%%        T -> T
%%    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
%% returns mg opts for call to search mg (mgsfx, mgaddr)
get_mg_opts(_State) -> get_mg_opts().
get_mg_opts() ->
    [{mgsfx,?U:mg_postfix(<<"def">>)}, % suffix for internal calls (not sg/esg)
     get_ua_addrs_option() % local mg is less priority, used only if no other servers found.
    ].

%% @private
%% Not sufficient, because other sip servers used to reinvite on migration.
%% so it would be ok, if mg role is not configured anywhere if any sip role is.
get_ua_addrs_option() -> get_ua_addrs_option(temp).
get_ua_addrs_option(temp) ->
    Key = server_interfaces,
    FAddrs = fun() -> lists:usort(lists:filtermap(fun({_,_,_,_}=A) -> {true,?EU:to_binary(inet:ntoa(A))}; (_) -> false end, ?ENVCFG:get_current_server_addrs())) end,
    Addrs = ?ENVSTORE:lazy_t(Key, FAddrs, {60000,30000,2000}, {true,true}),
    {ua_addrs,Addrs}.

%% ---
get_dialogid(#state_forking{dialogid=DialogId}=_StateData) -> DialogId;
get_dialogid(#state_dlg{dialogid=DialogId}=_StateData) -> DialogId.

%% ---
get_media(#state_forking{media=Media}=_State) -> Media;
get_media(#state_dlg{media=Media}=_State) -> Media.


%% @private ----
d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    DlgId = get_dialogid(State),
    ?LOGSIP("B2B. fsm ~p media:" ++ Fmt, [DlgId] ++ Args).
