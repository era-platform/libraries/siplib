%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 10.02.2017
%%% @doc Some periodical operations

-module(r_sip_b2bua_refresher).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").

-define(RefreshMsgLic, {timer_refresh_lic}).
-define(RefreshTimeoutLic, 60000).

-define(RefreshMsgGates, {timer_refresh_gates}).
-define(RefreshTimeoutGates, 10000).

-define(RefreshTimeout, 10000).
-define(RefreshMsg, {timer_refresh}).

-record(gate, {site,node,srvidx,addr,ifaces,ports,online}).
-define(IfacesMFA, {r_sip_utils, get_host_interfaces_ipv4, []}).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    gen_server:start_link(?MODULE, Opts, []).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) ->
    State1 = #{opts => Opts,
               ref => make_ref(),
               %
               liclastupdate => 0,
               lictimerref => erlang:send_after(0, self(), ?RefreshMsgLic),
               licdata => undefined,
               %
               gates => [],
               gatestimerref => erlang:send_after(0, self(), ?RefreshMsgGates),
               %
               timerref => erlang:send_after(0, self(), ?RefreshMsg)},
    {ok, State1}.

%% ------------------------------
%% Call
%% ------------------------------

handle_call(_Request, _From, State) ->
    {reply, undefined, State}.

%% ------------------------------
%% Cast
%% ------------------------------

handle_cast(_Request, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

% timer msg lic, update values
handle_info(?RefreshMsgLic, State) ->
    State1 = refresh_lic(State),
    State2 = State1#{liclastupdate => ?EU:timestamp(),
                     lictimerref => erlang:send_after(?RefreshTimeoutLic + ?EU:random(10000), self(), ?RefreshMsgLic)},
    {noreply, State2};

% timer msg lic, update values
handle_info(?RefreshMsgGates, State) ->
    State1 = refresh_gates(State),
    State2 = State1#{gatestimerref => erlang:send_after(?RefreshTimeoutGates + ?EU:random(2000), self(), ?RefreshMsgGates)},
    {noreply, State2};

handle_info(?RefreshMsg, State) ->
    State1 = refresh_insiders(State),
    State2 = State1#{timerref => erlang:send_after(?RefreshTimeout, self(), ?RefreshMsg)},
    {noreply, State2};

% other
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ------------------------------------------
%% refresh of license parameters
%% ------------------------------------------

% @private @licdata
refresh_lic(State) ->
    LicData = ?ENVLIC:get_licdata(),
    refresh_lic_1(LicData),
    State.

% @private
refresh_lic_1(LicData) ->
    refresh('dialog_max_time_ms',LicData).

% @private #96
% setup dialog timeout. default is unlimited=7200, if no license - then 30 sec
refresh('dialog_max_time_ms',LicData) ->
    case ?ENVLIC:dlgtimesec(LicData) of
        undefined -> ?SIPSTORE:store_t('dialog_max_time_ms',30000,?RefreshTimeoutLic+20000);
        false -> ok;
        {ok,TSec} when is_integer(TSec), TSec > 1, TSec =< 7200 ->
            ?SIPSTORE:store_t('dialog_max_time_ms',TSec*1000,?RefreshTimeoutLic+20000);
        {ok,_} -> ok
    end.

%% ------------------------------------------
%% refresh of site gates addrs
%% ------------------------------------------

% refresh gate interfaces, b2bua routing
refresh_gates(State) ->
    F = fun() ->
            Site = ?CFG:get_current_site(),
            Gates = ?ENVCFG:get_nodes_containing_role_site('sg',Site,[idx]),
            Gates1 = lists:filter(fun({S,_,_,_}) -> S==Site end, Gates),
            Ifaces = ?ENVMULTICALL:call_direct(lists:map(fun({_,Node,_,_}) -> Node end, Gates1), ?IfacesMFA, 3000),
            Gates2 = lists:zip(Gates1,lists:map(fun({_,R}) -> R end, Ifaces)),
            Gates3 = lists:map(fun({{_Site,Node,_,Addr},GIfaces}) ->
                                        SrvIdx = ?ENVCFG:get_node_index(Node),
                                        #gate{site=Site,node=Node,srvidx=SrvIdx,addr=Addr,ifaces=GIfaces} end, Gates2),
            Gates4 = lists:map(fun(#gate{ifaces=undefined}=G) -> G#gate{online=false};
                                  (#gate{}=G) -> G#gate{online=true}
                               end, Gates3),
            Gates5 = lists:map(fun(#gate{srvidx=SrvIdx}=G) ->
                                       RoleOpts = ?CFG:get_server_role_opts_by_index(SrvIdx,sg),
                                       Ports = ?EU:extract_optional_props([<<"udp">>,<<"tcp">>,<<"tls">>], RoleOpts),
                                       G#gate{ports=lists:usort(lists:filter(fun(X) -> X/=undefined end, Ports))}
                               end, Gates4),
            Gates5
        end,
    GatesX = F(),
    % store for using by b2bua router
    F1 = fun(Iface) -> ?SIPSTORE:store_t(?GateIface(Iface), true, ?RefreshTimeoutGates*10) end,
    lists:foreach(fun(#gate{ifaces=undefined}) -> ok;
                     (#gate{ifaces=Ifaces}) -> lists:foreach(F1,Ifaces) end, GatesX),
    State#{gates => GatesX}.

% store inside server addr keys
refresh_insiders(State) -> State.
