%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc
%%% @todo

-module(r_sip_b2bua_stat).
-author('Peter Bukashin <tbotc@yandex.ru>').

%% -------------------------------------------------------------------
%%
%% -------------------------------------------------------------------

-export([query/2]).

%% ==========================================================================
%% Defines
%% ==========================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

% test of routing
query(<<"test_route">>, #{}=Map) ->
    ?B2BUA_ROUTER_TEST:apply(Map);

query(<<"test_representative">>, #{}=Map) ->
    ?REPRESENTATIVE:test(Map);

% regerror
query(<<"regerror">>, #{}=Map) ->
    regerror(Map);

% activecalls #RP-417
query(<<"activecalls">>, #{}=Map) ->
    activecalls(Map);

% activity of b2bua
query(_Mode, #{}=_Map) ->
    M = #{dlgcount => ?DLG_STORE:get_dialog_count(),
          callcount => ?DLG_STORE:get_callid_count()},
    {ok, M}.

%% ====================================================================
%% Internal functions
%% ====================================================================

regerror(Map) ->
    MaxSec = maps:get(maxsec,Map,600),
    MaxLines = maps:get(maxlines,Map,100),
    FilterDomain = maps:get(domain,Map,undefined),
    {ok,CWD} = ?Rfile:get_cwd(),
    Mask = filename:join([CWD, ?BLlog:get_basic_log_dir(), "sip", "rgf_" ++ ["*"]]),
    Files = lists:reverse(lists:sort(?Rfilelib:wildcard(Mask))),
    %
    Data = try lists:foldl(
                 fun(FPath,Acc) ->
                         case ?EU:to_binary(filename:basename(FPath)) of
                             <<"rgf_",YFx:32/bitstring,"-",MFx:16/bitstring,"-",DFx:16/bitstring,_/bitstring>> ->
                                 DF = {?EU:to_int(YFx,0),?EU:to_int(MFx,0),?EU:to_int(DFx,0)},
                                 DTLim = calendar:gregorian_seconds_to_datetime(
                                           calendar:datetime_to_gregorian_seconds(calendar:local_time()) - MaxSec),
                                 DataF = case ?Rfile:open(FPath, [raw, read, binary]) of
                                             {error,_} -> undefined;
                                             {ok,RF} ->
                                                 F = fun F(AccF) -> case ?Rfile:read_line(RF) of
                                                                        {ok,<<HFx:16/bitstring,":",MiFx:16/bitstring,":",SFx:16/bitstring,".",_/binary>>=Ln} ->
                                                                            case {DF,{?EU:to_int(HFx,0),?EU:to_int(MiFx,0),?EU:to_int(SFx,0)}}>DTLim of
                                                                                true ->
                                                                                    Ln1 = binary:replace(binary:replace(Ln,<<"\t">>,<<" ">>,[global]),<<"\n">>,<<"">>,[global]),
                                                                                    F(filter_regerror(FilterDomain,Ln1,AccF));
                                                                                false -> F(AccF)
                                                                            end;
                                                                        {ok,_} -> F(AccF);
                                                                        _ -> ?Rfile:close(RF), AccF
                                                                    end end,
                                                 F([])
                                         end,
                                 case {length(Acc),length(DataF)} of
                                     {X,Y} when X+Y < MaxLines -> Acc++DataF;
                                     {X,_} -> {Add,_} = lists:split(MaxLines-X,DataF), throw(Acc++Add)
                                 end;
                             _ -> Acc
                         end end, [], Files)
           catch
               throw:R -> R;
               _:_ -> []
           end,
    {ok,#{data => Data}}.

%% --------------------------
%% @private #RP-411
filter_regerror(undefined, Line, Acc) -> [Line|Acc];
filter_regerror(Domain, Line, Acc) when is_binary(Domain) ->
    Query = <<"To=<[^@]*@", Domain/binary, ">">>,
    case re:run(Line,Query) of
        nomatch -> Acc;
        _ -> [Line|Acc]
    end.

%% --------------------------------------------------------------------
activecalls(Map) ->
    DlgsLinks = ?DLG_STORE:get_all_dlgs(),
    Fun = fun({_,{_,MapLink,_,_,_}}, {Count,Acc}) ->
                  Link = maps:get(link, MapLink),
                  F = fun() -> ?B2BUA_DLG:get_current_info(Link) end,
                  {Count+1, lists:append(Acc,[{Count, F}])}
          end,
    {_,MulticalArgs} = lists:foldl(Fun, {1,[]}, DlgsLinks),
    ResultCall = ?ENVMULTICALL:multicall(MulticalArgs,5000),
    FilterDomain = maps:get(domain, Map, undefined),
    OutputFun = fun({_,{ok,InputMap}}, Acc) -> lists:append(Acc,[filter_activecalls(FilterDomain,InputMap)]);
                   (_, Acc) -> Acc 
                end,
    Result = lists:foldl(OutputFun, [], ResultCall),
    {ok, #{data => Result}}.

%% --------------------------------------------------------------------
%% @private #RP-417
filter_activecalls(undefined,#{state:='dialog'}=InputMap) ->
    [maps:with([state,invite_id,invite_ts,acallid,bcallid],InputMap)];
filter_activecalls(undefined,#{state:='forking',b:=BS}=InputMap) ->
    OutputMap = maps:with([state,invite_id,invite_ts,acallid],InputMap),
    UpdateBS = lists:map(fun(#{bcallid:=BCallId}) -> #{bcallid=>BCallId} end, BS),
    [maps:put(b,UpdateBS,OutputMap)];
filter_activecalls(Domain,#{state:='dialog',aluri:=#uri{domain=Domain}}=InputMap) ->
    [maps:with([state,invite_id,invite_ts,acallid,bcallid],InputMap)];
filter_activecalls(Domain,#{state:='dialog',bruri:=#uri{domain=Domain}}=InputMap) ->
    [maps:with([state,invite_id,invite_ts,acallid,bcallid],InputMap)];
filter_activecalls(Domain,#{state:='forking',b:=BS,aluri:=#uri{domain=Domain}}=InputMap) ->
    OutputMap = maps:with([state,invite_id,invite_ts,acallid],InputMap),
    UpdateBS = lists:map(fun(#{bcallid:=BCallId}) -> #{bcallid=>BCallId} end, BS),
    [maps:put(b,UpdateBS,OutputMap)];
filter_activecalls(Domain,#{state:='forking',b:=BS}=InputMap) ->
    case lists:foldl(fun(#{bcallid:=BCallId,bruri:=#uri{domain=Domain1}},{false,Acc}) when Domain==Domain1 -> {true,lists:append(Acc,[#{bcallid=>BCallId}])};
                        (#{bcallid:=BCallId},{R,Acc}) -> {R,lists:append(Acc,[#{bcallid=>BCallId}])}
                     end, {false,[]}, BS) of
        {false,_} -> [];
        {true,UpdateBS} ->
            OutputMap = maps:with([state,invite_id,invite_ts,acallid],InputMap),
            [maps:put(b,UpdateBS,OutputMap)]
    end;
filter_activecalls(_Domain,_) -> [].

%% ====================================================================
%% Tests
%% ====================================================================

-ifdef(TEST).

-include_lib("eunit/include/eunit.hrl").

filter_regerror_test_() ->
    Line1 = <<"17:28:47.180    <0.2117.0>    Rcv=udp:192.168.0.73:45604; To=<sip:sip2@x.rootdomain.ru>; Contact=<sip:sip2@192.168.0.73:45604>; UA=''; Reason='B2B. Auth invalid prerequisites (2)'; Call-Id='YTczOGM2YjQ4YzZiMTI4MDExODM4YzExMjBjMTI4NWE.'">>,
    Line2 = <<"17:28:47.180    <0.2117.0>    Rcv=udp:192.168.0.73:45604; To=<sip:sip2@192.168.0.73>; Contact=<sip:sip2@x.rootdomain.ru:45604>; UA=''; Reason='B2B. Auth invalid prerequisites (2)'; Call-Id='YTczOGM2YjQ4YzZiMTI4MDExODM4YzExMjBjMTI4NWE.'">>,
    {"filter worker domain regerror test",
     [?_assertEqual([Line1], filter_regerror(undefined, Line1, [])),
      ?_assertEqual([Line1], filter_regerror(<<"x.rootdomain.ru">>, Line1, [])),
      ?_assertEqual([], filter_regerror(<<"test.rootdomain.ru">>, Line1, [])),
      ?_assertEqual([], filter_regerror(<<"x.rootdomain.ru">>, Line2, [])),
      ?_assertError(function_clause, filter_regerror("x.rootdomain.ru", Line2, []))
      ]}.

filter_activecalls_test_() ->
    U = #uri{},
    InviteId = <<"InviteId">>, InviteTs = <<"InviteTs">>,
    ACallId = <<"ACallId">>, BCallId = <<"BCallId">>,
    ARTag = <<"1">>, ALTag = <<"2">>, BRTag = <<"3">>, BLTag = <<"4">>,
    ReturnDialog = #{state=>dialog,invite_id=>InviteId,invite_ts=>InviteTs,acallid=>ACallId,bcallid=>BCallId},
    ReturnForking = #{state=>forking,invite_id=>InviteId,invite_ts=>InviteTs,acallid=>ACallId,b=>[#{bcallid=>BCallId}]},
    ReturnMultiForking = #{state=>forking,invite_id=>InviteId,invite_ts=>InviteTs,acallid=>ACallId,b=>[#{bcallid=>BCallId},#{bcallid=>BCallId},#{bcallid=>BCallId}]},
    {"filter worker domain activecalls test",
     [?_assertEqual([ReturnDialog], filter_activecalls(undefined, #{state=>dialog,invite_id=>InviteId,invite_ts=>InviteTs,acallid=>ACallId,artag=>ARTag,altag=>ALTag,aruri=>U,aluri=>U,bcallid=>BCallId,brtag=>BRTag,bltag=>BLTag,bruri=>U,bluri=>U})),
      ?_assertEqual([ReturnForking], filter_activecalls(undefined, #{state=>forking,invite_id=>InviteId,invite_ts=>InviteTs,acallid=>ACallId,artag=>ARTag,altag=>ALTag,aruri=>U,aluri=>U,bcallid=>BCallId,b=>[#{bcallid=>BCallId,brtag=>BRTag,bltag=>BLTag,bruri=>U,bluri=>U}]})),
      ?_assertEqual([ReturnDialog], filter_activecalls(<<"x.rootdomain.ru">>, #{state=>dialog,aluri=>U#uri{domain = <<"x.rootdomain.ru">>},bruri=>U#uri{domain = <<"test.rootdomain.ru">>},invite_id=>InviteId,invite_ts=>InviteTs,acallid=>ACallId,artag=>ARTag,altag=>ALTag,aruri=>U,bcallid=>BCallId,brtag=>BRTag,bltag=>BLTag,bluri=>U})),
      ?_assertEqual([ReturnDialog], filter_activecalls(<<"x.rootdomain.ru">>, #{state=>dialog,aluri=>U#uri{domain = <<"test.rootdomain.ru">>},bruri=>U#uri{domain = <<"x.rootdomain.ru">>},invite_id=>InviteId,invite_ts=>InviteTs,acallid=>ACallId,artag=>ARTag,altag=>ALTag,aruri=>U,bcallid=>BCallId,brtag=>BRTag,bltag=>BLTag,bluri=>U})),
      ?_assertEqual([], filter_activecalls(<<"x.rootdomain.ru">>, #{state=>dialog,aluri=>U,bruri=>U#uri{domain = <<"test.rootdomain.ru">>},aruri=>U#uri{domain = <<"x.rootdomain.ru">>},invite_id=>InviteId,invite_ts=>InviteTs,acallid=>ACallId,artag=>ARTag,altag=>ALTag,bcallid=>BCallId,brtag=>BRTag,bltag=>BLTag,bluri=>U})),
      ?_assertEqual([], filter_activecalls(<<"x.rootdomain.ru">>, #{state=>dialog,aluri=>U#uri{domain = <<"test.rootdomain.ru">>},bruri=>U,bluri=>U#uri{domain = <<"x.rootdomain.ru">>},invite_id=>InviteId,invite_ts=>InviteTs,acallid=>ACallId,artag=>ARTag,altag=>ALTag,aruri=>U,bcallid=>BCallId,brtag=>BRTag,bltag=>BLTag})),
      ?_assertEqual([], filter_activecalls(<<"x.rootdomain.ru">>, #{state=>dialog,aluri=>U#uri{domain = <<"test.rootdomain.ru">>},bruri=>U#uri{domain = <<"test.rootdomain.ru">>},invite_id=>InviteId,invite_ts=>InviteTs,acallid=>ACallId,artag=>ARTag,altag=>ALTag,aruri=>U,bcallid=>BCallId,brtag=>BRTag,bltag=>BLTag,bluri=>U})),
      ?_assertEqual([ReturnForking], filter_activecalls(<<"x.rootdomain.ru">>, #{state=>forking,aluri=>U#uri{domain = <<"x.rootdomain.ru">>},invite_id=>InviteId,invite_ts=>InviteTs,acallid=>ACallId,artag=>ARTag,altag=>ALTag,aruri=>U,bcallid=>BCallId,b=>[#{bcallid=>BCallId,brtag=>BRTag,bltag=>BLTag,bruri=>U,bluri=>U}]})),
      ?_assertEqual([ReturnForking], filter_activecalls(<<"x.rootdomain.ru">>, #{state=>forking,aluri=>U#uri{domain = <<"test.rootdomain.ru">>},b=>[#{bcallid=>BCallId,brtag=>BRTag,bltag=>BLTag,bruri=>#uri{domain = <<"x.rootdomain.ru">>},bluri=>U}],invite_id=>InviteId,invite_ts=>InviteTs,acallid=>ACallId,artag=>ARTag,altag=>ALTag,aruri=>U,bcallid=>BCallId})),
      ?_assertEqual([ReturnMultiForking], filter_activecalls(<<"x.rootdomain.ru">>, #{state=>forking,aluri=>U#uri{domain = <<"test.rootdomain.ru">>},b=>[#{bcallid=>BCallId,brtag=>BRTag,bltag=>BLTag,bruri=>#uri{domain = <<"y.rootdomain.ru">>},bluri=>U},#{bcallid=>BCallId,brtag=>BRTag,bltag=>BLTag,bruri=>#uri{domain = <<"x.rootdomain.ru">>},bluri=>U},#{bcallid=>BCallId,brtag=>BRTag,bltag=>BLTag,bruri=>#uri{domain = <<"z.rootdomain.ru">>},bluri=>U}],invite_id=>InviteId,invite_ts=>InviteTs,acallid=>ACallId,artag=>ARTag,altag=>ALTag,aruri=>U,bcallid=>BCallId})),
      ?_assertEqual([], filter_activecalls(<<"x.rootdomain.ru">>, #{state=>forking,aluri=>U#uri{domain = <<"test.rootdomain.ru">>},b=>[#{bcallid=>BCallId,brtag=>BRTag,bltag=>BLTag,bruri=>#uri{domain = <<"y.rootdomain.ru">>},bluri=>U},#{bcallid=>BCallId,brtag=>BRTag,bltag=>BLTag,bruri=>U,bluri=>U#uri{domain = <<"x.rootdomain.ru">>}},#{bcallid=>BCallId,brtag=>BRTag,bltag=>BLTag,bruri=>#uri{domain = <<"z.rootdomain.ru">>},bluri=>U}],invite_id=>InviteId,invite_ts=>InviteTs,acallid=>ACallId,artag=>ARTag,altag=>ALTag,aruri=>U,bcallid=>BCallId})),
      ?_assertEqual([], filter_activecalls(<<"x.rootdomain.ru">>, #{state=>forking,aluri=>U#uri{domain = <<"test.rootdomain.ru">>},b=>[#{bcallid=>BCallId,brtag=>BRTag,bltag=>BLTag,bruri=>#uri{domain = <<"y.rootdomain.ru">>},bluri=>U},#{bcallid=>BCallId,brtag=>BRTag,bltag=>BLTag,bruri=>#uri{domain = <<"z.rootdomain.ru">>},bluri=>U}],invite_id=>InviteId,invite_ts=>InviteTs,acallid=>ACallId,artag=>ARTag,altag=>ALTag,aruri=>U,bcallid=>BCallId})),
      ?_assertEqual([], filter_activecalls(<<"x.rootdomain.ru">>, #{state=>forking,aluri=>U#uri{domain = <<"test.rootdomain.ru">>},aruri=>U#uri{domain = <<"x.rootdomain.ru">>},b=>[#{bcallid=>BCallId,brtag=>BRTag,bltag=>BLTag,bruri=>#uri{domain = <<"test.rootdomain.ru">>},bluri=>U}],invite_id=>InviteId,invite_ts=>InviteTs,acallid=>ACallId,artag=>ARTag,altag=>ALTag,bcallid=>BCallId}))
      ]}.

-endif.