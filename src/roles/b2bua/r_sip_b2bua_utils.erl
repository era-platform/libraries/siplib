%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc
%%% @todo

-module(r_sip_b2bua_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([]).

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_headers.hrl").
-include("../include/r_sip_b2b.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

% =============================================
% Outgoing CallId
% =============================================
handle_uas_dialog_response_2xx([#sipmsg{cseq={_,'INVITE'}}=Resp], State, D) ->
    #sipmsg{to={_,ToTag}}=Resp,
    {ok,XCallId} = nksip_response:call_id(Resp),
    {ok,XDlgHandle} = nksip_dialog:get_handle(Resp),
    State1 = case State of
                 #state_dlg{a=#side{callid=XCallId}=ASide} -> State#state_dlg{a=ASide#side{dhandle=XDlgHandle}};
                 #state_dlg{b=#side{callid=XCallId, remotetag=ToTag}=BSide} -> State#state_dlg{b=BSide#side{dhandle=XDlgHandle}};
                 #state_dlg{b=#side{callid=XCallId}} -> D(State, "found double 2xx response on INVITE (tag=~p)", [ToTag]), State;
                 _T ->
                    #state_dlg{a=#side{callid=ACallId},b=#side{callid=BCallId}}=State,
                    ?OUT("uas response error: ~p, ~p, ~p", [XCallId, ACallId, BCallId]),
                    State
             end,
    State1;
handle_uas_dialog_response_2xx(_P, State, _D) ->
    State.

% =============================================
% Outgoing CallId
% =============================================

build_out_callid(DlgNum, CallIdx, ACallId) ->
    CurSrvCode = ?U:get_current_srv_textcode(),
    CallCode = ?U:build_textcode_by_index(CallIdx, 2),
    <<"rB2-", CurSrvCode/bitstring, "-", DlgNum/bitstring, "-", CallCode/bitstring, "-", ACallId/bitstring>>.

% ----
extract_callid(<<"rB2-", _:24/bitstring, "-", _:48/bitstring, "-", _:16/bitstring, "-", CallId/bitstring>>) -> extract_callid(CallId);
extract_callid(CallId) -> CallId.

% ----
-spec check_callid(CallOrDlgId :: binary()) -> {atom(), binary()}.
% ----
% checking CallOrDialogId type
check_callid(<<"rDlg-", _:24/bitstring, "-", _/bitstring>>=DlgId) -> {dialogid, DlgId};
check_callid(<<"rB2-", _:24/bitstring, "-", _:48/bitstring, "-", _:16/bitstring, "-", CallId/bitstring>>) -> {b2b_callid, extract_callid(CallId)};
check_callid(CallId) -> {callid, CallId}.

% =============================================
% Local Tags
% =============================================

build_tag() ->
    CurSrvCode = ?U:get_current_srv_textcode(),
    <<Tag0:32/bitstring, _/bitstring>> = ?U:luid(),
    <<"rB2-", CurSrvCode/bitstring, "-", Tag0/bitstring>>.

%% ----------------------------------------------------------
%% Extracts integer server index by {callid, fromtag, totag} - first occurance of "rB2-" prefix
%%
extract_b2bua_server_index(<<"rB2-", SrvCode:24/bitstring, "-", _/bitstring>>=_CallId,_FTag,_TTag) ->
    {true, ?EU:parse_textcode_to_index(SrvCode)};
extract_b2bua_server_index(_CallId,<<"rB2-", SrvCode:24/bitstring, "-", _/bitstring>>=_FTag,_TTag) ->
    {true, ?EU:parse_textcode_to_index(SrvCode)};
extract_b2bua_server_index(_CallId,_FTag,<<"rB2-", SrvCode:24/bitstring, "-", _/bitstring>>=_TTag) ->
    {true, ?EU:parse_textcode_to_index(SrvCode)};
extract_b2bua_server_index(_,_,_) -> false.


extract_b2bua_server_index(<<"rDlg-", SrvCode:24/bitstring, "-", _/bitstring>>=_DlgId) ->
    {true, ?EU:parse_textcode_to_index(SrvCode)};
extract_b2bua_server_index(<<"rB2-", SrvCode:24/bitstring, "-", _/bitstring>>=_CallId) ->
    {true, ?EU:parse_textcode_to_index(SrvCode)};
extract_b2bua_server_index(_) -> false.

% =============================================
% Log to callid-link log
% =============================================

%% #307, #354, RP-415
log_callid(DlgNum,ACallId,OutCallIdx,BCallId,Map,[_ADomain,_BDomain]=Domains) ->
    DStr = domains_str(Domains),
    case {maps:get('link_referred',Map,undefined), maps:get('link_replaces',Map,undefined)} of
        {undefined,undefined} ->
            ?BLlog:write({sip,callid}, {"dlg='~s'; acallid='~s'; idx=~p; bcallid='~s'; domains='~s'",[DlgNum,ACallId,OutCallIdx,BCallId,DStr]});
        {RefCallId,undefined} ->
            ?BLlog:write({sip,callid}, {"dlg='~s'; acallid='~s'; idx=~p; bcallid='~s'; rfcallid='~s'; domains='~s'",[DlgNum,ACallId,OutCallIdx,BCallId,RefCallId,DStr]});
        {RefCallId,RepCallId} ->
            ?BLlog:write({sip,callid}, {"dlg='~s'; acallid='~s'; idx=~p; bcallid='~s'; rfcallid='~s'; rpcallid='~s'; domains='~s'",[DlgNum,ACallId,OutCallIdx,BCallId,RefCallId,RepCallId,DStr]})
    end.

%% @private
domains_str([A,A]) -> ?EU:str("~s",[A]);
domains_str([A,B]) -> ?EU:str("~s,~s",[A,B]).

% =============================================
% Extracts prop from state
% =============================================

extract_property(Key, Default, #state_forking{map=Map}=_State) ->
    [Value] = ?EU:extract_optional_default([{Key,Default}], maps:get(opts,Map)),
    Value;
extract_property(Key, Default, #state_dlg{map=Map}=_State) ->
    [Value] = ?EU:extract_optional_default([{Key,Default}], maps:get(opts,Map)),
    Value.

% =============================================
% Builds remote-party-id URI by response
% =============================================

%% Ответ пришел, нужно пробросить.
%%
%%     Звонили не на внутреннего пользователя
%%         RemotePartyId есть - насквозь.
%%         RemotePartyId нет - быть не может - мы звоним на свои сервисы (conf,ivr,esg)
%%     Звонили на учетку (sipuser=A,extension=B)
%%         RemotePartyId нет
%%             x1. Если extension пуст
%%                 Формируется RemotePartyId, где
%%                     UserName = To:username(он же A.Login),
%%                     DisplayName - DC sipuser name modificator over ""
%%             x2. Если extension не пуст
%%                 Формируется RemotePartyId, где
%%                     UserName = To:username(потому что пофиг) (!) | A.Number + B (для полноты/чистоты) (X),
%%                     DisplayName - DC sipuser name modificator over ""
%%         RemotePartyId есть
%%             x3. Если extension пуст
%%                 Модифицируется RemotePartyId, где
%%                     UserName = To:username (!) | RemotePartyOriginal.User,
%%                     DisplayName - DC sipuser name modificator over RemotePartyOriginal.Display
%%             x4. Если extension не пуст
%%                 Модифицируется RemotePartyId, где
%%                     UserName = To:username (!) | RemotePartyOriginal.User (X) | A.Number + B (для полноты/чистоты) (X),
%%                     DisplayName - DC sipuser name modificator over RemotePartyOriginal.Display
%%
build_remote_party_id(#sipmsg{class={resp,_,_}}=Resp, #side_fork{sipuser=undefined}=_Fork, RUri) ->
    ?U:build_remote_party_id(Resp, RUri);
build_remote_party_id(#sipmsg{class={resp,_,_}}=Resp, #side_fork{sipuser=SipUser}=_Fork, RUri) ->
    rp(?U:extract_remoteparty_uri(Resp), SipUser, RUri).

% @private
rp(SipMsgRemoteParty, SipUser, RUri) ->
    [Login,Domain,Phone,Name,FNumExt] = ?EU:extract_required_props([login,domain,phone,name,extension],SipUser),
    Uri = #uri{scheme=sip, disp= <<>>, user=Login, domain=Domain},
    case {SipMsgRemoteParty, FNumExt} of
        {undefined,undefined} -> % x1
            Fextdisp = fun() -> ?FROM:build_ext_disp(Phone,Domain) end, % #366
            Disp = ?FROM:apply_modifier_disp(Name,<<>>,[<<>>,RUri#uri.user,RUri#uri.user,Fextdisp]),
            Uri#uri{disp=?U:quote_display(Disp)};
        {undefined,_} -> % x2
            UN = Login,
            %UN = <<Phone/binary,FNumExt/binary>>, % #299
            %UN = FNumExt, % russianpost.ru 26.06.2017
            Fextdisp = fun() -> ?FROM:build_ext_disp(<<Phone/binary,FNumExt/binary>>,Domain) end, % #366
            Disp = ?FROM:apply_modifier_disp(Name,<<>>,[<<>>,RUri#uri.user,RUri#uri.user,Fextdisp]),
            Uri#uri{disp=?U:quote_display(Disp),user=UN};
        {RPID,undefined} -> % x3
            RDisp = ?U:unquote_display(RPID#uri.disp),
            RUser = RPID#uri.user,
            Fextdisp = fun() -> ?FROM:build_ext_disp(Phone,Domain) end, % #366
            Disp = ?FROM:apply_modifier_disp(Name,RDisp,[RDisp,RUser,Phone,Fextdisp]),
            Uri#uri{disp=?U:quote_display(Disp)};
        {RPID,_} -> % x4
            UN = Login,
            %UN = <<Phone/binary,FNumExt/binary>>, #299
            %UN = FNumExt, % russianpost.ru 26.06.2017
            RDisp = ?U:unquote_display(RPID#uri.disp),
            RUser = RPID#uri.user,
            CNum = <<Phone/bitstring,RUser/bitstring>>,
            Fextdisp = fun() -> ?FROM:build_ext_disp(<<Phone/binary,FNumExt/binary>>,Domain) end, % #366
            Disp = ?FROM:apply_modifier_disp(Name,RDisp,[RDisp,RUser,CNum,Fextdisp]),
            Uri#uri{disp=?U:quote_display(Disp),user=UN}
    end.

% =============================================
% Make esg route uri by srvidx
% =============================================

get_esg_route_uri(SrvIdx) ->
    [Addr|_] = ?CFG:get_sipserver_addrs_by_index(SrvIdx),
    Opts = get_esg_opts(SrvIdx),
    [Udp,Tcp] = ?EU:extract_optional_props([<<"udp">>,<<"tcp">>], Opts),
    Port = case Tcp of
               undefined ->
                   case Udp of
                       undefined -> 5060;
                       PUdp -> ?EU:to_int(PUdp)
                   end;
               PTcp -> ?EU:to_int(PTcp)
           end,
    %% == George == scheme:sip
    #uri{scheme=sip,
         domain=Addr,
         port=Port,
         opts=?CFG:get_transport_opts()++[<<"lr">>]}.

%% @private
%% returns esg role opts
get_esg_opts(SrvIdx) ->
    Roles = [esg],
    F = fun(Role,[]) ->
                case ?CFG:get_sipserver_opts_by_index(SrvIdx, Role) of
                    undefined -> [];
                    T -> T
                end;
           (_,Acc) -> Acc
        end,
    lists:foldl(F, [], Roles).


% =============================================
% Build/update fork opts (link to ext account and sipuser)
% =============================================

% build opts to fork (by incoming request)
build_caller_fork_opts(#sipmsg{}=Req,SipUser) ->
    ForkOpts = build_forkopts_ext(Req),
    update_forkopts_sipuser(SipUser,ForkOpts).

%
build_forkopts_ext(Req) ->
    case nksip_request:header(?OwnerHeaderLow, Req) of
        {_,[<<"rEG-", SrvTextCode:24/bitstring>>|_]} ->
            SrvIdx = ?EU:parse_textcode_to_index(SrvTextCode),
            {_,ExtOpts} = ?U:parse_ext_account_header(Req),
            {_,ExtId} = lists:keyfind(<<"id">>,1,ExtOpts),
            {_,ClusterDomain} = lists:keyfind(<<"cd">>,1,ExtOpts),
            case ?DC:find_provider_account_by_id(ClusterDomain,ExtId) of
                #{}=ExtAccount -> update_forkopts_ext(ExtAccount#{serveridx=>SrvIdx},[]);
                _ -> []
            end;
        _ -> []
    end.

% update fork opts by ExtAccount
update_forkopts_ext(undefined,ForkOpts) -> ForkOpts;
update_forkopts_ext(ExtAccount,ForkOpts) when is_map(ExtAccount) ->
    ExtSrvIdx = maps:get(serveridx,ExtAccount),
    [{extaccount,ExtAccount},{extsrvidx,ExtSrvIdx} | ForkOpts].

% update fork opts by SipUser
update_forkopts_sipuser(undefined,ForkOpts) -> ForkOpts;
update_forkopts_sipuser(SipUser,ForkOpts) when is_list(SipUser) ->
    [U,D] = ?EU:lists_get([login,domain],SipUser),
    [{sipuser,{sip,U,D}} | ForkOpts].


% =============================================
% User state to state store
% =============================================

usr_state(UState,DialogId,#side{remoteuri=RUri,localtag=LTag,opts=Opts}) -> usr_state_1(UState,DialogId,{RUri,LTag,Opts});
usr_state(UState,DialogId,#side_fork{remoteuri=RUri,localtag=LTag,opts=Opts}) -> usr_state_1(UState,DialogId,{RUri,LTag,Opts}).

% @private
usr_state_1(UState,DialogId,{RUri,LTag,Opts}) ->
    Uri = case ?EU:extract_optional_props([sipuser], Opts) of
              [undefined] -> RUri;
              [{sip,_,_}=RealAOR] -> ?U:make_uri(RealAOR)
          end,
    usr_state_2(UState,DialogId,Uri,LTag).
%
usr_state_2(UState,DialogId,Uri,LTag) ->
    case UState of
        early -> ?USRSTATE:usr_early(Uri,{DialogId,self(),LTag});
        busy -> ?USRSTATE:usr_busy(Uri,{DialogId,self(),LTag});
        free -> ?USRSTATE:usr_free(Uri,{DialogId,self(),LTag})
    end.

% =============================================
% Dialog max time
% =============================================

dialog_max_time(State) ->
    % #96 Set dialog max duration by license
    Timeout0 = case ?SIPSTORE:find_t('dialog_max_time_ms') of {_,N} when is_integer(N) -> N; _ -> ?DIALOG_TIMEOUT end,
    % #280 Update dialog max duration by users
    Fdlgt = fun(SipUser) when is_list(SipUser) ->
                    {opts,Opts} = lists:keyfind(opts,1,SipUser),
                    case maps:get(<<"dlgtimesec">>,Opts,Timeout0) of
                        0 -> Timeout0;
                        T -> T * 1000
                    end end,
    Timeout1 = case State of
                   #state_dlg{a=#side{sipuser=undefined},b=#side{sipuser=undefined}} -> Timeout0;
                   #state_dlg{a=#side{sipuser=undefined},b=#side{sipuser=B}} -> erlang:min(Timeout0,Fdlgt(B));
                   #state_dlg{a=#side{sipuser=A},b=#side{sipuser=undefined}} -> erlang:min(Timeout0,Fdlgt(A));
                   #state_dlg{a=#side{sipuser=A},b=#side{sipuser=B}} -> erlang:min(Timeout0,erlang:min(Fdlgt(A),Fdlgt(B)))
               end,
    % result
    Timeout1.

% =============================================
% Make stop reason
% =============================================

%
reason_error(_, _, #{}=Reason) -> Reason;
reason_error(Operations, <<>>, Reason) ->
    #{type => error,
      operations => Operations,
      reason => Reason};
reason_error(Operations, CallId, Reason) ->
    #{type => error,
      operations => Operations,
      callid => CallId,
      reason => Reason}.

%
reason_timeout(Operations, <<>>, Reason) ->
    #{type => timeout,
      operations => Operations,
      reason => Reason};
reason_timeout(Operations, CallId, Reason) ->
    #{type => timeout,
      operations => Operations,
      callid => CallId,
      reason => Reason}.

%
reason_callterm(CallId) ->
    #{type => call_terminate,
      callid => CallId,
      reason => <<"Call leg terminated">>}.

%
reason_cancel(CallId) ->
    #{type => cancel,
      side => a,
      callid => CallId,
      reason => <<"Call cancelled">>}.

%
reason_callfinal(BestSipCode) ->
    #{type => call_final,
      side => b,
      sipcode => BestSipCode,
      reason => <<"Call not answered">>}.

%
reason_bye(CallId,Side) ->
    BinSide = ?EU:to_binary(Side),
    #{type => bye,
      side => Side,
      callid => CallId,
      reason => <<"Bye from side ", BinSide/bitstring>>}.

%
reason_external(#{}=Reason) -> Reason;
reason_external(Reason) when is_binary(Reason) ->
    #{type => external,
      side => ext,
      reason => Reason}.

%%------------------------------------------------------------
erlang_ts_concat({_,_,_}=Ts) -> ?EU:to_list(?EU:timestamp(Ts));
erlang_ts_concat(Ts) when is_list(Ts) -> Ts;
erlang_ts_concat(Ts) when is_integer(Ts) -> ?EU:to_list(Ts).

%%------------------------------------------------------------
rec_links_handler([],Res) -> Res;
rec_links_handler([RecLink|Tail],Res) ->
    {{_,{_,MgcName},{_,MgName}},Context,Ts,PrepPathToRec} = RecLink,
    PathToRec = filename:join(tl(filename:split(PrepPathToRec))),
    Res1 = Res ++
               ?EU:to_list(MgcName)++","++
               ?EU:to_list(MgName)++","++
               ?EU:to_list(Context)++","++
               ?EU:to_list(Ts)++","++
               ?EU:to_list(PathToRec)++";",
    rec_links_handler(Tail,Res1).
%% ====================================================================
%% Internal functions
%% ====================================================================
