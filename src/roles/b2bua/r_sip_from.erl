%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 02.02.2017
%%% @doc From uri routines for b2bua usage
%%%        Remote could be phone or pbx
%%%        Remote pbx number could be in from, in contact
%%%        Remote pbx displayname could be in from, in contact
%%%

-module(r_sip_from).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([build_from_abc/1,
         build_ext_disp/2,
         apply_modifier_disp/3,
         apply_modifier_ext/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").

-include("../include/r_sip_nk.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------------------------
%% Fields:
%%     From.user, Auth.user, Contact.user
%%     From.display, Contact.display
%%     DC.user phone, DC.user display(name)
%% ------------------------------------------
%% A. Forwarded from username is used for redial.
%%    Made over representative.
%%    Initial is A, then representative when called is defined
%% ------------------------------------------
%% B. Basic from for routing
%%    Used to search routes
%%    Marked as B
%% ------------------------------------------
%% C. Forwarded From display is used for display.
%%     Made automatically.
%%    Could be forwarded without change, be constant name of sipuser or modified by sipuser's modifier.
%%    Modifier could have {F}, constants.
%%     Marked as C, then sipuser's name and modifier over C, if sipuser found.
%% ------------------------------------------
%% D. Domain of caller
%% ------------------------------------------
%% Not Sipuser (from internal servers, not from gate)
%%     conf, ivr, ext - leave from as is
%%     X0. From.user/=Auth.user (Auth.user not found)                         => A=From.user, B=From.user, C=From.dn
%% SipUser (from gate, authenticate)
%%     X1. From.user==Auth.user (From.user found), Contact.user==undefined    => A=SipUser.phone, B=SipUser.phone, C=SipUser.name(From.dn),
%%     X2. From.user==Auth.user (From.user found), Contact.user==""           => A=SipUser.phone, B=SipUser.phone, C=SipUser.name(From.dn),
%%     X3. From.user==Auth.user (From.user found), Contact.user==From.user    => A=SipUser.phone, B=SipUser.phone, C=SipUser.name(From.dn),
%%     X4. From.user==Auth.user (From.user found), Contact.user/=Auth.user    => A=SipUser.phone+Contact.user, B=SipUser.phone+Contact.user, C=SipUser.name(From.dn)
%%     X5. From.user/=Auth.user (Auth.user found)                             => A=SipUser.phone+From.user, B=SipUser.phone+From.user, C=SipUser.name(From.dn)
%%     X6. From.user/=Auth.user, auth.user not found    unbelievable          => A=SipUser.phone+From.user, B=SipUser.phone+From.user, C=From.dn
%% -------------------------------------------
%% Now it is User-Agent insensitive.

%% define real sipuser account, remote number, and builds routed and forwarded username and displayname
%% returns {A,B,C,SipUserInfo} :: {forwarded initial username, routing initial number, forwarded displayname, undefined | [{Key,Value}] (login,name,phone,domain) }
%% -------------------------------------------
-spec build_from_abc(Req::#sipmsg{}) -> {A::binary(),B::binary(),C::binary(),SipUserInfo::list(),Abon::{sipuser|other,Username::binary()}}.
%% -------------------------------------------
build_from_abc(#sipmsg{}=Req) ->
    build_from_abc_1(Req).

%% First check what account is calling (reg/noreg, from/auth, ...)
build_from_abc_1(Req) ->
    #sipmsg{from={#uri{user=FU,domain=FD}=FromUri,_},
            contacts=[ContactUri|_]}=Req,
    % extract rcvfrom
    RcvFrom = case ?AUTH:extract_gate_rcvfrom(Req) of
                  {_,_,_}=GateRcvFrom -> GateRcvFrom;
                  undefined -> ?AUTH:extract_rcvfrom(Req)
              end,
    % find noreg account (auth has filtered some requests)
    case ?NOREG:find(RcvFrom) of
        {ok,[{U,D}]} ->
            FromUri1 = FromUri#uri{domain=D},
            AccUri = FromUri#uri{user=U,domain=D},
            build_from_abc_2(FromUri1,AccUri,ContactUri,Req);
        {ok,[_|_]=G} ->
            {U,D} = lists:keyfind(FD,2,G),
            FromUri1 = FromUri#uri{domain=D},
            AccUri = FromUri#uri{user=U,domain=D},
            build_from_abc_2(FromUri1,AccUri,ContactUri,Req);
        false ->
            %%%%%----------------------
            % request 'from' account (by from&auth)
            AccUri = case lists:keyfind(<<"username">>,1,?U:parse_auth(Req)) of
                         false -> FromUri; % x0 and x1,x2,x3,x4
                           {_,FU} -> FromUri; % x1,x2,x3,x4
                           {_,FUA} -> FromUri#uri{user=?U:unquote_display(FUA)} % x5,x6
                     end,
            build_from_abc_2(FromUri,AccUri,ContactUri,Req)
    end.

%% Then update by by internal_pbx accounts
build_from_abc_2(FromUri, AccUri, ContactUri, _Req) ->
    #uri{disp=FDN,user=FU,domain=FD}=FromUri,
    #uri{user=AccUser}=AccUri,
    #uri{user=CU}=ContactUri,
    %%%%%----------------------
    % request sipuser from DC
    SipUser = case ?ACCOUNTS:get_sipuser(AccUser,FD) of
                  {ok,SU} -> SU;
                  _ -> undefined
              end,
    %%%%%----------------------
    % request from account num, displayname from remote
    {FromNumUri,AccUser1} =
        case SipUser of
            undefined -> {FromUri,undefined}; % x0,x6
            _ -> {FromUri#uri{user=maps:get(phonenumber,SipUser)},AccUser} % x1,x2,x3,x4,x5
        end,
    %%%%%----------------------
    % route from num
%%     Fconcat = fun(Add) ->
%%                       case is_numeric(Add) of
%%                           true ->
%%                               UserPhone = FromNumUri#uri.user,
%%                               Uri = FromNumUri#uri{user= <<UserPhone/bitstring,Add/bitstring>>},
%%                               {Uri,FU};
%%                           false ->
%%                               {FromNumUri,undefined}
%%                       end end,
    % #189
    Fconcat = fun(Base) ->
                      case SipUser of
                           undefined -> {FromNumUri,undefined};
                          _ ->
                              UserPhone = FromNumUri#uri.user,
                              SUOpts = maps:get(opts,SipUser),
                              case maps:get(<<"modextin">>,SUOpts,<<>>) of
                                  <<>> -> {FromNumUri,undefined};
                                  Modifier ->
                                      case apply_modifier_ext(Modifier, b(Base), [AccUser,UserPhone,b(FU),b(CU)]) of
                                          <<>> -> {FromNumUri,undefined};
                                          Extension ->
                                              Uri = FromNumUri#uri{user= <<UserPhone/bitstring,Extension/bitstring>>},
                                              {Uri,Extension}
                                      end end end end,
    {RouteFromUri,FNumExt} =
        case {FromUri#uri.user==AccUri#uri.user, AccUser1/=undefined} of
%%             {true,true} -> % x1,x2,x3,x4
%%                 case CU of
%%                     undefined -> {FromNumUri,undefined};     % x1
%%                     <<>> -> {FromNumUri,undefined};          % x2
%%                     FU -> {FromNumUri,undefined};            % x3
%%                     _ when is_binary(CU) -> Fconcat(CU)      % x4
%%                 end;
            {true,true} -> Fconcat(CU);    % #189             % x1,x2,x3,x4
            {false,true} -> Fconcat(FU);                      % x5
            {false,false} -> {FromNumUri, undefined};         % x6
            {true,false} -> {FromNumUri, undefined}           % x0
        end,
    %% --------------------------
    A = RouteFromUri#uri.user,    % username for representative
    B = RouteFromUri#uri.user,    % username for routing
    C = case SipUser of           % forwarded displayname
            undefined -> FDN; % x0,x6
            _ -> % x1,x2,x3,x4,x5
                case maps:get(name,SipUser) of
                    <<>> -> FDN;
                    T ->
                        FDN1 = ?U:unquote_display(FDN),
                        Fextdisp = fun() -> build_ext_disp(RouteFromUri#uri.user,FD) end, % #366
                        ?U:quote_display(apply_modifier_disp(T,FDN1,[FDN1,FU,A,Fextdisp]))
                end end,
    D = FD,                       % from domain
    SipUserInfo =
        case SipUser of
            undefined -> undefined;
            _ -> [{login,maps:get(login,SipUser)},
                  {domain,FD},
                  {phone,maps:get(phonenumber,SipUser)},
                  {name,maps:get(name,SipUser)},
                  {opts,maps:get(opts,SipUser)},
                  {extension,FNumExt}]
        end,
    %% --------------------------
    Abon = case SipUser of
               undefined -> {other, A};
               _ -> {sipuser, maps:get(login,SipUser)}
           end,
    %% --------------------------
    %?OUT("FROM BUILD: A='~s', B='~s', C='~s', D='~s', ~n\tSipUserInfo=~500p", [A,B,C,D,SipUserInfo]),
    {A,B,C,D,SipUserInfo,Abon}.

%% @private #366, #380
build_ext_disp(User,Domain) ->
    Req = {get_addressbook_contact,User,[displayname]},
    case ?U:call_domaincenter(Domain,Req,undefined) of
        undefined -> <<>>;
        not_found -> <<>>;
        {ok,Item} -> maps:get(displayname,Item)
    end.

%% ====================================================================
%% API Modifier
%% ====================================================================

%% % return true when Value contain only numeric symbols
%% is_numeric(Value) ->
%%     lists:all(fun(X) when X>=$0,X=<$9 -> true; (_) -> false end, ?EU:to_list(Value)).

% return binary or empty
b(X) when is_binary(X) -> X;
b(_) -> <<>>.


%% ----------
%% APPLY_MODIFIER PROXY
%% modifies displayname (applies sipuser name modifier to existing displayname)
%%  Existing values list:: [ExDisplayName::binary(), ExUserName::binary(), ComplexNumberPbx::binary()]
%%
%% regex or text mask
%%  powered by r_env_modifier. To accelerate ~10% text modifier it could be used locally without mapping some fields
%% ----------
-spec apply_modifier_disp(NameModifier::binary(), Value::binary(), ExistingValuesList::list()) -> binary().
%% ----------
apply_modifier_disp(Modifier, Value, [FDN,FU,Num,ExtDisp|_]) when is_binary(Modifier) ->
    Flow = fun(X) when is_function(X,0) -> fun() -> string:to_lower(?EU:to_list(X())) end;
              (X) -> fun() -> string:to_lower(?EU:to_list(X)) end end,
    BraceOpts = [{$D,FDN}, {$d,Flow(FDN)},
                 {$U,FU}, {$u,Flow(FU)},
                 {$N,Num}, {$n,Flow(Num)},
                 {$A,ExtDisp}, {$a,Flow(ExtDisp)} % #366
                ],
    ?ENVMODIFIER:apply_modifier(Modifier, Value, BraceOpts, [ebrace,regex]);
apply_modifier_disp(Modifier, Value, [FDN,FU,Num|_]) when is_binary(Modifier) ->
    apply_modifier_disp(Modifier, Value, [FDN,FU,Num,<<>>]).

%% ----------
%% #189
%% modifies username (applies sipuser extension modifier to mainly differenting value - from or contact username)
%%  Existing values list:: [ExDisplayName::binary(), ExUserName::binary(), ComplexNumberPbx::binary()]
%%
%% regex or text mask
%%  powered by r_env_modifier. To accelerate ~10% text modifier it could be used locally without mapping some fields
%% ----------
-spec apply_modifier_ext(NameModifier::binary(), Value::binary(), ExistingValuesList::list()) -> binary().
%% ----------
apply_modifier_ext(Modifier, Value, [AU,AN,FU,CU|_]) when is_binary(Modifier) ->
     Flow = fun(X) -> fun() -> string:to_lower(?EU:to_list(X)) end end,
     BraceOpts = [{$U,AU}, {$u,Flow(AU)},
                  {$N,AN}, {$n,Flow(AN)},
                  {$F,FU}, {$f,Flow(FU)},
                  {$C,CU}, {$c,Flow(CU)}
                 ],
     ?ENVMODIFIER:apply_modifier(Modifier, Value, BraceOpts, [ebrace,regex,x,asterisk]).