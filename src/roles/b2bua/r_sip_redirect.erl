%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 4.02.2017
%%% @doc B2BUA redirect routines (number modifier used for pbx extensions)

-module(r_sip_redirect).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([apply_modifier/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").

%% ====================================================================
%% API functions
%% Modifier
%% ====================================================================

%% APPLY_MODIFIER PROXY
%% modifies username (applies tran number to fnum (extension))
%%  Existing values list:: [ExDisplayName::binary(), ExUserName::binary(), ComplexNumberPbx::binary()]
-spec apply_modifier(NameModifier::binary(), Value::binary(), ExistingValuesList::list()) -> binary().

%% regex or text mask
%%  powered by r_env_modifier. To accelerate ~10% text modifier it could be used locally without mapping some fields
apply_modifier(Modifier, Value, _) when is_binary(Modifier) ->
     BraceOpts = [{$V,Value}, {$v,Value}, {$N,Value}, {$n,Value}],
     ?ENVMODIFIER:apply_modifier(Modifier, Value, BraceOpts, [ebrace,regex]).
