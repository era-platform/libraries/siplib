%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 05.2016 refactored 31.12.2016
%%% @doc Router CDR eventing funs

-module(r_sip_b2bua_router_event).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([event/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_headers.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_b2b_invite_router.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

event(Type,[Ctx|_]=Args) ->
    case maps:get(event,Ctx,true) of
        false -> ok;
        true ->
            case Type of
                'invite' -> erlang:apply(fun event_invite/2, Args);
                'fin' -> erlang:apply(fun event_fin/2, Args);
                'redirect' -> erlang:apply(fun event_redirect/2, Args);
                'replacing' -> erlang:apply(fun event_replacing/3, Args);
                'feature' -> erlang:apply(fun event_feature/3, Args);
                'findroute' -> erlang:apply(fun event_findroute/3, Args);
                'route' -> erlang:apply(fun event_route/3, Args);
                'route_referred' -> erlang:apply(fun event_routereferred/1, Args);
                'represenative' -> erlang:apply(fun event_represenative/2, Args)
            end end.

%% ====================================================================
%% Internal functions
%% ====================================================================

% invite
event_invite(Ctx, AOR) ->
    Req = maps:get(req, Ctx),
    [ACallId,ACallIdH,IID,ITS,AUser] = ?EU:maps_get([acallid,acallidhash,invite_id,invite_ts,from_sipuser], Ctx),
    #sipmsg{from={From,_}, to={To,_}, contacts=[Contact|_]}=Req,
    {IsOuter,ProviderCode} = case ?U:parse_ext_account_header(Req) of
                                 {{_U,_D},AOpts} -> {true,?EU:get_by_key(<<"code">>,AOpts)};
                                 undefined -> {false,null}
                             end,
    Fnumber = fun(undefined,Uri) -> Uri#uri.user;
                   ([_|_]=SipUser,_) ->
                      case ?EU:get_by_key(extension,SipUser,undefined) of
                          undefined -> ?EU:get_by_key(phone,SipUser,<<>>);
                          Ext -> Ext
                      end end,
    Opts = #{ruri => AOR,
             to => To,
             from => From,
             fromusername => From#uri.user,
             fromnumber => Fnumber(AUser,From),
             fromdomain => From#uri.domain,
             fromouter => IsOuter,
             fromprovidercode => ProviderCode,
             callednum => To#uri.user,
             networkaddr => Contact#uri.domain,
             relates => maps:get(relates,Ctx),
             ctx_scrid_master => maps:get(ctx_scrid_master,Ctx),
             ctx_scrid_domain => maps:get(ctx_scrid_domain,Ctx),
             esgdlg => maps:get(esgdlg,Ctx),
             ghost_mode => maps:get(ghost_mode,Ctx)},
    % TODO RefByOpts get
    Opts1 = case nksip_request:header("referred-by", Req) of
                {ok,[RefH|_]} ->
                    [#uri{}=Uri|_] = ?U:parse_uris(RefH),
                    RefByOpts = maps:get(referredby_opts,Ctx,[]), %
                    Opts#{is_referred => true,
                          referred_dialogid => case lists:keyfind(<<"r-dlgid">>,1,RefByOpts) of {_,RD} -> RD; false -> <<>> end,
                          referred_acallid => case lists:keyfind(<<"r-acallid">>,1,RefByOpts) of {_,RC} -> ?EU:urldecode(RC); false -> <<>> end,
                          referred_side => case lists:keyfind(<<"r-side">>,1,RefByOpts) of {_,RS} -> RS; false -> <<>> end,
                          referred_by => Uri#uri{opts=[],ext_opts=[]}}; % may be leave other opts untouched
                _ ->
                    Opts#{is_referred => false}
            end,
    Opts2 = case nksip_request:header("replaces", Req) of
                {ok,[_|_]} -> Opts1#{is_replacing => true};
                _ -> Opts1#{is_replacing => false}
            end,
    ?EVENT:invite({{ACallId,ACallIdH},{IID,ITS}}, Opts2).

% fin
event_fin(Ctx, #{}=Opts) ->
    [ACallId,ACallIdH,IID,ITS] = ?EU:maps_get([acallid,acallidhash,invite_id,invite_ts], Ctx),
    Opts1 = Opts#{relates => maps:get(relates,Ctx),
                  ctx_scrid_master => maps:get(ctx_scrid_master,Ctx),
                  ctx_scrid_domain => maps:get(ctx_scrid_domain,Ctx)},
    ?EVENT:route_fin({{ACallId,ACallIdH},{IID,ITS}}, Opts1).

% redirect
event_redirect(Ctx, Opts) ->
    case maps:get(event,Ctx,true) of
        false -> ok;
        true ->
            [ACallId,ACallIdH,IID,ITS] = ?EU:maps_get([acallid,acallidhash,invite_id,invite_ts], Ctx),
            Opts1 = Opts#{relates => maps:get(relates,Ctx),
                          ctx_scrid_master => maps:get(ctx_scrid_master,Ctx),
                          ctx_scrid_domain => maps:get(ctx_scrid_domain,Ctx)},
            ?EVENT:route_redirect({{ACallId,ACallIdH},{IID,ITS}}, Opts1)
    end.

% replaces
event_replacing(Ctx, AOR, {CallId,RepDlg}) ->
    {{RepDlgId,RepDlgIdHash},{RepACallId,RepACallIdHash},{RepIID,RepITS}}=RepDlg,
    [ACallId,ACallIdH,IID,ITS] = ?EU:maps_get([acallid,acallidhash,invite_id,invite_ts], Ctx),
    Opts = #{ruri => AOR,
             replaced_callid => CallId,
             replaced_dialogid => RepDlgId,
             replaced_dialogidhash => RepDlgIdHash,
             replaced_acallid => RepACallId,
             replaced_acallidhash => RepACallIdHash,
             replaced_iid => RepIID,
             replaced_its => RepITS,
             relates => maps:get(relates,Ctx),
             ctx_scrid_master => maps:get(ctx_scrid_master,Ctx),
             ctx_scrid_domain => maps:get(ctx_scrid_domain,Ctx)},
    ?EVENT:route_replacing({{ACallId,ACallIdH},{IID,ITS}}, Opts).

% feature
event_feature(Ctx, UNum, #{}=Feature) ->
    FId = maps:get(id, Feature),
    FType = maps:get(type, Feature),
    Prefix = maps:get(prefix, Feature),
    FExtension = maps:get(extension,Feature),
    [<<>>,FNum] = binary:split(UNum,Prefix),
    [ACallId,ACallIdH,IID,ITS] = ?EU:maps_get([acallid,acallidhash,invite_id,invite_ts], Ctx),
    Opts = #{featurecode_type => FType,
             featurecode_id => FId,
             featurecode_extension => FExtension,
             number => FNum,
             relates => maps:get(relates,Ctx),
             ctx_scrid_master => maps:get(ctx_scrid_master,Ctx),
             ctx_scrid_domain => maps:get(ctx_scrid_domain,Ctx)},
    ?EVENT:route_feature({{ACallId,ACallIdH},{IID,ITS}}, Opts).

% find route (cdr log, but no cdr db)
event_findroute(Ctx, FindOpts, Rules) ->
    [ACallId,ACallIdH,IID,ITS] = ?EU:maps_get([acallid,acallidhash,invite_id,invite_ts], Ctx),
    Opts = #{findopts => FindOpts,
             rules => lists:foldr(fun(R, Acc) -> [{maps:get(vector,R),maps:get(id,R)}|Acc] end, [], Rules),
             relates => maps:get(relates,Ctx),
             ctx_scrid_master => maps:get(ctx_scrid_master,Ctx),
             ctx_scrid_domain => maps:get(ctx_scrid_domain,Ctx)},
    ?EVENT:route_find({{ACallId,ACallIdH},{IID,ITS}}, Opts).

% route
event_route(Ctx, #{}=Opts, Rule) ->
    [ACallId,ACallIdH,IID,ITS] = ?EU:maps_get([acallid,acallidhash,invite_id,invite_ts], Ctx),
    CD = maps:get(domain,Ctx),
    Opts1 = Opts#{domain => CD,
                  ruleid => maps:get(id,Rule),
                  action => maps:get(action,Rule),
                  to => maps:get(to_uri,Ctx),
                  by => maps:get(by_uri,Ctx),
                  from => maps:get(from_uri,Ctx),
                  vector => maps:get(vector,Rule),
                  isreferred => maps:get(isreferred, Ctx, false),
                  isredirected => maps:get(isredirected, Ctx, false),
                  relates => maps:get(relates,Ctx),
                  ctx_scrid_master => maps:get(ctx_scrid_master,Ctx),
                  ctx_scrid_domain => maps:get(ctx_scrid_domain,Ctx)},
    ?EVENT:route({{ACallId,ACallIdH},{IID,ITS}}, Opts1).

event_routereferred(Ctx) ->
    [ACallId,ACallIdH,IID,ITS] = ?EU:maps_get([acallid,acallidhash,invite_id,invite_ts], Ctx),
    [IsReferred, ByURI, LinkReferred, LinkIID, LinkITS, LinkDlgId, LinkSide] = ?EU:maps_get([isreferred,by_uri,link_referred,link_iid,link_its,link_dlgid,link_side], Ctx),
    Opts = case maps:find(link_replaces, Ctx) of
        {ok, Val} -> #{link_replaces => Val};
        _ -> #{}
    end,
    Opts2 = Opts#{is_referred => IsReferred,
                  by_uri => ByURI,
                  link_referred => LinkReferred,
                  link_iid => LinkIID,
                  link_its => LinkITS,
                  link_dlgid => LinkDlgId,
                  link_side => LinkSide,
                  relates => maps:get(relates,Ctx),
                  ctx_scrid_master => maps:get(ctx_scrid_master,Ctx),
                  ctx_scrid_domain => maps:get(ctx_scrid_domain,Ctx)},
    ?EVENT:route_referred({{ACallId,ACallIdH},{IID,ITS}}, Opts2).


% find representative
event_represenative(Ctx, #{}=Opts) ->
    [ACallId,ACallIdH,IID,ITS] = ?EU:maps_get([acallid,acallidhash,invite_id,invite_ts], Ctx),
    Opts1 = Opts#{relates => maps:get(relates,Ctx),
                  ctx_scrid_master => maps:get(ctx_scrid_master,Ctx),
                  ctx_scrid_domain => maps:get(ctx_scrid_domain,Ctx)},
    ?EVENT:representative({{ACallId,ACallIdH},{IID,ITS}}, Opts1).
