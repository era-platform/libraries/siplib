%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.2015 refactored 31.12.2016
%%% @doc Router general facade for invite requests and forking redirects

-module(r_sip_b2bua_router_invite).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([sip_invite/2,
         build_route_opts_onredirect/2]).

%% ==========================================================================
%% Defines
%% ==========================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_headers.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_b2b_invite_router.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% --------------------------------
%% Handles incoming invite (callid is unknown)
%% Checks if replacing invite,
%%  finds destination domain routing rules, apply to from/to
%%  and returns {reply,R} if error, denied, proxy..
%%       or noreply if starts fsm to handle dialog and invite forks (by found uri rules)
%% --------------------------------
-spec(sip_invite(AOR::tuple(), Request::nksip:request()) ->
        {reply, nksip:sipreply()} | noreply).
%% --------------------------------
sip_invite(AOR, Req) ->
%%      %% DEBUG
%%     #sipmsg{from={XFrom,XFT},to={XTo,XTT},headers=XH,contacts=[XC|XCR]}=XReq,
%%     ?OUT("FROM :~120p", [XFrom]),
%%     Req=XReq#sipmsg{from={XFrom#uri{user= <<"1202">>,disp= <<"Ivanov">>, domain= <<"test.rootdomain.ru">>},XFT},
%%                     %from={XFrom#uri{user= <<"sip1">>,domain= <<"test.rootdomain.ru">>},XFT},
%%                     %to={XTo#uri{user= <<"12">>, domain= <<"test.rootdomain.ru">>},XTT},
%%                     contacts=[XC#uri{user= <<"1202">>}|XCR],
%%                     headers=[{<<"authorization">>,<<"Digest username=\"sip2\";">>}|XH]
%%                    },
    #sipmsg{ruri=Ruri,
            to={To,_},
            from={From,_}}=Req,
    IID = ?EU:uuid_srvidx(), % RP-385, RP-640
    ITS = ?EU:timestamp(),
    % context, #429
    _RUri = ?U:clear_uri(Ruri, [user_orig]),
    #uri{user=ToNum0}=ToUri = ?U:clear_uri(To, [user_orig]), % made of AOR
    FromUri = ?U:clear_uri(From, [user_orig]),
    %% old, before internalpbx
    %FromUri1 = ?U:update_uri_display(FromUri),
    %FromUri1 = ?U:update_uri_user_to_phonenumber(FromUri),
    %% new
    %% make {fwd username (phonenumber), routed username (phonenumber), displayname, domain, SipUserInfo}
    {A,B,C,D,SipUserInfo,Abon} = ?FROM:build_from_abc(Req),
    FromUri1 = FromUri#uri{disp=C,user=A,domain=D},
    FromUri2 = FromUri1#uri{user=B},
    % update ToUri
    {ToNum,Dtmf2Send} = case binary:split(ToNum0,[<<",">>,<<"'">>]) of
                            [Num,Dtmf] -> {Num,Dtmf};
                            [Num] -> {Num,<<>>}
                        end,
    ToUri1 = ToUri#uri{user=ToNum},
    ToUri2 = case From of
                 #uri{domain=D} -> ToUri1;
                 _ -> ToUri1#uri{domain=D}
             end,
    %
    ACallId = Req#sipmsg.call_id, % #370
    OtherOpts = #{from=>A,fromr=>B,to=>To#uri.user,domain=>D,callid=>ACallId},
    ScrIdM = try_start_ctx_script(master,IID,D,OtherOpts),
    ScrIdD = try_start_ctx_script(domain,IID,D,OtherOpts),
    GhostMode = case nksip_request:header(?GhostHeaderLow, Req) of
                    {ok,[XRefToV|_]} -> XRefToV;
                    _ -> undefined
                end,
    RefByOpts = case nksip_request:header("referred-by", Req) of
                    {ok,[]} -> [];
                    {ok,[H1|_]} -> ?IR_REFERRED:get_referred_by_opts(H1)
                end,
    Ctx = #{req => Req,
            esgdlg => case ?U:parse_header_params(Req,?EsgDlgHeaderLow) of [] -> <<>>; [EsgDlg|_] -> EsgDlg end, % RP-1575
            acallid => ACallId, % #370
            acallidhash => erlang:phash2(ACallId), % #370
            invite_id => IID, % RP-385
            invite_ts => ITS,
            ghost_mode => GhostMode,
            % ------
            % header opts from other services. behaviour could be roughly managed (follow redirect, media mode, etc.)
                b2bhdr => lists:map(fun({K,V}) -> {K,?URLENCODE:decode(V)}; (T) -> T end, ?U:parse_header_params(Req,?B2BHeaderLow)),
            % sipuser options should be saved for representatives, response, sessionchange and so on.
                from_sipuser => SipUserInfo,
                from_displayname => C,
            % initial from uri for representative
                req_from_uri => FromUri1,
            % routing initial uris
                to_uri => ToUri2, % RUri     % for routing initial
                from_uri => FromUri2,        % for routing initial
                by_uri => FromUri2,          % for crossdomain and referred routing
                routecode => FromUri2#uri.user, % for routing over several rules without service modifying to and from
            % start by invite request, no redirected
                isredirected => false,
            % dialog relation for events
                relates => [{D,Abon}],
            % ctx script id
                ctx_scrid_master => ScrIdM,
                ctx_scrid_domain => ScrIdD,
            % outer entities bindings to call
                dlg_bindings => sets:new(),
            % referred-by opts
                referredby_opts => RefByOpts,
            % dtmf to send after 200 ok
                dtmf2send => Dtmf2Send
            },
    % event
    ?IR_EVENT:event('invite',[Ctx,AOR]),
    % bindings from header
    Ctx1 = update_bindings_from_header(Ctx,Req),
    % search route, trace rules
    case build_route_opts(Ctx1) of
        {return,R} -> R;
        {reply,_}=R -> R;
        {ok,L,Ctx2} when is_list(L) ->
            Req1 = maps:get(req,Ctx2),
            Opts = [{app,?SIPAPP},
                    {invite_id,IID}, % RP-385
                    {invite_ts,ITS},
                    {req,Req1},
                    {ghost_mode, GhostMode},
                    {call_pid,self()},
                    {forking_timeout,3600000}  % @TODO timeout could be redundant (5min)
                                               % total max calling time,
                                               % note that uri can have it's own rule limit, default limit and after_answer limit,
                                               % also there can be sequental call, that needs more time to operate fully
                   |L],
            ?B2BUA_DLG:start(Opts),
            noreply
    end.

%% @private
update_bindings_from_header(Ctx,Req) ->
    case ?U:parse_bindings_header(Req) of
        undefined -> Ctx;
        Bindings ->
            Bindings1 = ?EU:deduplicate(Bindings),
            ?B2BUA_BINDS:do_dialog_bindings(add,Bindings1,Ctx)
    end.

%% --------------------------------
%% Handles redirect while forking
%% Executes routing process synchronously and returns result to FSM.
%% --------------------------------
-spec(build_route_opts_onredirect(Ctx::map(), Request::nksip:request()) ->
        {return,R::term} | % for proxing {r_back_to_route, {proxy, RUri, ...}}
        {reply, nksip:sipreply()} |
        {ok, UriRules::list(), Ctx1::map()}).
%% UriRules:: [{urirules,[Group1,Group2,...]},{ctx,CTX}]
%% Group:: [UriRule1,UriRule2,...]
%% UriRule:: map()
%% --------------------------------
build_route_opts_onredirect(Ctx, Req) ->
    case maps:get(restrict_redirect,Ctx,false) of
        true -> {reply,?Forbidden("B2B. Found redirect in restricted case"),Ctx};
        false -> build_route_opts_onredirect_1(Ctx, Req)
    end.

%% @private next
build_route_opts_onredirect_1(Ctx,Req) ->
    % check
    _To = maps:get(to_uri,Ctx),
    By = maps:get(by_uri,Ctx),
    _From = maps:get(from_uri,Ctx),
    % route
    ByUri = ?U:update_uri_display(?U:clear_uri(By)),
    %
    ACallId = Req#sipmsg.call_id, % #370
    Ctx1 = Ctx#{req => Req,
                acallid => ACallId, % #370
                acallidhash => erlang:phash2(ACallId), % #370
                by_uri := ?U:update_uri_user_to_phonenumber(ByUri),
                isredirected => true},
    build_route_opts(Ctx1).


%% =================================================================================================
%% Internal functions
%% =================================================================================================

%% ----------------------
%% make routing and return URI rules in Opts
%% ----------------------
build_route_opts(Ctx) ->
    #sipmsg{from={From,_}}=Req = maps:get(req, Ctx),
    Ctx1 = ?IR_UTILS:update_ctx_dir(Ctx#{fwdcount => 10,
                                         nextcount => 10}, From, Req),
    do_invite(Ctx1).

%% ----------------------
%% choose route mode, invite could be referred or referred+replaces
%% ----------------------

%% check already redirected
do_invite(Ctx) ->
    case maps:get(isredirected,Ctx) of
        true -> do_invite_by_rule(Ctx);
        false -> do_invite_1(Ctx)
    end.

%% check replaces
do_invite_1(Ctx) ->
    case ?IR_REFERRED:check_referred(Ctx) of
        {ok,replaces,Ctx1} -> ?IR_REFERRED:route_replaces(Ctx1);
        {ok,referred,Ctx1} -> do_invite_2(Ctx1);
        {false,Ctx1} -> do_invite_2(Ctx1)
    end.

%% RP-1344 
%% check dummies
do_invite_2(Ctx) ->
    #uri{user=UN,domain=Domain} = maps:get(to_uri,Ctx),
    case UN of
        <<?DummyIvrPrefix,IvrCode/binary>> ->
            AOR = {sip,UN,Domain},
            ?IR_FEATURE_SVC:do_invite_ivr(IvrCode,AOR,Ctx);
        _ -> do_invite_by_rule(Ctx)
    end.

%% ---------------------------
%% when call routed by rule (could be redirected or referred)
%% ---------------------------

% mapping to route by rule
do_invite_by_rule(#{isredirected:=true}=Ctx) -> do_invite_from_inner_by_rule_1(Ctx);
do_invite_by_rule(#{isreferred:=true}=Ctx) -> do_invite_from_inner_by_rule_1(Ctx);
do_invite_by_rule(#{dir:='inner'}=Ctx) -> do_invite_from_inner_by_rule_1(Ctx);
do_invite_by_rule(#{dir:='outer'}=Ctx) -> do_invite_from_outer_by_rule_1(Ctx).

%% ---------------------------
try_start_ctx_script(DomainType,IID,D,OtherOpts) ->
    case get_ctx_scrcode(DomainType,D) of
        undefined -> undefined;
        Code -> start_ctx_script(DomainType,IID,Code,D,OtherOpts)
    end.

%% @private
get_ctx_scrcode(master,_D) ->
    get_ctx_scrcode_1(?EU:to_binary(?CFG:get_general_domain()));
get_ctx_scrcode(domain,D) ->
    get_ctx_scrcode_1(D).
%%
get_ctx_scrcode_1(Domain) ->
    case ?ENVDC:get_object_sticky(Domain,settings,[{keys,[<<"callscrcode_context">>]},{fields,[value]}],auto) of
        {ok,[Map],_} when is_map(Map) ->
            ScrCode = maps:get(value,Map),
            case ?ENVDC:get_script_sticky(Domain, 'svc', ScrCode) of
                #{}=_Item -> ScrCode;
                _ -> undefined
            end;
        _ -> undefined
    end.

%% @private
start_ctx_script(DomainType,IID,Code,D,OtherOpts) ->
    ?B2BUA_CTX:start_ctx_script(DomainType,IID,Code,D,OtherOpts).

%% =============================================
%% Invite from inner
%% =============================================

%%
do_invite_from_inner_by_rule_1(Ctx) ->
    % from request or overriden
    #uri{domain=TD} = maps:get(to_uri,Ctx),
    #uri{domain=BD} = maps:get(by_uri,Ctx),
    % check domains
    case BD == TD of
        true -> do_invite_from_inner_by_rule_2(Ctx);
        false -> ?IR_UTILS:reply(Ctx, ?Forbidden("B2B. Cross domain call blocked"))
    end.
%%
do_invite_from_inner_by_rule_2(Ctx) ->
    % from request or overriden
    #uri{domain=TD}=ToUri = maps:get(to_uri,Ctx),
    ByUri = maps:get(by_uri,Ctx),
    [ToAOR,ByAOR] = ?U:make_aors([ToUri,ByUri]),
    RouteCode = maps:get(routecode,Ctx,<<>>),
    % dir
    Dir = 'inner',
    % find
    FindOpts = [TD, Dir, ByAOR, ToAOR, <<>>, RouteCode],
    case ?IR_RULE:find_route(Ctx#{domain=>TD,
                                  findopts=>FindOpts}) of
        {ok,R,Ctx1} -> en(R,Ctx1);
        {error,_,Ctx1} -> ?IR_UTILS:reply(Ctx1, ?Forbidden("B2B. Routing error"));
        {reply,SipReply,Ctx1} -> ?IR_UTILS:reply(Ctx1, SipReply)
    end.

%% =============================================
%% Invite from outer
%% =============================================

%%
do_invite_from_outer_by_rule_1(#{req:=Req}=Ctx) ->
    % from request or overriden
    #uri{scheme=BS,user=BU,domain=BD} = maps:get(by_uri,Ctx),
    #uri{scheme=TS,user=TU,domain=TD} = maps:get(to_uri,Ctx),
    ByAOR = {BS,BU,BD},
    ToAOR = {TS,TU,TD},
    RouteCode = maps:get(routecode,Ctx,<<>>),
    % extaccount
    case ?U:parse_ext_account_header(Req) of
        undefined -> ExtAccCode = <<>>;
        {_,ExtOpts} -> case lists:keyfind(<<"code">>,1,ExtOpts) of
                           {_,ExtAccCode} -> ok;
                           false -> ExtAccCode = <<>>
                       end end,
    % dir & acc
    {Dir,Ext} = case maps:is_key(redirected_by, Ctx) orelse maps:is_key(referred_by, Ctx) of
                    true -> {'inner', <<>>}; % when redirected or referred by inner
                    false -> {'outer', ExtAccCode} % when same domain as entrance, not referred
                end,
    % find
    FindOpts = [TD, Dir, ByAOR, ToAOR, Ext, RouteCode],
    case ?IR_RULE:find_route(Ctx#{domain=>TD,
                                  findopts=>FindOpts}) of
        {ok,R,Ctx1} -> en(R,Ctx1);
        {error,_,Ctx1} -> ?IR_UTILS:reply(Ctx1, ?Forbidden("B2B. Routing error"));
        {reply,SipReply,Ctx1} -> ?IR_UTILS:reply(Ctx1, SipReply)
    end.

%% =============================================
%% Invite from outer
%% =============================================

en({error,{_,Reason}}, Ctx) -> ?IR_UTILS:reply(Ctx, ?InternalError(?EU:to_list(Reason)));
en({reply,{Code,Reason}}, Ctx) -> ?IR_UTILS:reply(Ctx, ?ReplyOpts(Code,Reason));
en({ok,{inside,{rules,[CalledAOR,UriRules]}}}, Ctx) -> ?IR_INSIDE:do_invite_inside(CalledAOR,UriRules,Ctx);
en({ok,{insidepbx,{rules,[CalledAOR,_FNum,UriRules]}}}, Ctx) -> ?IR_INSIDE:do_invite_inside(CalledAOR,UriRules,Ctx);
en({ok,{urirules,{rules,[CalledAOR,UriRules]}}}, Ctx) -> ?IR_URIRULES:do_invite_urirules(CalledAOR,UriRules,Ctx);
en({ok,{outside,[_ToU,_RouteCode,SrvIdx,_Code,Account]}}, Ctx) -> ?IR_OUTSIDE:do_invite_outside(SrvIdx,Account,Ctx);
en({ok,{feature,Feature}}, Ctx) ->
    case ?IR_FEATURE:do_invite_feature(Feature,Ctx) of
        {re,{ok,R,Ctx1}} -> en(R,Ctx1);
        T -> T
    end.
