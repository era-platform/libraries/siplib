%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 02.2016 refactored 31.12.2016
%%% @doc Invite routing to featurecode functions

-module(r_sip_b2bua_router_invite_feature).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([do_invite_feature/2,
         do_invite_conference/2,
         do_invite_ivr/4,
         do_invite_hunt/4,
         do_invite_prompt/4]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_headers.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_b2b_invite_router.hrl").

-define(IVR_UTILS, r_sip_ivr_utils).

%% ====================================================================
%% API functions
%% ====================================================================

%% -----------------------------------------------------
%% initiates feature call, checks available features
%% -----------------------------------------------------

do_invite_feature({<<"pickup">>, [{S,_U,D}=_AOR,FNum,_]}, Ctx) ->               do_invite_pickup_number({S,FNum,D}, Ctx);
do_invite_feature({<<"grouppickup">>, [{_S,_U,D}=_AOR,_,_]}, Ctx) ->            do_invite_pickup_group(D, Ctx);
do_invite_feature({<<"conference">>, [{S,_U,D}=_AOR,FNum,_]}, Ctx) ->           do_invite_conference({S,FNum,D}, Ctx);
do_invite_feature({<<"videoconference">>, [{S,_U,D}=_AOR,FNum,_]}, Ctx) ->      do_invite_conference({S,FNum,D}, Ctx);
do_invite_feature({<<"ivr">>, [{S,_U,D}=_AOR,FNum,Extns]}, Ctx) ->              do_invite_ivr(call, {S,FNum,D}, Extns, Ctx);
do_invite_feature({<<"hunt">>, [{S,_U,D}=_AOR,FNum,Extns]}, Ctx) ->             do_invite_hunt(call, {S,FNum,D}, Extns, Ctx);
do_invite_feature({<<"voicemail">>, [{S,_U,D}=_AOR,FNum,Extns]}, Ctx) ->        do_invite_ivr(call, {S,FNum,D}, Extns, Ctx);
do_invite_feature({<<"voicemail_send">>, [{S,_U,D}=_AOR,FNum,Extns]}, Ctx) ->   do_invite_ivr(call, {S,FNum,D}, Extns, Ctx);
do_invite_feature({<<"parking">>, [{S,_U,D}=_AOR,FNum,Extns]}, Ctx) ->          do_invite_ivr(call, {S,FNum,D}, Extns, Ctx);
do_invite_feature({<<"parking_get">>, [{S,_U,D}=_AOR,FNum,Extns]}, Ctx) ->      do_invite_ivr(call, {S,FNum,D}, Extns, Ctx);
do_invite_feature({<<"fax_to_email">>, [{S,_U,D}=_AOR,FNum,Extns]}, Ctx) ->     do_invite_ivr(call, {S,FNum,D}, Extns, Ctx);
do_invite_feature({<<"intercom">>, [{S,_U,D}=_AOR,FNum,Extns]}, Ctx) ->         do_invite_intercom(call, {S,FNum,D}, Extns, Ctx);
do_invite_feature({<<"barge">>, [{S,_U,D}=_AOR,FNum,Extns]}, Ctx) ->            do_invite_barge(call, {S,FNum,D}, Extns, Ctx);
do_invite_feature({<<"replace">>, [{S,_U,D}=_AOR,FNum,Extns]}, Ctx) ->          do_invite_replace(call, {S,FNum,D}, Extns, Ctx);
do_invite_feature({<<"prompt">>, [{S,_U,D}=_AOR,FNum,Extns]}, Ctx) ->           do_invite_prompt(prompt, {S,FNum,D}, Extns, Ctx);
do_invite_feature({<<"monitor">>, [{S,_U,D}=_AOR,FNum,Extns]}, Ctx) ->          do_invite_prompt(monitor, {S,FNum,D}, Extns, Ctx);
do_invite_feature({<<"mesh">>, [{S,_U,D}=_AOR,FNum,Extns]}, Ctx) ->             do_invite_prompt(mesh, {S,FNum,D}, Extns, Ctx);
do_invite_feature(_, Ctx) -> ?IR_UTILS:reply(Ctx, ?InternalError("B2B. Unknown featurecode type")).


%% ====================================================================
%% Internal functions
%% ====================================================================

%% ----------------------------------------
%% feature pickup
%% ----------------------------------------

%% ------------
%% Pickup from number
%% ------------
do_invite_pickup_number(AOR, Ctx) ->
    case ?USRSTATE:find_pickup_dialog(AOR) of
        {error,not_found} -> ?IR_UTILS:reply(Ctx, ?NotFound("B2B. Calls not found"));
        {error,R} -> ?IR_UTILS:reply(Ctx, ?Forbidden("B2B. Pickup error: "++?EU:to_list(R)));
        {ok,R} -> do_pickup(R,Ctx)
    end.

%% ------------
%% Pickup from group
%% ------------
do_invite_pickup_group(Domain, Ctx) ->
    Req = maps:get(req, Ctx),
    #sipmsg{from={#uri{user=FromUser,domain=FromDomain},_}}=Req,
    case FromDomain == Domain of
        false -> ?IR_UTILS:reply(Ctx, ?Forbidden("B2B. Pickup group error: caller out of domain"));
        true ->
            case ?NUMBERPLAN:find_pickup_users_by_groups_for(FromUser, Domain) of
                undefined -> ?IR_UTILS:reply(Ctx, ?InternalError("B2B. Pickup DC error"));
                [] -> ?IR_UTILS:reply(Ctx, ?Forbidden("B2B. Pickup group error: no group found"));
                Numbers ->
                    F = fun(Num,{undefined,_}) ->
                                AOR = {sip,Num,Domain},
                                case ?USRSTATE:find_pickup_dialog(AOR) of
                                    {error,R} -> {undefined,R};
                                    {ok,R} -> do_pickup(R,Ctx)
                                end;
                           (_,R) -> R
                        end,
                    case lists:foldl(F, {undefined,no_group}, Numbers) of
                        {undefined, not_found} -> ?IR_UTILS:reply(Ctx, ?Forbidden("B2B. Pickup group calls not found"));
                        {undefined, R} -> ?IR_UTILS:reply(Ctx, ?Forbidden("B2B. Pickup group error: "++?EU:to_list(R)));
                        R -> R
                    end end end.

%% ------------
% @private
do_pickup({SrvIdx,DialogId}, Ctx) ->
    Req = maps:get(req, Ctx),
    case ?CFG:get_my_sipserver_index() of
        SrvIdx ->
            Dlg = ?B2BUA_DLG:pull_fsm_by_dialogid(DialogId),
            FromSipUser = maps:get(from_sipuser,Ctx),
            FromReqUri = maps:get(req_from_uri,Ctx),
            ?B2BUA_DLG:sip_invite_pickup(Dlg, [Req#sipmsg.call_id, Req, FromSipUser, FromReqUri]),
            Reply = noreply,
            {return,Reply};
        _ ->
            #sipmsg{ruri=RUri, call_id=CallId}=Req,
            case ?SERVERS:find_responsible_sip_srvidx(SrvIdx, CallId) of
                false -> ?IR_UTILS:reply(Ctx, ?ServiceUnavailable("B2B. Pickup dialog responsible server not found"));
                RouteDomain ->
                    ?LOG("INVITE_ROUTER PROXY TO ANOTHER B2BUA: ~p ~p -> ~p", [SrvIdx, CallId, RouteDomain]),
                    % when proxing to insiders - stored by callid for next aprior routing
                    ?SIPSTORE:store_t(CallId, {?ProxyToInsider, SrvIdx}, 3 * 3600 * 1000), % 3 hours, delete on call_terminate
                    RouteUri = {route, ?U:unparse_uri(#uri{scheme=sip, domain=RouteDomain, opts=?CFG:get_transport_opts()++[<<"lr">>]})},
                    Reply = {r_back_to_route, {proxy, RUri, [RouteUri, record_route]}},
                    ?IR_EVENT:event(fin,[Ctx,#{action => proxy,
                                                 reason => pickup,
                                                 srvidx => SrvIdx}]),
                    {return,Reply}
            end end.

%% ----------------------------------------
%% feature conference
%% ----------------------------------------

do_invite_conference({Sch,Num,Domain}=AOR, Ctx) ->
    ConfUser = ?U:make_conf_user(Num),
    GStoreKey = {conf,{Sch,ConfUser,Domain}},
    case find_conf(Domain, GStoreKey) of
        undefined ->
            ?IR_UTILS:reply(Ctx, ?InternalError("B2B. Store not found, unable to find conference"));
        {ok,_Site,SrvIdx} ->
            ?IR_FEATURE_SVC:do_invite_service(SrvIdx, AOR, Ctx, ["Conf", ConfUser, true, ?U:is_record_conf(), []]);
        false ->
            Site = ?CFG:get_domain_site(Domain),
            case ?CFG:get_site_conf(Site) of
                [] -> ?IR_UTILS:reply(Ctx, ?ServiceUnavailable("B2B. Conf inaccessible (a)"));
                L when is_list(L) ->
                    case ?IR_FEATURE_SVC:select_responsible(random, Site, lists:map(fun({N,_}) -> N end, L)) of
                        empty -> ?IR_UTILS:reply(Ctx, ?ServiceUnavailable("B2B. Conf inaccessible (b)"));
                        {ok,Node} ->
                            SrvIdx = ?CFG:get_node_index(Node),
                            ?IR_FEATURE_SVC:do_invite_service(SrvIdx, AOR, Ctx, ["Conf", ?U:make_conf_user(Num), true, ?U:is_record_conf(), []])
                    end end end.

% @private
% find conference in stores of all sites of dest domain
% returns undefined if no domain site stores found, false if not found in store, {ok,Site,SrvIdx} if conf found somewhere
find_conf(Domain,GStoreKey) ->
    Sites = ?CFG:get_domain_sites(Domain),
    lists:foldl(fun(Site, Acc) when Acc==undefined; Acc==false ->
                        case ?U:call_store(Site, GStoreKey, fun() -> {get, [GStoreKey]} end, undefined) of
                            undefined when Acc==undefined -> undefined;
                            undefined -> false;
                            false -> false;
                            {ok,#{srvidx:=SrvIdx}} -> {ok,Site,SrvIdx}
                        end;
                   (_, Acc) -> Acc
                end, undefined, Sites).

%% ----------------------------------------
%% feature ivr
%% ----------------------------------------

do_invite_ivr(replace, {_,User,_}=AOR, RepMap, Ctx) when is_map(RepMap) ->
    TTag = maps:get(totag,RepMap),
    case ?IVR_UTILS:extract_ivr_server_index(TTag) of
        false -> ?IR_UTILS:reply(Ctx, ?ServiceUnavailable("B2B. IVR tag not found to replace"));
        {true, SrvIdx} -> ?IR_FEATURE_SVC:do_invite_service(SrvIdx, AOR, Ctx, ["IVR", User, true, ?U:is_record_ivr(), []])
    end;

do_invite_ivr(call, {_,_Num,Domain}=AOR, IvrCode, Ctx) ->
    case ?DC:check_ivrscript_exists_by_code(Domain, IvrCode) of
        false -> ?IR_UTILS:reply(Ctx, ?ServiceUnavailable("B2B. IVR script not found"));
        {true,_Id} -> ?IR_FEATURE_SVC:do_invite_ivr(IvrCode,AOR,Ctx)
    end.

%% ----------------------------------------
%% feature hunt
%% ----------------------------------------

do_invite_hunt(replace, {_,User,_}=AOR, RepMap, Ctx) when is_map(RepMap) ->
    TTag = maps:get(totag,RepMap),
    case ?IVR_UTILS:extract_ivr_server_index(TTag) of
        false -> ?IR_UTILS:reply(Ctx, ?ServiceUnavailable("B2B. IVR tag not found to replace"));
        {true, SrvIdx} -> ?IR_FEATURE_SVC:do_invite_service(SrvIdx, AOR, Ctx, ["IVR", User, true, ?U:is_record_ivr(), []])
    end;

do_invite_hunt(call, {_,<<>>,_}=_AOR, <<>>, Ctx) -> ?IR_UTILS:reply(Ctx, ?ServiceUnavailable("B2B. Hunt undefined"));
do_invite_hunt(call, {_,Num,_}=AOR, <<>>, Ctx) -> do_invite_hunt_1(Num, AOR, Ctx); % by feature extra number (extra digits of callednumber after featurecode number)
do_invite_hunt(call, {_,_,_}=AOR, Extns, Ctx) -> do_invite_hunt_1(Extns, AOR, Ctx). % by feature extension field (fixed number of huntsip by featurecode)

%% @private
do_invite_hunt_1(SearchKey,{_,_Num,Domain}=AOR, Ctx) ->
    case check_huntsip(Domain, SearchKey) of
        {true, [Id, NumH, <<"sipuser">>]} -> ?IR_FEATURE_SVC:do_invite_hunt({SearchKey,Id,NumH}, AOR, Ctx);
        {true, _} -> ?IR_UTILS:reply(Ctx, ?ServiceUnavailable("B2B. Hunt type invalid"));
        false -> ?IR_UTILS:reply(Ctx, ?ServiceUnavailable("B2B. Hunt not found"))
    end.

%% @private
check_huntsip(Domain, SearchKey) ->
    case ?ENVDC:get_object_nosticky(Domain,'hunt',[{cachekeys,[{phonenumber,SearchKey}]},{fields,[id,phonenumber,type]}],false,auto) of
        {ok,[Item],_CheckSum} -> {true,?EU:maps_get([id,phonenumber,type],Item)};
        {ok,[],_CheckSum} -> false;
        _ -> false
    end.

%% ----------------------------------------
%% feature intercom
%% ----------------------------------------

do_invite_intercom(call, {_,FNum,_}, <<>>, Ctx) -> do_invite_intercom_1(FNum, Ctx);
do_invite_intercom(call, _, Extns, Ctx) -> do_invite_intercom_1(Extns, Ctx).

% @private
do_invite_intercom_1(FNum, Ctx) ->
    ToUri = maps:get(to_uri,Ctx),
    ToUri1 = ToUri#uri{user=FNum},
    % find
    [TD, Dir, ByAOR, _, ExtAC, RouteCode] = maps:get(findopts, Ctx),
    FindOpts = [TD, Dir, ByAOR, ?U:make_aor(ToUri1), ExtAC, RouteCode],
    %
    Ctx1 = Ctx#{intercom => true,
                restrict_redirect => true,
                findopts := FindOpts,
                to_uri := ToUri1},
    case ?IR_RULE:find_route(Ctx1) of
        {ok,{ok,{inside,_}},_}=Res -> {re,Res};
        {ok,{ok,{insidepbx,_}},_}=Res -> {re,Res};
        {ok,_,Ctx2} -> ?IR_UTILS:reply(Ctx2, ?Forbidden("B2B. Intercom number restricted"));
        {error,_,Ctx2} -> ?IR_UTILS:reply(Ctx2, ?Forbidden("B2B. Routing error"));
        {reply,SipReply,Ctx2} -> ?IR_UTILS:reply(Ctx2, SipReply)
    end.

%% ----------------------------------------
%% feature barge (barge in)
%% ----------------------------------------

do_invite_barge(call, {_,FNum,_}, <<>>, Ctx) -> do_invite_barge_1(FNum, Ctx);
do_invite_barge(call, _, Extns, Ctx) -> do_invite_barge_1(Extns, Ctx).

%% @private
do_invite_barge_1(FNum, Ctx) ->
    ToUri = maps:get(to_uri,Ctx),
    ToUri1 = ToUri#uri{user=FNum},
    % find
    [TD, Dir, ByAOR, _, ExtAC, RouteCode] = maps:get(findopts, Ctx),
    FindOpts = [TD, Dir, ByAOR, ?U:make_aor(ToUri1), ExtAC, RouteCode],
    %
    Ctx1 = Ctx#{barge => true,
                restrict_redirect => true,
                findopts := FindOpts,
                to_uri := ToUri1},
    case ?IR_RULE:find_route(Ctx1) of
        {ok,{ok,{inside,_}},_}=Res -> {re,Res};
        {ok,{ok,{insidepbx,_}},_}=Res -> {re,Res};
        {ok,_,Ctx2} -> ?IR_UTILS:reply(Ctx2, ?Forbidden("B2B. Barge number restricted"));
        {error,_,Ctx2} -> ?IR_UTILS:reply(Ctx2, ?Forbidden("B2B. Routing error"));
        {reply,SipReply,Ctx2} -> ?IR_UTILS:reply(Ctx2, SipReply)
    end.

%% ----------------------------------------
%% feature replace (#163)
%% ----------------------------------------

do_invite_replace(call, ExtAOR, Extns, Ctx) ->
    case find_dlg(ExtAOR, Extns, Ctx) of
        {ok,Map,Ctx1} ->
            CallId = maps:get(callid,Map,un),
            Tag = maps:get(tag,Map,un),
            do_invite_replace_dlg({CallId,Tag}, Map, Ctx1);
        {{reply,SipReply}, Ctx1} ->
            ?IR_UTILS:reply(Ctx1, SipReply);
        {{error,_}=Err, Ctx1} ->
            err(Err, Ctx1)
    end.

%% @private
do_invite_replace_dlg({CallId,_}, Map, Ctx) when CallId/=un ->
    Opp = lists:member(<<"opposite">>, maps:get(feature_extension_opts,Ctx)),
    Side = case maps:get(acallid,Map) of
               CallId when not Opp -> a;
               CallId when Opp -> b;
               _ when Opp -> a;
               _ -> b
           end,
    do_invite_replace_dlg_1(Side, Map, Ctx);

%% @private
do_invite_replace_dlg({_,LTag}, Map, Ctx) when LTag/=un ->
    Opp = lists:member(<<"opposite">>, maps:get(feature_extension_opts,Ctx)),
    Side = case maps:get(altag,Map) of
               LTag when not Opp -> a;
               LTag when Opp -> b;
               _ when Opp -> a;
               _ -> b
           end,
    do_invite_replace_dlg_1(Side, Map, Ctx).

%% @private
do_invite_replace_dlg_1(Side, Map, Ctx) ->
    % replaces
        [Uri,CallId,FTag,TTag] = case Side of
                                     a -> ?EU:maps_get([aruri,acallid,artag,altag],Map);
                                     b -> ?EU:maps_get([bruri,bcallid,bltag,brtag],Map)
                                 end,
        {_,_,D}=AOR = ?U:make_aor(Uri),
        ReplacesValue = ?U:build_replaces_header(#{callid=>CallId,fromtag=>FTag,totag=>TTag}),
    % referred-by
        RefByUri = case maps:get(from_sipuser,Ctx,false) of
                       false -> #uri{scheme=sip,user= <<"system">>,domain=D}; % @todo: deny / user&domain
                       SipUser when is_list(SipUser) ->
                           [SU,SD] = ?EU:lists_get([login,domain], SipUser),
                           #uri{scheme=sip,user=SU,domain=SD}
                   end,
        % TODO RefByOpts set
        SrvIdx = ?U:get_current_srv_textcode(),
        Random = ?U:build_textcode_by_index(?EU:random(3521615)*1000000 + ?EU:random(606208), 7),
        EO = [{<<"r-dlgid">>,maps:get(dlgid,Map)},
              {<<"r-acallid">>,?EU:urlencode(maps:get(acallid,Map))},
              {<<"r-iid">>,maps:get(invite_id,Map)},
              {<<"r-side">>,?EU:to_binary(Side)}],
        ?ENVSTORE:store_t({referred_by_info,Random},EO,180000),
        RefByValue = ?U:unparse_uri(RefByUri#uri{ext_opts=[], opts=[{<<"r-key">>,<<SrvIdx/binary,"-",Random/binary>>}]}),

    % virtual replaces invite request
        RUri = ?U:make_uri(AOR),
        #sipmsg{to={_,XTag},headers=H0}=Req = maps:get(req,Ctx),
        H1 = lists:keystore(?ReplacesLow,1,H0,{?ReplacesLow,ReplacesValue}),
        H2 = lists:keystore(?ReferredByLow,1,H1,{?ReferredByLow,RefByValue}),
        Req1 = Req#sipmsg{ruri=RUri,to={RUri,XTag},headers=H2},
        Ctx1 = Ctx#{req => Req1, to_uri => RUri, isreferred => true, by_uri => RefByUri},
    % route directly as incoming is replaces invite request
        ?IR_REFERRED:route_replaces(Ctx1).

%% ----------------------------------------
%% feature prompt
%% ----------------------------------------

do_invite_prompt(replace, {_,User,_}=AOR, RepMap, Ctx) when is_map(RepMap) ->
    TTag = maps:get(totag,RepMap),
    case ?IVR_UTILS:extract_prompt_server_index(TTag) of
        false -> ?IR_UTILS:reply(Ctx, ?ServiceUnavailable("B2B. Prompt tag not found to replace"));
        {true, SrvIdx} -> ?IR_FEATURE_SVC:do_invite_service(SrvIdx, AOR, Ctx, ["PROMPT", User, true, true, []])
    end;

do_invite_prompt(Mode, ExtAOR, Extns, Ctx)
  when Mode==mesh; Mode==prompt; Mode==monitor ->
    case find_dlg(ExtAOR, Extns, Ctx) of
        {ok,Map,Ctx1} ->
            CallId = maps:get(callid,Map,un),
            Tag = maps:get(tag,Map,un),
            do_invite_prompt_dlg(Mode, {CallId,Tag}, Map, ExtAOR, Ctx1);
        {{reply,SipReply}, Ctx1} ->
            ?IR_UTILS:reply(Ctx1, SipReply);
        {{error,_}=Err, Ctx1} ->
            err(Err, Ctx1)
    end.

%% @private
do_invite_prompt_dlg(Mode, {CallId,_}, Map, ExtAOR, Ctx) when CallId/=un ->
    Opp = lists:member(<<"opposite">>, maps:get(feature_extension_opts,Ctx)),
    Side = case maps:get(acallid,Map) of
               CallId when not Opp -> a;
               CallId when Opp -> b;
               _ when Opp -> a;
               _ -> b
           end,
    do_invite_prompt_dlg_1(Mode, Map#{side => Side}, ExtAOR, Ctx);

%% @private
do_invite_prompt_dlg(Mode, {_,LTag}, Map, ExtAOR, Ctx) when LTag/=un ->
    Opp = lists:member(<<"opposite">>, maps:get(feature_extension_opts,Ctx)),
    Side = case maps:get(altag,Map) of
               LTag when not Opp -> a;
               LTag when Opp -> b;
               _ when Opp -> a;
               _ -> b
           end,
    do_invite_prompt_dlg_1(Mode, Map#{side => Side}, ExtAOR, Ctx).

%% @private
do_invite_prompt_dlg_1(Mode, Map, ExtAOR, Ctx) ->
    DlgId = maps:get(dlgid,Map),
    Side = maps:get(side,Map),
    ?IR_FEATURE_SVC:do_invite_prompt(Mode, DlgId, Side, ExtAOR, Ctx).

%% =========================================================
%% Internal functions
%% FIND DLG
%% =========================================================
find_dlg({_,FNum,_}, Extns, Ctx) ->
    Opts = binary:split(Extns, [<<";">>,<<",">>,<<" ">>], [global,trim_all]),
    Ctx1 = Ctx#{feature_extension_opts=>Opts},
    case lists:member(<<"callid">>,Opts) of
        false -> find_dlg_by_num(FNum, Ctx1);
        true -> find_dlg_by_callid(FNum, lists:member(<<"base64">>,Opts), Ctx1)
    end.

%% by num ----------------
find_dlg_by_num(FNum,Ctx) ->
    ToUri = maps:get(to_uri,Ctx),
    ToUri1 = ToUri#uri{user=FNum},
    % find
    [TD, Dir, ByAOR, _, ExtAC, RouteCode] = maps:get(findopts, Ctx),
    FindOpts = [TD, Dir, ByAOR, ?U:make_aor(ToUri1), ExtAC, RouteCode],
    %
    Ctx1 = Ctx#{restrict_redirect => true, % forbidden if redirect
                skip_redirect => true, % do not use redirect, apply previous user
                findopts := FindOpts,
                to_uri := ToUri1},
    case ?IR_RULE:find_route(Ctx1) of
        {ok,{ok,{inside,{rules,[AOR|_]}}},Ctx2} -> find_dlg_by_num_1(AOR,Ctx2);
        {ok,{ok,{insidepbx,{rules,[AOR|_]}}},Ctx2} -> find_dlg_by_num_1(AOR,Ctx2);
        {ok,_,Ctx2} -> {{error,number_restricted},Ctx2};
        {error,_,Ctx2} -> {{error,routing_error},Ctx2};
        {reply,SipReply,Ctx2} -> {{reply,SipReply},Ctx2}
    end.

find_dlg_by_num_1(AOR,Ctx) ->
    case ?USRSTATE:find_busy_dialogs(AOR) of
        [_|_]=Rest -> find_dlg_by_num_2(Ctx#{found_dialogs=>Rest});
        _ -> {{error, store_dialog_not_found},Ctx}
    end.

find_dlg_by_num_2(Ctx) ->
    case maps:get(found_dialogs, Ctx) of
        [] -> {{error,forking_state},Ctx};
        [{_SrvIdx,_DlgId,_Tag}=X|Rest] -> find_dlg_by_num_3(X, Ctx#{found_dialogs:=Rest})
    end.

find_dlg_by_num_3({SrvIdx,DlgId,Tag}, Ctx) ->
    case ?CFG:get_sipserver_by_index(SrvIdx) of
        undefined -> ?IR_UTILS:reply(Ctx, ?InternalError("B2B. Undefined srvidx"));
        {_Site, _Node}=Dest ->
            Request = {?B2BUA_DLG, get_current_info, [DlgId]},
            case ?ENVCROSS:call_node(Dest, Request, undefined, 2000) of
                undefined -> {{error,server_unavailable}, Ctx};
                false -> {{error,server_dialog_not_found}, Ctx};
                {ok,#{}=Map} ->
                    case maps:get(state,Map) of
                        forking -> find_dlg_by_num_2(Ctx);
                        dialog -> {ok, Map#{dlgid=>DlgId,tag=>Tag}, Ctx}
                    end end end.

%% by callid ----------------
find_dlg_by_callid(<<>>, _, Ctx) ->
    % if FNum is empty, then use header X-Era-Replaces-Call-Id
    R = lists:foldl(fun(_,{ok,[_|_]}=Acc) -> Acc; (H,_) -> nksip_request:header(H, maps:get(req,Ctx)) end,
                    false, [?DestCallIdLow, ?ReplacesCallIdLow]),
    case R of
        {ok,[CallId|_]} -> find_dlg_by_callid(CallId, Ctx);
        _ -> {{error, no_callid}, Ctx}
    end;
find_dlg_by_callid(CallId, true, Ctx) ->
    % if ext of featurecode contains base64 option, then decode FNum.
    case catch base64:decode(CallId) of
        {'EXIT',_} -> {{error,bad_base64}, Ctx};
        CallId1 -> find_dlg_by_callid(CallId1, Ctx)
    end;
find_dlg_by_callid(CallId, false, Ctx) ->
    find_dlg_by_callid(CallId, Ctx).

%% @private
find_dlg_by_callid(CallId, Ctx) ->
    case lists:member(<<"allsites">>, maps:get(feature_extension_opts,Ctx)) of
        false ->
            B2Bs = ?ENVCFG:get_nodes_containing_role_site('b2bua'),
            find_dlg_by_callid_1([B2Bs], CallId, Ctx);
        true ->
            B2Bs = ?ENVCFG:get_nodes_containing_role_dyncfg_check_free('b2bua'), % dyncfg :(
            %B2Bs = ?ENVCFG:get_nodes_containing_role_sites('b2bua',?ENVCFG:get_all_sites()),
            find_dlg_by_callid_1([B2Bs], CallId, Ctx)
    end.
%%
find_dlg_by_callid_1([], _, Ctx) -> {{error, dlg_not_found_by_callid}, Ctx};
find_dlg_by_callid_1([B2Bs|Rest], CallId, Ctx) when is_list(B2Bs) ->
    MFA = {?B2BUA_DLG, pull_fsm_by_callid, [CallId]},
    X = ?ENVMULTICALL:call_rpco(lists:map(fun({DS,DN,_,_}) -> {DS,DN} end, B2Bs), MFA, 1000),
    Y = lists:filter(fun({_Dest,{_DlgId,_Pid}}) -> true; (_) -> false end, X),
    case Y of
        [{{DS,DN},{DlgId,_}}] -> find_dlg_by_callid_2({DS,DN,DlgId,CallId}, Ctx#{replace_dialogs=>[DlgId]});
        [] -> find_dlg_by_callid_1(Rest, CallId, Ctx)
    end.
%%
find_dlg_by_callid_2({DS,DN,DlgId,CallId}, Ctx) ->
    Dest = {DS,DN},
    Request = {?B2BUA_DLG, get_current_info, [DlgId]},
    case ?ENVCROSS:call_node(Dest, Request, undefined) of
        undefined -> {{error,server_unavailable}, Ctx};
        false -> {{error,server_dialog_not_found}, Ctx};
        {ok,#{}=Map} ->
            case maps:get(state,Map) of
                forking -> {{error,forking_state}, Ctx};
                dialog -> {ok, Map#{dlgid=>DlgId, callid=>CallId}, Ctx}
            end end.

%% -------------------------------
%% @private
err({error,number_restricted}, Ctx) -> ?IR_UTILS:reply(Ctx, ?Forbidden("B2B. Destination number restricted for operation"));
err({error,routing_error}, Ctx) -> ?IR_UTILS:reply(Ctx, ?Forbidden("B2B. Routing error"));
err({error,store_dialog_not_found}, Ctx) -> ?IR_UTILS:reply(Ctx, ?NotFound("B2B. Store dialog not found by AOR"));
err({error,forking_state}, Ctx) ->  ?IR_UTILS:reply(Ctx, ?InternalError("B2B. Dlg server dialog not found (forking state)"));
err({error,server_unavailable}, Ctx) -> ?IR_UTILS:reply(Ctx, ?InternalError("B2B. Dlg server unavailable"));
err({error,server_dialog_not_found}, Ctx) -> ?IR_UTILS:reply(Ctx, ?NotFound("B2B. Dlg server dialog not found"));
err({error,no_callid}, Ctx) -> ?IR_UTILS:reply(Ctx, ?BadRequest("B2B. Destination CallId is not found neither in username nor in header"));
err({error,bad_base64}, Ctx) -> ?IR_UTILS:reply(Ctx, ?BadRequest("B2B. Invalid CallId base64 value"));
err({error,dlg_not_found_by_callid}, Ctx) -> ?IR_UTILS:reply(Ctx, ?NotFound("B2B. Dialog not found by callid"));
err(_, Ctx) -> ?IR_UTILS:reply(Ctx, ?InternalError("B2B. Unknown (x)")).