%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 2.05.2019
%%% @doc Featurecode services, ex.IVR

-module(r_sip_b2bua_router_invite_feature_service).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([do_invite_ivr/3,
         do_invite_hunt/3,
         do_invite_prompt/5,
         %
         do_invite_service/4,
         select_responsible/3
        ]).

%%% ====================================================================
%%% Define
%%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_headers.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_b2b_invite_router.hrl").

%%% ====================================================================
%%% API functions
%%% ====================================================================

%% -------------------------
do_invite_ivr(IvrCode, {_,Num,Domain}=AOR, Ctx) ->
    Site = ?CFG:get_domain_site(Domain),
    case ?CFG:get_site_ivr(Site) of
        [] -> ?IR_UTILS:reply(Ctx, ?ServiceUnavailable("B2B. IVR inaccessible (a)"));
        L when is_list(L) ->
            case ?IR_FEATURE_SVC:select_responsible(random, Site, lists:map(fun({N,_}) -> N end, L)) of
                empty -> ?IR_UTILS:reply(Ctx, ?ServiceUnavailable("B2B. IVR inaccessible (b)"));
                {ok,Node} ->
                    SrvIdx = ?CFG:get_node_index(Node),
                    #sipmsg{to={#uri{user=ToU,domain=ToD},_},from={#uri{user=FromU,domain=FromD},_}} = maps:get(req,Ctx),
                    #uri{user=ByU,domain=ByD} = maps:get(by_uri,Ctx),
                    AddHeaders = [{?IvrHeader, ?EU:join_binary([?EU:join_binary([<<"code">>,?URLENCODE:encode(IvrCode)],"="),
                                                                ?EU:join_binary([<<"called">>,?URLENCODE:encode((maps:get(to_uri,Ctx))#uri.user)],"="),
                                                                ?EU:join_binary([<<"ext">>,?URLENCODE:encode(Num)],"="),
                                                                ?EU:join_binary([<<"to">>,?URLENCODE:encode(?EU:strbin("~s@~s",[ToU,ToD]))],"="),
                                                                ?EU:join_binary([<<"from">>,?URLENCODE:encode(?EU:strbin("~s@~s",[FromU,FromD]))],"="),
                                                                ?EU:join_binary([<<"by">>,?URLENCODE:encode(?EU:strbin("~s@~s",[ByU,ByD]))],"=")
                                                               ], ";")}],
                    ?IR_FEATURE_SVC:do_invite_service(SrvIdx, AOR, Ctx, ["IVR", ?U:make_ivr_user(Num), true, ?U:is_record_ivr(), AddHeaders])
            end end.

%% -------------------------
do_invite_hunt({_SearchKey,Id,NumH}, {_,Num,Domain}=AOR, Ctx) ->
    Site = ?CFG:get_domain_site(Domain),
    case ?CFG:get_site_ivr(Site) of
        [] -> ?IR_UTILS:reply(Ctx, ?ServiceUnavailable("B2B. IVR inaccessible (c)"));
        L when is_list(L) ->
            [{Node,_Addr}|_] = ?EU:randomize(L), % @TODO
            SrvIdx = ?CFG:get_node_index(Node),
            #sipmsg{to={#uri{user=ToU,domain=ToD},_},from={#uri{user=FromU,domain=FromD},_}} = maps:get(req,Ctx),
            #uri{user=ByU,domain=ByD} = maps:get(by_uri,Ctx),
            AddHeaders = [{?HuntHeader, ?EU:join_binary([?EU:join_binary([<<"huntid">>,?URLENCODE:encode(Id)],"="),
                                                         ?EU:join_binary([<<"called">>,?URLENCODE:encode((maps:get(to_uri,Ctx))#uri.user)],"="),
                                                         ?EU:join_binary([<<"ext">>,?URLENCODE:encode(Num)],"="),
                                                         ?EU:join_binary([<<"to">>,?URLENCODE:encode(?EU:strbin("~s@~s",[ToU,ToD]))],"="),
                                                         ?EU:join_binary([<<"from">>,?URLENCODE:encode(?EU:strbin("~s@~s",[FromU,FromD]))],"="),
                                                         ?EU:join_binary([<<"by">>,?URLENCODE:encode(?EU:strbin("~s@~s",[ByU,ByD]))],"=")
                                                        ], ";")}],
            ?IR_FEATURE_SVC:do_invite_service(SrvIdx, AOR, Ctx, ["HUNT", ?U:make_hunt_user(NumH), true, ?U:is_record_ivr(), AddHeaders])
    end.

%% -------------------------
do_invite_prompt(Mode, DlgId, Side, {_,Num,Domain}=AOR, Ctx) ->
    Site = ?CFG:get_domain_site(Domain),
    case ?CFG:get_site_prompt(Site) of
        [] -> ?IR_UTILS:reply(Ctx, ?ServiceUnavailable("B2B. PROMPT inaccessible (a)"));
        L when is_list(L) ->
            case ?IR_FEATURE_SVC:select_responsible(random, Site, lists:map(fun({N,_}) -> N end, L)) of
                empty -> ?IR_UTILS:reply(Ctx, ?ServiceUnavailable("B2B. PROMPT inaccessible (b)"));
                {ok,Node} ->
                    SrvIdx = ?CFG:get_node_index(Node),
                    AddHeaders = [{?PromptHeader, ?EU:join_binary([?EU:join_binary([<<"mode">>,?EU:to_binary(Mode)],"="),
                                                                   ?EU:join_binary([<<"dlgid">>,?URLENCODE:encode(DlgId)],"="),
                                                                   ?EU:join_binary([<<"side">>,?EU:to_binary(Side)],"=")
                                                                  ], ";")}],
                    ?IR_FEATURE_SVC:do_invite_service(SrvIdx, AOR, Ctx, ["PROMPT", ?U:make_prompt_user(Num), true, ?U:is_record_prompt(), AddHeaders])
            end end.

%%% ====================================================================
%%% Internal functions
%%% ====================================================================

%% ----------------------------------------
%% @private
%% invite of internal sip services (conf, ivr, etc..)
%% ----------------------------------------
do_invite_service(SrvIdx, {_,_,Dom}=ToAOR, Ctx, [ServiceName, ToUser, UseMedia, UseRecord, AddHeaders]) ->
    Req = maps:get(req, Ctx),
    #sipmsg{call_id=CallId,
            ruri=_RUri}=Req,
    case ?SERVERS:find_responsible_sip_srvidx(SrvIdx, CallId) of
        false ->
            ?IR_UTILS:reply(Ctx, ?ServiceUnavailable("B2B. " ++ ServiceName ++ " server not found"));
        RouteDomain ->
            ?LOGSIP("INVITE_ROUTER CALL TO ~s: ~100p -> ~p (~100p)", [ServiceName, CallId, SrvIdx, RouteDomain]),
            % @route
            RouteUri = #uri{scheme=sip, domain=RouteDomain, opts=?CFG:get_transport_opts()},
            % @to
            ToUri = #uri{scheme=sip,
                         user=ToUser,
                         domain=Dom},
            % @from, @contact
            BLUri = ?IR_UTILS:build_from_uri({sip, ToUser, Dom}, Ctx),
            BLContact = #uri{scheme=sip, disp= <<>>, user= BLUri#uri.user},
            % rules
            UriRules = [#{% aor - automatic
                           % sipuser - undefined
                           uri => ToUri#uri{headers=nksip_headers:update(AddHeaders, [{multi, <<"route">>, RouteUri}])},
                           routectx => ?IR_UTILS:make_route_ctx(Ctx),
                           bl_uri => BLUri,
                           bl_contact => BLContact,
                           callednum => ToAOR, % real routed aor(number) by call
                           dir => 'inside',
                           redircond => [], % no redirect post conditions
                           ftimeout => 3605000, % @forktimeout timeout should be redundant (1 hr). call to service should be answered immediately, but ivr can put to queue in 182/183. Limited by resp_timer auto enlarged on 182/183.
                           is_feature => true, % RP-2078. Optional key for define callee address for find mg alias.
                           feature_rdomain => RouteDomain % Mg alias. For find callee address.
                         }],
            Opts = [{urirules,UriRules},
                    {ctx,Ctx}, % RP-640
                    {from_sipuser,maps:get(from_sipuser,Ctx)},
                    {req_from_uri,maps:get(req_from_uri,Ctx)},
                    {b2bhdr,maps:get(b2bhdr,Ctx)},
                    {link_referred,maps:get(link_referred,Ctx,undefined)}, % #354
                    {link_replaces,maps:get(link_replaces,Ctx,undefined)}, % #354
                    {use_media,?IR_UTILS:get_usemedia(Ctx,UseMedia)},
                    {use_record,UseRecord},
                    {addRemoteParty1xx,true},
                    {addRemoteParty2xx,true}],
            {ok, Opts, Ctx}
    end.

% ------
% @private
select_responsible(random, Site, L) ->
    F = fun(Node,Acc) ->
                case ?ENVCROSS:call_node({Site,Node}, {?STAT,test,[]}, undefined, 1000) of
                    ok -> throw({ok,Node});
                    undefined -> Acc
                end end,
    try lists:foldl(F, empty, ?EU:randomize(L))
    catch throw:R -> R
    end.
