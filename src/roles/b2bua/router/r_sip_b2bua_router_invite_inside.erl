%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.2015 refactored 31.12.2016.
%%% @doc Invite routing inside to internal user or group of users (aors)

-module(r_sip_b2bua_router_invite_inside).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([do_invite_inside_direct/2,
         do_invite_inside/3,
         make_urirules/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_headers.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_b2b_invite_router.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ====================================================================
%% Invite to inside
%% ====================================================================

%% ------------------------------
%% initiates referred+replaces call to user
%% ------------------------------
do_invite_inside_direct(CalledAOR, Ctx) ->
    case ?CALLEE:get_callee(map, CalledAOR) of
        [_|_]=UriRules -> ?IR_URIRULES:do_invite_urirules(CalledAOR,make_urirules(CalledAOR,UriRules,Ctx),Ctx);
        {reply, SipReply} -> ?IR_UTILS:reply(Ctx,SipReply)
    end.

%% ------------------------------
%% initiates call to user
%% ------------------------------
do_invite_inside(CalledAOR,UriRules,Ctx) ->
    UriRules1 = make_urirules(CalledAOR,UriRules,Ctx),
    ?IR_URIRULES:do_invite_urirules(CalledAOR,UriRules1,Ctx).

%% ----------------------------------
%% for tracing and grouping
%% ----------------------------------
make_urirules(CalledAOR,UriRules,Ctx) ->
    F = fun Up(#{}=UR,Acc1) ->
                 % @from @contact @callerid
                 BLUri = ?IR_UTILS:build_from_uri(maps:get(aor,UR), Ctx),
                 BLContact = #uri{scheme=sip, disp= <<>>, user= BLUri#uri.user},
                 % rule
                 [UR#{% aor - to (sipuser)
                      % uri - ruri
                      % ftimeout - % @forktimeout time limit ms till final answer, set up in urirule by domaincenter's numberplan search response
                      % sipuser - account info: login,domain,phone,name,ext
                      routectx => ?IR_UTILS:make_route_ctx(Ctx),
                      bl_uri => BLUri,
                      bl_contact => BLContact,
                      callednum => CalledAOR % real routed aor(number) by call
                      % fnum => maps:get(fnum,Ctx,undefined), % fnum (remote pbx extension) % @fnum
                      % dir
                      % redircond
                     } | Acc1]; % dir of call
            Up(L,Acc1) when is_list(L) -> [lists:foldr(Up, [], L) | Acc1] end,
    lists:foldr(F, [], UriRules).
