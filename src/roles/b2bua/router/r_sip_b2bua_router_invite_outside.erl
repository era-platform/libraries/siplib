%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 12.2015 refactored 31.12.2016
%%% @doc Invite routing outside to external account

-module(r_sip_b2bua_router_invite_outside).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([do_invite_outside/3,
         do_invite_outside_direct/2,
         make_urirules/2,
         make_urirules/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_headers.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_b2b_invite_router.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------------
%% @direct
%% used on replacing invite
%%
do_invite_outside_direct([AMap,SrvIdx],Ctx) ->
    Code = maps:get(code,AMap),
    do_invite_outside(SrvIdx,AMap,Ctx#{account => Code}).


%% ------------------------------
%% initiates external call. prepare route step
%%
do_invite_outside(SrvIdx, AMap, Ctx) ->
    Rule = maps:get(rule,Ctx,undefined),
    RouteUri = case ?B2BUA_UTILS:get_esg_route_uri(SrvIdx) of
                   undefined -> ?IR_UTILS:reply(Ctx, ?Forbidden("B2B. Route inaccessible"));
                   #uri{}=Uri -> Uri
               end,
    try do_invite_outside_1({AMap,RouteUri,Rule}, Ctx)
    catch error:_R -> ?IR_UTILS:reply(Ctx, ?InternalError("B2B. Error routing outside"))
    end.

%% ----
%% initiates external call. start dialog step
%%
do_invite_outside_1({AMap,RouteUri,_Rule}, Ctx) ->
    UriRules = make_urirules(AMap,RouteUri,Ctx),
    ?IR_URIRULES:do_invite_urirules(UriRules,Ctx).

%% ----------------------------------
%% for tracing and grouping
%% ----------------------------------
make_urirules({_ToU,_RouteCode,SrvIdx,_Code,Account},Ctx) ->
    case ?B2BUA_UTILS:get_esg_route_uri(SrvIdx) of
        undefined -> undefined;
        #uri{}=RouteUri -> make_urirules(Account,RouteUri,Ctx)
    end.

make_urirules(AMap,RouteUri,Ctx) ->
    AccId = maps:get(id,AMap),
    AccUN = maps:get(username,AMap),
    AccDom = maps:get(domain,AMap),
    % routecode for routing without service-oriented modifying of To and From
    RouteCode = maps:get(routecode,Ctx,<<>>),
    % @to
    ToUri = maps:get(to_uri, Ctx),
    ToUri1 = ToUri#uri{%domain=AccDom, % it should be cluster domain, to make representative while replacing
                       headers=nksip_headers:update([], [{multi, <<"route">>, RouteUri},
                                                         {multi, ?ExtAccountHeader, <<AccUN/binary,"@",AccDom/binary,";id=",AccId/binary,";routecode=",RouteCode/binary>>}])},
    % @from, @contact, @callerid
    BLUri = ?IR_UTILS:build_from_uri(?U:make_aor(ToUri), Ctx), % ! cluster domain in to
    BLUri1 = BLUri#uri{domain=maps:get(clusterdomain,AMap)} ,
    BLContact = #uri{scheme=sip, disp= <<>>, user= BLUri1#uri.user},
    % rules
    [#{% aor - automatic
       aor => {sip,ToUri1#uri.user,ToUri1#uri.domain},
       % sipuser - undefined
       uri => ToUri1,
       routectx => ?IR_UTILS:make_route_ctx(Ctx),
       bl_uri => BLUri1,
       bl_contact => BLContact,
       extaccount => AMap,
       callednum => ?U:make_aor(ToUri),
       dir => 'outside',
       ftimeout => 3605000 % @forktimeout timeout should be redundant (1 hr). call to outsite could recv 182/183 and wait in external queue. Limited by resp_timer auto enlarged on 182/183
      }].