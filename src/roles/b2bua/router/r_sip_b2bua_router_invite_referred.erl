%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 9.04.2017.
%%% @doc Route by referred/replaced. Modify from_uri, by_uri according to referring call
%%%        This module was distinguished from r_sip_b2bua_router_invite after implementing of #163.

-module(r_sip_b2bua_router_invite_referred).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    get_referred_by_opts/1,
    check_referred/1,
    route_replaces/1
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_headers.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_b2b_invite_router.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------------------------
%% Return referred-by opts by Header value.
%% Target opts could be stored on other node.
%% Referred-By Uri could contain opts directly or key to get from store.
%% ------------------------------------------
get_referred_by_opts(RefBy) when is_binary(RefBy) ->
    [#uri{}=Uri|_] = ?U:parse_uris(RefBy),
    get_referred_by_opts(Uri);
get_referred_by_opts(#uri{opts=[],ext_opts=[]}) -> [];
get_referred_by_opts(#uri{opts=RefByOpts,ext_opts=ExRefByOpts}) ->
    case ?EU:get_by_key(<<"r-key">>,RefByOpts,undefined) of
        undefined -> RefByOpts ++ ExRefByOpts; % may be opts are set directly ("r-leg=done", "r-acallid", "r-dlgid", etc.)
        Key ->
            CurNode = node(),
            [SrvIdxCode,DlgNum] = binary:split(Key,<<"-">>),
            SrvIdx = ?EU:parse_textcode_to_index(SrvIdxCode),
            case ?ENVCFG:get_node_by_index(SrvIdx) of
                {_Site,CurNode} ->
                    case ?ENVSTORE:find_t({referred_by_info,DlgNum}) of
                        {_,StoredOpts} -> StoredOpts;
                        false -> []
                    end;
                {_Site,_Node}=Dest ->
                    case ?ENVCROSS:call_node(Dest,{?ENVSTORE,find_t,[{referred_by_info,DlgNum}]},false,5000) of
                        {_,StoredOpts} -> StoredOpts;
                        false -> []
                    end
            end
    end.

%% ------------------------------------------
%% checks if incoming invite request is referred and should be routed directly
%% ------------------------------------------
-spec check_referred(Ctx::map()) ->
          {false,Ctx1} | {ok,referred,Ctx1} | {ok,replaces,Ctx1}.
%% ------------------------------------------
check_referred(Ctx) ->
    Req = maps:get(req, Ctx),
    Fcmn = fun(RefBy) ->
               % referred-by
               [RefByUri|_] = ?U:parse_uris(RefBy),
               RefByOpts = maps:get(referredby_opts,Ctx,[]),
               RefMap = #{acallid => ?EU:urldecode(?EU:get_by_key(<<"r-acallid">>,RefByOpts,undefined)), % #354
                          side => ?EU:get_by_key(<<"r-side">>,RefByOpts,undefined),
                          iid => ?EU:get_by_key(<<"r-iid">>,RefByOpts,undefined),
                          its => ?EU:get_by_key(<<"r-its">>,RefByOpts,undefined),
                          dlgid => ?EU:get_by_key(<<"r-dlgid">>,RefByOpts,undefined)},
               Ctx1 = ?B2BUA_BINDS:dialog_inherit_bindings(maps:get(dlgid,RefMap), Ctx),
               Ctx2 = Ctx1#{isreferred => true,
                            by_uri => ?U:clear_uri(RefByUri),
                            link_referred => maps:get(acallid,RefMap), % #354
                            link_iid => maps:get(iid,RefMap),
                            link_its => ?EU:to_int(maps:get(its,RefMap),0),
                            link_dlgid => maps:get(dlgid,RefMap),
                            link_side => maps:get(side,RefMap)},
               {ok,RefMap,Ctx2}
           end,
    case do_check_referred_request(Req) of
        {true,true,[RefBy,Repl]} ->
            % replaces
            RProps = parse_replaces(Repl),
            RepCallId = maps:get(callid,RProps),
            {ok,RefMap,Ctx1} = Fcmn(RefBy),
            Ctx2 = update_ctx_referred(true, Ctx1#{link_replaces => RepCallId}),
            ?IR_EVENT:event('route_referred',[Ctx2]),
            link_refer_dialog(Ctx2,RefMap),
            {ok,replaces,Ctx2};
        {true,false,[RefBy]} ->
            % referred
            {ok,RefMap,Ctx1} = Fcmn(RefBy),
            Ctx2 = update_ctx_referred(false, Ctx1),
            ?IR_EVENT:event('route_referred',[Ctx2]),
            link_refer_dialog(Ctx2,RefMap),
            {ok,referred,Ctx2};
         _ ->
            {false,Ctx}
    end.

%% ---------------

% applies referred-replaces invite request
-spec route_replaces(Ctx::map()) -> {reply,SipReply::tuple()} | {ok,Opts::list()}.

route_replaces(Ctx) ->
    Req = maps:get(req, Ctx),
    {true,true,[_RefBy,Repl]} = do_check_referred_request(Req),
    RefByOpts = maps:get(referredby_opts,Ctx,[]),
    RProps = parse_replaces(Repl),
    % TODO RefByOpts get
    case lists:keyfind(<<"r-leg">>,1,RefByOpts) of
        {_,_} -> route_replaces(RProps, Ctx); % @usualway
        false ->
            % #163
            case change_leg(RProps, Ctx) of
                {ok,RProps1,Ctx1} -> route_replaces(RProps1, Ctx1);
                Reply -> Reply
            end end.

% @private
route_replaces(RProps, Ctx) when is_map(RProps) ->
    CallId = maps:get(callid,RProps),
    ToAOR = ?U:make_aor(maps:get(to_uri,Ctx)),
    Req = maps:get(req, Ctx),
    % link replaced dlg
    OwnerDlg = ?REPLACES:build_owner({ctx,Ctx}),
    ?REPLACES:link_invite_to_replaced(RProps, OwnerDlg),
    case ?REPLACES:find_replaced_dlg(RProps, Req#sipmsg.call_id) of
        {ok,{{_,_}=_RepDlgIdData,{RepACallId,_}=_RepACallIdData,{_,_}=_RepInviteData}=RepDlg} ->
            ?STAT_FACADE:link_replaced(Req#sipmsg.call_id, RepACallId), % @stattrace
            ?IR_EVENT:event('replacing',[Ctx,ToAOR,{CallId,RepDlg}]);
        _ -> ok
    end,
    % route direct
    case do_check_route_direct_0(ToAOR,Req,RProps,Ctx) of
        {ok,conf,Ctx1} -> ?IR_FEATURE:do_invite_conference(ToAOR, Ctx1);
        {ok,ivr,RProps,Ctx1} -> ?IR_FEATURE:do_invite_ivr(replace, ToAOR, RProps, Ctx1);
        {ok,hunt,RProps,Ctx1} -> ?IR_FEATURE:do_invite_hunt(replace, ToAOR, RProps, Ctx1);
        {ok,prompt,RProps,Ctx1} -> ?IR_FEATURE:do_invite_prompt(replace, ToAOR, RProps, Ctx1);
        {ok,user,Ctx1} -> ?IR_INSIDE:do_invite_inside_direct(ToAOR, Ctx1);
        {ok,user,RealAOR,Ctx1} -> ?IR_INSIDE:do_invite_inside_direct(RealAOR, Ctx1);
        {ok,ext,Ctx1,P} -> ?IR_OUTSIDE:do_invite_outside_direct(P,Ctx1);
        {false,Ctx1} -> ?IR_UTILS:reply(Ctx1, ?Forbidden("B2B. Routing replaces ruri error"))
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ----------------------------
%% checks if call should be directly routed
%% @private check request is referred (to direct user/conf/ivr/ext...)
-spec do_check_referred_request(Req::#sipmsg{}) -> {HasReferred::boolean(),HasReplaces::boolean(),H::list()} | error .
%% ----------------------------
do_check_referred_request(Req) ->
    case {nksip_request:header("referred-by", Req), nksip_request:header("replaces", Req)} of
        {{ok,[]},{ok,[]}} -> {false, false, []};
        {{ok,[H1|_]},{ok,[]}} -> {true, false, [H1]};
        {{ok,[]},{ok,_}} -> error;
        {{ok,[H1|_]},{ok,[H2|_]}} -> {true, true, [H1,H2]};
        _ -> error
    end.

%% ----------------------------
%% Links to A->B dialog where REFER was accepted
%% ----------------------------
link_refer_dialog(Ctx,RefMap) ->
    case maps:get(dlgid,RefMap) of
        undefined -> ok;
        _ ->
            OwnerDlg = ?REPLACES:build_owner({ctx,Ctx}),
            RefMap1 = RefMap#{callid => maps:get(acallid,RefMap)},
            ?REPLACES:link_invite_to_referred(RefMap1, OwnerDlg)
    end.

%% -----------------------------------
%% Change call/leg to opposite side
%% -----------------------------------

%% --------------
%% @private
%% #163 if incoming invite is replaces, but does not have r-dlgid opts in referred-by.
%%      then it should be changed to opposite (it was not changed by b2bua in refer request)
%% --------------
-spec change_leg(RProps::#{callid=>binary(),fromtag=>binary(),totag=>binary(),opts=>[{K::binary(),V::binary()}]}, Ctx::map())
      -> {ok, RProps1::#{callid=>binary(),fromtag=>binary(),totag=>binary(),opts=>[{K::binary(),V::binary()}]}, Ctx1::map()}
       | {reply,SipReply::tuple()}.
%% --------------
change_leg(RProps, Ctx) when is_map(RProps) ->
    [CallId,FTag,TTag] = ?EU:maps_get([callid,fromtag,totag],RProps),
    case ?B2BUA_UTILS:extract_b2bua_server_index(CallId,FTag,TTag) of
        false -> ?IR_UTILS:reply(Ctx, ?Forbidden("B2B. Routing error, invalid callid/from-tag/to-tag"));
        {true, SrvIdx} ->
            case get_dialog_info(SrvIdx, CallId, Ctx) of
                #{}=Map -> change_leg_1(RProps, Map, Ctx);
                Reply -> Reply
            end end.
%%
change_leg_1(RProps, Map, Ctx) ->
    CallId = maps:get(callid,RProps),
    [NUri,NCallId,NFTag,NTTag] = case maps:get(acallid,Map) of
                                       CallId -> ?EU:maps_get([bruri,bcallid,bltag,brtag],Map);
                                       _ -> ?EU:maps_get([aruri,acallid,artag,altag],Map)
                                   end,
    % opposite replace opts
    RProps1 = #{callid => NCallId, fromtag => NFTag, totag => NTTag, opts => maps:get(opts,RProps)},
    ReplacesValue = ?U:build_replaces_header(RProps1),
    % update req
    #sipmsg{to={_,XTag},headers=H0}=Req = maps:get(req,Ctx),
    H1 = lists:keystore(?ReplacesLow,1,H0,{?ReplacesLow,ReplacesValue}),
    Req1 = Req#sipmsg{ruri=NUri,to={NUri,XTag},headers=H1},
    % next
    Ctx1 = Ctx#{req => Req1, to_uri => NUri},
    {ok,RProps1,Ctx1}.
%%     % @todo #184.de
%%     % forks (if forking state, N>1 forks)
%%     ForksReplaces =
%%         lists:foldl(
%%           fun({BCallId,BFMap,_BInfo},Acc) ->
%%                     case ?EU:maps_get([bruri,bcallid,bltag,brtag],BFMap) of
%%                         [_,_,_,undefined] -> Acc;
%%                         [BUri,BCallId,BFTag,BTTag] ->
%%                             BReplacesValue = <<BCallId/bitstring,";from-tag=",BFTag/bitstring,";to-tag=",BTTag/bitstring>>,
%%                             BRProps1 = {BCallId,BFTag,BTTag},
%%                             [{BUri,BRProps1,BReplacesValue} | Acc]
%%                     end end, [], maps:get(bforks,Map,[])),
%%     % next
%%     Ctx1 = Ctx#{req => Req1, to_uri => NUri, forks_replaces => ForksReplaces},
%%     {ok,RProps1,Ctx1}.

% ----
% @private
-spec get_dialog_info(SrvIdx::integer(), CallId::binary(), Ctx::map()) -> {reply,SipReply::tuple()} | map().

get_dialog_info(SrvIdx, CallId, Ctx) ->
    case ?CFG:get_sipserver_by_index(SrvIdx) of
        undefined -> ?IR_UTILS:reply(Ctx, ?InternalError("B2B. Unknown srvidx"));
        {_Site, _Node}=Dest ->
            MFA1 = {?B2BUA_DLG, pull_fsm_by_callid, [CallId]},
            case ?ENVCROSS:call_node(Dest, MFA1, undefined, 2000) of
                undefined -> ?IR_UTILS:reply(Ctx, ?InternalError("B2B. Dlg server unavailable"));
                false -> ?IR_UTILS:reply(Ctx, ?InternalError("B2B. Dlg server dialog not found"));
                {DlgId,_} ->
                    fix_forks_on_replace(Dest, DlgId, Ctx),
                    get_dialog_info_1(Dest, DlgId, Ctx)
            end end.

% @private
fix_forks_on_replace(Dest, DlgId, _Ctx) ->
    Request = {?B2BUA_DLG, fix_forks_on_replace, [DlgId]},
    ?ENVCROSS:call_node(Dest, Request, undefined).

% @private
get_dialog_info_1(Dest, DlgId, Ctx) ->
    Request = {?B2BUA_DLG, get_current_info, [DlgId]},
    case ?ENVCROSS:call_node(Dest, Request, undefined) of
        undefined -> ?IR_UTILS:reply(Ctx, ?InternalError("B2B. Dlg server unavailable"));
        false -> ?IR_UTILS:reply(Ctx, ?InternalError("B2B. Dlg server dialog not found"));
        {ok,#{}=Map} ->
            case maps:get(state,Map) of
                dialog -> Map;
                forking ->
                    case maps:get(b,Map) of
                        [] -> ?IR_UTILS:reply(Ctx, ?Forbidden("B2B. Dlg server dialog in forking state, 0 forks"));
                        [BInfo] ->
                            % forking state (1 fork)
                            [CallId,LTag,RTag,RUri] = ?EU:maps_get([bcallid,bltag,brtag,bruri],BInfo),
                            Map#{bruri=>RUri,bcallid=>CallId,bltag=>LTag,brtag=>RTag};
                        [_,_|_]=L ->
                            ?IR_UTILS:reply(Ctx, ?Forbidden(?EU:str("B2B. Dlg server dialog in forking state, ~p forks",[length(L)])))
%%                         [_|_]=L ->
%%                             % @todo #184.de
%%                             % forking state (N forks)
%%                             [{_,M1,_}|_]=BForks =
%%                                    lists:foldl(fun(BInfo,Acc) ->
%%                                                         [CallId,LTag,RTag,RUri] = ?EU:maps_get([bcallid,bltag,brtag,bruri],BInfo),
%%                                                         [{CallId,#{bruri=>RUri,bcallid=>CallId,bltag=>LTag,brtag=>RTag},BInfo} | Acc]
%%                                                end, [], L),
%%                             maps:merge(Map#{bforks=>BForks}, M1)
                    end end end.

%% -----------------------------------
%% Replaces
%% -----------------------------------

%% ------------------------------
%% @private
%% parse replaces header into RepMap::#{callid,fromtag,totag,opts}
%% ------------------------------
-spec parse_replaces(Repl::binary()) -> {CallId::binary(),FromTag::binary(),ToTag::binary()}.
%% ------------------------------
parse_replaces(Repl) ->
    [YCallId|Params] = binary:split(Repl,<<";">>,[global]),
    Params1 = lists:map(fun(A) -> case binary:split(A, <<"=">>, [global]) of [K,V] -> {K,V}; [K] -> {K,undefined} end end, Params),
    [FTag,TTag] = ?EU:extract_optional_default([{<<"from-tag">>,<<>>},{<<"to-tag">>,<<>>}], Params1),
    Ftrim = fun(T) -> binary:replace(T,<<" ">>,<<>>) end,
    #{callid => Ftrim(YCallId),
      fromtag => Ftrim(FTag),
      totag => Ftrim(TTag),
      opts => lists:keydelete(<<"from-tag">>,1,lists:keydelete(<<"to-tag">>,1,Params1))}.

% @private check to forks #184.de
do_check_route_direct_0(AOR,Req,RProps,Ctx) ->
    case maps:get(forks_replaces,Ctx,[]) of
        [_,_|_]=FR ->
            % @todo #184.de
            {ok,forks,FR,Ctx};
        _ -> do_check_route_direct_1(AOR,Req,RProps,Ctx)
    end.
% @private check to conf
do_check_route_direct_1({_S,U,_D}=AOR,Req,RProps,Ctx) ->
    case ?U:parse_conf_user(U) of
        {ok,_} -> {ok,conf,Ctx};
        false -> do_check_route_direct_2(AOR,Req,RProps,Ctx)
    end.
% @private check to ivr
do_check_route_direct_2({_S,U,_D}=AOR,Req,RProps,Ctx) ->
    case ?U:parse_ivr_user(U) of
        {ok,{ivr,_}} -> {ok,ivr,RProps,Ctx};
        {ok,{hunt,_}} -> {ok,hunt,RProps,Ctx};
        _ -> do_check_route_direct_3(AOR,Req,RProps,Ctx)
    end.
% @private check to prompt
do_check_route_direct_3({_S,U,_D}=AOR,Req,RProps,Ctx) ->
    case ?U:parse_prompt_user(U) of
        {ok,_} -> {ok,prompt,RProps,Ctx};
        _ -> do_check_route_direct_4(AOR,Req,RProps,Ctx)
    end.
% @private check to user
do_check_route_direct_4({_S,U,D}=AOR,Req,RProps,Ctx) ->
    case ?ACCOUNTS:check_sipuser_exists(U,D) of
        true -> {ok,user,Ctx};
        false ->
            case ROpts=?REPLACES:find_replaced_opts(RProps, Req#sipmsg.call_id) of
                {error,_} -> do_check_route_direct_5(AOR,Req,RProps,ROpts,Ctx);
                {ok,Opts} ->
                    case ?EU:extract_optional_props([sipuser], Opts) of
                        [undefined] -> do_check_route_direct_5(AOR,Req,RProps,ROpts,Ctx);
                        [{sip,_,_}=RealAOR] -> {ok,user,RealAOR,Ctx}
                    end end end.
% @private check to ext
do_check_route_direct_5(_AOR,Req,RProps,undefined,Ctx) ->
    ROpts = ?REPLACES:find_replaced_opts(RProps, Req#sipmsg.call_id),
    do_check_route_direct_5(_AOR,Req,RProps,ROpts,Ctx);
do_check_route_direct_5(AOR,Req,RProps,{error,_},Ctx) -> do_check_route_direct_6(AOR,Req,RProps,Ctx);
do_check_route_direct_5(AOR,Req,RProps,{ok,Opts},Ctx) when is_list(Opts) ->
    case ?EU:extract_optional_props([extaccount,extsrvidx], Opts) of
        [undefined,undefined] -> do_check_route_direct_6(AOR,Req,RProps,Ctx);
        [#{}=_Account,_SrvIdx]=P -> {ok,ext,Ctx,P}
    end.
% @private
do_check_route_direct_6(_AOR,_Req,RProps,Ctx) ->
    case maps:get(callid,RProps,undefined) of
        <<"rCF-",_/binary>> -> {ok,conf,Ctx};
        <<"rIV-",_/binary>> -> {ok,ivr,RProps,Ctx};
        <<"rPR-",_/binary>> -> {ok,prompt,RProps,Ctx};
        _ -> {false,Ctx}
    end.

%% -----------------------------------
%% Referred-By
%% -----------------------------------

% @private
% update ctx when referred call (define achievable caller-id)
update_ctx_referred(_IsReplaces, Ctx) ->
    RefByUri = maps:get(by_uri,Ctx),
    ByUri = ?U:update_uri_user_to_phonenumber(?U:clear_uri(RefByUri)), % ref by uri
    % FromUri = maps:get(from_uri,Ctx), % real from uri
    Ctx#{by_uri => ByUri}.
