%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 22.07.2020
%%% @doc Invite routing to set of urirules (internal user, external number, group of users or numbers) (aors)

-module(r_sip_b2bua_router_invite_urirules).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([do_invite_urirules/3,
         do_invite_urirules/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_headers.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_b2b_invite_router.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

do_invite_urirules(_CalledAOR, UriRules, Ctx) ->
    do_invite_urirules(UriRules, Ctx).

do_invite_urirules(UriRules, Ctx) ->
    Opts = [{urirules,UriRules},
            {ctx,Ctx}, % RP-640
            {from_sipuser,maps:get(from_sipuser,Ctx)},
            {req_from_uri,maps:get(req_from_uri,Ctx)},
            {b2bhdr,maps:get(b2bhdr,Ctx)},
            {intercom,maps:get(intercom,Ctx,false)},
            {link_referred,maps:get(link_referred,Ctx,undefined)}, % #354
            {link_replaces,maps:get(link_replaces,Ctx,undefined)}, % #354
            {use_media,?IR_UTILS:get_usemedia(Ctx,true)},
            {addRemoteParty1xx,true},
            {addRemoteParty2xx,true}
           | ?IR_UTILS:build_opts_by_ctx(Ctx)],
    {ok, Opts, Ctx}.
