%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.2015 refactored 31.12.2016
%%% @doc Router rule search&apply engine.

-module(r_sip_b2bua_router_rule).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([find_route/1,
         apply_redirect/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_b2b_invite_router.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

% ------------
% common start
% ------------

-spec find_route(Ctx::map()) ->
          {ok, Result::{ok,{inside,{rules,Args::list()}}} |
                       {ok,{outside,{aor,[AOR::aor()]}}} |
                       {ok,{feature,{FT::binary(),Args::list()}}, Ctx::map()}} |
          {reply, {SipCode::integer(), Opts::list()}, Ctx::map()} |
          {error, Reason::{Code::atom(),R::list()}, Ctx::map()}.

find_route(#{}=Ctx) ->
    [TD,FindOpts] = ?EU:maps_get([domain,findopts], Ctx),
    Opts = case maps:get(trace,Ctx,undefined) of
               undefined -> [];
               _ -> [trace]
           end,
    case ?DC:find_route_rule(FindOpts,Opts) of
        [] -> reply(Ctx, ?NotFound("B2B. No routes found"));
        {ok,Rules,Trace} ->
            ?IR_EVENT:event('findroute',[Ctx,FindOpts,Rules]),
            ?IR_TRACE:trace({dc,TD,Trace},Ctx),
            case Rules of
                [Rule|Rest] ->
                    ?IR_TRACE:trace({dcfoundlen,TD,length(Rest)+1},Ctx),
                    do_invite_by_rule(Rule, Ctx#{rules => Rest});
                [] ->
                    ?IR_TRACE:trace({dcnotfound,TD},Ctx),
                    reply(Ctx, ?NotFound("B2B. No routes found"))
            end end.

%% ====================================================================
%% Internal functions
%% ====================================================================

ok(Res,Ctx) -> {ok,Res,Ctx}.
reply(Ctx,SipReply) -> {reply,SipReply,Ctx}.
err(Err,Ctx) -> {error,Err,Ctx}.

%% ====================================================
% restart by redirect
%% ====================================================

%% @internal
apply_redirect(Ctx) ->
    case maps:get(restrict_redirect,Ctx,false) of
        true -> reply(Ctx,?Forbidden("B2B. Found redirect in restricted case"));
        false -> apply_redirect_1(Ctx)
    end.

%% @private
apply_redirect_1(Ctx) ->
    RedirAORS = maps:get(redirected_aors,Ctx,[]),
    #uri{}=To = maps:get(to_uri,Ctx),
    #uri{}=By = maps:get(by_uri,Ctx),
    % check if no cycle of redirect
    Key = {?U:make_aor(By),?U:make_aor(To)},
    case lists:member(Key, RedirAORS) of
        true -> reply(Ctx,?LoopDetected("B2B. Detected loop of redirects"));
        false -> apply_redirect_2(Ctx#{redirected_aors => [Key|RedirAORS]})
    end.

%% @private
apply_redirect_2(Ctx) ->
    % check
    #uri{user=ToU}=To = maps:get(to_uri,Ctx),
    #uri{domain=TD,user=ByU}=By = maps:get(by_uri,Ctx),
    RouteCode = maps:get(routecode,Ctx,<<>>),
    % trace
    ?IR_TRACE:trace({redirect,TD,ByU,ToU}, Ctx),
    % route
    Ctx1 = Ctx#{isredirected => true,
                findopts => [TD, 'inner', ?U:make_aor(By), ?U:make_aor(To), undefined, RouteCode],
                fwdcount => 10,
                nextcount => 10},
    find_route(Ctx1).

%% ====================================================
%% follow next rule on aprior failure
%% ====================================================

% @private
follow_next_rule(Ctx, SipReply) ->
    PrevCtx = maps:get(prev_ctx,Ctx),
    case maps:get('rules', Ctx, []) of
        [] -> reply(Ctx, SipReply);
        [Rule|Rest] ->
            ?IR_TRACE:trace({stash_reply, ?IR_UTILS:parse_reply(SipReply)}, Ctx),
            do_invite_by_rule(Rule, PrevCtx#{rules => Rest,
                                             sipreply => maps:get('sipreply', Ctx, SipReply)})
    end.

%% ====================================================
%% apply route rule
%% ====================================================

% @private
do_invite_by_rule(Rule, Ctx) ->
    Action = maps:get(action,Rule),
    do_invite_by_rule_1(Action, Rule, Ctx).

% -- DENIED
do_invite_by_rule_1('denied', Rule, Ctx) ->
    % trace rule
    ?IR_TRACE:trace({rule,action,denied}, Ctx),
    % event route
    ?IR_EVENT:event('route',[Ctx,#{},Rule]),
    % reply
    reply(Ctx, ?Forbidden("B2B. Route denied"));

% -- INTERNAL
do_invite_by_rule_1('internal', Rule, #{}=Ctx) ->
    {ok, Ctx1, [#uri{user=ToU}=To,#uri{user=ByU}=By,_From]} = ?IR_UTILS:modify_ToByFrom_uris(Ctx, Rule, undefined),
    [ToAOR,ByAOR] = ?U:make_aors([To,By]),
    % trace rule
    ?IR_TRACE:trace({rule,action,internal,ByU,ToU}, Ctx),
    % event route
    ?IR_EVENT:event('route',[Ctx1,#{},Rule]),
    % route through redirect
    Ctx2 = Ctx1#{prev_ctx => Ctx},
    do_invite_inside(ByAOR, ToAOR, Ctx2);

% -- INTERNALPBX
do_invite_by_rule_1('internalpbx', Rule, #{}=Ctx) ->
    {ok, Ctx1, [#uri{user=ToU},#uri{user=ByU},_From]} = ?IR_UTILS:modify_ToByFrom_uris(Ctx, Rule, undefined),
    % trace rule
    ?IR_TRACE:trace({rule,action,internalpbx,ByU,ToU}, Ctx),
    % event route
    ?IR_EVENT:event('route',[Ctx1,#{},Rule]),
    % find and route through redirect
    Ctx2 = Ctx1#{prev_ctx => Ctx},
    do_invite_insidepbx(Ctx2);

% -- FEATURECODE
do_invite_by_rule_1('featurecode', Rule, Ctx) ->
    {ok, Ctx1, [#uri{user=ToU},#uri{user=ByU},_]} = ?IR_UTILS:modify_ToByFrom_uris(Ctx, Rule, undefined),
    % trace rule
    ?IR_TRACE:trace({rule,action,featurecode,ByU,ToU}, Ctx),
    % event route
    ?IR_EVENT:event('route',[Ctx1,#{},Rule]),
    % route
    do_invite_feature(Ctx1#{prev_ctx => Ctx});

% -- CROSSDOMAIN
do_invite_by_rule_1('crossdomain', _, #{fwdcount:=0}=Ctx) ->
    reply(Ctx, ?Forbidden("B2B. Cross domain forward limit"));
do_invite_by_rule_1('crossdomain', Rule, #{fwdcount:=FwdCnt}=Ctx) ->
    ToDomainModifier = maps:get(domain,Rule),
    {ok, Ctx1, [#uri{user=ToU,domain=RuleDomain}=To,#uri{user=ByU}=By,_From]} = ?IR_UTILS:modify_ToByFrom_uris(Ctx, Rule, ToDomainModifier),
    [ToAOR,ByAOR] = ?U:make_aors([To,By]),
    RouteCode = maps:get(routecode,Ctx,<<>>),
    % trace rule
    ?IR_TRACE:trace({rule,action,crossdomain,RuleDomain,ByU,ToU}, Ctx),
    % event route
    ?IR_EVENT:event('route',[Ctx,#{},Rule]),
    % next
    FindOpts = [RuleDomain, 'cross', ByAOR, ToAOR, <<>>, RouteCode],
    Opts = case maps:get(trace,Ctx,undefined) of
               undefined -> [];
               _ -> [trace]
           end,
    case ?DC:find_route_rule(FindOpts,Opts) of
        [] -> reply(Ctx, ?NotFound("B2B. No routes found"));
        {ok,Rules,Trace} ->
            % trace dc
            ?IR_TRACE:trace({dc,RuleDomain,Trace},Ctx),
            % event findroute
            ?IR_EVENT:event('findroute',[Ctx,FindOpts,Rules]),
            case Rules of
                [RuleCross|Rest] ->
                    ?IR_TRACE:trace({dcfoundlen,RuleDomain,length(Rest)+1},Ctx),
                    do_invite_by_rule(RuleCross, Ctx1#{domain:=RuleDomain,
                                                       fwdcount:=FwdCnt-1,
                                                       rules => Rest});
                [] ->
                    ?IR_TRACE:trace({dcnotfound,RuleDomain},Ctx),
                    reply(Ctx1, ?Forbidden("B2B. No routes found"))
            end end;

% -- EXTERNAL
do_invite_by_rule_1('external', Rule, Ctx) ->
    AccountCode = maps:get(account,Rule),
    RuleDomain = maps:get(domain,Rule),
    {ok, Ctx1, [#uri{user=ToU},#uri{user=ByU},_]} = ?IR_UTILS:modify_ToByFrom_uris(Ctx, Rule, RuleDomain),
    % trace rule
    ?IR_TRACE:trace({rule,action,external,AccountCode,ByU,ToU}, Ctx),
    % event route
    ?IR_EVENT:event('route',[Ctx,#{},Rule]),
    % route
    do_invite_outside(Ctx1#{account => AccountCode,
                            rule => Rule,
                            prev_ctx => Ctx});

% -- NEXT
do_invite_by_rule_1('next', _, #{nextcount:=0}=Ctx) ->
    reply(Ctx, ?Forbidden("B2B. Next action forward limit"));
do_invite_by_rule_1('next', Rule, #{nextcount:=NxtCnt}=Ctx) ->
    Domain = maps:get(domain,Ctx),
    {ok, Ctx1, [#uri{user=ToU}=To,#uri{user=ByU}=By,_From]} = ?IR_UTILS:modify_ToByFrom_uris(Ctx, Rule, Domain),
    [ToAOR,ByAOR] = ?U:make_aors([To,By]),
    RouteCode = maps:get(routecode,Ctx,<<>>),
    % trace rule
    ?IR_TRACE:trace({rule,action,next,ByU,ToU}, Ctx),
    % event route
    ?IR_EVENT:event('route',[Ctx,#{},Rule]),
    % next
    FindOpts = [Domain, 'inner', ByAOR, ToAOR, <<>>, RouteCode],
    Opts = case maps:get(trace,Ctx,undefined) of
               undefined -> [];
               _ -> [trace]
           end,
    case ?DC:find_route_rule(FindOpts,Opts) of
        [] -> reply(Ctx, ?NotFound("B2B. No routes found"));
        {ok,Rules,Trace} ->
            % trace dc
            ?IR_TRACE:trace({dc,Domain,Trace},Ctx),
            % event findroute
            ?IR_EVENT:event('findroute',[Ctx,FindOpts,Rules]),
            case Rules of
                [RuleNext|Rest] ->
                    ?IR_TRACE:trace({dcfoundlen,Domain,length(Rest)+1},Ctx),
                    do_invite_by_rule(RuleNext, Ctx1#{domain:=Domain,
                                                         nextcount:=NxtCnt-1,
                                                         rules => Rest});
                [] ->
                    ?IR_TRACE:trace({dcnotfound,Domain},Ctx),
                    reply(Ctx1, ?Forbidden("B2B. No routes found"))
            end end.


%% ====================================================
%% route direction defined (inside/outside/feature)
%% ====================================================

% -----------------------------------
% inside on sipuser
% -----------------------------------

% ---------------------
% insidepbx (to number)
% ---------------------
do_invite_insidepbx(Ctx) ->
    [ToUri,ByUri] = ?EU:maps_get([to_uri,by_uri],Ctx),
    [{ToS,ToU,ToD}=ToAOR,{_,ByU,_}=ByAOR] = ?U:make_aors([ToUri,ByUri]),
    case ?DC:find_insidepbx(ToAOR) of
        undefined -> reply(Ctx, ?InternalError("B2B. InsidePBX DC error"));
        not_found -> follow_next_rule(clear_ctx(Ctx), ?NotFound("B2B. InsidePBX not found")); % #162
        Number when is_binary(Number) ->
            [<<>>,FNum] = binary:split(ToU, Number),
            % trace fnum
            ?IR_TRACE:trace({rule,action,internalpbx,ByU,Number,FNum}, Ctx),
            % fwd as internal, but fnum..
            do_invite_inside(ByAOR, {ToS,Number,ToD}, Ctx#{fnum => FNum,
                                                           to_uri := ToUri#uri{user=Number}}) % replace!
    end.

% ---------------------
% inside (to username)
% ---------------------
do_invite_inside(ByAOR, ToAOR, Ctx) ->
    % check redirect absolute
    case ?IR_UTILS:check_redirect(ByAOR, ToAOR, ?ABSOLUTE, Ctx) of
        false -> do_invite_inside_1(ToAOR, Ctx);
        {false, RedirCond} -> do_invite_inside_1(ToAOR, Ctx#{redircond => RedirCond});
        {ok, <<"dnd">>} -> reply(Ctx, ?TemporarilyUnavailable("B2B. Redirect to dnd absolutely"));
        {ok, RedirNumber} ->
            % @todo @redirect apply modifier to FNumExt
            RedirNumber1 = ?REDIRECT:apply_modifier(RedirNumber,maps:get(fnum,Ctx,<<>>),[]),
            ToUri = maps:get(to_uri,Ctx),
            RouteCtx = Ctx#{fnum => undefined, % redirect not use insidepbx fnum anyway?
                            to_uri => ToUri#uri{user=RedirNumber1},
                            by_uri => ?U:clear_uri(ToUri)},
            apply_redirect(RouteCtx)
    end.

% ------------------
% 'subscribe' filter
do_invite_inside_1(CalledAOR, Ctx) ->
    case maps:get(subscribe,Ctx,false) of
        false -> do_invite_inside_2(invite, CalledAOR, Ctx);
        true -> do_invite_inside_2(subscribe, CalledAOR, Ctx)
    end.

% obtaining sipusers from dc, forks from registrar
% @todo redirects should be made on internal accounts to handle calls to group numbers
%       now group numbers block using absolute, unregistered, busy reasons
%        note cycled redirects
do_invite_inside_2(subscribe,CalledAOR, Ctx) ->
    FOk = fun() -> Ok = {ok,{inside,{aor,[CalledAOR]}}},
                   ?IR_TRACE:trace(Ok,Ctx),
                   ok(Ok,Ctx)
          end,
    case ?CALLEE:get_callee(map, CalledAOR) of
        [_|_] -> FOk();
        {reply, {404,_}=SipReply} -> follow_next_rule(clear_ctx(Ctx), SipReply);
        {reply, {480,_}} -> FOk();
        {reply, SipReply} -> reply(clear_ctx(Ctx),SipReply)
    end;

%% when not subscribe (ex. invite)
do_invite_inside_2(Method, {_,Num,Domain}=CalledAOR, Ctx) ->
    case ?NUMBERPLAN:find_sipgroup_subnumbers(Num, Domain) of
        undefined -> do_invite_inside_direct(Method, CalledAOR, Ctx);
        [] -> reply(clear_ctx(Ctx), ?NotFound("B2B. Unknown number"));
        %_ -> do_invite_inside_2a(Method, CalledAOR, Ctx);
        [Item] when not is_list(Item) -> do_invite_inside_direct(Method, CalledAOR, Ctx);
        SeqList when is_list(SeqList) ->
            case ?IR_GROUP:do_invite_inside_group(Method,CalledAOR,Ctx,SeqList,fun apply_redirect/1) of
                {reply,SipReply} -> reply(clear_ctx(Ctx), SipReply);
                T -> T
            end
    end.

%% ---------------------------------------------
%% @private
%% when direct number
%% ---------------------------------------------
do_invite_inside_direct(_, CalledAOR, Ctx) ->
    case ?CALLEE:get_callee(map, CalledAOR) of
        [_|_]=UriRules -> do_invite_inside_direct_1(CalledAOR, UriRules, Ctx);
        {reply, {404,_}=SipReply} -> follow_next_rule(clear_ctx(Ctx), SipReply);
        {reply, {480,_}=SipReply} -> do_invite_inside_redirect(CalledAOR, ?UNREGISTERED, clear_ctx(Ctx), SipReply);
        {reply, SipReply} -> reply(clear_ctx(Ctx),SipReply)
    end.

% redircond into UriRule (for groups)
do_invite_inside_direct_1(CalledAOR, UriRules, Ctx) ->
    F = fun(UR) -> UR#{dir => 'inside',
                       redircond => maps:get(redircond,Ctx,[])}
        end,
    UriRules1 = ?IR_UTILS:map_urirules(F, UriRules),
    UriRules2 = ?IR_INSIDE:make_urirules(CalledAOR,UriRules1,Ctx),
    do_invite_inside_direct_2(CalledAOR, UriRules2, Ctx).

% #141 barge in or limited trunk count
do_invite_inside_direct_2(CalledAOR, UriRules, Ctx) ->
    case maps:get(barge,Ctx,false) of
        true -> do_invite_inside_direct_2a(CalledAOR, UriRules, Ctx);
        false -> do_invite_inside_direct_2b(CalledAOR, UriRules, Ctx)
    end.

% #141 barge in business
do_invite_inside_direct_2a(CalledAOR, UriRules, Ctx) ->
    Fdirect = fun Fdirect(R,Acc) when is_map(R) ->
                            SipUser = maps:get(sipuser,R),
                            [Login,Domain] = ?EU:extract_required_props([login,domain],SipUser),
                            case {sip,Login,Domain} of
                                Acc -> Acc;
                                AOR when Acc==undefined -> AOR;
                                _ -> group
                            end;
                  Fdirect(R,Acc) when is_list(R) -> lists:foldl(Fdirect,Acc,R)
              end,
    case lists:foldl(Fdirect,undefined,UriRules) of
        undefined -> reply(Ctx,?Forbidden("B2B. Invalid fork data"));
        group -> reply(Ctx,?Forbidden("B2B. Barge is not allowed on group"));
        AOR ->
            case ?USRSTATE:find_dialog_count(AOR) of
                0 -> do_invite_inside_direct_3(CalledAOR, UriRules, Ctx);
                _ -> do_invite_inside_direct_3(CalledAOR, UriRules, Ctx#{intercom => true})
            end end.

% #65 check limit of trunks
do_invite_inside_direct_2b(CalledAOR, UriRules, Ctx) ->
    case maps:get(skip_redirect,Ctx,false) of
        true -> do_invite_inside_direct_3(CalledAOR, UriRules, Ctx);
        false ->
            case aor_business(UriRules) of
                {false,_} -> do_invite_inside_direct_3(CalledAOR, UriRules, Ctx);
                {true,BusyMap} ->
                    case maps:size(BusyMap) of
                        1 -> % direct call - use busy redirect
                            AOR = rule_aor('phone',UriRules),
                            do_invite_inside_redirect(AOR, ?BUSY, clear_ctx(Ctx), ?BusyHere("B2B. Trunk count limit reached"));
                        _ -> % group call - filter busy
                            Ffilter = fun Ffilter(R) when is_map(R) ->
                                               case maps:find(rule_aor('phone',R), BusyMap) of
                                                   {_,true} -> false;
                                                   {_,false} -> {true,R}
                                               end;
                                             Ffilter(R) when is_list(R) -> case lists:filtermap(Ffilter,R) of [] -> false; R1 -> {true,R1} end
                                         end,
                            case lists:filtermap(Ffilter,UriRules) of
                                [] -> follow_next_rule(clear_ctx(Ctx), ?BusyHere("B2B. Trunk count limit reached"));
                                UriRules1 -> do_invite_inside_direct_3(CalledAOR, UriRules1, Ctx)
                            end end end end.

% #86 insidepbx extensions
do_invite_inside_direct_3(CalledAOR, [_|_]=UriRules, Ctx) ->
    case  maps:get(fnum, Ctx, undefined) of
        undefined ->
            Ok = {ok,{inside,{rules,[CalledAOR,UriRules]}}},
            ?IR_TRACE:trace(Ok,Ctx),
            ok(Ok,Ctx);
        FNum when is_binary(FNum) ->
            % #86 map request uri from sipuser to fnum
            Fmap = fun Fmap(R) when is_map(R) ->
                            [Uri,SipUser] = ?EU:maps_get([uri,sipuser],R),
                            % #189
                            %{U1,FNum1} = {FNum,FNum},
                            [Login,Phone,SUOpts] = ?EU:extract_required_props([login,phone,opts],SipUser),
                            {U1,FNum1} =
                                case maps:get(<<"modextout">>,SUOpts,<<"*">>) of
                                    <<"*">> -> {FNum,FNum};
                                    <<>> -> {Login,undefined};
                                    Modifier ->
                                        case ?FROM:apply_modifier_ext(Modifier, FNum, [Login,Phone,FNum,FNum]) of
                                            <<>> -> {Login,undefined};
                                            ER -> {ER,ER}
                                        end end,
                            R#{uri:=Uri#uri{user=U1},
                               sipuser:=lists:keystore(extension,1,SipUser,{extension,FNum1})};
                       Fmap(R) when is_list(R) -> lists:map(Fmap,R)
                   end,
            UriRules1 = lists:map(Fmap, UriRules),
            Ok = {ok,{insidepbx,{rules,[CalledAOR,FNum,UriRules1]}}},
            ?IR_TRACE:trace(Ok,Ctx),
            ok(Ok,Ctx)
    end.

%% ---------------------------------------------
%% @private
%% redirect unregistered
%% ---------------------------------------------
do_invite_inside_redirect(CalledAOR, RedirectReason, Ctx, DefSipReply) ->
    ByAOR = ?U:make_aor(maps:get(by_uri, Ctx)),
    case ?IR_UTILS:check_redirect(ByAOR, CalledAOR, RedirectReason, Ctx) of
        false -> reply(Ctx,DefSipReply);
        {false, _} -> reply(Ctx,DefSipReply);
        {ok, RedirNumber} ->
            % @todo @redirect apply modifier to FNumExt
            RedirNumber1 = ?REDIRECT:apply_modifier(RedirNumber,maps:get(fnum,Ctx,<<>>),[]),
            ToUri = maps:get(to_uri,Ctx),
            RouteCtx = Ctx#{fnum => undefined, % redirect not use insidepbx fnum anyway?
                            to_uri => ToUri#uri{user=RedirNumber1},
                            by_uri => ?U:clear_uri(ToUri)},
            apply_redirect(RouteCtx)
    end.

%% @private
%% #65 check limit of trunks
%%   return {HasAnyBusy,BusyDict} when BusyDict contains {AOR => IsBusy}
aor_business(UriRules) ->
    Flimit = fun Flimit(R,{Any,Acc}) when is_map(R) ->
                      SipUser = maps:get(sipuser,R),
                      [Login,Domain] = ?EU:extract_required_props([login,domain],SipUser),
                      AOR = {sip,Login,Domain},
                      case maps:find(AOR,Acc) of
                          {_,_} -> {Any,Acc};
                          error ->
                              [Opts] = ?EU:extract_required_props([opts],SipUser),
                              IsBusy = case maps:get(<<"trunks">>,Opts,-1) of
                                             -1 -> false;
                                             0 -> true;
                                             Limit ->
                                               case ?USRSTATE:find_dialog_count({sip,Login,Domain}) of
                                                       N when N >= Limit -> true;
                                                   _ -> false
                                               end end,
                              Acc1 = maps:put(AOR,IsBusy,Acc),
                              case IsBusy of true -> {true,Acc1}; false -> {Any,Acc1} end
                      end;
                 Flimit(R,Acc) when is_list(R) -> lists:foldl(Flimit,Acc,R)
           end,
    lists:foldl(Flimit,{false,maps:new()},UriRules).

%% @private
%% #65
rule_aor(Key,R) when is_map(R) andalso (Key==phone orelse Key==login)  ->
    SipUser = maps:get(sipuser,R),
    [Value,Domain] = ?EU:extract_required_props([Key,domain],SipUser),
    {sip,Value,Domain};
rule_aor(Key,[_|_]=Rules) ->
    lists:foldl(fun(_, Acc) when Acc/=undefined -> Acc;
                   ([], Acc) -> Acc;
                   ([[]|_], Acc)  -> Acc;
                   ([R|_], _) -> rule_aor(Key,R);
                   (_, Acc) -> Acc
                end, undefined, Rules).

%% --------
%% @private
%% clear fnum, intercom, barge when attempt error
clear_ctx(Ctx) -> maps:without([fnum, intercom, barge], Ctx).

%% -------------------------------
%% outside
%% -------------------------------
do_invite_outside(Ctx) ->
    case maps:get(subscribe,Ctx,false) of
        false -> do_invite_outside_1(Ctx);
        true ->
            Res = {error,{invalid_params,<<"Route destination mismatch (external)">>}},
            err(Res,Ctx)
    end.

%% @private
do_invite_outside_1(Ctx) ->
    D = maps:get(domain,Ctx),
    A = maps:get(account,Ctx),
    case ?DC:find_provider_account_by_code(D, A) of
        #{}=Account ->
            %% TODO make over registrar spec schema (esg puts, b2b gets. to economy on multicall)
            SrvIdxs = get_serveridxs(D,Account),
            Code = maps:get(code,Account),
            Res = ?ENVMULTICALL:apply([{?IR_UTILS, check_provider_account, [SrvIdx, Account]} || SrvIdx <- SrvIdxs], 2000),
            case lists:filter(fun({_,ok}) -> true; (_) -> false end,Res) of
                [{{_,_,[SrvIdx,_]},_}|_] ->
                    #uri{user=ToU}=maps:get(to_uri,Ctx),
                    RouteCode = maps:get(routecode,Ctx,<<>>),
                    Ok = {ok,{outside,[ToU,RouteCode,SrvIdx,Code,Account#{serveridx => SrvIdx}]}},
                    ?IR_TRACE:trace(Ok,Ctx),
                    ok(Ok,Ctx);
                [{error,Reason}|_] -> follow_next_rule(Ctx, ?NotFound(?EU:str("B2B. Ext route inaccessible (~ts)", [Reason])));
                [] -> follow_next_rule(Ctx, ?NotFound(?EU:str("B2B. Ext route inaccessible", [])))
            end;
        _ -> follow_next_rule(Ctx, ?Forbidden("B2B. Route/account not found"))
    end.

%% @private
get_serveridxs(Domain,Account) ->
    case maps:get(serveridxs,Account) of
        SrvIdx when is_integer(SrvIdx) -> [SrvIdx];
        [_|_]=SrvIdxs -> SrvIdxs;
        [] ->
            {ok,[ExtSett],_} = ?ENVDC:get_object_sticky(Domain,settings,[{keys,[<<"ext">>]}],false,auto),
            case maps:get(<<"default_provider_serveridxs">>,maps:get(value,ExtSett,#{}),undefined) of
                undefined ->
                    DomainSites = ?ENVCFGU:get_domain_sites(Domain),
                    lists:sort([Idx || {_,_,Idx,_} <- lists:flatten([?ENVCFG:get_nodes_containing_role_site('esg',Site,[idx]) || Site <- DomainSites])]);
                SrvIdxs when is_list(SrvIdxs) -> SrvIdxs;
                _ -> []
            end
    end.

%% -----------------------------
%% featurecode
%% -----------------------------
do_invite_feature(Ctx) ->
    {_,U,_}=AOR = ?U:make_aor(maps:get(to_uri,Ctx)),
    case ?DC:find_featurecode(AOR) of
        undefined -> reply(Ctx, ?InternalError("B2B. Feature DC error"));
        not_found -> reply(Ctx, ?Forbidden("B2B. Feature not found"));
        #{}=F ->
            Prefix = maps:get(prefix,F),
            % parse
            [<<>>,FNum] = binary:split(U, Prefix),
            Type = maps:get(type,F),
            Extension = maps:get(extension,F),
            % result
            Ok = {ok,{feature,{Type,[AOR,FNum,Extension]}}},
            ?IR_TRACE:trace(Ok,Ctx),
            ?IR_EVENT:event('feature',[Ctx,U,F]),
            ok(Ok,Ctx#{featurecode_id => maps:get(id,F),
                       featurecode_prefix => Prefix})
    end.

%% ====================================================
%%
%% ====================================================
