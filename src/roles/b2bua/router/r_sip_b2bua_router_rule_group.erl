%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 22.07.2020
%%% @doc Route to group
%%%      Uses redirect mode

-module(r_sip_b2bua_router_rule_group).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([do_invite_inside_group/5]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_b2b_invite_router.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---------------------------------------------
%% when group number
%% ---------------------------------------------
do_invite_inside_group(_Method, {_,_,Domain}=CalledAOR, Ctx, SeqList, FunRedirect) ->
    ?IR_TRACE:trace({group_start,Domain,SeqList}, Ctx),
    Ftimeout = fun(UriRules,undefined) -> UriRules;
                  (UriRules,Timeout) ->
                      F = fun(UR) -> case maps:get(ftimeout,UR,undefined) of
                                         undefined -> UR#{ftimeout => Timeout};
                                         FTimeout -> UR#{ftimeout => min(Timeout,FTimeout)}
                                     end end,
                      ?IR_UTILS:map_urirules(F, UriRules)
               end,
    F3 = fun(N,RulesAcc,Timeout) ->
                % TODO filter appropriate numbers
                ?IR_TRACE:trace({group_item_start,Domain,N}, Ctx),
                RedirNumber1 = ?REDIRECT:apply_modifier(N,maps:get(fnum,Ctx,<<>>),[]),
                ToUri = maps:get(to_uri,Ctx),
                RouteCtx = Ctx#{fnum => undefined, % redirect not use insidepbx fnum anyway?
                                to_uri => ToUri#uri{user=RedirNumber1},
                                by_uri => ?U:clear_uri(ToUri)},
                FOk = fun(Rules) ->
                            ?IR_TRACE:trace({group_item_final,ok}, Ctx),
                            RulesAcc ++ [Ftimeout(Rules,Timeout)] end,
                FSkip = fun() ->
                            ?IR_TRACE:trace({group_item_final,skip}, Ctx),
                            RulesAcc end,
                case FunRedirect(RouteCtx) of
                    {ok,{ok,{inside,{rules,[_CalledAOR,UriRules]}}},_Ctx1} -> FOk(UriRules);
                    {ok,{ok,{insidepbx,{rules,[_CalledAOR,_FNum,UriRules]}}},_Ctx1} -> FOk(UriRules);
                    {ok,{ok,{outside,[ToU,RouteCode,SrvIdx,Code,Account]}},Ctx1} ->
                        case ?IR_OUTSIDE:make_urirules({ToU,RouteCode,SrvIdx,Code,Account},Ctx1) of
                            UriRules when is_list(UriRules) -> FOk([UriRules]);
                            _ -> FSkip()
                        end;
                    _ -> FSkip()
                end
         end,
    F2 = fun(N,Acc) when is_binary(N) -> F3(N,Acc,undefined);
            ({N,Timeout},Acc) when is_binary(N) -> F3(N,Acc,Timeout)
         end,
    %
    F1 = fun(Group,Acc) when is_list(Group) ->
                % [N1,N2,...]; N :: [[R1]] | [[R1,...]] | [[R1],[R2]] | [[R1,R2,...],[R3,R4,...],...]
                Acc2 = lists:foldl(F2, [], Group),
                % example: [[[a1,a2],[a3]],[[b1,b2,b3]],[[c1],[c2,c3],[c4,c5,c6]]] -> [[a1,a2,b1,b2,b3,c1],[a3,c2,c3],[c4,c5,c6]]
                FMix = fun(L, AccN) ->
                            Len = max(length(L), length(AccN)),
                            FEnlarge = fun(List) -> List ++ lists:map(fun(_) -> [] end, lists:seq(length(List),Len-1)) end,
                            L1 = FEnlarge(L),
                            AccN1 = FEnlarge(AccN),
                            lists:zipwith(fun(E1,E2) ->  E1 ++ E2 end,AccN1,L1)
                       end,
                Res = lists:foldl(FMix, [], Acc2),
                Acc ++ Res
         end,
    %
    case lists:foldl(F1, [], SeqList) of
        UriRules when is_list(UriRules) ->
            % [[R1,R2,...],[R3,R4,...],...]
            UriRules1 = filter_duplicates(UriRules,get_group(CalledAOR)),
            UriRules2 = filter_empties(UriRules1),
            CtxNew = update_ctx(UriRules2, Ctx),
            case lists:flatten(UriRules1) of
                [] -> {reply, ?NotFound("B2B. Group is empty or unavailable")};
                _ ->
                    ?IR_TRACE:trace({group_final,CalledAOR,UriRules2}, CtxNew),
                    {ok,{ok,{urirules,{rules,[CalledAOR,UriRules2]}}},CtxNew}
            end;
        _T -> {reply, ?InternalError("B2B. Group call error")}
    end.

%% @private
%% filter same aor rules
filter_duplicates(UriRules,GroupItem) ->
    case get_filtermode(GroupItem) of
        'none' -> UriRules;
        FilterMode ->
            Fhash = fun(UriRule) ->
                        case maps:get(dir,UriRule) of
                            'outside' when FilterMode=='inner' -> {'outside', maps:get(aor,UriRule), maps:get(code,maps:get(extaccount,UriRule)), make_ref()};
                            'outside' -> {'outside', maps:get(aor,UriRule), maps:get(code,maps:get(extaccount,UriRule))};
                            'inside' when FilterMode=='outer' -> {'inside', maps:get(aor,UriRule), make_ref()};
                            'inside' -> {'inside', maps:get(aor,UriRule), erlang:phash2(maps:get(uri,UriRule))}
                        end end,
            F2 = fun(I, {Acc,Hash}) ->
                        Key = Fhash(I),
                        case maps:get(Key,Hash,undefined) of
                            undefined -> {Acc ++ [I], Hash#{Key => 1}};
                            _ -> {Acc, Hash}
                        end end,
            F1 = fun(I,{Acc,Hash}) ->
                        {I1,Hash1} = lists:foldl(F2,{[],Hash},I),
                        {lists:append(Acc,[I1]), Hash1}
                 end,
            element(1,lists:foldl(F1, {[],#{}}, UriRules))
    end.

%% @private
get_group({_,Num,Domain}=_CalledAOR) ->
    case ?ENVDC:get_object_sticky(Domain,group,[{keys,[Num]}],auto) of
        {ok,[Item],_} -> Item;
        _ -> undefined
    end.

%% @private
get_filtermode(undefined) -> 'all';
get_filtermode(GroupItem) ->
    case maps:get(<<"filter_mode">>,maps:get(opts,GroupItem),'all') of
        <<"none">> -> 'none';
        <<"inner">> -> 'inner';
        <<"outer">> -> 'outer';
        <<"all">> -> 'all';
        _ -> 'all'
    end.

%% @private
%% filter empty groups
filter_empties(UriRules) ->
    lists:filter(fun([]) -> false; (_) -> true end, UriRules).

%% @private
update_ctx(_UriRules, Ctx) -> Ctx.

