%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 3.01.2017.
%%% @doc Router facade for subscribe requests

-module(r_sip_b2bua_router_subscribe).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([sip_subscribe/1]).

%% ==========================================================================
%% Define
%% ==========================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_headers.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_b2b_invite_router.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

-spec sip_subscribe(Req::#sipmsg{}) ->
          {error,Reason::{Code::atom(),Reason::binary()}} |
          {reply,{SipCode::integer(),Reason::string()}} |
          {ok,E::{'inside',{'aor',[CalledAOR::{Scheme::atom(),UserName::binary(),Domain::binary()}]}} |
                 {'feature',{Type::binary(),Args::list()}}}.

sip_subscribe(#sipmsg{}=Req) ->
    #sipmsg{from={#uri{user=FromUser,domain=FD},_},
            to={#uri{user=ToNumber,domain=TD},_}}=Req,
    case FD==TD of
        true ->
            Dir = 'inner',
            ExtCode = <<>>,
            RouteCode = FromUser,
            FromUri = ?U:update_uri_user_to_phonenumber(?U:make_uri({sip,FromUser,TD})),
            FindOpts = [TD,Dir,?U:make_aor(FromUri),ToAOR={sip,ToNumber,TD},ExtCode,RouteCode],
            Ctx = #{dir => 'inner',
                    from_uri => FromUri,
                    by_uri => FromUri,
                    to_uri => ?U:make_uri(ToAOR),
                    domain => TD,
                    fwdcount => 10,
                    nextcount => 10,
                    findopts => FindOpts,
                    event => false, % no eventing
                    % routing for subscribe BLF purposes
                    subscribe => true
                   },
            % route by rule
            case ?IR_RULE:find_route(Ctx) of
                {ok,{ok,R},_Ctx1} -> {ok,R};
                {error,Err,_Ctx1} -> Err;
                {reply,SipReply,_Ctx1} -> {reply, ?IR_UTILS:parse_reply(SipReply)}
            end;
        false ->
            {error,{access_denied,<<"B2B. Cross domain subscription blocked">>}}
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================



