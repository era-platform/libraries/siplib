%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 3.01.2017.
%%% @doc

-module(r_sip_b2bua_router_test).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([apply/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_headers.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_b2b_invite_router.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

% test
apply(Map) ->
    ?ENV:set_group_leader(),
    case ?EU:maps_get([domain,dir,from,to,extcode],Map) of
        {error,_} -> {error, {invalid_params,<<"Invalid params. Expected 'domain', 'dir', 'from', 'to', 'extcode'">>}};
        LO when is_list(LO) -> apply_test(LO,Map)
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -----------------------------
%% start
%% -----------------------------

apply_test([TD,Dir,FromUser,ToNumber,ExtCode],Map) ->
    [DoTrace,ReturnResult,UseSubscribe] = ?EU:maps_get_default(lists:map(fun(K) -> {K,undefined} end, [trace,result,subscribe]),Map),
    FromUri = ?U:update_uri_user_to_phonenumber(?U:make_uri({sip,FromUser,TD})),
    RouteCode = FromUser,
    FindOpts = [TD,Dir,?U:make_aor(FromUri),ToAOR={sip,ToNumber,TD},ExtCode,RouteCode],
    Ctx = #{dir => Dir,
            from_uri => FromUri,
            by_uri => FromUri,
            to_uri => ?U:make_uri(ToAOR),
            domain => TD,
            fwdcount => 10,
            nextcount => 10,
            findopts => FindOpts,
            event => false, % no eventing
            % routing for subscribe BLF purposes
            subscribe => ?EU:to_bool(UseSubscribe)
           },
    % do trace routing for test purposes
    Ctx1 = case ?EU:to_bool(DoTrace) of
               true ->
                   Ctx0 = ?IR_TRACE:start(Ctx),
                   ?IR_TRACE:trace({start,TD,FromUser,ToNumber},Ctx0),
                   Ctx0;
               _ -> Ctx
           end,
    % route by rule
    {ok,Res,Trace} =
        case ?IR_RULE:find_route(Ctx1) of
            {ok,R,Ctx2} -> ok(R,Ctx2);
            {error,Err,Ctx2} -> err(Err,Ctx2);
            {reply,SipReply,Ctx2} -> reply(Ctx2,SipReply)
        end,
    % return value
    case {?EU:to_bool(ReturnResult), ?EU:to_bool(DoTrace)} of
        {true,true} -> {ok,Res,Trace};
        {true,false} -> {ok,Res};
        {false,true} -> {ok,Trace};
        {false,false} -> ok
    end.

%% -----------------------------
%% final
%% -----------------------------

% success
ok(R,Ctx) ->
    fin(R,Ctx).

% reject
reply(Ctx, SipReply) ->
    R = {reply, ?IR_UTILS:parse_reply(SipReply)},
    ?IR_TRACE:trace(R, Ctx),
    ok(R,Ctx).

% error
err({error,_}=R,Ctx) ->
    ?IR_TRACE:trace(R,Ctx),
    fin(R,Ctx).

% -----------
% finalize
fin(R,Ctx) ->
    {ok, R, ?IR_TRACE:stop(Ctx)}.
