%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 14.12.2016
%%% @doc Route tester. WS calls to test routes. Uses trace

-module(r_sip_b2bua_router_trace).
-author('Peter Bukashin <tbotc@yandex.ru>').

%% -------------------------------------------------------------------
%%
%% -------------------------------------------------------------------

-export([trace/2,
         start/1,
         stop/1]).

%% ==========================================================================
%% Defines
%% ==========================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_b2b_invite_router.hrl").

%% ====================================================================
%% API functions
%% ====================================================================


% trace line
trace(T, Ctx) ->
    case maps:get(trace,Ctx,undefined) of
        FTrace when is_function(FTrace,1) -> FTrace(T);
        _ -> ok
    end.

% start trace
start(#{}=Ctx) -> Ctx#{trace => ?ENVTRACE:start()}.

% stop trace
stop(FTrace) when is_function(FTrace,1) ->
    {ok,Trace} = ?ENVTRACE:stop(FTrace),
    make_trace(Trace);
stop(#{}=Ctx) ->
    case maps:get(trace,Ctx,undefined) of
        FTrace when is_function(FTrace,1) -> stop(FTrace);
        _ -> []
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -----------------------------
%% final
%% -----------------------------

% @private
make_trace([]) -> [];
make_trace(undefined) -> [];
make_trace(Trace) ->
    % map dc trace
    T1 = lists:foldl(fun({dc,Domain,T}, Acc) ->
                             lists:reverse(lists:map(fun({Tp,Vec,Id,Pr,Txt}) ->
                                                             {dc,Domain,Tp,Vec,Id,Pr,Txt}
                                                     end, T)) ++ Acc;
                        (T,Acc) -> [T|Acc]
                     end, [], Trace),
    % convert dc trace to string
    {_,T2} = lists:foldl(fun(A,{Idx,Acc}) -> {Idx+1,[make_ln(Idx,A)|Acc]} end, {1,[]}, T1),
    lists:reverse(T2).

% @private
make_ln(I, {start,TD,FromNumber,ToNumber}) ->                         ln("~p. Start: domain='~s', from='~s', to='~s'",[I,TD,FromNumber,ToNumber]);
make_ln(I, {redirect,TD,ByU,ToU}) ->                                  ln("~p. Redirect: domain='~s', from='~s', to='~s'",[I,TD,ByU,ToU]);
make_ln(I, {group_start,TD,SeqList}) ->                               ln("~p. Group detected: domain='~s', seq=~1000tp",[I,TD,SeqList]);
make_ln(I, {group_item_start,TD,Num}) ->                              ln("~p. -> Next group item: domain='~s', num='~s'",[I,TD,Num]);
make_ln(I, {group_item_final,Res}) ->                                 ln("~p. <- Group item final: ~s",[I,Res]);
make_ln(I, {group_final,CalledAOR,UriRules}) ->                       ln("~p. Group final: route to ~s on ~1000p groups, ~1000p forks [~s]",[I,?U:unparse_uri(?U:make_uri(CalledAOR)),length(UriRules),length(lists:flatten(UriRules)),make_inside_rules(UriRules)]);
make_ln(I, {dc,TD,route,V,Id,Pr,Txt}) ->                              ln("~p. DC '~s' route=(vector='~s', priority=~1000p, id='~s'): ~s", [I,TD,V,Pr,Id,Txt]);
make_ln(I, {dc,TD,vectorrule,V,Id,Pr,Txt}) ->                         ln("~p. DC '~s' vectorrule=(vector='~s', priority=~1000p, id='~s'): ~s", [I,TD,V,Pr,Id,Txt]);
make_ln(I, {dcfoundlen,_TD,Len}) ->                                   ln("~p. Found rules count: ~1000p", [I,Len]);
make_ln(I, {dcnotfound,_TD}) ->                                       ln("~p. No rules found", [I]);
make_ln(I, {rule,action,denied}) ->                                   ln("~p. Rule action denied", [I]);
make_ln(I, {rule,action,internal,ByU,ToU}) ->                         ln("~p. Rule action internal: by='~s', to='~s'", [I,ByU,ToU]);
make_ln(I, {rule,action,internalpbx,ByU,ToU}) ->                      ln("~p. Rule action internalpbx: by='~s', to='~s'", [I,ByU,ToU]);
make_ln(I, {rule,action,internalpbx,ByU,ToU,FNum}) ->                 ln("~p. Rule action internalpbx: by='~s', to='~s' as '~s'", [I,ByU,ToU,FNum]);
make_ln(I, {rule,action,featurecode,ByU,ToU}) ->                      ln("~p. Rule action featurecode: by='~s', to='~s'", [I,ByU,ToU]);
make_ln(I, {rule,action,crossdomain,Domain,ByU,ToU}) ->               ln("~p. Rule action crossdomain: domain='~s', by='~s', to='~s'", [I,Domain,ByU,ToU]);
make_ln(I, {rule,action,next,ByU,ToU}) ->                             ln("~p. Rule action next: by='~s', to='~s'", [I,ByU,ToU]);
make_ln(I, {rule,action,external,ExtCode,ByU,ToU}) ->                 ln("~p. Rule action external: code='~s', by='~s', to='~s'", [I,ExtCode,ByU,ToU]);
make_ln(I, {ok,{inside,{aor,[CalledAOR]}}}) ->                        ln("~p. Final: route inside on ~s", [I,?U:unparse_uri(?U:make_uri(CalledAOR))]);
make_ln(I, {ok,{inside,{rules,[CalledAOR,UriRules]}}}) ->             ln("~p. Final: call inside to ~s on ~1000p groups, ~1000p forks [~s]", [I,?U:unparse_uri(?U:make_uri(CalledAOR)),length(UriRules),length(lists:flatten(UriRules)),make_inside_rules(UriRules)]);
make_ln(I, {ok,{insidepbx,{rules,[CalledAOR,FNum,UriRules]}}}) ->     ln("~p. Final: call insidepbx to ~s/~s on ~1000p groups, ~1000p forks [~s]", [I,?U:unparse_uri(?U:make_uri(CalledAOR)),FNum,length(UriRules),length(lists:flatten(UriRules)),make_inside_rules(UriRules)]);
make_ln(I, {ok,{urirules,{rules,[CalledAOR,UriRules]}}}) ->           ln("~p. Final: call * to ~s on ~1000p groups, ~1000p forks [~s]", [I,?U:unparse_uri(?U:make_uri(CalledAOR)),length(UriRules),length(lists:flatten(UriRules)),make_inside_rules(UriRules)]);
make_ln(I, {ok,{outside,[Num,RouteCode,SrvIdx,Code,_AMap]}}) ->       ln("~p. Final: call outside num='~s', routecode='~s', code='~s', srv=~1000p", [I,Num,RouteCode,Code,SrvIdx]);
make_ln(I, {ok,{feature,{<<"pickup">>,[_AOR,Num,_]}}}) ->             ln("~p. Final: feature pickup num='~s'", [I,Num]);
make_ln(I, {ok,{feature,{<<"grouppickup">>,[_AOR,_,_]}}}) ->          ln("~p. Final: feature pickup group", [I]);
make_ln(I, {ok,{feature,{<<"conference">>,[_AOR,Num,_]}}}) ->         ln("~p. Final: feature call conference num='~s'", [I,Num]);
make_ln(I, {ok,{feature,{<<"videoconference">>,[_AOR,Num,_]}}}) ->    ln("~p. Final: feature call videoconference num='~s'", [I,Num]);
make_ln(I, {ok,{feature,{<<"ivr">>,[_AOR,Num,Extns]}}}) ->            ln("~p. Final: feature call ivr num='~s', code='~s'", [I,Num,Extns]);
make_ln(I, {ok,{feature,{<<"hunt">>,[_AOR,Num,Extns]}}}) ->           ln("~p. Final: feature call hunt num='~s', foreground num='~s'", [I,Num,Extns]);
make_ln(I, {ok,{feature,{<<"voicemail">>,[_AOR,Num,Extns]}}}) ->      ln("~p. Final: voicemail manage num='~s', code='~s'", [I,Num,Extns]);
make_ln(I, {ok,{feature,{<<"voicemail_send">>,[_AOR,Num,Extns]}}}) -> ln("~p. Final: voicemail send num='~s', code='~s'", [I,Num,Extns]);
make_ln(I, {ok,{feature,{<<"parking">>,[_AOR,Num,Extns]}}}) ->        ln("~p. Final: parking put/wait num='~s', code='~s'", [I,Num,Extns]);
make_ln(I, {ok,{feature,{<<"parking_get">>,[_AOR,Num,Extns]}}}) ->    ln("~p. Final: parking get num='~s', code='~s'", [I,Num,Extns]);
make_ln(I, {ok,{feature,{<<"fax_to_email">>,[_AOR,Num,Extns]}}}) ->   ln("~p. Final: fax to email num='~s', code='~s'", [I,Num,Extns]);
make_ln(I, {ok,{feature,{<<"intercom">>,[_AOR,Num,Extns]}}}) ->       ln("~p. Final: intercom num='~s', ext='~s'", [I,Num,Extns]);
make_ln(I, {ok,{feature,{<<"barge">>,[_AOR,Num,Extns]}}}) ->          ln("~p. Final: barge num='~s', ext='~s'", [I,Num,Extns]);
make_ln(I, {ok,{feature,{<<"replace">>,[_AOR,Num,Extns]}}}) ->        ln("~p. Final: replace num='~s', ext='~s'", [I,Num,Extns]);
make_ln(I, {ok,{feature,{Type,[_AOR,Num,Extns]}}}) ->                 ln("~p. Final: feature '~s' num='~s', ext='~s'", [I,Type,Num,Extns]);
make_ln(I, {stash_reply,{SipCode,Reason}}) ->                         ln("~p. Reply stashed: sipcode='~1000p', reason='~s'", [I,SipCode,Reason]);
make_ln(I, {reply,{SipCode,Reason}}) ->                               ln("~p. Final: reply sipcode='~1000p', reason='~s'",[I,SipCode,Reason]);
make_ln(I, _) ->                                                      ln("~p. Unknown trace line",[I]).

% @private
make_inside_rules(UriRules) ->
    F = fun F(L,Acc) when is_list(L) ->
                    ["[" ++ string:join(lists:reverse(lists:foldl(F,[],L)), ", ") ++ "]" | Acc];
            F(M,Acc) when is_map(M) ->
                    [?EU:to_list(?U:unparse_uri(?U:make_uri(maps:get(aor,M)))) ++
                        case maps:get(dir,M,undefined) of 'outside' -> io_lib:format("(out '~s')", [maps:get(code,maps:get(extaccount,M))]); _ -> [] end
                    |Acc]
        end,
    string:join(lists:reverse(lists:foldl(F, [], UriRules)), "; ").

% @private
ln(Fmt,Args) -> ?EU:strbin(Fmt,Args).
