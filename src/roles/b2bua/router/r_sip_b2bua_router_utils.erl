%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.2015 refactored 31.12.2016.
%%% @doc Router utils and pure functions

-module(r_sip_b2bua_router_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([update_ctx_dir/3,
         %
         check_redirect/4,
         build_opts_by_ctx/1,
         build_from_uri/2,
         make_route_ctx/1,
         %
         modify_ToByFrom_uris/3,
         %do_apply_modifier/3, apply_modifier/4,% debug
         %
         check_provider_account/2,
         get_usemedia/2,
         %
         reply/2,
         parse_reply/1,
         %
         map_urirules/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_headers.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_b2b_invite_router.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ----------------------------
-spec update_ctx_dir(Ctx::map(), From::#uri{}, Req::#sipmsg{}) -> NewCtx::map().
%% ----------------------------
update_ctx_dir(Ctx, From, Req) ->
    % from inside or from outside?
    case ?U:get_call_source(From,Req) of
        'outer' -> Ctx#{dir => 'outer'};
        'inner' -> update_ctx_inner(Ctx#{dir => 'inner'}, Req)
    end.

%% @private
update_ctx_inner(Ctx, Req) ->
    case nksip_request:header(?CallerTypeHeaderLow, Req) of
        {ok,[]} -> Ctx;
        {ok,[X|_]} -> Ctx#{caller_type => X}
    end.

%% @private
update_ctx_uris(Ctx, [ToUri,ByUri,FromUri],RouteCode) ->
    Ctx#{to_uri => ToUri,
         by_uri => ByUri,
         from_uri => FromUri,
         routecode => RouteCode}.

%% ===================================================================

%% ----------------------------
%% check redirect of internal number by type
%% ----------------------------
-spec check_redirect(FromAOR::tuple(), ToAOR::tuple(), Mode::atom(), Ctx::map()) -> false | {false, Conditions::list()} | {ok, RedirNumber::binary()}.
%% ----------------------------
check_redirect(FromAOR, ToAOR, Reason, Ctx) ->
    % 05.02.2017 check if there is no denial of redirect in header opts (x-era-b2bua)
    B2BHdr = maps:get(b2bhdr,Ctx,[]),
    case lists:keyfind(<<"redirect">>,1,B2BHdr) of
        false -> check_redirect_1(FromAOR, ToAOR, Reason, Ctx);
        {_,<<"0">>} -> false
    end.

%% @private
check_redirect_1(FromAOR, {S,_,D}=ToAOR, Reason, Ctx) ->
    case maps:get(skip_redirect,Ctx,false) of
        true -> false;
        false ->
            case ?DC:get_active_redirect(FromAOR, ToAOR) of
                {error,_} -> false;
                [] -> false;
                X when is_list(X) ->
                    case ?EU:get_by_key(Reason, X, false) of
                        false -> {false,X};
                        RedirNumber ->
                            ?IR_EVENT:event('redirect',[Ctx,#{redirect_to => {S,RedirNumber,D},
                                                              owner => ToAOR,
                                                              reason => Reason}]),
                            {ok, RedirNumber}
                    end end end.

%% ===================================================================

%% ----------------------------
%% build opts by ctx
%% ----------------------------
build_opts_by_ctx(Ctx) ->
    % conf/ivr call to b2b by rtp, b2b could forward to webrtc/rtp, so media should be used
    case maps:get(caller_type, Ctx, undefined) of
        <<"conference">> -> [{use_media,true},
                             {use_record,?U:is_record_conf()}];
        <<"videoconference">> -> [{use_media,true},
                                  {use_record,?U:is_record_conf()}];
        <<"prompt">> -> [{use_media,true},
                         {use_record,?U:is_record_prompt()}];
        <<"ivr">> -> [{use_media,true},
                      {use_record,?U:is_record_ivr()}];
        _ -> []
    end.

%% ----------------------------
%% build from uri (callerid of ctx's from for display at ToAOR)
%% ----------------------------
build_from_uri({_,_,TD}=ToAOR, Ctx) ->
    % @TODO @from @remoteparty for b on invite header 'from'
    FromSipUserInfo = maps:get('from_sipuser',Ctx,undefined),
    FromDisplay = maps:get('from_displayname',Ctx,undefined),
    FromUri = case maps:get('req_from_uri',Ctx,undefined) of
                  undefined -> maps:get('from_uri',Ctx);
                  T -> T
              end,
    FromAOR = ?U:make_aor(FromUri),
    Flogrepr = fun(#{}=M) -> ?IR_EVENT:event('represenative',[Ctx,M]) end,
    {ok,ResUser,ResDisp} = ?REPRESENTATIVE:apply(FromAOR,ToAOR,FromSipUserInfo,FromDisplay,Flogrepr),
    _BLUri = FromUri#uri{scheme=sip, disp=ResDisp, user=ResUser, domain=TD}.

%% ----------------------------
%% makes lightweight route context for fork (used in redirects - old, used before representatives)
%% ----------------------------
make_route_ctx(Ctx) ->
    maps:with([from_sipuser,
               to_uri, by_uri, from_uri,
               routecode,
               req_from_uri,
               invite_id, invite_ts,
               restrict_redirect,
               skip_redirect,
               b2bhdr,
               relates,
               dlg_links,
               ctx_scrid_master,
               ctx_scrid_domain,
               featurecode_id, featurecode_prefix,
               dtmf2send], Ctx).

%% ===================================================================

%% ---------------------------
%% modify uris in context by rule
%% ----------------------------
modify_ToByFrom_uris(Ctx, Rule, ToDomainModifier) ->
    CD = maps:get(domain,Ctx),
    [ToUri,ByUri,FromUri] = [maps:get(to_uri,Ctx), maps:get(by_uri,Ctx), maps:get(from_uri,Ctx)],
    RouteCode = maps:get(routecode,Ctx,<<>>),
    FromTo = {?EU:to_list(FromUri#uri.user), ?EU:to_list(ToUri#uri.user)},
    % user
    ToUri1 = ToUri#uri{user=apply_modifier('tomodifier', Rule, ToUri#uri.user, FromTo, [x,t])},
    ByUri1 = ByUri#uri{user=apply_modifier('frommodifier', Rule, ByUri#uri.user, FromTo, [x,t])},
    FromUri1 = FromUri#uri{user=apply_modifier('frommodifier', Rule, FromUri#uri.user, FromTo, [x,t])},
    RouteCode1 = apply_modifier('routecodemodifier', Rule, RouteCode, FromTo, []),
    % domain
    ToUri2 = case is_binary(ToDomainModifier) andalso ToDomainModifier/= <<>> of
                 false -> ToUri1;
                 true -> ToUri1#uri{domain=apply_domain_modifier(ToDomainModifier,CD,Rule)}
             end,
    ByUri2 = ByUri1#uri{domain=CD},
    FromUri2 = FromUri1#uri{domain=CD},
    % result
    Uris = [ToUri2,ByUri2,FromUri2],
    {ok, update_ctx_uris(Ctx,Uris,RouteCode1), Uris}.

%% ---------------------------
%% APPLY_MODIFIER PROXY (vector rules)
%% modify number by rule (/X/7XX2*, 7T, XX/*/, /reg/pattern/replace/gi /reg/pattern2/replace2/)
%% ----------------------------
apply_modifier(_, undefined, Value, _, _) -> Value;
apply_modifier(Key, #{}=Rule, Value, {From,To}, ExOpts) ->
    case maps:get(Key, Rule, undefined) of
        undefined -> Value;
        Modifier when is_binary(Modifier) -> do_apply_modifier(Modifier, Value, {From,To}, Rule, ExOpts)
    end.

%% @private
%% regex or text mask
%%  powered by r_env_modifier. To accelerate ~10% text modifier it could be used locally without mapping some fields
do_apply_modifier(Modifier, Value, {From,To}, Rule, ExOpts) when is_binary(Modifier) ->
    Flow = fun(X) -> fun() -> string:to_lower(?EU:to_list(X)) end end,
    BraceOpts = [{$F,From}, {$f,Flow(From)}, {$T,To}, {$t,Flow(To)}],
    ?ENVMODIFIER:apply_modifier(Modifier, Value, BraceOpts, [asterisk,ebrace,regex,{rule,Rule} | ExOpts]). % ExOpts :: [x,t]

%% modify to_domain (for cross rules)
apply_domain_modifier(ToDomain, _, undefined) -> ToDomain;
apply_domain_modifier(ToDomain, MyDomain, #{}=Rule) ->
    Modifier = ToDomain,
    Value = MyDomain,
    BraceOpts = [{$D,MyDomain}],
    ?ENVMODIFIER:apply_modifier(Modifier, Value, BraceOpts, [asterisk,ebrace,regex,{rule,Rule}]).

%% ===================================================================

%% ------------------------------
%% connect to esg and check if account could be used
%% ------------------------------
-spec check_provider_account(SrvIdx::integer(), Account::map()) -> ok | {error, Reason::binary()}.
%% ------------------------------
check_provider_account(SrvIdx, Account) ->
    case check_reg(SrvIdx, Account) of
        ok -> check_trunkcount(SrvIdx, Account);
        Error -> Error
    end.

%% @private
%% checks esg node is active and registration is active
check_reg(SrvIdx, Account) ->
    Request = {?EXT_REGSRV,rpc_check_reg,[Account]},
    case ?CFG:get_sipserver_by_index(SrvIdx) of
        undefined -> {error,<<"node_inaccessible">>};
        {_Site, _Node}=Dest ->
            case ?ENVCROSS:call_node(Dest, Request, undefined, 1000) of
                undefined -> {error,<<"node_inaccessible">>};
                Reply -> Reply
            end end.

%% @private
%% checks if active trunk count on provider account is not limited yet
check_trunkcount(SrvIdx, AMap) ->
    case maps:get(trunksout,AMap) of
        T when not is_integer(T) -> {error,<<"error">>};
        T when T >= 99999 -> ok;
        _Limit ->
            % RP-493
            AMap1 = maps:with([id,clusterdomain,lic,trunksout],AMap),
            Request = {?EXT_REGSRV,rpc_check_limit_inner,[AMap1]},
            case ?CFG:get_sipserver_by_index(SrvIdx) of
                undefined -> {error, <<"node_inaccessible">>};
                {_Site, _Node}=Dest ->
                    case ?ENVCROSS:call_node(Dest, Request, undefined, 1000) of
                        undefined -> {error, <<"node_inaccessible">>};
                        {ok,true} -> ok;
                        _ -> {error,<<"license">>}
                    end end end.

%% ===================================================================

%% ---------------------------
%% extract media option from x-era-b2bua header, return default if not set
%% ----------------------------
-spec get_usemedia(Ctx::map(), Default::true|false) -> true|false.
%% ----------------------------
get_usemedia(Ctx,Default) ->
    B2BHdr = maps:get(b2bhdr,Ctx),
    case lists:keyfind(<<"media">>,1,B2BHdr) of
        {_,<<"1">>} -> true;
        {_,<<"0">>} -> false;
        _ -> Default
    end.

%% ===================================================================

%% ---------------------------
%% makes reply. search context if there is already registered high priority reply (from first rule).
%% ----------------------------
-spec reply(Ctx::map(), DefaultSipReply::{DefCode::integer(),DefOpts::list()}) -> {reply,SipReply::{Code::integer(), Opts::list()}}.
%% ----------------------------
reply(Ctx, DefaultSipReply) ->
    {Code,Opts}=SipReply1 = maps:get('sipreply', Ctx, DefaultSipReply),
    ?IR_EVENT:event('fin',[Ctx,#{action => reply,
                                 sipcode => Code,
                                 reason => case lists:keyfind(reason,1,Opts) of false -> ""; {_,{_,_,R}} -> R end}]),
    {reply, SipReply1}.

%% ---------------------------
%% parse reply into {Code,Reason}
%% ----------------------------
parse_reply({Code,ReplyOpts}=_SipReply) ->
    {_,{sip,_,Reason}} = lists:keyfind(reason,1,ReplyOpts),
    {Code,Reason}.

%% ===================================================================

map_urirules(F, UriRules) ->
   Up = fun Up(List) when is_list(List) -> lists:map(Up,List);
            Up(Map) when is_map(Map) -> F(Map)
        end,
    lists:map(Up, UriRules).