%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Pavel Abramov, Peter Bukashin <tbotc@yandex.ru>
%%% @date 26.02.2019
%%% @doc RP-1207. Saves operation call info in domain's CallStore

-module(r_sip_b2bua_callstats_srv).

-behaviour(gen_server).

-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).
-export([start_link/1,
         call_state/3, call_state/4, call_state/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").

-include_lib("stdlib/include/ms_transform.hrl").

-record(stat_event_s, {
          ets = undefined :: undefined | reference(),
          pid = undefined :: undefined | pid(),
          debug = false :: boolean(),
          ref = undefined :: undefined | reference()
         }).

-record(stat_dialog, {
          dialogid,
          forks,
          timerref,
          dlg_monref,
          dlg_pid
         }).

-define(TIMEOUT, 280000).
-define(TIMEOUTSec, (?TIMEOUT + 20000) div 1000).
-define(TYPE, <<"b2b">>).

-define(KEYFork, <<"callid">>).
-define(KEYCall, <<"dialogid">>).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) when is_map(Opts) ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, Opts, []).

%% -----------------------------------------------
-spec call_state(UState::atom(), Side::a|b, ForkInfo::#side_fork{}|undefined, CallState::#state_forking{}|#state_dlg{}) -> ok | {error, binary()}.
%% -----------------------------------------------
call_state(UState, Side, ForkInfo, CallState) ->
    call_state_do(UState, Side, ForkInfo, CallState),
    {Id, XSide} = get_side(Side, ForkInfo, CallState),
    ?B2BUA_UTILS:usr_state(UState, Id, XSide).

%% -----------------------------------------------
call_state(UState, Side, CallState) ->
    call_state(UState, Side, undefined, CallState).

%% @private
get_side(a, undefined, #state_forking{dialogid=Id, a=ASide}) -> {Id, ASide};
get_side(b, undefined, #state_forking{dialogid=Id, b=BSide}) -> {Id, BSide};
get_side(a, undefined, #state_dlg{dialogid=Id, a=ASide}) -> {Id, ASide};
get_side(b, undefined, #state_dlg{dialogid=Id, b=BSide}) -> {Id, BSide};
get_side(_, ForkInfo, #state_forking{dialogid=Id}) -> {Id, ForkInfo};
get_side(_, ForkInfo, #state_dlg{dialogid=Id}) -> {Id, ForkInfo}.

%% -----------------------------------------------
call_state(CallState) ->
    call_state_do(undefined, undefined, undefined, CallState).

%% ====================================================================
%% Behavioural functions
%% ====================================================================

%% -----------------------------------------------
init(Opts) ->
    Debug = maps:get(<<"debug">>, Opts, false),
    ETS = ets:new(callstats,[public,set,{keypos,#stat_dialog.dialogid}]),
    Ref = erlang:make_ref(),
    State = #stat_event_s{ets=ETS, debug=Debug, pid=self(), ref=Ref},
    d(State, "CallStats srv started. Opts: ~120p, ETS: ~120p", [Opts,ETS]),
    {ok, State}.

%% -----------------------------------------------
handle_call(Request, _From, State) ->
    d(State, "Call: ~120p",[Request]),
    {noreply, State}.

%% -----------------------------------------------
handle_cast({event, UState, Data, List}, State) ->
    DialogID = maps:get(?KEYCall, Data),
    d(State, "Cast: ~120p (~120p)", [UState,DialogID]),
    ?CsWorkerSrv:cast_workf(DialogID, fun() -> send_event(UState, Data, List, State) end),
    {noreply, State};

handle_cast({down,DialogID,Data}, State) ->
    d(State, "Monitor down: ~120p (~120p)", [DialogID]),
    ?OUT("DLG DOWN: ~p", [DialogID]),
    ?CsWorkerSrv:cast_workf(DialogID, fun() -> on_down(DialogID, Data, State) end),
    {noreply, State};

handle_cast({restart}, State) ->
    {stop, restart, State};

handle_cast(Msg, State) ->
    d(State, "Cast: unknown msg - ~120p",[Msg]),
    {noreply, State}.

%% -----------------------------------------------
handle_info({prolongation, DialogID, Ref}, #stat_event_s{ref=Ref}=State) ->
    d(State, "Info: prolongation ~120p",[DialogID]),
    ?CsWorkerSrv:cast_workf(DialogID, fun() -> prolongation(DialogID, State) end),
    {noreply, State};

handle_info(Info, State) ->
    d(State, "Info: unknown msg - ~120p",[Info]),
    {noreply, State}.

%% -----------------------------------------------
terminate(Reason, State) ->
    d(State, "callstat - terminate: ~120p",[Reason]),
    ok.

%% -----------------------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

d(#stat_event_s{debug=true}, Fmt, Args) -> ?OUT("callstat - " ++ Fmt,Args);
d(#stat_event_s{debug=false}, _, _) -> ok.

%% -----------------------------------------------
call_state_do(UState, Side, ForkInfo, State) ->
    case make_data(State) of
        {error,_}=Error -> Error;
        Data ->
            List = make_forks(Side, ForkInfo, State),
            gen_server:cast(?MODULE, {event, UState, Data, List})
    end.

%% @private
make_data(#state_forking{pid=Pid,acallid=ACId,dialogid=DID,invite_id=IID,invite_ts=ITS,map=M,a=ASide}) ->
    #side{localuri=UriL, remoteuri=UriR}=ASide,
    Opts = maps:get(opts, M, []),
    #{<<"site">> => ?CFG:get_current_site(),
      <<"pid">> => Pid,
      <<"status">> => <<"forking">>,
      <<"acallid">> => ACId,
      <<"dialogid">> => DID,
      <<"startts">> => ITS,
      <<"inviteid">> => IID,
      <<"remoteuri">> => clear_uri(UriR),
      <<"localuri">> => clear_uri(UriL),
      <<"esgdlg">> => maps:get(ctx_esgdlg,M),
      <<"opts">> => parse_opts(Opts)};
%%
make_data(#state_dlg{pid=Pid,acallid=ACId,dialogid=DID,invite_id=IID,invite_ts=ITS,map=M,a=ASide}) ->
    #side{localuri=UriL, remoteuri=UriR}=ASide,
    Opts = maps:get(opts, M, []),
    #{<<"site">> => ?CFG:get_current_site(),
      <<"pid">> => Pid,
      <<"status">> => <<"dialog">>,
      <<"acallid">> => ACId,
      <<"dialogid">> => DID,
      <<"startts">> => ITS,
      <<"inviteid">> => IID,
      <<"remoteuri">> => clear_uri(UriR),
      <<"localuri">> => clear_uri(UriL),
      <<"esgdlg">> => maps:get(ctx_esgdlg,M),
      <<"opts">> => parse_opts(Opts)};
%%
make_data(State) ->
    ?LOG("~120p -- Unknown state format: ~120p",[?MODULE, State]),
    {error, <<"Make data error. Unknown state format.">>}.

%% @private
parse_opts(Opts)->
    HFun = fun(K,R) -> nksip_request:header(K, R) end,
    case lists:keyfind(req, 1, Opts) of
        false -> [];
        {_, Req} ->
            F = fun(K, Acc) -> case HFun(K, Req) of {ok, [V|_]} -> Acc#{?EU:to_binary(K) => V}; _ -> Acc end end,
            lists:foldl(F, #{}, ["referred-by", "replaces"])
    end.

%% ---------------------------
make_forks(a, undefined, #state_dlg{a=A}) -> [map_fork(a,A)];
make_forks(b, undefined, #state_dlg{b=B}) -> [map_fork(b,B)];
make_forks(a, undefined, #state_forking{a=A}) -> [map_fork(a,A)];
make_forks(b, undefined, #state_forking{b_act=BAct}) ->
    F = fun(Fork) -> map_fork(b,Fork) end,
    lists:map(F,BAct);
make_forks(undefined, undefined, _) -> [];
make_forks(Side, ForkInfo, _) -> [map_fork(Side, ForkInfo)].

%% @private
map_fork(Side, #side{callid=CallID, localuri=UriL, remoteuri=UriR, finaltime=Time}) ->
    map_fork(Side, CallID, UriL, UriR, Time);
map_fork(Side, #side_fork{callid=CallID, localuri=UriL, remoteuri=UriR, rule_timeout=Time}) ->
    map_fork(Side, CallID, UriL, UriR, Time).

%% @private
map_fork(Side, CallID, UriL, UriR, Time) ->
    #{<<"expirets">> => Time,
      %<<"type">> => <<>>,
      <<"callid">> => CallID,
      <<"remoteuri">> => clear_uri(UriR),
      <<"localuri">> => clear_uri(UriL),
      <<"side">> => Side}.

%% @private
clear_uri(Uri) ->
    #uri{scheme=Uri#uri.scheme,
         user=Uri#uri.user,
         domain=Uri#uri.domain}.

%% -----------------------------------------------
%% worker fun
%% -----------------------------------------------
send_event(undefined, Data, _, #stat_event_s{}=State) ->
    DialogID = maps:get(?KEYCall, Data),
    #stat_dialog{forks=Forks}=_StatDlg = case ets_get(DialogID, State) of
                                            undefined -> new_stat_dialog(DialogID,Data);
                                            #stat_dialog{}=X -> X
                                        end,
    Domains = maps:keys(Forks),
    store_put(DialogID, Domains, Forks, Data);
%%
send_event(free, Data, List, #stat_event_s{}=State) ->
    DialogID = maps:get(?KEYCall, Data),
    #stat_dialog{forks=Forks}=StatDlg = case ets_get(DialogID, State) of
                                            undefined -> new_stat_dialog(DialogID,Data);
                                            #stat_dialog{}=X -> X
                                        end,
    {ToDelForks, ActForks} = forks_analyze(List, Forks),
    d(State, "W. Active: ~120p, NeedDelete:~120p",[maps:size(ActForks),maps:size(ToDelForks)]),
    ets_put(StatDlg#stat_dialog{forks=ActForks}, State),
    {ActDomains, ToDelDomains} = calculate_calls_domain(ActForks, ToDelForks, Forks),
    d(State, "W. ActData: ~120p, DelData:~120p",[ActDomains, ToDelDomains]),
    FilterForksDel = maps:without(ToDelDomains, ToDelForks),
    store_delete(DialogID, ToDelDomains, FilterForksDel),
    store_put(DialogID, ActDomains, ActForks, Data);
%%
send_event(_UState, Data, List, #stat_event_s{}=State) ->
    DialogID = maps:get(?KEYCall, Data),
    #stat_dialog{forks=Forks}=StatDlg = case ets_get(DialogID, State) of
                                            undefined -> new_stat_dialog(DialogID,Data);
                                            #stat_dialog{}=X -> X
                                        end,
    UpForks = zip_by_domain(List, Forks),
    {New, Old} = forks_analyze(List, UpForks),
    d(State, "W. New: ~120p, Old: ~120p",[New, Old]),
    Append = zip_by_domain(New, Old),
    d(State, "W. Append: ~120p",[Append]),
    ets_put(StatDlg#stat_dialog{forks=Append}, State),
    {ActData, _} = calculate_calls_domain(New, Old, Forks),
    d(State, "W. Update calls: ~120p",[ActData]),
    store_put(DialogID, ActData, New, Data).

%%
new_stat_dialog(DialogID,Data) ->
    DlgPid = maps:get(<<"pid">>, Data),
    ?MONITOR:append_fun(DlgPid, call_stats, fun() -> gen_server:cast(?MODULE, {down,DialogID,Data}) end),
    #stat_dialog{dialogid=DialogID,
                 forks=#{},
                 timerref=undefined,
                 dlg_pid=DlgPid}.

%%
on_down(DialogID,Data,State) ->
    case ets_get(DialogID, State) of
        undefined -> ok;
        #stat_dialog{forks=Forks}=_StatDlg ->
            ForksList = maps:fold(fun(_Dom,List,Acc) -> Acc ++ List end, [], Forks),
            send_event(free, Data, ForksList, State)
    end.

%% -----------------------------------------------
%% Returns a list of new forks and the remainder of the cache minus new
%% -----------------------------------------------
forks_analyze(List, ForksInCache) ->
    F = fun(ForkMap, {Map, Rest}) ->
                {CallId, Domain} = get_callid_and_domain(ForkMap),
                RestCache = remove_from_cache(CallId, Domain, Rest),
                {maps:update_with(Domain, fun(V) -> [ForkMap|V] end, [ForkMap], Map), RestCache}
        end,
    lists:foldl(F, {#{},ForksInCache}, List).

%% @private
remove_from_cache(CallId, Domain, ForksCached) ->
    F = fun(D, Forks, Map) when D==Domain ->
                FlterFun = fun(Fork) -> case maps:get(?KEYFork, Fork) of CallId -> false; _ -> true end end,
                case lists:filter(FlterFun, Forks) of
                    [] -> Map;
                    UpForks -> maps:put(D, UpForks, Map)
                end;
           (D, Forks, Map) -> maps:put(D, Forks, Map)
        end,
    maps:fold(F, #{}, ForksCached).

%% -----------------------------------------------
%% Calculate the domains for which new calls and
%% for which domains there are no forks, using the cache
%% -----------------------------------------------
calculate_calls_domain(Active, NeedDelete, Cache) ->
    KA = maps:keys(Active),
    KD = maps:keys(NeedDelete),
    Keys = maps:keys(Cache),
    M = fun(D) -> lists:member(D, Keys) end,
    F = fun(D, Acc) -> case M(D) of true -> Acc; false -> [D|Acc] end end,
    {lists:foldl(F, [], KA), lists:foldl(F, [], KD)}.

%% -----------------------------------------------
%% Supplement the forks in the cache with new ones, if they match, replace
%% -----------------------------------------------
zip_by_domain(Forks1, Forks2) when is_list(Forks1),is_map(Forks2) ->
    F = fun(Fork, Map) ->
                {_C,D} = get_callid_and_domain(Fork),
                maps:update_with(D, fun(V) -> [Fork|V] end, [Fork], Map)
        end,
    lists:foldl(F, Forks2, Forks1);
%%
zip_by_domain(Forks1, Forks2) when is_map(Forks1),is_map(Forks2) ->
    F = fun(D, List, Map) -> maps:update_with(D, fun(V) -> merge_forks(List, V) end, List, Map) end,
    maps:fold(F, Forks2, Forks1).

%% @private
merge_forks(Forks1, Forks2) when is_list(Forks1), is_list(Forks2) ->
    F1 = fun(Fork1, Acc) ->
                 case contains(Fork1, Acc) of true -> Acc; false -> [Fork1|Acc] end end,
    lists:foldl(F1, Forks1, Forks2).

%% @private
contains(Fork, List) ->
    Id = maps:get(?KEYFork, Fork),
    F = fun(ForkIn) -> maps:get(?KEYFork, ForkIn) == Id end,
    lists:any(F, List).

%% -----------------------------------------------
get_callid_and_domain(ForkMap) when is_map(ForkMap) ->
    case ?EU:maps_get([<<"callid">>,<<"side">>,<<"remoteuri">>,<<"localuri">>],ForkMap) of
        [CId,a,UriR,_UriL] -> {CId, UriR#uri.domain};
        [CId,b,_UriR,UriL] -> {CId, UriL#uri.domain}
    end.
%% -----------------------------------------------
cancel_timer(undefined) -> ok;
cancel_timer(TimerRef) -> erlang:cancel_timer(TimerRef).

%% -----------------------------------------------
prolongation(DialogID, State) ->
    case ets_get(DialogID, State) of
        undefined -> ok;
        #stat_dialog{forks=Forks}=StatDlg ->
            {_, Active} = forks_analyze([], Forks),
            store_nochange(DialogID, Active),
            ets_put(StatDlg, State)
    end.

%% -----------------------------------------------
%% CallStore fun
%% -----------------------------------------------
store_put(DialogID, CallDomains, Forks, Data) when is_map(Forks) ->
    % first add new calls with one fork in the store, by domain
    RestForks = store_put_call(DialogID, CallDomains, Forks, Data),
    % then for each fork make a request to the domain
    F1 = fun(D, List, ok) ->
                 F2 = fun(Fork) -> call_sipstore(D, {put_list, [DialogID, maps:get(?KEYFork, Fork), format_data(Fork), ?TIMEOUTSec]}) end,
                 lists:foreach(F2, List)
         end,
    maps:fold(F1, ok, RestForks).

%% @private
store_put_call(DialogID, CallDomains, Forks, Data) when is_map(Forks) ->
    FD = fun(D, Rest) ->
                 [First|Tail] = maps:get(D, Rest),
                 call_sipstore(D, {put_list, [DialogID, ?TYPE, format_data(Data), maps:get(?KEYFork, First), format_data(First), ?TIMEOUTSec]}),
                 case Tail of [] -> maps:remove(D, Rest); _ -> maps:put(D, Tail, Rest) end
         end,
    lists:foldl(FD, Forks, CallDomains).

%% @private
format_data(Map) ->
    F = fun(_, #uri{}=Uri) -> ?U:unparse_uri(?U:clear_uri(Uri)); (_, V) -> V end,
    maps:map(F, Map).

%% ---------------------------
store_delete(DialogID, Domains, ToDelForkInfo) when is_map(ToDelForkInfo) ->
    store_delete_calls(DialogID, Domains),
    store_delete_forks(DialogID, ToDelForkInfo).

%% @private
store_delete_calls(DialogID, Domains) ->
    F = fun(Domain) ->
                % do not try to merge calls because domain can be different
                ?ENVCALL:cast_callstore(Domain, {del_list, [DialogID]})
        end,
    lists:foreach(F, Domains).

%% @private
store_delete_forks(DialogID, ToDelForkInfo) ->
    F = fun(Domain, List, ok) ->
                Keys = lists:map(fun(Fork) -> {DialogID, maps:get(?KEYFork, Fork)} end, List),
                % do not try to merge keys because domain can be different
                ?ENVCALL:cast_callstore(Domain, {del_list, Keys})
        end,
    maps:fold(F, ok, ToDelForkInfo).


%% ---------------------------
store_nochange(DialogID, Active) when is_map(Active) ->
    F1 = fun(D, List, ok) ->
                 F2 = fun(Fork) -> call_sipstore(D, {put_list, [DialogID, maps:get(?KEYFork, Fork), ?TIMEOUTSec]}) end,
                 lists:foreach(F2, List)
         end,
    maps:fold(F1, ok, Active).

%% ---------------------------
call_sipstore(Domain, Request) ->
    case ?ENVCALL:call_callstore(Domain, Request, undefined) of
        ok -> ok;
        true -> ok;
        Other -> ?LOG("~120p -- call_sipstore: ~120p (~120p) -> ~120p",[?MODULE, Domain, Request, Other])
    end,
    ok.

%% -----------------------------------------------
%% ETS fun
%% -----------------------------------------------
ets_put(#stat_dialog{forks=[]}=StatDlg, State) ->
    ets_del(StatDlg, State);
%%
ets_put(#stat_dialog{forks=Forks}=StatDlg, State) when is_list(Forks) ->
    ForksMap = zip_by_domain(Forks, #{}),
    ets_put(StatDlg#stat_dialog{forks=ForksMap}, State);
%%
ets_put(#stat_dialog{dialogid=DialogID,forks=Forks,timerref=TimerRef}=StatDlg, #stat_event_s{ets=ETS,pid=Pid,ref=Ref}=State)
  when is_map(Forks) ->
    cancel_timer(TimerRef),
    case maps:size(Forks) of
        0 -> ets_del(StatDlg, State);
        _ ->
            NewTimerRef = erlang:send_after(?TIMEOUT, Pid, {prolongation, DialogID, Ref}),
            ets:insert(ETS, StatDlg#stat_dialog{timerref=NewTimerRef})
    end.

%% ---------------------------
ets_get(DialogID, #stat_event_s{ets=ETS}) ->
    case ets:lookup(ETS,DialogID) of
        [#stat_dialog{}=StatDlg|_] -> StatDlg;
        [] -> undefined
    end.

%% ---------------------------
ets_del(#stat_dialog{dialogid=DialogID, timerref=TimerRef}=_StatDlg, #stat_event_s{ets=ETS}) ->
    cancel_timer(TimerRef),
    ets:delete(ETS, DialogID).

%% ===================================================================
%% EUnit tests
%% ===================================================================

-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").

include_environment() ->
    {ok,CWD} = file:get_cwd(),
    RPath = filename:dirname(filename:dirname(CWD)),
    Path = filename:join([RPath, "era_env", "ebin"]),
    code:add_patha(Path).

forks_analyze_test_() ->
    D1 = <<"x.rootdomain.ru">>,
    D2 = <<"test.rootdomain.ru">>,
    UriR = #uri{domain = D1},
    UriL = #uri{domain = D2},
    Fork1 = #{<<"callid">> => <<"111">>, <<"side">> => a, <<"remoteuri">> => UriR, <<"localuri">> => UriL},
    Fork2 = #{<<"callid">> => <<"222">>, <<"side">> => b, <<"remoteuri">> => UriR, <<"localuri">> => UriL},
    Fork3 = #{<<"callid">> => <<"333">>, <<"side">> => a, <<"remoteuri">> => UriR, <<"localuri">> => UriL},
    Fork4 = #{<<"callid">> => <<"444">>, <<"side">> => b, <<"remoteuri">> => UriR, <<"localuri">> => UriL},

    {"forks analyze test",
     {setup, fun include_environment/0,
      [
       ?_assertEqual({#{D1 => [Fork1], D2 => [Fork2]}, #{}}, forks_analyze([Fork1,Fork2], #{})),
       ?_assertEqual({#{D1 => [Fork3,Fork1], D2 => [Fork4,Fork2]}, #{}}, forks_analyze([Fork1,Fork2,Fork3,Fork4], #{})),
       ?_assertEqual({#{}, #{D1 => [Fork3,Fork1], D2 => [Fork4,Fork2]}}, forks_analyze([], #{D1 => [Fork3,Fork1], D2 => [Fork4,Fork2]})),
       ?_assertEqual({#{D1 => [Fork1], D2 => [Fork2]}, #{D1 => [Fork3], D2 => [Fork4]}}, forks_analyze([Fork1,Fork2], #{D1 => [Fork3], D2 => [Fork4]})),
       ?_assertEqual({#{D1 => [Fork1], D2 => [Fork2]}, #{D1 => [Fork3], D2 => [Fork4]}}, forks_analyze([Fork1,Fork2], #{D1 => [Fork3,Fork1], D2 => [Fork4,Fork2]})),
       ?_assertEqual({#{D1 => [Fork1], D2 => [Fork2]}, #{}}, forks_analyze([Fork1,Fork2], #{D1 => [Fork1], D2 => [Fork2]})),
       ?_assertEqual({#{D1 => [Fork1], D2 => [Fork4,Fork2]}, #{D1 => [Fork3]}}, forks_analyze([Fork1,Fork2,Fork4], #{D1 => [Fork3,Fork1], D2 => [Fork2]}))
      ]}}.

-endif.
