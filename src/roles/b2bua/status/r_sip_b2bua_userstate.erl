%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Collects user busy states reservations.
%%%      Only dialog process can move user to any busy state. If it falls down, then all its reservation should be cleared.
%%%        This gen_server monitors dialog processes and collects busy states (according to AOR and Local Tag).
%%%      When user free, it's local tag removes from dialog.
%%%        When last user tag is removed then User totally removed from dialog and frees user on upper storage/
%%%        When last user removed from dialog, srv demonitors it's pid.
%%% @todo

-module(r_sip_b2bua_userstate).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([start_link/1,
         usr_busy/2,
         usr_early/2,
         usr_free/2,
         find_pickup_dialog/1,
         find_dialog_count/1,
         find_busy_dialogs/1]).

-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_mgc.hrl").

-record(usrstatemon, {
    ets,
    cache
  }).

-define(CacheCheckUserSec, 30).
-define(StoreTimeoutSec, 300).
-define(RefreshTimeout, 280000).

-define(FREE, 'free').
-define(EARLY, 'early').
-define(BUSY, 'busy').

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    gen_server:start_link({local,?MODULE}, ?MODULE, Opts, []).

% -----------------
% send event to statestore about usr free by Dialog
%
usr_free(Uri, {DialogId, DialogPid, LTag}) ->
    gen_server:cast(?MODULE, {usr, ?FREE, [Uri, LTag, DialogId, DialogPid]}).

% -----------------
% send event to statestore about usr busy by Dialog
%
usr_busy(Uri, {DialogId, DialogPid, LTag}) ->
    % filter by user account (check account in calling proc, but using gen_server's cache for economy)
    case check_user_in_cache(Uri) of
        true -> gen_server:cast(?MODULE, {usr, ?BUSY, [Uri, LTag, DialogId, DialogPid]});
        false -> ok
    end.

% -----------------
% send event to statestore about usr early by Dialog
%
usr_early(Uri, {DialogId, DialogPid, LTag}) ->
    % filter by user account (check account in calling proc, but using gen_server's cache for economy)
    case check_user_in_cache(Uri) of
        true -> gen_server:cast(?MODULE, {usr, ?EARLY, [Uri, LTag, DialogId, DialogPid]});
        false -> ok
    end.

% -----------------
% make request to cluster store to find first early state owner for defined AOR (for pickup function)
%
find_pickup_dialog({_,_,Domain}=AOR) ->
    FunSelect = fun([]) -> not_found;
                   ([{_Key,_XInfo}|_]=Items) ->
                        [{Key,_}|_] = lists:keysort(2, Items),
                        Key
                end,
    case ?U:call_states_store(Domain, {get_selected_state, [AOR, ?EARLY]}, 3000, undefined) of
        undefined -> {error, undefined};
        Items when is_list(Items) ->
            case FunSelect(Items) of
                not_found -> {error, not_found};
                {_,SrvIdx,DialogId,_,_} -> {ok, {SrvIdx,DialogId}}
            end end.

% -----------------
% make request to cluster store to find early&busy dialogs (#65 count limit of trunks function)
%
find_dialog_count({_,_,Domain}=AOR) ->
    case ?U:call_states_store(Domain, {get_selected_state, [AOR, [?EARLY,?BUSY]]}, 3000, undefined) of
        undefined -> 0;
        Items when is_list(Items) ->
            length(lists:filtermap(fun({{"B2B",SrvIdx,DialogId,_,_},_}) -> {true,{SrvIdx,DialogId}}; (_) -> false end, Items))
    end.

% -----------------
% make_request to cluster store to find busy dialogs
%
find_busy_dialogs({_,_,Domain}=AOR) ->
    case ?U:call_states_store(Domain, {get_selected_state, [AOR, ?BUSY]}, 3000, undefined) of
        undefined -> {error, undefined};
        Items when is_list(Items) ->
            lists:filtermap(fun({{"B2B",SrvIdx,DialogId,_,Tag},_}) -> {true,{SrvIdx,DialogId,Tag}}; (_) -> false end, Items)
    end.

%% ====================================================================
%% Callback functions
%% ====================================================================


%% ------------------------------------------------------------
%% Init
%% ------------------------------------------------------------
init(Opts) ->
    [ETS] = ?EU:extract_required_props([ets], Opts),
    State = #usrstatemon{ets=ETS,
                         cache=ets:new(cache,[set])},
    Self = self(),
    ?MONITOR:start_monitor(self(), "Sip usrstate", [fun(StopReason) -> ?OUT("Process ~p terminated (usr_state_monitor): ~120p", [Self, StopReason]) end]),
    restart_monitor(ETS),
    ?LOG("UsrStateMonitor Srv. inited (~140p)", [Opts]),
    {ok, State}.

%% ------------------------------------------------------------
%% Call
%% ------------------------------------------------------------

%% -----------------------
%% returns actual stored cached value of UKey is real cluster user or not, or undefined if unknown or cache-timeout
handle_call({check_user_in_cache, UKey}, _From, State) ->
    #usrstatemon{cache=Cache}=State,
    Now = ?EU:current_gregsecond(),
    Reply = case ets:lookup(Cache, UKey) of
                [{_, #{exp:=Exp, res:=Res}}] when Exp > Now -> Res;
                _ -> undefined
            end,
    {reply, Reply, State};

%% -----------------------
%% print state
handle_call({print}, _From, State) ->
    ?OUT("UsrStateMonitor. State ~p: ~140p", [self(), State]),
    {reply, ok, State};

%% -----------------------
%% returns count of callid-dialog pairs linked
handle_call({count}, _From, State) ->
    #usrstatemon{ets=ETS}=State,
    Size = ets:info(ETS, size),
    ?LOG("UsrStateMonitor. Count of items: ~140p", [Size]),
    {reply, Size, State};

%% -----------------------
%% other
handle_call(Request, _From, State) ->
    ?OUT("UsrStateMonitor. handle_cast(~140p)", [Request]),
    Reply = ok,
    {reply, Reply, State}.

%% ------------------------------------------------------------
%% Cast
%% ------------------------------------------------------------

%% -----------------------
%% when usr state changed
handle_cast({set_user_in_cache, UKey, Val}, #usrstatemon{cache=Cache}=State) ->
    Now = ?EU:current_gregsecond(),
    CacheMap = #{res => Val,
                 exp => Now + ?CacheCheckUserSec},
    CacheMap1 = case Val of
                    true -> CacheMap;
                    false -> CacheMap#{timerref => erlang:send_after(?CacheCheckUserSec * 1000, self(), {timeout_no_user_cache, UKey})}
                end,
    ets:insert(Cache, {UKey, CacheMap1}),
    {noreply, State};

%% -----------------------
%% when usr state changed
handle_cast({usr, ?FREE, Arg}, State) ->
    del_usr_state(Arg, State),
    {noreply, State};
%%
handle_cast({usr, UState, Arg}, State) ->
    add_usr_state(UState, Arg, State),
    {noreply, State};

%% -----------------------
%% restart
handle_cast({stop}, State) ->
    ?LOG(" UsrStateMonitor. Restart", []),
    {stop, normal, State};

%% -----------------------
%% other
handle_cast(Request, State) ->
    ?OUT(" UsrStateMonitor. handle_cast(~140p)", [Request]),
    {noreply, State}.

%% ------------------------------------------------------------
%% Info
%% ------------------------------------------------------------

%% when proc is down
handle_info({'DOWN', Ref, process, Pid, _Result}, State) ->
    ?LOG("UsrStateMonitor. Dlg process DOWN: ~p", [Pid]),
    #usrstatemon{ets=ETS}=State,
    clear_on_down(Pid, Ref, ETS),
    {noreply, State};

%% when time to refresh state
handle_info({'refresh_state_to_store', Pid, AOR, LTag}, State) ->
    {_,_,D}=AOR,
    #usrstatemon{ets=ETS}=State,
    case ets:lookup(ETS, Pid) of
        [] -> ok;
        [{Pid, #{id:=DialogId, aors:=AORs}=Map}] ->
            case lists:keyfind(AOR, 1, AORs) of
                false -> ok;
                {_,Tags} ->
                    case lists:keyfind(LTag,1,Tags) of
                        false -> ok;
                        {_,#{state:=TagState,xinfo:=XInfo}=TagData} ->
                            % refresh
                            send_to_store(D, {TagState,AOR,LTag,DialogId,?StoreTimeoutSec,XInfo}),
                            % new timer
                            TagData1 = TagData#{refreshref:=erlang:send_after(?RefreshTimeout, self(), {'refresh_state_to_store',Pid,AOR,LTag})},
                            Tags1 = lists:keystore(LTag,1,Tags,{LTag,TagData1}),
                            AORs1 = lists:keystore(AOR,1,AORs,{AOR,Tags1}),
                            ets:insert(ETS, {Pid, Map#{aors:=AORs1}})
                    end
            end
    end,
    {noreply, State};

%% cached no_user timeout
handle_info({timeout_no_user_cache, UKey}, #usrstatemon{cache=Cache}=State) ->
    ets:delete(Cache, UKey),
    {noreply, State};

%% other
handle_info(Info, State) ->
    ?LOG(" UsrStateMonitor. handle_info(~140p)", [Info]),
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -----------------------
%% when usr free
%%
del_usr_state([Uri, LTag, DialogId, DialogPid], State) ->
    #usrstatemon{ets=ETS}=State,
    #uri{scheme=S,user=U,domain=D}=Uri,
    AOR = {S,U,D},
    case ets:lookup(ETS, DialogPid) of
        [] -> ok;
        [{_,#{ref:=Ref, aors:=AORs}=Map}] ->
            case lists:keyfind(AOR, 1, AORs) of
                false -> ok;
                {_,Tags} ->
                    case lists:keyfind(LTag,1,Tags) of
                        false -> ok;
                        {_,TagData} ->
                            case TagData of
                                #{refreshref:=TimerRef} when is_reference(TimerRef) -> erlang:cancel_timer(TimerRef);
                                _ -> ok
                            end,
                            send_to_store(D, {?FREE,AOR,LTag,DialogId}),
                            AORs1 = case lists:keydelete(LTag,1,Tags) of
                                        [] -> lists:keydelete(AOR, 1, AORs);
                                        Tags1 -> lists:keystore(AOR, 1, AORs, {AOR,Tags1})
                                    end,
                            case AORs1 of
                                [] ->
                                    % DEMONITOR
                                    erlang:demonitor(Ref),
                                    ets:delete(ETS, DialogPid);
                                _ ->
                                    ets:insert(ETS, {DialogPid, Map#{aors:=AORs1}})
                            end
                    end
            end
    end.

%% ------------------------
%% when usr busy
%%
add_usr_state(UsrState, [Uri, LTag, DialogId, DialogPid], State) ->
    #usrstatemon{ets=ETS}=State,
    #uri{scheme=S,user=U,domain=D}=Uri,
    AOR = {S,U,D},
    % here could be check_user, but economy obliges to make check in calling proc, not in gen_server
    #{aors:=AORs}=Map1 =
           case ets:lookup(ETS, DialogPid) of
               [] ->
                   % MONITOR
                   #{id => DialogId,
                     pid => DialogPid,
                     self => self(),
                     ref => erlang:monitor(process, DialogPid),
                     aors => []};
               [{_,Map}] -> Map
           end,
    AORs1 = case lists:keyfind(AOR, 1, AORs) of
                false ->
                    XInfo = #{ts => ?EU:utcdatetime_ms(),
                              dialogid => DialogId},
                    send_to_store(D, {UsrState,AOR,LTag,DialogId,?StoreTimeoutSec,XInfo}), % 3*3600
                    TagData = #{state => UsrState,
                                refreshref => erlang:send_after(?RefreshTimeout, self(), {'refresh_state_to_store',DialogPid,AOR,LTag}),
                                xinfo => XInfo},
                    [{AOR, [{LTag,TagData}]} | AORs];
                {_,Tags} ->
                    case lists:keyfind(LTag,1,Tags) of
                        false ->
                            XInfo = #{ts => ?EU:utcdatetime_ms(),
                                      dialogid => DialogId},
                            send_to_store(D, {UsrState,AOR,LTag,DialogId,?StoreTimeoutSec,XInfo}), % 3*3600
                            TagData = #{state => UsrState,
                                        refreshref => erlang:send_after(?RefreshTimeout, self(), {'refresh_state_to_store',DialogPid,AOR,LTag}),
                                        xinfo => XInfo},
                            lists:keystore(AOR, 1, AORs, {AOR, [{LTag,TagData}|Tags]});
                        {_,#{state:=PrevState,refreshref:=TimerRef,xinfo:=XInfo}=TagData} when PrevState /= UsrState ->
                            erlang:cancel_timer(TimerRef),
                            send_to_store(D, {UsrState,AOR,LTag,DialogId,?StoreTimeoutSec,XInfo}), % 3*3600
                            TagData1 = TagData#{state := UsrState,
                                                refreshref := erlang:send_after(?RefreshTimeout, self(), {'refresh_state_to_store',DialogPid,AOR,LTag})},
                            Tags1 = lists:keystore(LTag, 1, Tags, {LTag,TagData1}),
                            lists:keystore(AOR, 1, AORs, {AOR, Tags1});
                        _ ->
                            AORs
                    end
            end,
    Map2 = Map1#{aors:=AORs1},
    ets:insert(ETS, {DialogPid, Map2}).

%% ====================================================================
%% Internal functions
%% ====================================================================

% ---
restart_monitor(ETS) ->
    Self = self(),
    DomainETS = ets:new(temp,[]),
    F = fun({Pid,#{self:=Prev,aors:=AORs}=Map}, null) ->
                case process_info(Pid, status) of
                    undefined ->
                        % CLEAR
                        clear_on_down(Pid,undefined,ETS);
                    _ ->
                        Faors = fun({{_S,_U,Domain}=AOR,LTags}, Acc) ->
                                    case ets:lookup(DomainETS, Domain) of
                                        [] -> ets:insert(DomainETS, {Domain,Prev});
                                        _ -> ok
                                    end,
                                    FunTimer = fun({LTag,#{refreshref:=TimerRef}=TagData}) when is_reference(TimerRef) ->
                                                       erlang:cancel_timer(TimerRef),
                                                       {LTag,TagData#{refreshref:=erlang:send_after(?RefreshTimeout, self(), {refresh_state_to_store,Pid,AOR,LTag})}};
                                                  ({LTag,TagData}) -> {LTag,TagData}
                                               end,
                                    [{AOR, lists:map(FunTimer, LTags)} | Acc]
                                end,
                        AORs1 = lists:foldl(Faors, [], AORs),
                        ets:insert(ETS, {Pid, Map#{self:=Self,
                                                   ref:= erlang:monitor(process,Pid),
                                                   aors:=AORs1}}),
                        null
                end end,
    ets:foldl(F, null, ETS),
    F2 = fun({Domain,PrevPid},Acc) ->
                 % CHANGE PID
                 send_to_store(Domain, {'change_owner_pid',PrevPid,Self}),
                 Acc
         end,
    ets:foldl(F2, ok, DomainETS).

% ---------------
clear_on_down(Pid, _Ref, ETS) ->
    case ets:lookup(ETS, Pid) of
        [] -> ok;
        [{Pid, #{id:=DialogId, aors:=AORs}}] ->
            ets:delete(ETS, Pid),
            lists:foreach(fun({{_S,_U,D}=AOR,LTagList}) ->
                                lists:foreach(fun({LTag,#{refreshref:=TimerRef}}) ->
                                                      erlang:cancel_timer(TimerRef),
                                                      send_to_store(D, {?FREE,AOR,LTag,DialogId});
                                                 ({LTag,_}) ->
                                                      send_to_store(D, {?FREE,AOR,LTag,DialogId})
                                              end, LTagList)
                          end, AORs)
    end,
    ok.

% ---------------
send_to_store(Domain, {'change_owner_pid',PrevPid,Self}) ->
    %?OUT("UsrState Pid changed", []),
    Event = fun() -> {change_owner_pid, [PrevPid,Self]} end,
    ?U:cast_states_store(Domain, Event, {worker,Self});

send_to_store(Domain, {?FREE,{S,User,D}=_AOR,LTag,DialogId}) ->
    %?OUT("STATE of ~p ---> free", [User]),
    Owner = self(),
    FunMsg = fun(U) -> fun() -> {del_link, [{S,U,D}, build_key(DialogId,U,LTag), Owner]} end end,
    ?U:cast_states_store(Domain, FunMsg(User), {worker,LTag});

send_to_store(Domain, {ObjState,{S,User,D}=_AOR,LTag,DialogId,LimitTTL,XInfo}) ->
    %?OUT("STATE of ~p ---> busy", [User]),
    Owner = self(),
    FunMsg = fun(U) -> fun() -> {put, [{S,U,D}, build_key(DialogId,U,LTag), ObjState, Owner, LimitTTL, XInfo]} end end,
    ?U:cast_states_store(Domain, FunMsg(User), {worker,LTag}).

% ---
build_key(DialogId,User,LTag) ->
    {"B2B",?CFG:get_my_sipserver_index(),DialogId,User,LTag}.

% ---
% this func is executing in external procs.
% checks if defined AOR is real user of cluster (not ext number or service).
% uses current gen_server's cache to econom calls to outer service (cache for 30 sec).
check_user_in_cache(#uri{user=U,domain=D}) ->
    UKey = {U,D},
    case gen_server:call(?MODULE, {check_user_in_cache, UKey}) of
        true -> true;
        false -> false;
        undefined ->
            Res = ?ACCOUNTS:check_sipuser_exists(U, D),
            gen_server:cast(?MODULE, {set_user_in_cache, UKey, Res}),
            Res
    end.

