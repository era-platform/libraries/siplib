%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 24.02.2022
%%% @doc Callstats worker's supv

-module(r_sip_b2bua_callstats_worker_supv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([start_link/1]).
-export([init/1]).
-export([get_worker_count/0, get_worker_name/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("r_sip.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Args) ->
    supervisor:start_link(?MODULE, Args).

get_worker_count() -> 16.

get_worker_name(0) -> cs_worker_0;
get_worker_name(1) -> cs_worker_1;
get_worker_name(2) -> cs_worker_2;
get_worker_name(3) -> cs_worker_3;
get_worker_name(4) -> cs_worker_4;
get_worker_name(5) -> cs_worker_5;
get_worker_name(6) -> cs_worker_6;
get_worker_name(7) -> cs_worker_7;
get_worker_name(8) -> cs_worker_8;
get_worker_name(9) -> cs_worker_9;
get_worker_name(10) -> cs_worker_10;
get_worker_name(11) -> cs_worker_11;
get_worker_name(12) -> cs_worker_12;
get_worker_name(13) -> cs_worker_13;
get_worker_name(14) -> cs_worker_14;
get_worker_name(15) -> cs_worker_15;
get_worker_name(A) -> ?EU:to_atom_new("cs_worker_" ++ ?EU:to_list(A)).


%% ====================================================================
%% Callbacks
%% ====================================================================

init(Args) ->
    ChildSpec = prepare_child_specs(lists:seq(0,get_worker_count()-1), []),
    ?LOG('$info', " B2B callstats worker supervisor inited (~120tp)", [Args]),
    {ok, {{one_for_one, 10, 2}, ChildSpec}}.

%% ====================================================================
%% Internal functions
%% ====================================================================

prepare_child_specs([], ChildSpecs) -> lists:reverse(ChildSpecs);
prepare_child_specs([F|Rest], ChildSpecs) ->
    Name = get_worker_name(F),
    Spec = {Name, {?WORKER, start_link, [Name]}, permanent, 1000, worker, [?WORKER]},
    prepare_child_specs(Rest, [Spec|ChildSpecs]).


