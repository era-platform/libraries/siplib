%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Handles BLF subscriptions and send NOTIFY when state changes.
%%%         Powered by StateStore role.

-module(r_sip_subscriptions).

-export([do_notify/2,
         subscribe/3]).

%-compile([export_all,nowarn_export_all]).

%% ==========================================================================
%% Types
%% ==========================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_subscr.hrl").

% -include("../include/r_nksip.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

% ------------
% SUBSCRIBE from device
% ------------
subscribe(Req, Call, IsInitial) ->
    ?SUBSCRIBE:subscribe(Req, Call, IsInitial).

% ----
% NOTIFY from storage
% ----
do_notify(#{}=Data, #{}=NotifyArg) ->
    ?NOTIFY:notify(Data,NotifyArg).


%% ====================================================================
%% Internal functions
%% ====================================================================
