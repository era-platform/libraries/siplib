%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 30.12.2016
%%% @doc

-module(r_sip_subscriptions_notify).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([notify/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_headers.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------
%% facade to send notify
%% ------------------------
notify(Data, NotifyArg) ->
    do_notify(Data, NotifyArg).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ------------------------
%% internal prepare and send notify
%% ------------------------
do_notify(#{}=Data, #{}=NotifyArg) ->
    ObjState = maps:get(state,NotifyArg),
    %
    %_CallId = maps:get(callid,Data),
    %_User = maps:get(user,Data),
    %?ENV:set_group_leader(),
    %?LOGSIP("--> send_notify to ~p, ~p -> ~p", [_CallId, _User, ObjState]),
    %
    DlgHandle = maps:get(dlghandle,Data),
    Event = maps:get(event,Data),
    Accept = maps:get(accept,Data),
    StartGS = maps:get(startgs,Data),
    Expires = maps:get(expires,Data),
    ToUri = maps:get(touri,Data),
    Contact = maps:get(contact,Data),
    FromDomain = maps:get(fromdomain,Data,undefined), % and value undefined too
    %
    Expires1 = Expires - (?EU:current_gregsecond() - StartGS),
    Vrs = 1, % @todo +1
    Opts = [{contact, Contact},
            {event, Event},
            server],
    Opts1 = case maps:get(initial,NotifyArg,false) of
                true -> [{cseq_num, 1}|Opts];
                _ -> Opts
            end,
    Opts2 = case maps:get(subscription_state, NotifyArg, <<>>) of
                SubState when is_tuple(SubState); is_atom(SubState) ->
                    [{subscription_state, SubState} | Opts1];
                _ ->
                    ContentType = Accept,
                    [{subscription_state, {active, Expires1}},
                     {content_type, build_contenttype(Event, ContentType, Vrs)},
                     %{replace, <<"Content-Encoding">>, <<"UTF-8">>}, % CISCO 9971 respond 400
                     {body, build_body(Event, ContentType, Vrs, ToUri, ObjState)}
                    |Opts1]
            end,
    Opts3 = case FromDomain of
                undefined -> Opts2;
                _ -> [{replace, ?RcvFromDomHeader, FromDomain} | Opts2]
            end,
    nksip_uac:notify(DlgHandle, [async|Opts3]).

%% ------------------------------
%% notify content-type
%% ------------------------------
build_contenttype(_Event, <<"application/cpim-pidf+xml">>, _Version) -> <<"application/pidf+xml">>;
build_contenttype(_,ContentType,_) -> ContentType.

%% ------------------------------
%% notify body
%% ------------------------------

%% event: dialog
build_body({<<"dialog">>,_}, _ContentType, Vrs, #uri{user=U}=Uri, ObjState) ->
    URI = uri_to_http(Uri),
    Version = ?EU:to_binary(Vrs),
    DState = dialog_state(ObjState),
    Body = <<"<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "\r\n",
             "<dialog-info xmlns=\"urn:ietf:params:xml:ns:dialog-info\"",
                          " version=\"", Version/bitstring,"\"",
                           " state=\"full\"",
                         " entity=\"", URI/bitstring, "\">", "\r\n",
                 "<dialog id=\"", U/bitstring, "\">",
                     "<state>", DState/bitstring, "</state>",
                 "</dialog>", %"\r\n",
             "</dialog-info>",
             "\r\n">>,
    % application/dialog-info+xml
    Body;

%% event: presence (CISCO)
build_body({<<"presence">>,_}, <<"application/cpim-pidf+xml">>, _Vrs, #uri{user=U}=Uri, ObjState) ->
    URI = uri_to_http(Uri),
    %Version = ?EU:to_binary(Vrs),
    {SipState, _ExtState, _PersonState, _NoteState} = presence_state(ObjState),
    %{{Yea,Mon,Day},{Hou,Min,Sec}} = erlang:universaltime(),
    %Time = ?EU:strbin("~4.10.0B-~2.10.0B-~2.10.0BT~2.10.0B:~2.10.0B:~2.10.0BZ", [Yea,Mon,Day,Hou,Min,Sec]),
    Body = <<"<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "\r\n",
             "<presence xmlns='urn:ietf:params:xml:ns:cpim-pidf'",
                      " xmlns:im='urn:ietf:params:xml:ns:cpim-pidf:im'",
                      " entity='pres:", URI/binary, "'",
                      ">", "\r\n",
               "<tuple id='", U/binary, "'>", %"\r\n",
                   "<status>",
                       "<basic>", SipState/binary, "</basic>",
                   "</status>", %"\r\n",
                   "<contact priority=\"1\">", URI/binary, "</contact>",
               "</tuple>", "\r\n",
             "</presence>",
             "\r\n">>,
    % application/pidf+xml
    Body;

build_body({<<"presence">>,_}, _ContentType, Vrs, #uri{user=U}=Uri, ObjState) ->
    URI = uri_to_http(Uri),
    Version = ?EU:to_binary(Vrs),
    {SipState, ExtState, PersonState, _NoteState} = presence_state(ObjState),
    {{Yea,Mon,Day},{Hou,Min,Sec}} = erlang:universaltime(),
    Time = ?EU:strbin("~4.10.0B-~2.10.0B-~2.10.0BT~2.10.0B:~2.10.0B:~2.10.0BZ", [Yea,Mon,Day,Hou,Min,Sec]),
    Body = <<"<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "\r\n",
             "<presence xmlns='urn:ietf:params:xml:ns:pidf'",
                      " xmlns:dm='urn:ietf:params:xml:ns:pidf:data-model'",
                      " xmlns:rpid='urn:ietf:params:xml:ns:pidf:rpid'",
                      " xmlns:c='urn:ietf:params:xml:ns:pidf:cipid'",
                      " entity='pres:", URI/bitstring, "'",
                      " version='", Version/bitstring, "'>", "\r\n",
                 % tuple
                 "<tuple id='t", U/bitstring, "'>", %"\r\n",
                     "<status>",
                         "<basic>", SipState/bitstring, "</basic>",
                         "<ts:state>", ExtState/bitstring, "</ts:state>",
                     "</status>", %"\r\n",
                     "<timestamp>", Time/bitstring, "</timestamp>", %"\r\n",
                 "</tuple>", "\r\n",
                 % person
                 "<dm:person id='p", U/bitstring, "'>",
                     "<rpid:activities>", "<rpid:", PersonState/bitstring, "/>", "</rpid:activities>",
                 "</dm:person>", "\r\n",
                 % note
%                 "<note xml:lang='en'>",
%                     NoteState/bitstring,
%                 "</note>", %"\r\n",
             "</presence>",
             "\r\n">>,
    % application/pidf+xml
    Body;

%% event: message-summary
build_body({<<"message-summary">>,_}, _ContentType, _Vrs, #uri{}=Uri, ObjState) ->
    URI = uri_to_http(Uri),
    {MW,State} = message_summary_state(ObjState),
    Body = <<"Messages-Waiting: ", MW/bitstring, "\r\n",
             "Message-Account: ", URI/bitstring, "\r\n",
             State/bitstring>>,
    Body.

%% ------------------------
%% state of sipuser
%% ------------------------
dialog_state(busy) -> <<"confirmed">>;
dialog_state(calling) -> <<"confirmed">>;
dialog_state(early) -> <<"early">>;
dialog_state(registered) -> <<"terminated">>;
dialog_state(out_of_service) -> <<"unavailable">>;
dialog_state(huntblock) -> dialog_state(registered);
dialog_state(_) -> presence_state(out_of_service).

%% ------------------------
%% state of sipuser
%% ------------------------
presence_state(busy) -> {<<"inuse">>, <<"busy">>, <<"busy">>, <<"Busy">>};
presence_state(calling) -> {<<"inuse">>, <<"busy">>, <<"calling">>, <<"Calling">>};
presence_state(early) -> {<<"inuse">>, <<"busy">>, <<"early">>, <<"Early">>};
presence_state(registered) -> {<<"open">>, <<"reachable">>, <<"idle">>, <<"Idle">>};
presence_state(out_of_service) -> {<<"closed">>, <<"unreachable">>, <<"away">>, <<"Disconnected">>};
presence_state(huntblock) -> presence_state(registered); % RP-892
presence_state(_) -> presence_state(out_of_service).

sub_state(busy) -> <<"onthephone">>;
sub_state(calling) -> <<"onthephone">>;
sub_state(early) -> <<"onthephone">>;
sub_state(registered) -> <<"online">>;
sub_state(out_of_service) -> <<"offline">>;
sub_state(huntblock) -> sub_state(registered);
sub_state(_) -> presence_state(out_of_service).

%% ------------------------
%% state of voice-mailbox
%% ------------------------
message_summary_state({voice,P}) -> mss(<<"Voice-Message">>,P);
message_summary_state({text,P}) -> mss(<<"Text-Message">>,P);
message_summary_state({fax,P}) -> mss(<<"Fax-Message">>,P);
message_summary_state(_) -> {<<"no">>,<<>>}.

% @private
mss(H, {New,Old})
  when is_integer(New), is_integer(Old) ->
    [BNew,BOld]=lists:map(fun(X)->?EU:to_binary(X) end,[New,Old]),
    {mw(New), <<H/bitstring, ": ", BNew/bitstring, "/", BOld/bitstring>>};
mss(H, {{New,Old},{Urg,NUrg}})
  when is_integer(New), is_integer(Old), is_integer(Urg), is_integer(NUrg) ->
    [BNew,BOld,BUrg,BNUrg]=lists:map(fun(X)->?EU:to_binary(X) end,[New,Old,Urg,NUrg]),
    {mw(New), <<H/bitstring, ": ", BNew/bitstring, "/", BOld/bitstring, " (", BUrg/bitstring, "/", BNUrg/bitstring, ")">>}.
% @private
mw(0) -> <<"no">>;
mw(_) -> <<"yes">>.


%% ------------------------
uri_to_http(#uri{scheme=S,user=U,domain=D,port=P}=_Uri) ->
    string:trim(?U:unparse_uri(#uri{scheme=S,user=U,domain=D,port=P}),both,[$<,$>]).
