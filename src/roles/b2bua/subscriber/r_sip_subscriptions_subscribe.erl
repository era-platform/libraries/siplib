%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 30.12.2016
%%% @doc

-module(r_sip_subscriptions_subscribe).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([subscribe/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../include/r_sip_headers.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

subscribe(#sipmsg{}=Req, #call{}=Call, IsInitial)
  when is_boolean(IsInitial) ->
    case check_subscribe(Req) of
        {ok,AOR,Accept} -> do_subscribe(AOR,Accept,Req,Call,IsInitial);
        R -> R
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

% --------------------
% check subscribe
% --------------------

% @private
% Expires
check_subscribe(Req) ->
    #sipmsg{expires=Expires}=Req,
    case Expires of
        0 -> check_subscribe_1(Req);
        _ when Expires >= 30 -> check_subscribe_1(Req);
        false -> {reply, ?IntervalTooBrief(<<"30">>, "B2B. Interval too brief")}
    end.

% @private
% Event & Accept
% @todo
%  rfc3680, event=reg, ct=application/reginfo+xml @todo
%  rfc4660, event=presence, ct=application/simple-filter+xml, application/pidf+xml
%  rfc4235, event=dialog, ct=application/dialog-info+xml
%  rfc3842, event=message-summary, ct=application/simple-message-summary
check_subscribe_1(Req) ->
    case get_event_accept(Req) of
        undefined ->
            % bad event
            Opts = [user_agent,
                    {replace, <<"Allow-Events">>, <<"presence, dialog, message-summary">>}],
            {reply, {489,Opts}}; % bad event
        {_,undefined} ->
            {reply, {406,[user_agent]}}; % not acceptable here
        {EventType,Accept} ->
            check_subscribe_2(EventType, Accept, Req)
    end.

%% @private
get_event_accept(Req) ->
    case Req#sipmsg.event of
        {EventType,[]} ->
            case lists:keyfind(EventType,1,supported_event_accept()) of
                false -> undefined;
                {_,SupportedA} ->
                    case nksip_request:header(<<"accept">>, Req) of
                        {ok,[_|_]=A} ->
                            A1 = lists:foldl(fun(V,Acc) -> Acc ++ binary:split(V,[<<",">>,<<" ">>,<<"\t">>],[global,trim_all]) end, [], A),
                            lists:foldl(fun(X,{_,undefined}) ->
                                                case lists:member(X,A1) of
                                                    false -> {EventType,undefined};
                                                    true -> {EventType,X}
                                                end;
                                           (_,Acc) -> Acc
                                        end, {EventType,undefined}, SupportedA);
                        _ -> undefined
                    end
            end;
        _ -> undefined
    end.

%% @private
supported_event_accept() ->
    [{<<"dialog">>, [<<"application/dialog-info+xml">>]},
     {<<"presence">>,[<<"application/pidf+xml">>, <<"application/simple-filter+xml">>, <<"application/cpim-pidf+xml">>]},
     {<<"message-summary">>, [<<"application/simple-message-summary">>]}].

%% ------------------------------------------------------------------------------
%% SUBSCRIBE sip:192.168.0.12:5090;transport=tcp SIP/2.0
%% Via: SIP/2.0/TCP 192.168.0.146:5064;branch=z9hG4bK3543217346
%% Route: <sip:r_S@192.168.0.12:5060;transport=tcp;lr>
%% From: "654-T28-146" <sip:654@test.rootdomain.ru>;tag=2188446199
%% To: <sip:19@test.rootdomain.ru>;tag=rB2-002-MZqg
%% Call-ID: 2272296318@192.168.0.146
%% CSeq: 91 SUBSCRIBE
%% Contact: <sip:654@192.168.0.146:5064;transport=TCP>
%% Accept: application/dialog-info+xml
%% Max-Forwards: 70
%% User-Agent: Yealink SIP-T28P 2.72.14.6
%% Expires: 60
%% Event: dialog
%% Content-Length: 0
%% ------------------------------------------------------------------------------
%% SUBSCRIBE sip:357357@test.rootdomain.ru:5060 SIP/2.0
%% Via: SIP/2.0/TCP 192.168.0.146:5066;branch=z9hG4bK42906377
%% From: "SIP1-T28-146" <sip:sip3@test.rootdomain.ru>;tag=2204865631
%% To: <sip:357357@test.rootdomain.ru>
%% Call-ID: 2224948216@192.168.0.146
%% CSeq: 1 SUBSCRIBE
%% Contact: <sip:sip3@192.168.0.146:5066;transport=TCP>
%% Accept: application/simple-message-summary
%% Max-Forwards: 70
%% User-Agent: Yealink SIP-T28P 2.72.14.6
%% Expires: 60
%% Event: message-summary
%% Content-Length: 0
%% ---------
%% NOTIFY sip:alice@alice-phone.example.com SIP/2.0
%% To: <sip:sip3@test.rootdomain.ru>;tag=2204865631
%% From: <sip:357357@test.rootdomain.ru>;tag=4442
%% Date: Mon, 10 Jul 2000 03:55:07 GMT
%% Call-Id: 2224948216@192.168.0.146
%% CSeq: 1 NOTIFY
%% Contact: <sip:357357@test.rootdomain.ru>
%% Event: message-summary
%% Subscription-State: active
%% Content-Type: application/simple-message-summary
%% Content-Length: 99
%%
%% Messages-Waiting: yes
%% Message-Account: sip:357357@test.rootdomain.ru
%% Voice-Message: 2/8 (0/2)
%% ------------------------------------------------------------------------------

%% @private
check_subscribe_2(T, Accept, Req)
  when T==<<"dialog">>;
       T==<<"presence">>;
       T==<<"message-summary">> ->
    case find_destination(route,T,Req) of
        {error,{_,R}} when is_binary(R) ->
            err_subscribe(Req, {terminated, noresource}, {sip,500,?EU:to_list(R)});
        {error,_R} ->
            err_subscribe(Req, {terminated, noresource}, {sip,500,<<"B2B. Unknown reason">>});
        {reply,{SipCode,Reason}} ->
            err_subscribe(Req, {terminated, noresource}, {sip,SipCode,?EU:to_list(Reason)});
        {ok,AOR} -> {ok,AOR,Accept}
    end.

%% -----------------------------
%% routing
%% -----------------------------

%% @private
%% find destination user AOR
%% by route table (cross domain blocked)
find_destination(route,Type,Req) ->
    #sipmsg{from={#uri{domain=FD},_}}=Req,
    case ?B2BUA_ROUTER_SUBSCRIBE:sip_subscribe(Req) of
        {error,_}=Err -> Err;
        {reply,_}=Reply -> Reply;
        {ok,T} when Type==<<"presence">>; Type==<<"dialog">> ->
            % allowed only to internal sipuser
            case T of
                {inside,{aor,[AOR]}} -> {ok,AOR};
                {feature,_} -> {error,{invalid_params,<<"Route destination mismatch (featurecode)">>}};
                _ -> {error,{invalid_params,<<"Route destination mismatch">>}}
            end;
        {ok,T} when Type==<<"message-summary">> ->
            % allowed only to voicemail featurecode
            case T of
                {feature,{<<"voicemail">>,[_,Num,_]}} -> AOR = {msgbox,Num,FD}, {ok,AOR};
                {feature,_} -> {error,{invalid_params,<<"Route destination mismatch (invalid featurecode)">>}};
                {inside,_A} ->
                    {error,{invalid_params,<<"Route destination mismatch (sipuser)">>}};
                _ -> {error,{invalid_params,<<"Route destination mismatch">>}}
            end
    end;
%
%% by direct (cross domain allowed)
find_destination(direct,_Type,Req) ->
    #sipmsg{to={#uri{user=User, domain=Domain}=_ToUri,_}}=Req,
    AOR = {sip,User,Domain},
    case ?ACCOUNTS:check_sipuser_exists(User, Domain) of
        true -> {ok,AOR};
        false ->
            case ?NUMBERPLAN:find_users(User, Domain) of
                undefined -> {reply,?NotFound("B2B. No user@domain found")};
                [] -> {reply,?NotFound("B2B. No user found in domain")};
                _U when is_binary(_U) -> {ok,AOR};
                _L when is_list(_L) -> {ok,AOR}
            end
    end.

%% -----------------------------
%% Subscription denied
%% -----------------------------
err_subscribe(Req, SubscrState, Reason) ->
    % spawn, not worker
    spawn(fun() -> err_subscribe_1(Req, SubscrState, Reason) end),
    noreply.

err_subscribe_1(Req, SubscrState, Reason) ->
    Contact = make_notify_contact(Req),
    % answer
    {ok, ReqHandle} = nksip_request:get_handle(Req),
    nksip_request:reply({200, [{reason, Reason},
                               {contact, Contact},
                               {expires,0},
                               server]}, ReqHandle),
    % notify
    #sipmsg{srv_id=AppId,
            call_id=CallId,
            event=Event}=Req,
    DlgHandle = nksip_dialog_lib:get_handle(#dialog{id=nksip_dialog_lib:make_id(uas, Req), srv_id=AppId, call_id=CallId}),
    Opts = [%{reason, Reason},
            {cseq_num, 1},
            {contact, Contact},
            {event, Event},
            {subscription_state, SubscrState},
            server],
    nksip_uac:notify(DlgHandle, [async|Opts]).

%% -----------------------------
%% subscription allowed
%% -----------------------------
do_subscribe({_,U,D}=AOR, Accept, Req, _Call, IsInitial) ->
    #sipmsg{srv_id=AppId,
            to={ToUri,_},
            from={FromUri,_},
            contacts=[#uri{domain=CDomain}|_],
            expires=TTL0,
            call_id=CallId,
            event=Event}=Req,
    TTL = min(max(TTL0,60),7200),
    Key = <<(?U:unparse_uri(?U:clear_uri(FromUri#uri{disp= <<>>})))/binary,";callid=",CallId/binary,";addr=",CDomain/binary,";b2b=",(?EU:node_name())/binary>>, % 245
    DlgHandle = nksip_dialog_lib:get_handle(#dialog{id=nksip_dialog_lib:make_id(uas, Req), srv_id=AppId, call_id=CallId}),
    LContact = make_notify_contact(Req),
    FromDomain = get_from_domain(Req), % cisco
    Info = #{node => node(),
             site => ?CFG:get_current_site(),
             m => ?SUBSCRIPTIONS,
             f => do_notify,
             a => [#{user => U, % useless
                     callid => CallId, % useless
                     dlghandle => DlgHandle,
                     event => Event,
                     accept => Accept,
                     startgs => ?EU:current_gregsecond(),
                     expires => TTL,
                     touri => ToUri,
                     contact => LContact,
                     fromdomain => FromDomain}],
             initial => IsInitial},
    Msg = fun() -> {put, [AOR, [{Key,Info}], TTL]} end,
    ?U:cast_subscription_store(D, Msg),
    {reply, {202, [{contact,LContact},
                   {expires,TTL},
                   {add,<<"Allow-Events">>,?EU:join_binary([Ev || {Ev,_} <- supported_event_accept()],<<", ">>)},
                   server]}}.

%% @private
get_from_domain(Req) ->
    case nksip_request:header(?RcvFromDomHeaderLow, Req) of
        {ok,[FromDomain|_]} -> FromDomain;
        _ -> undefined
    end.

%% @private
make_notify_contact(#sipmsg{nkport=#nkport{local_ip={0,0,0,0}}}) ->
    Domain = ?CFG:get_my_sipserver_addr(), % @localcontact (cfg=default!)
    Port = ?U:get_local_port(),
    #uri{scheme=sip, domain=Domain, port=Port, opts=?CFG:get_transport_opts(), headers=[], ext_opts=[]};
%
make_notify_contact(#sipmsg{nkport=#nkport{local_ip=Ip}}) ->
    Domain = ?EU:to_binary(inet:ntoa(Ip)), % @localcontact? (cfg=default!)
    Port = ?U:get_local_port(),
      #uri{scheme=sip, domain=Domain, port=Port, opts=?CFG:get_transport_opts(), headers=[], ext_opts=[]}.
