%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>, Peter Bukashin <tbotc@yandex.ru>
%%% @doc

-module(r_sip_conf_event_handler).

%% ==========================================================================
%% Defines
%% ==========================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_conf.hrl").
-include("../include/r_sip_mgc.hrl").

-include("../_build/default/lib/nksip/include/nksip.hrl").

-define(ConfPattern, <<"confevents">>).
-define(CommonType,common).

%% ====================================================================
%% API functions
%% ====================================================================

-export([prepare_event/2,
         prepare_event/3,
         prepare_event/4]).

%% ====================================================================
%% Public functions
%% ====================================================================

%%-----------------------------------------------
%% Prepare Event common part
%%-----------------------------------------------

prepare_event(EventType,State) ->
    Event = make_event(EventType,{State}),
    prepare_event_header(EventType,Event).

prepare_event(EventType,XState,State) ->
    Event = make_event(EventType,{XState,State}),
    prepare_event_header(EventType,Event).

prepare_event(EventType,Fork,XState,State) ->
    Event = make_event(EventType,{Fork,XState,State}),
    prepare_event_header(EventType,Event).

%% @private
prepare_event_header(EventType,Event) ->
    Pattern = {?ConfPattern,?CommonType,undefined},
    EventHead = #{<<"class">> => ?ConfPattern,
                  <<"type">> => ?EU:to_binary(EventType),
                  <<"data">> => Event,
                  <<"eventts">> => ?EU:timestamp()},
    From = self(),
    {Pattern,EventHead,From}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ----------------------------------
make_event(EventType,Params) ->
    Meta = ?EVENTMETA:get_meta(EventType),
    F = fun({Key,StateType,ValueIdx},Acc) ->
                case extract_value(Params, StateType,ValueIdx) of
                    {ok, Value} -> Acc#{Key => Value};
                    _ ->
                        %?OUT("~p. extract_value for (~120p) K:~120p; StateType:~120p; ValueIdx:~120p ERROR",[?MODULE,EventType,Key,StateType,ValueIdx]),
                        Acc
                end;
           (_V,_Acc) -> ?OUT("ERROR: function clause ~p", [_V]), _Acc
        end,
    lists:foldl(F,#{},Meta).

 % conf_init ==> ConfId::binary(), Room::aor(), ITS::integer()
 % conf_dead ==> ConfId::binary(), Room::aor(), ITS::integer()
extract_value({{ConfId,_ConfIdHash}, {_S,_U,_D}, _ITS},state,confid) -> {ok,ConfId};
extract_value({{_ConfId,ConfIdHash}, {_S,_U,_D}, _ITS},state,confidhash) -> {ok,ConfIdHash};
extract_value({{_ConfId,_ConfIdHash}, {_S,Room,_D}, _ITS},state,room) -> {ok,Room};
extract_value({{_ConfId,_ConfIdHash}, {_S,_U,ConfDomain}, _ITS},state,confdomain) -> {ok,ConfDomain};
extract_value({{_ConfId,_ConfIdHash}, {_S,_U,_D}, ITS},state,invitets) -> {ok,ITS};
 % conf_start ==> State::#state_conf{}
 % conf_stop ==> ConfId::binary(), Room::aor(), State::#state_conf{}
extract_value({#state_conf{confid=ConfId}},state,confid) -> {ok,ConfId};
extract_value({#state_conf{confidhash=ConfIdHash}},state,confidhash) -> {ok,ConfIdHash};
extract_value({#state_conf{confnum=ConfNum}},state,confnum) -> {ok,ConfNum};
extract_value({#state_conf{invite_ts=ITS}},state,invitets) -> {ok,ITS};
%% extract_value({#state_conf{media=Media}},media,Key) -> {ok,get_media(Media, Key)};
extract_value({#state_conf{media=#media_conf{mgc=MGC}}}, media, mgc) -> {ok, MGC};
extract_value({#state_conf{media=#media_conf{rec_links=RecLinks}}}, media, reclinks) -> {ok, RecLinks};
extract_value({#state_conf{media=#media_conf{is_rec=IsRec}}}, media, isrec) -> {ok, IsRec};
extract_value({#state_conf{participants=Participants}},participants,Key) -> {ok,get_participants(Participants, Key)};
extract_value({#state_conf{room={_,Room,_}}},state,room) -> {ok,Room};
extract_value({#state_conf{room={_,_,ConfDomain}}},state,confdomain) -> {ok,ConfDomain};
extract_value({#state_conf{stopreason=StopReason}},state,stoptype) -> {ok,maps:get(type,StopReason, <<>>)};
extract_value({#state_conf{stopreason=StopReason}},state,stopreason) -> {ok,maps:get(reason,StopReason, <<>>)};
extract_value({#state_conf{stopreason=StopReason}},state,stopoperations) -> {ok,maps:get(operations,StopReason, <<>>)};
extract_value({#state_conf{stopreason=StopReason}},state,stopcallid) -> {ok,maps:get(callid,StopReason,<<>>)};

 % call_attach ==> Side::#side{}, State::#state_conf{}
 % call_detach ==> Side::#side{}, State::#state_conf{}
extract_value({#side{},#state_conf{confid=ConfId}},state,confid) -> {ok,ConfId};
extract_value({#side{},#state_conf{confidhash=ConfIdHash}},state,confidhash) -> {ok,ConfIdHash};
extract_value({#side{},#state_conf{confnum=ConfNum}},state,confnum) -> {ok,ConfNum};
extract_value({#side{},#state_conf{invite_ts=ITS}},state,invitets) -> {ok,ITS};
extract_value({#side{},#state_conf{room={_,Room,_}}},state,room) -> {ok,Room};
extract_value({#side{},#state_conf{room={_,_,ConfDomain}}},state,confdomain) -> {ok,ConfDomain};
extract_value({#side{remoteuri=RURI},#state_conf{}},side,remoteuri) -> {ok,?U:unparse_uri(RURI)};
extract_value({#side{localuri=LURI},#state_conf{}},side,localuri) -> {ok,?U:unparse_uri(LURI)};
extract_value({#side{localtag=LTAG},#state_conf{}},side,localtag) -> {ok,LTAG};
extract_value({#side{callid=CallId},#state_conf{}},side,acallid) -> {ok,CallId};
extract_value({#side{participantid=PartcpId},#state_conf{}},side,partcpid) -> {ok,PartcpId};
 % fork_start ==> Fork::#side_fork{}, State::#state_forking{}
 % fork_stop ==> Fork::#side_fork{}, State::#state_forking{}
extract_value({#side_fork{},#state_conf{confid=ConfId}},state,confid) -> {ok,ConfId};
extract_value({#side_fork{},#state_conf{confidhash=ConfIdHash}},state,confidhash) -> {ok,ConfIdHash};
extract_value({#side_fork{},#state_conf{confnum=ConfNum}},state,confnum) -> {ok,ConfNum};
extract_value({#side_fork{},#state_conf{invite_ts=ITS}},state,invitets) -> {ok,ITS};
extract_value({#side_fork{},#state_conf{room={_,Room,_}}},state,room) -> {ok,Room};
extract_value({#side_fork{},#state_conf{room={_,_,ConfDomain}}},state,confdomain) -> {ok,ConfDomain};
extract_value({#side_fork{remoteuri=RURI},#state_conf{}},fork,remoteuri) -> {ok,?U:unparse_uri(RURI)};
extract_value({#side_fork{requesturi=ReqURI},#state_conf{}},fork,requesturi) -> {ok,?U:unparse_uri(ReqURI)};
extract_value({#side_fork{localtag=LTAG},#state_conf{}},fork,localtag) -> {ok,LTAG};
extract_value({#side_fork{callid=CallId},#state_conf{}},fork,acallid) -> {ok,CallId};
extract_value({#side_fork{participantid=PartcpId},#state_conf{}},fork,partcpid) -> {ok,PartcpId};
%%---
extract_value(_,_,_) -> {error, undef}.

%% ----------------------------------------------
%% helper functions
%% ----------------------------------------------
get_participants([],_) -> "";
get_participants(Participants,participants) ->
    F = fun({Participant,_}, Acc) -> Acc ++ ?EU:to_list(Participant) ++ "," end,
    lists:foldl(F, "", Participants);
get_participants(Participants,participantsdata) ->
    F = fun({Participant,PartcpSide},Acc) -> Acc ++ ?EU:to_list(Participant) ++ extract_data(PartcpSide) ++ ";" end,
    lists:foldl(F, "", Participants).

%% @private
extract_data(#side{callid=ACallId,localuri=ALocalUri,localtag=ALocalTag,remoteuri=ARemoteUri}) ->
    ?EU:to_list(ACallId)++","++
        ?EU:to_list(?U:unparse_uri(?U:clear_uri(ALocalUri)))++","++
        ?EU:to_list(ALocalTag)++","++
        ?EU:to_list(?U:unparse_uri(?U:clear_uri(ARemoteUri))).
