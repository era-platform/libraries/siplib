%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author George Makarov <georgemkrv@gmail.com>, Peter Bukashin <tbotc@yandex.ru>
%%% @doc


-module(r_sip_conf_event_meta).

-export([get_meta/1]).

%% ==========================================================================
%% Defines
%% ==========================================================================

%% ====================================================================
%% API functions
%% ====================================================================

% conf_init ==> ConfId::binary(), Room::aor(), ITS::integer()
% conf_dead ==> ConfId::binary(), Room::aor(), ITS::integer()
get_meta(Req) when Req == conf_init;Req == conf_dead ->
    [{<<"confid">>, state, confid},
     {<<"confidhash">>, state, confidhash},
     {<<"room">>, state, room},
     {<<"confdomain">>, state, confdomain},
     {<<"invitets">>, state, invitets}];
%% conf_start ==> State::#state_conf{}
get_meta(conf_start) ->
    [{<<"confid">>, state, confid},
     {<<"confidhash">>, state, confidhash},
     {<<"confnum">>, state, confnum},
     {<<"invitets">>, state, invitets},
     {<<"mgc">>, media, mgc},
     {<<"reclinks">>, media, reclinks},
     {<<"isrec">>, media, isrec},
     {<<"participants">>, participants, participants},
     {<<"participantsdata">>, participants, participantsdata},
     {<<"room">>,state,room},
     {<<"confdomain">>,state,confdomain}];
% conf_stop ==> ConfId::binary(), Room::aor(), State::#state_conf{}
get_meta(conf_stop) ->
    [{<<"confid">>,state,confid},
     {<<"confidhash">>,state,confidhash},
     {<<"confnum">>,state, confnum},
     {<<"invitets">>,state,invitets},
     {<<"mgc">>, media, mgc},
     {<<"reclinks">>, media, reclinks},
     {<<"isrec">>, media, isrec},
     {<<"participants">>,state,participants},
     {<<"participantsdata">>, participants, participantsdata},
     {<<"room">>,state,room},
     {<<"comfdomain">>,state,confdomain},
     {<<"stopreason">>, state,stopreason},
     {<<"stoptype">>, state,stoptype},
     {<<"stopoperations">>, state,stopoperations},
     {<<"stopcallid">>, state,stopcallid}];
% call_attach ==> Side::#side{}, State::#state_conf{}
% call_detach ==> Side::#side{}, State::#state_conf{}
%% #{<<"side">> => side(Side),<<"state">> => confcid(State,Event)};
get_meta(Req) when Req==call_attach;Req==call_detach ->
    [{<<"confid">>,state,confid},
     {<<"confidhash">>,state,confidhash},
     {<<"confnum">>,state,confnum},
     {<<"invitets">>,state,invitets},
     {<<"room">>,state,room},
     {<<"confdomain">>,state,confdomain},
     {<<"remoteuri">>,side,remoteuri},
     {<<"localuri">>,side,localuri},
     {<<"localtag">>,side,localtag},
     {<<"callid">>,side,callid},
     {<<"partcpid">>,side,partcpid}];
% fork_start ==> Fork::#side_fork{}, State::#state_forking{}
% fork_stop ==> Fork::#side_fork{}, State::#state_forking{}
% fork_preanswer ==> Fork::#side_fork{}, State::#state_forking{}
% fork_answer ==> Fork::#side_fork{}, State::#state_forking{}
% #{<<"fork">> => fork(Fork),<<"state">> => confcid(State,Event)}.
get_meta(Req) when Req==fork_start;Req==fork_stop;Req==fork_preanswer;Req==fork_answer ->
    [{<<"confid">>,state,confid},
     {<<"confidhash">>,state,confidhash},
     {<<"confnum">>,state,confnum},
     {<<"invitets">>,state,invite_ts},
     {<<"room">>,state,room},
     {<<"requesturi">>,fork,requesturi},
     {<<"remoteuri">>,fork,remoteuri},
     {<<"localtag">>,fork,localtag},
     {<<"callid">>,fork,callid},
     {<<"partcpid">>,fork,partcpid}];
get_meta(_) -> [].
