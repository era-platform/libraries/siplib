%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc
%%% @todo

-module(r_sip_conf_eventing).
-author('Peter Bukashin <tbotc@yandex.ru>').

%% -------------------------------------------------------------------
%%
%% -------------------------------------------------------------------

-compile([export_all,nowarn_export_all]).

%-import(r_sip_conf_eventing_log, [id/1]).

%% ==========================================================================
%% Defines
%% ==========================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_conf.hrl").

% -include("../include/r_nksip.hrl").

%-define(OUTD(Fmt,Args), ?OUT(Fmt,Args)).
-define(OUTD(Fmt,Args), ?NOOUT(Fmt,Args)).

%% ====================================================================
%% API functions
%% ====================================================================

%% ------
%% when conference init (after routing)
%% ConfIdData::{ConfId::binary(),ConfIdHash::integer()}, Room::aor(), ITS::integer()
conf_init({{ConfId,_}=_ConfIdData, Room, _ITS}=Id,Opts) ->
    Event = ?EVENTHANDLER:prepare_event(conf_init,Id),
    send_event(ConfId,Room,Event,Opts).

%% when conference start (after init)
%% State::#state_conf{}
conf_start(#state_conf{confid=ConfId,room=Room}=State,Opts) ->
    Event = ?EVENTHANDLER:prepare_event(conf_start,State),
    send_event(ConfId,Room,Event,Opts).


%% ------
%% when new participant attached
%% Side::#side{}, State::#state_conf{}
call_attach(Side, #state_conf{confid=ConfId,room=Room}=State, Opts) ->
    Event = ?EVENTHANDLER:prepare_event(call_attach,Side,State),
    send_event(ConfId,Room,Event,Opts).

%% when participant detached
%% Side::#side{}, State::#state_conf{}
call_detach(Side, #state_conf{confid=ConfId,room=Room}=State, Opts) ->
    Event = ?EVENTHANDLER:prepare_event(call_detach,Side, State),
    send_event(ConfId,Room,Event,Opts).


%% ------
%% when conf detects dtmf from abonent
dtmf(_Side, _Dtmf, _State, _Opts) ->
    ok.

%% when remote side hold
hold(_Side, _State, _Opts) ->
    ok.

%% when remote side unhold
unhold(_Side, _State, _Opts) ->
    ok.

%% when remote side session changed, but not hold/unhold
sesschanged(_Side, _State, _Opts) ->
    ok.

%% ------
%% when fork start invite
%% Fork::#side_fork{}, State::#state_forking{}
fork_start(Fork, #state_conf{confid=ConfId,room=Room}=State, Opts) ->
    Event = ?EVENTHANDLER:prepare_event(fork_start,Fork,State),
    send_event(ConfId,Room,Event,Opts).

%% when fork got 1xx
%% Fork::#side_fork{}, State::#state_forking{}
fork_preanswer(Fork, SipCodeReason, #state_conf{confid=ConfId,room=Room}=State, Opts) ->
    Event = ?EVENTHANDLER:prepare_event(fork_preanswer,Fork,SipCodeReason,State),
    send_event(ConfId,Room,Event,Opts).

%% when fork got 2xx
%% Fork::#side_fork{}, State::#state_forking{}
fork_answer(Fork, SipCodeReason, #state_conf{confid=ConfId,room=Room}=State, Opts) ->
    Event = ?EVENTHANDLER:prepare_event(fork_answer,Fork,SipCodeReason,State),
    send_event(ConfId,Room,Event,Opts).

%% when fork got 3xx, 4xx-6xx or timeout
%% Fork::#side_fork{}, State::#state_forking{}
fork_stop(Fork, #state_conf{confid=ConfId,room=Room}=State, Opts) ->
    Event = ?EVENTHANDLER:prepare_event(fork_stop,Fork, State),
    send_event(ConfId,Room,Event,Opts).


%% ------
%% when conference stops correctly
%%   stopreason: #{type:atom, reason::binary(), [operations::list(), callid::binary()]}
conf_stop(#state_conf{room=Room, stopreason=_StopResult}=State,Opts) ->
    Event = ?EVENTHANDLER:prepare_event(conf_stop,State),
    erlang:spawn(?MODULE,send_event_delayed,[Room,Event,Opts]),
    ok.

%% when conference dead
%% ConfIdData::{ConfId::binary(),ConfIdHash::integer()}, Room::aor(), ITS::integer()
conf_dead({{ConfId,_}=_ConfIdData, Room, _ITS}=Id,Opts) ->
    Event = ?EVENTHANDLER:prepare_event(conf_dead,Id),
    send_event(ConfId,Room,Event,Opts).

%% ====================================================================
%% Internal functions
%% ====================================================================

send_event(ConfId, Room, Event, Opts) ->
    ?WORKER:cast_workf(ConfId, fun() -> send_event_worker(Room,Event,Opts) end).

%% @private
send_event_worker(Room,Event,Opts) ->
    case ?ENVLIC:is_cdr_allowed() of
        false -> ok; % RP-363
        true -> ?ENVEVENTGATE:send_event(Event,make_destinations(Room,Opts))
    end.

send_event_delayed(Room,Event,Opts) ->
    case ?ENVLIC:is_cdr_allowed() of
        false -> ok; % RP-363
        true ->
            timer:sleep(3000),
            ?ENVEVENTGATE:send_event(Event,make_destinations(Room,Opts))
    end.

%% @private
make_destinations({_,_,Domain},Opts) ->
    %ScriptIdM = maps:get(ctx_scrid_master,Opts),
    %ScriptIdD = maps:get(ctx_scrid_domain,Opts),
    Relates = maps:get(relates,Opts,[]),
    #{kafka => [Domain], % RP-1853
      wssubscr => [{Dom,[{Type,ObjKey}]} || {Dom,{Type,ObjKey}} <- Relates],
      %svcscript => [{ScriptIdM, ?EU:to_binary(?ENVCFG:get_general_domain()), <<"conf">>},
      %              {ScriptIdD, Domain, <<"conf">>}],
      sq => false}. % DEBUG 2021-11-25
