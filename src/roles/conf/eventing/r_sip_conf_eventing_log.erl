%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc
%%% @todo

-module(r_sip_conf_eventing_log).
-author('Peter Bukashin <tbotc@yandex.ru>').

%% -------------------------------------------------------------------
%%
%% -------------------------------------------------------------------

-compile([export_all,nowarn_export_all]).

%% ==========================================================================
%% Defines
%% ==========================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_conf.hrl").

% -include("../include/r_nksip.hrl").

%-define(OUTD(Fmt,Args), ok).
-define(OUTD(Fmt,Args), ?LOGCDR(Fmt,Args)).

%% ====================================================================
%% API functions
%% ====================================================================

%% ------
%% when conference init (after routing)
%% ConfIdData::{ConfId::binary(),ConfIdHash::integer()}, Room::aor(), ITS::integer()
conf_init({_ConfIdData, _Room, _ITS}=Id, Opts) ->
    ?OUTD("conf_init ~1000tp", [id(Id)]),
    ?EVENT_CDR:conf_init(Id, Opts).

%% when conference start (after init)
%% State::#state_conf{}
conf_start(State, Opts) ->
    ?OUTD("conf_start ~1000tp", [id(confcid(State))]),
    notify_owner(State, fun() -> {conf_start, [State#state_conf.confnum]} end),
    ?EVENT_CDR:conf_start(State, Opts).


%% ------
%% when new participant attached
%% Side::#side{}, State::#state_conf{}
call_attach(Side, State, Opts) ->
    ?OUTD("call_attach ~1000tp, ~1000tp", [id(confcid(State)), side(Side)]),
    notify_owner(State, fun() -> {call_attach, Side#side.participantid, [Side#side.callid, ?U:extract_display(Side#side.remoteparty)]} end),
    ?EVENT_CDR:call_attach(Side, State, Opts).

%% when participant detached
%% Side::#side{}, State::#state_conf{}
call_detach(Side, State, Opts) ->
    ?OUTD("call_detach ~1000tp, ~1000tp", [id(confcid(State)), side(Side)]),
    notify_owner(State, fun() -> {call_detach, Side#side.participantid, [Side#side.callid]} end),
    ?EVENT_CDR:call_detach(Side, State, Opts).


%% ------
%% when conf detects dtmf from abonent
dtmf(Side, Dtmf, State, Opts) ->
    ?OUTD("dtmf ~1000tp, ~1000tp : ~s", [id(confcid(State)), side(Side), Dtmf]),
    notify_owner(State, fun() -> {dtmf, Side#side.participantid, [Dtmf]} end),
    ?EVENT_CDR:dtmf(Side, Dtmf, State, Opts).

%% when remote side hold
hold(Side, State, Opts) ->
    ?OUTD("hold ~1000tp, ~1000tp", [id(confcid(State)), side(Side)]),
    notify_owner(State, fun() -> {hold, Side#side.participantid, [?U:extract_display(Side#side.remoteparty)]} end),
    ?EVENT_CDR:hold(Side, State, Opts).

%% when remote side unhold
unhold(Side, State, Opts) ->
    ?OUTD("unhold ~1000tp, ~1000tp", [id(confcid(State)), side(Side)]),
    notify_owner(State, fun() -> {unhold, Side#side.participantid, [?U:extract_display(Side#side.remoteparty)]} end),
    ?EVENT_CDR:unhold(Side, State, Opts).

%% when sess changed, but not hold/unhold
sesschanged(Side, State, Opts) ->
    ?OUTD("sesschanged ~1000tp, ~1000tp", [id(confcid(State)), side(Side)]),
    notify_owner(State, fun() -> {sesschanged, Side#side.participantid, [?U:extract_display(Side#side.remoteparty)]} end),
    ?EVENT_CDR:sesschanged(Side, State, Opts).


%% ------
%% when fork start invite
%% Fork::#side_fork{}, State::#state_forking{}
fork_start(Fork, State, Opts) ->
    ?OUTD("fork_start ~1000tp, ~1000tp", [id(confcid(State)), fork(Fork)]),
    notify_owner(State, fun() -> {fork_start, Fork#side_fork.participantid, []} end),
    ?EVENT_CDR:fork_start(Fork, State, Opts).

%% when fork got 1xx
%% Fork::#side_fork{}, State::#state_forking{}
fork_preanswer(Fork, SipCodeReason, State, Opts) ->
    ?OUTD("fork_preanswer ~1000tp, ~1000tp, ~1000tp", [id(confcid(State)), fork(Fork), SipCodeReason]),
    notify_owner(State, fun() -> {fork_preanswer, Fork#side_fork.participantid, []} end),
    ?EVENT_CDR:fork_preanswer(Fork, SipCodeReason, State, Opts).

%% when fork got 2xx
%% Fork::#side_fork{}, State::#state_forking{}
fork_answer(Fork, SipCodeReason, State, Opts) ->
    ?OUTD("fork_answer ~1000tp, ~1000tp, ~1000tp", [id(confcid(State)), fork(Fork), SipCodeReason]),
    notify_owner(State, fun() -> {fork_answer, Fork#side_fork.participantid, []} end),
    ?EVENT_CDR:fork_answer(Fork, SipCodeReason, State, Opts).

%% when fork got 3xx, 4xx-6xx or timeout
%% Fork::#side_fork{}, State::#state_forking{}
fork_stop(Fork, State, Opts) ->
    ?OUTD("fork_stop ~1000tp, ~1000tp", [id(confcid(State)), fork(Fork)]),
    notify_owner(State, fun() -> {fork_stop, Fork#side_fork.participantid, []} end),
    ?EVENT_CDR:fork_stop(Fork, State, Opts).


%% ------
%% when conference stops correctly
%% ConfId::binary(), Room::aor(), State::#state_conf{}
%%   stopreason: #{type:atom, reason::binary(), [operations::list(), callid::binary()]}
conf_stop(#state_conf{stopreason=StopResult}=State, Opts) ->
    ?OUTD("conf_stop ~1000tp, ~1000tp", [id(confcid(State)),StopResult]),
    notify_owner(State, fun() -> {conf_stop,StopResult} end),
    ?EVENT_CDR:conf_stop(State, Opts).

% when conference dead
% ConfIdData::{ConfId::binary(),ConfIdHash::integer()}, Room::aor(), ITS::integer()
conf_dead({_ConfIdData, _Room, _ITS}=Id, OwnerPid, Opts) ->
    ?OUTD("conf_dead ~1000tp", [id(Id)]),
    notify_owner(OwnerPid, fun() -> {conf_dead} end),
    ?EVENT_CDR:conf_dead(Id, Opts).

%% ====================================================================
%% Internal functions
%% ====================================================================

id({{ConfId,_ConfIdH}, Room, ITS}) -> {ConfId, Room, ?EU:timestamp(ITS)};
id({ConfId, Room, ITS}) -> {ConfId, Room, ?EU:timestamp(ITS)}.

confcid(#state_conf{confnum=ConfId,room=Room,invite_ts=ITS}=_State) -> {ConfId, Room, ITS}.

side(#side{callid=CallId,participantid=PartcpId,remoteuri=RUri,localtag=LTag}=_Side) -> {?U:unparse_uri(?U:clear_uri(RUri)),LTag,CallId,PartcpId};
side(#side_fork{}=Fork) -> fork(Fork). % #451(b)

fork(#side_fork{callid=CallId,participantid=PartcpId,requesturi=RUri,localtag=LTag}=_Side) -> {?U:unparse_uri(?U:clear_uri(RUri)),LTag,CallId,PartcpId}.

notify_owner(#state_conf{ownerpid=Owner}=_State, F) -> notify_owner(Owner, F);
notify_owner(undefined, _) -> ok;
notify_owner(Owner, F) when is_pid(Owner), is_function(F) ->
    Owner ! {'conf_event', F()}.
