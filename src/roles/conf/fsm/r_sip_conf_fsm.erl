%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc

-module(r_sip_conf_fsm).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([init/1, terminate/3, code_change/4]).
-export([callback_mode/0, handle_event/4]). % gen_statem
-export([handle_event/3, handle_sync_event/4, handle_info/3]). % gen_fsm

-export([active/2, active/3,
         stopping/2, stopping/3]).

-export([start_empty/3,
         start/1,
         supv_start_link/1,
         stop/1, stop/2,
         stop_forcely/1,
         pull_fsm_by_callid/1,
         pull_fsm_by_confid/1,
         test/1,
         build_tag/2,
         attach_participant/2,
         start_outgoing_call/2,
         sync_call_start/4,
         sync_call_stop/2,
         sync_call_get_state/2,
         sync_call_set_mode/3,
         sync_get_topology/2,
         sync_set_topology/2,
         sync_start_play/4,
         get_participants/2,
         find_participant/2,
         get_participant_info/3,
         get_info/2]).

-export([uac_response/2,
         uas_dialog_response/2,
         sip_cancel/2,
         sip_bye/2,
         sip_reinvite/2,
         sip_refer/2,
         sip_notify/2,
         sip_info/2,
         sip_message/2,
         call_terminate/2]).

-export([apply_fun/2]). % #292

-export([get_current_media_link/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_conf.hrl").
-include("../include/r_sip_mgc.hrl").
-include("../include/r_sip_headers.hrl").

% -include("../include/r_nksip.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------
%% start from ext service (ex. selector)
%% ------------------------
-spec start_empty({Room::binary(), Domain::binary()}, Owner::pid(), Opts::list()) -> {ok,Pid::pid(),ConfId::binary()} | {error,Reason::term()}.
%% ------------------------
start_empty({Room, Domain}, Owner, Opts) ->
    Opts1 = maps:to_list(maps:without([app,req,room,call_pid], maps:from_list(Opts))),
    Opts2 = [{app,?SIPAPP},
             {req,undefined},
             {room,{sip,Room,Domain}},
             {call_pid,undefined},
             {owner,Owner}
            |Opts1],
    ?MODULE:start(Opts2).

%% ------------------------
%% start new conf
%% ------------------------
-spec start(Opts::list()) -> {ok,Pid::pid(),ConfId::binary()} | {error,Reason::term()}.
%% ------------------------
start(Opts) ->
    RoomAOR = lists:keyfind('room', 1, Opts),
    {ConfId, D, ConfIdHash} = generate_ids(RoomAOR),
    Opts1 = [{confid,ConfId}, {confnum,D}, {confidhash,ConfIdHash} | Opts], % TODO videoconference extension from selector
    ChildSpec = {ConfId, {?MODULE, supv_start_link, [Opts1]}, temporary, 1000, worker, [?MODULE]},
    case ?CONF_SUPV:start_child(ChildSpec) of
        {ok,Pid} -> {ok,Pid,ConfId};
        {error,_}=Err -> Err
    end.

%%
supv_start_link(Opts) ->
    start_link(?MODULE, Opts, []).

%% ------------------------
%% stop existing conf
%% ------------------------
stop(DlgX) ->
    stop(DlgX, "Undefined").

stop({_ConfId,Pid}, Reason) when is_pid(Pid) ->
    send_event(Pid, {stop_external, ?CONF_UTILS:reason_external(Reason)});
stop(Pid, Reason) when is_pid(Pid) ->
    send_event(Pid, {stop_external, ?CONF_UTILS:reason_external(Reason)});
stop(ConfId, Reason) when is_binary(ConfId) ->
    to_fsm(ConfId, fun(Pid) -> send_event(Pid, {stop_external, ?CONF_UTILS:reason_external(Reason)}) end).

%% ------------------------
stop_forcely({_ConfId,Pid}) when is_pid(Pid) ->
    send_all_state_event(Pid, {stop_forcely});
stop_forcely(ConfId) when is_binary(ConfId) ->
    to_fsm(ConfId, fun(Pid) -> send_all_state_event(Pid, {stop_forcely}) end).

% --
pull_fsm_by_callid(CallId) ->
    ?DLG_STORE:pull_dlg(CallId).

% --
pull_fsm_by_confid(ConfId) ->
    case ?DLG_STORE:find_t(ConfId) of
        false -> false;
        {_,#{link:=Lnk}} -> Lnk
    end.

% --
test({_ConfId,Pid})
  when is_pid(Pid) ->
    send_all_state_event(Pid, {test}).

% --------------------

%% ------------------------
%% builds_tag which doesn't exist. SYNC, wait response
%% ------------------------
build_tag({_ConfId,Pid},Req)
  when is_pid(Pid) ->
    sync_send_event(Pid, {build_tag,[Req]}).

%% ------------------------
%% attach participant to existing conf
%% ------------------------
attach_participant({_ConfId,Pid},Opts)
  when is_pid(Pid) ->
    send_event(Pid, {attach_participant,[Opts]}).

%% ------------------------
%% Async start outgoing call from conf
%% ------------------------
start_outgoing_call({_Conf,Pid}, To) when is_pid(Pid) ->
    send_event(Pid, {start_outgoing_call,[To]});
start_outgoing_call(ConfId, To) when is_binary(ConfId) ->
    to_fsm(ConfId, fun(Pid) -> send_event(Pid, {start_outgoing_call,[To]}) end).

%% ------------------------
%% Sync start call to participant
%% ------------------------
sync_call_start(ConfId, PartcpId, To, Opts) when is_binary(ConfId) ->
    to_fsm(ConfId, fun(Pid) -> sync_send_event(Pid, {sync_call_start,[PartcpId, To, Opts]}) end).

%% ------------------------
%% Sync stop call (online or forking)
%% ------------------------
sync_call_stop(ConfId, PartcpId) when is_binary(ConfId) ->
    to_fsm(ConfId, fun(Pid) -> sync_send_event(Pid, {sync_call_stop,[PartcpId]}) end).

%% ------------------------
%% Sync get state of call (online or forking)
%% ------------------------
sync_call_get_state(ConfId, PartcpId) when is_binary(ConfId) ->
    to_fsm(ConfId, fun(Pid) -> sync_send_event(Pid, {sync_call_get_state,[PartcpId]}) end).

%% ------------------------
%% Sync set mode of call (participant)
%% ------------------------
sync_call_set_mode(ConfId, PartcpId, Opts) when is_binary(ConfId) ->
    to_fsm(ConfId, fun(Pid) -> sync_send_event(Pid, {sync_call_set_mode,[PartcpId, Opts]}) end).

%% ------------------------
%% Sync get topology
%% ------------------------
sync_get_topology(ConfId,Opts) when is_binary(ConfId) ->
    to_fsm(ConfId, fun(Pid) -> sync_send_event(Pid, {sync_get_topology,[Opts]}) end).

%% ------------------------
%% Sync set topology
%% ------------------------
sync_set_topology(ConfId, Opts) when is_binary(ConfId) ->
    to_fsm(ConfId, fun(Pid) -> sync_send_event(Pid, {sync_set_topology,[Opts]}) end).

%% ------------------------
%% Sync start play
%% ------------------------
sync_start_play(ConfId, PlayerId, File, Opts) when is_binary(ConfId) ->
    to_fsm(ConfId, fun(Pid) -> sync_send_event(Pid, {sync_start_play,[PlayerId, File, Opts]}) end).

%% ------------------------
%% Return participants list
%% ------------------------
get_participants(ConfId, Opts) when is_binary(ConfId) ->
    to_fsm(ConfId, fun(Pid) -> sync_send_event(Pid, {get_participants,[Opts]}) end).

%% ------------------------
%% Find participant, return PartcpId
%% ------------------------
find_participant(ConfId, Key) when is_binary(ConfId) ->
    to_fsm(ConfId, fun(Pid) -> sync_send_event(Pid, {find_participant,[Key]}) end).

%% ------------------------
%% Return participant info
%% ------------------------
get_participant_info(ConfId, PartcpId, Opts) when is_binary(ConfId) ->
    to_fsm(ConfId, fun(Pid) -> sync_send_event(Pid, {get_participant_info,[PartcpId,Opts]}) end).

%% ------------------------
%% Return conference info
%% ------------------------
get_info(ConfId,Opts) when is_binary(ConfId) ->
    to_fsm(ConfId, fun(Pid) -> sync_send_event(Pid, {get_info,Opts}) end).

%% ------------------------

uac_response({_DlgId,Pid},[_Resp]=Args)
  when is_pid(Pid) ->
    send_event(Pid, {uac_response, Args}),
    continue.

uas_dialog_response({_DlgId,Pid},[_Resp]=Args)
  when is_pid(Pid) ->
    send_event(Pid, {uas_dialog_response, Args}),
    continue.

sip_cancel({_DlgId,Pid},Args)
  when is_pid(Pid) ->
    send_event(Pid, {sip_cancel,Args}).

sip_bye({_DlgId,Pid},Args)
  when is_pid(Pid) ->
    send_event(Pid, {sip_bye,Args}).

sip_reinvite({_DlgId,Pid},Args)
  when is_pid(Pid) ->
    send_event(Pid, {sip_reinvite,Args}).

sip_refer({_DlgId,Pid},Args)
  when is_pid(Pid) ->
    send_event(Pid, {sip_refer,Args}).

sip_notify({_DlgId,Pid},Args)
  when is_pid(Pid) ->
    send_all_state_event(Pid, {sip_notify,Args}).

sip_info({_DlgId,Pid},Args)
  when is_pid(Pid) ->
    send_all_state_event(Pid, {sip_info,Args}).

sip_message({_DlgId,Pid},Args)
  when is_pid(Pid) ->
    send_all_state_event(Pid, {sip_message,Args}).

call_terminate({_DlgId,Pid},Args)
  when is_pid(Pid) ->
    send_event(Pid, {call_terminate,Args}).

%% ------------------------

%% #292
apply_fun({_DlgId,Pid},Args)
  when is_pid(Pid) ->
    send_all_state_event(Pid, {apply_fun,Args}).

%% ------------------------

get_current_media_link({_DlgId,Pid}) when is_pid(Pid) ->
    sync_send_all_state_event(Pid, {get_current_media_link}).

%% ====================================================================
%% gen_fsm => gen_statem translation functions
%% ====================================================================

% --------------------
% gen_fsm calls
% --------------------
start_link(Module,Args,Opts) ->
    ?FSMT:start_link(Module,Args,Opts).

send_event(FsmRef,Event) ->
    ?FSMT:send_event(FsmRef,Event).

sync_send_event(FsmRef,Event) ->
    ?FSMT:sync_send_event(FsmRef,Event).

send_all_state_event(FsmRef,Event) ->
    ?FSMT:send_all_state_event(FsmRef,Event).

sync_send_all_state_event(FsmRef,Event) ->
    ?FSMT:sync_send_all_state_event(FsmRef,Event).

% --------------------
% gen_statem callbacks
% --------------------
callback_mode() ->
    ?FSMT:callback_mode(?MODULE).

handle_event(EventType, EventContent, StateName, StateData) ->
    case catch ?FSMT:handle_event(?MODULE,EventType,EventContent,StateName,StateData) of
        {'EXIT',_}=E -> ?OUT("Error: ~120p", [E]), stop;
        T -> T
    end.

%% ====================================================================
%% Callback functions
%% ====================================================================

init(Arg) ->
    self() ! 'do_init',
    ?LOGSIP("CONF fsm 'initial'", []),
    {ok, 'initial', {'initial',Arg}}.

init_internal(Opts) ->
    Self = self(),
    [App,ConfId,ConfIdHash,ConfNum,{_,_,Domain}=Room] = ?EU:extract_required_props([app,confid,confidhash,confnum,room], Opts),
    [Req,Owner,Video0,VideoSchema0] = ?EU:extract_optional_props([req,owner,video,videoschema], Opts),
    case Req of
        #sipmsg{call_id=XCallId} ->
            AutoClose = true,
            ?DLG_STORE:push_dlg(XCallId, {ConfId,Self}),
            % featurecode
            FeaturecodeId = case nksip_request:header(?FCHeaderLow,Req) of {ok,[FCId|_]} -> FCId; _ -> undefined end,
            Featurecode = case (FeaturecodeId/=undefined) andalso (?ENVDC:get_object_sticky(Domain, featurecode, [{ids,[FeaturecodeId]}],auto)) of
                              {ok,[FCItem|_],_} -> FCItem;
                              _ -> undefined
                          end;
        _ ->
            XCallId = undefined,
            AutoClose = false,
            FeaturecodeId = undefined,
            Featurecode = undefined
    end,
    NowTS = os:timestamp(),
    ITS = ?EU:timestamp(NowTS),
    StateData0 = #state_conf{pid=Self,
                             confid=ConfId,
                             confidhash=ConfIdHash, % #370
                             confnum=ConfNum,
                             room=Room,
                             invite_ts=ITS,
                             ownerpid=Owner,
                             participants=[],
                             autoclose=AutoClose,
                             starttime=NowTS},
    d(StateData0, 'init', "srv initing by XCallId=~120p",[XCallId]),
    % Event
    ?FSM_EVENT:conf_init({{ConfId,ConfIdHash},Room,ITS}),
    % Local node storage
    LStoreData = #{pid => Self,
                   timestart => erlang:time(),
                   id => ConfId, % binary()
                   room => Room, % aor()
                   link => {ConfId,Self},
                   callids => [XCallId], % used only for log
                   fun_cleanup => fun() -> ok end,
                   supv => ?CONF_SUPV},
    ?DLG_STORE:store_t(ConfId, LStoreData, ?STORE_TIMEOUT),
    erlang:send_after(?STORE_REFRESH, Self, {refresh_at_store_local}),
    % Site conf link storage
    GStoreKey = {conf,Room},
    GStoreData = #{srvidx => ?CFG:get_my_sipserver_index(),
                   confid => ConfId},
    ?U:cast_store(GStoreKey, fun() -> {put_ttl, [GStoreKey, 'conf_room', GStoreData, ?STORE_TIMEOUT]} end),
    erlang:send_after(?STORE_REFRESH, Self, {refresh_at_store_global}),
    % Monitor
    % we can't apriory setup clear usr states clear, because of fork-dependant domains. It would be done by state collector's monitor.
    % we can't also setup clear media, it starts later, it can migrate.
    Flog = fun(StopReason) -> ?OUT("Process ~p terminated (conf=~p): ~120p", [Self, ConfNum, StopReason]) end,
    Fclear = fun() -> ?DLG_STORE:unlink_dlg(XCallId),
                      ?DLG_STORE:delete_t(ConfId),
                      ?FSM_EVENT:conf_dead({{ConfId,ConfIdHash},Room,ITS},Owner),
                      ?U:cast_store(GStoreKey, fun() -> {del, [GStoreKey]} end),
                      ?CONFSTORE:del_conf(ConfId)
             end,
    ?MONITOR:start_monitor(Self, "Sip conf dlg", [Flog, Fclear]),
    % monitor owner to auto close conf when owner down (selector)
    monitor_owner(Owner,ConfId),
    % videoconf opts
    {Video,VideoSchema} = case Video0 of
                              true -> {Video0,VideoSchema0};
                              _ ->
                                  case Featurecode of
                                      undefined -> {false,undefined};
                                      #{} ->
                                          case maps:get(type,Featurecode) of
                                              <<"videoconference">> -> {true, maps:get(extension,Featurecode)};
                                              _ -> {false,undefined}
                                          end end end,
    %
    StateData1 = StateData0#state_conf{video=Video,
                                       map=#{app => App,
                                             opts => Opts,
                                             featurecode_id => FeaturecodeId,
                                             featurecode => Featurecode,
                                             videoSchema => VideoSchema,
                                             l_store => {ConfId, LStoreData},
                                             g_store => {GStoreKey, GStoreData}}},
    % to callstore
    ?CONFSTORE:add_conf(StateData1),
    %
    StateData2 = ?ACTIVE:init([Opts], StateData1),
    d(StateData2, 'init', "srv inited by XCallId=~120p",[XCallId]),
    {ok, ?ACTIVE_STATE, StateData2}.

% -------------------------
% 'forking' events
% -------------------------

active({attach_participant, _Args}=Event, #state_conf{media=undefined}=StateData) ->
    send_event(self(), Event),
    {next_state, ?ACTIVE_STATE, StateData};
active({attach_participant, Args}=_Event, StateData) ->
    ?ACTIVE:attach_participant(Args, StateData); %% TODO ???(25.04.2019)

active({sip_cancel, Args}=_Event, StateData) ->
    ?ACTIVE:sip_cancel(Args, StateData);

active({sip_bye, Args}=_Event, StateData) ->
    ?ACTIVE:sip_bye(Args, StateData);

active({sip_reinvite, Args}=_Event, StateData) ->
    ?ACTIVE:sip_reinvite(Args, StateData);

active({sip_refer, Args}=_Event, StateData) ->
    ?ACTIVE_REFER:sip_refer(Args, StateData);

active({sip_notify, Args}=_Event, StateData) ->
    ?ACTIVE:sip_notify(Args, ?ACTIVE_STATE, StateData);

active({sip_info, Args}=_Event, StateData) ->
    ?ACTIVE:sip_info(Args, ?ACTIVE_STATE, StateData);

active({sip_message, Args}=_Event, StateData) ->
    ?ACTIVE:sip_message(Args, ?ACTIVE_STATE, StateData);

active({uac_response,Args}, StateData) ->
    ?ACTIVE:uac_response(Args, StateData);

active({uas_dialog_response,Args}, StateData) ->
    ?ACTIVE:uas_dialog_response(Args, StateData);

active({call_terminate, Args}=_Event, StateData) ->
    ?ACTIVE:call_terminate(Args, StateData);

active({stop_external, Reason}, StateData) ->
    ?ACTIVE:stop_external(Reason, StateData);

active({test}, StateData) ->
    ?ACTIVE:test(StateData);

active({start_outgoing_call, Args}=_Event, StateData) ->
    ?ACTIVE_CALL:start_outgoing_call(Args, StateData).

% -

active({sync_call_start, Args}=_Event, _From, StateData) ->
    ?ACTIVE_CALL:sync_call_start(Args, StateData);

active({sync_call_stop, Args}=_Event, _From, StateData) ->
    ?ACTIVE_CALL:sync_call_stop(Args, StateData);

active({sync_call_get_state, Args}=_Event, _From, StateData) ->
    ?ACTIVE_CALL:sync_call_get_state(Args, StateData);

active({sync_call_set_mode, Args}=_Event, _From, StateData) ->
    ?ACTIVE_CALL:sync_call_set_mode(Args, StateData);

active({sync_get_topology, Args}=_Event, _From, StateData) ->
    ?TOPOLOGY:sync_get_topology(Args, StateData);

active({sync_set_topology, Args}=_Event, _From, StateData) ->
    ?TOPOLOGY:sync_set_topology(Args, StateData);

active({sync_start_play, Args}=_Event, _From, StateData) ->
    ?ACTIVE_PLAY:sync_start_play(Args, StateData);

active({get_participants, Args}=_Event, _From, StateData) ->
    Reply = ?ACTIVE_UTILS:get_participants(Args, StateData),
    {reply, Reply, ?ACTIVE_STATE, StateData};

active({find_participant, [Key]}=_Event, _From, StateData) ->
    Reply = ?ACTIVE_UTILS:find_participant(Key, StateData),
    {reply, Reply, ?ACTIVE_STATE, StateData};

active({get_participant_info, Args}=_Event, _From, StateData) -> 
    Reply = ?ACTIVE_UTILS:get_participant_info(Args, StateData),
    {reply, Reply, ?ACTIVE_STATE, StateData};

active({get_info, Opts}=_Event, _From, StateData) ->
    Reply = ?ACTIVE_UTILS:get_info(Opts, StateData),
    {reply, Reply, ?ACTIVE_STATE, StateData};

%
active({build_tag, Args}=_Event, _From, StateData) ->
    ?ACTIVE:build_tag_sync(Args, StateData).

% ------------------------
% 'stopping' events
% ------------------------
stopping({sip_bye, [_CallId, Req]}=_Event, StateData) ->
    {ok,ReqHandle} = nksip_request:get_handle(Req),
    ?U:send_sip_reply(fun() -> nksip_request:reply(accepted, ReqHandle) end),
    {next_state, ?STOPPING_STATE, StateData};

stopping({sip_reinvite, [_CallId, Req]}=_Event, StateData) ->
    {ok,ReqHandle} = nksip_request:get_handle(Req),
    ?U:send_sip_reply(fun() -> nksip_request:reply(?TemporarilyUnavailable("CONF. Stopping"), ReqHandle) end),
    {next_state, ?STOPPING_STATE, StateData};

stopping({sip_refer, [_CallId, Req]}=_Event, StateData) ->
    {ok,ReqHandle} = nksip_request:get_handle(Req),
    ?U:send_sip_reply(fun() -> nksip_request:reply(?TemporarilyUnavailable("CONF. Stopping"), ReqHandle) end),
    {next_state, ?STOPPING_STATE, StateData};

stopping(Event, StateData) ->
    E = case is_tuple(Event) of
            true -> erlang:element(1, Event);
            false -> Event
        end,
    d(StateData, ?STOPPING_STATE, "event ~120p", [E]),
    {next_state, ?STOPPING_STATE, StateData}.

% -

stopping(Event, _From, StateData) ->
    E = case is_tuple(Event) of
            true -> erlang:element(1, Event);
            false -> Event
        end,
    d(StateData, ?STOPPING_STATE, "sync event ~120p", [E]),
    Err = {error, {invalid_request, <<"Conference in stopping state">>}},
    {reply, Err, ?STOPPING_STATE, StateData}.

% ------------------------
% All states events
% ------------------------

handle_event({sip_notify, [_CallId,Req]=Args}=_Event, StateName, StateData) ->
    case StateName of
        ?STOPPING_STATE -> send_response({forbidden}, Req, {StateName, StateData});
          _ ->
            StateData1 = ?ACTIVE:sip_notify(Args, StateName, StateData),
            {next_state, StateName, StateData1}
    end;

handle_event({sip_info, [_CallId,Req]=Args}=_Event, StateName, StateData) ->
     case StateName of
        ?STOPPING_STATE -> send_response({forbidden}, Req, {StateName, StateData});
          _ ->
            StateData1 = ?ACTIVE:sip_info(Args, StateName, StateData),
            {next_state, StateName, StateData1}
    end;

handle_event({sip_message, [_CallId,Req]=Args}=_Event, StateName, StateData) ->
    case StateName of
        ?STOPPING_STATE -> send_response({forbidden}, Req, {StateName, StateData});
          _ ->
            StateData1 = ?ACTIVE:sip_message(Args, StateName, StateData),
            {next_state, StateName, StateData1}
    end;

% #292
handle_event({apply_fun, [F]}, StateName, StateData) when is_function(F,2) ->
    F(StateName,StateData);
handle_event({apply_fun, [F]}, _StateName, StateData) when is_function(F,1) ->
    F(StateData);

%
handle_event({stop_forcely}, _StateName, StateData) ->
    {stop, normal, StateData};

%
handle_event(Event, StateName, StateData) ->
    E = case is_tuple(Event) of
            true -> erlang:element(1, Event);
            false -> Event
        end,
    d(StateData, StateName, "handle_event ~120p", [E]),
    {next_state, StateName, StateData}.

% -------------------------
% 'active' requests
% 'stopping' requests
% -------------------------

% -------------------------
% All states requests
% -------------------------
handle_sync_event({get_current_media_link}, _From, StateName, StateData) ->
    Reply = ?CONF_MEDIA:get_current_media_link(StateData),
    {reply, Reply, StateName, StateData};

handle_sync_event(Event, _From, StateName, StateData) ->
    E = case is_tuple(Event) of
            true -> erlang:element(1, Event);
            false -> Event
        end,
    d(StateData, StateName, "handle_sync_event ~120p", [E]),
    {reply, b2bua_sync_event_default, StateName, StateData}.

% -------------------------
% Process messages
% -------------------------
%%
handle_info('do_init', 'initial', {'initial',Opts}) ->
    {ok,StateName,StateData} = init_internal(Opts),
    {next_state,StateName,StateData};

%%
handle_info({refresh_at_store_local}, StateName, StateData) ->
    d(StateData, StateName, "refresh_at_store"),
    #state_conf{map=#{l_store:={LStoreKey,LStoreData}}}=StateData,
    ?DLG_STORE:store_t(LStoreKey, LStoreData, ?STORE_TIMEOUT),
    erlang:send_after(?STORE_REFRESH, self(), {refresh_at_store_local}),
    {next_state, StateName, StateData};

handle_info({refresh_at_store_global}, StateName, StateData) ->
    d(StateData, StateName, "refresh_at_store"),
    #state_conf{map=#{g_store:={GStoreKey,GStoreData}}}=StateData,
    ?U:cast_store(GStoreKey, fun() -> {put_ttl, [GStoreKey, 'conf_room', GStoreData, ?STORE_TIMEOUT]} end),
    erlang:send_after(?STORE_REFRESH, self(), {refresh_at_store_global}),
    {next_state, StateName, StateData};

handle_info({start, Ref}, ?ACTIVE_STATE, #state_conf{ref=Ref}=StateData) ->
    ?ACTIVE:start(StateData);

handle_info(?CONF_TIMEOUT_MSG, StateName, StateData) ->
    d(StateData, StateName, "conference_timeout"),
    StopReason = ?CONF_UTILS:reason_timeout([conference], <<>>, <<"Global timeout">>),
    ?MODULE:stop(self(), StopReason),
    {next_state, StateName, StateData};

handle_info({stopping_timeout}, ?STOPPING_STATE, StateData) ->
    final(StateData),
    {stop, normal, StateData};

%% B2B override max fork timeout itself
handle_info({?FORK_TIMEOUT_MSG, Args}, ?ACTIVE_STATE, StateData) ->
    ?ACTIVE:fork_timeout(Args, StateData);

handle_info({mg_dtmf, Args}, ?ACTIVE_STATE, StateData) ->
    ?ACTIVE:mg_dtmf(Args, StateData);

handle_info({mg_event, Args}, ?ACTIVE_STATE, StateData) ->
    ?ACTIVE:mg_event(Args, StateData);

handle_info({mg_trunknovoice, _Args}, StateName, StateData) ->
    {next_state, StateName, StateData};

% ----------
handle_info({mg_disconnected, Args}, StateName, StateData) ->
    self() ! {mg_migrate_priv, Args, 0, 64},
    %erlang:send_after(?EU:random(200), self(), {mg_migrate_priv, Args, 0, 64}),
    {next_state, StateName, StateData};

% -
handle_info({mg_migrate, Args}, StateName, StateData) ->
    self() ! {mg_migrate_priv, Args, 0, 64},
    %erlang:send_after(?EU:random(200), self(), {mg_migrate_priv, Args, 0, 64}),
    {next_state, StateName, StateData};

% -
handle_info({mg_migrate_priv,Args,I,T}, StateName, StateData) ->
    d(StateData, StateName, "MG found disconnected priv"),
    #state_conf{participants=Partcps, forks=Forks}=StateData,
    Count = length(Partcps) + length(Forks),
    case ?CONF_MEDIA:media_check_mg(StateData, Args) of
        false -> ok;
        true -> case ?CONF_MEDIA:check_available_slots(StateData) of
                    {ok,_MgCnt,CtxCnt} when CtxCnt > 100, CtxCnt > Count -> self() ! {change_mg};
                    _ -> case I > 11 of % 64 + 128 + ... + 1024 = 8 sec
                             false ->
                                 P = {mg_migrate_priv, Args, I+1, case T>=1000 of true->T;false->T*2 end},
                                 erlang:send_after(T, self(), P);
                             true ->
                                StopReason = ?CONF_UTILS:reason_timeout([migrate, media, wait_available], <<>>, <<"Available slots not found for migration">>),
                                stop(self(), StopReason)
                         end
                end
    end,
    {next_state, StateName, StateData};

% -
handle_info({change_mg}, ?STOPPING_STATE, StateData) ->
    {next_state, ?STOPPING_STATE, StateData};
handle_info({change_mg}, ?ACTIVE_STATE, StateData) ->
    ?ACTIVE_MIGRATING:migrate(StateData);

% -----

handle_info(Event, StateName, StateData) ->
    E = case is_tuple(Event) of
            true -> erlang:element(1, Event);
            false -> Event
        end,
    d(StateData, StateName, "handle_info ~120p", [E]),
    {next_state, StateName, StateData}.

% -------------------------
%
% -------------------------
terminate(_Reason, _StateName, _StateData) ->
    ok.

code_change(_OldVsn, _StateName, _StateData, _Extra) ->
    ok.

%% ====================================================================
%% Internal functions
%% ====================================================================

% ---
generate_ids(RoomAOR) ->
    <<B6:48/bitstring, _Rest/bitstring>> = ?U:luid(),
    SrvCode = ?U:get_current_srv_textcode(),
    ConfId = <<"rCF-", SrvCode/bitstring, "-", B6/bitstring>>,
    case ?DLG_STORE:find_t(ConfId) of
        false -> {ConfId, B6, ?U:int64_hash(<<SrvCode/binary,B6/binary>>)}; % #370
        _ -> generate_ids(RoomAOR)
    end.

% ---
send_response(SipReply, Req, {StateName, StateData}) ->
    {ok,ReqHandle} = nksip_request:get_handle(Req),
    ?U:send_sip_reply(fun() -> nksip_request:reply(SipReply, ReqHandle) end),
    {next_state, StateName, StateData}.

% ---
to_fsm(ConfId, Fun) ->
    case ?DLG_STORE:find_t(ConfId) of
        false -> {error, {not_found, <<"Conference not found">>}};
        {_,Map} ->
            Pid = maps:get(pid,Map),
            Fun(Pid)
    end.

% ---
final(StateData) ->
    d(StateData, ?STOPPING_STATE, "final"),
    #state_conf{confid=ConfId}=StateData,
    {ok,_} = ?CONF_MEDIA:media_stop(StateData), % media
    ?DLG_STORE:delete_t(ConfId), % store
    ?MONITOR:stop_monitor(self()), % monitor
    ?CONF_SUPV:drop_child(ConfId). % supv

% ---
monitor_owner(Owner, ConfId) when is_pid(Owner) ->
    ?MONITOR:start_monitor(Owner, "Sip conf -> owner", [fun() -> ?LOGSIP("CONF fsm ~p found owner process down", [ConfId]),
                                                                 stop(ConfId, <<"Owner process down">>) end]);
monitor_owner(_, _) -> ok.

% -----
d(StateData, StateName, Text) -> d(StateData, StateName, Text, []).
d(StateData, StateName, Fmt, Args) ->
    #state_conf{confid=ConfId}=StateData,
    ?LOGSIP("CONF fsm ~p '~p':" ++ Fmt, [ConfId,StateName] ++ Args).
