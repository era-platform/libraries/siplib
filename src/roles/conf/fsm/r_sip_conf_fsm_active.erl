%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Presents 'active' state of CONF FSM

-module(r_sip_conf_fsm_active).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([init/2,
         start/1,
         stop_external/2,
         test/1,
         attach_participant/2,
         build_tag_sync/2,
         sip_cancel/2,
         sip_bye/2,
         sip_reinvite/2,
         sip_notify/3,
         sip_info/3,
         sip_message/3,
         uas_dialog_response/2,
         call_terminate/2,
         fork_timeout/2,
         uac_response/2,
         mg_dtmf/2,
         mg_event/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_conf.hrl").
-include("../include/r_sip_mgc.hrl").

% -include("../include/r_nksip.hrl").

-define(CURRSTATE, active).

%% ====================================================================
%% API functions
%% ====================================================================

init(Args, State) ->
    do_init(Args, State).

start(State) ->
    do_start(State).

attach_participant(Args, State) ->
    do_attach_incoming(Args, State).

% ----
stop_external(Reason, State) ->
    d(State, "stop_external(~p)", [Reason]),
    #state_conf{participants=Partcps, forks=Forks}=State,
    lists:foreach(fun({_,Side}) -> ?CONF_UTILS:send_bye(Side, State) end, Partcps),
    lists:foreach(fun({_,#{}=ForkCall}) -> ?CONF_UTILS:cancel_active_forks([maps:get(fork,ForkCall)], [], State) end, Forks),
    StopReason = ?CONF_UTILS:reason_external(Reason),
    ?CONF_UTILS:return_stopping(?CONF_UTILS:do_finalize_conf(State#state_conf{stopreason=StopReason})).

% ----
test(State) ->
    d(State, "test"),
    {next_state, ?CURRSTATE, State}.

% ----
build_tag_sync([_Req], State) ->
    {Tag,State1} = ?CONF_UTILS:generate_tag(State),
    Reply = Tag,
    {reply, Reply, ?CURRSTATE, State1}.

%% =========================

% ----
sip_cancel([CallId, _Req, _InviteReq], State) ->
    d(State, "sip_cancel, CallId=~120p", [CallId]),
    %
    do_detach(CallId, State).

% ----
sip_bye([CallId, Req], State) ->
    d(State, "sip_bye, CallId=~120p", [CallId]),
    State1 = ?CONF_UTILS:send_response(Req, ok, State),
    %
    do_detach(CallId, State1).

%% =========================

% ----
sip_reinvite([CallId, _Req]=P, State) ->
    d(State, "sip_reinvite, CallId=~120p", [CallId]),
    do_reinvite_incoming(P, State).

% ----
sip_notify([CallId, Req]=_P, _StateName, State) ->
    d(State, "sip_notify, CallId=~120p", [CallId]),
    State1 = ?CONF_UTILS:send_response(Req, ?Forbidden("CONF. Not implemented"), State),
    State1.

% ----
sip_info([CallId, Req], _StateName, State) ->
    d(State, "sip_info, CallId=~120p", [CallId]),
    % State1 = ?CONF_UTILS:send_response(Req, ?Forbidden("CONF. Not implemented"), State),
    State1 = ?CONF_UTILS:send_response(Req, {200,[]}, State),
    ?U:get_dtmf_from_sipinfo(Req,
                             fun(Dtmf) -> case ?CONF_UTILS:get_participant_by_callid(CallId,State) of
                                              #side{}=Side -> ?FSM_EVENT:dtmf(Side, Dtmf, State);
                                              _ -> ok
                                          end end,
                             fun() -> ok end),
    State1.

% ----
sip_message([CallId,Req]=_P, _StateName, State) ->
    d(State, "sip_message, CallId=~120p", [CallId]),
    State1 = ?CONF_UTILS:send_response(Req, ?Forbidden("CONF. Not implemented"), State),
    State1.

%% =========================

% ----
uas_dialog_response([_Resp]=P, State) ->
    #sipmsg{class={resp,SipCode,_},
            cseq={CSeq,Method},
            call_id=CallId}=_Resp,
    d(State, "uas_dialog_response ~p:~p -> ~p, CallId=~120p", [Method,CSeq,SipCode,CallId]),
    do_on_uas_dialog_response(P, State).

% ----
call_terminate([Reason, Call], State) ->
    #call{call_id=CallId}=Call,
    d(State, "call_terminate, CallId=~120p, Reason=~120p", [CallId, Reason]),
    %
    do_detach(CallId, State).

% ============================
% ----
fork_timeout([CallId, BLTag]=P, State) ->
    d(State, "fork_timeout BLTag=~120p, CallId=~120p", [BLTag, CallId]),
    do_on_fork_timeout(P, State).

% ----
uac_response([_Resp]=Args, State) ->
    #sipmsg{class={resp,SipCode,SipReason},
            cseq={CSeq,Method},
            call_id=CallId}=_Resp,
    d(State, "uac_response ~p:~p -> ~p ~p, CallId=~120p", [Method,CSeq,SipCode,SipReason,CallId]),
    do_on_uac_response(Args, State).

% ============================
mg_dtmf([CtxId, CtxTermId, #{dtmf:=Dtmf}=Event]=Args, State) ->
    ?LOGSIP("DTMF RFC2833/inband in conf: ~p from ~p", [Dtmf, CtxTermId]),
    case find_callid_by_termid(CtxTermId, State) of
        undefined ->
            ?OUT("  TERM NOT FOUND ~p", [CtxTermId]),
            {next_state, ?CURRSTATE, State};
        CallId ->
            _ = {CtxId, CtxTermId, Event, Args},
            case ?CONF_UTILS:get_participant_by_callid(CallId,State) of
                #side{}=Side -> ?FSM_EVENT:dtmf(Side, Dtmf, State);
                _ -> ok
            end,
            {next_state, ?CURRSTATE, State}
    end.

% ----
mg_event([_CtxId, CtxTermId, "endplay"=_EType, #{}=Event]=_Args, State) ->
    ?ACTIVE_PLAY:play_stopped(Event, CtxTermId, State);
mg_event([_CtxId, _CtxTermId, _EType, #{}=_Event]=_Args, State) ->
    ?LOGSIP("Event ~s in conf from ~p skipped", [_EType, _CtxTermId]),
    {next_state, ?CURRSTATE, State}.

% @private
find_callid_by_termid(CtxTermId, State) ->
    #state_conf{media=#media_conf{terms=Terms}}=State,
    lists:foldl(fun({K,#term_conf{term_id=TID}}, undefined) when TID==CtxTermId -> K;
                   (_, Acc) -> Acc
                end, undefined, Terms).

%% ====================================================================
%% Internal functions
%% ====================================================================

% --------
% when init conf
do_init(_Args, State) ->
    Ref = make_ref(),
    State1 = State#state_conf{ref=Ref,
                              timer_ref=erlang:send_after(?CONF_TIMEOUT, self(), ?CONF_TIMEOUT_MSG)},
    self() ! {start, Ref},
    State1.

% --------
% first queue message to make conference active (MG connect)
do_start(State) ->
    #state_conf{map=#{opts:=Opts}}=State,
    {XSide,State1} = case ?EU:extract_required_props([req], Opts) of
                         [#sipmsg{}] -> ?ACTIVE_UTILS:prepare_side(Opts, State);
                         _ -> {undefined,State}
                     end,
    case ?CONF_MEDIA:check_available(State1) of
        {error,mgc_overload} ->
            ?CONF_UTILS:error_mgc_final("MGC overload", State1, XSide, ?InternalError("CONF. MGC overload"));
        {error,_} ->
            ?CONF_UTILS:error_mgc_final("MGC not ready", State1, XSide, ?InternalError("CONF. MGC not ready"));
        {ok,false} ->
            ?CONF_UTILS:error_mgc_final("No media trunk available", State1, XSide, ?InternalError("CONF. No media trunk available"));
        {ok,true} ->
            case ?CONF_MEDIA:media_prepare_start(State1) of
                {error, Reply} ->
                    ?CONF_UTILS:error_mgc_final("", State1, XSide, Reply);
                {ok,State2} ->
                    ?FSM_EVENT:conf_start(State2),
                    d(State2, " -> created"),
                    do_start_1(XSide,State2)
            end
    end.
% @private
do_start_1(undefined, State) -> {next_state, ?CURRSTATE, State};
do_start_1(#side{}=XSide, State) ->
    State1 = case do_attach_incoming(XSide, State) of
                 {next_state,_,S}=R -> S;
                 {next_state,_,S,_}=R -> S
             end,
    case ?ACTIVE_UTILS:check_final(State1) of
        true -> ?CONF_UTILS:return_stopping(?CONF_UTILS:do_finalize_conf(State1));
        false -> R
    end.

% ----

% --------
% attach new participant to existing conference (could be replaceing), by incoming invite
do_attach_incoming([Opts]=_Args, State) when is_list(Opts) ->
    {XSide,State1} = ?ACTIVE_UTILS:prepare_side(Opts, State),
    do_attach_incoming(XSide, State1);

do_attach_incoming(#side{req=Req,callid=XCallId,localuri=#uri{domain=XDomain}}=XSide, #state_conf{confid=ConfId}=State) ->
    case ?ACTIVE_UTILS:check_replaces(Req, State) of
        {error,Reason} ->
            SipReply = ?CallLegTransDoesNotExist("CONF. Replacing failure (" ++ ?EU:to_list(Reason) ++ ")"),
            State1 = decline_incoming(XSide, SipReply, State),
            {next_state, ?CURRSTATE, State1};
        false ->
            ?CONF_UTILS:log_callid(incoming,ConfId,XCallId,XDomain), % #354, RP-415
            FunSuccess = fun(StateX) -> {next_state, ?CURRSTATE, StateX} end,
            ?STAT_FACADE:link_domain(XCallId, XDomain), % RP-415
            do_attach_incoming_1(XSide, State, FunSuccess);
        {ok,#side{callid=RCallId}=ReplacedSide} ->
            ?CONF_UTILS:log_callid(replaces,ConfId,XCallId,RCallId,XDomain), % #354, RP-415
            FunReplace = fun(StateX) ->
                                 ?STAT_FACADE:link_replaced(RCallId, XCallId), % @stattrace
                                 ?STAT_FACADE:link_domain(XCallId, XDomain), % RP-415
                                 do_detach_bye(RCallId, StateX)
                         end,
            XSide1 = XSide#side{sel_opts=ReplacedSide#side.sel_opts,
                                participantid=ReplacedSide#side.participantid}, % RP-1344
            do_attach_incoming_1(XSide1, State, FunReplace)
    end.
%
do_attach_incoming_1(#side{}=XSide, State, FunSuccess) ->
    case ?CONF_MEDIA:media_attach(XSide, State) of
        {ok, State1, LSdp} ->
            ?FSM_EVENT:call_attach(XSide, State1),
            #state_conf{confid=ConfId}=State2 = ?ACTIVE_UTILS:send_200ok_to_participant(LSdp, XSide, State1),
            d(State2, " -> attached"),
            #side{callid=XCallId}=XSide,
            % callid to store
            Self = self(),
            ?DLG_STORE:push_dlg(XCallId, {ConfId,Self}),
            ?MONITOR:append_fun(Self, fun() -> ?DLG_STORE:unlink_dlg(XCallId) end),
            % fun after success (replaces)
            FunSuccess(State2);
        {error, SipReply} ->
            State1 = decline_incoming(XSide, SipReply, State),
            {next_state, ?CURRSTATE, State1}
    end.

% @private
decline_incoming(XSide, SipReply, State) ->
    #side{callid=XCallId,
          rhandle=ReqHandle}=XSide,
    #state_conf{participants=Partcps}=State,
    State1 = State#state_conf{participants=lists:keydelete(XCallId, 1, Partcps)},
    ?CONF_UTILS:send_response(ReqHandle, SipReply, State1),
    State1.

% --------
% detach participant from conference and send bye (last should destroy conference)
do_detach_bye(CallId, State) ->
    case ?ACTIVE_UTILS:do_detach_bye(CallId, State) of
        stop ->
            StopReason = ?CONF_UTILS:reason_lastdetach(CallId),
            ?CONF_UTILS:return_stopping(?CONF_UTILS:do_finalize_conf(State#state_conf{stopreason=StopReason}));
        {ok,State1} ->
            {next_state, ?CURRSTATE, State1}
    end.
% detach participant from conference (last should destroy conference)
do_detach(CallId, State) ->
    case ?ACTIVE_UTILS:do_detach(CallId, State) of
        stop ->
            StopReason = ?CONF_UTILS:reason_lastdetach(CallId),
            ?CONF_UTILS:return_stopping(?CONF_UTILS:do_finalize_conf(State#state_conf{stopreason=StopReason}));
        {ok,State1} ->
            {next_state, ?CURRSTATE, State1}
    end.

% --------
% modifies participant's media by incoming reinvite
do_reinvite_incoming([CallId, #sipmsg{from={From,_}}=Req], State) ->
    #sdp{}=RSdp=?U:extract_sdp(Req),
    #state_conf{participants=Partcps}=State,
    case lists:keyfind(CallId, 1, Partcps) of
        false ->
            State1 = ?CONF_UTILS:send_response(Req, ?InternalError("CONF. Participant not found"), State),
            {next_state, ?CURRSTATE, State1};
        {_,Side} ->
            case ?CONF_MEDIA:media_sessionchange(Req, Side, State) of
                {error, SipReply} ->
                    State1 = ?CONF_UTILS:send_response(Req, SipReply, State),
                    {next_state, ?CURRSTATE, State1};
                {ok, State1, LSdp} ->
                    Opts = [{body, LSdp},
                            user_agent],
                    State2 = ?CONF_UTILS:send_response(Req, {200,Opts}, State1),
                    Side1 = Side#side{remotesdp=RSdp,
                                      remoteparty=?U:clear_uri(From)},
                    State3 = State2#state_conf{participants=lists:keystore(CallId, 1, Partcps, {CallId, Side1})},
                    check_event_sesschanged(Side, Req, State3), % !!! Side previous!
                    {next_state, ?CURRSTATE, State3}
            end
    end.

% @private
check_event_sesschanged(#side{remotesdp=RSdp1}=Side, #sipmsg{}=Msg, State) ->
    % modes
    #sdp{}=RSdp2=?U:extract_sdp(Msg),
    {MX1,MX2} = {?M_SDP:get_audio_mode(RSdp1), ?M_SDP:get_audio_mode(RSdp2)},
    M = case {MX1,MX2} of
            {<<"sendrecv">>,<<"sendonly">>} -> hold;
            {<<"sendrecv">>,<<"inactive">>} -> hold;
            {<<"sendonly">>,<<"sendrecv">>} -> unhold;
            {<<"inactive">>,<<"sendrecv">>} -> unhold;
            _ -> undefined
        end,
    % event
    case M of
        hold -> ?FSM_EVENT:hold(Side, State);
        unhold -> ?FSM_EVENT:unhold(Side, State);
        undefined -> ?FSM_EVENT:sesschanged(Side, State)
    end.

%% ====================================================================

%% -----
%% UAC fork timeout
%%
do_on_fork_timeout([CallId, _LTag]=P, State) ->
    #state_conf{forks=Forks}=State,
    case lists:keyfind(CallId,1,Forks) of
        false ->
            {next_state, ?CURRSTATE, State};
        {_,#side_fork{}=_Side} ->
            {next_state, ?CURRSTATE, State};
        {_,#{type:='refer'}=Refer} ->
            ?ACTIVE_REFER:fork_timeout(P, Refer, State);
        {_,#{type:='call'}=ForkCall} ->
            ?ACTIVE_CALL:fork_timeout(P, ForkCall, State)
    end.

%% -----
%% UAC response handlers
%%
do_on_uac_response([#sipmsg{cseq={_,'INVITE'}, call_id=CallId}=Resp]=P, State) ->
    #sipmsg{class={resp,SipCode,_}}=Resp,
    #state_conf{forks=Forks}=State,
    d(State, "uac_response('INVITE') ~p from Z: ~p", [SipCode, CallId]),
    case lists:keyfind(CallId,1,Forks) of
        false ->
            {next_state, ?CURRSTATE, State};
        {_,#side_fork{}=_Side} ->
            {next_state, ?CURRSTATE, State};
        {_,#{type:='refer'}=Refer} ->
            ?ACTIVE_REFER:uac_response(P, Refer, State);
        {_,#{type:='call'}=ForkCall} ->
            ?ACTIVE_CALL:uac_response(P, ForkCall, State)
    end;
%
do_on_uac_response([#sipmsg{cseq={_,'NOTIFY'}}=Resp], State) ->
    #sipmsg{class={resp,SipCode,_}, call_id=CallId}=Resp,
    d(State, "uac_response('NOTIFY') ~p from ~p", [SipCode, CallId]),
    {next_state, ?CURRSTATE, State};
%
do_on_uac_response([#sipmsg{cseq={_,Method}}=_Resp], State) ->
    d(State, "uac_response(~p) skipped", [Method]),
    {next_state, ?CURRSTATE, State}.

%% ====================================================================

%% ============================
%% UAS response, dialog ready
%% ============================

do_on_uas_dialog_response([#sipmsg{class={resp,SipCode,_}, cseq={_,'INVITE'}}=Resp], State)
  when SipCode >= 200, SipCode < 300 ->
    %#sipmsg{to={_,ToTag}}=Resp,
    {ok,XCallId} = nksip_response:call_id(Resp),
    {ok,XDlgHandle} = nksip_dialog:get_handle(Resp),
    #state_conf{participants=Partcps}=State,
    State1 = case lists:keytake(XCallId, 1, Partcps) of
                 {value,{_,#side{dir='in'}=XSide},Partcps1} ->
                     XSide1 = XSide#side{dhandle=XDlgHandle},
                     State#state_conf{participants=[{XCallId,XSide1}|Partcps1]};
                 _ ->
                     State
             end,
    {next_state, ?CURRSTATE, State1};
do_on_uas_dialog_response(_P, State) ->
    {next_state, ?CURRSTATE, State}.

%% =====================================================================

%% % @private test debug
%% test_topology(State) ->
%%     % bothway, oneway, isolate
%%     #state_conf{participants=Partcps}=State,
%%     case length(Partcps) of
%%         0 -> ok;
%%         1 -> ok;
%%         2 ->
%%             [{B,_},{A,_}]=Partcps,
%%             Topology = [{A,B,oneway}],
%%             ?CONF_MEDIA:modify_topology(State, Topology);
%%         _ ->
%%             [{C,_},{B,_},{A,_}|_]=Partcps,
%%             Topology = [{A,B,bothway},
%%                         {B,C,oneway}],
%%             ?CONF_MEDIA:modify_topology(State, Topology)
%%     end.

% @private ----
d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    #state_conf{confid=ConfId}=State,
    ?LOGSIP("CONF fsm ~p '~p':" ++ Fmt, [ConfId,?CURRSTATE] ++ Args).
