%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Outgoing call utils of 'active' state of CONF FSM

-module(r_sip_conf_fsm_active_call).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_conf.hrl").
-include("../include/r_sip_mgc.hrl").
-include("../include/r_sip_headers.hrl").

% -include("../include/r_nksip.hrl").

-define(CURRSTATE, 'active').

%% ====================================================================
%% API functions
%% ====================================================================

start_outgoing_call([To], State) ->
    PartcpId = ?EU:newid(),
    d(State, "Start outgoing call ~p as ~p", [To, PartcpId]),
    FRes = fun(_Result, ResState) -> {next_state, ?CURRSTATE, ResState} end,
    do_start_call([PartcpId,To], State, FRes).


%% ----------------------------
-spec sync_call_start(Args::list(), State::#state_conf{}) -> ok | {error, Reason::term()}.
%% ----------------------------
sync_call_start([PartcpId,To,Opts], State) ->
    %?OUT("Start call ~p as ~p", [To, PartcpId]),
    FRes = fun(Result, ResState) -> {reply, Result, ?CURRSTATE, ResState} end,
    do_start_call([PartcpId,To,Opts], State, FRes).

%% ----------------------------
-spec sync_call_stop(Args::list(), State::#state_conf{}) -> ok | {error, Reason::term()}.
%% ----------------------------
sync_call_stop([PartcpId], State) ->
    %?OUT("Stop call ~p", [PartcpId]),
    FRes = fun(NewStateName, Result, ResState) -> {reply, Result, NewStateName, ResState} end,
    do_stop_call([PartcpId], State, FRes).

%% ----------------------------
-spec sync_call_get_state(Args::list(), State::#state_conf{}) -> {ok, S::'online' | 'calling'} | {error, Reason::term()}.
%% ----------------------------
sync_call_get_state([PartcpId], State) ->
    %?OUT("Get call's state ~p", [PartcpId]),
    FRes = fun(Result, ResState) -> {reply, Result, ?CURRSTATE, ResState} end,
    do_get_call_state([PartcpId], State, FRes).

%% ----------------------------
-spec sync_call_set_mode(Args::list(), State::#state_conf{}) -> ok | {error, Reason::term()}.
%% ----------------------------
sync_call_set_mode([PartcpId, Opts], State) ->
    %?OUT("Set call's mode ~120p", [PartcpId]),
    FRes = fun(Result, ResState) -> {reply, Result, ?CURRSTATE, ResState} end,
    do_set_call_mode([PartcpId, Opts], State, FRes).

%% ----------------------------
fork_timeout(P, ForkCall, State) ->
    do_on_fork_timeout(P, ForkCall, State).

%% ----------------------------
uac_response(P, Refer, State) ->
    do_on_uac_response(P, Refer, State).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ================================================
%% Start refer inside
%% ================================================

%% ----
%% check participantid
do_start_call([PartcpId,_,_]=P, State, FRes) ->
    case check_participantid_exists(PartcpId, State) of
        false ->
            do_start_call_1(P, State, FRes);
        true ->
            Err = {error, {already_exists, <<"ParticipantId already exists">>}},
            FRes(Err, State)
    end.

%% ----
%% find b2bua
do_start_call_1(P, State, FRes) ->
    #state_conf{confid=ConfId}=State,
    ZCallId = ?CONF_UTILS:build_out_callid(State),
    case ?SERVERS:find_responsible_b2bua_nostore(ZCallId) of
        false ->
            FRes({error, {internal_error, <<"B2BUA server not found">>}}, State);
        RouteDomain ->
            ?DLG_STORE:push_dlg(ZCallId, {ConfId,self()}),
            ?MONITOR:append_fun(self(), fun() -> ?DLG_STORE:unlink_dlg(ZCallId) end),
            do_start_call_2(P, [ZCallId, RouteDomain], State, FRes)
    end.

%% ----
%% create media context
do_start_call_2(P, [ZCallId, RouteDomain], State, FRes) ->
    case ?CONF_MEDIA:media_add_outgoing(ZCallId, State) of
        {error, _}=Err -> FRes(Err, State);
        {ok, State1, ZLSdp} ->
            do_start_call_3(P, [ZCallId, RouteDomain, ZLSdp], State1, FRes)
    end.

%% ----
%% make invite options (inside cluster call)
do_start_call_3([PartcpId,To,Opts], [ZCallId, RouteDomain, ZLSdp], #state_conf{map=Map}=State, FRes) ->
    SrvIdxT = ?U:get_current_srv_textcode(),
    %
    #state_conf{room={FrS,FrU,FrD}}=State,
    % RP-1344
    ExtHeaders = ?EU:get_by_key(headers,Opts,[]),
    HeadersToAdd = [{add,{HK,HV}} || {HK,HV} <- ExtHeaders, is_binary(HK) andalso is_binary(HV)],
    %
    ZRToUri = case To of
                  {ToS,ToU,ToD} when is_atom(ToS), is_binary(ToU), is_binary(ToD) -> #uri{scheme=ToS, user=ToU, domain=ToD};
                  {ToU,ToD} when is_binary(ToU), is_binary(ToD) -> #uri{scheme=sip, user=ToU, domain=ToD};
                  ToU when is_binary(ToU) -> #uri{scheme=sip, user=ToU, domain=FrD};
                  ToU when is_integer(ToU) -> #uri{scheme=sip, user=?EU:to_binary(ToU), domain=FrD};
                  #uri{} -> To
              end,
    ZLUri = #uri{scheme=FrS, disp=?CONF_UTILS:make_conf_display(State), user=FrU, domain=FrD},
    {ZLTag,State1} = ?CONF_UTILS:generate_tag(State),
    ZLFromUri = ZLUri#uri{ext_opts=[{<<"tag">>,ZLTag}]},
    ZLContact = ?U:build_local_contact(ZLUri), % @localcontact (cfg=default! | RouteDomain!)
    %
    ?STAT_FACADE:link_fork(ZCallId), % @stattrace
    ?STAT_FACADE:link_domain(ZCallId,FrD), % RP-415
    ?CONF_UTILS:log_callid(outgoing,State#state_conf.confid,ZCallId,FrD), % #354, RP-415
    % RP-1344
    TimeoutDef1 = 60,
    TimeoutDef2 = ?EU:get_by_key(<<"calltimeout">>,maps:get(opts,Map),TimeoutDef1),
    Timeout = ?EU:to_int(?EU:get_by_key(<<"calltimeout">>, Opts, TimeoutDef2), TimeoutDef2) * 1000,
    %
    ?OUT("CONF call to ~p, callid=~p", [To, ZCallId]),
    Route = {route, ?U:unparse_uri(#uri{scheme=sip, domain=RouteDomain, opts=?CFG:get_transport_opts()++[<<"lr">>]})},
    CmnOpts = [{call_id,ZCallId},
               {from,ZLFromUri},
               {to,ZRToUri},
               {contact,ZLContact},
               Route,
               user_agent],
    InviteOpts = [{body,ZLSdp},
                  auto_2xx_ack,
                  %record_route, % #244
                  {cseq_num,1},
                  {add, {?OwnerHeader, <<"rCF-", SrvIdxT/bitstring>>}},
                  {add, {?CallerTypeHeader, <<"conference">>}} | HeadersToAdd],
    %
    ZFork = #side_fork{callid=ZCallId,
                       participantid=PartcpId,
                       sel_opts=?ACTIVE_UTILS:init_sel_opts(State,Opts),
                       %
                       localuri=ZLFromUri,localtag=ZLTag,localcontact=ZLContact,
                       remoteuri=ZRToUri,
                       requesturi=ZRToUri,
                       rule_timeout=Timeout,
                       cmnopts=CmnOpts,
                       inviteopts=InviteOpts,
                       starttime=os:timestamp()},
    %
    do_start_call_invite(ZFork, State1, FRes).


%% ---------------------------
%% stop existing call / participant
%% ---------------------------
do_stop_call([PartcpId], State, FRes) ->
    #state_conf{participants=Partcps, forks=Forks}=State,
    F = fun({_,#side{participantid=C,callid=CallId}=Side}, {SideAcc,StateAcc}) when C==PartcpId ->
                case ?ACTIVE_UTILS:do_detach_bye(CallId, StateAcc) of
                    stop -> {[{stop,CallId},Side|SideAcc],StateAcc};
                    {ok,StateAcc1} -> {[Side|SideAcc],StateAcc1}
                end;
           ({_,#{}=ForkCall}, {SideAcc,StateAcc}=Acc) ->
                case maps:get(fork,ForkCall) of
                    #side_fork{participantid=C}=Fork when C==PartcpId ->
                        ?CONF_UTILS:cancel_active_forks([Fork], [], StateAcc),
                        {[Fork|SideAcc],StateAcc};
                    _ -> Acc
                end;
           ({_,_},Acc) -> Acc
        end,
    {R1,State1} = lists:foldl(F, {[],State}, Partcps),
    {R2,State2} = lists:foldl(F, {R1,State1}, Forks),
    case R2 of
        [] ->
            Err = {error, {not_found, <<"Call/Participant not found">>}},
            FRes(?CURRSTATE, Err, State2);
        [_|_]=L ->
            case lists:keyfind(stop, 1, L) of
                false -> FRes(?CURRSTATE, ok, State2);
                {_,CallId} ->
                    StopReason = ?CONF_UTILS:reason_lastdetach(CallId),
                    {_,_,State3} = ?CONF_UTILS:return_stopping(?CONF_UTILS:do_finalize_conf(State2#state_conf{stopreason=StopReason})),
                    FRes(?STOPPING_STATE, ok, State3)
            end end.

%% ---------------------------
%% get existing call / participant 's state
%% ---------------------------
do_get_call_state([PartcpId], State, FRes) ->
    #state_conf{participants=Partcps, forks=Forks}=State,
    F = fun(_, Acc) when Acc/=undefined -> Acc;
           ({_,#side{participantid=C}=Side}, _) when C==PartcpId -> Side;
           ({_,#{}=ForkCall}, Acc) ->
                case maps:get(fork,ForkCall) of
                    #side_fork{participantid=C}=Fork when C==PartcpId -> Fork;
                    _ -> Acc
                end;
           ({_,_},Acc) -> Acc
        end,
    Reply = case lists:foldl(F, undefined, Partcps ++ Forks) of
                undefined -> {error, {not_found, <<"Call/Participant not found">>}};
                #side_fork{} -> {ok,'calling'};
                #side{} -> {ok,'online'}
            end,
    FRes(Reply, State).

%% ---------------------------
%% set call / participant 's mode
%% ---------------------------
do_set_call_mode([PartcpId, Opts], State, FRes) ->
    [Spk,Mic] = ?EU:extract_optional_props([<<"spk">>,<<"mic">>], Opts),
    #state_conf{participants=Partcps, forks=Forks}=State,
    F = fun(_, {Acc,StateAcc}) when Acc/=undefined -> {Acc,StateAcc};
           ({K,#side{participantid=C,sel_opts=SelOpts}=Side}, {_,StateAcc}) when C==PartcpId ->
                Spk1 = case Spk of undefined -> SelOpts#sel_opts.spk; _ -> ?EU:to_bool(Spk) end,
                Mic1 = case Mic of undefined -> SelOpts#sel_opts.mic; _ -> ?EU:to_bool(Mic) end,
                Mode = ?M_SDP:build_media_mode([Spk1,Mic1]),
                case ?CONF_MEDIA:media_modify_mode(StateAcc, Side, Mode) of
                    {error,_}=Err -> {Err,StateAcc};
                    {ok,#state_conf{participants=PartcpsX}=StateX} ->
                        SideX = Side#side{sel_opts=SelOpts#sel_opts{mode_max=Mode,
                                                                    spk = Spk1,
                                                                    mic = Mic1}},
                        {SideX,StateX#state_conf{participants=lists:keyreplace(K, 1, PartcpsX, {K,SideX})}}
                end;
           ({K,#{}=ForkCall}, {Acc,StateAcc}) ->
                case maps:get(fork,ForkCall) of
                    #side_fork{participantid=C}=Fork when C==PartcpId ->
                        Mode = ?M_SDP:build_media_mode([Spk,Mic]),
                        #side_fork{sel_opts=SelOpts}=Fork,
                        #state_conf{forks=ForksX}=StateAcc,
                        ForkX = Fork#side_fork{sel_opts=SelOpts#sel_opts{mode_max=Mode}},
                        ForkCallX = ForkCall#{fork=>ForkX},
                        {ForkX,StateAcc#state_conf{forks=lists:keyreplace(K, 1, ForksX, {K,ForkCallX})}};
                    _ -> {Acc,StateAcc}
                end;
           ({_,_},Acc) -> Acc
        end,
    {Res,State1} = lists:foldl(F, {undefined,State}, Partcps ++ Forks),
    Reply = case Res of
                undefined -> {error, {not_found, <<"Call/Participant not found">>}};
                {error,_}=Err -> Err;
                #side_fork{} -> ok; %{error, {not_implemented, <<"Call/Participant in calling state">>}}; % #451(b)
                #side{} -> ok
            end,
    FRes(Reply, State1).

%% =================================================
%% Invite services
%% =================================================

%% -----
%% send invite and initialize state
%%
do_start_call_invite(Fork, State, FRes) ->
    ?FSM_EVENT:fork_start(Fork, State),
    #state_conf{forks=Forks}=State,
    #side_fork{callid=ZCallId}=Fork,
    case send_invite(Fork, State) of
        {error,Err2}=Err1 ->
            {ok, State1} = ?CONF_MEDIA:media_detach(ZCallId, State),
            Err = case Err2 of {error,_} -> Err2; _ -> Err1 end,
            FRes(Err, State1);
        {ok, Fork1} ->
            ForkCall = #{type => 'call',
                         fork => Fork1,
                         zcallid => ZCallId,
                         startgs => ?EU:current_gregsecond()},
            State1 = State#state_conf{forks=[{ZCallId,ForkCall}|Forks]},
            FRes(ok, State1)
    end.

%% ----
%% sends invite to Z
%%
send_invite(Fork, State) ->
    #state_conf{map=#{app:=App}}=State,
    #side_fork{callid=ZCallId,
               requesturi=Uri,
               localtag=ZLTag,
               rule_timeout=Timeout,
               cmnopts=CmnOpts,
               inviteopts=InviteOpts}=Fork,
    %
    Now = os:timestamp(),
    %
    case catch nksip_uac:invite(App, Uri, [async|CmnOpts++InviteOpts]) of
        {'EXIT',Err} ->
            d(State, "invite_fork BLTag=~p, Caught error=~120p", [ZLTag, Err]),
            {error, Err};
        {error,_R}=Err ->
            d(State, "invite_fork BLTag=~p, Error=~120p", [ZLTag, Err]),
            {error, Err};
        {async,ReqHandle} ->
            d(State, "invite_fork BLTag=~p", [ZLTag]),
            % @todo account early side b
            Fork1 = Fork#side_fork{rhandle=ReqHandle,
                                   starttime=Now,
                                   rule_timer_ref=erlang:send_after(Timeout, self(), {?FORK_TIMEOUT_MSG, [ZCallId, ZLTag]})},
            {ok, Fork1}
    end.

%% ==================================================
%%
%% ==================================================

%% -------------------------------------
%% UAC fork timeout
%%
do_on_fork_timeout([CallId,_LTag], ForkCall, State) ->
    case maps:get(fork,ForkCall) of
        #side_fork{}=Fork -> ?CONF_UTILS:cancel_active_forks([Fork], [], State); % RP-1344
        _ -> ok
    end,
    fork_error(CallId, ForkCall, {timeout}, State).

%% -------------------------------------
%% UAC response (only invite filtered)
%%
do_on_uac_response([#sipmsg{cseq={_,'INVITE'}, call_id=CallId}=Req]=_P, ForkCall, State) ->
    fork_invite_response([CallId, Req], ForkCall, State).

%% -------------------------------------
%% @private Fork invite response
%% -------------------------------------
fork_invite_response([_CallId, #sipmsg{class={resp,SipCode,_}}=_Resp]=P, ForkCall, State) when SipCode >= 100, SipCode < 200 ->
    fork_invite_response_1xx(P,ForkCall,State);

fork_invite_response([_CallId, #sipmsg{class={resp,SipCode,_}}=_Resp]=P, ForkCall, State) when SipCode >= 200, SipCode < 300 ->
    fork_invite_response_2xx(P,ForkCall,State);

fork_invite_response([CallId, #sipmsg{class={resp,SipCode,_}}=_Resp], ForkCall, State) when SipCode >= 300, SipCode < 400 ->
    fork_error(CallId, ForkCall, {response,SipCode}, State);

fork_invite_response([CallId, #sipmsg{class={resp,SipCode,_}}=_Resp], ForkCall, State) when SipCode >= 400, SipCode < 700 ->
    fork_error(CallId, ForkCall, {response,SipCode}, State);

fork_invite_response([CallId, #sipmsg{class={resp,SipCode,_}}=_Resp], ForkCall, State) ->
    fork_error(CallId, ForkCall, {response,SipCode}, State).

%% -------------------------------------
%% @private Fork invite response 1xx
%% -------------------------------------

fork_invite_response_1xx([CallId,#sipmsg{class={resp,SipCode,SipReason}}=Resp],ForkCall,State) ->
    #state_conf{forks=Forks}=State,
    Fork=maps:get(fork,ForkCall),
    Fork1 = ?ACTIVE_UTILS:update_side_fork(Fork,Resp),
    ForkCall1 = ForkCall#{fork:=Fork1},
    State1 = State#state_conf{forks=lists:keyreplace(CallId, 1, Forks, {CallId,ForkCall1})},
    case SipCode/=100 of
        true -> ?FSM_EVENT:fork_preanswer(Fork1, {resp,SipCode,SipReason}, State1);
        false -> ok
    end,
    case ?U:extract_sdp(Resp) of
        #sdp{}=RSdp -> fork_invite_response_1xx_connect([CallId,Resp,RSdp],ForkCall1,State1);
        _ -> {next_state, ?CURRSTATE, State1}
    end.

% @private
fork_invite_response_1xx_connect([CallId,_Resp,RSdp],ForkCall,State) ->
    #state_conf{forks=Forks}=State,
    #side_fork{rhandle=ReqHandle}=Fork=maps:get(fork,ForkCall),
    case ?CONF_MEDIA:media_update_outgoing_by_remote(Fork, RSdp, State) of
        {error,_} ->
            State1 = ?CONF_UTILS:send_response(ReqHandle, ?InternalError("CONF. Attach media error"), State),
            State2 = State1#state_conf{forks=lists:keydelete(CallId,1,Forks)},
            case ?ACTIVE_UTILS:check_final(State2) of
                true -> ?CONF_UTILS:return_stopping(?CONF_UTILS:do_finalize_conf(State2));
                false -> {next_state, ?CURRSTATE, State2}
            end;
        {ok, State1} -> {next_state, ?CURRSTATE, State1}
    end.

%% -------------------------------------
%% @private Fork invite response 2xx
%% -------------------------------------
fork_invite_response_2xx([CallId,Resp],ForkCall,State) ->
    #state_conf{forks=Forks,
                participants=Partcps}=State,
    #sipmsg{class={resp,SipCode,SipReason}}=Resp,
    Fork = maps:get(fork,ForkCall),
    ?FSM_EVENT:fork_answer(Fork, {resp,SipCode,SipReason}, State),
    Side = ?ACTIVE_UTILS:make_side_by_fork(Fork,Resp),
    State1 = State#state_conf{forks=lists:keydelete(CallId, 1, Forks),
                              participants=[{CallId,Side}|Partcps]},
    #sdp{}=RSdp=?U:extract_sdp(Resp),
    case ?CONF_MEDIA:media_update_outgoing_by_remote(Fork, RSdp, State1) of
        {error,_} ->
            case ?ACTIVE_UTILS:do_detach_bye(CallId, State1) of
                stop -> ?CONF_UTILS:return_stopping(?CONF_UTILS:do_finalize_conf(State1));
                {ok,State2} -> {next_state, ?CURRSTATE, State2}
            end;
        {ok, State2} ->
            ?FSM_EVENT:call_attach(Side, State2),
            {next_state, ?CURRSTATE, State2}
    end.

%% -------------------------------------
%% @private Delete fork on error
%% -------------------------------------
fork_error(CallId, ForkCall, ForkResult, State) ->
    {ok, State1} = ?CONF_MEDIA:media_detach(CallId, State),
    #state_conf{forks=Forks}=State1,
    State2 = case lists:keytake(CallId, 1, Forks) of
                 false -> State1;
                 {value,_,Forks1} ->
                     Fork = maps:get(fork,ForkCall),
                     ?FSM_EVENT:fork_stop(Fork#side_fork{res=ForkResult,
                                                     finaltime=os:timestamp()}, State1),
                     % remove fork
                     #side_fork{rule_timer_ref=TimerRef}=Fork,
                     erlang:cancel_timer(TimerRef),
                     ?DLG_STORE:unlink_dlg(CallId), % #304
                     State1#state_conf{forks=Forks1}
             end,
    case ?ACTIVE_UTILS:check_final(State2) of
        true -> ?CONF_UTILS:return_stopping(?CONF_UTILS:do_finalize_conf(State2));
        false -> {next_state, ?CURRSTATE, State2}
    end.

%% ==================================================
%% ==================================================

% returns true if PartcpId exists among forks and active participants, otherwise false
check_participantid_exists(PartcpId, State) ->
    #state_conf{forks=Forks, participants=Partcps}=State,
    F = fun(_, true) -> true;
           ({_,#side{participantid=C}}, _) when C==PartcpId -> true;
           ({_,#{}=ForkCall}, _) ->
                case maps:get(fork,ForkCall) of
                    #side_fork{participantid=C} when C==PartcpId -> true;
                    _ -> false
                end;
           ({_,_},Acc) -> Acc
        end,
    lists:foldl(F, false, Partcps ++ Forks).

% @private ----
d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    #state_conf{confid=ConfId}=State,
    ?LOGSIP("CONF fsm ~p '~p':" ++ Fmt, [ConfId,?CURRSTATE] ++ Args).
