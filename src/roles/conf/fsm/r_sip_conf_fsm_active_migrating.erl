%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Migrating utils of 'active' state of CONF FSM

-module(r_sip_conf_fsm_active_migrating).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([migrate/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_conf.hrl").
-include("../include/r_sip_mgc.hrl").

% -include("../include/r_nksip.hrl").

-define(CURRSTATE, active).

%% ====================================================================
%% API functions
%% ====================================================================

migrate(State) ->
    d(State, "migrate"),
    do_migrate(State).

%% ====================================================================
%% Internal functions
%% ====================================================================

do_migrate(State) ->
    % @TODO
    %  outgoing reinvite to all participants,
    %  cancel forks, new invite to forks,
    %  back to active,
    %  wait for answers/timeouts
    %    491 -> to resend reinvite,
    %    4xx,5xx,6xx - participant detach
    StopReason = ?CONF_UTILS:reason_error([migrate], <<>>, <<"Migration not implemented">>),
    ?ACTIVE:stop_external(StopReason, State).

% @private ----
d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    #state_conf{confid=ConfId}=State,
    ?LOGSIP("CONF fsm ~p '~p':" ++ Fmt, [ConfId,?CURRSTATE] ++ Args).
