%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 16.06.2016
%%% @doc Player utils of 'active' state of CONF FSM

-module(r_sip_conf_fsm_active_play).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([sync_start_play/2,
         play_stopped/3]).

%-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_conf.hrl").
-include("../include/r_sip_mgc.hrl").

% -include("../include/r_nksip.hrl").

-define(CURRSTATE, 'active').

%% ====================================================================
%% API functions
%% ====================================================================

%% ---
-spec sync_start_play(Args::list(), State::#state_conf{}) -> ok | {error, Reason::term()}.
sync_start_play([PlayerId, File, Opts], State) ->
    % Opts: [{{A,B},Enabled},...]
    case start_play([PlayerId, File, Opts], State) of
        {error,_}=Reply -> State1=State;
        {ok,State1} -> Reply = ok
    end,
    {reply, Reply, ?CURRSTATE, State1}.

%% --
play_stopped(_Event, CtxTermId, State) ->
    State2 = case ?CONF_MEDIA:media_find_playerid(CtxTermId, State) of
                 undefined -> State;
                 {ok,'$init'} -> State;
                 {ok,PlayerId} ->
                     O = #{playerid => PlayerId},
                     case ?CONF_MEDIA:media_detach_player(O, State) of
                         {error,_} -> State;
                         {ok,State1} -> State1
                     end
             end,
    {next_state, ?CURRSTATE, State2}.

%% ====================================================================
%% Internal functions
%% ====================================================================

start_play([PlayerId, File, Opts], State) ->
    O = maps:without([playerid, mode],
                     maps:from_list(
                       lists:filter(fun({_,_}) -> true; (_) -> false end,
                                    Opts))),
    O1 = O#{playerid => PlayerId,
              mode => 'play'},
    O2 = case File of
             L when is_list(L), length(L) > 0 -> O1#{file => File};
             _ -> O1
         end,
    ?CONF_MEDIA:media_attach_player(O2, State).
