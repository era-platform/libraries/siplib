%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Refer utils of 'active' state of CONF FSM

-module(r_sip_conf_fsm_active_refer).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_conf.hrl").
-include("../include/r_sip_mgc.hrl").
-include("../include/r_sip_headers.hrl").

% -include("../include/r_nksip.hrl").

-define(CURRSTATE, 'active').
-define(TimeoutReferredCall, 185000). % more than 183 fork timeout

%% ====================================================================
%% API functions
%% ====================================================================

sip_refer([_CallId, _Req]=P, State) ->
    d(State, "sip_refer, CallId=~120p", [_CallId]),
    do_start_refer(P, State).

fork_timeout(P, Refer, State) ->
    do_on_fork_timeout(P, Refer, State).

uac_response(P, Refer, State) ->
    do_on_uac_response(P, Refer, State).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ================================================
%% Start refer inside
%% ================================================

do_start_refer([CallId, Req], State) ->
    #state_conf{participants=Partcps}=State,
    case lists:keyfind(CallId, 1, Partcps) of
        false ->
            State1 = ?CONF_UTILS:send_response(Req, ?CallLegTransDoesNotExist("CONF. Participant not found"), State),
            {next_state, ?CURRSTATE, State1};
        {_,XSide} ->
            %State1 = ?CONF_UTILS:send_response(Req, ?InternalError("CONF. Not implemented"), State),
            %{next_state, ?CURRSTATE, State1}
            {_XRefBy,_XRefTo,_RefOpts}=RefP = ?U:parse_refer_headers(Req),
            P = [CallId, Req, RefP],
            do_start_refer_1(P, XSide, State)
    end.

%% ----
%% check if not yet referred
do_start_refer_1([_,Req,_]=P, XSide, State) ->
    #state_conf{forks=Forks}=State,
    #side{refer_callid=ZCallId}=XSide,
    case ZCallId of
        undefined ->
            do_start_refer_2(P, XSide, State);
        _ ->
            case lists:keyfind(ZCallId,1,Forks) of
                false -> do_start_refer_2(P, XSide, State);
                _ ->
                    State1 = ?CONF_UTILS:send_response(Req, ?Forbidden("CONF. Refer already in work"), State),
                    {next_state, ?CURRSTATE, State1}
            end
    end.

%% ----
%% find b2bua
do_start_refer_2([_,Req,_]=P, XSide, State) ->
    #state_conf{confid=ConfId}=State,
    ZCallId = ?CONF_UTILS:build_out_callid(State),
    case ?SERVERS:find_responsible_b2bua_nostore(ZCallId) of
        false ->
            ?CONF_UTILS:send_response(Req, ?ServiceUnavailable("CONF. Responsible servers not found"), State),
            {next_state, ?CURRSTATE, State};
        RouteDomain ->
            ?DLG_STORE:push_dlg(ZCallId, {ConfId,self()}),
            ?MONITOR:append_fun(self(), fun() -> ?DLG_STORE:unlink_dlg(ZCallId) end),
            do_start_refer_3(P, [ZCallId, RouteDomain], XSide, State)
    end.

%% ----
%% create media context
do_start_refer_3([_, Req, _]=P, [ZCallId, RouteDomain], XSide, State) ->
    case ?CONF_MEDIA:media_add_outgoing(ZCallId, State) of
        {error, Reason} ->
            R = case is_list(Reason) of true -> Reason; false -> "unknown" end,
            ?CONF_UTILS:send_response(Req, ?InternalError("CONF. Media error req: " ++ R), State),
            {next_state, ?CURRSTATE, State};
        {ok, State1, ZLSdp} ->
            do_start_refer_4(P, [ZCallId, RouteDomain, ZLSdp], XSide, State1)
    end.

%% ----
%% make invite options (inside cluster call)
do_start_refer_4([CallId,_,RefP]=P, [ZCallId, RouteDomain, ZLSdp], XSide, State) ->
    SrvIdxT = ?U:get_current_srv_textcode(),
    %
    {_XRefBy, XRefTo, RefOpts} = RefP,
    ZRToUri = ?U:clear_uri(XRefTo),
    %
    #state_conf{room={_,U,D}}=State,
    ZLUri = #uri{scheme=sip,user=U,domain=D},
    {ZLTag,State1} = ?CONF_UTILS:generate_tag(State),
    ZLFromUri = ZLUri#uri{ext_opts=[{<<"tag">>,ZLTag}]},
    ZLContact = ?U:build_local_contact(ZLUri), % @localcontact (cfg=default! | RouteDomain!)
    %
    ?STAT_FACADE:link_fork(CallId, ZCallId), % @stattrace
    ?STAT_FACADE:link_domain(ZCallId,ZLFromUri#uri.domain), % RP-415
    ?CONF_UTILS:log_callid(referred,State#state_conf.confid,ZCallId,XSide#side.callid,D), % #354, RP-415
    Timeout = ?TimeoutReferredCall,
    %
    ?OUT("CONF refer call to ~p, callid=~p", [XRefTo, ZCallId]),
    Route = {route, ?U:unparse_uri(#uri{scheme=sip, domain=RouteDomain, opts=?CFG:get_transport_opts()++[<<"lr">>]})},
    CmnOpts = [{call_id,ZCallId},
               {from,ZLFromUri},
               {to,ZRToUri},
               {contact,ZLContact},
               Route,
               user_agent],
    InviteOpts = [{body,ZLSdp},
                  auto_2xx_ack,
                  %record_route, % #244
                  {cseq_num,1},
                  {add, {?OwnerHeader, <<"rCF-", SrvIdxT/bitstring>>}},
                  {add, {?CallerTypeHeader, <<"conference">>}}
                 | RefOpts],
    %
    ZFork = #side_fork{callid=ZCallId,
                       participantid=XSide#side.participantid,
                       sel_opts=XSide#side.sel_opts,
                       %
                       localuri=ZLFromUri,localtag=ZLTag,localcontact=ZLContact,
                       remoteuri=ZRToUri,
                       requesturi=ZRToUri,
                       rule_timeout=Timeout,
                       cmnopts=CmnOpts,
                       inviteopts=InviteOpts,
                       starttime=os:timestamp()},
    %
    do_start_refer_invite(P, [ZFork, XSide], State1).

%% =================================================
%% Invite services
%% =================================================

%% -----
%% send invite and initialize state
%%
do_start_refer_invite([CallId, Req, _], [Fork, XSide], State) ->
    ?FSM_EVENT:fork_start(Fork, State),
    #state_conf{forks=Forks,
                participants=Partcps}=State,
    #sipmsg{event=Ev}=Req,
    #side_fork{callid=ZCallId}=Fork,
    case send_invite(Fork, State) of
        {error,_Err} ->
            ?CONF_UTILS:send_response(Req, ?InternalError("CONF. Send invite error"), State),
            {ok, State1} = ?CONF_MEDIA:media_detach(ZCallId, State),
            {next_state, ?CURRSTATE, State1};
        {ok, Fork1} ->
            Refer = #{type => 'refer',
                      fork => Fork1,
                      xcallid => CallId,
                      xreq => Req,
                      x => #side{dhandle=XSide#side.dhandle,
                                 localcontact=XSide#side.localcontact,
                                 localuri=XSide#side.localuri,
                                 localtag=XSide#side.localtag,
                                 remoteuri=XSide#side.remoteuri,
                                 remotetag=XSide#side.remotetag},
                      xnotify => case Ev of {<<"refer">>,_} -> true; _ -> false end,
                      zcallid => ZCallId,
                      startgs => ?EU:current_gregsecond()},
            %
            AnswerOpts = [{expires, ?REFER_TIMEOUT div 1000},
                          user_agent],
            State1 = ?CONF_UTILS:send_response(Req, {202,AnswerOpts}, State),
            State2 = State1#state_conf{forks=[{ZCallId,Refer}|Forks],
                                       participants=lists:keyreplace(CallId,1,Partcps,{CallId,XSide#side{refer_callid=ZCallId}})},
            {next_state, ?CURRSTATE, State2}
    end.

%% ----
%% sends invite to Z
%%
send_invite(Fork, State) ->
    #state_conf{map=#{app:=App}}=State,
    #side_fork{callid=ZCallId,
               requesturi=Uri,
               localtag=ZLTag,
               rule_timeout=Timeout,
               cmnopts=CmnOpts,
               inviteopts=InviteOpts}=Fork,
    %
    Now = os:timestamp(),
    %
    case catch nksip_uac:invite(App, Uri, [async|CmnOpts++InviteOpts]) of
        {'EXIT',Err} ->
            d(State, "invite_fork BLTag=~p, Caught error=~120p", [ZLTag, Err]),
            {error, Err};
        {error,_R}=Err ->
            d(State, "invite_fork BLTag=~p, Error=~120p", [ZLTag, Err]),
            {error, Err};
        {async,ReqHandle} ->
            d(State, "invite_fork BLTag=~p", [ZLTag]),
            % @todo account early side b
            Fork1 = Fork#side_fork{rhandle=ReqHandle,
                                   starttime=Now,
                                   rule_timer_ref=erlang:send_after(Timeout, self(), {?FORK_TIMEOUT_MSG, [ZCallId, ZLTag]})},
            {ok, Fork1}
    end.

%% ----
%% sends notify event to X (containing Z response line)
%%
send_notify(_,_,#{xnotify:=false}=_Refer,State) -> {ok,State};
send_notify(Body, SubscrState, Refer, State) ->
    #{x:=XSide,
      startgs:=StartGS}=Refer,
    Opts = [{content_type, <<"message/sipfrag">>},
            {event, <<"refer">>},
            {subscription_state, case SubscrState of
                                     active -> {active, erlang:max(0, 180-(?EU:current_gregsecond()-StartGS))};
                                     terminated -> {terminated, noresource}
                                 end},
            {body, Body},
            %record_route, % #244
            user_agent],
    #side{dhandle=XDlgHandle}=XSide,
    catch nksip_uac:notify(XDlgHandle, [async|Opts]),
    {ok,State}.

%% ==================================================
%%
%% ==================================================

%% -------------------------------------
%% UAC fork timeout
%%
do_on_fork_timeout([CallId,_LTag], Refer, State) ->    
    case maps:get(fork,Refer) of
        #side_fork{}=Fork -> ?CONF_UTILS:cancel_active_forks([Fork], [], State); % RP-1344
        _ -> ok
    end,
    fork_error(CallId, Refer, {timeout}, State).

%% -------------------------------------
%% UAC response (only invite filtered)
%%
do_on_uac_response([#sipmsg{cseq={_,'INVITE'}, call_id=CallId}=Req]=_P, Refer, State) ->
    fork_invite_response([CallId, Req], Refer, State).

%% -------------------------------------
%% @private Fork invite response
%% -------------------------------------
fork_invite_response([_CallId, #sipmsg{class={resp,SipCode,Phrase}}=_Resp]=P, Refer, State) when SipCode >= 100, SipCode < 200 ->
    send_notify(?U:get_response_string(SipCode,Phrase), active, Refer, State),
    fork_invite_response_1xx(P,Refer,State);

fork_invite_response([_CallId, #sipmsg{class={resp,SipCode,Phrase}}=_Resp]=P, Refer, State) when SipCode >= 200, SipCode < 300 ->
    send_notify(?U:get_response_string(SipCode,Phrase), terminated, Refer, State),
    fork_invite_response_2xx(P,Refer,State);

fork_invite_response([CallId, #sipmsg{class={resp,SipCode,_}}=_Resp], Refer, State) when SipCode >= 300, SipCode < 400 ->
    send_notify(?U:get_response_string(500,"Internal Server Error"), terminated, Refer, State),
    fork_error(CallId, Refer, {response,SipCode}, State);

fork_invite_response([CallId, #sipmsg{class={resp,SipCode,Phrase}}=_Resp], Refer, State) when SipCode >= 400, SipCode < 700 ->
    send_notify(?U:get_response_string(SipCode,Phrase), terminated, Refer, State),
    fork_error(CallId, Refer, {response,SipCode}, State);

fork_invite_response([CallId, #sipmsg{class={resp,SipCode,Phrase}}=_Resp], Refer, State) ->
    send_notify(?U:get_response_string(SipCode,Phrase), terminated, Refer, State),
    fork_error(CallId, Refer, {response,SipCode}, State).

%% -------------------------------------
%% @private Fork invite response 1xx
%% -------------------------------------
fork_invite_response_1xx([CallId,#sipmsg{class={resp,SipCode,SipReason}}=Resp],ForkCall,State) ->
    #state_conf{forks=Forks}=State,
    Fork=maps:get(fork,ForkCall),
    Fork1 = ?ACTIVE_UTILS:update_side_fork(Fork,Resp),
    ForkCall1 = ForkCall#{fork:=Fork1},
    State1 = State#state_conf{forks=lists:keyreplace(CallId, 1, Forks, {CallId,ForkCall1})},
    case SipCode/=100 of
        true -> ?FSM_EVENT:fork_preanswer(Fork1, {resp,SipCode,SipReason}, State1);
        false -> ok
    end,
    case ?U:extract_sdp(Resp) of
        undefined -> {next_state, ?CURRSTATE, State};
        #sdp{}=RSdp -> fork_invite_response_1xx_connect([CallId,Resp,RSdp],ForkCall,State)
    end.

% @private
fork_invite_response_1xx_connect([CallId,#sipmsg{}=Resp,RSdp],ForkCall,State) ->
    #state_conf{forks=Forks}=State,
    #side_fork{rhandle=ReqHandle}=Fork=maps:get(fork,ForkCall),
    case ?CONF_MEDIA:media_update_outgoing_by_remote(Fork, RSdp, State) of
        {error,_} ->
            State1 = ?CONF_UTILS:send_response(ReqHandle, ?InternalError("CONF. Attach media error"), State),
            State2 = State1#state_conf{forks=lists:keydelete(CallId,1,Forks)},
            case ?ACTIVE_UTILS:check_final(State2) of
                true -> ?CONF_UTILS:return_stopping(?CONF_UTILS:do_finalize_conf(State2));
                false -> {next_state, ?CURRSTATE, State2}
            end;
        {ok, State1} ->
            Fork1 = ?ACTIVE_UTILS:update_side_fork(Fork,Resp),
            State2 = State1#state_conf{forks=lists:keyreplace(CallId, 1, Forks, {CallId,ForkCall#{fork:=Fork1}})},
             {next_state, ?CURRSTATE, State2}
    end.

%% -------------------------------------
%% @private Fork invite response 2xx
%% -------------------------------------
fork_invite_response_2xx([ZCallId,ZResp],Refer,State) ->
    #state_conf{forks=Forks,
                participants=Partcps}=State,
    #sipmsg{class={resp,SipCode,SipReason}}=ZResp,
    #{fork:=ZFork,
      xcallid:=XCallId}=Refer,
    ?FSM_EVENT:fork_answer(ZFork, {resp,SipCode,SipReason}, State),
    ZSide = ?ACTIVE_UTILS:make_side_by_fork(ZFork,ZResp),
    State1 = State#state_conf{forks=lists:keydelete(ZCallId, 1, Forks),
                              participants=[{ZCallId,ZSide}|Partcps]},
    #sdp{}=ZRSdp=?U:extract_sdp(ZResp),
    case ?CONF_MEDIA:media_update_outgoing_by_remote(ZFork, ZRSdp, State1) of
        {error,_} ->
            case ?ACTIVE_UTILS:do_detach_bye(ZCallId, State1) of
                stop -> ?CONF_UTILS:return_stopping(?CONF_UTILS:do_finalize_conf(State1));
                {ok,State2} -> {next_state, ?CURRSTATE, State2}
            end;
        {ok, State2} ->
            ?FSM_EVENT:call_attach(ZSide, State2),
%%            % wait for incoming bye
%%            {next_state, ?CURRSTATE, State2}
            % self bye to referred-by
            {ok,State3} = ?ACTIVE_UTILS:do_detach_bye(XCallId, State2),
            {next_state, ?CURRSTATE, State3}
    end.

%% -------------------------------------
%% @private Delete fork on error
%% -------------------------------------
fork_error(CallId, Refer, ForkResult, State) ->
    {ok, State1} = ?CONF_MEDIA:media_detach(CallId, State),
    #state_conf{forks=Forks,
                participants=Partcps}=State1,
    State3 = case lists:keytake(CallId, 1, Forks) of
                 false -> State1;
                 {value,_,Forks1} ->
                     Fork=maps:get(fork,Refer),
                     ?FSM_EVENT:fork_stop(Fork#side_fork{res=ForkResult,
                                                     finaltime=os:timestamp()}, State1),
                     % remove fork
                     erlang:cancel_timer(Fork#side_fork.rule_timer_ref),
                     State2 = State1#state_conf{forks=Forks1},
                     % remove referrer link to fork from side
                     #{xcallid:=XCallId}=Refer,
                     case lists:keyfind(XCallId,1,Partcps) of
                         false -> State2;
                         {_,XSide} -> State2#state_conf{participants=lists:keyreplace(XCallId,1,Partcps,{XCallId,XSide#side{refer_callid=undefined}})}
                     end
             end,
    case ?ACTIVE_UTILS:check_final(State3) of
        true -> ?CONF_UTILS:return_stopping(?CONF_UTILS:do_finalize_conf(State3));
        false -> {next_state, ?CURRSTATE, State3}
    end.

%% ==================================================
%% ==================================================

% @private ----
d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    #state_conf{confid=ConfId}=State,
    ?LOGSIP("CONF fsm ~p '~p':" ++ Fmt, [ConfId,?CURRSTATE] ++ Args).
