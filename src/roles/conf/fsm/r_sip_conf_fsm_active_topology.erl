%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 16.06.2016
%%% @doc Topology call utils of 'active' state of CONF FSM

-module(r_sip_conf_fsm_active_topology).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    sync_get_topology/2,
    sync_set_topology/2
]).

%-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("r_sip.hrl").
-include("r_sip_conf.hrl").
-include("r_sip_mgc.hrl").

% -include("../include/r_nksip.hrl").

-define(CURRSTATE, 'active').

%% ====================================================================
%% API functions
%% ====================================================================

%% --------------------------------
-spec sync_get_topology(Args::[ Arg::term() ], State::#state_conf{})
      -> ok | {error, Reason::term()}.
%% --------------------------------
sync_get_topology([Opts]=_Args, State) ->
    ?LOGSIP('$trace', "Get topology. Opts=~120p", [Opts]),
    case get_topology(Opts, State) of
        {error,_}=Reply -> State1=State;
        {ok,Res,State1} -> Reply = {ok,Res}
    end,
    {reply, Reply, ?CURRSTATE, State1}.

%% --------------------------------
-spec sync_set_topology(Args::[ [{{A::term(),B::term()},Enabled::boolean()}] ], State::#state_conf{})
      -> ok | {error, Reason::term()}.
%% --------------------------------
sync_set_topology([Opts]=_Args, State) ->
    ?LOGSIP('$trace', "Set topology. Opts=~120p", [Opts]),
    case set_topology(Opts, State) of
        {error,_}=Reply -> State1=State;
        {ok,State1} -> Reply = ok
    end,
    {reply, Reply, ?CURRSTATE, State1}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% --------------------------------
%% return current topology
%% --------------------------------
get_topology(_Opts, #state_conf{topology=Topology}=State) ->
    Items0 = lists:reverse(lists:filter(fun({_,Enabled}) -> not Enabled end, maps:to_list(Topology))),
    Res = lists:map(fun({{A,B},En}) -> #{from => A, to => B, enabled => En} end, [{{<<"*">>,<<"*">>},true} | Items0]),
    {ok,Res,State}.

%% --------------------------------
%% set topology, save new topology opts
%% --------------------------------
set_topology(NewL, #state_conf{topology=PrevM}=State) ->
    NewL1 = expand_asterisks(NewL,State),
    TotalM = unite_topology(PrevM, NewL1),
    % bothway, oneway, isolate
    Topology = build_topology(NewL1, TotalM, State),
    ?LOGSIP('$trace', "Result topology: ~120p", [Topology]),
    case ?CONF_MEDIA:modify_topology(State, Topology) of
        {error,_}=Err -> Err;
        {ok,State1} ->
            {ok,State1#state_conf{topology=TotalM}} % #374(c)
    end.

%% @private
%% for callmanager, non for selector
expand_asterisks(NewL,State) ->
    Ast = lists:foldl(fun({{A,B},_},false) -> A==<<"*">> orelse B==<<"*">>;
                           (_,true) -> true
                      end, false, NewL),
    case Ast of
        false -> NewL;
        true ->
            Parts = build_partcp_callids(State),
            lists:filter(fun({{A,B},_}) -> A/=B end,
                         lists:foldl(fun({{<<"*">>,<<"*">>},En},Acc) -> Acc ++ lists:flatten(lists:map(fun({A,_}) -> lists:map(fun({B,_}) -> {{A,B},En} end, Parts) end, Parts));
                                        ({{<<"*">>,B},En},Acc) -> Acc ++ lists:map(fun({PartId,_}) -> {{PartId,B},En} end, Parts);
                                        ({{A,<<"*">>},En},Acc) -> Acc ++ lists:map(fun({PartId,_}) -> {{A,PartId},En} end, Parts);
                                        (X,Acc) -> Acc + [X]
                                     end, [], NewL))
    end.

%% @private
%% unite previous topology settings with new,
%% PrevM::map(), NewL::list() -> TotalM::map()
unite_topology(PrevL, NewL) when is_list(PrevL) ->
    maps:merge(maps:from_list(PrevL), maps:from_list(NewL));
unite_topology(PrevM, NewL) -> unite_topology(maps:to_list(PrevM), NewL).

%% @private
%% build parameter for mgc
build_topology(NewL, TotalM, State) ->
    Hash = build_partcp_callids(State),
    F = fun({{PartcpIdA,PartcpIdB},X}, Acc) ->
                case ?EU:extract_optional_props([PartcpIdA,PartcpIdB],Hash) of
                    [undefined,_] -> Acc;
                    [_,undefined] -> Acc;
                    [CallIdA,CallIdB] ->
                        Back = case maps:find({PartcpIdB,PartcpIdA},TotalM) of
                                   error -> true;
                                   {_,Y} -> Y
                               end,
                        case X of
                            true when Back -> [{CallIdA,CallIdB,bothway}|Acc];
                            false when Back -> [{CallIdB,CallIdA,oneway}|Acc];
                            true -> [{CallIdA,CallIdB,oneway}|Acc];
                            false -> [{CallIdA,CallIdB,isolate}|Acc]
                        end end end,
    Mix = lists:foldl(F, [], NewL),
    % #374(b) leave only unique (one side setup)
    Set = lists:foldl(fun({A,B,_},Acc) when A<B -> ordsets:add_element({A,B}, Acc);
                          (_,Acc) -> Acc
                      end, [], Mix),
    lists:filter(fun({A,B,_}) when A>B -> not ordsets:is_element({B,A},Set);
                    (_) -> true
                 end, lists:usort(Mix)).


%% @private
%% make list of current [{PartcpId,CallId},...]
build_partcp_callids(State) ->
    #state_conf{participants=Partcps, forks=Forks}=State,
    F = fun ({_,#side{participantid=C,callid=CallId}}, Acc) -> [{C,CallId}|Acc];
            ({_,#{}=ForkCall}, Acc) ->
                case maps:get(fork,ForkCall) of
                    #side_fork{participantid=C,callid=CallId} -> [{C,CallId}|Acc];
                    _ -> Acc
                end end,
    lists:usort(lists:foldl(F, [], Partcps++Forks)).
