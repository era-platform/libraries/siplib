%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Service utils of 'active' state of CONF FSM

-module(r_sip_conf_fsm_active_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_conf.hrl").
-include("../include/r_sip_mgc.hrl").
-include("../include/r_sip_headers.hrl").

% -include("../include/r_nksip.hrl").

-define(CURRSTATE, 'active').

%% ====================================================================
%% API functions
%% ====================================================================

%% ---------------------------------
%% prepare side rec by opts for new participant
%% ---------------------------------
-spec prepare_side(Opts::list(), State::#state_conf{}) -> {XSide::#side{}, NewState::#state_conf{}}.
%% ---------------------------------
prepare_side(Opts, State) ->
    [Req] = ?EU:extract_required_props([req], Opts),
    Now = os:timestamp(),
    % -------
    #sipmsg{call_id=XCallId,
            from={XRUri,XRTag}}=Req,
    {ok, XReqHandle} = nksip_request:get_handle(Req),
    #sdp{}=XRSdp=?U:extract_sdp(Req),
    %
    #sipmsg{from={XRUri,_},
            to={#uri{ext_opts=XLExtOpts}=XLUri,_},
            contacts=XRContacts,
            to_tag_candidate=ToTagCandidate}=Req,
    % side local uri
    XLTag1 = ToTagCandidate,
    State1 = case State of
                 #state_conf{usedtags=[]} ->
                     State#state_conf{usedtags=[XRTag, XLTag1]};
                 _ -> State
             end,
    XLUri1 = XLUri#uri{ext_opts = ?U:store_value(<<"tag">>, XLTag1, XLExtOpts)},
    % side local contact
    XLContact = ?U:build_local_contact(XLUri), % @localcontact? (cfg=default! | incoming: top via's domain | outgoing: uri's route )
    % side rec
    XSide = #side{callid=XCallId,
                  rhandle=XReqHandle,
                  dhandle=undefined,
                  dir='in',
                  req=Req,
                  %
                  participantid=?EU:newid(),
                  sel_opts=?ACTIVE_UTILS:init_sel_opts(State),
                  %
                  remoteuri=XRUri,
                  remotetag=XRTag,
                  remotecontacts=XRContacts,
                  remotesdp=XRSdp,
                  remoteparty=?U:clear_uri(XRUri), % update display not need, cause b2bua should do it
                  %
                  localuri=XLUri1,
                  localtag=XLTag1,
                  localcontact=XLContact,
                  %
                  starttime=Now
                 },
    #state_conf{participants=Partcps}=State1,
    State2 = State1#state_conf{participants=[{XCallId,XSide}|Partcps]},
    {XSide,State2}.

%% @private
%% initialize selector side's opts (mode_max, mic, spk, topology)
init_sel_opts(State) -> init_sel_opts(State,[]).
init_sel_opts(State,Opts) ->
    [Spk0,Mic0] = ?EU:extract_optional_default([{<<"default_spk">>,true},
                                                {<<"default_mic">>,true}], maps:get(opts,State#state_conf.map)),
    [Spk,Mic] = ?EU:extract_optional_default([{<<"spk">>,Spk0},
                                              {<<"mic">>,Mic0}], Opts),
    #sel_opts{mode_max=?M_SDP:build_media_mode(Spk,Mic),
              spk=?EU:to_bool(Spk),
              mic=?EU:to_bool(Mic)}.

%% ---------------------------------
%% make side rec by fork of new participant
%% ---------------------------------
-spec make_side_by_fork(Fork::#side_fork{}, Resp::#sipmsg{}) -> Side::#side{}.
%% ---------------------------------
make_side_by_fork(#side_fork{}=Fork, #sipmsg{class={resp,_,_}}=Resp) ->
    #side_fork{}=Fork,
    {ok,DlgHandle} = nksip_dialog:get_handle(Resp),
    #sipmsg{to={RUri,RTag},contacts=RContacts}=Resp,
    #sdp{}=RSdp=?U:extract_sdp(Resp),
    Now = os:timestamp(),
    XSide = #side{callid=Fork#side_fork.callid,
                  rhandle=Fork#side_fork.rhandle,
                  dhandle=DlgHandle,
                  dir='out',
                  req=Resp,
                  %
                  participantid=Fork#side_fork.participantid,
                  sel_opts=Fork#side_fork.sel_opts,
                  %
                  remoteuri=RUri,
                  remotetag=RTag,
                  remotecontacts=RContacts,
                  remotesdp=RSdp,
                  remoteparty= ?U:build_remote_party_id(Resp, Fork#side_fork.remoteuri), % remote-party-id header should exist, guaranteed by b2bua
                  %
                  localuri=Fork#side_fork.localuri,
                  localtag=Fork#side_fork.localtag,
                  localcontact=Fork#side_fork.localcontact,
                  %
                  starttime=Fork#side_fork.starttime,
                  answertime=Now
                 },
    XSide.

%% ---------------------------------
%% update fork_side rec of new participant by 1xx response with sdp
%% ---------------------------------
-spec update_side_fork(Fork::#side_fork{}, Resp::#sipmsg{}) -> UpFork::#side_fork{}.
%% ---------------------------------
update_side_fork(#side_fork{}=Fork, #sipmsg{class={resp,SipCode,_}}=Resp) ->
    #side_fork{}=Fork,
    {ok,DlgHandle} = nksip_dialog:get_handle(Resp),
    #sipmsg{contacts=RContacts,to={_,RTag}}=Resp,
    RSdp = ?U:extract_sdp(Resp),
    Fork#side_fork{dhandle=DlgHandle,
                   last_response_code=SipCode,
                   remotetag=RTag,
                   remotecontacts=RContacts,
                   remotesdp=RSdp}.

%% ---------------------------------
%% replaces another participant by referred invite
%% ---------------------------------
-spec check_replaces(Req::#sipmsg{}, State::#state_conf{}) -> false | {error,callid} | {error,tag} | {ok,ReplacedSide::#side{}}.
%% ---------------------------------
check_replaces(Req, State) ->
    case {nksip_request:header("referred-by", Req), nksip_request:header("replaces", Req)} of
        {_,{ok,[]}} -> false;
        {_,{ok,[Replaces|_]}} ->
            RepMap = ?U:parse_replaces(Replaces),
            [RCallId,RFromTag,RToTag] = ?EU:maps_get([callid,fromtag,totag],RepMap),
            #state_conf{participants=Partcps}=State,
            case lists:keyfind(RCallId, 1, Partcps) of
                false -> {error, callid};
                {_,#side{remotetag=RFromTag,localtag=RToTag}=Side} -> {ok,Side};
                {_,#side{remotetag=RToTag,localtag=RFromTag}=Side} -> {ok,Side};
                _ -> {error, tag}
            end
    end.

%% ---------------------------------
%% send response to participant's invite
%% ---------------------------------
-spec send_200ok_to_participant(LSdp::#sdp{}, Side::#side{}, State::#state_conf{}) -> State1::#state_conf{}.
%% ---------------------------------
send_200ok_to_participant(LSdp, Side, State) ->
    #side{rhandle=ReqHandle,
          callid=XCallId}=Side,
    % @todo @remoteparty remote-party-id option forward (response header, from+display(dc), options, rights)
    #state_conf{room={_,U,D}=_RoomAOR}=State,
    RemoteParty = #uri{scheme=sip, disp=?CONF_UTILS:make_conf_display(State), user=U, domain=D},
    % contact
    LContact = ?U:build_local_contact(#uri{scheme=sip, user=U}), % @localcontact (cfg=default! | Top via's domain!)
    %
    Opts = [{body, LSdp},
            {contact, LContact},
             user_agent],
    Opts1 = ?U:remotepartyid_opts(RemoteParty,'2xx') ++ Opts,
    State1 = ?CONF_UTILS:send_response(ReqHandle, {200, Opts1}, State),
    %
    Now = os:timestamp(),
    Side1 = Side#side{answertime=Now},
    #state_conf{participants=Partcps}=State1,
    State1#state_conf{participants=lists:keyreplace(XCallId,1,Partcps,{XCallId,Side1})}.

%% ---------------------------------
%% detach existing participant
%% ---------------------------------
-spec do_detach_bye(CallId::binary(), State::#state_conf{}) -> {ok, State::#state_conf{}} | stop.
%% ---------------------------------
do_detach_bye(CallId, State) ->
    #state_conf{participants=Partcps}=State,
    case lists:keyfind(CallId, 1, Partcps) of
        false -> ok;
        {_,Side} ->
            ?CONF_UTILS:send_bye(Side, State)
    end,
    do_detach(CallId, State).

%% ---------------------------------
%% detach participant from conference (last should destroy conference)
%% ---------------------------------
-spec do_detach(CallId::binary(), State::#state_conf{}) -> {ok, State::#state_conf{}} | stop.
%% ---------------------------------
do_detach(CallId, State) ->
    #state_conf{participants=Partcps, forks=Forks, autoclose=AutoClose}=State,
    % from store
    ?DLG_STORE:unlink_dlg(CallId),
    %
    case lists:keytake(CallId, 1, Partcps) of
        false ->
            {ok,State};
        {value,{_,Side},[]} when Forks==[], AutoClose ->
            ?FSM_EVENT:call_detach(Side, State),
            stop;
        {value,{_,Side},Partcps1} ->
            ?FSM_EVENT:call_detach(Side, State),
            % from participants
            State1 = State#state_conf{participants=Partcps1},
            % from media
            {ok,_State2} = ?CONF_MEDIA:media_detach(Side,State1)
    end.

%% ---------------------------------
%% Checks if no user found in state
%% ---------------------------------
-spec check_final(State::#state_conf{}) -> true | false.
%% ---------------------------------
check_final(State) ->
    #state_conf{participants=Partcps, forks=Forks, autoclose=AutoClose}=State,
    case {Partcps,Forks} of
        {[],[]} when AutoClose -> true;
        _ -> false
    end.


%% ------------------------------------------------------------------------
%% Get info about conference
%% ------------------------------------------------------------------------

%% ---------------------------------
%% Return conference info
%% ---------------------------------
-spec get_info(Opts::list(),State::#state_conf{}) -> {ok,Info::map()} | {error,Reason::term()}.
%% ---------------------------------
get_info(_Opts,#state_conf{room=Room,confnum=ConfNum,confid=ConfId}=_State) ->
    {ok, #{room => Room,
           confnum => ConfNum,
           confid => ConfId}}.

%% ---------------------------------
%% Return participants list
%% ---------------------------------
-spec get_participants(Args::list(),State::#state_conf{}) -> {ok,Info::map()} | {error,Reason::term()}.
%% ---------------------------------
get_participants([Opts],#state_conf{participants=Partcps,forks=Forks}=_State) ->
    Unparse = lists:member(unparse,Opts),
    {ok, lists:foldr(fun(_,{ok,_}=Acc) -> Acc;
                        ({_,#side{}=Side},Acc) -> [build_partcp(Side,Unparse)|Acc];
                        ({_,#{}=ForkCall},Acc) ->
                             case maps:get(fork,ForkCall) of
                                 #side_fork{}=Fork -> [build_partcp(Fork,Unparse)|Acc];
                                 _ -> Acc
                             end;
                        (_,Acc) -> Acc
                     end, [], Partcps++Forks)}.

%% ---------------------------------
%% Find and return participant id by key
%% ---------------------------------
-spec find_participant(Key::binary(),State::#state_conf{}) -> {ok,PartcpId::binary()} | {error,Reason::term()}.
%% ---------------------------------
find_participant(Key,#state_conf{participants=Partcps, forks=Forks}=_State) ->
    KeyUri = case ?U:parse_uris(Key) of
                 [#uri{}=U|_] -> U;
                 _ -> #uri{}
             end,
    FCheck = fun({CallId,_}, PartcpId) when CallId==Key -> {ok,PartcpId};
                ({_,#uri{disp=DN,user=User,domain=Domain}}, PartcpId) -> 
                        case KeyUri of
                            #uri{user=User,domain=Domain} -> {ok,PartcpId};
                            _ when DN==Key -> {ok,PartcpId};
                            _ -> false
                        end end,
    R = lists:foldl(fun(_,{ok,_}=Acc) -> Acc;
                       ({_,#side{participantid=C,callid=CallId,remoteuri=RUri}},_Acc) -> FCheck({CallId,RUri},C);
                       ({_,#{}=ForkCall},Acc) -> 
                            case maps:get(fork,ForkCall) of
                                #side_fork{participantid=C,callid=CallId,remoteuri=RUri} -> FCheck({CallId,RUri},C);
                                _ -> Acc
                            end end, false, Partcps++Forks),
    case R of
        {ok,_}=Ok -> Ok;
        false -> {error,{not_found,<<"Participant not found">>}}
    end.

%% ---------------------------------
%% Return participant info
%% ---------------------------------
-spec get_participant_info(Args::list(),State::#state_conf{}) -> {ok,Info::map()} | {error,Reason::term()}.
%% ---------------------------------
get_participant_info([PartcpId,Opts],#state_conf{participants=Partcps,forks=Forks}=_State) ->
    Unparse = lists:member(unparse,Opts),
    R = lists:foldl(fun(_,{ok,_}=Acc) -> Acc;
                       ({_,#side{participantid=C}=Side},_Acc) when C==PartcpId -> {ok,build_partcp(Side,Unparse)};
                       ({_,#{}=ForkCall},Acc) -> 
                            case maps:get(fork,ForkCall) of
                                #side_fork{participantid=C}=Fork when C==PartcpId -> {ok,build_partcp(Fork,Unparse)};
                                _ -> Acc
                            end;
                       (_,Acc) -> Acc
                    end, false, Partcps++Forks),
    case R of
        {ok,_}=Ok -> Ok;
        false -> {error,{not_found,<<"Participant not found">>}}
    end. 

%% ---------------
%% privates
%% ---------------

%% @private
build_partcp(Partcp,Unparse) ->
    case Partcp of
        #side{participantid=PartcpId,callid=CallId,remotetag=RTag,localtag=LTag,remoteuri=RUri,localuri=LUri} ->
            #{state=>active,
                participantid=>PartcpId,callid=>CallId,rtag=>RTag,ltag=>LTag,
                ruri=>build_uri(RUri,Unparse),luri=>build_uri(LUri,Unparse)};
        #side_fork{participantid=PartcpId,callid=CallId,remotetag=RTag,localtag=LTag,remoteuri=RUri,localuri=LUri} ->
            #{state=>calling,
                participantid=>PartcpId,callid=>CallId,rtag=>RTag,ltag=>LTag,
                ruri=>build_uri(RUri,Unparse),luri=>build_uri(LUri,Unparse)}
    end.

%% @private
build_uri(Uri,true) -> ?U:unparse_uri(?U:clear_uri2(Uri));
build_uri(Uri,_) -> Uri.

%% ====================================================================
%% Internal functions
%% ====================================================================

% @private ----
d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    #state_conf{confid=ConfId}=State,
    ?LOGSIP("CONF fsm ~p '~p':" ++ Fmt, [ConfId,?CURRSTATE] ++ Args).
