%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 24.04.2019.
%%% @doc RP-1331

-module(r_sip_conf_fsm_event).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([conf_init/1,
         conf_start/1,
         %
         call_attach/2,
         call_detach/2,
         %
         fork_start/2,
         fork_preanswer/3,
         fork_answer/3,
         fork_stop/2,
         %
         dtmf/3,
         hold/2,
         unhold/2,
         sesschanged/2,
         %
         conf_stop/1,
         conf_dead/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_conf.hrl").
-include("../include/r_sip_mgc.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ------
%% when conference init (after routing)
%% ConfIdData::{ConfId::binary(),ConfIdHash::integer()}, Room::aor(), ITS::integer()
conf_init({_ConfIdData, _Room, _ITS}=Id) ->
    EOpts = #{relates => relates(Id)},
    ?EVENT:conf_init(Id,EOpts).

%% when conference start (after init)
%% State::#state_conf{}
conf_start(State) ->
    EOpts = #{relates => relates(State)},
    ?EVENT:conf_start(State,EOpts).


%% ------
%% when new participant attached
%% Side::#side{}, State::#state_conf{}
call_attach(Side, State) ->
    EOpts = #{relates => relates(State)},
    ?EVENT:call_attach(Side, State, EOpts).

%% when participant detached
%% Side::#side{}, State::#state_conf{}
call_detach(Side, State) ->
    EOpts = #{relates => relates(State)},
    ?EVENT:call_detach(Side, State, EOpts).


%% ------
%% when conf detects dtmf from abonent
dtmf(Side, Dtmf, State) ->
    EOpts = #{relates => relates(State)},
    ?EVENT:dtmf(Side, Dtmf, State, EOpts).

%% when remote side hold
hold(Side, State) ->
    EOpts = #{relates => relates(State)},
    ?EVENT:hold(Side, State, EOpts).

%% when remote side unhold
unhold(Side, State) ->
    EOpts = #{relates => relates(State)},
    ?EVENT:unhold(Side, State, EOpts).

%% when sess changed, but not hold/unhold
sesschanged(Side, State) ->
    EOpts = #{relates => relates(State)},
    ?EVENT:sesschanged(Side, State, EOpts).


%% ------
%% when fork start invite
%% Fork::#side_fork{}, State::#state_forking{}
fork_start(Fork, State) ->
    EOpts = #{relates => relates(State)},
    ?EVENT:fork_start(Fork, State, EOpts).

%% when fork got 1xx
%% Fork::#side_fork{}, State::#state_forking{}
fork_preanswer(Fork, SipCodeReason, State) ->
    EOpts = #{relates => relates(State)},
    ?EVENT:fork_preanswer(Fork, SipCodeReason, State, EOpts).

%% when fork got 2xx
%% Fork::#side_fork{}, State::#state_forking{}
fork_answer(Fork, SipCodeReason, State) ->
    EOpts = #{relates => relates(State)},
    ?EVENT:fork_answer(Fork, SipCodeReason, State, EOpts).

%% when fork got 3xx, 4xx-6xx or timeout
%% Fork::#side_fork{}, State::#state_forking{}
fork_stop(Fork, State) ->
    EOpts = #{relates => relates(State)},
    ?EVENT:fork_stop(Fork, State, EOpts).


%% ------
%% when conference stops correctly
%% State::#state_conf{}
%%   stopreason: #{type:atom, reason::binary(), [operations::list(), callid::binary()]}
conf_stop(State) ->
    EOpts = #{relates => relates(State)},
    ?EVENT:conf_stop(State, EOpts).

%% when conference dead
%% ConfIdData::{ConfId::binary(),ConfIdHash::integer()}, Room::aor(), ITS::integer()
conf_dead({_ConfIdData, _Room, _ITS}=Id, OwnerPid) ->
    EOpts = #{relates => relates(Id)},
    ?EVENT:conf_dead(Id, OwnerPid, EOpts).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ----------------------------
%% Returns list of relates for event purposes
%%   (ex. [{<<"test.rootdomain.ru",{conf,<<"rCF-005-E19C39">>}},{<<"test.rootdomain.ru">>,{conf,<<"7428913">>}}])
%% ----------------------------
%% when dialog
relates(#state_conf{confid=ConfId,room={_,ConfName,Domain}=_Room}=_State) ->
    [{Domain,{conf,ConfId}},
     {Domain,{conf,ConfName}}];
relates({ConfId,{_,ConfName,Domain}=_Room,_TS}=_ConfIdData) ->
    [{Domain,{conf,ConfId}},
     {Domain,{conf,ConfName}}].

