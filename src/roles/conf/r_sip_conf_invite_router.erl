%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc
%%% @todo

-module(r_sip_conf_invite_router).
-author('Peter Bukashin <tbotc@yandex.ru>').

%% -------------------------------------------------------------------
%%
%% -------------------------------------------------------------------

-export([sip_invite/2]).

%-compile([export_all,nowarn_export_all]).

%% ==========================================================================
%% Defines
%% ==========================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_conf.hrl").

% -include("../include/r_nksip.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------
%% start from incoming call (featurecode)
%% ------------------------
-spec(sip_invite(AOR::tuple(), Request::nksip:request()) ->
        {reply, nksip:sipreply()} | noreply | block).
%% ------------------------
sip_invite(AOR, Req) ->
    case build_route_opts(AOR, Req) of
        {reply,_}=R -> R;
        noreply -> noreply;
        block -> block
    end.

%% ------------------------
-spec(build_route_opts(AOR::tuple(), Request::nksip:request()) ->
        {reply, nksip:sipreply()} | {ok, RouteOpts::list()} | noreply | block).
%% ------------------------
build_route_opts(AOR, Req) ->
    #sipmsg{ruri=#uri{domain=_RUriDomain}=_RUri,
            vias=[#via{domain=ViaDomain, port=_ViaPort}|_],
            nkport=#nkport{remote_ip=RemoteIp}}=Req,
    % from inside or from outside?
    % check via domain existence to detect is this call from inside or outside
    Insiders = ?CFG:get_all_sipserver_addrs(),
    BRemoteIp = ?EU:to_binary(inet:ntoa(RemoteIp)),
    case {lists:member(ViaDomain,Insiders), lists:member(BRemoteIp,Insiders)} of
        {false,_} -> ?FILTER:check_block_response_malware(invalid_address, Req, {reply, ?InternalError("CONF. Access denied, remote address is unknown")});
        {true,true} -> do_invite_conf(AOR, Req);
        {true,false} ->
            ?OUT("CONF. Incoming request forbidden, via ~120p is cluster address, but remote ~120p is not", [ViaDomain, BRemoteIp]),
            ?FILTER:check_block_response_malware(invalid_address, Req, {reply, ?Forbidden("CONF. Invalid remote address")})
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

% enter existing or absent conf by room=AOR
do_invite_conf({_,U,_}=AOR, Req) ->
    case ?U:parse_conf_user(U) of
        false -> ?FILTER:check_block_response_malware(unknown_account, Req, {reply, ?Forbidden("CONF. User not found")});
        {ok,_} ->
            GStoreKey = {conf,AOR},
            case ?U:call_store(GStoreKey, fun() -> {get, [GStoreKey]} end, undefined) of
                undefined ->
                    {reply, ?InternalError("CONF. Store not found, unable to find conference")};
                {ok, #{confid:=ConfId}} ->
                    case ?DLG_STORE:find_t(ConfId) of
                        false -> do_start_conf(AOR, Req);
                        {_,#{link:=Conf}} -> do_attach_conf(Conf, AOR, Req)
                    end;
                false ->
                    do_start_conf(AOR, Req)
            end
    end.

%% create new conf room
do_start_conf(AOR, Req) ->
    Opts = [{app,?SIPAPP},
            {req,Req},
            {room,AOR},
            {call_pid,self()}],
    ?CONF_SRV:start(Opts),
    noreply.

%% attach existing conf room
do_attach_conf(Conf, _AOR, Req) ->
    Opts = [{app,?SIPAPP},
            {req,Req},
            {call_pid,self()}],
    ?CONF_SRV:attach_participant(Conf, Opts),
    noreply.

