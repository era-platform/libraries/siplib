%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Media utils of 'active' state of CONF FSM
%%% @todo

-module(r_sip_conf_media).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_conf.hrl").
-include("../include/r_sip_mgc.hrl").

% -include("../include/r_nksip.hrl").

-define(CURRSTATE, 'active').

%% ====================================================================
%% API functions
%% ====================================================================

%% ---------------------------------
%% checks if media is available (mgc-mg pair, or no media is need)
%% ---------------------------------
-spec check_available(State::#state_conf{}) -> {ok, true|false} | {error,Reason::term()}.

check_available(State) ->
    ?MGC:mg_check_any_available(get_mg_opts(State)).

%% ---------------------------------
%% checks if media is available and return count of  (mgc-mg pair, or no media is need)
%% ---------------------------------
-spec check_available_slots(State::#state_conf{}) -> {ok, MGCnt::integer(), CtxCnt::integer()} | {error,Reason::term()}.

check_available_slots(State) ->
    ?MGC:mg_check_available_slots(get_mg_opts(State)).

%% ---------------------------------
%% prepare media by creating new context (player termination or first participant)
%% ---------------------------------

-spec media_prepare_start(State::#state_conf{}) -> {ok, NewState::#state_conf{}} | {error, SipReply::{SipCode::integer(),SipOpts::list()}}.

media_prepare_start(State) ->
    #state_conf{confid=ConfId,video=Video}=State,
    <<B4:32/bitstring, _/bitstring>> = ?U:luid(),
    MSID = <<ConfId/bitstring, "_", B4/bitstring>>,
    MediaOpts = #{rec => false, % RP-369 conf record disabled
                  mg_opts => get_mg_opts(State),
                  play => #{files => []}}, % #{file => ...}}
    % videoconference options. Screen schema in JSON.
    MediaOpts1 = case Video of
                     true -> MediaOpts#{video => true,
                                        videoScreenSchema => build_video_screen_schema(prepare,State)};
                     false -> MediaOpts
                 end,
    %
    {Mks,Res} = timer:tc(fun() -> ?MEDIA_CONF:start_media(MSID, MediaOpts1) end),
    d(State, " .. prepare_media (~p ms)", [Mks/1000]),
    case Res of
        {reply, SipReply} when is_tuple(SipReply) ->
            d(State, "MGC error: create media context failure"),
            {error, SipReply};
        {ok, Media} ->
            State1 = State#state_conf{media=Media},
            {ok, State1}
    end.

%% ---------------------------------
%% stops existing media context
%% ---------------------------------
-spec media_stop(State::#state_conf{}) -> {ok, NewState::#state_conf{}}.

media_stop(#state_conf{media=undefined}=State) -> {ok, State};
media_stop(#state_conf{media=Media}=State) ->
    ?MEDIA_CONF:stop_media(Media),
    {ok, State#state_conf{media=undefined}}.

%% ---------------------------------
%% returns map with current media properties (mgc/mg/ctxid)
%% ---------------------------------
-spec get_current_media_link(State::#state_conf{}) -> {ok, Map::map()} | undefined.

get_current_media_link(#state_conf{media=Media}=_State) ->
    case Media of
        undefined -> undefined;
        #media_conf{mgc=MGC, msid=MSID, mgid=MG, ctx=CtxId} -> {ok, #{mgc=>MGC, msid=>MSID, mgid=>MG, ctx=>CtxId}}
    end.

%% ---------------------------------
%% modify media by attaching new participant
%% ---------------------------------
-spec media_attach(XSide::#side{}, State::#state_conf{}) -> {ok, NewState::#state_conf{}, LSdp::#sdp{}} | {error, SipReply::{SipCode::integer(),SipOpts::list()}}.

media_attach(Side, State) ->
    #side{remotesdp=RSdp,
          req=Req}=Side,
    case ?M_SDP:check_known_fmt(RSdp) of
        false ->
            d(State, "MGC error: Not supported media"),
            {error, ?MediaNotSupported("CONF. Unknown audio formats")};
        true ->
            #state_conf{media=Media}=State,
            case ?MEDIA_CONF:attach_term(Media, Req, stream_mode(Side,Req)) of
                {error, Reason} ->
                    d(State, "MGC attach error: ~120p", [Reason]),
                    {error, ?InternalError("CONF. MGC error")};
                {ok, Media1, LSdp} ->
                    State1 = State#state_conf{media=Media1},
                    case refresh_screen_schema(State1) of
                        {error, Reason1} ->
                            d(State, "MGC attach error: ~120p", [Reason1]),
                            {error, ?InternalError("CONF. MGC error")};
                        {ok, State2} ->
                            {ok, State2, LSdp}
                    end end end.

%% ---------------------------------
%% modify media by detaching participant
%% ---------------------------------
-spec media_detach(XSide::#side{}|binary(), State::#state_conf{}) -> {ok, NewState::#state_conf{}}.

media_detach(#side{callid=CallId}=_Side, State) ->
    media_detach(CallId, State);

media_detach(CallId, State) when is_binary(CallId) ->
    #state_conf{media=Media}=State,
    case ?MEDIA_CONF:detach_term(Media, CallId) of
        {error, Reason} ->
            d(State, "MGC detach error: ~120p", [Reason]),
            {ok, State};
        {ok, Media1} ->
            State1 = State#state_conf{media=Media1},
            case refresh_screen_schema(State1) of
                {error, Reason1} ->
                    d(State, "MGC attach error: ~120p", [Reason1]),
                    {error, ?InternalError("CONF. MGC error")};
                {ok, State2} ->
                    {ok, State2}
            end end.

%% ---------------------------------
%% modify media by participant's reinvite
%% ---------------------------------
-spec media_sessionchange(Req::#sipmsg{}, XSide::#side{}, State::#state_conf{}) -> {ok, NewState::#state_conf{}, LSdp::#sdp{}} | {error, SipReply::{SipCode::integer(),SipOpts::list()}}.

media_sessionchange(Req, XSide, State) ->
    #sdp{}=RSdp=?U:extract_sdp(Req),
    case ?M_SDP:check_known_fmt(RSdp) of
        false ->
            d(State, "MGC error: Not supported media"),
            {error, ?MediaNotSupported("CONF. Unknown audio formats")};
        true ->
            #state_conf{media=Media}=State,
            case ?MEDIA_CONF:modify_term(Media, Req, stream_mode(XSide,RSdp)) of
                {error, Reason} ->
                    d(State, "MGC sessionchange error: ~120p", [Reason]),
                    {error, ?InternalError("CONF. MGC error")};
                {ok, Media1, LSdp} ->
                    d(State, " -> session changed"),
                    State1 = State#state_conf{media=Media1},
                    {ok, State1, LSdp}
            end
    end.

%% ---------------------------------
%% modify media by adding new participant's outgoing call (only local side)
%% ---------------------------------
-spec media_add_outgoing(CallId::binary(), State::#state_conf{}) -> {ok, NewState::#state_conf{}, LSdp::#sdp{}} | {error, SipReply::{SipCode::integer(),SipOpts::list()}}.

media_add_outgoing(CallId, State) ->
    #state_conf{media=Media}=State,
    case ?MEDIA_CONF:add_outgoing_term(Media, CallId) of
        {error, Reason} ->
            d(State, "MGC error: ~120p", [Reason]),
            {error, ?InternalError("CONF. MGC error")};
        {ok, Media1, LSdp} ->
            d(State, " -> added local term"),
            State1 = State#state_conf{media=Media1},
            {ok, State1, LSdp}
    end.

%%---------
-spec media_update_outgoing_by_remote(Fork::#side_fork{}, RSdp::#sdp{}, State::#state_conf{}) -> {ok, NewState::#state_conf{}} | {error, Reason::term()}.

media_update_outgoing_by_remote(Fork, RSdp, State) ->
    #side_fork{callid=CallId}=Fork,
    #state_conf{media=Media}=State,
    case ?MEDIA_CONF:update_outgoing_term_by_remote(Media, CallId, RSdp, stream_mode(Fork,RSdp)) of
        {error, Reason} ->
            d(State, "MGC error: ~120p", [Reason]),
            {error, Reason};
        {ok, Media1} ->
            d(State, " -> modified local term"),
            State1 = State#state_conf{media=Media1},
            {ok, State1}
    end.

%% ---------------------------------
%% checks if MGC/MG pair, linked in current state data corresponds to args
%% ---------------------------------
-spec media_check_mg(State::#state_conf{}, Args::list()) -> true | false.

media_check_mg(State, Args) ->
    #state_conf{media=Media}=State,
    ?MEDIA_CONF:check_media_mg(Media, Args).

%% ---------------------------------
%% modify mode of term
%% ---------------------------------
-spec media_modify_mode(State::#state_conf{}, Side::#side{}, Mode::binary()) ->
          {ok, NewState::#state_conf{}} | {error,R::term()}.

media_modify_mode(State, #side{}=Side, Mode) when is_binary(Mode) ->
    #side{callid=CallId, sel_opts=SelOpts, remotesdp=RSdp}=Side,
    case
        SelOpts#sel_opts.mode_max == Mode
        orelse stream_mode(Mode, RSdp) == stream_mode(SelOpts, RSdp)
      of
        true -> {ok, State}; % new state equals to previous state
        false ->
            #state_conf{media=Media}=State,
            case ?MEDIA_CONF:modify_term_mode(Media, CallId, Mode) of
                {error, Reason} ->
                    d(State, "MGC error: ~120p", [Reason]),
                    {error, Reason};
                {ok, Media1} ->
                    d(State, " -> mode modified"),
                    State1 = State#state_conf{media=Media1},
                    {ok, State1}
            end
    end.

%% ---------------------------------
%% modify topology
%% ---------------------------------
-spec modify_topology(State::#state_conf{}, Topology::[{FromCallId::binary(), ToCallId::binary(), bothway|oneway|isolate}]) ->
          {ok, NewState::#state_conf{}} | {error,R::term()}.

modify_topology(State, []) -> {ok, State};
modify_topology(State, [_|_]=TopologyList) ->
    #state_conf{media=Media}=State,
    case ?MEDIA_CONF:modify_topology(Media, TopologyList) of
        {error, Reason} ->
            d(State, "MGC error: ~120p", [Reason]),
            {error, Reason};
        {ok, Media1} ->
            d(State, " -> topology modified"),
            State1 = State#state_conf{media=Media1},
            {ok, State1}
    end.

%% ---------------------------------
%% modify context props
%% ---------------------------------
-spec modify_context_props(State::#state_conf{}, ContextProps::[{Key :: atom(), Value :: binary() | string()}]) ->
          {ok, NewState::#state_conf{}} | {error,R::term()}.

modify_context_props(State, []) -> {ok, State};
modify_context_props(State, ContextProps) ->
    #state_conf{media=Media}=State,
    case ?MEDIA_CONF:modify_context_props(Media, ContextProps) of
        {error, Reason} ->
            d(State, "MGC error: ~120p", [Reason]),
            {error, Reason};
        {ok, Media1} ->
            d(State, " -> context props modified"),
            State1 = State#state_conf{media=Media1},
            {ok, State1}
    end.

%% ---------------------------------
%% refresh screen schema
%% ---------------------------------
-spec refresh_screen_schema(State::#state_conf{}) ->
          {ok, NewState::#state_conf{}} | {error,R::term()}.

refresh_screen_schema(#state_conf{video=false}=State) -> {ok, State};
refresh_screen_schema(#state_conf{}=State) ->
    VideoScreenSchema = build_video_screen_schema(update,State),
    modify_context_props(State, [{'videoScreenSchema',VideoScreenSchema}]).
    
%% @private
build_video_screen_schema(prepare,_) -> #{};
build_video_screen_schema(update,#state_conf{map=Map,media=#media_conf{terms=Terms}}) ->
    _VideoSchema = maps:get(videoSchema,Map,undefined),
    %% TODO: variants of screenschema
    Cnt = length(Terms),
    Fields0 = build_video_fields(Cnt),
    Fields1 = lists:map(fun({I,{Idx,{L,T,W,H}}}) ->
                                M = #{id => ?EU:to_binary(I),
                                      left => L,
                                      top => T,
                                      width => W,
                                      height => H,
                                      type => <<"video">>,
                                      'z-index' => I,
                                      mode => case Idx of auto -> <<"auto">>; _ when is_integer(Idx) -> <<"term">> end},
                                case Idx of
                                    auto -> M;
                                    _ when is_integer(Idx), Idx =< Cnt ->
                                        {CallId,_TermItem} = lists:nth(Idx,Terms),
                                        M#{termId => erlang:phash2(CallId)};
                                    _ -> M#{mode => <<"auto">>}
                                end end, lists:zip(lists:seq(1,length(Fields0)),Fields0)),
    #{frame => #{width => 1280,
                 height => 720,
                 backgroundColor => <<"fe4d13">>,
                 foregroundColor => <<"d00000">>},
      vad => true,
      fields => Fields1}.

%% @private
build_video_fields(X) when X=<4 ->
    W=640,H=360,
    [{1,{0,0,W,H}},  {2,{640,0,W,H}},
     {3,{0,360,W,H}},{4,{640,360,W,H}}];
build_video_fields(X) when X=<6 ->
    W=640,H=240,
    [{1,{0,0,W,H}},  {2,{640,0,W,H}},
     {3,{0,240,W,H}},{4,{640,240,W,H}},
     {5,{0,480,W,H}},{6,{640,480,W,H}}];
build_video_fields(X) when X=<9 ->
    W=426,H=240,
    [{1,{1,0,W,H}},  {2,{427,0,W,H}},  {3,{853,0,W,H}},
     {4,{1,240,W,H}},{5,{427,240,W,H}},{6,{853,240,W,H}},
     {7,{1,480,W,H}},{8,{427,480,W,H}},{9,{853,480,W,H}}];
build_video_fields(X) when X>9 ->
    W=320,H=180,
    [{auto,{320,360,2*W,2*H}},
     {1,{0,0,W,H}},  {2,{320,0,W,H}},  {3,{640,0,W,H}},  {4,{960,0,W,H}},
     {5,{0,180,W,H}},                                    {6,{960,360,W,H}},
     {7,{0,360,W,H}},                                    {8,{960,360,W,H}},
     {9,{0,540,W,H}},{3,{320,540,W,H}},{3,{640,540,W,H}},{10,{960,540,W,H}}].

%% ---------------------------------
%% update context by adding/removing ivr termination with player
%% ---------------------------------
-spec media_attach_player(Opts::list(), State::#state_conf{}) -> {ok, NewState::#state_conf{}} | {error, Reason::term()}.

media_attach_player(Opts, #state_conf{media=Media}=State) ->
    case ?MEDIA_CONF:conf_attach_player(Media, Opts) of
        {error,_}=Err -> Err;
        {ok,Media1} ->
            {ok, State#state_conf{media=Media1}}
    end.

%% ----
-spec media_detach_player(Opts::list(), State::#state_conf{}) -> {ok, NewState::#state_conf{}} | {error, Reason::term()}.

media_detach_player(Opts, #state_conf{media=Media}=State) ->
    case ?MEDIA_CONF:conf_detach_player(Media, Opts) of
        {error,_}=Err -> Err;
        {ok,Media1} ->
            {ok, State#state_conf{media=Media1}}
    end.

%% ----
-spec media_modify_player(Opts::list(), State::#state_conf{}) -> {ok, NewState::#state_conf{}} | {error, Reason::term()}.

media_modify_player(Opts, #state_conf{media=Media}=State) ->
    case ?MEDIA_CONF:conf_modify_player(Media, Opts) of
        {error,_}=Err -> Err;
        {ok,Media1} ->
            {ok, State#state_conf{media=Media1}}
    end.

%% ----
-spec media_find_playerid(TermId::term(), State::#state_conf{}) -> {ok, PlayerId::term()} | undefined.

media_find_playerid(TermId, #state_conf{media=Media}=_State) ->
    ?MEDIA_CONF:conf_find_player_id(TermId, Media).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
%% returns mg opts for call to search mg (mgsfx, mgaddr)
get_mg_opts(_State) -> get_mg_opts().
get_mg_opts() ->
    [{mgsfx,?U:mg_postfix(<<"def">>)}, % suffix for internal calls (not sg/esg)
     get_ua_addrs_option() % local mg is less priority, used only if 1-server mode.
    ].

%% @private
get_ua_addrs_option() ->
    Key = server_interfaces,
    FAddrs = fun() -> lists:usort(lists:filtermap(fun({_,_,_,_}=A) -> {true,?EU:to_binary(inet:ntoa(A))}; (_) -> false end, ?ENVCFG:get_current_server_addrs())) end,
    Addrs = ?ENVSTORE:lazy_t(Key, FAddrs, {60000,30000,2000}, {true,true}),
    {ua_addrs,Addrs}.

%% @private
%% return overriden stream mode by selector opts of participant (undefined if not overriden), to manage mic/spk
stream_mode(#side{sel_opts=SelOpts}, ReqOrSdp) -> stream_mode(SelOpts, ReqOrSdp);
stream_mode(#side_fork{sel_opts=SelOpts}, ReqOrSdp) -> stream_mode(SelOpts, ReqOrSdp);
stream_mode(#sel_opts{}=SelOpts, #sipmsg{}=Msg) -> RSdp=?U:extract_sdp(Msg), stream_mode(SelOpts, RSdp);
stream_mode(#sel_opts{mode_max=ModeMax}, #sdp{}=RSdp) -> stream_mode(ModeMax, RSdp);
stream_mode(ModeMax, #sdp{}=RSdp) when is_binary(ModeMax) ->
    ?M_SDP:limit_media_mode_nochange(?M_SDP:get_audio_mode(RSdp), ModeMax).

%% @private ----
d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    #state_conf{confid=ConfId}=State,
    ?LOGSIP("CONF fsm ~p '~p':" ++ Fmt, [ConfId,?CURRSTATE] ++ Args).
