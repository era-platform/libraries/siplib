%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc
%%% @todo

-module(r_sip_conf_stat).
-author('Peter Bukashin <tbotc@yandex.ru>').

%% -------------------------------------------------------------------
%%
%% -------------------------------------------------------------------

-export([query/2]).

%% ==========================================================================
%% Defines
%% ==========================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_conf.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

query(<<"rooms">>, #{}=Map) ->
    DlgsLinks = ?DLG_STORE:get_all_dlgs_attrs([room]),
    Rooms = case maps:get(domain, Map, undefined) of
                undefined ->
                    lists:map(fun([RoomAOR]) -> ?U:unparse_uri(?U:make_uri(RoomAOR)) end, DlgsLinks);
                TD ->
                    lists:foldl(fun([{_Proto, _User, Domain}=RoomAOR], Acc) -> case Domain of TD -> Acc ++ [?U:unparse_uri(?U:make_uri(RoomAOR))]; _ -> Acc end end, [], DlgsLinks)
            end,
    M = #{rooms => lists:sort(Rooms)},
    {ok, M};

query(<<"participants">>, #{}=Map) ->
    TD = maps:get(domain, Map, undefined),
    {ok, query_participants(TD, Map)};

query(<<"active">>, #{}=_Map) ->
    M = #{dlgcount => ?DLG_STORE:get_dialog_count(),
          callcount => ?DLG_STORE:get_callid_count()},
    {ok, M}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%%
query_participants(FilterDomain, Map) ->
    R = case maps:get(room,Map,undefined) of
            undefined -> {error,{invalid_params,<<"Invalid params. Expected 'room'">>}};
            Room ->
                case catch ?U:parse_uris(Room) of
                    {'EXIT',_} -> {error,{invalid_params,?EU:strbin("Invalid params. Expected 'room' as URI (sip:~sROOM@DOMAIN)",[?ConfPrefix])}};
                    error -> {error,{invalid_params,?EU:strbin("Invalid params. Expected 'room' as URI (sip:~sROOM@DOMAIN)",[?ConfPrefix])}};
                    [#uri{domain=D}=Uri|_] when FilterDomain==undefined; FilterDomain==D ->
                        AOR = ?U:make_aor(Uri),
                        Ffind = fun(Val) ->
                                        case maps:get(room,Val,undefined) of
                                            undefined -> false;
                                            AOR -> true;
                                            _ -> false
                                        end end,
                        case ?DLG_STORE:find_dlg_by_attr_fun(Ffind) of
                            [] -> {error,{not_found,<<"Conference room not found">>}};
                            [V|_] ->
                                DlgX = maps:get(link,V),
                                {Self,Ref} = {self(),make_ref()},
                                F = fun(StateName, #state_conf{participants=Partcps}=StateData) ->
                                            Participants = lists:map(fun({_CallId,Side}) -> make_participant(Side) end, Partcps),
                                            Self ! {ok,Ref,Participants},
                                            {next_state, StateName, StateData}
                                    end,
                                ?CONF_SRV:apply_fun(DlgX,[F]),
                                receive {ok,Ref,Participants} -> Participants
                                after 2000 -> {error,{not_found,<<"Conference room timeout">>}}
                                end
                        end;
                    _ -> {error,{invalid_params,<<"Access denied. Invalid room">>}}
                end end,
    case R of
        {error,_}=Err -> #{error => Err};
        _ -> #{participants => R}
    end.

%% @private
make_participant(#side{callid=CallId,remoteuri=RUri,remoteparty=RPUri}=_Side) ->
    #{callid => CallId,
      uri => ?U:unparse_uri(?U:make_uri(?U:make_aor(RUri))),
      disp => ?EU:ensure_unquoted(case RPUri of #uri{disp=Disp} -> Disp; _ -> RPUri#uri.disp end)}.
