%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc
%%% @todo

-module(r_sip_conf_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([]).

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_conf.hrl").
-include("../include/r_sip_mgc.hrl").

% -include("../include/r_nksip.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

% =============================================
% Local Tags
% =============================================

build_tag() ->
    CurSrvCode = ?U:get_current_srv_textcode(),
    <<Tag0:48/bitstring, _/bitstring>> = ?U:luid(),
    <<"rCF-", CurSrvCode/bitstring, "-", Tag0/bitstring>>. % RP-1344
%<<CurSrvCode/bitstring, "-", Tag0/bitstring>>.

%% -------------------
%% Generates non existing tag
%%
generate_tag(#state_conf{usedtags=Tags}=State) ->
    Tag = build_tag(),
    case lists:member(Tag, Tags) of
        false -> {Tag, State#state_conf{usedtags=[Tag|Tags]}};
        true -> generate_tag(State)
    end.

%% ----------------------------------------------------------
%% Build call id for out call. Checks for non exist in current conf
%%

build_out_callid(#state_conf{confnum=ConfNum, participants=Partcps}=State) when is_list(Partcps) ->
    CurSrvCode = ?U:get_current_srv_textcode(),
    CallId = ?U:luid(),
    case lists:keyfind(CallId, 1, Partcps) of
        false -> <<"rCF-", CurSrvCode/bitstring, "-", ConfNum/bitstring, "-", CallId/bitstring>>;
        _ -> build_out_callid(State)
    end;
build_out_callid(#state_conf{confnum=ConfNum}) ->
    CurSrvCode = ?U:get_current_srv_textcode(),
    CallId = ?U:luid(),
    <<"rCF-", CurSrvCode/bitstring, "-", ConfNum/bitstring, "-", CallId/bitstring>>.

% =============================================
% Log to callid-link log
% =============================================

% #307, #354, RP-415
log_callid(outgoing,DlgNum,CallId,Domain) ->
    ?BLlog:write({sip,callid}, {"dlg='~s'; callid='~s'; domain='~s'",[DlgNum,CallId,Domain]});
log_callid(incoming,DlgNum,CallId,Domain) ->
    ?BLlog:write({sip,callid}, {"dlg='~s'; callid='~s'; domain='~s'",[DlgNum,CallId,Domain]}).
log_callid(referred,DlgNum,CallId,RCallId,Domain) ->
    ?BLlog:write({sip,callid}, {"dlg='~s'; callid='~s'; rfcallid='~s'; domain='~s'",[DlgNum,CallId,RCallId,Domain]});
log_callid(replaces,DlgNum,CallId,RCallId,Domain) ->
    ?BLlog:write({sip,callid}, {"dlg='~s'; callid='~s'; rpcallid='~s'; domain='~s'",[DlgNum,CallId,RCallId,Domain]}).

%% ----------------------------------------------------------
%% Build conf display name
%%
make_conf_display(#state_conf{room={_,U,_}, map=Map}=_State) ->
    D = case lists:keyfind(<<"displayname">>, 1, maps:get(opts,Map)) of
            false -> ?U:make_conf_display(U);
            {_,ConfDisp} -> ConfDisp
        end,
    ?U:quote_display(D).

%% ----------------------------------------------------------
%% Send sip response to incoming request
%%
send_response(#sipmsg{class={req,_}}=Req, SipReply, State) ->
    {ok,ReqHandle} = nksip_request:get_handle(Req),
    send_response(ReqHandle, SipReply, State);
send_response(ReqHandle, {SipCode,_Opts}=SipReply, State) ->
    d(State, "send_response ~p", [SipCode]),
    ?U:send_sip_reply(fun() -> nksip_request:reply(SipReply, ReqHandle) end),
    State;
send_response(ReqHandle, SipReply, State)  ->
    ?U:send_sip_reply(fun() -> nksip_request:reply(SipReply, ReqHandle) end),
    State.

%% -------------------
%% Finalize conf
%%
do_finalize_conf(State) ->
    % from site storage (links to conf user)
    #state_conf{map=#{g_store:={GStoreKey,_}}}=State,
    ?U:cast_store(GStoreKey, fun() -> {del, [GStoreKey]} end),
    % life timer
    #state_conf{timer_ref=TimerRef}=State,
    erlang:cancel_timer(TimerRef),
    % unlink callids (on error it hangs incoming)
    unlink_callids(State),
    State.

%% @private
%% Unlink callids
unlink_callids(#state_conf{participants=Partcps, forks=Forks}=_State) ->
    F = fun(CallId, Acc) ->
                case lists:member(CallId, Acc) of
                    false -> ?DLG_STORE:unlink_dlg(CallId), [CallId|Acc];
                    true -> Acc
                end end,
    X0 = [],
    X1 = lists:foldl(fun({CallId,#{}=_ForkCall}, Acc) -> F(CallId, Acc) end, X0, Forks),
    _X2 = lists:foldl(fun({CallId,#side{}}, Acc) -> F(CallId, Acc) end, X1, Partcps).

%% -------------------
%% Switch conf to stopping state
%%
return_stopping(#state_conf{confid=ConfId}=State) ->
    d(State, " -> switch to '~p'", [?STOPPING_STATE]),
    State1 = case State of
                 #state_conf{stopreason=undefined} -> State#state_conf{stopreason=#{}};
                 _ -> State
             end,
    % event
    ?FSM_EVENT:conf_stop(State1),
    % from media
    {ok,State2} = ?CONF_MEDIA:media_stop(State1),
    % from callstore
    ?CONFSTORE:del_conf(ConfId), % callstore
    % timeout
    erlang:send_after(?STOPPING_TIMEOUT, self(), {stopping_timeout}),
    {next_state, ?STOPPING_STATE, State2}.

%% -------------------
%% finish with error of mgc
%%
error_mgc_final(Reason, State, Side) ->
    error_mgc_final(Reason, State, Side, ?InternalError("CONF. MGC error")).

error_mgc_final("", State, Side, SipReply) -> error_mgc_final_1(State, Side, SipReply);
error_mgc_final(Reason, State, Side, SipReply) ->
    d(State, "MGC error: ~120p", [Reason]),
    State1 = State#state_conf{stopreason=#{error => reason_error([media], <<>>, Reason)}},
    error_mgc_final_1(State1, Side, SipReply).

error_mgc_final_1(State, undefined, _) ->
    ?CONF_UTILS:return_stopping(?CONF_UTILS:do_finalize_conf(State));
error_mgc_final_1(State, Side, SipReply) ->
    #side{rhandle=RHandle}=Side,
    State1 = ?CONF_UTILS:send_response(RHandle, SipReply, State),
    error_mgc_final_1(State1, undefined, SipReply).

%% -------------------
%% send bye to participant
%%
send_bye(Side, _State) ->
    #side{dhandle=DlgHandle}=Side,
    Opts = [user_agent],
    catch nksip_uac:bye(DlgHandle, [async|Opts]).


%% -------------------
%% Cancel active forks
%%
cancel_active_forks([],_,_) -> ok;
cancel_active_forks([Fork|Rest], Opts, State) ->
    #side_fork{rhandle=ReqHandle,
               cmnopts=CmnOpts}=Fork,
    cancel_timer(Fork),
    catch nksip_uac:cancel(ReqHandle, [async|Opts++CmnOpts]),
    cancel_active_forks(Rest, Opts, State).
% @private
cancel_timer(#side_fork{rule_timer_ref=T1, resp_timer_ref=T2}=Fork) ->
    case T1 of undefined -> false; _ -> erlang:cancel_timer(T1) end,
    case T2 of undefined -> false; _ -> erlang:cancel_timer(T2) end,
    Fork#side_fork{rule_timer_ref=undefined,
                   resp_timer_ref=undefined}.


%% -------------------
%% return side/fork by callid
get_participant_by_callid(CallId, #state_conf{participants=Partcps, forks=Forks}=_State) ->
    case lists:keyfind(CallId, 1, Partcps++Forks) of
        false -> undefined;
        {_,#side{}=Side} -> Side;
        {_,#{}=ForkCall} -> maps:get(fork,ForkCall,undefined)
    end.

% =============================================
% Make stop reason
% =============================================

%
reason_error(_, _, #{}=Reason) -> Reason;
reason_error(Operations, <<>>, Reason) ->
    #{type => error,
      operations => Operations,
      reason => Reason};
reason_error(Operations, CallId, Reason) ->
    #{type => error,
      operations => Operations,
      callid => CallId,
      reason => Reason}.

%
reason_timeout(Operations, <<>>, Reason) ->
    #{type => timeout,
      operations => Operations,
      reason => Reason};
reason_timeout(Operations, CallId, Reason) ->
    #{type => timeout,
      operations => Operations,
      callid => CallId,
      reason => Reason}.

%
reason_lastdetach(CallId) ->
    #{type => lastdetach,
      callid => CallId,
      reason => <<"Last participant detached">>}.

%
reason_external(#{}=Reason) -> Reason;
reason_external(Reason) ->
    #{type => external,
      reason => Reason}.

%% ====================================================================
%% Internal functions
%% ====================================================================

% @private ----
d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    #state_conf{confid=ConfId}=State,
    ?LOGSIP("CONF fsm ~p '~p':" ++ Fmt, [ConfId,?ACTIVE_STATE] ++ Args).
