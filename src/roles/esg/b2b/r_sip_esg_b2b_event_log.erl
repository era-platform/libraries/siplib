%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 14.11.2016
%%% @doc

-module(r_sip_esg_b2b_event_log).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([]).

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_esg.hrl").

%-define(OUTD(Fmt,Args), ok).
-define(OUTD(Fmt,Args), ?LOGCDR(Fmt,Args)).

%% ====================================================================
%% API functions
%% ====================================================================

% when apply representative complete
normalize_callerid({{_ACallId,_ACallIdH}}=Id, #{}=Opts) ->
    ?OUTD("normalize_callerid ~1000p, ~1000p", [id(Id), Opts]).

%% ====================================================================
%% Internal functions
%% ====================================================================

id({{ACallId,_ACallIdH}}) -> {ACallId};
id({{DialogId,_DialogIdH}, {ACallId,_ACallIdH}}) -> {DialogId, ACallId}.

dlgcid(#state_forking{dialogid=DialogId,a=#side{callid=ACallId}}=_State) -> {DialogId, ACallId};
dlgcid(#?StateDlg{dialogid=DialogId,a=#side{callid=ACallId}}=_State) -> {DialogId, ACallId}.

fork(#side_fork{callid=CallId,requesturi=RUri,localtag=LTag}) -> {?U:unparse_uri(?U:clear_uri(RUri)),LTag,CallId}.

side(#side{callid=CallId,remoteuri=RUri,localtag=LTag}) -> {?U:unparse_uri(?U:clear_uri(RUri)),LTag,CallId}.

