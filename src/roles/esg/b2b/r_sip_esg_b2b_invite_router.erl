%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc

-module(r_sip_esg_b2b_invite_router).
-author('Peter Bukashin <tbotc@yandex.ru>').

%% -------------------------------------------------------------------
%%
%% -------------------------------------------------------------------

-export([sip_invite/2,
         build_route_opts/2]).

%% ==========================================================================
%% Defines
%% ==========================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_esg.hrl").
-include("../include/r_sip_headers.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% -------------------------------------------------------------------
-spec(sip_invite(AOR::tuple(), Request::nksip:request()) ->
        {reply, nksip:sipreply()} | noreply | block).
%% -------------------------------------------------------------------
sip_invite(AOR, Req) ->
    case build_route_opts(AOR, Req) of
        noreply -> noreply;
        block -> block;
        {reply,_}=R -> R;
        {ok, L} when is_list(L) ->
            Opts = [{app,?SIPAPP},
                    {req,Req},
                    {call_pid,self()},
                    {forking_timeout, 3600000} % @TODO timeout could be redundant (5min)
                                                  % total max calling time,
                                                   % note that uri can have it's own rule limit, default limit and after_answer limit,
                                                  % also there can be sequental call, that needs more time to operate fully
                   |L],
            ?ESG_DLG:start(Opts),
            noreply
    end.

%% -------------------------------------------------------------------
-spec(build_route_opts(AOR::tuple(), Request::nksip:request()) ->
        {reply, nksip:sipreply()} | {ok,Opts::list()} | noreply | block ).
%% -------------------------------------------------------------------
build_route_opts(AOR, Req) ->
    #sipmsg{ruri=#uri{domain=_RUriDomain}=_RUri,
            vias=[#via{domain=ViaDomain, port=_ViaPort}|_],
            nkport=#nkport{remote_ip=RemoteIp}}=Req,
    % from inside or from outside?
    % check via domain existence to detect is this call from inside or outside
    Insiders = ?CFG:get_all_sipserver_addrs(),
    BRemoteIp = ?EU:to_binary(inet:ntoa(RemoteIp)),
    % check esg header and from domain existence to detect if this call from inside or outside
    {ok,EAH} = nksip_request:header(?ExtAccountHeaderLow, Req),
    case {lists:member(ViaDomain,Insiders), lists:member(BRemoteIp,Insiders)} of
        {false,_} -> invite_from_outer(AOR, Insiders, Req);
        {true,true} when length(EAH)>0 -> invite_from_inner(AOR, Req);
        {true,true} -> invite_from_outer(AOR, Insiders, Req);
        {true,false} ->
            ?OUT("ESG. Incoming request forbidden, via ~120p is cluster address, but remote ~120p is not", [ViaDomain, BRemoteIp]),
            ?FILTER:check_block_response_malware(invalid_address, Req, {reply, ?Forbidden("EG.InOut. Invalid remote address")})
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% =================================
%% Invite from inner outside
%% =================================

invite_from_inner(_AOR, Req) ->
    ?U:send_100_trying(Req),
    case ?U:parse_ext_account_header(Req) of
        undefined -> {reply, ?Forbidden("EB2B.InOut. Missed X-Era-ExtAccount header")};
        {{ExtU,ExtD},ExtOpts} ->
            % check account
            FilterOpts = lists:keydelete(<<"routecode">>,1,ExtOpts),
            case ?EXT_REGSRV:check_ruri(ExtU,ExtD,FilterOpts) of
                {true, AMap} -> do_invite_from_inner(Req, AMap);
                false -> {reply, ?Forbidden("EB2B.InOut. Unknown account")}
            end end.

%% ------------------------------
%% Check replaces
%% ------------------------------
do_invite_from_inner(Req, AMap) ->
    case nksip_request:header("replaces", Req) of
        {ok,[Replaces|_]} -> route_referred(Req, AMap, Replaces, ?SideLocal);
        _ -> route_outside_1(Req, AMap, #{})
    end.

%% ------------------------------
%% Route outside
%% ------------------------------
route_outside_1(Req, AMap, Ctx) ->
    % Outbound proxy
    Dom = maps:get(domain,AMap,undefined),
    PrA = maps:get(proxyaddr,AMap,undefined),
    PrP = maps:get(proxyport,AMap,5060),
    Tr = maps:get(transport,AMap,<<"udp">>),
    RouteUri = #uri{scheme=sip,
                    domain=case PrA of undefined -> Dom; <<>> -> Dom; _ -> PrA end,
                    port=case PrP of 0 -> 5060; _ -> PrP end,
                    opts=[{<<"transport">>,Tr}, <<"lr">>]},
    route_outside_2(RouteUri, Req, AMap, Ctx).

route_outside_2(RouteUri, Req, AMap, Ctx) ->
    route_outside_3(RouteUri, Req, AMap, Ctx).

%%
route_outside_3(#uri{domain=RouteDomain}=RouteUri, Req, AMap, Ctx) ->
    % media usage depends on settings
    UseMedia = maps:get(media,AMap),
    % destination ruri corrected by account domain. filled by destination route
    NRule = find_outside_normalization_rule(Req, AMap),
    % @TODO @remoteparty for b on invite header 'from'
    BLUri = ?U:clear_uri(build_outside_from_uri(Req, NRule, AMap), [user_orig]), % #429
    BRUri = ?U:clear_uri(build_outside_to_uri(Req, BLUri, NRule, AMap), [user_orig]), % #429
    BLContact = ?U:clear_uri(build_outside_contact_uri(Req, AMap), [user_orig]), % #429
    % event to cdr
    event_normalize_callerid(#{domain => maps:get(clusterdomain,AMap),
                               providerid => maps:get(id,AMap),
                               from => ?U:make_aor(element(1,Req#sipmsg.from)),
                               to => ?U:make_aor(element(1,Req#sipmsg.to)),
                               ruleid => case NRule of #{} -> maps:get(id,NRule); _ -> undefined end,
                               fromnew => ?U:make_aor(BLUri),
                               tonew => ?U:make_aor(BRUri)}, Req),
    %
    FwdOpts = [{route, ?U:unparse_uri(RouteUri)}],
    UriRules = [#{uri => BRUri,
                  ftimeout => 3610000, % @forktimeout timeout should be redundant (1 hr). Limited by resp_timer auto enlarged on 182/183
                  opts => FwdOpts,
                  bl_uri => BLUri,
                  bl_contact => BLContact}],
    %
    Opts = [{urirules,UriRules},
            %% -----------------
            {dir,?DirOutside},
            {account,AMap},
            {replaces,maps:get(replaces,Ctx,undefined)}, % #115
            {use_media,?EU:to_bool(UseMedia)},
            {routedomain,RouteDomain}, % to detect media iface (single fork)
            {addRemoteParty1xx, true},
            {addRemoteParty2xx, true}
           ],
    {ok, Opts}.

%% @private
find_outside_normalization_rule(Req, AMap) ->
    #sipmsg{from={FromUri,_},
            to={ToUri,_}}=Req,
    ?ESG_UTILS:find_inner_normalization_rule(FromUri, ToUri, AMap).

%% @private
% by normalization rule modificators
build_outside_from_uri(Req, NRule, AMap) ->
    #sipmsg{from={FromUri,_},
            to={ToUri,_}}=Req,
    ?ESG_UTILS:build_inner_from_uri(false, FromUri, ToUri, NRule, AMap).

%% @private
% by normalization rule modificators
build_outside_to_uri(Req, NewFromUri, NRule, AMap) ->
    #sipmsg{ruri=RUri}=Req, % check, really apply to ruri, or to
    RUri1 = RUri#uri{domain=maps:get(domain,AMap)},
    ?ESG_UTILS:build_inner_to_uri(false, NewFromUri, RUri1, NRule, AMap).

%% @private
build_outside_contact_uri(_Req, AMap) ->
    User = maps:get(username, AMap),
    #uri{scheme=sip, disp= <<>>, user=User}.

%% =================================
%% Invite from outer inside
%% =================================

invite_from_outer(_AOR, _Insiders, Req) ->
    % reg: {to:user, to:domain}, {contact:user, contact:domain}
    % no reg: {from:user, from:domain}
    #sipmsg{to={#uri{user=ToUser, domain=ToDomain},_},
            from={#uri{domain=FromDomain},_}}=Req, % nkport=#nkport{transp=TrProto}
    % -----------
    % 1. check registered, by To:user/domain
    case ?EXT_REGSRV:check_ruri(ToUser,ToDomain,[{reg,true}]) of % {transport,TrProto}
        {true,AMap} -> invite_from_outer_1(ToUser, Req, AMap); % by to : reg
        false ->
            % -----------
            % 2. check registered by To:user/From:domain (new asterisk, RP-2002 mk 16.05.2020)
            case ?EXT_REGSRV:check_ruri(ToUser,FromDomain,[{reg,true}]) of
                {true,AMap} -> invite_from_outer_1(ToUser, Req, AMap); % by to & from : reg
                false ->
                    case Req of
                        #sipmsg{contacts=[#uri{user=CUser, domain=CDomain}|_]} ->
                            % -----------
                            % 3. check registered, by Contact:user/domain
                            case ?EXT_REGSRV:check_ruri(CUser,CDomain,[{reg,true}]) of
                                {true,AMap} -> invite_from_outer_1(CUser, Req, AMap); % by contact : reg
                                false ->
                                    % -----------
                                    % 4. check nonreg, by Contact:user/domain
                                    case ?EXT_REGSRV:check_ruri(CUser,CDomain,[{reg,false}]) of
                                        {true,AMap} -> invite_from_outer_1(CUser, Req, AMap); % by contact : noreg
                                        false ->
                                            % -----------
                                            % 5. check nonreg, by Contact:<<>>/domain
                                            case ?EXT_REGSRV:check_ruri(<<>>,CDomain,[{reg,false}]) of
                                                {true,AMap} -> invite_from_outer_1(<<>>, Req, AMap); % by contact : noreg, username empty in account
                                                false ->
                                                    % -----------
                                                    % 6. check nonreg, by To:user/From:domain
                                                    case ?EXT_REGSRV:check_ruri(ToUser,FromDomain,[{reg,false}]) of
                                                        {true,AMap} -> invite_from_outer_1(CUser, Req, AMap); % by to & from : noreg
                                                        false ->
                                                            fail(Req),
                                                            check_filter_on_reply(Req, {reply, ?Forbidden("EB2B.OutIn. Unknown account in contact")})
                                                    end
                                            end
                                    end
                            end;
                        _ ->
                            fail(Req),
                            check_filter_on_reply(Req, {reply, ?Forbidden("EB2B.OutIn. Unknown account, no contact, no store")})
                    end
            end
    end.

%% @private
check_filter_on_reply(Req, Reply) ->
    case ?FILTER:check_route_inside(Req) of
        block -> block;
        _ -> ?FILTER:check_block_response_malware(unknown_account, Req, Reply)
    end.

%% ------------------
%% checks remote transport ip-address
%% ------------------
invite_from_outer_1(_User, Req, AMap) ->
    #sipmsg{nkport=#nkport{remote_ip=TrAddr}}=Req,
    TrAddr1 = ?EU:to_binary(inet:ntoa(TrAddr)),
    case ?EXT_REGSRV:check_incoming_extaddr(TrAddr1, AMap) of
        ok -> invite_from_outer_2(Req, AMap);
        {error,R} ->
            ?OUT("Received Addr check failed: ~120p", [R]),
            fail(Req),
            check_filter_on_reply(Req, {reply, ?Forbidden("EB2B.OutIn. Unknown remote transport address")})
    end.

%% ------------------
%% RP-493
%% checks active trunk limit
%% ------------------
invite_from_outer_2(Req, AMap) ->
    ?U:send_100_trying(Req),
    case ?EXT_UTILS:check_limit(outer,AMap) of
        true -> do_invite_from_outer(Req, AMap);
        false -> {reply, ?Forbidden("EB2B.OutIn. Trunk limit reached")}
    end.

%% -----------------------------------
%% check replaces
%% -----------------------------------
do_invite_from_outer(Req, AMap) ->
    {ok,ReplacesH} = nksip_request:header("replaces", Req),
    case ReplacesH of
        [] -> route_inside_1(Req, AMap, #{});
        [Replaces|_] -> route_referred(Req, AMap, Replaces, ?SideRemote)
    end.

%% ------------------------------------
%% Route inside
%% -----------------------------------
route_inside_1(Req, AMap, Ctx) ->
    #sipmsg{call_id=CallId} = Req,
    % find and link b2bua
    case ?SERVERS:find_responsible_b2bua_nostore(CallId) of
        false -> {reply, ?ServiceUnavailable("EB2B.OutIn. Responsible servers not found")};
        RouteAddrPort ->
            [RouteDomain,RoutePort] = binary:split(RouteAddrPort, <<":">>),
            RouteUri = #uri{scheme=sip,
                            domain=RouteDomain,
                            port=?EU:to_int(RoutePort),
                            opts=?CFG:get_transport_opts()++[<<"lr">>]},
            route_inside_2(RouteUri, Req, AMap, Ctx)
    end.

%%
route_inside_2(#uri{domain=RouteDomain}=RouteUri, Req, AMap, Ctx) ->
    NRule = find_inside_normalization_rule(Req, AMap),
    % destination ruri
    FwdOpts = [{route, ?U:unparse_uri(RouteUri)}],
    BLUri = build_inside_from_uri(Req, NRule, AMap), % @TODO @remoteparty for b on invite header 'from'
    RUri = build_inside_route_uri(Req, BLUri, NRule, AMap),
    BLContact = build_inside_contact_uri(Req, AMap),
    UriRules = [#{uri => RUri,
                  ftimeout => 3610000, % @forktimeout timeout should be redundant (1 hr), call could be forked or queued by b2bua. Limited by resp_timer auto enlarged on 182/183
                  opts => FwdOpts,
                  %extaccount => AMap,
                  bl_uri => BLUri,
                  bl_contact => BLContact}
               ],
    UseMedia = maps:get(media,AMap),
    %
    Opts = [{urirules,UriRules},
            %% -----------------
            %% to prepare common side's initials (not extremely need)
            {bl_uri, BLUri},
            {bl_contact, BLContact},
            %% -----------------
            {dir,?DirInside},
            {account,AMap},
            {replaces,maps:get(replaces,Ctx,undefined)}, % #115
            {use_media,?EU:to_bool(UseMedia)},
            {routedomain,RouteDomain}, % to detect media iface (single fork)
            {addRemoteParty1xx, true},
            {addRemoteParty2xx, true}
           ],
    {ok, Opts}.

%% @private
find_inside_normalization_rule(Req, AMap) ->
    #sipmsg{from={FromUri,_},
            to={ToUri,_}}=Req,
    ?ESG_UTILS:find_outer_normalization_rule(FromUri, ToUri, AMap).

%% @private
build_inside_from_uri(Req, NRule, AMap) ->
    #sipmsg{from={FromUri,_},
            to={ToUri,_}}=Req,
    ?ESG_UTILS:build_outer_from_uri(false, FromUri, ToUri, NRule, AMap).

%% @private
build_inside_route_uri(Req, NewFromUri, NRule, AMap) ->
    User = maps:get(username,AMap),
    CD = maps:get(clusterdomain,AMap),
    % #261
    NUser = case Req of
                #sipmsg{ruri=#uri{user=User}} -> <<>>;
                #sipmsg{ruri=#uri{user=Number}} -> Number
            end,
    Uri = #uri{scheme=sip,
               user=NUser,
               domain=CD,
               opts=?CFG:get_transport_opts()},
    ?ESG_UTILS:build_outer_to_uri(false, NewFromUri, Uri, NRule, AMap).

%% @private
build_inside_contact_uri(_Req, _AMap) ->
    #uri{scheme=sip, disp= <<>>, user= <<>>}.

%% @private
% consider failed request to ban remote address
fail(#sipmsg{nkport=#nkport{remote_ip=Ip}}=_Req) ->
    ?FILTER:consider_failed_request_from(Ip).

%% =================================
%% Referred/replaced
%% =================================

%% ----------------------------------------
%% Route referred (change abonent)
%% ----------------------------------------
route_referred(Req, AMap, Replaces, ZType) ->
    [RepCallId|_] = binary:split(Replaces, <<$;>>, [trim_all]),
    ?STAT_FACADE:link_replaced(Req#sipmsg.call_id, RepCallId), % @stattrace
    case ?ESG_DLG:pull_fsm_by_callid(RepCallId) of
        false ->
            ?LOGSIP("EB2B.Ref. Unknown replaced dialog: ~120p", [RepCallId]),
            {reply, ?CallLegTransDoesNotExist("EB2B.Ref. Unknown replaced dialog")};
        DlgX ->
            route_referred_2(Req, AMap, {Replaces, ZType, RepCallId, DlgX})
    end.

%%
route_referred_2(Req, AMap, {Replaces, ZType, RepCallId, DlgX}) ->
    case parse_tags(Replaces) of
        #{from:=FTag,to:=TTag} ->
            case ?ESG_DLG:get_tags(DlgX) of
                % forking
                #{state:='forking', a:=#{callid:=RepCallId, ltag:=T1, rtag:=T2}}=T
                  when (T1==FTag andalso T2==TTag) orelse (T1==TTag andalso T2==FTag) ->
                    route_referred_forking(Req, AMap, {Replaces, ZType, RepCallId, DlgX, T});
                #{state:='forking'} ->
                    ?LOGSIP("EB2B.Ref. Replaced dialog tags wrong: ~120p", [Replaces]),
                    {reply, ?CallLegTransDoesNotExist("EB2B.Ref. Replaced call leg tags wrong")};
                % dialog
                #{a:=#{callid:=RepCallId, ltag:=T1, rtag:=T2}, b:=#{}}
                  when (T1==FTag andalso T2==TTag) orelse (T1==TTag andalso T2==FTag) ->
                    route_referred_dialog(Req, AMap, DlgX, ZType);
                #{b:=#{callid:=RepCallId, ltag:=T1, rtag:=T2}, a:=#{}}
                  when (T1==FTag andalso T2==TTag) orelse (T1==TTag andalso T2==FTag) ->
                    route_referred_dialog(Req, AMap, DlgX, ZType);
                #{a:=_,b:=_} ->
                    ?LOGSIP("EB2B.Ref. Replaced dialog tags wrong: ~120p", [Replaces]),
                    {reply, ?CallLegTransDoesNotExist("EB2B.Ref. Replaced call leg tags wrong")};
                % error
                _T ->
                    ?LOGSIP("EB2B.Ref. Replaced dialog error: ~120p", [RepCallId]),
                    {reply, ?InternalError("EB2B.Ref. Replaced dialog error")}
            end;
        _ ->
            ?LOGSIP("EB2B.Ref. No tags found in Replaces header: ~120p", [Replaces]),
            {reply, ?BadRequest("EB2B.Ref. No tags found in replaces")}
    end.

%% ------------------------------
%% when invite replaces dialog call
%% ------------------------------
route_referred_dialog(Req, _AMap, DlgX, ZType) ->
    router_referred_replace(DlgX,Req,ZType).

%% ------------------------------
%% #115
%% when invite replaces forking call (rfc-3311 or caching)
%% ------------------------------
route_referred_forking(Req, AMap, {Replaces, ZType, RepCallId, DlgX, _}) ->
    % #115
    case ?ESG_DLG:get_current_info(DlgX) of
        {ok,#{state:='forking',b:=BS}} ->
            case BS of
                [] ->
                    ?LOGSIP("EB2B.Ref. Replaced dialog is forking (0 forks): ~120p", [RepCallId]),
                    ?ESG_DLG:stop(DlgX, "Referring invite failed (0 forks found)"),
                    {reply, ?Pending("EB2B.Ref. Call is forking, 0 forks")};
                [_,_|_]=L ->
                    ?LOGSIP("EB2B.Ref. Replaced dialog is forking (~p forks): ~120p", [length(L),RepCallId]),
                    ?ESG_DLG:stop(DlgX, "Referring invite failed (too many forks)"),
                    {reply, ?Pending(?EU:str("EB2B.Ref. Call is forking, ~p forks",[length(L)]))};
                [B|_] ->
                    case maps:get(brtag,B) of
                        undefined ->
                            ?LOGSIP("EB2B.Ref. Replaced dialog is forking (Fork to-tag not defined yet): ~120p", [RepCallId]),
                            ?ESG_DLG:stop(DlgX, "Referring invite failed (fork to-tag undefined)"),
                            {reply, ?Pending("EB2B.Ref. Call is forking, to-tag undefined")};
                        _RTag ->
                            case maps:get(replacemode,AMap,0) of
                                0 -> % #265
                                    router_referred_replace(DlgX,Req,ZType);
                                1 -> % #115
                                    router_referred_forward(B,Req,ZType,AMap)
                            end end end;
        {ok,#{state:='dialog'}} ->
            route_referred_2(Req, AMap, {Replaces, ZType, RepCallId, DlgX});
        _ ->
            ?LOGSIP("EB2B.Ref. Replaced dialog error: ~120p", [RepCallId]),
            {reply, ?InternalError("EB2B.Ref. Replaced dialog error")}
    end.

%% ------------------------------
%% @private
%% #265
%% ------------------------------
router_referred_replace(DlgX,Req,ZType) ->
    %?OUT("EB2B. REPLACE #265"),
    #sipmsg{call_id=CallId}=Req,
    ?ESG_DLG:sip_replace_invite(DlgX, [CallId, Req, ZType]),
    noreply.

%% ------------------------------
%% @private
%% #115
%% ------------------------------
router_referred_forward(B,Req,ZType,AMap) ->
    %?OUT("EB2B. REPLACE #115"),
    [CallId,LTag,RTag,BLUri] = ?EU:maps_get([bcallid,bltag,brtag,bluri],B),
    Opts = [{add, ?Replaces, <<CallId/bitstring,";from-tag=",LTag/bitstring,";to-tag=",RTag/bitstring>>},
            {add, ?ReferredBy, ?U:unparse_uri(?U:clear_uri(BLUri))}],
    Ctx = #{replaces => Opts},
    case ZType of
        ?SideRemote -> route_inside_1(Req, AMap, Ctx);
        ?SideLocal -> route_outside_1(Req, AMap, Ctx)
    end.

%% ------------------------------
parse_tags(Replaces) when is_binary(Replaces) ->
    [_|Tags] = binary:split(Replaces, <<$;>>, [global, trim_all]),
    parse_tags(Tags, #{}).

parse_tags([], Acc) -> Acc;
parse_tags([<<"from-tag=",FTag/bitstring>>|Rest], Acc) ->
    parse_tags(Rest, Acc#{from => FTag});
parse_tags([<<"to-tag=",TTag/bitstring>>|Rest], Acc) ->
    parse_tags(Rest, Acc#{to => TTag});
parse_tags([_|Rest], Acc) ->
    parse_tags(Rest, Acc).

%% ===================================================================
%% Events
%% ===================================================================

%% find representative
event_normalize_callerid(#{}=Opts, Req) ->
    ACallId = Req#sipmsg.call_id,
    ACallIdH = erlang:phash2(ACallId),
    ?EVENT:normalize_callerid({{ACallId,ACallIdH}}, Opts).

