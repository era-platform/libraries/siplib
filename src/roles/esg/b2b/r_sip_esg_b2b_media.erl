%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc

-module(r_sip_esg_b2b_media).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_esg.hrl").
-include("../include/r_sip_mgc.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ---------------------------------
%% checks if media is available (mgc-mg pair, or no media is need)
%% ---------------------------------
-spec check_available(State::#state_forking{}) -> {ok, true|false} | {error,Reason::term()}.
%% ---------------------------------
check_available(#state_forking{use_media=false}=_State) -> {ok, true};
check_available(State) ->
    ?MGC:mg_check_any_available(get_mg_opts(State)).

%% ---------------------------------
%% checks if media is available and return count of  (mgc-mg pair, or no media is need)
%% ---------------------------------
-spec check_available_slots(State::#state_forking{}|#?StateDlg{}) -> {ok, MGCnt::integer(), CtxCnt::integer()} | {error,Reason::term()}.
%% ---------------------------------
check_available_slots(State) ->
    ?MGC:mg_check_available_slots(get_mg_opts(State)).

%% ---------------------------------
%% prepare media by creating new context (first participant)
%% ---------------------------------
-spec prepare_start(State::#state_forking{}) -> {ok, NewState::#state_forking{}} | {error, SipReply::{SipCode::integer(),SipOpts::list()}}.
%% ---------------------------------
prepare_start(#state_forking{use_media=false}=State) ->
    {ok, State};

prepare_start(#state_forking{}=State) ->
    #state_forking{dialogid=DialogId,
                   a=#side{remotesdp=RSdp},
                   map=#{init_req:=Req,
                         opts:=DlgOpts,
                         account:=AMap}}=State,
    Reinvite = ?EU:to_bool(maps:get('reinvite',AMap)),
    % @todo don't check media. always forward
    case ?M_SDP:check_known_fmt(RSdp) of
        false ->
            d(State, "MGC error: Not supported media"),
            {error, ?MediaNotSupported("EB2B.F. Unknown audio formats")};
        true ->
            <<B4:32/bitstring, _/bitstring>> = ?U:luid(),
            MSID = <<DialogId/bitstring, "_", B4/bitstring>>,
            Aliases = define_iface_aliases(Req, DlgOpts),
            MediaOpts = #{transcoding => not Reinvite, % transcoding enabled if reinvite terminates on esg, else disabled
                          rec => false, % record disabled
                          mg_opts => get_mg_opts(State),
                          aliases => Aliases},
            {Mks,Res} = timer:tc(fun() -> ?MEDIA_B2B:fwd_start_media(MSID, Req, MediaOpts) end),
            d(State, " .. prepare_media (~p ms)", [Mks/1000]),
            case Res of
                {reply, SipReply} when is_tuple(SipReply) ->
                    d(State, "MGC error: create media context failure"),
                    {error, SipReply};
                {ok, Media} ->
                    State1 = State#state_forking{media=Media},
                    {ok, State1}
            end
    end.

% @private
define_iface_aliases(#sipmsg{nkport=#nkport{local_ip=FromIface, remote_ip=FromIp}}=_Req, DlgOpts) ->
    try [RouteDomain] = ?EU:extract_required_props([routedomain], DlgOpts), % route domain (single fork)
        case nkpacket_dns:resolve(#uri{scheme=sip,domain=RouteDomain}) of
            {ok,[]} -> undefined;
            {ok,[{_,RouteAddr,_}|_]} ->
                FromIface1 = case FromIface of {0,0,0,0} -> ?U:get_host_addr(FromIp); _ -> FromIface end, % 07.11.2016
                ToIface = ?U:get_host_addr(RouteAddr),
                {?U:get_interface_alias(FromIface1), ?U:get_interface_alias(ToIface)}
        end
    catch _:_ -> undefined
    end.

% @private
define_iface_alias(RouteDomain) when is_binary(RouteDomain) ->
    [RD1|_] = binary:split(RouteDomain,<<":">>),
    try case nkpacket_dns:resolve(#uri{scheme=sip,domain=RD1}) of
            {ok,[]} -> undefined;
            {ok,[{_,RouteAddr,_}|_]} ->
                Iface = ?U:get_host_addr(RouteAddr),
                ?U:get_interface_alias(Iface)
        end
    catch _:_ -> undefined
    end;
define_iface_alias(#sipmsg{nkport=#nkport{local_ip=FromIface,remote_ip=FromIp}}=_Req) ->
    FromIface1 = case FromIface of {0,0,0,0} -> ?U:get_host_addr(FromIp); _ -> FromIface end,
    ?U:get_interface_alias(FromIface1).

%% ---------------------------------
%% stops existing media context
%% ---------------------------------
-spec stop(State::#state_forking{}|#?StateDlg{}) -> {ok, NewState::#state_forking{}|#?StateDlg{}}.
%% ---------------------------------
stop(#state_forking{use_media=false}=State) -> {ok,State};
stop(#state_forking{media=undefined}=State) -> {ok,State};
stop(#?StateDlg{media=undefined}=State) -> {ok,State};
stop(#state_forking{media=Media}=State) -> ?MEDIA_B2B:stop_media(Media), {ok,State#state_forking{media=undefined}};
stop(#?StateDlg{media=Media}=State) -> ?MEDIA_B2B:stop_media(Media), {ok,State#?StateDlg{media=undefined}}.


%% ---------------------------------
%% returns map with current media properties (mgc/mg/ctxid)
%% ---------------------------------
-spec get_current_media_link(State::#state_forking{}|#?StateDlg{}) -> {ok, Map::map()} | undefined.
%% ---------------------------------
get_current_media_link(State) ->
    case State of
        #?StateDlg{media=Media} -> ok;
        #state_forking{media=Media} -> ok
    end,
    case Media of
        undefined -> undefined;
        #media{mgc=MGC, msid=MSID, mgid=MG, ctx=CtxId} -> {ok, #{mgc=>MGC, msid=>MSID, mgid=>MG, ctx=>CtxId}}
    end.

%% ---------------------------------
%% prepare forward request from a to b, make local sdp for b
%% ---------------------------------
-spec fwd_invite_request_to_b(State::#state_forking{}, BUri::#uri{}, BCallId::binary()) ->
                {ok, NewState::#state_forking{}, LSdp::#sdp{}} | {error, Reason::term()}.
%% ---------------------------------
fwd_invite_request_to_b(#state_forking{use_media=false}=State, _BUri, _BCallId) ->
    #state_forking{a=#side{remotesdp=ARSdp}}=State,
    {ok, State, ARSdp};

fwd_invite_request_to_b(State, Uri, BCallId) ->
    #state_forking{media=Media}=State,
    case ?MEDIA_B2B:fwd_invite_request_to_b(Media, Uri, BCallId) of
        {error,_}=Err -> Err;
        {ok,Media1,LSdp} ->
            {ok, State#state_forking{media=Media1}, LSdp}
    end.

%% ---------------------------------
%% prepare forward response from b to a, make local sdp for a
%% ---------------------------------
-spec fwd_invite_response_to_a(State::#state_forking{}, Resp::#sipmsg{}) -> {ok, NewState::#state_forking{}, LSdp::#sdp{}} | {error, Reason::term()}.
%% ---------------------------------
fwd_invite_response_to_a(#state_forking{use_media=false}=State, #sipmsg{}=Resp) ->
    #sdp{}=BRSdp = ?U:extract_sdp(Resp),
    {ok, State, BRSdp};

fwd_invite_response_to_a(State, Resp) ->
    #state_forking{media=Media}=State,
    case ?MEDIA_B2B:fwd_invite_response_to_a(Media, Resp) of
        {error,_}=Err -> Err;
        {ok,Media1,LSdp} ->
            {ok, State#state_forking{media=Media1}, LSdp}
    end.



%% ---------------------------------
%% prepare forward reinvite request from x to y, make local sdp for y
%% ---------------------------------
-spec fwd_reinvite_request_to_y(State::#?StateDlg{}, Uri::#uri{}) -> {ok, NewState::#?StateDlg{}, LSdp::#sdp{}} | {error, Reason::term()}.
%% ---------------------------------
fwd_reinvite_request_to_y(#?StateDlg{use_media=false}=State, #sipmsg{}=Req) ->
    #sdp{}=XRSdp = ?U:extract_sdp(Req),
    {ok, State, XRSdp};

fwd_reinvite_request_to_y(State, Req) ->
    #?StateDlg{media=Media}=State,
    case ?MEDIA_B2B:fwd_reinvite_request_to_y(Media, Req) of
        {error,_}=Err -> Err;
        {ok,Media1,LSdp} ->
            {ok, State#?StateDlg{media=Media1}, LSdp}
    end.

%% ---------------------------------
%% prepare forward reinvite response from y to x, make local sdp for x
%% ---------------------------------
-spec fwd_reinvite_response_to_x(State::#?StateDlg{}, Uri::#uri{}) -> {ok, NewState::#?StateDlg{}, LSdp::#sdp{}} | {error, Reason::term()}.
%% ---------------------------------
fwd_reinvite_response_to_x(#?StateDlg{use_media=false}=State, #sipmsg{}=Resp) ->
    #sdp{}=YRSdp = ?U:extract_sdp(Resp),
    {ok, State, YRSdp};

fwd_reinvite_response_to_x(State, Resp) ->
    #?StateDlg{media=Media}=State,
    case ?MEDIA_B2B:fwd_reinvite_response_to_x(Media, Resp) of
        {error,_}=Err -> Err;
        {ok,Media1,LSdp} ->
            {ok, State#?StateDlg{media=Media1}, LSdp}
    end.

%% ---------------------------------
%% rollbacks reinvite response on 4xx-6xx (mg y term could be already modified (local sdp))
%% ---------------------------------
-spec rollback_reinvite(State::#?StateDlg{}) -> {ok, NewState::#?StateDlg{}} | {error, Reason::term()}.
%% ---------------------------------
rollback_reinvite(#?StateDlg{use_media=false}=State) ->
    {ok, State};

rollback_reinvite(State) ->
    #?StateDlg{media=Media}=State,
    case ?MEDIA_B2B:rollback_reinvite(Media) of
        {error,_}=Err -> Err;
        {ok,Media1} ->
            {ok, State#?StateDlg{media=Media1}}
    end.

%% ---------------------------------
%% prepare forward reinvite response from y to x, make local sdp for x
%% ---------------------------------
-spec mirror_reinvite_request(State::#?StateDlg{}, Uri::#uri{}) -> {ok, NewState::#?StateDlg{}, LSdp::#sdp{}} | {error, Reason::term()}.
%% ---------------------------------
mirror_reinvite_request(#?StateDlg{use_media=false}=_State, _Req) -> {error, "media/reinvite disabled"};

mirror_reinvite_request(State, Req) ->
    #?StateDlg{media=Media}=State,
    case ?MEDIA_B2B:mirror_reinvite_request(Media, Req) of
        {error,_}=Err -> Err;
        {ok,Media1,LSdp} ->
            {ok, State#?StateDlg{media=Media1}, LSdp}
    end.

%% ---------------------------------
%% checks if MGC/MG pair, linked in current state data corresponds to args
%% ---------------------------------
-spec media_check_mg(State::#?StateDlg{}|#state_forking{}, Args::list()) -> true | false.
%% ---------------------------------
media_check_mg(#state_forking{use_media=false}=_State, _Args) -> false;
media_check_mg(#?StateDlg{use_media=false}=_State, _Args) -> false;
media_check_mg(State, Args) ->
    ?MEDIA_B2B:check_media_mg(get_media(State), Args).

%% ===========================================

onreplace_x_subtract(#?StateDlg{use_media=false}=State, _XRTag) -> {ok,State};
onreplace_x_subtract(State, XRTag) ->
    #?StateDlg{media=Media}=State,
    case ?MEDIA_B2B:onreplace_x_subtract(Media, XRTag) of
        {ok, Media1} -> {ok, State#?StateDlg{media=Media1}};
        {error,_}=Err -> Err
    end.

onreplace_y_request(#?StateDlg{use_media=false}=State,_YRTag,ZRSdp) -> {ok, State, ZRSdp};
onreplace_y_request(State,YRTag,ZRSdp) ->
    #?StateDlg{media=Media}=State,
    case ?MEDIA_B2B:onreplace_y_request(Media,YRTag,ZRSdp) of
        {ok,Media1,YLSdp} -> {ok, State#?StateDlg{media=Media1}, YLSdp};
        {error,_}=Err -> Err
    end.

onreplace_y_response(#?StateDlg{use_media=false}=State, _Resp) -> {ok,State};
onreplace_y_response(State,Resp) ->
    #?StateDlg{media=Media}=State,
    case ?MEDIA_B2B:onreplace_y_response(Media,Resp) of
        {ok,Media1} -> {ok, State#?StateDlg{media=Media1}};
        {error,_}=Err -> Err
    end.

onreplace_z_add(#?StateDlg{use_media=false}=State, {_ZReq,#sipmsg{}=YResp}, {_ZTag,_XTag}) ->
    #sdp{}=YRSdp = ?U:extract_sdp(YResp),
    {ok, State, YRSdp};
onreplace_z_add(State, {ZReq,YResp}, {ZTag,XTag}) ->
    #?StateDlg{media=Media}=State,
    ZIfaceAlias = define_iface_alias(ZReq),
    case ?MEDIA_B2B:onreplace_z_add(Media, {ZReq,YResp}, {ZTag,XTag}, ZIfaceAlias) of
        {ok, Media1, ZLSdp} -> {ok, State#?StateDlg{media=Media1}, ZLSdp};
        {error,_}=Err -> Err
    end.

onreplace_z_add_mirror(#?StateDlg{use_media=false}=_State, _ZReq, {_ZTag,_XTag}) -> {error, "media/reinvite disabled"};
onreplace_z_add_mirror(State, ZReq, {ZTag,XTag}) ->
    #?StateDlg{media=Media}=State,
    ZIfaceAlias = define_iface_alias(ZReq),
    case ?MEDIA_B2B:onreplace_z_add_mirror(Media, ZReq, {ZTag,XTag}, ZIfaceAlias) of
        {ok, Media1, ZLSdp} -> {ok, State#?StateDlg{media=Media1}, ZLSdp};
        {error,_}=Err -> Err
    end.

%% ========================================

onrefer_z_request(#?StateDlg{use_media=false}=State,XTag,_,_) ->
    case State of
        #?StateDlg{a=#side{remotetag=RT,localtag=LT}, b=#side{remotesdp=YRSdp}} when RT==XTag; LT==XTag -> YRSdp;
        #?StateDlg{b=#side{remotetag=RT,localtag=LT}, a=#side{remotesdp=YRSdp}} when RT==XTag; LT==XTag -> YRSdp
    end,
    {ok, State, ?SENDRECV(?UNZEROIP(YRSdp))}; % RP-25
onrefer_z_request(State,XTag,RouteDomain,SendReinvite) ->
    #?StateDlg{media=Media}=State,
    ZIfaceAlias = define_iface_alias(RouteDomain),
    case ?MEDIA_B2B:onrefer_z_request(Media,XTag,ZIfaceAlias,SendReinvite) of
        {ok, Media1, ZLSdp} -> {ok, State#?StateDlg{media=Media1}, ZLSdp};
        {error,_}=Err -> Err
    end.

onrefer_z_response(#?StateDlg{use_media=false}=State, _Resp) -> {ok, State};
onrefer_z_response(State,Resp) ->
    #?StateDlg{media=Media}=State,
    case ?MEDIA_B2B:onrefer_z_response(Media,Resp) of
        {ok, Media1} -> {ok, State#?StateDlg{media=Media1}};
        {error,_}=Err -> Err
    end.

onrefer_y_request(#?StateDlg{use_media=false}=State,_YRTag,ZRSdp) -> {ok,State,ZRSdp};
onrefer_y_request(State,YRTag,ZRSdp) ->
    #?StateDlg{media=Media}=State,
    case ?MEDIA_B2B:onrefer_y_request(Media,YRTag,ZRSdp) of
        {error,_}=Err -> Err;
        {ok,Media1,YLSdp} -> {ok,State#?StateDlg{media=Media1},YLSdp}
    end.

onrefer_y_response(#?StateDlg{use_media=false}=State, _Resp) -> {ok, State};
onrefer_y_response(State,Resp) ->
    #?StateDlg{media=Media}=State,
    case ?MEDIA_B2B:onrefer_y_response(Media,Resp) of
        {ok, Media1} -> {ok, State#?StateDlg{media=Media1}};
        {error,_}=Err -> Err
    end.

%% ---------------------------------
%% apply ack's remote sdp
%% ---------------------------------
-spec apply_remote_ext(CallId::binary(), RSdp::#sdp{}, State::#?StateDlg{}) -> {ok, NewState::#?StateDlg{}} | {error, Reason::term()}.
%% ---------------------------------
apply_remote_ext(_CallId, _RSdp, #?StateDlg{use_media=false}=State) ->
    {ok, State};

apply_remote_ext(CallId, RSdp, State) ->
    #?StateDlg{media=Media}=State,
    Side = case State of
               #?StateDlg{a=#side{callid=CallId}} -> a;
               #?StateDlg{b=#side{callid=CallId}} -> b
           end,
    case ?MEDIA_B2B:update_term_by_remote(Media, Side, RSdp) of
        {error, Reason} ->
            d(State, "MGC error: ~120p", [Reason]),
            {error, Reason};
        {ok, Media1} ->
            d(State, " -> modified local term"),
            State1 = State#?StateDlg{media=Media1},
            {ok, State1}
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

% @private
%% returns mg opts for call to search mg (mgsfx, mgaddr)
get_mg_opts(_State) -> get_mg_opts().
get_mg_opts() ->
    % 07.11.2016 Peter
    %  We should use bg-mg, containing same aliases as current esg.
    %  If no media should be used, then media operations filtered earlier.
    %  If no aliases used (only one interface on esg, then should use any mg on current address)
    case lists:usort(?U:get_aliases()) of
        [_|_]=Aliases ->
            % any bgmg on same aliases
            BAliasHash = ?EU:to_binary(erlang:phash2(Aliases)),
            [{mgsfx, <<"bg",BAliasHash/bitstring>>}];
        _ ->
            % any mg on current server's address
            [{mgsfx, <<"def">>},
             {mgaddr, ?U:get_host_interfaces_ipv4()}]
    end.

% ---
get_dialogid(#state_forking{dialogid=DialogId}=_StateData) -> DialogId;
get_dialogid(#?StateDlg{dialogid=DialogId}=_StateData) -> DialogId.

% ---
get_media(#state_forking{media=Media}=_State) -> Media;
get_media(#?StateDlg{media=Media}=_State) -> Media.


% @private ----
d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    DlgId = get_dialogid(State),
    ?LOGSIP("B2B. fsm ~p media:" ++ Fmt, [DlgId] ++ Args).
