%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc

-module(r_sip_esg_b2b_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([]).

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_esg.hrl").
-include("../include/r_sip_headers.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% =============================================
%% ASide
%% =============================================

build_aside(Req, State) ->
    #state_forking{map=#{opts:=DlgOpts}}=State,
    Now = os:timestamp(),
    {ok, AReqHandle} = nksip_request:get_handle(Req),
    #sdp{}=ARSdp=?U:extract_sdp(Req),
    #sipmsg{call_id=ACallId,
            vias=[#via{domain=ViaDomain,opts=ViaOpts}|_],
            from={ARUri,ARTag},
            to={#uri{ext_opts=ALExtOpts}=ALUri,_},
            contacts=ARContacts,
            to_tag_candidate=ToTagCandidate}=Req,
    %
    ALTag1 = ToTagCandidate,
    ALUri1 = ALUri#uri{ext_opts = ?U:store_value(<<"tag">>, ALTag1, ALExtOpts)},
    %
    ATransp = ?ESG_UTILS:get_transport_opts('a', DlgOpts),
    RemoteAddr = ?EU:get_by_key(<<"maddr">>,ViaOpts,ViaDomain),
    ALContact = ?U:build_local_contact(RemoteAddr,ALUri), % @localcontact (cfg? | ViaDomain! or maddr of Via)
    ALContact1 = ALContact#uri{opts=ATransp},
    #side{callid=ACallId,
          rhandle=AReqHandle,
          calldir=?DirInside,
          %
          remoteuri=ARUri,
          remotetag=ARTag,
          remotecontacts=ARContacts,
          remotesdp=ARSdp,
          %
          localuri=ALUri1,
          localtag=ALTag1,
          localcontact=ALContact1,
          %
          starttime=Now
         }.

%% =============================================
%% Outgoing CallId
%% =============================================

build_out_callid(DlgNum, CallIdx, _ACallId) ->
    %<<"rEB-", (?U:luid())/bitstring>>.
    Rnd = ?U:luid(),
    CurSrvCode = ?U:get_current_srv_textcode(),
    CallCode = ?U:build_textcode_by_index(CallIdx, 2),
    <<"rEB-", CurSrvCode/bitstring, "-", DlgNum/bitstring, "-", CallCode/bitstring, "-", Rnd/bitstring>>.

%% ----
%extract_callid(<<"rEB-", _:24/bitstring, "-", _:48/bitstring, "-", _:16/bitstring, "-", CallId/bitstring>>) -> extract_callid(CallId);
%extract_callid(CallId) -> CallId.

%% =============================================
%% Extracts prop from state
%% =============================================

extract_property(Key, Default, #state_forking{map=Map}=_State) ->
    [Value] = ?EU:extract_optional_default([{Key,Default}], maps:get(opts,Map)),
    Value;
extract_property(Key, Default, #?StateDlg{map=Map}=_State) ->
    [Value] = ?EU:extract_optional_default([{Key,Default}], maps:get(opts,Map)),
    Value.

%% =============================================
%% Local Tags
%% =============================================

build_tag() ->
    CurSrvCode = ?U:get_current_srv_textcode(),
    <<Tag0:48/bitstring, _/bitstring>> = ?U:luid(),
    % <<"rEB-", CurSrvCode/bitstring, "-", Tag0/bitstring>>.
    <<CurSrvCode/bitstring, "-", Tag0/bitstring>>.

%% =============================================
%% Log to callid-link log
%% =============================================

log_callid(b,DlgNum,ACallId,OutCallIdx,BCallId,Domain) ->
    ?BLlog:write({sip,callid}, {"dlg='~s'; acallid='~s'; idx=~p; bcallid='~s'; domain='~s'",[DlgNum,ACallId,OutCallIdx,BCallId,Domain]});
log_callid(referred,DlgNum,CallId,OutCallIdx,RCallId,Domain) ->
    ?BLlog:write({sip,callid}, {"dlg='~s'; callid='~s'; idx=~p; rfcallid='~s'; domain='~s'",[DlgNum,CallId,OutCallIdx,RCallId,Domain]}).
log_callid(replaces,DlgNum,CallId,RCallId,Domain) ->
    ?BLlog:write({sip,callid}, {"dlg='~s'; callid='~s'; rpcallid='~s'; domain='~s'",[DlgNum,CallId,RCallId,Domain]}).

%% =============================================
%% Log to normalization log
%% =============================================
%% @private RP-96
log_normalization(#uri{}=To, #uri{}=From, Rule, AMap) when is_map(Rule), is_map(AMap) ->
    IdP = maps:get(id, AMap),
    CodeP = maps:get(code, AMap),
    ?BLlog:write({sip,norm}, {"To='~120p';~n\tFrom='~120p';~n\tId='~s'; Code='~s';~n\tRule='~p'",[To, From, IdP, CodeP, Rule]}).

%% =============================================
%% Local Contact Transport
%% =============================================

get_transport_opts(Side, DlgOpts) ->
    [Dir,AMap] = ?EU:extract_required_props([dir,account], DlgOpts),
    case {Side, Dir} of
        {'a', 'outside'} -> ?CFG:get_transport_opts();
        {'a', 'inside'} ->  get_transport_opts(AMap);
        {'b', 'inside'} -> ?CFG:get_transport_opts();
        {'b', 'outside'} ->  get_transport_opts(AMap)
    end.
get_transport_opts(Map) ->
    case maps:get(transport,Map,udp) of
        udp -> [];
        tcp -> [{<<"transport">>, <<"tcp">>}];
        tls -> [{<<"transport">>, <<"tls">>}]
    end.

%% =============================================
%% Local Contact Transport
%% =============================================

build_auth_opts(?SideLocal, DlgOpts) when is_list(DlgOpts) ->
    [AMap] = ?EU:extract_required_props([account], DlgOpts),
    build_auth_opts_int(AMap);
build_auth_opts(?SideRemote, DlgOpts) when is_list(DlgOpts) ->
    [AMap] = ?EU:extract_required_props([account], DlgOpts),
    build_auth_opts_ext(AMap);
build_auth_opts(Side, DlgOpts) when is_list(DlgOpts) ->
    [Dir,AMap] = ?EU:extract_required_props([dir,account], DlgOpts),
    case {Side, Dir} of
        {'a', 'outside'} -> build_auth_opts_int(AMap);
        {'a', 'inside'} -> build_auth_opts_ext(AMap);
        {'b', 'inside'} -> build_auth_opts_int(AMap);
        {'b', 'outside'} -> build_auth_opts_ext(AMap)
    end.
build_auth_opts_ext(AMap) ->
    case ?EU:to_bool(maps:get(reg,AMap)) of
        false -> [];
        true ->
            LoginOpts = [{sip_pass, maps:get(pwd,AMap)}],
            case maps:get(login,AMap,<<>>) of
                <<>> -> LoginOpts;
                Login -> [{user, Login}|LoginOpts]
            end end.
build_auth_opts_int(AMap) ->
    ExtId = maps:get(id,AMap),
    ExtCode = maps:get(code,AMap),
    ExtU = maps:get(username,AMap),
    ExtD = maps:get(domain,AMap),
    CD = maps:get(clusterdomain,AMap),
    SrvIdxT = ?U:get_current_srv_textcode(),
    [{add, {?OwnerHeader, <<"rEG-", SrvIdxT/bitstring>>}},
     {add, {?ExtAccountHeader, <<ExtU/bitstring,"@",ExtD/bitstring,";id=",ExtId/bitstring,";code=",ExtCode/bitstring,";cd=",CD/bitstring>>}}].


%% =============================================
%% Modify number/domain/display by rule in settings (/X/7XX2*, XX/*/)
%% APPLY_MODIFIER PROXY (normalization provider_callerid rules)
%% =============================================

%% apply_modifier(_, undefined, Value) -> Value;
%% apply_modifier(Key, #{}=Map, Value) ->
%%     case maps:get(Key, Map, undefined) of
%%         undefined -> Value;
%%         Modifier -> apply_modifier(Modifier, Value)
%%     end.

%% @private
apply_modifier(Key, NRule, Value, AMap) when is_atom(Key) ->
    do_apply_modifier(maps:get(Key,NRule), Value, AMap, NRule);
apply_modifier(Modifier, NRule, Value, AMap) when is_binary(Modifier) ->
    do_apply_modifier(Modifier, Value, AMap, NRule).

%% @private
%% regex or text mask
%%  powered by r_env_modifier. To accelerate ~10% text modifier it could be used locally without mapping some fields
do_apply_modifier(Modifier, Value, AMap, NRule) when is_binary(Modifier) ->
    Fget = fun(Key) -> fun() -> maps:get(Key,AMap) end end,
    Flow = fun(Key) -> fun() -> string:to_lower(?EU:to_list(maps:get(Key,AMap))) end end,
    BraceOpts = [{$C,Fget(code)}, {$c,Flow(code)}, {$U,Fget(username)}, {$u,Flow(username)}, {$D,Fget(display)}, {$d,Flow(display)}],
    ?ENVMODIFIER:apply_modifier(Modifier, Value, BraceOpts, [x,asterisk,ebrace,regex,{rule,NRule}]). % no t because username and displayname could contain t symbol. also x.

%% =============================================
%% Builds from/to URI by account settings
%% =============================================

%% finds provider callerid normalization rule in domain center when call from inside (b2bua) to outer (provider)
find_inner_normalization_rule(FromUri, ToUri, AMap) ->
    find_normalization_rule('inner', FromUri, ToUri, AMap, 0).

%% build from uri when call from inner (b2bua) to outside (provider)
build_inner_from_uri(IsReinvite, FromUri, ToUri, AMap) ->
    NRule = find_inner_normalization_rule(FromUri, ToUri, AMap),
    build_inner_from_uri(IsReinvite, FromUri, ToUri, NRule, AMap).
build_inner_from_uri(IsReinvite, FromUri, ToUri, NRule, AMap) ->
    CD = maps:get(clusterdomain, AMap),
    % #177 usage of localdomain
    LocalDomain = case ?EU:to_bool(maps:get(reg, AMap)) of
                      true -> maps:get(domain, AMap);
                      false ->
                          case maps:get(localdomain, AMap, <<>>) of
                                <<>> -> maps:get(domain, AMap);
                                 D -> D
                             end end,
    FromUri1 = update_from_uri(IsReinvite, CD, FromUri, ToUri#uri{domain=CD}),
    case NRule of
        #{} ->
            #uri{disp=FDN,user=FUN,domain=_FDom}=FromUri1,
            Disp = apply_modifier('mod_fromdisplay', NRule, ?U:unquote_display(FDN), AMap),
            User = apply_modifier('mod_fromnumber', NRule, FUN, AMap),
            ResultFromUri = #uri{scheme=sip, disp=?U:quote_display(Disp), user=User, domain=LocalDomain}, % @todo esg from domain
            log_normalization(ToUri, ResultFromUri, NRule, AMap),
            ResultFromUri;
        _ -> FromUri1#uri{domain=LocalDomain} % @todo esg from domain
    end.

%% @private (modify reinvite's direct user to representative in current cluster)
%update_from_uri(false, _, FromUri, _ToUri) -> FromUri; % first call
update_from_uri(_, CD, #uri{disp=FromDisplay,user=U,domain=D}=FromUri, ToUri) -> % reinvite
   case ?ACCOUNTS:check_sipuser_exists(U, D) of
       false -> FromUri; % service or external
       true ->
           {ok,ResNum,ResDisp} = ?REPRESENTATIVE:get_user_phonenumber({sip,U,D}, {sip,ToUri#uri.user,ToUri#uri.domain}, FromDisplay),
           FromUri#uri{disp=ResDisp, user=ResNum, domain=CD}
           % do not modify display name by found account name (04.2017)
           % 24.01.2022 display could be modified by representative rule too
    end.

%% build to uri when call from inner (b2bua) to outside (provider)
build_inner_to_uri(_, _, ToUri, NRule, _) when not is_map(NRule) -> ToUri;
build_inner_to_uri(_IsReinvite, NewFromUri, ToUri, NRule, AMap) -> build_to_uri('inner', NewFromUri, ToUri, NRule, AMap).

%% ---------------------

%% finds provider callerid normalization rule in domain center
find_outer_normalization_rule(FromUri, ToUri, AMap) ->
    find_normalization_rule('outer', FromUri, ToUri, AMap, 0).

%% build from uri when call from outer (provider) to inside (b2bua)
build_outer_from_uri(IsReinvite, FromUri, ToUri, AMap) ->
    NRule = find_outer_normalization_rule(FromUri, ToUri, AMap),
    build_outer_from_uri(IsReinvite, FromUri, ToUri, NRule, AMap).
build_outer_from_uri(_IsReinvite, FromUri, ToUri, NRule, AMap) ->
    CD = maps:get(clusterdomain, AMap),
    case NRule of
        #{} ->
            #uri{disp=FDN,user=FUN,domain=_FDom}=FromUri,
            Disp = apply_modifier('mod_fromdisplay', NRule, ?U:unquote_display(FDN), AMap),
            User = apply_modifier('mod_fromnumber', NRule, FUN, AMap),
            ResultFromUri = #uri{scheme=sip, disp=?U:quote_display(Disp), user=User, domain=CD}, % domain=CD domain=maps:get(domain, AMap)  % @todo esg from domain
            log_normalization(ToUri, ResultFromUri, NRule, AMap),
            ResultFromUri;
        _ -> FromUri#uri{domain=CD} % @todo esg from domain
    end.

%% build to uri when call from outer (provider) to inside (b2bua)
build_outer_to_uri(_,_,ToUri,NRule,_) when not is_map(NRule) -> ToUri;
build_outer_to_uri(_IsReinvite, NewFromUri, ToUri, NRule, AMap) -> build_to_uri('outer', NewFromUri, ToUri, NRule, AMap).

%% ---------------------

%% @private both inner|outer
find_normalization_rule(Dir, FromUri, ToUri, AMap, StartFromPriority)
  when (Dir=='inner' orelse Dir=='outer') andalso is_integer(StartFromPriority) ->
    IdP = maps:get(id, AMap),
    CD = maps:get(clusterdomain, AMap),
    Ftrace = maps:get(normalization_trace, AMap,undefined), % RP-315
    ?DC:find_provider_normalization_rule(CD, IdP, Dir, ?U:make_aor(FromUri), ?U:make_aor(ToUri), StartFromPriority, Ftrace).

%% @private both inner|outer
build_to_uri(Dir,NewFromUri,ToUri,NRule,AMap) when is_map(NRule) ->
    #uri{user=TUN}=ToUri,
    case maps:get('mod_tonumber',NRule) of
        <<"priority=",X/bitstring>> -> % #192
            StartFromPriority = try ?EU:to_int(X) catch _:_ -> 0 end,
            case find_normalization_rule(Dir, NewFromUri, ToUri, AMap, StartFromPriority) of
                #{}=NewNRule ->
                    User = apply_modifier('mod_tonumber', NewNRule, TUN, AMap),
                    ResultToUri = ToUri#uri{user=User},
                    log_normalization(ResultToUri, NewFromUri, NewNRule, AMap),
                    ResultToUri;
                _ ->
                    ToUri
            end;
        ModTo ->
            User = apply_modifier(ModTo, NRule, TUN, AMap),
            ToUri#uri{user=User}
    end.

%% =============================================
%% Builds remote-party-id URI by response
%% =============================================

build_remote_party_id(IsReinvite, Resp, RUri, State) ->
    case
        case State of
            #?StateDlg{refer=#refer{ytype=?SideLocal}} -> fwd_to_user;
            #?StateDlg{refer=#refer{ytype=?SideRemote}} -> fwd_to_provider;
            #state_forking{map=#{dir:=?DirInside}} -> fwd_to_provider;
            #state_forking{map=#{dir:=?DirOutside}} -> fwd_to_user
        end
    of
        fwd_to_provider ->
            % build_inner_remote_party(RUri, State);
            case extract_rpid(Resp) of
                undefined -> build_inner_remote_party(IsReinvite, Resp, ?U:clear_uri(RUri), State);
                RpidH ->
                    case ?U:parse_uris(RpidH) of
                        [RpidUri|_] -> build_inner_remote_party(IsReinvite, Resp, RpidUri, State);
                        _ -> build_inner_remote_party(IsReinvite, Resp, RUri, State)
                    end end;
        fwd_to_user ->
            case extract_rpid(Resp) of
                undefined -> build_outer_remote_party(IsReinvite, Resp, ?U:clear_uri(RUri), State);
                RpidH ->
                    case ?U:parse_uris(RpidH) of
                        [RpidUri|_] -> build_outer_remote_party(IsReinvite, Resp, RpidUri, State); % RpidUri;
                        _ -> build_outer_remote_party(IsReinvite, Resp, RUri, State)
                    end end
    end.

%% @private
extract_rpid(Resp) ->
    case nksip_response:header(<<"remote-party-id">>, Resp) of
        {ok,[RpidH|_]} -> RpidH;
        _ ->
            case nksip_response:header(<<"p-asserted-identity">>, Resp) of
                {ok,[RpidH|_]} -> RpidH;
                _ -> undefined
            end end.

%% @private
build_inner_remote_party(IsReinvite, Resp, RUri, State) ->
    % ToUri = #uri{scheme=sip, user= <<>>, domain= <<>>}
    #sipmsg{to={ToUri,_}}=Resp,
    build_inner_from_uri(IsReinvite, RUri, ToUri, extract_account(State)).

%% @private
build_outer_remote_party(IsReinvite, Resp, RUri, State) ->
    % ToUri = #uri{scheme=sip, user= <<>>, domain= <<>>}
    #sipmsg{to={ToUri,_}}=Resp,
    build_outer_from_uri(IsReinvite, RUri, ToUri, extract_account(State)).

%% @private --
extract_account(#state_forking{map=Map}) -> maps:get(account,Map);
extract_account(#?StateDlg{map=Map}) -> maps:get(account,Map).

%%%
get_side_type('a',?DirOutside) -> inner;
get_side_type('a',?DirInside) -> outer;
get_side_type('b',?DirOutside) -> outer;
get_side_type('b',?DirInside) -> inner.

%% =============================================
%% Routines
%% =============================================

%% ---
send_bye(ZSide, _State) ->
    #side{dhandle=ZDlgHandle}=ZSide,
    Opts = [user_agent],
    catch nksip_uac:bye(ZDlgHandle, [async|Opts]).

%% ---
send_response(SipReply, Req, State) ->
    {ok,Handle} = nksip_request:get_handle(Req),
    ?LOG("EB2B fsm ~p '~p': send_response: ~1000tp", [reply_code(SipReply)]),
    DlgId = get_dialogid(State),
    ?U:send_sip_reply(fun() ->
                          Res = nksip_request:reply(SipReply, Handle),
                          ?LOG("EB2B fsm ~p: send_response result: ~1000tp", [DlgId,Res])
                      end).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -----
get_dialogid(#?StateDlg{dialogid=DlgId}) -> DlgId;
get_dialogid(#state_forking{dialogid=DlgId}) -> DlgId.

%% -----
reply_code({Code,_}) -> Code;
reply_code(SipReply) -> SipReply.
