%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Pavel Abramov, Peter Bukashin <tbotc@yandex.ru>
%%% @date 14.03.2019
%%% @doc RP-1181. Send event to CDR.

-module(r_sip_esg_eventing).
-author(['Pavel Abramov','Peter Bukashin <tbotc@yandex.ru>']).

-export([provider_state_changed/1]).

%% ===================================================================
%% Defines
%% ===================================================================

-include("r_sip_esg_eventing.hrl").

%% ===================================================================
%% API functions
%% ===================================================================

%% -----------------------------------------------
-spec provider_state_changed(map()) -> ok.
%% -----------------------------------------------
provider_state_changed(Map) when is_map(Map) ->
    Event = ?EVENTHANDLER:prepare_event(?Event_StateChanged, Map),
    ?LOGCDR("~120ts. provider_state_changed ~1000tp - ~1000tp", [maps:get(domain,Map), maps:get(regid,Map), maps:get(regstate,Map)]),
    Dests = make_destinations(Map),
    send_event(Event, Dests).

%% ===================================================================
%% Internal functions
%% ===================================================================

send_event({_Pattern, EventHeader, _From}=Event, Dests) ->
    Key = maps:get(<<"objid">>, maps:get(<<"data">>, EventHeader)),
    %send_event_worker(Event, Dests).
    ?WORKER:cast_workf(Key,fun() -> send_event_worker(Event, Dests) end).

%% @private
send_event_worker(Event, Dests) ->
    case is_cdr_allowed() of
        true -> ?ENVEVENTGATE:send_event(Event, Dests);
        false -> ok
    end.

is_cdr_allowed() -> true.
%is_cdr_allowed() -> ?ENVLIC:is_cdr_allowed().

%% @private
make_destinations(Map) ->
    {reg,_,Domain,Item} = maps:get(accountinfo, Map),
    Code = maps:get(code, Item),
    #{kafka => [], % RP-1853
      wssubscr => [{Domain, {provider,Code}}],
      sq => false % DEBUG 2021-11-25
     }.

