%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Pavel Abramov, Peter Bukashin <tbotc@yandex.ru>
%%% @date 14.03.2019
%%% @doc RP-1181. Make event body.

-module(r_sip_esg_eventing_handler).
-author(['Pavel Abramov','Peter Bukashin <tbotc@yandex.ru>']).

-export([prepare_event/2]).

%% ===================================================================
%% Defines
%% ===================================================================

-include("r_sip_esg_eventing.hrl").

%% ===================================================================
%% API functions
%% ===================================================================

%% -----------------------------------------------
-spec prepare_event(binary(), map()) -> tuple().
%% -----------------------------------------------
prepare_event(EventType, Map)
  when is_binary(EventType), is_map(Map) ->
    Event = make_event(EventType, Map),
    {reg,_,Domain,_} = maps:get(accountinfo, Map),
    prepare_event_header(EventType, Domain, Event).

%% ===================================================================
%% Internal functions
%% ===================================================================
make_event(EventType, Map) ->
    Fields = get_fields(EventType),
    F = fun(Field, Acc) ->
                Value = fill_field(EventType, Field, Map),
                append_field(EventType, Value, Acc)
        end,
    lists:foldl(F, #{}, Fields).

%% -----------------------------------------------
get_fields(?Event_StateChanged) ->
    [{<<"code">>, accountinfo, fun parse_amap/2, code},
     {<<"objid">>, accountinfo, fun parse_amap/2, id},
     {<<"state">>, regstate}].

%% -----------------------------------------------
fill_field(_, {Field, Key}, Map) -> {Field, maps:get(Key,Map)};
fill_field(_, {Field, Key1, Fun, Key2}, Map) when is_function(Fun) ->
    {Field, Fun(maps:get(Key1, Map), Key2)}.

%% -----------------------------------------------
append_field(_, {K,V}, Acc) -> maps:put(K, V, Acc).

%% -----------------------------------------------
prepare_event_header(EventType, Domain, Event) ->
    Pattern = {?ClassPattern, ?ClassType, Domain},
    EventHeader = #{<<"class">> => ?ClassPattern,
                    <<"type">> => EventType,
                    <<"data">> => Event,
                    <<"eventts">> => ?EU:timestamp()},
    From = self(),
    {Pattern, EventHeader, From}.

%% -----------------------------------------------
parse_amap({reg,_,_,AMap}, Key) -> maps:get(Key, AMap).

