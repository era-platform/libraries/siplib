%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc
%%% @todo

-module(r_sip_esg_fsm).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([init/1, terminate/3, code_change/4]).
-export([callback_mode/0, handle_event/4]). % gen_statem
-export([handle_event/3, handle_sync_event/4, handle_info/3]). % gen_fsm

-export([forking/2,
         dialog/2,
         sessionchanging/2,
         refering/2,
         replacing/2,
         migrating/2,
         stopping/2]).

-export([start/1,
         supv_start_link/1,
         stop/1, stop/2,
         stop_forcely/1,
         test/1,
         pull_fsm_by_callid/1,
         pull_fsm_by_dialogid/1]).

-export([sip_cancel/2,
         sip_ack/2,
         sip_bye/2,
         sip_reinvite/2,
         sip_replace_invite/2,
         sip_refer/2,
         sip_notify/2,
         sip_info/2,
         sip_message/2,
         %uac_pre_response/2,
         uac_response/2,
         uas_dialog_response/2,
         call_terminate/2]).

-export([apply_fun/2]). % #265

-export([get_tags/1,
         get_dialog_info/2,
         get_current_media_link/1,
         get_current_info/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_esg.hrl").
-include("../include/r_sip_mgc.hrl").

% -include("../include/r_nksip.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

start(Opts) ->
    {DialogId, D, DlgIdHash} = generate_ids(),
    Opts1 = [{dialogid,DialogId}, {dlgnum,D}, {dlgidhash,DlgIdHash} | Opts],
    ChildSpec = {DialogId, {?MODULE, supv_start_link, [Opts1]}, temporary, 1000, worker, [?MODULE]},
    ?ESG_SUPV:start_child(ChildSpec).

supv_start_link(Opts) ->
    start_link(?MODULE, Opts, []).

% --
stop(DlgX) ->
    stop(DlgX, "External").

stop({_DlgId,Pid}, Reason) when is_pid(Pid) ->
    send_event(Pid, {stop_external, Reason});
stop(Pid, Reason) when is_pid(Pid) ->
    send_event(Pid, {stop_external, Reason});
stop(DlgId, Reason) when is_binary(DlgId) ->
    to_fsm(DlgId, fun(Pid) -> send_event(Pid, {stop_external, Reason}) end).

% --
stop_forcely({_DlgId,Pid}) when is_pid(Pid) ->
    send_all_state_event(Pid, {stop_forcely});
stop_forcely(DlgId) when is_binary(DlgId) ->
    to_fsm(DlgId, fun(Pid) -> send_all_state_event(Pid, {stop_forcely}) end).

% --
pull_fsm_by_callid(CallId) ->
    ?DLG_STORE:pull_dlg(CallId).

% --
pull_fsm_by_dialogid(DlgId) when is_binary(DlgId) ->
    case ?DLG_STORE:find_t(DlgId) of
        false -> false;
        {_,#{link:=Dlg}} -> Dlg
    end.

% --
test({_DlgId,Pid})
  when is_pid(Pid) ->
    send_all_state_event(Pid, {test}).

% --------------------

sip_cancel({_DlgId,Pid},Args)
  when is_pid(Pid) ->
    send_event(Pid, {sip_cancel,Args}).

sip_ack({_DlgId,Pid},Args)
  when is_pid(Pid) ->
    send_all_state_event(Pid, {sip_ack,Args}).

sip_bye({_DlgId,Pid},Args)
  when is_pid(Pid) ->
    send_event(Pid, {sip_bye,Args}).

sip_reinvite({_DlgId,Pid},Args)
  when is_pid(Pid) ->
    send_event(Pid, {sip_reinvite,Args}).

sip_replace_invite({_DlgId,Pid},Args)
  when is_pid(Pid) ->
    send_event(Pid, {sip_replace_invite,Args}).

sip_refer({_DlgId,Pid},Args)
  when is_pid(Pid) ->
    send_event(Pid, {sip_refer,Args}).

sip_notify({_DlgId,Pid},Args)
  when is_pid(Pid) ->
    send_all_state_event(Pid, {sip_notify,Args}).

sip_info({_DlgId,Pid},Args)
  when is_pid(Pid) ->
    send_all_state_event(Pid, {sip_info,Args}).

sip_message({_DlgId,Pid},Args)
  when is_pid(Pid) ->
    send_all_state_event(Pid, {sip_message,Args}).

%% uac_pre_response({_DlgId,Pid},[Resp,_UAC,_Call]=Args)
%%   when is_pid(Pid) ->
%%     send_event(Pid, {uac_response, [Resp]}),
%%     {continue,Args}.

uac_response({_DlgId,Pid}, [_Resp]=Args)
  when is_pid(Pid) ->
    send_event(Pid, {uac_response, Args}),
    continue.

uas_dialog_response({_DlgId,Pid}, [_Resp]=Args)
  when is_pid(Pid) ->
    send_event(Pid, {uas_dialog_response, Args}),
    continue.

call_terminate({_DlgId,Pid},Args)
  when is_pid(Pid) ->
    send_event(Pid, {call_terminate,Args}).

% --------------------------------

% #265
apply_fun({_DlgId,Pid},Args)
  when is_pid(Pid) ->
    send_all_state_event(Pid, {apply_fun,Args}).

% --------------------------------

get_tags({_DlgId,Pid})
  when is_pid(Pid) ->
    sync_send_all_state_event(Pid, {get_tags}).

get_dialog_info({_DlgId,Pid}, _Args) ->
    sync_send_all_state_event(Pid, {get_store_data}).

get_current_media_link({_DlgId,Pid}) when is_pid(Pid) ->
    sync_send_all_state_event(Pid, {get_current_media_link}).

get_current_info({_DlgId,Pid}) when is_pid(Pid) ->
    sync_send_all_state_event(Pid, {get_current_info, activesides});

get_current_info(DlgId) when is_binary(DlgId) ->
    case pull_fsm_by_dialogid(DlgId) of
        false -> false;
        Dlg -> get_current_info(Dlg)
    end.

%% ====================================================================
%% gen_fsm => gen_statem translation functions
%% ====================================================================

% --------------------
% gen_fsm calls
% --------------------
start_link(Module,Args,Opts) ->
    ?FSMT:start_link(Module,Args,Opts).

send_event(FsmRef,Event) ->
    ?FSMT:send_event(FsmRef,Event).

%%sync_send_event(FsmRef,Event) ->
%%    ?FSMT:sync_send_event(FsmRef,Event).

send_all_state_event(FsmRef,Event) ->
    ?FSMT:send_all_state_event(FsmRef,Event).

sync_send_all_state_event(FsmRef,Event) ->
    ?FSMT:sync_send_all_state_event(FsmRef,Event).

% --------------------
% gen_statem callbacks
% --------------------
callback_mode() ->
    ?FSMT:callback_mode(?MODULE).

handle_event(EventType, EventContent, StateName, StateData) ->
    case catch ?FSMT:handle_event(?MODULE,EventType,EventContent,StateName,StateData) of
        {'EXIT',_}=E -> ?OUT("Error: ~120p", [E]), stop;
        T -> T
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

init(Opts) ->
    self() ! 'do_init',
    [DialogId,Req] = ?EU:extract_required_props(['dialogid','req'], Opts),
    #sipmsg{call_id=ACallId}=Req,
    ?LOG("EB2B fsm ~p '~p': ~120p", [DialogId,'initial',ACallId]),
    {ok, 'initial', {'initial',Opts}}.

init_internal(Opts) ->
    Self = self(),
    [App,DialogId,DlgNum,DlgIdHash,Req,Dir,AMap] = ?EU:extract_required_props([app,dialogid,dlgnum,dlgidhash,req,dir,account], Opts),
    [UseMedia, ReplOpts] = ?EU:extract_optional_default([{use_media,true}, {replaces,undefined}], Opts),
    #sipmsg{call_id=ACallId}=Req,
    %
    ExtAccCode = maps:get(code,AMap),
    StoreData = #{pid => Self,
                  timestart => erlang:time(),
                  link => {DialogId,Self},
                  callids => [ACallId], % used only for log
                  dir => Dir,
                  extacccode => ExtAccCode, % used to get account code while routing by rules in b2bua
                  fun_cleanup => fun() -> ok end,
                  supv => ?ESG_SUPV},
    %
    StateData = #state_forking{dialogid = DialogId,
                               dialogidhash = DlgIdHash, % #370
                               map = #{app => App,
                                       dlgnum => DlgNum,
                                       opts => Opts,
                                       init_req => Req,
                                       dir => Dir, % ?DirInside|?DirOutside
                                       account => AMap,
                                       replace_opts => ReplOpts, % #115
                                       store_data => StoreData,
                                       last_out_callid_idx => 0,
                                       last_response => undefined,
                                       last_response_full => undefined,
                                       applied_redirects => []},
                               use_media=UseMedia,
                               acallid = ACallId},
    d(StateData, 'init', "srv initing. ACallId=~120p",[ACallId]),
    % to store
    ?DLG_STORE:store_t(DialogId, StoreData, ?STORE_TIMEOUT),
    erlang:send_after(?STORE_REFRESH, Self, {refresh_at_store}),
    ?DLG_STORE:push_dlg(ACallId, {DialogId,Self}),
    % Monitor
    % we can't apriory setup clear usr states clear, because of fork-dependant domains. It would be done by state collector's monitor.
    % we can't also setup clear media, it starts later, it can migrate.
    Flog = fun(StopReason) -> ?OUT("Process ~p terminated (dlg=~p): ~120p", [Self, DlgNum, StopReason]) end,
    Fclear = fun() -> ?DLG_STORE:unlink_dlg(ACallId),
                      ?DLG_STORE:delete_t(DialogId) end,
    ?MONITOR:start_monitor(Self, "Sip esg dlg", [Flog, Fclear]),
    %
    d(StateData, 'init', "srv start forking. ACallId=~120p",[ACallId]),
    StateData1 = ?FORKING:init(StateData),
    d(StateData1, 'init', "srv inited. ACallId=~120p",[ACallId]),
    {ok, ?FORKING_STATE, StateData1}.

% -------------------------
% 'forking' events
% -------------------------
forking({sip_cancel, Args}=_Event, StateData) ->
    ?FORKING:sip_cancel(Args, StateData);

forking({sip_bye, Args}=_Event, StateData) ->
    ?FORKING:sip_bye(Args, StateData);

% rfc-3311 update or caching
forking({sip_replace_invite, Args}=_Event, StateData) ->
    ?FORKING:sip_replace_invite(Args, StateData);

forking({uac_response,Args}, StateData) ->
    ?FORKING:uac_response(Args, StateData);

forking({uas_dialog_response,Args}, StateData) ->
    ?FORKING:uas_dialog_response(Args, StateData);

forking({call_terminate, Args}=_Event, StateData) ->
    ?FORKING:call_terminate(Args, StateData);

forking({stop_external, Reason}, StateData) ->
    ?FORKING:stop_external(Reason, StateData);

forking({test}, StateData) ->
    ?FORKING:test(StateData).

% ------------------------
% 'dialog' events
% ------------------------
dialog({sip_cancel, [_CallId, Req, _InviteReq]}=_Event, StateData) ->
    send_response({ok,[]}, Req, {?DIALOG_STATE, StateData});

dialog({sip_bye, Args}=_Event, StateData) ->
    ?DIALOG:sip_bye(Args, StateData);

dialog({sip_reinvite, Args}=_Event, StateData) ->
    ?SESSIONCHANGING:enter_by_reinvite(Args, StateData);

dialog({sip_replace_invite, Args}=_Event, StateData) ->
    ?REPLACING:enter_by_invite(Args, StateData);

dialog({sip_refer, Args}=_Event, StateData) ->
    ?REFERING:enter_by_refer(Args, StateData);

dialog({uac_response,Args}, StateData) ->
    ?DIALOG:uac_response(Args, StateData);

dialog({uas_dialog_response,Args}, StateData) ->
    ?DIALOG:uas_dialog_response(Args, StateData);

dialog({call_terminate, Args}=_Event, StateData) ->
    ?DIALOG:call_terminate(Args, StateData);

dialog({stop_external, Reason}, StateData) ->
    ?DIALOG:stop_external(Reason, StateData);

dialog({test}, StateData) ->
    ?DIALOG:test(StateData).

% ------------------------
% 'sessionchanging' events
% ------------------------
sessionchanging({sip_bye, Args}=_Event, StateData) ->
    ?SESSIONCHANGING:sip_bye(Args, StateData);

sessionchanging({sip_reinvite, [_CallId, Req]=Args}=_Event, StateData) ->
    StateData1 = ?SESSIONCHANGING:pending_reinvite(Args, StateData),
    send_response(?Pending("EB2B. Session changing."), Req, {?SESSIONCHANGING_STATE, StateData1});

sessionchanging({sip_replace_invite, [_CallId, Req|_]}=_Event, StateData) ->
    send_response(?Pending("EB2B. Session changing."), Req, {?SESSIONCHANGING_STATE, StateData});

sessionchanging({sip_refer, [_CallId, Req]}=_Event, StateData) ->
    send_response(?Pending("EB2B. Session changing."), Req, {?SESSIONCHANGING_STATE, StateData});

sessionchanging({uac_response,Args}, StateData) ->
    ?SESSIONCHANGING:uac_response(Args, StateData);

sessionchanging({uas_dialog_response,Args}, StateData) ->
    ?SESSIONCHANGING:uas_dialog_response(Args, StateData);

sessionchanging({call_terminate, Args}=_Event, StateData) ->
    ?SESSIONCHANGING:call_terminate(Args, StateData);

sessionchanging({stop_external, Reason}, StateData) ->
    ?SESSIONCHANGING:stop_external(Reason, StateData);

sessionchanging({test}, StateData) ->
    ?SESSIONCHANGING:test(StateData).

% ------------------------
% 'sessionchanging' events
% ------------------------
migrating({sip_bye, Args}=_Event, StateData) ->
    ?MIGRATING:sip_bye(Args, StateData);

migrating({sip_reinvite, Args}=_Event, StateData) ->
    ?MIGRATING:sip_reinvite(Args, StateData);

migrating({sip_replace_invite, [_CallId, Req|_]}=_Event, StateData) ->
    send_response(?Pending("EB2B. Migrating"), Req, {?MIGRATING_STATE, StateData});

migrating({sip_refer, [_CallId, Req]}=_Event, StateData) ->
    send_response(?Pending("EB2B. Migrating"), Req, {?MIGRATING_STATE, StateData});

migrating({uac_response,Args}, StateData) ->
    ?MIGRATING:uac_response(Args, StateData);

migrating({uas_dialog_response,Args}, StateData) ->
    ?MIGRATING:uas_dialog_response(Args, StateData);

migrating({call_terminate, Args}=_Event, StateData) ->
    ?MIGRATING:call_terminate(Args, StateData);

migrating({stop_external, Reason}, StateData) ->
    ?MIGRATING:stop_external(Reason, StateData);

migrating({test}, StateData) ->
    ?MIGRATING:test(StateData).

% ------------------------
% 'refering' events
% ------------------------
refering({sip_bye, Args}=_Event, StateData) ->
    ?REFERING:sip_bye(Args, StateData);

refering({sip_reinvite, [_CallId, _Req]=_Args}=_Event, StateData) ->
    ?REFERING:reinvite_while_transfering(_Args, StateData);

refering({sip_replace_invite, [_CallId, Req|_]}=_Event, StateData) ->
    send_response(?Pending("EB2B. Refering"), Req, {?REFERING_STATE, StateData});

refering({sip_refer, [_CallId, _Req]=_Args}=_Event, StateData) ->
    ?REFERING:refer_while_transfering(_Args, StateData);

refering({uac_response,Args}, StateData) ->
    ?REFERING:uac_response(Args, StateData);

refering({uas_dialog_response,Args}, StateData) ->
    ?REFERING:uas_dialog_response(Args, StateData);

refering({call_terminate, Args}=_Event, StateData) ->
    ?REFERING:call_terminate(Args, StateData);

refering({stop_external, Reason}, StateData) ->
    ?REFERING:stop_external(Reason, StateData);

refering({test}, StateData) ->
    ?REFERING:test(StateData).

% ------------------------
% 'replacing' events
% ------------------------
replacing({sip_bye, Args}=_Event, StateData) ->
    ?REPLACING:sip_bye(Args, StateData);

replacing({sip_reinvite, [_CallId, Req]=_Args}=_Event, StateData) ->
    send_response(?Pending("EB2B. Replacing"), Req, {?REPLACING_STATE, StateData});

replacing({sip_replace_invite, [_CallId, Req|_]}=_Event, StateData) ->
    send_response(?Pending("EB2B. Replacing"), Req, {?REPLACING_STATE, StateData});

replacing({sip_refer, [_CallId, Req]=_Args}=_Event, StateData) ->
    send_response(?Pending("EB2B. Replacing"), Req, {?REPLACING_STATE, StateData});

replacing({uac_response,Args}, StateData) ->
    ?REPLACING:uac_response(Args, StateData);

replacing({uas_dialog_response,Args}, StateData) ->
    ?REPLACING:uas_dialog_response(Args, StateData);

replacing({call_terminate, Args}=_Event, StateData) ->
    ?REPLACING:call_terminate(Args, StateData);

replacing({stop_external, Reason}, StateData) ->
    ?REFERING:stop_external(Reason, StateData);

replacing({test}, StateData) ->
    ?REPLACING:test(StateData).

% ------------------------
% 'stopping' events
% ------------------------
stopping({sip_bye, [_CallId, Req]}=_Event, StateData) ->
    {ok,ReqHandle} = nksip_request:get_handle(Req),
    ?U:send_sip_reply(fun() -> nksip_request:reply(accepted, ReqHandle) end),
    {next_state, ?STOPPING_STATE, StateData};

stopping({sip_reinvite, [_CallId, Req]}=_Event, StateData) ->
    {ok,ReqHandle} = nksip_request:get_handle(Req),
    ?U:send_sip_reply(fun() -> nksip_request:reply(?TemporarilyUnavailable("EB2B. Stopping"), ReqHandle) end),
    {next_state, ?STOPPING_STATE, StateData};

stopping({sip_replace_invite, [_CallId, Req|_]}=_Event, StateData) ->
    {ok,ReqHandle} = nksip_request:get_handle(Req),
    ?U:send_sip_reply(fun() -> nksip_request:reply(?TemporarilyUnavailable("EB2B. Stopping"), ReqHandle) end),
    {next_state, ?STOPPING_STATE, StateData};

stopping({sip_refer, [_CallId, Req]}=_Event, StateData) ->
    {ok,ReqHandle} = nksip_request:get_handle(Req),
    ?U:send_sip_reply(fun() -> nksip_request:reply(?TemporarilyUnavailable("EB2B. Stopping"), ReqHandle) end),
    {next_state, ?STOPPING_STATE, StateData};

stopping(Event, StateData) ->
    E = case is_tuple(Event) of
            true -> erlang:element(1, Event);
            false -> Event
        end,
    d(StateData, ?STOPPING_STATE, "event ~120p", [E]),
    {next_state, ?STOPPING_STATE, StateData}.

% ------------------------
% All states events
% ------------------------

handle_event({sip_ack, [_CallId,_Req]=Args}=_Event, ?DIALOG_STATE, StateData) ->
    ?DIALOG:sip_ack(Args, StateData);
handle_event({sip_ack, [_CallId,_Req]=_Args}=_Event, StateName, StateData) ->
    {next_state, StateName, StateData};

handle_event({sip_notify, [_CallId,Req]=Args}, StateName, StateData) ->
    case StateName of
        ?STOPPING_STATE -> send_response({forbidden}, Req, {StateName, StateData});
          ?FORKING_STATE -> send_response({forbidden}, Req, {StateName, StateData});
        _ ->
            StateData1 = ?DIALOG:sip_notify(Args, StateName, StateData),
            {next_state, StateName, StateData1}
    end;

handle_event({sip_info, [_CallId,Req]=Args}=_Event, StateName, StateData) ->
     case StateName of
        ?STOPPING_STATE -> send_response({forbidden}, Req, {StateName, StateData});
          ?FORKING_STATE -> send_response({forbidden}, Req, {StateName, StateData});
        _ ->
            StateData1 = ?DIALOG:sip_info(Args, StateName, StateData),
            {next_state, StateName, StateData1}
    end;

handle_event({sip_message, [_CallId,Req]=Args}, StateName, StateData) ->
    case StateName of
        ?STOPPING_STATE -> send_response({forbidden}, Req, {StateName, StateData});
          ?FORKING_STATE -> send_response({forbidden}, Req, {StateName, StateData});
        _ ->
            StateData1 = ?DIALOG:sip_message(Args, StateName, StateData),
            {next_state, StateName, StateData1}
    end;

% #265
handle_event({apply_fun, [F]}, StateName, StateData) when is_function(F,2) ->
    F(StateName,StateData);
handle_event({apply_fun, [F]}, _StateName, StateData) when is_function(F,1) ->
    F(StateData);

%
handle_event({stop_forcely}, _StateName, StateData) ->
    {stop, normal, StateData};

%
handle_event(Event, StateName, StateData) ->
    E = case is_tuple(Event) of
            true -> erlang:element(1, Event);
            false -> Event
        end,
    d(StateData, StateName, "handle_event ~120p", [E]),
    {next_state, StateName, StateData}.

% -------------------------
% 'forking' requests
% 'dialog' requests
% 'sessionchanging' requests
% 'refering' requests
% 'replacing' requests
% 'stopping' requests
% -------------------------

% -------------------------
% All states requests
% -------------------------
handle_sync_event({get_tags}, _From, StateName, StateData) ->
    Reply = case StateData of
                #?StateDlg{a=#side{callid=ACallId,localtag=ALTag,remotetag=ARTag},
                           b=#side{callid=BCallId,localtag=BLTag,remotetag=BRTag}} ->
                    #{state => 'dialog',
                      a => #{callid => ACallId, ltag => ALTag, rtag => ARTag},
                      b => #{callid => BCallId, ltag => BLTag, rtag => BRTag}};
                 #state_forking{a=#side{callid=ACallId,localtag=ALTag,remotetag=ARTag},
                               b_act=BAct} ->
                    #{state => 'forking',
                      a => #{callid => ACallId, ltag => ALTag, rtag => ARTag},
                      bs => lists:map(fun(#side_fork{callid=BCallId,localtag=BLTag,remotetag=BRTag}) ->
                                              #{callid => BCallId, ltag => BLTag, rtag => BRTag}
                                      end, BAct)};
                _T ->
                    {error, "Not normal state"}
            end,
    {reply, Reply, StateName, StateData};

handle_sync_event({get_store_data}, _From, StateName, StateData) ->
    Reply = get_info_storedata(StateData),
    {reply, Reply, StateName, StateData};

handle_sync_event({get_current_media_link}, _From, StateName, StateData) ->
    Reply = ?ESG_MEDIA:get_current_media_link(StateData),
    {reply, Reply, StateName, StateData};

handle_sync_event({get_current_info, X}, _From, StateName, StateData) ->
    Reply = get_current_info(X, StateData),
    {reply, Reply, StateName, StateData};

handle_sync_event(Event, _From, StateName, StateData) ->
    E = case is_tuple(Event) of
            true -> erlang:element(1, Event);
            false -> Event
        end,
    d(StateData, StateName, "handle_sync_event ~120p", [E]),
    {reply, b2bua_sync_event_default, StateName, StateData}.


% -------------------------
% Process messages
% -------------------------
%%
handle_info('do_init', 'initial', {'initial',Opts}) ->
    {ok,StateName,StateData} = init_internal(Opts),
    {next_state,StateName,StateData};

%%
handle_info({refresh_at_store}, StateName, StateData) ->
    d(StateData, StateName, "refresh_at_store"),
    ?DLG_STORE:store_t(get_dialogid(StateData), get_storedata(StateData), ?STORE_TIMEOUT),
    erlang:send_after(?STORE_REFRESH, self(), {refresh_at_store}),
    {next_state, StateName, StateData};

handle_info({start, Ref}, ?FORKING_STATE, #state_forking{ref=Ref}=StateData) ->
    ?FORKING:start(StateData);

handle_info({fork_timeout, Args}, ?FORKING_STATE, StateData) ->
    ?FORKING:fork_timeout(Args, StateData);

handle_info({total_timeout}, ?FORKING_STATE, StateData) ->
    ?FORKING:total_timeout(StateData);

handle_info({reinvite_timeout, Args}, ?SESSIONCHANGING_STATE, StateData) ->
    ?SESSIONCHANGING:reinvite_timeout(Args, StateData);

handle_info({refer_timeout, Args}, ?REFERING_STATE, StateData) ->
    ?REFERING:refer_timeout(Args, StateData);

handle_info({replace_timeout, Args}, ?REPLACING_STATE, StateData) ->
    ?REPLACING:replace_timeout(Args, StateData);

handle_info({referred_dialog_timeout}, StateName, StateData) ->
    d(StateData, StateName, "referred_dialog_timeout"),
    ?MODULE:stop(self(), "Referred timeout"),
    {next_state, StateName, StateData};

handle_info({dialog_timeout}, StateName, StateData) ->
    d(StateData, StateName, "dialog_timeout"),
    ?MODULE:stop(self(), "Dialog timeout"),
    {next_state, StateName, StateData};

handle_info({stopping_timeout}, ?STOPPING_STATE, StateData) ->
    final(StateData),
    {stop, normal, StateData};

handle_info({migration_timeout, Args}, ?MIGRATING_STATE, StateData) ->
    ?MIGRATING:migration_timeout(Args, StateData);

% #347
handle_info({mg_dtmf, Args}, StateName, StateData)
  when StateName==?DIALOG_STATE; StateName==?MIGRATING_STATE; StateName==?REFERING_STATE ->
    ?DIALOG:mg_dtmf(Args, StateName, StateData);
handle_info({mg_dtmf, _Args}, StateName, StateData) ->
    {next_state, StateName, StateData};

handle_info({mg_trunknovoice, _Args}, StateName, StateData) ->
    {next_state, StateName, StateData};

% ----------
% ----------
handle_info({mg_disconnected, Args}, StateName, StateData) ->
    %self() ! {mg_migrate_priv, Args, 0, 64},
    erlang:send_after(?EU:random(200), self(), {mg_migrate_priv, Args, 0, 64}),
    {next_state, StateName, StateData};

% -
handle_info({mg_migrate, Args}, StateName, StateData) ->
    %self() ! {mg_migrate_priv, Args, 0, 64},
    erlang:send_after(?EU:random(200), self(), {mg_migrate_priv, Args, 0, 64}),
    {next_state, StateName, StateData};

% -
handle_info({mg_migrate_priv,Args,I,T}, StateName, StateData) ->
    d(StateData, StateName, "MG found disconnected priv"),
    case ?ESG_MEDIA:media_check_mg(StateData, Args) of
        false -> ok;
        true -> case ?ESG_MEDIA:check_available_slots(StateData) of
                    {ok,_MgCnt,CtxCnt} when CtxCnt > 100 -> self() ! {change_mg};
                    _ -> case I > 11 of % 64 + 128 + ... + 1024 = 8 sec
                             false ->
                                 P = {mg_migrate_priv, Args, I+1, case T>=1000 of true->T;false->T*2 end},
                                 erlang:send_after(T, self(), P);
                             true -> stop(self(), "MGC/MG migration timeout")
                         end
                end
    end,
    {next_state, StateName, StateData};

% -
handle_info({change_mg}, ?FORKING_STATE, StateData) ->
    ?FORKING:stop_external("MG change on forking", StateData);
handle_info({change_mg}, ?STOPPING_STATE, StateData) ->
    {next_state, ?STOPPING_STATE, StateData};
handle_info({change_mg}, ?DIALOG_STATE, StateData) ->
    ?MIGRATING:start(StateData);
handle_info({change_mg}, StateName, #?StateDlg{map=#{timer_migration_ref:=undefined}=Map}=StateData) ->
    Map1 = Map#{timer_migration_ref := erlang:send_after(100, self(), {change_mg_timer})},
    {next_state, StateName, StateData#?StateDlg{map=Map1}};
handle_info({change_mg}, StateName, StateData) ->
    {next_state, StateName, StateData};

% -
handle_info({change_mg_timer}, StateName, #?StateDlg{map=#{timer_migration_ref:=undefined}}=StateData) ->
    {next_state, StateName, StateData};
handle_info({change_mg_timer}, StateName, #?StateDlg{map=Map}=StateData) ->
    self() ! {change_mg},
    Map1 = Map#{timer_migration_ref := undefined},
    {next_state, StateName, StateData#?StateDlg{map=Map1}};

% -----

handle_info(Event, StateName, StateData) ->
    E = case is_tuple(Event) of
            true -> erlang:element(1, Event);
            false -> Event
        end,
    d(StateData, StateName, "handle_info ~120p", [E]),
    {next_state, StateName, StateData}.


% -------------------------
%
% -------------------------
terminate(_Reason, _StateName, _StateData) ->
    ok.

code_change(_OldVsn, _StateName, _StateData, _Extra) ->
    ok.

%% ====================================================================
%% Internal functions
%% ====================================================================

% ---
get_dialogid(#state_forking{dialogid=DialogId}=_StateData) -> DialogId;
get_dialogid(#?StateDlg{dialogid=DialogId}=_StateData) -> DialogId.

% ---
get_exitfuns(#state_forking{exitfuns=ExitFuns}=_StateData) -> ExitFuns;
get_exitfuns(#?StateDlg{exitfuns=ExitFuns}=_StateData) -> ExitFuns.


% ---
get_storedata(#state_forking{map=#{store_data:=StoreData}}=_StateData) -> StoreData;
get_storedata(#?StateDlg{map=#{store_data:=StoreData}}=_StateData) -> StoreData.

% ---
get_info_storedata(#state_forking{map=#{account:=AMap,store_data:=StoreData},a=A,b_act=BAct}=_StateData) -> [{store_data,StoreData},{account,AMap},{aside,[A]},{bside,[]},{bside_fork,BAct}];
get_info_storedata(#?StateDlg{map=#{account:=AMap,store_data:=StoreData},a=A,b=B}=_StateData) -> [{store_data,StoreData},{account,AMap},{aside,[A]},{bside,[B]},{bside_fork,[]}].

% ---
get_current_info(activesides, #?StateDlg{}=State) ->
    #?StateDlg{a=#side{callid=ACallId,remotetag=ARTag,localtag=ALTag,remoteuri=ARUri,localuri=ALUri},
               b=#side{callid=BCallId,remotetag=BRTag,localtag=BLTag,remoteuri=BRUri,localuri=BLUri}}=State,
    {ok,#{state=>dialog,
          acallid=>ACallId,artag=>ARTag,altag=>ALTag,aruri=>ARUri,aluri=>ALUri,
          bcallid=>BCallId,brtag=>BRTag,bltag=>BLTag,bruri=>BRUri,bluri=>BLUri}};
get_current_info(activesides, #state_forking{b_act=BAct}=State) ->
    #state_forking{a=#side{callid=ACallId,remotetag=ARTag,localtag=ALTag,remoteuri=ARUri,localuri=ALUri}}=State,
    {ok,#{state=>forking,
          acallid=>ACallId,artag=>ARTag,altag=>ALTag,aruri=>ARUri,aluri=>ALUri,
          b=>lists:map(fun(#side_fork{callid=BCallId,remotetag=BRTag,localtag=BLTag,remoteuri=BRUri,localuri=BLUri}) ->
                               #{bcallid=>BCallId,brtag=>BRTag,bltag=>BLTag,bruri=>BRUri,bluri=>BLUri}
                       end, BAct)}}.

% ---
generate_ids() ->
    <<B6:48/bitstring, _Rest/bitstring>> = ?U:luid(),
    SrvCode = ?U:get_current_srv_textcode(),
    DlgId = <<"rDlg-", SrvCode/bitstring, "-", B6/bitstring>>,
    case ?DLG_STORE:find_t(DlgId) of
        false -> {DlgId, B6, ?U:int64_hash(<<SrvCode/binary,B6/binary>>)}; % #370
        _ -> generate_ids()
    end.

% ---
send_response(SipReply, Req, {StateName, StateData}) ->
    {ok,ReqHandle} = nksip_request:get_handle(Req),
    d(StateData, "send_response: ~1000tp", [reply_code(SipReply)]),
    DlgId = get_dialogid(StateData),
    ?U:send_sip_reply(fun() ->
                          Res = nksip_request:reply(SipReply, ReqHandle),
                          ?LOG("EB2B fsm ~p '~p': send_response result: ~1000tp", [DlgId,StateName,Res])
                      end),
    {next_state, StateName, StateData}.

% ---
to_fsm(DlgId, Fun) ->
    case ?DLG_STORE:find_t(DlgId) of
        false -> {error, not_found};
        {_,Pid} -> Fun(Pid)
    end.

% ---
final(StateData) ->
    d(StateData, ?STOPPING_STATE, "final"),
    DlgId = get_dialogid(StateData),
    {ok,_} = ?ESG_MEDIA:stop(StateData), % media
    ?DLG_STORE:delete_t(DlgId), % store
    ?MONITOR:stop_monitor(self()), % monitor
    ExitFuns = get_exitfuns(StateData),
    lists:foreach(fun(F) when is_function(F) -> F() end, ExitFuns),
    ?ESG_SUPV:drop_child(DlgId). % supv

%% -----
d(StateData, StateName, Text) -> d(StateData, StateName, Text, []).
d(StateData, StateName, Fmt, Args) ->
    ?LOG("EB2B fsm ~p '~p':" ++ Fmt, [get_dialogid(StateData),StateName] ++ Args).

%% -----
reply_code({Code,_}) -> Code;
reply_code(SipReply) -> SipReply.