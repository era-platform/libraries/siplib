%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Presents 'dialog' state of B2BUA Dialog FSM.
%%%      Also handle some events of dialog in other states (sip_info, sip_notify, sip_bye..)
%%%        It's most common state, sometimes switching to other states to handle some events.
%%%        Ends with bye.

-module(r_sip_esg_fsm_dialog).
-author('Peter Bukashin <tbotc@yandex.ru>').


-export([start/1,
         finish/1,
         stop_external/2,
         stop_error/2,
         test/1,
         sip_ack/2,
         sip_bye/2,
         sip_notify/3,
         sip_info/3,
         sip_message/3,
         uac_response/2,
         uas_dialog_response/2,
         call_terminate/2,
         mg_dtmf/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_esg.hrl").
-include("../include/r_sip_mgc.hrl").

% -include("../include/r_nksip.hrl").

-define(CURRSTATE, 'dialog').

%% ====================================================================
%% API functions
%% ====================================================================

% ----
start(State) ->
    State1 = State#?StateDlg{timer_ref=erlang:send_after(?DIALOG_TIMEOUT, self(), {dialog_timeout})},
    {next_state, ?CURRSTATE, State1}.

% ----
finish(State) ->
    {ok,State1} = ?ESG_MEDIA:stop(State),
    d(State1, " -> switch to '~p'", [?STOPPING_STATE]),
    erlang:send_after(?STOPPING_TIMEOUT, self(), {stopping_timeout}),
    {next_state, ?STOPPING_STATE, State1}.

% ----
stop_external(Reason, State) ->
    d(State, ":stop_external(~120p)", [Reason]),
    finalize_on_stop(Reason, State).

% ----
stop_error(Reason, State) ->
    d(State, "stop_error(~120p)", [Reason]),
    finalize_on_stop(Reason, State).

% ----
test(State) ->
    d(State, "test"),
    {next_state, ?CURRSTATE, State}.

% ----
sip_ack([_CallId, _Req]=P, State) ->
    d(State, "sip_ack, CallId=~120p", [_CallId]),
    do_on_ack(P, State).

% ----
sip_bye([_CallId, _Req]=P, State) ->
    d(State, "sip_bye, CallId=~120p", [_CallId]),
    do_on_bye(P, State).

% ----
sip_notify([_CallId, _Req]=P, StateName, State) ->
    d(State, StateName, "sip_notify, CallId=~120p", [_CallId]),
    do_on_notify(P, StateName, State).

% ----
sip_info([_CallId, _Req]=P, StateName, State) ->
    d(State, StateName, "sip_info, CallId=~120p", [_CallId]),
    do_on_info(P, StateName, State).

% ----
sip_message([_CallId, _Req]=P, StateName, State) ->
    d(State, StateName, "sip_message, CallId=~120p", [_CallId]),
    do_on_message(P, StateName, State).

% ----
uac_response([_Resp], State) ->
    #sipmsg{class={resp,SipCode,SipReason},
            cseq={CSeq,Method},
            call_id=CallId}=_Resp,
    d(State, "uac_response ~p:~p -> ~p ~p, CallId=~120p", [Method,CSeq,SipCode,SipReason,CallId]),
    {next_state, ?CURRSTATE, State}.

% ----
uas_dialog_response([_Resp]=Args, State) ->
    #sipmsg{class={resp,SipCode,_},
            cseq={CSeq,Method},
            call_id=CallId}=_Resp,
    d(State, "uas_dialog_response ~p:~p -> ~p, CallId=~120p", [Method,CSeq,SipCode,CallId]),
    do_on_uas_dialog_response(Args, State).

% ----
call_terminate([_Reason, _Call], State) ->
    #call{call_id=CallId}=_Call,
    d(State, "call_terminate, CallId=~120p, Reason=~120p", [CallId, _Reason]),
    case State of
        #?StateDlg{a=#side{callid=CallId}} ->
            finalize_on_stop("Call terminated", State);
        #?StateDlg{b=#side{callid=CallId}} ->
            finalize_on_stop("Call terminated", State);
        _ ->
            d(State, "call_terminate skipped"),
            {next_state, ?CURRSTATE, State}
    end.

% ================================
% #347
mg_dtmf([_CtxId, _CtxTermId, _Event]=Args, StateName, StateData) ->
    do_on_mg_dtmf(Args, StateName, StateData).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ============================
%% Notify
%% ============================

do_on_notify([CallId, Req], StateName, State) ->
    case State of
        #?StateDlg{a=#side{callid=CallId}, b=BSide} ->
            forward_notify(Req, BSide, StateName, State);
        #?StateDlg{a=ASide, b=#side{callid=CallId}} ->
            forward_notify(Req, ASide, StateName, State);
        _ ->
            State
    end.

forward_notify(Req, YSide, StateName, State) ->
    #sipmsg{cseq={CSeq,_},
            content_type=ContentType,
            event=Event,
            body=Body}=Req,
    d(State, StateName, "forward_notify. UNKNOWN DIALOG NOTIFY MESSAGE CSeq=~p, Event=~100p, Content-Type=~100p", [CSeq, Event, ContentType]),
    SubS = case nksip_subscription_lib:state(Req) of
               invalid -> active;
               T -> T
           end,
    Opts = [{content_type, ContentType},
            {event, Event},
            {subscription_state, SubS},
            {body, Body},
            user_agent],
    #side{dhandle=YDlgHandle}=YSide,
    catch nksip_uac:notify(YDlgHandle, [async|Opts]),
    send_response({200,[]}, Req, State),
    State.

%% ============================
%% Info
%% ============================

do_on_info([CallId, Req], StateName, State) ->
    case State of
        #?StateDlg{a=#side{callid=CallId}, b=BSide} ->
            forward_info(Req, BSide, StateName, State);
        #?StateDlg{a=ASide, b=#side{callid=CallId}} ->
            forward_info(Req, ASide, StateName, State);
        _ ->
            State
    end.

forward_info(Req, YSide, StateName, State) ->
    #sipmsg{cseq={CSeq,_},
            content_type=ContentType,
            body=Body}=Req,
    d(State, StateName, "forward_info. CSeq=~p, Content-Type=~100p", [CSeq, ContentType]),
    Opts = [{content_type, ContentType},
            {body, Body},
            user_agent],
    #side{dhandle=YDlgHandle}=YSide,
    catch nksip_uac:info(YDlgHandle, [async|Opts]),
    send_response({200,[]}, Req, State),
    State.

% ------
% #347
do_on_mg_dtmf([_CtxId, CtxTermId, #{}=Event]=_Args, StateName, StateData) ->
    #?StateDlg{media=#media{caller=ATerm, callee=BTerm}, a=ASide, b=BSide}=StateData,
    % @todo check if yterm is not support 101 telephone-event
    case {?MGC_TERM:get_term_id(ATerm), ?MGC_TERM:get_term_id(BTerm)} of
        {CtxTermId,_} -> send_info_dtmf(BSide,Event,StateName,StateData);
        {_,CtxTermId} -> send_info_dtmf(ASide,Event,StateName,StateData);
        _ -> {next_state, StateName, StateData}
    end.
% @private
send_info_dtmf(#side{remotesdp=RSdp}=Side,Event,StateName,StateData) ->
    case ?M_SDP:check_rfc2833(RSdp) of
        false -> send_info_dtmf_1(Side,Event,StateName,StateData);
        true -> {next_state, StateName, StateData}
    end.
% @private
send_info_dtmf_1(#side{dhandle=DlgHandle}=_Side,Event,StateName,StateData) ->
    DTMF = lists:map(fun(Sym) when is_integer(Sym) -> ?M_SDP:dtmf_rfc2833_to_info(Sym) end, lists:flatten(maps:get(dtmf,Event))),
    Opts = [{content_type, <<"application/dtmf-relay">>},
            {body, <<"Signal=",(?EU:to_binary(DTMF))/binary,"\r\nDuration=300">>},
            user_agent],
    catch nksip_uac:info(DlgHandle, [async|Opts]),
    {next_state, StateName, StateData}.

%% ============================
%% Message
%% ============================

do_on_message([CallId, Req], StateName, State) ->
    case State of
        #?StateDlg{a=#side{callid=CallId}, b=BSide} ->
            forward_message(Req, BSide, StateName, State);
        #?StateDlg{a=ASide, b=#side{callid=CallId}} ->
            forward_message(Req, ASide, StateName, State);
        _ ->
            State
    end.

forward_message(Req, YSide, StateName, State) ->
    #sipmsg{cseq={CSeq,_},
            content_type=ContentType,
            body=Body}=Req,
    d(State, StateName, "forward_message. CSeq=~p, Content-Type=~100p", [CSeq, ContentType]),
    Opts = [{content_type, ContentType},
            {body, Body},
            user_agent],
    #side{dhandle=YDlgHandle}=YSide,
    catch nksip_uac:message(YDlgHandle, [async|Opts]),
    send_response({200,[]}, Req, State),
    State.

%% ============================
%% Ack
%% ============================

do_on_ack([CallId, Req]=_P, State) ->
    case ?U:extract_sdp(Req) of
        undefined -> {next_state, ?CURRSTATE, State};
        #sdp{}=RSdp ->
            case ?ESG_MEDIA:apply_remote_ext(CallId, RSdp, State) of
                {error,R} ->
                    stop_error(R, State#?StateDlg{reinvite=undefined});
                {ok, #?StateDlg{a=#side{callid=CallId}=A}=State1} ->
                    {next_state, ?CURRSTATE, State1#?StateDlg{a=A#side{remotesdp=RSdp}}};
                {ok, #?StateDlg{b=#side{callid=CallId}=B}=State1} ->
                    {next_state, ?CURRSTATE, State1#?StateDlg{b=B#side{remotesdp=RSdp}}}
            end end.

%% ============================
%% Bye
%% ============================

do_on_bye([CallId, Req]=P, State) ->
    case State of
        #?StateDlg{a=#side{callid=CallId}=ASide, b=BSide} ->
            finalize_on_bye(P, "Bye from A", {ASide, BSide}, State);
        #?StateDlg{a=ASide, b=#side{callid=CallId}=BSide} ->
            finalize_on_bye(P, "Bye from B", {BSide, ASide}, State);
        _ ->
            #?StateDlg{a=#side{callid=ACallId},b=#side{callid=BCallId}}=State,
            ?LOG("BYE side not found by ~p (a=~p, b=~p)", [CallId, ACallId, BCallId]),
            response_bye(ok, Req, State),
            ?DLG_STORE:unlink_dlg(CallId), % it's simple trimming
            {next_state, ?CURRSTATE, State}
    end.

% ----
finalize_on_bye([_CallId, Req], Reason, {XSide,YSide}, State) ->
    %
    d(State, " -> answer incoming bye ~p",[XSide#side.localtag]),
    response_bye(ok, Req, State),
    %
    d(State, " -> send bye to opposite ~p",[YSide#side.localtag]),
    ?ESG_UTILS:send_bye(YSide, State),
    %
    cleanup_dialog(State#?StateDlg{stopreason=Reason}).

%% ============================
%% Service
%% ============================

% ---
send_response(SipReply, Req, State) ->
    {ok,Handle} = nksip_request:get_handle(Req),
    d(State, "send_response: ~1000tp", [reply_code(SipReply)]),
    DlgId = get_dialogid(State),
    ?U:send_sip_reply(fun() ->
                          Res = nksip_request:reply(SipReply, Handle),
                          ?LOG("EB2B fsm ~p '~p': send_response result: ~1000tp", [DlgId, ?CURRSTATE, Res])
                      end).


% ----
finalize_on_stop(Reason, State) ->
    #?StateDlg{a=ASide, b=BSide}=State,
    %
    d(State, " -> send bye to A ~p", [ASide#side.localtag]),
    ?ESG_UTILS:send_bye(ASide, State),
    d(State, " -> send bye to B ~p", [BSide#side.localtag]),
    ?ESG_UTILS:send_bye(BSide, State),
    %
    cleanup_dialog(State#?StateDlg{stopreason=Reason}).

% ----
response_bye(SipReply, ByeReq, State) ->
    send_response(SipReply, ByeReq, State).

% ----
cleanup_dialog(State) ->
    d(State, ":cleanup"),
    Now = os:timestamp(),
    #?StateDlg{a=#side{callid=ACallId}=ASide,
               b=#side{callid=BCallId}=BSide}=State,
    %
    State1 = State#?StateDlg{a=ASide#side{finaltime=Now},
                                     b=BSide#side{finaltime=Now},
                                     finaltime=Now},
    %
    % @todo account free side a, side b
    %
    ?DLG_STORE:unlink_dlg(ACallId),
    ?DLG_STORE:unlink_dlg(BCallId),
    % media
    {ok,State2} = ?ESG_MEDIA:stop(State1),
    % statistics
    return_stopping(State2).

% ----
return_stopping(State) ->
    #?StateDlg{timer_ref=TimerRef,
               stopfuns=StopFuns}=State,
    erlang:cancel_timer(TimerRef),
    lists:foreach(fun(F) when is_function(F) -> F() end, StopFuns),
    finish(State).

%% ============================
%% UAS response, dialog ready
%% ============================

do_on_uas_dialog_response([#sipmsg{class={resp,SipCode,_}, cseq={_,'INVITE'}}=Resp], State)
  when SipCode >= 200, SipCode < 300 ->
    #sipmsg{to={_,ToTag}}=Resp,
    {ok,XCallId} = nksip_response:call_id(Resp),
    {ok,XDlgHandle} = nksip_dialog:get_handle(Resp),
    State1 = case State of
                     #?StateDlg{a=#side{callid=XCallId}=ASide} ->
                         State#?StateDlg{a=ASide#side{dhandle=XDlgHandle}};
                     #?StateDlg{b=#side{callid=XCallId, remotetag=RTag, localtag=LTag}=BSide} when RTag==ToTag; LTag==ToTag ->
                         State#?StateDlg{b=BSide#side{dhandle=XDlgHandle}};
                     #?StateDlg{b=#side{callid=XCallId}} -> d(State, "found double 2xx response on INVITE (tag=~p)", [ToTag]), State
                 end,
    {next_state, ?CURRSTATE, State1};
do_on_uas_dialog_response(_P, State) ->
    {next_state, ?CURRSTATE, State}.

%% ============================
%% Internal functions
%% ============================

%% -----
d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    DlgId = get_dialogid(State),
    ?LOG("EB2B fsm ~p '~p':" ++ Fmt, [DlgId,?CURRSTATE] ++ Args).
d(State, StateName, Fmt, Args) ->
    DlgId = get_dialogid(State),
    ?LOG("EB2B fsm ~p '~p':" ++ Fmt, [DlgId,StateName] ++ Args).

%% -----
get_dialogid(#?StateDlg{dialogid=DlgId}) -> DlgId;
get_dialogid(#state_forking{dialogid=DlgId}) -> DlgId.

%% -----
reply_code({Code,_}) -> Code;
reply_code(SipReply) -> SipReply.