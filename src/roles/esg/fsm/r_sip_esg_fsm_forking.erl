%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Presents 'forking' state of B2BUA Dialog FSM
%%%         Acts on start to make forward calls to selected uris.
%%%        Ends with final 2xx answer or timeout-4xx-5xx-6xx answers from all forks.
%%% @todo

-module(r_sip_esg_fsm_forking).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([init/1,
         start/1,
         stop_external/2,
         test/1,
         sip_cancel/2,
         sip_bye/2,
         sip_replace_invite/2,
         fork_timeout/2,
         total_timeout/1,
         uac_response/2,
         uas_dialog_response/2,
         call_terminate/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_esg.hrl").
-include("../include/r_sip_mgc.hrl").
-include("../include/r_sip_headers.hrl").

% -include("../include/r_nksip.hrl").

-define(CURRSTATE, 'forking').

%% ====================================================================
%% API functions
%% ====================================================================

init(State) ->
    do_init(State).

start(State) ->
    do_start(State).

% ----
stop_external(Reason, State) ->
    d(State, "stop_external(~p)", [Reason]),
    ?FORKING_UTILS:do_final(?Decline("EB2B.F. Call aborted"), State).

% ----
test(State) ->
    d(State, "test"),
    {next_state, ?CURRSTATE, State}.

% ----
sip_cancel([CallId, _Req, _InviteReq], State) ->
    d(State, "cancel, CallId=~120p", [CallId]),
    % State1 = ?FORKING_UTILS:send_response_to_caller({487,[]}, State), % automatically by nksip
    case State of
        #state_forking{a=#side{callid=CallId}} -> ok;
        _ -> ?FORKING_UTILS:send_response_to_caller({487,[]}, State) % #265
    end,
    ?FORKING_UTILS:do_final(State).

% ----
sip_bye([_CallId, Req], State) ->
    d(State, "forking bye, CallId=~120p", [_CallId]),
    {ok,Handle} = nksip_request:get_handle(Req),
    DlgId = get_dialogid(State),
    % spawn, not worker
    Pid = spawn(fun() ->
                    Res = nksip_request:reply(ok, Handle),
                    ?LOG("EB2B fsm ~p '~p': send_response result: ~1000tp", [DlgId,?CURRSTATE,Res])
                end),
    d(State,"send_response: ~1000tp (pid=~1000tp)", [200,Pid]),
    %
    ?FORKING_UTILS:do_final(State).

% ----
% rfc-3311 update or caching
sip_replace_invite(Args, State) ->
    ?FORKING_REPLACING:enter_by_invite(Args,State).

% ----
fork_timeout([CallId, BLTag]=P, State) ->
    d(State, "fork_timeout BLTag=~120p, CallId=~120p", [BLTag, CallId]),
    do_on_uac_timeout(P, State).

% ----
total_timeout(State) ->
    d(State, "total_timeout"),
    ?FORKING_UTILS:do_final(?RequestTimeout("EB2B.F. Call timeout"), State).

% ----
uac_response([_Resp]=Args, State) ->
    #sipmsg{class={resp,SipCode,SipReason},
            cseq={CSeq,Method},
            call_id=CallId}=_Resp,
    d(State, "uac_response ~p:~p -> ~p ~p, CallId=~120p", [Method,CSeq,SipCode,SipReason,CallId]),
    do_on_uac_response(Args, State).

% ----
uas_dialog_response([_Resp], State) ->
    {next_state, ?CURRSTATE, State}.

% ----
call_terminate([Reason, Call], State) ->
    #call{call_id=CallId}=Call,
    case State of
        #?StateDlg{a=#side{callid=CallId}} ->
            d(State, "call_terminate, CallId=~120p, Reason=~120p", [CallId, Reason]),
            ?FORKING_UTILS:do_final(?InternalError("EB2B.F. Call terminated"), State);
        _ ->
            {next_state, ?CURRSTATE, State}
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% =============================================================
%% Initialization
%% =============================================================

do_init(State) ->
    Now = os:timestamp(),
    #state_forking{map=#{opts:=DlgOpts}=Map}=State,
    [Req,UriRules] = ?EU:extract_required_props([req,urirules], DlgOpts),
    #sipmsg{ruri=#uri{scheme=Scheme,user=User,domain=Domain}}=Req,
    % -------
    #side{callid=ACallId,remotetag=ARTag,localtag=ALTag}=ASide = ?ESG_UTILS:build_aside(Req,State),
    ASide1 = ASide#side{starttime=Now},
    % -------
    TotalTimerRef = case lists:keyfind(forking_timeout, 1, DlgOpts) of
                        {_,TotalTimeout} -> erlang:send_after(TotalTimeout, self(), {total_timeout});
                        false -> undefined
                    end,
    Ref = make_ref(),
    % -------
    State2 = State#state_forking{a=ASide1,
                                 map=Map#{applied_redirects:=[{Scheme,User,Domain}]},
                                 usedtags=[ARTag, ALTag],
                                 b_rule=?U:canonize_uri_rules(UriRules),
                                 total_timer_ref=TotalTimerRef,
                                 ref=Ref,
                                 starttime=Now},
    ?STAT_FACADE:link_domain(ACallId, maps:get(clusterdomain,maps:get(account,Map))), % RP-415
    % -------
    self() ! {start, Ref},
    State2.

%% =============================================================
%% Starting
%% =============================================================

do_start(State) ->
    check_trunk_count_limit(State).

%% --------
%% checks trunk count limit
%%
check_trunk_count_limit(State) ->
    #state_forking{map=Map,
                   stopfuns=StopFuns}=State,
    Dir = maps:get(dir,Map),
    AMap = maps:get(account,Map),
    case ?EXT_UTILS:reserve_active_trunk(AMap,Dir) of
        false ->
            ?FORKING_UTILS:do_final(?Forbidden("EB2B.F. Trunk count limit reached"), State);
        {ok,FunClear} ->
            State1 = State#state_forking{stopfuns=[FunClear|StopFuns]},
            ?MONITOR:append_fun(self(), FunClear),
            check_media_available(State1)
    end.

%% --------
%% checks if mgc-mg available
%%
check_media_available(State) ->
    case ?ESG_MEDIA:check_available(State) of
        {error,mgc_overload} ->
            ?FORKING_UTILS:error_mgc("MGC overload", State, ?InternalError("EB2B.F. MGC overload"));
        {error,_} ->
            ?FORKING_UTILS:error_mgc("MGC not ready", State, ?InternalError("EB2B.F. MGC not ready"));
        {ok,false} ->
            ?FORKING_UTILS:error_mgc("No media trunk available", State, ?InternalError("EB2B.F. No media trunk available"));
        {ok,true} ->
            % @todo account busy side a
            start_prepare_media(State)
    end.

%% --------
%% checks media formats and create media context
%%
start_prepare_media(State) ->
    case ?ESG_MEDIA:prepare_start(State) of
        {error, Reply} when is_tuple(Reply) ->
            ?FORKING_UTILS:do_final(Reply, State);
        {ok, State1} ->
            State2 = prepare_b_locals_common(State1),
            start_forward_bside(State2)
    end.

%% --------
%% forms b local uris at start (same for all forks)
%%
prepare_b_locals_common(State) ->
    #state_forking{map=Map,
                   a=#side{remoteuri=AUri,
                           remotecontacts=[AContact|_]}}=State,
    [DlgOpts,Dir,AMap] = ?EU:maps_get([opts,dir,account],Map),
    % b from uri
    BLUri = ?U:clear_uri(AUri),
    % b local contact
    BTransp = ?ESG_UTILS:get_transport_opts('b', DlgOpts),
    BLContact = case Dir of % @localcontact (inside: cfg=default! | outside: reg!)
                    ?DirInside -> ?U:build_local_contact(AContact);
                    ?DirOutside -> ?U:build_local_contact(?EXT_UTILS:build_local_contact_addr(AMap),AContact)
                end,
    BLContact1 = BLContact#uri{opts=BTransp},
    % shadow replacement
    BLUriOpt = lists:keyfind('bl_uri', 1, DlgOpts),
    BLContactOpt = lists:keyfind('bl_contact', 1, DlgOpts),
    {BLUri2, BLContact2} = prepare_b_locals({BLUri, BLContact1}, {BLUriOpt, BLContactOpt}),
    %
    State#state_forking{b=#side{localuri=BLUri2,
                                localcontact=BLContact2}}.
%
prepare_b_locals({BLUri, BLContact}, {BLUriOpt, BLContactOpt}) ->
    {BLUri1, BLContact1} =
        try
            case BLUriOpt of
                false -> {BLUri, BLContact};
                {_,#uri{}=Uri1} -> throw(Uri1);
                #uri{}=Uri1 -> throw(Uri1)
            end
        catch
            throw:#uri{disp=Disp,user=User,domain=Domain} ->
                {?U:update_uri(BLUri, lists:filter(fun({_,undefined})->false;(_)->true end,
                                                   lists:zip([disp,user,domain,user_orig], [Disp,User,Domain,<<>>]))),
                 ?U:update_uri(BLContact, lists:filter(fun({_,undefined})->false;(_)->true end,
                                                       lists:zip([disp,user], [Disp,User])))}
        end,
    BLContact2 =
        try
            case BLContactOpt of
                false -> BLContact1;
                {_,#uri{}=Uri2} -> throw(Uri2);
                #uri{}=Uri2 -> throw(Uri2)
            end
        catch
            throw:#uri{disp=ContDisp,user=ContUser} ->
                ?U:update_uri(BLContact1, lists:filter(fun({_,undefined})->false;(_)->true end,
                                                       lists:zip([disp,user], [ContDisp,ContUser])))
        end,
    {BLUri1, BLContact2}.

%% --------
%% starts a portion of parallel forked uris
%%
start_forward_bside(State) ->
    case forward_bside(State) of
        #state_forking{}=State1 ->
            {next_state, ?CURRSTATE, State1};
        Term -> Term
    end.

%% =============================================================
%% Forking
%% =============================================================

% -----
forward_bside(State) ->
    #state_forking{b_rule=[UriGroup|Rest]}=State,
    case prepare_calls(UriGroup, State#state_forking{b_rule=Rest}) of
        #state_forking{b_prep=Prepared}=State1 ->
            #state_forking{} = invite_prepared(Prepared, State1#state_forking{b_prep=[]});
        Term -> Term
    end.

% -----
prepare_calls([], State) -> State;
prepare_calls([UriRule|Rest], State) ->
    #state_forking{dialogid=DialogId,
                   map=Map,
                   acallid=ACallId,
                   a=ASide,
                   b=#side{localuri=BLUriCmn,
                           localcontact=BLContactCmn},
                   b_prep=BPrepared}=State,
    [AReq,DlgOpts,DlgNum,OutCallIdx,Dir,ReplOpts] = ?EU:maps_get([init_req,opts,dlgnum,last_out_callid_idx,dir,replace_opts], Map),
    #side{remotesdp=#sdp{}}=ASide,
    % callid
    BCallId = ?ESG_UTILS:build_out_callid(DlgNum, OutCallIdx+1, ACallId),
    % from uri, local contact
    RuleMap = ?U:parse_uri_rule(UriRule, {?DEFAULT_FORK_RULE_TIMEOUT}), % @forktimeout
    AOR = maps:get('aor',RuleMap),
    Uri = maps:get('uri',RuleMap),
    Timeout = maps:get('ftimeout',RuleMap),
    {BLUri, BLContact} = prepare_b_locals({BLUriCmn, BLContactCmn}, % @localcontact (cfg? | Uri's route!)
                                          {maps:get('bl_uri', RuleMap, false),
                                           maps:get('bl_contact', RuleMap, false)}),
    % From URI (A side)
    {BLFromTag, State1} = ?FORKING_UTILS:generate_tag(State),
    BLFromUri = BLUri#uri{ext_opts=[{<<"tag">>,BLFromTag}]},
    % To URI (B side)
    {AorS,AorU,AorD}=AOR,
    BRToUri = ?U:update_uri(?U:clear_uri(Uri),
                            [{scheme,AorS},{user,AorU},{domain,AorD},{port,0}]), % @todo AOR-domain
    % monitor, links
    ?DLG_STORE:push_dlg(BCallId, {DialogId,self()}),
    ?MONITOR:append_fun(self(), fun() -> ?DLG_STORE:unlink_dlg(BCallId) end),
    ?STAT_FACADE:link_fork(BCallId, ACallId), % @stattrace
    BDomain = maps:get(clusterdomain,maps:get(account,Map)),
    ?STAT_FACADE:link_domain(BCallId, BDomain), % RP-415
    ?ESG_UTILS:log_callid(b,DlgNum,ACallId,OutCallIdx+1,BCallId,BDomain), % #307, RP-415
    % media check modify
    case ?ESG_MEDIA:fwd_invite_request_to_b(State1, Uri, BCallId) of
        {error, R} -> ?FORKING_UTILS:error_mgc(R,State1);
        {ok, State2, BLSdp} ->
            AuthOpts = ?ESG_UTILS:build_auth_opts('b', DlgOpts),
            %
            CmnOpts = [{call_id,BCallId},
                       {from,BLFromUri},
                       {to,BRToUri},
                       {contact,BLContact},
                       %translate_header_list('supported', AReq),
                       %translate_header_list('allow', AReq),
                       %translate_header_list('allow_events', AReq),
                       user_agent
                      | AuthOpts],
            CmnOpts1 = case RuleMap of
                           #{opts:=ROpts} -> ROpts ++ CmnOpts;
                           _ -> CmnOpts
                       end,
            CmnOpts2 = case {Dir, ?U:is_b2bmedia()} of
                           {?DirInside,false} -> [{replace, {?B2BHeader, <<"media=0">>}} | CmnOpts1];
                           _ -> CmnOpts1
                       end,
            %
            InviteOpts = [{body,BLSdp},
                          auto_2xx_ack,
                          %record_route, % #244
                          {cseq_num,1}],
            % #115
            InviteOpts1 = case ReplOpts of
                              _ when is_list(ReplOpts) -> ReplOpts ++ InviteOpts;
                              undefined -> InviteOpts
                          end,
            % RP-1575
            InviteOpts2 = case maps:get(dir,Map) of
                              ?DirOutside -> InviteOpts1;
                              ?DirInside -> [{add,{?EsgDlgHeader,DialogId}} | InviteOpts1]
                          end,
            % forward custom headers
            InviteOpts3 = lists:foldl(fun({<<"x-fwd-",N1/binary>>,V},Acc) -> [{add,{N1,V}} | Acc];
                                         (_,Acc) -> Acc
                                      end, InviteOpts2, AReq#sipmsg.headers),

            %
            BSide=#side_fork{callid=BCallId,
                             calldir=?DirOutside,
                             localuri=BLFromUri,localtag=BLFromTag,localcontact=BLContact,
                             remoteuri=BRToUri,
                             requesturi=Uri,
                             rule_timeout=Timeout,
                             cmnopts=CmnOpts2,
                             inviteopts=InviteOpts3},
            State3 = State2#state_forking{b_prep=[BSide|BPrepared],
                                          map=Map#{last_out_callid_idx:=OutCallIdx}},
            prepare_calls(Rest, State3)
    end.

% -----
invite_prepared([], State) -> State;
invite_prepared([ForkInfo|Rest], State) ->
    State1 = invite_fork(ForkInfo, State),
    invite_prepared(Rest, State1).

% -----
invite_fork(ForkInfo, State) ->
    #state_forking{map=#{app:=App},
                   b_act=BAct}=State,
    #side_fork{callid=CallId,
               requesturi=Uri,
               localtag=BLTag,
               rule_timeout=Timeout, % @forktimeout
               cmnopts=CmnOpts,
               inviteopts=InviteOpts}=ForkInfo,
    %
    Now = os:timestamp(),
    %
    case catch nksip_uac:invite(App, Uri, [async|CmnOpts++InviteOpts]) of
        {'EXIT',Err} ->
            d(State, "invite_fork BLTag=~p, Caught error=~120p", [BLTag, Err]),
            State#state_forking{b_fin=[ForkInfo#side_fork{res={error,Err}, starttime=Now, finaltime=Now}|BAct]};
        {error,_R}=Err ->
            d(State, "invite_fork BLTag=~p, Error=~120p", [BLTag, Err]),
            State#state_forking{b_fin=[ForkInfo#side_fork{res=Err, starttime=Now, finaltime=Now}|BAct]};
        {async,ReqHandle} ->
            d(State, "invite_fork BLTag=~p", [BLTag]),
            % @todo account early side b
            Fork1 = ForkInfo#side_fork{rhandle=ReqHandle,
                                       starttime=Now,
                                       rule_timer_ref=erlang:send_after(Timeout, self(), {fork_timeout, [CallId, BLTag]}), % @forktimeout
                                       resp_timer_ref=erlang:send_after(?DEFAULT_FORK_100_TIMEOUT, self(), {fork_timeout, [CallId, BLTag]})}, % @forktimeout
            State#state_forking{b_act=[Fork1|BAct]}
    end.

%% =============================================================
%% Handling UAC responses
%% =============================================================

% -----
do_on_uac_timeout([_CallId, BLTag], State) ->
    #state_forking{b_act=BAct, b_fin=BFin}=State,
    d(State, "fork_timeout BLTag=~p, CallId=~p", [BLTag, _CallId]),
    case lists:keyfind(BLTag, #side_fork.localtag, BAct) of
        false -> {next_state, ?CURRSTATE, State};
        #side_fork{}=Fork ->
            ?FORKING_UTILS:cancel_active_forks([Fork], [], State),
            Fork1 = Fork#side_fork{res={timeout},
                                   finaltime=os:timestamp(),
                                   rule_timer_ref=undefined,
                                   resp_timer_ref=undefined},
            State1 = State#state_forking{b_act=BAct--[Fork], b_fin=[Fork1|BFin]},
            case check_final(State1) of
                {true, State2} -> ?FORKING_UTILS:return_stopping(State2);
                {false, State2} -> {next_state, ?CURRSTATE, State2}
            end
    end.

% -----
do_on_uac_response([#sipmsg{cseq={_,'INVITE'}}=_Resp]=P, State) ->
    on_invite_response(P, State);

do_on_uac_response([#sipmsg{cseq={_,Method}}=_Resp], State) ->
    d(State, "uac_pre_response('~p') skipped", [Method]),
    {next_state, ?CURRSTATE, State}.

% -----
on_invite_response([#sipmsg{class={resp,100,_SipReason}}=_Resp], State) ->
    {next_state, ?CURRSTATE, State};
on_invite_response([#sipmsg{class={resp,SipCode,_SipReason}}=_Resp]=P, State) when SipCode > 100, SipCode < 200 ->
    on_invite_response_1xx(P, State);
on_invite_response([#sipmsg{class={resp,SipCode,_SipReason}}=_Resp]=P, State) when SipCode >= 200, SipCode < 300 ->
    on_invite_response_2xx(P, State);
on_invite_response([#sipmsg{class={resp,SipCode,_SipReason}}=_Resp]=P, State) when SipCode >= 300, SipCode < 400 ->
    on_invite_response_3xx(P, State);
on_invite_response([#sipmsg{class={resp,SipCode,SipReason}}=Resp], State) when SipCode == 401; SipCode == 407 ->
    Resp1 = Resp#sipmsg{class={resp,403,SipReason}},
    on_invite_response_4xx_5xx_6xx([Resp1], {response,Resp1}, State);
on_invite_response([#sipmsg{class={resp,SipCode,_}}=Resp]=P, State) when SipCode >= 400, SipCode < 500 ->
    on_invite_response_4xx_5xx_6xx(P, {response,Resp}, State);
on_invite_response([#sipmsg{class={resp,SipCode,_SipReason}}=Resp]=P, State) when SipCode >= 500, SipCode < 600 ->
    on_invite_response_4xx_5xx_6xx(P, {response,Resp}, State);
on_invite_response([#sipmsg{class={resp,SipCode,_SipReason}}=Resp]=P, State) when SipCode >= 600, SipCode < 700 ->
    on_invite_response_4xx_5xx_6xx(P, {response,Resp}, State);
on_invite_response([_Resp]=P, State) ->
    on_invite_response_error(P, State).

% ----
% 3xx
%
on_invite_response_3xx([Resp]=P, State) ->
    #sipmsg{contacts=[#uri{scheme=Scheme, user=User, domain=Domain}=_MoveTo|_]}=Resp,
    % @todo account free side b
    AOR = {Scheme, User, Domain},
    #state_forking{map=#{init_req:=Req,
                         applied_redirects:=Redirects}=Map}=State,
    case lists:member(AOR,Redirects) of
        true ->
            d(State, "3xx-MoveTo ~120p, found cycled", [AOR]),
            SipCode1 = 482, % loop detected
            State3 = State;
        false ->
            SipCode1 = 410, % gone (408 not acceptable, 488 not acceptable here, 410 gone, 480 temporarily unavailable)
            % @todo what about redirect from outer?
            case ?ESG_ROUTER:build_route_opts(AOR, Req) of
                {reply, _Reply} ->
                    State2 = State;
                {ok, [{urirules,[_|_]=UriRules}|_]} ->
                    #state_forking{b_rule=OrigRules}=State,
                    [UriGroup|Rest] = ?U:canonize_uri_rules(UriRules),
                    #state_forking{b_prep=Prepared}=State1= prepare_calls(UriGroup, State#state_forking{b_rule=Rest++OrigRules}),
                    State2 = invite_prepared(Prepared, State1#state_forking{b_prep=[]})
            end,
            State3 = State2#state_forking{map=Map#{applied_redirects:=[AOR|Redirects]}}
    end,
    Resp1 = Resp#sipmsg{class={resp,SipCode1,nksip_unparse:response_phrase(SipCode1)}},
    on_invite_response_4xx_5xx_6xx(P, {response,Resp1}, State3).

% ----
% unknown
%
on_invite_response_error([_Resp]=P, State) ->
    on_invite_response_4xx_5xx_6xx(P, {error}, State).

%% ===================================
%% Negative UAC response
%% ===================================

% ---
% 4xx, 5xx, 6xx
%
on_invite_response_4xx_5xx_6xx([Resp], ForkResult, State) ->
    #sipmsg{from={_,BLTag}}=Resp,
    % @todo account free side b
    #state_forking{b_act=BAct, b_fin=BFin}=State,
    ForkInfo = lists:keyfind(BLTag, #side_fork.localtag, BAct),
    ForkInfo1 = ?FORKING_UTILS:cancel_timer(ForkInfo),
    %
    ForkInfo2 = ForkInfo1#side_fork{res=ForkResult,
                                    finaltime=os:timestamp()},
    State1 = State#state_forking{b_act=BAct--[ForkInfo], b_fin=[ForkInfo2|BFin]},
    case check_final(State1) of
        {true, State2} -> ?FORKING_UTILS:return_stopping(State2);
        {false, State2} -> {next_state, ?CURRSTATE, State2}
    end.

% -------------------------
% Checks if there is no one fork remain
%
check_final(#state_forking{b_act=[], b_rule=[]}=State) ->
    % final
    State1 = finalize_best_result(State),
    {true, State1};
check_final(#state_forking{b_act=[]}=State) ->
    % next_prep
    {ok,State1} = start_next_fork_group(State),
    {false, State1};
check_final(#state_forking{}=State) ->
    % wait active
    {false, State}.

% ------------------------
% Enforces next fork group, which is waiting for active forks
%
start_next_fork_group(State) ->
    State1 = forward_bside(State),
    {ok,State1}.

% ------------------------
% Find best negative result, send to caller and finalize dialog
%
finalize_best_result(#state_forking{b_fin=BFin}=State) ->
    Fbest = fun({response,#sipmsg{class={resp,SipCode,_}}=Resp}, undefined) -> {SipCode,Resp};
               ({response,#sipmsg{class={resp,SipCode,_}}=Resp}, {AccSipCode,_}=Acc) ->
                    case ?FORKING_UTILS:choose_best_result({SipCode div 100, SipCode}, {AccSipCode div 100, AccSipCode}) of
                        SipCode -> {SipCode,Resp};
                        _ -> Acc
                    end;
               ({timeout}, undefined) -> {408, undefined};
               ({timeout}, {AccSipCode,_}=Acc) ->
                    case ?FORKING_UTILS:choose_best_result({4, 408}, {AccSipCode div 100, AccSipCode}) of
                        408 -> {408, undefined};
                        _ -> Acc
                    end;
               ({error}, undefined) -> {500, undefined};
               ({error}, Acc) -> Acc
            end,
    {BestSipCode,Resp} = lists:foldl(Fbest, undefined, [Res || #side_fork{res=Res} <- BFin]),
    % reason & warning headers
    Opts = ?FORKING_UTILS:fwd_response_headers_auto(Resp, []),
    State1 = ?FORKING_UTILS:send_response_to_caller({BestSipCode, Opts}, State),
    State2 = ?FORKING_UTILS:do_finalize_dialog(State1),
    State2.

%% ===================================
%% Positive UAC response
%% ===================================

% -----------------------
% 1xx
% -----------------------
on_invite_response_1xx([#sipmsg{class={resp,SipCode,_}}=Resp], State) ->
    #state_forking{map=#{last_response:=LastResp}=Map,
                   b_act=BAct}=State,
    {Fork,State1} = update_fork(Resp, State), % @forktimeout
    Opts = ?FORKING_UTILS:fwd_response_headers_auto(Resp, [no_dialog]),
    case {length(BAct), LastResp, ?U:extract_sdp(Resp)} of
        % only 1 fork active (current group)
            {1, _, #sdp{}} ->
                case ?ESG_MEDIA:fwd_invite_response_to_a(State1, Resp) of
                    % TODO: some error to recall by another rtp/srtp mode
                    {error, R} -> ?FORKING_UTILS:error_mgc(R,State1);
                    {ok, State2, ALSdp} ->
                        forward_response_1xx({SipCode, [{body, ALSdp}|Opts]}, {Fork,Resp}, State2#state_forking{map=Map#{last_response:={SipCode,true}}})
                end;
            {1, _, _} ->
                forward_response_1xx({SipCode, Opts}, {Fork,Resp}, State1#state_forking{map=Map#{last_response:={SipCode,false}}});
        % more than 1 fork active
            {_N, undefined, _} when _N > 1 -> % not answered yet
                forward_response_1xx({180, Opts}, {Fork,Resp}, State1#state_forking{map=Map#{last_response:={180,false}}});
            {_N, {_,true}, _} when _N > 1 -> % answered + sdp
                forward_response_1xx({180, Opts}, {Fork,Resp}, State1#state_forking{map=Map#{last_response:={180,false}}});
            {_N, {_,false}, _} when _N > 1 -> % answered no sdp
                {next_state, ?CURRSTATE, State1}
    end.

% --
update_fork(#sipmsg{}=Resp, State) ->
    #sipmsg{call_id=CallId,
            class={resp,SipCode,_},
            from={_,BLTag},
            to={_,BRTag}}=Resp,
    #state_forking{b_act=BAct}=State,
    #side_fork{resp_timer_ref=T,
               last_response_code=LastSipCode}=Fork = lists:keyfind(BLTag, #side_fork.localtag, BAct),
    X = case SipCode of
            180 when LastSipCode/=SipCode -> ?DEFAULT_FORK_180_TIMEOUT; % @forktimeout
            182 -> ?DEFAULT_FORK_182_TIMEOUT; % @forktimeout
            183 -> ?DEFAULT_FORK_183_TIMEOUT; % @forktimeout
            _ -> undefined
        end,
    Fork1 = case X of
                undefined -> Fork;
                Timeout ->
                    ?EU:cancel_timer(T),
                    Fork#side_fork{resp_timer_ref=erlang:send_after(Timeout, self(), {fork_timeout, [CallId, BLTag]})}
            end,
    Fork2 = Fork1#side_fork{last_response_code=SipCode,
                            remotetag=BRTag},
    BAct1 = lists:keystore(BLTag, #side_fork.localtag, BAct, Fork2),
    {Fork2,State#state_forking{b_act=BAct1}}.

% --
forward_response_1xx({SipCode, Opts}, {_Fork,Resp}, State) ->
    Opts1 = ?FORKING_UTILS:fwd_response_remoteparty_headers(Resp, State) ++ Opts,
    % --
    #state_forking{map=Map}=State1 = ?FORKING_UTILS:send_response_to_caller({SipCode, Opts1}, State),
    State2 = State1#state_forking{map=Map#{last_response_full:=Resp}},
    {next_state, ?CURRSTATE, State2}.

% ------------------------
% 2xx
% ------------------------
on_invite_response_2xx([Resp], State) ->
    Now = os:timestamp(),
    % -----
    #sipmsg{class={resp,SipCode,_},
            from={_,BLTag},
            to={BRUri,_}}=Resp,
    %#sdp{}=?U:extract_sdp(Resp),
    #state_forking{dialogid=DialogId,
                   map=Map,
                   b_act=BAct,
                   b=BSide,
                   b_fin=BFin,
                   total_timer_ref=TotalTimerRef}=State,
    case TotalTimerRef of undefined -> ok; _ -> erlang:cancel_timer(TotalTimerRef) end,
    ForkInfo = lists:keyfind(BLTag, #side_fork.localtag, BAct),
    ?FORKING_UTILS:cancel_timer(ForkInfo),
    %
    BCancel = BAct--[ForkInfo],
    ?FORKING_UTILS:cancel_active_forks(BCancel, [{reason, {sip, 200, "Call completed elsewhere"}}], State),
    ?FORKING_UTILS:unlink_forks(BCancel++BFin),
    % -----
    case ?ESG_MEDIA:fwd_invite_response_to_a(State, Resp) of
        % TODO: some error to recall by another rtp/srtp mode
        {error, R} -> ?FORKING_UTILS:error_mgc(R,State);
        {ok, State1, ALSdp} ->
            Opts = [{body, ALSdp},
                    user_agent],
            Opts1 = ?FORKING_UTILS:fwd_response_headers_auto(Resp, Opts),
            % @TODO @remoteparty for a on invite response header 'remote-party' (future from when reinvite backside)
            FRemoteParty = fun() -> ?ESG_UTILS:build_remote_party_id(false, Resp, BRUri, State) end,
            Opts2 = case ?ESG_UTILS:extract_property(addRemoteParty2xx, false, State) of
                        false -> RemoteParty = FRemoteParty(), Opts1;
                        true -> RemoteParty = FRemoteParty(), ?U:remotepartyid_opts(RemoteParty,'2xx') ++ Opts1;
                        {true,#uri{}=RemoteParty} -> ?U:remotepartyid_opts(RemoteParty,'2xx') ++ Opts1
                    end,
            % RP-1575
            Opts3 = case maps:get(dir,Map) of
                        ?DirOutside -> [{add,{?EsgDlgHeader,DialogId}} | Opts2];
                        ?DirInside -> Opts2
                    end,
            % ----
            State2 = ?FORKING_UTILS:send_response_to_caller({SipCode, Opts3}, State1),
            % -----
            #state_forking{a=ASide}=State1,
            % @todo account busy side a, side b
            %
            ASide1 = ASide#side{last_remote_party=RemoteParty,
                                answertime=Now},
            %
            BSide1 = ?FORKING_UTILS:make_dlg_side(ForkInfo, BSide, Now, {Resp}),
            %
            State3 = ?FORKING_UTILS:make_dlg_state(State2, Now, {ASide1,BSide1,Resp}),
            d(State2, " -> switch to '~p'", [?DIALOG_STATE]), % !
            % #265
            {next_state,_,State4} = ?DIALOG:start(State3),
            ?FORKING_REPLACING:apply_200(State4)
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

% -----
d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    DlgId = get_dialogid(State),
    ?LOG("EB2B fsm ~p '~p':" ++ Fmt, [DlgId,?CURRSTATE] ++ Args).

% -----
get_dialogid(#state_forking{dialogid=DlgId}) -> DlgId.
