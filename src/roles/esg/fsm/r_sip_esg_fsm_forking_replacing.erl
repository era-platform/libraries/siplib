%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 7.06.2017
%%% @doc #265

-module(r_sip_esg_fsm_forking_replacing).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([enter_by_invite/2,
         apply_200/1,
         abort/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_esg.hrl").
-include("../include/r_sip_mgc.hrl").
-include("../include/r_sip_headers.hrl").

% -include("../include/r_nksip.hrl").

-define(CURRSTATE, ?FORKING_STATE).

%% ====================================================================
%% API functions
%% ====================================================================

enter_by_invite(Args, State) ->
    do_start_replace(Args, State).

apply_200(State) ->
    do_fin_replace(State).

abort(SipReply, State) ->
    do_abort(SipReply, State).

%% ====================================================================
%% Internal functions
%% ====================================================================

% ================================
% Start when forking
% ================================

% @private
do_start_replace([ZCallId, ZReq, ZType], State) ->
    {ok,[Replaces|_]} = nksip_request:header("replaces", ZReq),
    [RepCallId|_] = binary:split(Replaces, <<$;>>, []),
    d(State, "Replaces ~p -> ~p", [ZCallId, RepCallId]),
    %
    case State of
        #state_forking{a=#side{callid=RepCallId}} ->
            do_start_replace_1(ZReq,ZType,State);
        _ ->
            d(State, " Refer error. Unknown CallId detected ~120p", [ZCallId]),
            ?ESG_UTILS:send_response(?InternalError("EB2B.RP. Unknown state combination"), ZReq, State),
            {next_state, ?FORKING_STATE, State}
    end.

% @private
do_start_replace_1(#sipmsg{call_id=ZCallId}=ZReq, ZType, State) ->
    FR = {replaces,ZReq,ZType},
    #state_forking{dialogid=DialogId,map=Map}=State1 = State#state_forking{forking_replaces=FR},
    ?DLG_STORE:push_dlg(ZCallId, {DialogId,self()}),
    StateX = case maps:get(last_response_full,Map) of
                 undefined -> State1;
                 LastRespF ->
                     Opts = ?FORKING_UTILS:fwd_response_headers_auto(LastRespF, [no_dialog]),
                     Opts1 = ?FORKING_UTILS:fwd_response_remoteparty_headers(LastRespF,State1) ++ Opts,
                     ?ESG_UTILS:send_response({180, Opts1}, ZReq, State),
                     State1
             end,
    {next_state, ?FORKING_STATE, StateX}.

% ================================
% Fin when dialog
% ================================

% when no forking replace
do_fin_replace(#?StateDlg{forking_replaces=undefined}=State) ->
    {next_state, ?DIALOG_STATE, State};

% when dhandle is not set yet
do_fin_replace(#?StateDlg{a=#side{dhandle=ADH},b=#side{dhandle=BDH}}=State)
  when ADH==undefined; BDH==undefined ->
    #?StateDlg{dialogid=DlgId}=State,
    Pid = self(),
    F = fun F(?DIALOG_STATE,#?StateDlg{a=#side{dhandle=_ADH},b=#side{dhandle=_BDH}}=StateData)
              when _ADH/=undefined,_BDH/=undefined ->
                 apply_200(StateData);
            F(StateName,StateData) ->
                 spawn(fun() -> timer:sleep(100),?ESG_DLG:apply_fun({DlgId,Pid},[F]) end),
                 {next_state, StateName, StateData}
        end,
    F(?DIALOG_STATE,State);

% when can/should apply forking replace
do_fin_replace(#?StateDlg{forking_replaces=FR}=State) ->
    case FR of
        {replaces,#sipmsg{call_id=ZCallId}=ZReq,ZType} ->
            State1 = State#?StateDlg{forking_replaces=undefined},
            ?REPLACING:enter_by_invite([ZCallId, ZReq, ZType], State1);
        _ -> {next_state, ?DIALOG_STATE, State}
    end.

% ================================
% Final on forking
% ================================

do_abort(SipReply, #state_forking{forking_replaces=FR}=State) ->
    case FR of
        {replaces,ZReq,_ZType} ->
            ?ESG_UTILS:send_response(SipReply, ZReq, State),
            State#state_forking{forking_replaces=undefined};
        _ -> State
    end.

% ================================
%
% ================================

%d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    #state_forking{dialogid=DlgId}=State,
    ?LOG("EB2B fsm ~p '~p':" ++ Fmt, [DlgId,?CURRSTATE] ++ Args).
