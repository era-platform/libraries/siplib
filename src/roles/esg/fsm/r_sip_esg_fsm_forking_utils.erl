%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Service utils of 'forking' state of B2BUA Dialog FSM
%%% @todo

-module(r_sip_esg_fsm_forking_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_headers.hrl").
-include("../include/r_sip_esg.hrl").
-include("../include/r_sip_mgc.hrl").

% -include("../include/r_nksip.hrl").

-define(CURRSTATE, 'forking').

%% ====================================================================
%% API functions
%% ====================================================================

%% -------------
generate_tag(#state_forking{usedtags=Tags}=State) ->
    Tag = ?ESG_UTILS:build_tag(),
    case lists:member(Tag, Tags) of
        false -> {Tag, State#state_forking{usedtags=[Tag|Tags]}};
        true -> generate_tag(State)
    end.

%% -------------------
%% Cancel active forks
%% -------------------
cancel_active_forks([],_,_) -> ok;
cancel_active_forks([Fork|Rest], Opts, State) ->
    #side_fork{rhandle=ReqHandle,
               cmnopts=CmnOpts}=Fork,
    ?FORKING_UTILS:cancel_timer(Fork),
    catch nksip_uac:cancel(ReqHandle, [async|Opts++CmnOpts]),
    % @todo account free side b
    cancel_active_forks(Rest, Opts, State).


%% -------------------
%% Send response to caller
%% -------------------

% #265
send_response_to_caller({SipCode,_}=SipReply, #state_forking{forking_replaces=FR}=State)
  when FR/=undefined, SipCode>=400 ->
    State1 = ?FORKING_REPLACING:abort(SipReply,State),
    send_response_to_caller(SipReply, State1);

% #265
send_response_to_caller({SipCode,_}=SipReply, #state_forking{forking_replaces=FR}=State)
  when FR/=undefined, SipCode=<199 ->
    State1 = ?FORKING_REPLACING:abort({180,[]},State),
    send_response_to_caller(SipReply, State1);

send_response_to_caller({SipCode, Opts}, State) ->
    #state_forking{a=#side{rhandle=ReqHandle,
                             localtag=ALTag,
                             localcontact=ALContact}}=State,
    Opts1 = [{to_tag, ALTag},
             {contact, ALContact}|Opts],
    d(State, "send_response_to_caller ~1000tp", [SipCode]),
    DlgId = get_dialogid(State),
    ?U:send_sip_reply(fun() ->
                          Result = nksip_request:reply({SipCode, Opts1}, ReqHandle),
                          ?LOG("EB2B fsm ~p '~p': send_response_to_caller result: ~1000tp", [DlgId, ?CURRSTATE, Result])
                      end),
    State.

%% -------------------
%% Fwd response headers
%% -------------------

% make opts to forward some headers (reason, warning)
fwd_response_headers_auto(undefined, Opts) -> Opts;
fwd_response_headers_auto(Resp, Opts) ->
    #sipmsg{class={resp,_,RPhrase}}=Resp,
    Opts1 = [{reason_phrase,?EU:to_binary(RPhrase)} | Opts], % #
    fwd_response_headers([?Reason,?Warning], Resp, Opts1).

% make opts to forward response headers
fwd_response_headers(_, undefined, Opts) -> Opts;
fwd_response_headers([], _Resp, Opts) -> Opts;
fwd_response_headers([H|Rest], Resp, Opts) ->
    case nksip_response:header(?EU:to_binary(string:to_lower(?EU:to_list(H))),Resp) of
        {ok,[_|_]=Vs} -> fwd_response_headers(Rest, Resp, Opts ++ lists:map(fun(V) -> {add, {H,V}} end, Vs));
        _ -> fwd_response_headers(Rest, Resp, Opts)
    end.

% make opts, containing remoteparty
fwd_response_remoteparty_headers(Resp, State) ->
    % @TODO @remoteparty
    case ?ESG_UTILS:extract_property(addRemoteParty1xx, false, State) of
        false -> [];
        true ->
            #sipmsg{to={BRUri,_Tag}}=Resp,
            RemoteParty = ?ESG_UTILS:build_remote_party_id(false, Resp, BRUri, State),
            ?U:remotepartyid_opts(RemoteParty,'1xx');
        {true,#uri{}=RemoteParty} ->
            ?U:remotepartyid_opts(RemoteParty,'1xx')
    end.

%% -------------------
%% Cancel timers
% -------------------
cancel_timer(#side_fork{rule_timer_ref=T1, resp_timer_ref=T2}=Fork) ->
    cancel_timer(T1),
    cancel_timer(T2),
    Fork#side_fork{rule_timer_ref=undefined,
                   resp_timer_ref=undefined};

cancel_timer(#state_forking{total_timer_ref=T}=State) ->
    cancel_timer(T),
    State#state_forking{total_timer_ref=undefined};

cancel_timer(undefined) -> undefined;
cancel_timer(T) when is_reference(T) -> erlang:cancel_timer(T).

%% -------------------
%% make side record by side_fork when switch to dialog state
%% -------------------
make_dlg_side(#side_fork{}=ForkInfo, BSide, Now, {Resp}) ->
    #sipmsg{to={BRUri,BRTag},
            contacts=BRContacts}=Resp,
    #sdp{}=BRSdp=?U:extract_sdp(Resp),
    %
    {ok,BDlgHandle} = nksip_dialog:get_handle(Resp),
    %
    BSide#side{rhandle=ForkInfo#side_fork.rhandle,
               dhandle=BDlgHandle,
               callid=ForkInfo#side_fork.callid,
               calldir=ForkInfo#side_fork.calldir,
               %
               remoteuri=BRUri,
               remotetag=BRTag,
               remotecontacts=BRContacts,
               remotesdp=BRSdp,
               %
               localuri=ForkInfo#side_fork.localuri,
               localtag=ForkInfo#side_fork.localtag,
               localcontact=ForkInfo#side_fork.localcontact,
               last_remote_party=?U:clear_uri(ForkInfo#side_fork.localuri),
               %
               starttime=ForkInfo#side_fork.starttime,
               answertime=Now}.

%% -------------------
%% make state_dlg by state_forking when switch to dialog state
%% -------------------
make_dlg_state(#state_forking{map=Map}=State, Now, {ASide,BSide,_Resp}) ->
    Map1 = maps:with([app,dlgnum,opts,dir,account,store_data,last_out_callid_idx],Map),
    #?StateDlg{dialogid = State#state_forking.dialogid,
               dialogidhash = State#state_forking.dialogidhash, % #370
               map = Map1#{init_callid => State#state_forking.acallid,
                           timer_migration_ref => undefined},
               use_media = State#state_forking.use_media,
               media = State#state_forking.media,
               forking_replaces = State#state_forking.forking_replaces,
               %
               stopfuns = State#state_forking.stopfuns,
               exitfuns = State#state_forking.exitfuns,
               %
               a = ASide,
               b = BSide,
               %
               starttime = State#state_forking.starttime,
               answertime = Now}.


%% -------------------
%% when one abonent is called, then like a proxy
%% -------------------
choose_best_result({G1,Code1}, {G2,Code2}) when G1==6, G2==6 ->
    choose_best_result_of(Code1, Code2, [603,606,600,604]);
choose_best_result({G1,Code1}, {G2,Code2}) when G2==5, G1==5 ->
    choose_best_result_of(Code1, Code2, [500,501,502,503,504,505,513,580]);
choose_best_result({G1,Code1}, {G2,Code2}) when G2==4, G1==4 ->
    choose_best_result_of(Code1, Code2, [401,407,415,420,484,
                                         400,402,403,404,405,406,409,410,411,412,413,414,416,417,
                                         421,422,423,424,428,429,430,433,436,437,438,439,
                                         470,480,481,482,483,485,486,487,488,489,491,493,494,
                                         408]);
choose_best_result({G1,_Code1}, {G2,Code2}) when G2==4, G1/=4 -> Code2;
choose_best_result({G1,Code1}, {G2,_Code2}) when G2/=4, G1==4 -> Code1;
choose_best_result({G1,_Code1}, {G2,Code2}) when G2==5, G1/=5 -> Code2;
choose_best_result({G1,Code1}, {G2,_Code2}) when G2/=5, G1==5 -> Code1;
choose_best_result({G1,_Code1}, {G2,Code2}) when G2==6, G1/=6 -> Code2;
choose_best_result({G1,Code1}, {G2,_Code2}) when G2/=6, G1==6 -> Code1.

choose_best_result_of(Code1, Code2, PriorityList) ->
    case lists:filter(fun(C) when C==Code1; C==Code2 -> true;
                         (_) -> false
                      end, PriorityList) of
        [] -> Code2;
        [Code1|_] -> Code1;
        [Code2|_] -> Code2
    end.

%% -------------------
%% Finalize dialog
%% -------------------
do_finalize_dialog(State) ->
    Now = os:timestamp(),
    #state_forking{b_act=BAct,
                   b_fin=BFin,
                   a=ASide,
                   acallid=ACallId}=State,
    State1 = ?FORKING_UTILS:cancel_timer(State),
    ASide1 = ASide#side{finaltime=Now},
    %
    ?FORKING_UTILS:cancel_active_forks(BAct,[],State),
    BCanc = [ForkInfo#side_fork{res={cancelled},
                                finaltime=Now,
                                rule_timer_ref=undefined,
                                resp_timer_ref=undefined} || ForkInfo <- BAct],
    BFin1 = BFin++BCanc,
    State2 = State1#state_forking{a=ASide1,
                                  b_act=[],
                                  b_fin=BFin1,
                                  finaltime=Now},
    % @todo account free side a
    % stop media context
    {ok,State3} = ?ESG_MEDIA:stop(State2),
    % delete links call-id, dlg-id
    ?DLG_STORE:unlink_dlg(ACallId),
    unlink_forks(BFin1),
    State3.

%% -------------------
%% Unlink forks' callids
%% -------------------
unlink_forks(Forks) ->
    lists:foldl(fun(#side_fork{callid=FCallId}, Acc) ->
                        case lists:member(FCallId, Acc) of
                            false -> ?DLG_STORE:unlink_dlg(FCallId), [FCallId|Acc];
                            true -> Acc
                        end end, [], Forks).

%% -------------------
%% Handle error of mgc/mg
%% -------------------
error_mgc(Reason, State) ->
    error_mgc(Reason, State, ?InternalError("EB2B.F. MGC error")).
error_mgc(Reason, State, SipReply) ->
    d(State, "MGC error: ~120p", [Reason]),
    do_final(SipReply, State).

%% -------------------
%% Finalized dialog and switch fsm to stopping state
%% -------------------
do_final(SipReply, State) ->
    State1 = send_response_to_caller(SipReply, State),
    do_final(State1).

do_final(State) ->
    State1 = do_finalize_dialog(State),
    return_stopping(State1).

%% -------------------
%% Switches to stopping state
%% -------------------
return_stopping(State) ->
    #state_forking{stopfuns=StopFuns}=State,
    lists:foreach(fun(F) when is_function(F) -> F() end, StopFuns),
    ?DIALOG:finish(State).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -----
d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    DlgId = get_dialogid(State),
    ?LOG("EB2B fsm ~p '~p':" ++ Fmt, [DlgId,?CURRSTATE] ++ Args).

%% -----
get_dialogid(#state_forking{dialogid=DlgId}) -> DlgId.

%% -----
reply_code({Code,_}) -> Code;
reply_code(SipReply) -> SipReply.