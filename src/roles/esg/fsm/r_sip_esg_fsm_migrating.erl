%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Presents 'migrating' state of B2BUA Dialog FSM
%%%      Acts on service message to make both reinvite to other Media-Gate
%%%        Ends with both final answers or timeout.
%%% @todo

-module(r_sip_esg_fsm_migrating).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([start/1,
         migration_timeout/2,
         sip_reinvite/2,
         sip_bye/2,
         uac_response/2,
         uas_dialog_response/2,
         call_terminate/2,
         stop_external/2,
         test/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_esg.hrl").
-include("../include/r_sip_mgc.hrl").

% -include("../include/r_nksip.hrl").

-define(CURRSTATE, 'migrating').
-define(TOTAL, 'total').
-define(RETRY, 'retry_side').
-define(KEY, 'migration').

%% ====================================================================
%% API functions
%% ====================================================================

% ----
start(State) ->
    d(State, "start migration to mg"),
    do_start(State).

% ----
stop_external(Reason, State) ->
    d(State, "stop_external(~p)", [Reason]),
    go_abort(stop_external, Reason, State).

% ----
test(State) ->
    d(State, "test"),
    {next_state, ?CURRSTATE, State}.

% ----
migration_timeout([?TOTAL], State) ->
    do_on_timeout(State);
migration_timeout([?RETRY, SideIdx], State) ->
    do_retry_side(SideIdx, State).

% ----
sip_reinvite([CallId, Req], State) ->
    % simply send prepared data, because of pending reinvite.
    % It solves problem of contradictional invites in double migration
    {ok,ReqHandle} = nksip_request:get_handle(Req),
    FReply491 = fun() -> ?U:send_sip_reply(fun() -> nksip_request:reply(?Pending("EB2B.Migrating"), ReqHandle) end) end,
    FReply200 = fun(ZSide,ZLSdp) ->
                        #side{localtag=ZLTag,
                              localcontact=ZLContact}=ZSide,
                        Opts = [{body, ZLSdp},
                                {to_tag, ZLTag},
                                {contact, ZLContact}],
                        ?U:send_sip_reply(fun() -> nksip_request:reply({200, Opts}, ReqHandle) end)
                end,
    case State of
        #?StateDlg{lstate=#{state:=?KEY,a:=#{callid:=CallId, lsdp:=ZLSdp, timer:=TimerRef}}, a=ZSide} when TimerRef/=undefined -> FReply200(ZSide,ZLSdp);
        #?StateDlg{lstate=#{state:=?KEY,b:=#{callid:=CallId, lsdp:=ZLSdp, timer:=TimerRef}}, b=ZSide} when TimerRef/=undefined -> FReply200(ZSide,ZLSdp);
        _ -> FReply491()
    end,
    {next_state, ?CURRSTATE, State}.

% ----
sip_bye([_CallId, _Req]=Args, State) ->
    d(State, "bye, CallId=~120p", [_CallId]),
    go_abort(sip_bye, Args, State).

% ----
uac_response([_Resp]=Args, State) ->
    #sipmsg{class={resp,SipCode,SipReason},
            cseq={CSeq,Method},
            call_id=CallId}=_Resp,
    d(State, "uac_response ~p:~p -> ~p ~p, CallId=~120p", [Method,CSeq,SipCode,SipReason,CallId]),
    do_on_uac_response(Args, State).

% ----
uas_dialog_response([_Resp], State) ->
    {next_state, ?CURRSTATE, State}.

% ----
call_terminate([Reason, _Call], State) ->
    #call{call_id=_CallId}=_Call,
    d(State, "call_terminate, CallId=~120p, Reason=~120p", [_CallId, Reason]),
    go_error("Call terminated", State).

%% ====================================================================
%% Internal functions
%% ====================================================================


%% =============================================================
%% Start reinvite
%% =============================================================

%% -----
%% start migration
%%
do_start(State) ->
    #?StateDlg{dialogid=DialogId,
               media=OldMedia}=State,
    % media
    <<B4:32/bitstring, _/bitstring>> = ?U:luid(),
    MSID = <<DialogId/bitstring, "_", B4/bitstring>>,
    {Mks,Res} = timer:tc(fun() -> ?MEDIA_B2B:re_start_media(MSID, OldMedia) end),
    d(State, " .. re prepare_media (~p ms)", [Mks/1000]),
    case Res of
        {ok, Media} ->
            start_reinvite(Media, State);
        {reply,_} ->
            d(State, "Error MGC"),
            go_error("Migration failed on MGC error", State)
    end.

%% ----
%% prepares reinvite to both sides
%%
start_reinvite(Media, State) ->
    #?StateDlg{a=#side{callid=ACallId}=A,
               b=#side{callid=BCallId}=B,
               media=OldMedia}=State,
    State1 = State#?StateDlg{media=Media,
                             lstate=#{state => ?KEY,
                                      timestart => os:timestamp(),
                                      timer_ref => erlang:send_after(?REINVITE_TIMEOUT, self(), {'migration_timeout', [?TOTAL]}),
                                      a => #{callid => ACallId},
                                      b => #{callid => BCallId},
                                      oldmedia => OldMedia,
                                      resp_count => 0,
                                      retry_count => 0}},
    {ResA, State2} = reinvite_side(A, 'a', State1),
    {ResB, State3} = reinvite_side(B, 'b', State2),
    case (ResA==error) orelse (ResB==error) of
        true ->
            go_error("Migration failed sending reinvite", State1);
        false ->
            d(State3, " -> switch to '~p'", [?CURRSTATE]),
            {next_state, ?MIGRATING_STATE, State3}
    end.

%% ----
%% after 491 pending and timeout
%%
do_retry_side(ZIdx, State) ->
    #?StateDlg{lstate=#{state:=?KEY, retry_count:=RetryCount}=LState}=State,
    ZSide = case LState of
                #{a:=#{idx:=ZIdx}} -> State#?StateDlg.a;
                #{b:=#{idx:=ZIdx}} -> State#?StateDlg.b
            end,
    case reinvite_side(ZSide, ZIdx, State#?StateDlg{lstate=LState#{retry_count:=RetryCount+1}}) of
        {error,State1} -> go_error("Migration failed sending reinvite", State1);
        {_,State1} -> {next_state, ?CURRSTATE, State1}
    end.

%% ----
%% reinvite operation
%%
reinvite_side(ZSide, ZIdx, State) ->
    #?StateDlg{media=Media,
               lstate=#{ZIdx:=ZMigr}=LState,
               map=#{opts:=DlgOpts}}=State,
    #side{remotetag=ZRTag,
          localtag=ZLTag,
          last_remote_party=ZLFrom}=ZSide,
    % media (make term's local), only gets sdp, no mgc connect
    {ok, Media1, ZLSdp} = ?MEDIA_B2B:re_reinvite_request(Media, ZRTag),
    State1 = State#?StateDlg{media=Media1},
    %
    AuthOpts = ?ESG_UTILS:build_auth_opts(ZIdx, DlgOpts),
    InviteOpts=[{from,?U:set_uri_tag(ZLFrom,ZLTag)},
                {body,ZLSdp},
                user_agent,
                auto_2xx_ack
               | AuthOpts],
    case send_reinvite_offer(InviteOpts, ZSide, State1) of
        error ->
            {error, State1};
        {ok,ReqHandle} ->
            ZMigr1 = ZMigr#{rhandle => ReqHandle,
                            idx => ZIdx,
                            ltag => ZSide#side.localtag,
                            lsdp => ZLSdp,
                            rsdp => undefined,
                            timer => undefined},
            State2 = State1#?StateDlg{lstate=LState#{ZIdx:=ZMigr1}},
            {ok, State2}
    end.

%% ----
%% send request
%%
send_reinvite_offer(InviteOpts, ZSide, State) ->
    #side{dhandle=DlgHandle,
          localtag=LTag}=ZSide,
    case catch nksip_uac:invite(DlgHandle, [async|InviteOpts]) of
        {'EXIT',Err} ->
            d(State, "send_reinvite_offer to LTag=~p, Caugth error=~120p", [LTag, Err]),
            error;
        {error,_R}=Err ->
            d(State, "send_reinvite_offer to LTag=~p, Error=~120p", [LTag, Err]),
            error;
        {async,ReqHandle} ->
            d(State, "send_reinvite_offer to LTag=~p", [LTag]),
            {ok,ReqHandle}
    end.

%% =============================================================
%% Handling timeout
%% =============================================================

% ----
do_on_timeout(State) ->
    d(State, "timeout"),
    go_error("Migration failed with timeout", State).

%% =============================================================
%% Handling UAC responses
%% =============================================================

% -----
do_on_uac_response([#sipmsg{cseq={_,'INVITE'}, call_id=CallId, from={_,LTag}}=Resp]=P,
                   #?StateDlg{lstate=#{state:=?KEY,a:=#{callid:=CallId, ltag:=LTag}}}=State) ->
    #sipmsg{class={resp,SipCode,_}}=Resp,
    d(State, "uac_response('INVITE') ~p from ~p", [SipCode, LTag]),
    on_invite_response(P, State);

do_on_uac_response([#sipmsg{cseq={_,'INVITE'}, call_id=CallId, from={_,LTag}}=Resp]=P,
                   #?StateDlg{lstate=#{state:=?KEY,b:=#{callid:=CallId, ltag:=LTag}}}=State) ->
    #sipmsg{class={resp,SipCode,_}, from={_,LTag}}=Resp,
    d(State, "uac_response('INVITE') ~p from ~p", [SipCode, LTag]),
    on_invite_response(P, State);

do_on_uac_response([#sipmsg{cseq={_,Method}}=Resp], State) ->
    #sipmsg{call_id=CallId, from={_,LTag}}=Resp,
    #?StateDlg{lstate=LState}=State,
    d(State, "uac_response('~p') skipped ~n\t~140p, ~140p~n\t~140p", [Method, CallId, LTag, LState]),
    {next_state, ?CURRSTATE, State}.

% -----
on_invite_response([#sipmsg{class={resp,SipCode,_}}=_Resp], State) when SipCode >= 100, SipCode < 200 ->
    {next_state, ?CURRSTATE, State};
on_invite_response([#sipmsg{class={resp,SipCode,_}}=_Resp]=P, State) when SipCode >= 200, SipCode < 300 ->
    on_invite_response_2xx(P,State);
on_invite_response([#sipmsg{class={resp,SipCode,_}}=_Resp], State) when SipCode >= 300, SipCode < 400 ->
    on_invite_response_error(State);
on_invite_response([#sipmsg{class={resp,SipCode,_}}=_Resp]=P, State) when SipCode == 491 ->
    on_invite_response_491(P,State);
on_invite_response([#sipmsg{class={resp,SipCode,_}}=_Resp]=P, State) when SipCode >= 400, SipCode < 700 ->
    on_invite_response_4xx_5xx_6xx(P,State);
on_invite_response([_Resp], State) ->
    on_invite_response_error(State).

%% ===================================
%% Negative UAC response
%% ===================================

on_invite_response_error(State) ->
    go_error("Migration failed with error reinvite response code", State).

on_invite_response_4xx_5xx_6xx([_Resp], State) ->
    go_error("Migration failed with 4xx-6xx reinvite response code", State).

on_invite_response_491([Resp],State) ->
    #sipmsg{call_id=CallId}=Resp,
    #?StateDlg{lstate=#{state:=?KEY}=LState}=State,
    Timeout = ?EU:random(300,700),
    Fretry = fun(ZIdx,ZMigr) ->
                    TimerRef = erlang:send_after(Timeout, self(), {'migration_timeout', [?RETRY, ZIdx]}),
                    State1 = State#?StateDlg{lstate=LState#{ZIdx:=ZMigr#{timer:=TimerRef}}},
                    {next_state, ?CURRSTATE, State1}
             end,
    case LState of
        #{retry_count:=Cnt} when Cnt>5 ->
            go_error("Migration failed with 4xx-6xx reinvite response code", State);
        #{a:=#{callid:=CallId, idx:=ZIdx}=ZMigr} -> Fretry(ZIdx,ZMigr);
        #{b:=#{callid:=CallId, idx:=ZIdx}=ZMigr} -> Fretry(ZIdx,ZMigr)
    end.

%% ===================================
%% Positive UAC response
%% ===================================

on_invite_response_2xx([Resp], State) ->
    %
    #?StateDlg{a=ASide,
               b=BSide,
               media=Media,
               lstate=#{state:=?KEY,
                        a:=A,
                        b:=B,
                        resp_count:=RespCount}=LState}=State,
    #sipmsg{call_id=CallId,
            to={_,ZRTag}}=Resp,
    #sdp{}=ZRSdp=?U:extract_sdp(Resp),
    % @todo media (update term's remote)
    case ?MEDIA_B2B:re_reinvite_response(Media, ZRTag, Resp) of
        {error, R} -> go_error(R, State);
        {ok, Media1} ->
            State1 = State#?StateDlg{media=Media1},
            %
            {ok,DlgHandle} = nksip_dialog:get_handle(Resp),
            State2 = case State1 of
                         #?StateDlg{a=#side{callid=CallId}=ASide, b=BSide} ->
                            State1#?StateDlg{lstate=LState#{a:=A#{rsdp:=ZRSdp},
                                                            resp_count:=RespCount+1},
                                             a=ASide#side{dhandle=DlgHandle,
                                                          remotesdp=ZRSdp}};
                         #?StateDlg{a=ASide, b=#side{callid=CallId}=BSide} ->
                            State1#?StateDlg{lstate=LState#{b:=B#{rsdp:=ZRSdp},
                                                            resp_count:=RespCount+1},
                                             b=BSide#side{dhandle=DlgHandle,
                                                          remotesdp=ZRSdp}}
                     end,
            %
            case RespCount of
                _N when _N < 1 -> {next_state, ?CURRSTATE, State2};
                _ -> done(State2)
            end
    end.

%% =============================================================
%% Services
%% =============================================================

% ----
cancel_timeout_timer(#?StateDlg{lstate=#{state:=?KEY,timer_ref:=TimerRef}}=_State) ->
    erlang:cancel_timer(TimerRef);
cancel_timeout_timer(_State) -> ok.

% ----
done(State) ->
    cancel_timeout_timer(State),
    #?StateDlg{lstate=#{state:=?KEY,
                        oldmedia:=OldMedia}}=State,
    ?WORKER:cast_workf(fun() -> ?MEDIA_B2B:stop_media(OldMedia) end),
    d(State, " -> switch to '~p'", [?DIALOG_STATE]),
    {next_state, ?DIALOG_STATE, State#?StateDlg{lstate=undefined}}.

% ----
go_error(Reason, State) ->
    go_abort(stop_error, Reason, State).

% ----
go_abort(Fun, Arg, #?StateDlg{lstate=#{state:=?KEY,oldmedia:=OldMedia}}=State) ->
    ?WORKER:cast_workf(fun() -> ?MEDIA_B2B:stop_media(OldMedia) end),
    cancel_timeout_timer(State),
    ?DIALOG:Fun(Arg, State);
go_abort(Fun, Arg, State) ->
    cancel_timeout_timer(State),
    ?DIALOG:Fun(Arg, State).

% -----
d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    #?StateDlg{dialogid=DlgId}=State,
    ?LOGSIP("B2B fsm ~p '~p':" ++ Fmt, [DlgId,?CURRSTATE] ++ Args).
