%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Presents 'refering' state of EB2B Dialog FSM
%%%       Acts self REFER handling. X - referring, Y - referred, Z - refer-to
%%%        Ends with final 2xx,4xx,5xx,6xx answer, error, reinvite or timeout.
%%% @todo

-module(r_sip_esg_fsm_refering).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([enter_by_refer/2,
         refer_timeout/2,
         reinvite_while_transfering/2,
         refer_while_transfering/2,
         sip_bye/2,
         uac_response/2,
         uas_dialog_response/2,
         call_terminate/2,
         stop_external/2,
         test/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_headers.hrl").
-include("../include/r_sip_esg.hrl").
-include("../include/r_sip_mgc.hrl").

% -include("../include/r_nksip.hrl").

-define(CURRSTATE, 'refering').

-define(FORK_TIMEOUT, 'fork').
-define(TOTAL_TIMEOUT, 'total').
-define(TimeoutInside, 185000). % more than 183 fork timeout
-define(TimeoutOutside, 185000). % more than 183 fork timeout

%% ====================================================================
%% API functions
%% ====================================================================

% ----
enter_by_refer([_CallId, _Req]=Args, State) ->
    d(State, "enter_by_refer, CallId=~120p", [_CallId]),
    do_start_refer(Args, State).

% ----
stop_external(Reason, State) ->
    d(State, "stop_external(~p)", [Reason]),
    to_stop(Reason, State).

% ----
test(State) ->
    d(State, "test"),
    {next_state, ?CURRSTATE, State}.

% ----
refer_timeout(Args, State) ->
    do_on_timeout(Args, State).

% ----
reinvite_while_transfering([CallId, Req]=Args, State) ->
    d(State, "reinvite while refering, CallId=~120p", [CallId]),
    case State of
        #?StateDlg{refer=#refer{ycallid=CallId}} ->
            % from y -> pending
                send_response(?Pending("EB2B. Refering"), Req, State),
                {next_state, ?CURRSTATE, State};
        #?StateDlg{refer=#refer{stage='wait_z', xcallid=CallId}} ->
            % from x when waiting z response -> stop call z, return to dialog
                RSdp = ?U:extract_sdp(Req),
                case ?M_SDP:get_audio_mode(RSdp) of
                    M when M==<<"sendonly">>; M==<<"recvonly">> ->
                        % 30.07.2020 mk axatel microsip hold after refer
                        send_response(?Pending("EB2B. Refering"), Req, State),
                        {next_state, ?CURRSTATE, State};
                    _ ->
                        State1 = cancel_timers(State),
                        % cancel to z
                        #?StateDlg{refer=#refer{fork=#side_fork{localtag=ZLTag}=ZFork}}=State1,
                        d(State1, " -> send cancel/bye to Z ~p (got x reinvite)", [ZLTag]),
                        send_cancel(ZFork, [{reason, {sip, 487, "EB2B.RF. Refer cancelled"}}]),
                        % goto session changing
                        ?SESSIONCHANGING:enter_by_reinvite(Args, State1)
                end;
        #?StateDlg{refer=#refer{stage='wait_y', xcallid=CallId, x=#side{dhandle=XDlgHandle}}} ->
            % from x when z already answered -> error to x, bye to x
                send_response(?CallLegTransDoesNotExist("EB2B. Refer already done"), Req, State),
                send_bye(XDlgHandle),
                {next_state, ?CURRSTATE, State};
        #?StateDlg{refer=#refer{stage='wait_y', zcallid=CallId}} ->
            % from z when wait y -> pending
                send_response(?Pending("EB2B. Wait refer response"), Req, State),
                {next_state, ?CURRSTATE, State}
    end.

% ----
refer_while_transfering([_CallId, Req]=_Args, State) ->
    d(State, "refer while refering, CallId=~120p", [_CallId]),
    send_response(?Pending("EB2B. Already refering"), Req, State),
    {next_state, ?CURRSTATE, State}.

% ----
sip_bye([CallId, Req]=Args, State) ->
    d(State, "bye, CallId=~120p", [CallId]),
    case State of
        #?StateDlg{refer=#refer{stage='wait_z', ycallid=CallId}} ->
            % bye from y on wait_z stage -> stop dialog
                % timers
                State1 = cancel_timers(State),
                % cancel to z
                #?StateDlg{refer=#refer{fork=ZFork}}=State1,
                send_cancel(ZFork, [{reason, {sip, 487, "EB2B.RF. Refer bye"}}]),
                % fwd bye
                ?DIALOG:sip_bye(Args, State1#?StateDlg{refer=undefined});
        #?StateDlg{refer=#refer{stage='wait_y', ycallid=CallId}} ->
            % bye from y on wait_y stage -> stop dialog
                % timers
                State1 = cancel_timers(State),
                % fwd bye
                ?DIALOG:sip_bye(Args, State1#?StateDlg{refer=undefined});
        #?StateDlg{refer=#refer{xcallid=CallId}=Refer} ->
            % bye from x on any stage -> stop notify, ok
                send_response({200,[]}, Req, State),
                State1 = State#?StateDlg{refer=Refer#refer{xnotify=false}},
                {next_state, ?CURRSTATE, State1};
        #?StateDlg{refer=#refer{stage='wait_y', zcallid=CallId}} ->
            % bye from z (only on wait_y stage) -> fwd bye, state is ready
                State1 = cancel_timers(State),
                ?DIALOG:sip_bye(Args, State1#?StateDlg{refer=undefined})
    end.

% ----
uac_response([_Resp]=Args, State) ->
    #sipmsg{class={resp,SipCode,SipReason},
            cseq={CSeq,Method},
            call_id=CallId}=_Resp,
    d(State, "uac_response ~p:~p -> ~p ~p, CallId=~120p", [Method,CSeq,SipCode,SipReason,CallId]),
    do_on_uac_response(Args, State).

% ----
uas_dialog_response([_Resp]=_Args, State) ->
    {next_state, ?CURRSTATE, State}.

% ----
call_terminate([_Reason, _Call], State) ->
    #call{call_id=CallId}=_Call,
    d(State, "call_terminate, CallId=~120p, Reason=~120p", [CallId, _Reason]),
    case State of
        #?StateDlg{a=#side{callid=CallId}} ->
            to_stop({error, "Call terminated"}, State);
        #?StateDlg{b=#side{callid=CallId}} ->
            to_stop({error, "Call terminated"}, State);
        _ ->
            d(State, "call_terminate skipped"),
            {next_state, ?CURRSTATE, State}
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% =============================================================
%% Start refer
%% =============================================================

%% ---
%% initiates on received refer
%%
do_start_refer([CallId, Req], State) ->
    d(State, "Refer CallId: ~p", [CallId]),
    %
    {_XRefBy,_XRefTo,_RefOpts}=RefP = ?U:parse_refer_headers(Req),
    P1 = [CallId, Req, RefP],
    %
    case State of
        #?StateDlg{a=#side{callid=CallId}=A, b=B, map=#{dir:=?DirOutside}} -> do_start_refer_inside(P1, {A,B}, State);
        #?StateDlg{a=#side{callid=CallId}=A, b=B, map=#{dir:=?DirInside}} -> do_start_refer_outside(P1, {A,B}, State);
        #?StateDlg{a=A, b=#side{callid=CallId}=B, map=#{dir:=?DirInside}} -> do_start_refer_inside(P1, {B,A}, State);
        #?StateDlg{a=A, b=#side{callid=CallId}=B, map=#{dir:=?DirOutside}} -> do_start_refer_outside(P1, {B,A}, State);
        _ ->
            d(State, " Refer error. Unknown CallId detected or invalid state ~120p", [CallId]),
            send_response(?InternalError("EB2B. Unknown CallId"), Req, State),
            to_dialog(State)
    end.

%% ---
%% initiates on received Z's 3xx invite response
%%
do_restart_refer(MoveTo, State) ->
    #?StateDlg{refer=#refer{fork=#side_fork{callid=ZCallId},xcallid=XCallId,xreq=XReq}}=State,
    {XRefBy,_XRefTo,RefOpts}=RefP = ?U:parse_refer_headers(XReq),
    RefP = {XRefBy,MoveTo,RefOpts},
    P = [XCallId, XReq, RefP],
    P1 = [ZCallId],
    %
    case State of
        #?StateDlg{a=#side{callid=XCallId}=A, b=B, map=#{dir:=?DirOutside}} -> do_start_refer_inside_0(P, P1, {A,B}, State);
        #?StateDlg{a=#side{callid=XCallId}=A, b=B, map=#{dir:=?DirInside}} -> do_start_refer_outside_0(P, P1, {A,B}, State);
        #?StateDlg{a=A, b=#side{callid=XCallId}=B, map=#{dir:=?DirInside}} -> do_start_refer_inside_0(P, P1, {B,A}, State);
        #?StateDlg{a=A, b=#side{callid=XCallId}=B, map=#{dir:=?DirOutside}} -> do_start_refer_outside_0(P, P1, {B,A}, State)
    end.

%% =============================================================
%% Start refer outside
%% =============================================================

%% ----
%% initial
%%
do_start_refer_outside([CallId,_,_]=P, {X,Y}, State) ->
    % create ZCallId
    #?StateDlg{dialogid=DialogId,
               map=#{dlgnum:=DlgNum,
                     last_out_callid_idx:=OutCallIdx}=Map}=State,
    ZCallId = ?ESG_UTILS:build_out_callid(DlgNum, OutCallIdx+1, CallId),
    ZDomain = maps:get(clusterdomain,maps:get(account,Map)),
    ?ESG_UTILS:log_callid(referred,State#?StateDlg.dialogid,ZCallId,OutCallIdx+1,X#side.callid,ZDomain), % #307, RP-415
    % store ZCallId to monitor
    ?DLG_STORE:push_dlg(ZCallId, {DialogId,self()}),
    ?MONITOR:append_fun(self(), fun() -> ?DLG_STORE:unlink_dlg(ZCallId) end),
    do_start_refer_outside_0(P, [ZCallId], {X,Y}, State#?StateDlg{map=Map#{last_out_callid_idx:=OutCallIdx+1}}).

%% ---
%% make route uri
%%
do_start_refer_outside_0(P, [ZCallId], P2, State) ->
    #?StateDlg{map=#{account:=AMap}}=State,
    Dom = maps:get(domain,AMap,undefined),
    PrA = maps:get(proxyaddr,AMap,undefined),
    PrP = maps:get(proxyport,AMap,5060),
    Tr = maps:get(transport,AMap,udp),
    %
    RouteUri = #uri{scheme=sip,
                    domain=case PrA of undefined -> Dom; <<>> -> Dom; _ -> PrA end,
                    port=case PrP of 0 -> 5060; _ -> PrP end,
                    opts=[{<<"transport">>,?EU:to_binary(Tr)}, <<"lr">>]},
    do_start_refer_outside_1(P, [ZCallId, RouteUri], P2, State).

%% ----
%% create media context
%%
do_start_refer_outside_1([_,Req,_]=P, [ZCallId, RouteUri], {#side{remotetag=XTag}=_X,_Y}=P2, State) ->
    case ?ESG_MEDIA:onrefer_z_request(State,XTag,RouteUri#uri.domain,true) of
        {error,Reason} ->
            R = case is_list(Reason) of true -> Reason; false -> "unknown" end,
            send_response(?InternalError("EB2B.RF.InOut. Media error req: " ++ R), Req, State),
            to_dialog(State);
        {ok, State1, ZLSdp} ->
            do_start_refer_outside_2(P, [ZCallId, RouteUri, ZLSdp], P2, State1)
    end.

%% ----
%% make invite options (outside cluster call)
%%
do_start_refer_outside_2([CallId, _, RefP]=P, [ZCallId, RouteUri, ZLSdp], {X,Y}, State) ->
    #side{localcontact=XLContact,
          last_remote_party=XLUri}=X,
    {_XRefBy, XRefTo, RefOpts} = RefP,
    %
    #?StateDlg{map=Map}=State,
    AMap = maps:get(account,Map),
    ZRToUri = ?U:clear_uri(XRefTo),
    ZLTag = ?ESG_UTILS:build_tag(),
    % @TODO @remoteparty for z on reinvite header 'from' when refering outside
    ZLFromUri = case maps:get(dir,Map) of
                    % ?DirOutside -> ?ESG_UTILS:build_inner_from_uri(false, XLUri, ZRToUri, AMap);
                    _ -> XLUri
                end,
    ZLFromUri1 = ZLFromUri#uri{ext_opts=[{<<"tag">>,ZLTag}]},
    ZLContact = XLContact, % @localcontact (same as previous!)
    ?STAT_FACADE:link_fork(CallId, ZCallId), % @stattrace
    ?STAT_FACADE:link_domain(ZCallId, maps:get(clusterdomain,AMap)), % RP-415
    Timeout = ?TimeoutOutside,
    %
    AuthOpts = ?ESG_UTILS:build_auth_opts_ext(AMap),
    CmnOpts = [{call_id,ZCallId},
               {from,ZLFromUri1},
               {to,ZRToUri},
               {contact,ZLContact},
               user_agent
              | AuthOpts],
    InviteOpts = [{body,ZLSdp},
                  auto_2xx_ack,
                  %record_route, % #244
                  {cseq_num,1}
                 | RefOpts],
    %
    ZFork=#side_fork{callid=ZCallId,
                     localuri=ZLFromUri,localtag=ZLTag,localcontact=ZLContact,
                     remoteuri=ZRToUri,
                     requesturi=ZRToUri,
                     rule_timeout=Timeout,
                     cmnopts=CmnOpts,
                     inviteopts=InviteOpts,
                     starttime=os:timestamp()},
    %
    Route = {route, ?U:unparse_uri(RouteUri)},
    do_start_refer_invite(P, [ZFork#side_fork{cmnopts=[Route|CmnOpts]}, {?SideRemote,?SideLocal}], {X,Y}, State).

%% =============================================================
%% Start refer inside
%% =============================================================

%% ----
%% find b2bua
%%
do_start_refer_inside([CallId, Req, _]=P, {X,Y}, State) ->
    #?StateDlg{dialogid=DialogId,
               map=#{dlgnum:=DlgNum,
                     last_out_callid_idx:=OutCallIdx}=Map}=State,
    ZCallId = ?ESG_UTILS:build_out_callid(DlgNum, OutCallIdx+1, CallId),
    ZDomain = maps:get(clusterdomain,maps:get(account,Map)),
    ?ESG_UTILS:log_callid(referred,State#?StateDlg.dialogid,ZCallId,OutCallIdx+1,X#side.callid,ZDomain), % #307, RP-415
    % find b2bua
    case ?SERVERS:find_responsible_b2bua_nostore(ZCallId) of
        false ->
            send_response(?ServiceUnavailable("EB2B.RF.OutIn. Responsible servers not found"), Req, State),
            to_dialog(State);
        RouteDomain ->
            ?DLG_STORE:push_dlg(ZCallId, {DialogId,self()}),
            ?MONITOR:append_fun(self(), fun() -> ?DLG_STORE:unlink_dlg(ZCallId) end),
            do_start_refer_inside_0(P, [ZCallId, RouteDomain], {X,Y}, State#?StateDlg{map=Map#{last_out_callid_idx:=OutCallIdx+1}})
    end.

%%
do_start_refer_inside_0(P, P1, P2, State) ->
    do_start_refer_inside_1(P, P1, P2, State).

%% ----
%% create media context
%%
do_start_refer_inside_1([_, Req, _]=P, [ZCallId, RouteDomain], {#side{remotetag=XTag}=_X,_Y}=P2, State) ->
    #?StateDlg{map=#{account:=AMap}}=State,
    SendReinvite = maps:get(reinvite,AMap,true),
    case ?ESG_MEDIA:onrefer_z_request(State,XTag,RouteDomain,SendReinvite) of
        {error,Reason} ->
            R = case is_list(Reason) of true -> Reason; false -> "unknown" end,
            send_response(?InternalError("EB2B.RF.OutIn. Media error req: " ++ R), Req, State),
            to_dialog(State);
        {ok, State1, ZLSdp} ->
            do_start_refer_inside_2(P, [ZCallId, RouteDomain, ZLSdp], P2, State1)
    end.

%% ----
%% make invite options (inside cluster call)
%%
do_start_refer_inside_2([CallId, _, RefP]=P, [ZCallId, RouteDomain, ZLSdp], {X,Y}, State) ->
    #?StateDlg{dialogid=DialogId,
               map=Map}=State,
    AMap = maps:get(account,Map),
    #side{localcontact=XLContact,% local internal
          last_remote_party=XLUri}=X, % local internal
    {_XRefBy, XRefTo, RefOpts} = RefP,
    %
    ZRToUri = ?U:clear_uri(XRefTo),
    ZLTag = ?ESG_UTILS:build_tag(),
    % @TODO @remoteparty for z on invite header 'from' when refering inside
    ZLFromUri1 = XLUri#uri{domain=maps:get(clusterdomain, AMap), % @todo esg from domain
                           ext_opts=[{<<"tag">>,ZLTag}]},
    ZLContact = XLContact, % @localcontact (same as previous!)
    ?STAT_FACADE:link_fork(CallId, ZCallId), % @stattrace
    ?STAT_FACADE:link_domain(ZCallId, maps:get(clusterdomain,AMap)), % RP-415
    Timeout = ?TimeoutInside,
    %
    AuthOpts = ?ESG_UTILS:build_auth_opts_int(AMap),
    CmnOpts = [{call_id,ZCallId},
               {from,ZLFromUri1},
               {to,ZRToUri},
               {contact,ZLContact},
               user_agent
              | AuthOpts],
    CmnOpts1 = case ?U:is_b2bmedia() of
                   false -> [{replace, {?B2BHeader, <<"media=0">>}} | CmnOpts];
                   _ -> CmnOpts
               end,
    %
    InviteOpts = [{body,ZLSdp},
                  auto_2xx_ack,
                  %record_route, % #244
                  {cseq_num,1},
                  {add,{?EsgDlgHeader,DialogId}} % RP-1575
                 | RefOpts],
    %
    ZFork=#side_fork{callid=ZCallId,
                     calldir=?DirOutside,
                     localuri=ZLFromUri1,localtag=ZLTag,localcontact=ZLContact,
                     remoteuri=ZRToUri,
                     requesturi=ZRToUri,
                     rule_timeout=Timeout,
                     cmnopts=CmnOpts1,
                     inviteopts=InviteOpts,
                     starttime=os:timestamp()},
    %
    Route = {route, ?U:unparse_uri(#uri{scheme=sip, domain=RouteDomain, opts=?CFG:get_transport_opts()++[<<"lr">>]})},
    do_start_refer_invite(P, [ZFork#side_fork{cmnopts=[Route|CmnOpts]}, {?SideLocal,?SideRemote}], {X,Y}, State).

%% =============================================================
%% Invite services
%% =============================================================

%% -----
%% send invite and initialize state
%%
do_start_refer_invite([CallId, Req, _], [Fork,{XType,YType}], {X,Y}, State) ->
    #sipmsg{event=Ev}=Req,
    #side_fork{callid=ZCallId,
               remoteuri=#uri{scheme=Scheme,user=User,domain=Domain}}=Fork,
    #side{callid=YCallId, localtag=YLTag}=Y,
    case send_invite(Fork, State) of
        {error,_Err} ->
            send_response(?InternalError("EB2B. Send invite error"), Req, State),
            to_dialog(State);
        {ok, Fork1} ->
            Refer = #refer{stage='wait_z',
                           fork=Fork1,
                           xcallid=CallId,
                           xreq=Req,
                           x=#side{dhandle=X#side.dhandle,
                                   localcontact=X#side.localcontact,
                                   localuri=X#side.localuri,
                                   localtag=X#side.localtag,
                                   remoteuri=X#side.remoteuri,
                                   remotetag=X#side.remotetag},
                           xnotify=case Ev of {<<"refer">>,_} -> true; _ -> false end,
                           xtype=XType,
                           ycallid=YCallId,
                           ylocaltag=YLTag,
                           ytype=YType,
                           zcallid=ZCallId,
                           startgs=?EU:current_gregsecond(),
                           applied_redirects=[{Scheme,User,Domain}]},
            %
            AnswerOpts = [{expires, ?REFER_TIMEOUT div 1000},
                           user_agent],
            send_response({202,AnswerOpts}, Req, State),
            d(State, " -> switch to '~p'", [?CURRSTATE]),
            {next_state, ?CURRSTATE, State#?StateDlg{refer=Refer}}
    end.

%% ----
%% sends invite to Z
%%
send_invite(Fork, State) ->
    #?StateDlg{map=#{app:=App}}=State,
    #side_fork{callid=ZCallId,
               requesturi=Uri,
               localtag=ZLTag,
               rule_timeout=Timeout,
               cmnopts=CmnOpts,
               inviteopts=InviteOpts}=Fork,
    %
    Now = os:timestamp(),
    %
    case catch nksip_uac:invite(App, Uri, [async|CmnOpts++InviteOpts]) of
        {'EXIT',Err} ->
            d(State, "invite_fork BLTag=~p, Caught error=~120p", [ZLTag, Err]),
            {error, Err};
        {error,_R}=Err ->
            d(State, "invite_fork BLTag=~p, Error=~120p", [ZLTag, Err]),
            {error, Err};
        {async,ReqHandle} ->
            d(State, "invite_fork BLTag=~p", [ZLTag]),
            % @todo account early side b
            Fork1 = Fork#side_fork{rhandle=ReqHandle,
                                   starttime=Now,
                                   rule_timer_ref=erlang:send_after(Timeout, self(), {refer_timeout, [?FORK_TIMEOUT, ZCallId, ZLTag]})},
            {ok, Fork1}
    end.

%% ----
%% sends notify event to X (containing Z response line)
%%
send_notify(_,_,#?StateDlg{refer=#refer{xnotify=false}}=State) -> {ok,State};
send_notify(Body, SubscrState, State) ->
    #?StateDlg{map=#{opts:=DlgOpts},
               refer=#refer{x=XSide,
                            xtype=XType,
                            startgs=StartGS}}=State,
    AuthOpts = ?ESG_UTILS:build_auth_opts(XType, DlgOpts),
    Opts = [{content_type, <<"message/sipfrag">>},
            {event, <<"refer">>},
            {subscription_state, case SubscrState of
                                     active -> {active, erlang:max(0, 180-(?EU:current_gregsecond()-StartGS))};
                                     terminated -> {terminated, noresource}
                                 end},
            {body, Body},
            %record_route, % #244
            user_agent
           | AuthOpts],
    #side{dhandle=XDlgHandle}=XSide,
    catch nksip_uac:notify(XDlgHandle, [async|Opts]),
    {ok,State}.

%% =============================================================
%% Handling UAC responses
%% =============================================================

%% ----
%% Timeout handlers
%%
do_on_timeout([?FORK_TIMEOUT, ZCallId, ZLTag], State) ->
    d(State, "timeout '~p' ZLTag=~120p, ZCallId=~120p", [?FORK_TIMEOUT, ZLTag, ZCallId]),
    #?StateDlg{refer=#refer{fork=#side_fork{localtag=ZLTag}=ZFork}}=State,
    send_cancel(ZFork, [{reason, {sip, 408, "EB2B.RF. Fork timeout"}}]),
    send_notify(?U:get_response_string(408,"Request Timeout"), terminated, State),
    to_dialog(State);
do_on_timeout([?TOTAL_TIMEOUT], State) ->
    d(State, "timeout '~p'", [?TOTAL_TIMEOUT]),
    #?StateDlg{refer=#refer{fork=ZFork}}=State,
    send_cancel(ZFork, [{reason, {sip, 408, "EB2B.RF. Dialog timeout"}}]),
    send_notify(?U:get_response_string(408,"Request Timeout"), terminated, State),
    to_dialog(State).

%% -----
%% UAC response handlers
%%
do_on_uac_response([#sipmsg{cseq={_,'INVITE'}, from={_,ZLTag}}=Resp]=P, #?StateDlg{refer=#refer{fork=#side_fork{localtag=ZLTag}}}=State) ->
    #sipmsg{class={resp,SipCode,_}}=Resp,
    d(State, "uac_response('INVITE') ~p from Z: ~p", [SipCode, ZLTag]),
    z_invite_response(P, State);

do_on_uac_response([#sipmsg{cseq={_,'INVITE'}, from={_,YLTag}}=Resp]=P, #?StateDlg{refer=#refer{ylocaltag=YLTag}}=State) ->
    #sipmsg{class={resp,SipCode,_}}=Resp,
    d(State, "uac_response('INVITE') ~p from Y: ~p", [SipCode, YLTag]),
    y_invite_response(P, State);

do_on_uac_response([#sipmsg{cseq={_,'NOTIFY'}}=Resp], State) ->
    #sipmsg{class={resp,SipCode,_}, from={_,BLTag}}=Resp,
    d(State, "uac_response('NOTIFY') ~p from ~p", [SipCode, BLTag]),
    {next_state, ?CURRSTATE, State};

do_on_uac_response([#sipmsg{cseq={_,Method}}=_Resp], State) ->
    d(State, "uac_response(~p) skipped", [Method]),
    {next_state, ?CURRSTATE, State}.

%% -----
%% Z invite response handlers
%%
z_invite_response([#sipmsg{class={resp,SipCode,Phrase}}=_Resp]=P, State) when SipCode >= 100, SipCode < 200 ->
    send_notify(?U:get_response_string(SipCode,Phrase), active, State),
    z_invite_response_1xx(P,State);

z_invite_response([#sipmsg{class={resp,SipCode,Phrase}}=_Resp]=P, State) when SipCode >= 200, SipCode < 300 ->
    send_notify(?U:get_response_string(SipCode,Phrase), terminated, State),
    z_invite_response_2xx(P,State);

z_invite_response([#sipmsg{class={resp,SipCode,_}}=Resp], State) when SipCode >= 300, SipCode < 400 ->
    z_invite_response_3xx(Resp, State);

z_invite_response([#sipmsg{class={resp,SipCode,Phrase}}=_Resp], State) when SipCode >= 400, SipCode < 700 ->
    send_notify(?U:get_response_string(SipCode,Phrase), terminated, State),
    to_dialog(State);

z_invite_response([#sipmsg{class={resp,SipCode,Phrase}}=_Resp], State) ->
    send_notify(?U:get_response_string(SipCode,Phrase), terminated, State),
    to_dialog(State).

%% -----
%% Y invite response handlers
%%
y_invite_response([#sipmsg{class={resp,SipCode,_}}=_Resp], State) when SipCode >= 100, SipCode < 200 ->
    {next_state, ?CURRSTATE, State};

y_invite_response([#sipmsg{class={resp,SipCode,_}}=_Resp]=P, State) when SipCode >= 200, SipCode < 300 ->
    y_invite_response_2xx(P,State);

y_invite_response([#sipmsg{class={resp,SipCode,_}}=_Resp], State) when SipCode >= 400, SipCode < 700 ->
    to_stop({error,"EB2B.RF. Reinvite referring response"}, State);

y_invite_response([_Resp], State) ->
    to_stop({error,"EB2B.RF. Reinvite referring response"}, State).

%% ===================================
%% Z UAC response
%% ===================================

%% ----
%% 3xx from Z
%%
z_invite_response_3xx(Resp, State) ->
    #sipmsg{contacts=[#uri{scheme=Scheme, user=User, domain=Domain}=_MoveTo|_]}=Resp,
    % account free side b
    AOR = {Scheme, User, Domain},
    #?StateDlg{refer=#refer{applied_redirects=Redirects}=Refer}=State,
    case lists:member(AOR,Redirects) of
        true ->
            d(State, "3xx-MoveTo ~120p, found cycled", [AOR]),
            send_notify(?U:get_response_string(482,<<"Loop Detected">>), terminated, State), % loop detected
            to_dialog(State);
        false ->
            d(State, "3xx-MoveTo ~p", [AOR]),
            State1 = State#?StateDlg{refer=Refer#refer{applied_redirects=[AOR|Redirects]}},
            MoveTo = #uri{scheme=Scheme, user=User, domain=Domain},
            do_restart_refer(MoveTo, State1)
    end.

%% ----
%% 1xx from Z
%%
z_invite_response_1xx([Resp], State) ->
    case ?U:extract_sdp(Resp) of
        #sdp{} ->
            case ?ESG_MEDIA:onrefer_z_response(State,Resp) of
                {error,_}=Err ->
                    % cancel to z
                    #?StateDlg{refer=#refer{fork=#side_fork{localtag=ZLTag}=ZFork}}=State,
                    d(State, " -> send cancel/bye to Z ~p (1xx media error: ~p)", [ZLTag, Err]),
                    send_cancel(ZFork, [{reason, {sip, 500, "EB2B.RF. Media error resp"}}]),
                    % stop a-b dialog or back to a-b dialog (on media error it's difficult)
                    send_notify(?U:get_response_string(500,"Server Internal Error"), terminated, State),
                    to_stop(Err, State);
                {ok, State1} ->
                    % reinvite to y
                    case State1 of
                        #?StateDlg{refer=#refer{ycallid=YCallId}, a=#side{callid=YCallId}=Y} -> ok;
                        #?StateDlg{refer=#refer{ycallid=YCallId}, b=#side{callid=YCallId}=Y} -> ok
                    end,
                    case reinvite_yside(Y, Resp, State1) of
                        {error,_}=Err ->
                            % cancel to z
                            #?StateDlg{refer=#refer{fork=#side_fork{localtag=ZLTag}=ZFork}}=State,
                            d(State, " -> send cancel/bye to Z ~p (1xx media error: ~p)", [ZLTag, Err]),
                            send_cancel(ZFork, [{reason, {sip, 500, "EB2B.RF. Media error resp"}}]),
                            % stop a-b dialog
                            send_notify(?U:get_response_string(500,"Server Internal Error"), terminated, State),
                            to_stop(Err,State1);
                        {ok,State2} ->
                            {next_state, ?CURRSTATE, State2}
                    end
            end;
        _ ->
            {next_state, ?CURRSTATE, State}
    end.

%% ----
%% 2xx from Z
%%
z_invite_response_2xx([Resp], State) ->
    State1 = cancel_timers(State),
    case ?ESG_MEDIA:onrefer_z_response(State1,Resp) of
        {error,_}=Err ->
            % bye to z
            #?StateDlg{refer=#refer{fork=#side_fork{localtag=ZLTag}}}=State,
            {ok,ZDlgHandle} = nksip_dialog:get_handle(Resp),
            d(State, " -> send bye to Z ~p (2xx media error ~p)", [ZLTag,Err]),
            send_bye(ZDlgHandle),
            % stop a-b dialog
            to_stop(Err, State); % already x notifier
        {ok, State2} ->
            #?StateDlg{refer=#refer{xcallid=XCallId,
                                    fork=ZFork}=Refer}=State2,
            % unlink x callid
            ?DLG_STORE:unlink_dlg(XCallId), % it's simple trimming
            % change x to z
            State3 = State2#?StateDlg{refer=Refer#refer{fork=undefined}}, % undefined if y remain sendrecv
            State4 = case State3 of
                         #?StateDlg{a=#side{callid=XCallId}} -> State3#?StateDlg{a=make_dlg_side(ZFork, Resp)};
                         #?StateDlg{b=#side{callid=XCallId}} -> State3#?StateDlg{b=make_dlg_side(ZFork, Resp)}
                     end,
            update_y(Resp, State4)
    end.

%
update_y(#sipmsg{to={ZRUri,_},from={ZLUri,_}}=ZResp, #?StateDlg{map=Map}=State) ->
    AMap = maps:get(account,Map),
    case maps:get(reinvite,AMap) of
        false ->
            to_dialog(State);
        true ->
            % remote-party-id option forward (response header, from+display(dc), options, rights)
            % @TODO @remoteparty for y on reinvite response header 'remote-party' when refering (future from)
            Dir = maps:get(dir,Map),
            RemoteParty = ?ESG_UTILS:build_remote_party_id(true, ZResp, ZRUri, State),
            FunUpYSide = fun(Y,FwdSide) ->
                                YLFromUri = case ?ESG_UTILS:get_side_type(FwdSide, Dir) of
                                                'outer' -> ?ESG_UTILS:build_inner_from_uri(true, RemoteParty, ZLUri, AMap); % y is ext
                                                'inner' -> ?ESG_UTILS:build_outer_from_uri(true, RemoteParty, ZLUri, AMap) % y is int
                                            end,
                                Y#side{localuri=YLFromUri,
                                       last_remote_party=YLFromUri} % not RemoteParty
                         end,
            State1 = case State of
                         #?StateDlg{refer=#refer{ycallid=YCallId}, a=#side{callid=YCallId}=Y} ->
                             ?ESG_UTILS:get_side_type(a, Dir),
                             State#?StateDlg{a=YSide1=FunUpYSide(Y,a)};
                         #?StateDlg{refer=#refer{ycallid=YCallId}, b=#side{callid=YCallId}=Y} ->
                             ?ESG_UTILS:get_side_type(b, Dir),
                             State#?StateDlg{b=YSide1=FunUpYSide(Y,b)}
                     end,
            % reinvite
            case reinvite_yside(YSide1, ZResp, State1) of
                {error,_}=Err ->
                    to_stop(Err,State1); % already x notified
                {ok,#?StateDlg{refer=Refer}=State2} ->
                    State3 = State2#?StateDlg{refer=Refer#refer{stage='wait_y'}},
                    {next_state, ?CURRSTATE, State3}
            end
    end.

%% ===================================
%% Reinvite Y referred side
%% ===================================

%% ---
reinvite_yside(YSide, ZResp, State) ->
    #side{remotetag=YRTag}=YSide,
    #sdp{}=ZRSdp=?U:extract_sdp(ZResp),
    case ?ESG_MEDIA:onrefer_y_request(State,YRTag,ZRSdp) of
        {error,_}=Err -> Err;
        {ok,State1,YLSdp} ->
            do_reinvite_yside(YSide,YLSdp,State1)
    end.

%%
do_reinvite_yside(YSide, YLSdp, State) ->
    #side{dhandle=DlgHandle,
          localtag=YLTag,
          last_remote_party=YLFrom}=YSide,
    #?StateDlg{map=#{opts:=DlgOpts},
               refer=#refer{ytype=YType}}=State,
    AuthOpts = ?ESG_UTILS:build_auth_opts(YType, DlgOpts),
    % @TODO @remoteparty for y on reinvite header 'from' when refering (by prev remote-party)
    CmnOpts = [{from, YLFrom#uri{ext_opts=[{<<"tag">>,YLTag}]}},
               user_agent
              | AuthOpts],
    InviteOpts = [{body,YLSdp},
                  auto_2xx_ack
                 | CmnOpts],
    case catch nksip_uac:invite(DlgHandle, [async|InviteOpts]) of
        {'EXIT',Err} ->
            d(State, "invite_referred =~p, Caught error=~120p", [YLTag, Err]),
            {error, Err};
        {error,_R}=Err ->
            d(State, "invite_referred =~p, error=~120p", [YLTag, Err]),
            Err;
        {async,_ReqHandle} ->
            d(State, "invite_referred =~p, ok", [YLTag]),
            {ok, State}
    end.

%% ===================================
%% Y UAC response
%% ===================================

%% ----
%% 2xx from Y
%%
y_invite_response_2xx([YResp],State) ->
    #?StateDlg{refer=Refer}=State,
    case ?ESG_MEDIA:onrefer_y_response(State,YResp) of
        {error,_}=Err ->
            to_stop(Err,State); % already x notified
        {ok,State1} ->
            case Refer of
                #refer{fork=undefined} ->
                    to_dialog(State1); % already x notified and state prepared
                _ ->
                    {next_state, ?CURRSTATE, State1}
            end
    end.

%% =============================================================
%% Services
%% =============================================================

%% ---
%% prepares #side{} from #side_fork
%%
make_dlg_side(#side_fork{}=ForkInfo, ZResp) ->
    #sipmsg{to={ZRUri,ZRTag},
            contacts=ZRContacts}=ZResp,
    #sdp{}=ZRSdp=?U:extract_sdp(ZResp),
    %
    {ok,ZDlgHandle} = nksip_dialog:get_handle(ZResp),
    %
    #side{rhandle=ForkInfo#side_fork.rhandle,
          dhandle=ZDlgHandle,
          callid=ForkInfo#side_fork.callid,
          %
          remoteuri=ZRUri,
          remotetag=ZRTag,
          remotecontacts=ZRContacts,
          remotesdp=ZRSdp,
          %
          localuri=ForkInfo#side_fork.localuri,
          localtag=ForkInfo#side_fork.localtag,
          localcontact=ForkInfo#side_fork.localcontact,
          last_remote_party=?U:clear_uri(ForkInfo#side_fork.localuri),
          %
          starttime=ForkInfo#side_fork.starttime,
          answertime=os:timestamp()}.

%% ---
%% send sip response on request
%%
send_response(SipReply, Req, State) ->
    {ok,Handle} = nksip_request:get_handle(Req),
    d(State, "send_response: ~1000tp", [reply_code(SipReply)]),
    DlgId = get_dialogid(State),
    ?U:send_sip_reply(fun() ->
                          Res = nksip_request:reply(SipReply, Handle),
                          ?LOG("EB2B fsm ~p '~p': send_response result: ~1000tp", [DlgId, ?CURRSTATE, Res])
                      end).

%% ---
%% switches to 'dialog' state
%%
to_dialog(State) ->
    d(State, " -> switch to '~p'", [?DIALOG_STATE]),
    %
    case State of
        #?StateDlg{refer=#refer{fork=#side_fork{callid=ZCallId}}} ->
            ?DLG_STORE:unlink_dlg(ZCallId);
        _ -> ok
    end,
    %
    State1 = cancel_timers(State),
    {next_state, ?DIALOG_STATE, State1#?StateDlg{refer=undefined}}.

%% ---
%% stops dialog
%%
to_stop(Reason, State) ->
    case State of
        #?StateDlg{refer=#refer{fork=#side_fork{callid=ZCallId}}} ->
            ?DLG_STORE:unlink_dlg(ZCallId);
        _ -> ok
    end,
    State1 = cancel_timers(State),
    State2 = State1#?StateDlg{refer=undefined},
    case Reason of
        {error, R} -> ?DIALOG:stop_error(R, State2);
        _ -> ?DIALOG:stop_external(Reason, State2)
    end.

%% ---
%% sends sip bye (to z, to x)
%%
send_bye(DlgHandle) ->
    Opts = [user_agent],
    catch nksip_uac:bye(DlgHandle, [async|Opts]).

%% ---
%% sends sip cancel to Z
%%
send_cancel(Fork, Opts) ->
    #side_fork{rhandle=ReqHandle,
               cmnopts=CmnOpts}=Fork,
    catch nksip_uac:cancel(ReqHandle, [async|Opts++CmnOpts]).

%% ---
%% stop timers
%%
cancel_timers(State) ->
    State1 = cancel_timeout_timer(State),
    State2 = cancel_fork_timer(State1),
    State2.
%%
cancel_timeout_timer(State) ->
    case State of
        #?StateDlg{refer=#refer{timer_ref=TimerRef}=Refer} when TimerRef /= undefined ->
            erlang:cancel_timer(TimerRef),
            State#?StateDlg{refer=Refer#refer{timer_ref=undefined}};
        _ -> State
    end.
%%
cancel_fork_timer(State) ->
    case State of
        #?StateDlg{refer=#refer{fork=#side_fork{rule_timer_ref=TimerRef}=Fork}=Refer} when TimerRef /= undefined ->
            erlang:cancel_timer(TimerRef),
            State#?StateDlg{refer=Refer#refer{fork=Fork#side_fork{rule_timer_ref=undefined}}};
        _ -> State
    end.

%% =============================================================
%% Internal functions
%% =============================================================

%% -----
d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    DlgId = get_dialogid(State),
    ?LOG("EB2B fsm ~p '~p':" ++ Fmt, [DlgId,?CURRSTATE] ++ Args).

%% -----
get_dialogid(#?StateDlg{dialogid=DlgId}) -> DlgId.

%% -----
reply_code({Code,_}) -> Code;
reply_code(SipReply) -> SipReply.