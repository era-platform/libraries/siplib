%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Presents 'replacing' state of EB2B Dialog FSM
%%%       Changes abonent, send by to excluded.
%%% @todo

-module(r_sip_esg_fsm_replacing).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([enter_by_invite/2,
         replace_timeout/2,
         sip_bye/2,
         uac_response/2,
         uas_dialog_response/2,
         call_terminate/2,
         stop_external/2,
         test/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_esg.hrl").
-include("../include/r_sip_mgc.hrl").
-include("../include/r_sip_headers.hrl").

% -include("../include/r_nksip.hrl").

-define(CURRSTATE, 'replacing').
-define(TOTALTIMEOUT, 'total').

%% ====================================================================
%% API functions
%% ====================================================================

enter_by_invite(Args, State) ->
    do_start_replace(Args, State).

% ----
stop_external(Reason, State) ->
    d(State, "stop_external(~p)", [Reason]),
    to_stop(Reason, State).

% ----
test(State) ->
    d(State, "test"),
    {next_state, ?CURRSTATE, State}.

% ----
replace_timeout(Args, State) ->
    do_on_timeout(Args, State).

% ----
sip_bye([_CallId, _Req]=Args, State) ->
    d(State, "bye, CallId=~120p", [_CallId]),
    State1 = cancel_timeout_timer(State),
    State2 = send_replace_error_response(State1),
    ?DIALOG:sip_bye(Args, State2#?StateDlg{replace=undefined}).

% ----
uac_response([_Resp]=Args, State) ->
    #sipmsg{class={resp,SipCode,SipReason},
            cseq={CSeq,Method},
            call_id=CallId}=_Resp,
    d(State, "uac_response ~p:~p -> ~p ~p, CallId=~120p", [Method,CSeq,SipCode,SipReason,CallId]),
    do_on_uac_response(Args, State).

% ----
uas_dialog_response([_Resp], State) ->
    {next_state, ?CURRSTATE, State}.

% ----
call_terminate([_Reason, _Call], State) ->
    #call{call_id=CallId}=_Call,
    d(State, "call_terminate, CallId=~120p, Reason=~120p", [CallId, _Reason]),
    case State of
        #?StateDlg{a=#side{callid=CallId}} ->
            State1 = cancel_timeout_timer(State),
            ?DIALOG:stop_error("Call terminated", State1#?StateDlg{replace=undefined});
        #?StateDlg{b=#side{callid=CallId}} ->
            State1 = cancel_timeout_timer(State),
            ?DIALOG:stop_error("Call terminated", State1#?StateDlg{replace=undefined});
        _ ->
            d(State, "call_terminate skipped"),
            {next_state, ?CURRSTATE, State}
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ==================================
%% Start replace
%% ==================================

do_start_replace([ZCallId, ZReq, _ZType]=P, State) ->
    {ok,[Replaces|_]} = nksip_request:header("replaces", ZReq),
    [RepCallId|_] = binary:split(Replaces, <<$;>>, []),
    d(State, "Replaces ~p -> ~p", [ZCallId, RepCallId]),
    %
    case State of
        #?StateDlg{a=#side{callid=RepCallId}=A, b=B} -> do_start_replace_1(P, 'b', {A,B}, State);
        #?StateDlg{a=A, b=#side{callid=RepCallId}=B} -> do_start_replace_1(P, 'a', {B,A}, State);
        _ ->
            d(State, " Refer error. Unknown CallId detected ~120p", [ZCallId]),
            ?ESG_UTILS:send_response(?InternalError("EB2B.RP. Unknown state combination"), ZReq, State),
            to_dialog(State)
    end.

%%
do_start_replace_1([ZCallId,_,_]=P, FwdSide, {X,Y}, State) ->
    % subtract x from media
    #side{remotetag=XRTag}=X,
    ZDomain = maps:get(clusterdomain,maps:get(account,State#?StateDlg.map)),
    ?ESG_UTILS:log_callid(replaces,State#?StateDlg.dialogid,ZCallId,X#side.callid,ZDomain), % #307, RP-415
    case ?ESG_MEDIA:onreplace_x_subtract(State, XRTag) of
        {ok, State1} ->
            do_start_replace_2(P, FwdSide, {X,Y}, State1);
        {error,_}=Err ->
            to_stop(Err, State)
    end.

%%
do_start_replace_2([ZCallId, _ZReq, _ZType]=P, FwdSide, {X,Y}, #?StateDlg{map=Map}=State) ->
    % store ZCallId to dialog's monitor
    #?StateDlg{dialogid=DialogId}=State,
    ?DLG_STORE:push_dlg(ZCallId, {DialogId,self()}),
    ?MONITOR:append_fun(self(), fun() -> ?DLG_STORE:unlink_dlg(ZCallId) end),
    % unlink XCallId
    #side{callid=XCallId}=X,
    ?DLG_STORE:unlink_dlg(XCallId),
    % switch if should reinvite
    AMap = maps:get(account,Map),
    case maps:get(reinvite,AMap) of
        true -> do_start_replace_reinvite_1(P, FwdSide, {X,Y}, State);
        false -> do_start_replace_mirror(P, FwdSide, {X,Y}, State)
    end.


%% --------------
%% when no reinvite, but mirror
%% --------------
%%
do_start_replace_mirror([ZCallId,ZReq,ZType]=P, FwdSide, {X,Y}, State) ->
    ZSide = build_zside(P, FwdSide, {X,Y}, State),
    State1 = State#?StateDlg{replace=#replace{xside=X,
                                              zcallid=ZCallId,
                                              zreq=ZReq,
                                              zside=ZSide,
                                              ztype=ZType}},
    do_attach_zside_on_mirror(State1).

%% --------------
%% when reinvite
%% --------------
%%
do_start_replace_reinvite_1([_,ZReq,_]=P, FwdSide, {X,Y}, State) ->
    #sipmsg{from={ZRUri,_}}=ZReq,
    % @TODO @remoteparty for y on reinvite header 'from' when replacing
    % RemoteParty = ?U:clear_uri(ZRUri),
    #?StateDlg{map=Map}=State,
    AMap = maps:get(account,Map),
    RemoteParty = case ?ESG_UTILS:get_side_type(FwdSide, maps:get(dir,Map)) of
                      'inner' -> ?ESG_UTILS:build_outer_from_uri(true, ?U:clear_uri(ZRUri), Y#side.remoteuri, AMap);
                      'outer' -> ?ESG_UTILS:build_inner_from_uri(true, ?U:clear_uri(ZRUri), Y#side.remoteuri, AMap)
                  end,
    YSide1 = Y#side{last_remote_party=RemoteParty},
    State1 = case FwdSide of
                 'a' -> State#?StateDlg{a=YSide1};
                 'b' -> State#?StateDlg{b=YSide1}
             end,
    % reinvite y by zsdp
    case reinvite_yside(YSide1, ZReq, FwdSide, State1) of
        {error,_}=Err ->
            to_stop(Err, State1);
        {ok, State2} ->
            do_start_replace_reinvite_2(P, FwdSide, {X,Y}, State2)
    end.

%%
do_start_replace_reinvite_2([ZCallId, ZReq, ZType]=P, FwdSide, {X,Y}, State) ->
    ZSide = build_zside(P, FwdSide, {X,Y}, State),
    % replace object
    Replace = #replace{xside=X,
                       zcallid=ZCallId,
                       zreq=ZReq,
                       zside=ZSide,
                       ztype=ZType,
                       ylocaltag=Y#side.localtag,
                       timer_ref=erlang:send_after(?REPLACING_TIMEOUT, self(), {replace_timeout,[]})},
    State1 = State#?StateDlg{replace=Replace},
    d(State, " -> switch to '~p'", [?CURRSTATE]),
    {next_state, ?CURRSTATE, State1}.

%% ----------
% @private
generate_tag(ZRTag, #?StateDlg{a=#side{localtag=ALTag,remotetag=ARTag},b=#side{localtag=BLTag,remotetag=BRTag}}=State) ->
    case ?ESG_UTILS:build_tag() of
        ALTag -> generate_tag(ZRTag, State);
        ARTag -> generate_tag(ZRTag, State);
        BLTag -> generate_tag(ZRTag, State);
        BRTag -> generate_tag(ZRTag, State);
        ZRTag -> generate_tag(ZRTag, State);
        Tag -> Tag
    end.

%% ----------
% @private
build_zside([ZCallId, ZReq, _ZType], _FwdSide, {X,_Y}, State) ->
    Now = os:timestamp(),
    % z remote
    {ok, ZReqHandle} = nksip_request:get_handle(ZReq),
    #sipmsg{from={ZRUri,ZRTag},
            to={#uri{ext_opts=ZLExtOpts}=ZLUri,_},
            contacts=ZRContacts}=ZReq,
    #sdp{}=ZRSdp=?U:extract_sdp(ZReq),
    % z local uri
    ZLTag1 = generate_tag(ZRTag,State),
    ZLUri1 = ZLUri#uri{ext_opts = ?U:store_value(<<"tag">>, ZLTag1, ZLExtOpts)},
    % z local contact
    #side{localcontact=XLContact}=X,
    ZLContact = XLContact,
    % z remote party (copied from x)
    ZRemoteParty = X#side.last_remote_party,
    % z side
    _ZSide=#side{rhandle=ZReqHandle,
                 dhandle=undefined,
                 callid=ZCallId,
                 calldir=?DirInside,
                 %
                 remoteuri=ZRUri,
                 remotetag=ZRTag,
                 remotecontacts=ZRContacts,
                 remotesdp=ZRSdp,
                 %
                 localuri=ZLUri1,
                 localtag=ZLTag1,
                 localcontact=ZLContact,
                 last_remote_party=ZRemoteParty,
                 %
                 starttime=Now,
                 answertime=undefined,
                 finaltime=undefined}.

%% ===================================
%% Reinvite Y referred side
%% ===================================

% ---
reinvite_yside(YSide, ZReq, FwdSide, State) ->
    #side{remotetag=YRTag}=YSide,
    #sdp{}=ZRSdp=?U:extract_sdp(ZReq),
    case ?ESG_MEDIA:onreplace_y_request(State,YRTag,ZRSdp) of
        {error,_}=Err -> Err;
        {ok,State1,YLSdp} ->
            do_reinvite_yside(YSide,YLSdp,FwdSide,State1)
    end.

%
do_reinvite_yside(YSide, YLSdp, FwdSide, State) ->
    #?StateDlg{map=Map}=State,
    #side{dhandle=DlgHandle,
          localtag=YLTag,
          last_remote_party=YLFrom}=YSide,
    % TODO: 2021-12-29 may be make ?ESG_UTILS:build_inner_from_uri / ?ESG_UTILS:build_outer_from_uri by ZRFrom
    %
    DlgOpts = maps:get(opts,Map),
    Dir = maps:get(dir,Map),
    YType = case {Dir,FwdSide} of
                {?DirInside,'a'} -> ?SideRemote;
                {?DirInside,'b'} -> ?SideLocal;
                {?DirOutside,'a'} -> ?SideLocal;
                {?DirOutside,'b'} -> ?SideRemote
            end,
    AuthOpts = ?ESG_UTILS:build_auth_opts(YType, DlgOpts),
    % @TODO @remoteparty for y on reinvite header 'from' when refering (by prev remote-party)
    CmnOpts = [{from,?U:set_uri_tag(YLFrom, YLTag)},
               user_agent
              | AuthOpts],
    InviteOpts = [{body,YLSdp},
                  %record_route, % #244
                  auto_2xx_ack
                 | CmnOpts],
    case catch nksip_uac:invite(DlgHandle, [async|InviteOpts]) of
        {'EXIT',Err} ->
            d(State, "invite_referred =~p, Caught error=~120p", [YLTag, Err]),
            {error, Err};
        {error,_R}=Err ->
            d(State, "invite_referred =~p, error=~120p", [YLTag, Err]),
            Err;
        {async,_ReqHandle} ->
            d(State, "invite_referred =~p, ok", [YLTag]),
            {ok, State}
    end.

%% =============================================================
%% Handling UAC responses
%% =============================================================

% ----
do_on_timeout(_Args, State) ->
    d(State, "timeout"),
    to_stop({error, "Replacing timeout"}, State).

% -----
do_on_uac_response([#sipmsg{cseq={_,'INVITE'}, from={_,YLTag}}=Resp]=P, #?StateDlg{replace=#replace{ylocaltag=YLTag}}=State) ->
    #sipmsg{class={resp,SipCode,_}}=Resp,
    d(State, "uac_response('INVITE') ~p from Y: ~p", [SipCode, YLTag]),
    y_invite_response(P, State);

do_on_uac_response([#sipmsg{cseq={_,Method}}=_Resp], State) ->
    d(State, "uac_response(~p) skipped", [Method]),
    {next_state, ?CURRSTATE, State}.

% -----
y_invite_response([#sipmsg{class={resp,SipCode,_}}=_Resp], State) when SipCode >= 100, SipCode < 200 ->
    {next_state, ?CURRSTATE, State};

y_invite_response([#sipmsg{class={resp,SipCode,_}}=_Resp]=P, State) when SipCode >= 200, SipCode < 300 ->
    y_invite_response_2xx(P,State);

y_invite_response([#sipmsg{class={resp,SipCode,_}}=_Resp]=P, State) when SipCode >= 400, SipCode < 700 ->
    y_invite_response_4xx_5xx_6xx(P,State);

y_invite_response([#sipmsg{class={resp,_,_}}=_Resp], State) ->
    y_invite_response_error(State).

%% ===================================
%% Y UAC response
%% ===================================

%% ----
%% error
%%
y_invite_response_error(State) ->
    to_stop({error,"EB2B.R. Reinvite replacing error response"}, State).

%% ----
%% 4xx, 5xx, 6xx
%%
y_invite_response_4xx_5xx_6xx([_Resp],State) ->
    to_stop({error,"EB2B.R. Reinvite replacing response"}, State).

%% ----
%% 2xx
%%
y_invite_response_2xx([Resp],State) ->
    % cancel timer
    State1 = cancel_timeout_timer(State),
    % change y media
    case ?ESG_MEDIA:onreplace_y_response(State1,Resp) of
        {error,_}=Err ->
            to_stop(Err,State1);
        {ok,State2} ->
            do_attach_zside_on_y_response(Resp, State2)
    end.

%% ==================================
%% Attach Z side
%% ==================================

%% ----------
%% when reinvited replace
do_attach_zside_on_y_response(YResp, State) ->
    #?StateDlg{replace=#replace{zreq=ZReq,
                                zside=#side{remotetag=ZTag},
                                xside=#side{remotetag=XTag}}}=State,
    case ?ESG_MEDIA:onreplace_z_add(State, {ZReq,YResp}, {ZTag,XTag}) of
        {error,Reason}=Err ->
            R = case is_list(Reason) of true -> Reason; false -> "unknown" end,
            ?ESG_UTILS:send_response(?InternalError("EB2B.RP. Media error req: " ++ R), ZReq, State),
            to_stop(Err, State);
        {ok, State1, ZLSdp} ->
            % @TODO @remoteparty for z on reinvite response header 'remote-party' when replacing (future from)
            do_attach_zside_1(ZLSdp, State1)
    end.

%% -----------
%% when mirrored replace
do_attach_zside_on_mirror(State) ->
    #?StateDlg{replace=#replace{zreq=ZReq,
                                zside=#side{remotetag=ZTag},
                                xside=#side{remotetag=XTag}}}=State,
    case ?ESG_MEDIA:onreplace_z_add_mirror(State, ZReq, {ZTag,XTag}) of
        {error,Reason}=Err ->
            R = case is_list(Reason) of true -> Reason; false -> "unknown" end,
            ?ESG_UTILS:send_response(?InternalError("EB2B.RP. Media error req: " ++ R), ZReq, State),
            to_stop(Err, State);
        {ok, State1, ZLSdp} ->
            do_attach_zside_1(ZLSdp, State1)
    end.

%% -----------
%%
do_attach_zside_1(ZLSdp, State) ->
    % send response to z
    #?StateDlg{replace=#replace{zside=ZSide,
                                xside=#side{callid=XCallId,
                                            last_remote_party=RemoteParty}}}=State,
    Opts = [{body, ZLSdp},
            user_agent],
    % @remoteparty
    Opts1 = case ?ESG_UTILS:extract_property(addRemoteParty2xx, false, State) of
                false -> Opts;
                _ -> ?U:remotepartyid_opts(RemoteParty,'2xx') ++ Opts
            end,
    % --
    State1 = send_response_to_z({200, Opts1}, State),
    % prepare state x -> z
    ZSide1 = ZSide#side{answertime=os:timestamp()},
    State2 = case State1 of
        #?StateDlg{a=#side{callid=XCallId}} -> State1#?StateDlg{a=ZSide1};
        #?StateDlg{b=#side{callid=XCallId}} -> State1#?StateDlg{b=ZSide1}
    end,
    do_bye_xside(State2).

% @private -- Send response to caller --
send_response_to_z({SipCode, Opts}, State) ->
    #?StateDlg{dialogid=DialogId,
               replace=#replace{ztype=ZType,
                                zside=#side{rhandle=ReqHandle,
                                            localtag=ZLTag,
                                            localcontact=ZLContact}}}=State,
    % @TODO @remoteparty for z on reinvite response header 'remote-party' when replacing (future from)
    Opts1 = [{to_tag, ZLTag},
             {contact, ZLContact}|Opts],
    % RP-1575
    Opts2 = case ZType of
                ?SideLocal -> [{add,{?EsgDlgHeader,DialogId}} | Opts1];
                ?SideRemote -> Opts1
            end,
    d(State, "send_response_to_z ~1000tp", [reply_code(SipCode)]),
    DlgId = get_dialogid(State),
    ?U:send_sip_reply(fun() ->
                          Res = nksip_request:reply({SipCode, Opts2}, ReqHandle),
                          ?LOG("EB2B fsm ~p '~p': send_response_to_z result: ~1000tp", [DlgId, ?CURRSTATE, Res])
                      end),
    State.

%%
do_bye_xside(State) ->
    % send bye to xside
    #?StateDlg{replace=#replace{xside=#side{dhandle=XDlgHandle}}}=State,
    Opts = [user_agent],
    catch nksip_uac:bye(XDlgHandle, [async|Opts]),
    % final
    do_finish(State).

%%
do_finish(State) ->
    State1 = State#?StateDlg{replace=undefined},
    to_dialog(State1).

%% ===================================
%%
%% ===================================

% ---
to_dialog(State) ->
    d(State, " -> switch to '~p'", [?DIALOG_STATE]),
    %
    State1 = cancel_timeout_timer(State),
    {next_state, ?DIALOG_STATE, State1#?StateDlg{replace=undefined}}.

% ---
to_stop(Reason, State) ->
    State1 = cancel_timeout_timer(State),
    State2 = State1#?StateDlg{replace=undefined},
    case Reason of
        {error, R} -> ?DIALOG:stop_error(R, State2);
        _ -> ?DIALOG:stop_external(Reason, State2)
    end.

% ---
send_replace_error_response(State) ->
    State.

% ---
cancel_timeout_timer(State) ->
    case State of
        #?StateDlg{replace=#replace{timer_ref=TimerRef}=Replace} when TimerRef /= undefined ->
            erlang:cancel_timer(TimerRef),
            State#?StateDlg{replace=Replace#replace{timer_ref=undefined}};
        _ -> State
    end.

%% ===================================
%% Internal functions
%% ===================================

%% -----
d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    DlgId = get_dialogid(State),
    ?LOGSIP("EB2B fsm ~p '~p':" ++ Fmt, [DlgId,?CURRSTATE] ++ Args).

%% -----
get_dialogid(#?StateDlg{dialogid=DlgId}) -> DlgId.

%% -----
reply_code({Code,_}) -> Code;
reply_code(SipReply) -> SipReply.