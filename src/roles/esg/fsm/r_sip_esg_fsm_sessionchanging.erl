%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Presents 'sessionchanging' state of B2BUA Dialog FSM.
%%%      Acts forward of reinvite. Ends with final answer or timeout.
%%% @todo

-module(r_sip_esg_fsm_sessionchanging).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([enter_by_reinvite/2,
         pending_reinvite/2,
         reinvite_timeout/2,
         sip_bye/2,
         uac_response/2,
         uas_dialog_response/2,
         call_terminate/2,
         stop_external/2,
         test/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_esg.hrl").
-include("../include/r_sip_mgc.hrl").

% -include("../include/r_nksip.hrl").

-define(CURRSTATE, 'sessionchanging').

%% ====================================================================
%% API functions
%% ====================================================================

% ----
enter_by_reinvite([_CallId, _Req]=P, State) ->
    d(State, "enter_by_reinvite, CallId=~120p", [_CallId]),
    do_start_reinvite(P, State).

% ----
pending_reinvite([_CallId, _Req], State) ->
    d(State, "pending_reinvite, CallId=~120p", [_CallId]),
    do_on_pending_reinvite(State).

% ----
stop_external(Reason, State) ->
    d(State, "stop_external(~p)", [Reason]),
    #?StateDlg{reinvite=#reinvite{xreq=XReq}}=State,
    send_response({603,[]}, XReq, State),
    ?DIALOG:stop_external(Reason, State#?StateDlg{reinvite=undefined}).

% ----
test(State) ->
    d(State, "test"),
    {next_state, ?CURRSTATE, State}.

% ----
reinvite_timeout([<<"total">>|_]=Args, State) ->
    do_on_uac_timeout(Args, State);

reinvite_timeout([<<"pending">>|_]=Args, State) ->
    do_on_pending_timeout(Args, State).

% ----
sip_bye([_CallId, _Req]=Args, State) ->
    d(State, "bye, CallId=~120p", [_CallId]),
    #?StateDlg{reinvite=#reinvite{xreq=XReq}}=State,
    send_response({603,[]}, XReq, State),
    ?DIALOG:sip_bye(Args, State#?StateDlg{reinvite=undefined}).

% ----
uac_response([_Resp]=Args, State) ->
    #sipmsg{class={resp,SipCode,SipReason},
            cseq={CSeq,Method},
            call_id=CallId}=_Resp,
    d(State, "uac_response ~p:~p -> ~p ~p, CallId=~120p", [Method,CSeq,SipCode,SipReason,CallId]),
    do_on_uac_response(Args, State).

% ----
uas_dialog_response([_Resp]=Args, State) ->
    #sipmsg{class={resp,SipCode,_},
            cseq={CSeq,Method},
            call_id=CallId}=_Resp,
    d(State, "uas_dialog_response ~p:~p -> ~p, CallId=~120p", [Method,CSeq,SipCode,CallId]),
    do_on_uas_dialog_response(Args, State).

% ----
call_terminate([Reason, _Call], State) ->
    #call{call_id=_CallId}=_Call,
    d(State, "call_terminate, CallId=~120p, Reason=~120p", [_CallId, Reason]),
    #?StateDlg{reinvite=#reinvite{xreq=XReq}}=State,
    send_response(?InternalError("EB2B.SC. Call terminated"), XReq, State),
    ?DIALOG:stop_error("Call terminated", State#?StateDlg{reinvite=undefined}).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% =============================================================
%% Start reinvite
%% =============================================================

do_start_reinvite([CallId, Req]=P, State) ->
    case State of
        #?StateDlg{a=#side{callid=CallId}=A, b=B} -> do_start_reinvite_1(P, 'b', {A,B}, State);
        #?StateDlg{a=A, b=#side{callid=CallId}=B} -> do_start_reinvite_1(P, 'a', {B,A}, State);
        _ ->
            send_reinvite_answer_at_start(internal_error, Req, State),
            {next_state, ?DIALOG_STATE, State}
    end.

% ----
do_start_reinvite_1([XCallId, XReq], FwdSide, {X,Y}, #?StateDlg{map=Map}=State) ->
    AMap = maps:get(account,Map),
    case maps:get(reinvite,AMap) of
        true ->
            Dir = maps:get(dir,Map),
            do_start_reinvite_2([XCallId, XReq, AMap, Dir], FwdSide, {X,Y}, State);
        false ->
            case ?ESG_MEDIA:mirror_reinvite_request(State, XReq) of
                {error,R} ->
                    go_error(R, XReq, State, "EB2B.SC. Media error");
                {ok, State1, XLSdp} ->
                    ReplyOpts=[{body,XLSdp},
                                  user_agent],
                    send_response({200, ReplyOpts}, XReq, State),
                    %
                    #sdp{}=XRSdp=?U:extract_sdp(XReq),
                    State2 = case State1 of
                                 #?StateDlg{a=#side{callid=XCallId}=ASide} ->
                                    State1#?StateDlg{a=ASide#side{remotesdp=XRSdp}};
                                 #?StateDlg{b=#side{callid=XCallId}=BSide} ->
                                    State1#?StateDlg{b=BSide#side{remotesdp=XRSdp}}
                             end,
                    %
                    {next_state, ?DIALOG_STATE, State2}
            end
    end.

% ----
do_start_reinvite_2([_XCallId, XReq, AMap, Dir], FwdSide, {X,Y}, State) ->
    %
    #sipmsg{to={XLUri,_},
            from={XRUri,_}}=XReq,
    #sdp{}=XRSdp=?U:extract_sdp(XReq),
    % media
    case ?ESG_MEDIA:fwd_reinvite_request_to_y(State, XReq) of
        {error,R} -> go_error(R, XReq, State, "EB2B.SC. Media error");
        {ok, State1, YLSdp} ->
            % @TODO @remoteparty for y on reinvite header 'from'
            % 'from' uri should be changed by account modifiers
            YLFromUri = ?U:clear_uri(XRUri),
            YLFromUri1 = case ?ESG_UTILS:get_side_type(FwdSide, Dir) of
                             'outer' -> ?ESG_UTILS:build_inner_from_uri(true, YLFromUri, XLUri, AMap); % y is ext
                             'inner' -> ?ESG_UTILS:build_outer_from_uri(true, YLFromUri, XLUri, AMap) % y is int
                         end,
            #side{localtag=YLTag}=Y,
            YSide1 = Y#side{localuri=YLFromUri1,
                            last_remote_party=YLFromUri1},
            FromOpts = [{from, ?U:set_uri_tag(YLFromUri1, YLTag)}],
            % reinvite
            AuthOpts = build_auth_opts(FwdSide, State),
            InviteOpts = [{body,YLSdp},
                          user_agent,
                          auto_2xx_ack
                         | AuthOpts ++ FromOpts],
            case send_reinvite_offer(InviteOpts, Y, State1) of
                error ->
                    send_reinvite_answer_at_start(internal_error, XReq, State1),
                    d(State1, " -> switch to '~p'", [?DIALOG_STATE]),
                    {next_state, ?DIALOG_STATE, State1};
                {ok,YReqHandle} ->
                    {ok,XReqHandle} = nksip_request:get_handle(XReq),
                    StateReinvite = #reinvite{xreq=XReq,
                                              fwdside=FwdSide,
                                              timestart=os:timestamp(),
                                              timer_ref=erlang:send_after(?REINVITE_TIMEOUT, self(), {reinvite_timeout, [<<"total">>,Y#side.callid,Y#side.localtag]}),
                                              xrhandle=XReqHandle,
                                              xcallid=X#side.callid,
                                              xltag=X#side.localtag,
                                              xrsdp=XRSdp,
                                              yrhandle=YReqHandle,
                                              ycallid=Y#side.callid,
                                              yltag=Y#side.localtag,
                                              ylsdp=YLSdp},
                    State2 = State1#?StateDlg{reinvite=StateReinvite},
                    State3 = case FwdSide of
                                 'a' -> State2#?StateDlg{a=YSide1};
                                 'b' -> State2#?StateDlg{b=YSide1}
                             end,
                    %
                    d(State3, " -> switch to '~p'", [?CURRSTATE]),
                    {next_state, ?CURRSTATE, State3}
            end
    end.

% ----
send_reinvite_offer(InviteOpts, Y, State) ->
    #side{dhandle=DlgHandle,
          localtag=LTag}=Y,
    case catch nksip_uac:invite(DlgHandle, [async|InviteOpts]) of
        {'EXIT',Err} ->
            d(State, "send_reinvite_offer to LTag=~p, Caught error=~120p", [LTag, Err]),
            error;
        {error,_R}=Err ->
            d(State, "send_reinvite_offer to LTag=~p, Error=~120p", [LTag, Err]),
            error;
        {async,ReqHandle} ->
            d(State, "send_reinvite_offer to LTag=~p", [LTag]),
            {ok,ReqHandle}
    end.

% ----
send_reinvite_answer_at_start(SipReply, Req, StateData) ->
    send_response(SipReply, Req, StateData).

build_auth_opts(Side, #?StateDlg{map=#{opts:=DlgOpts}}) ->
    ?ESG_UTILS:build_auth_opts(Side, DlgOpts).

% ----
% @private nksip's bug, when recv incoming opposite reinvite while offering, offer is not being sent.
%
do_on_pending_reinvite(State) ->
    erlang:send_after(1000, self(), {reinvite_timeout, [<<"pending">>]}),
    State.

do_on_pending_timeout(_Args, State) ->
    #?StateDlg{reinvite=#reinvite{offer_opts=InviteOpts, y=Y}=Re}=State,
    case send_reinvite_offer(InviteOpts, Y, State) of
        error ->
            send_reinvite_answer_at_start(internal_error, Re#reinvite.xreq, State),
            d(State, " -> switch to '~p'", [?DIALOG_STATE]),
            {next_state, ?DIALOG_STATE, State};
        {ok,YReqHandle} ->
            State1 = State#?StateDlg{reinvite=Re#reinvite{yrhandle=YReqHandle}},
            {next_state, ?CURRSTATE, State1}
    end.

%% =============================================================
%% Handling UAC responses
%% =============================================================

% ----
do_on_uac_timeout([CallId, YLTag], State) ->
    d(State, "uac_timeout YLTag=~120p, CallId=~120p", [YLTag, CallId]),
    #?StateDlg{reinvite=#reinvite{xreq=XReq}}=State,
    send_response(?RequestTimeout("EB2B.SC. Forward timeout"), XReq, State),
    ?DIALOG:stop_error("Reinvite failed with timeout", State#?StateDlg{reinvite=undefined}).

% -----
do_on_uac_response([#sipmsg{cseq={_,'INVITE'}, from={_,YLTag}}=Resp]=P, #?StateDlg{reinvite=#reinvite{yltag=YLTag}}=State) ->
    #sipmsg{class={resp,SipCode,_}}=Resp,
    d(State, "uac_response('INVITE') ~p from ~p", [SipCode, YLTag]),
    on_invite_response(P, State);

do_on_uac_response([#sipmsg{cseq={_,Method}}=_Resp], State) ->
    d(State, "uac_response('~p') skipped", [Method]),
    {next_state, ?CURRSTATE, State}.

% -----
on_invite_response([#sipmsg{class={resp,SipCode,_}}=_Resp], State) when SipCode >= 100, SipCode < 200 ->
    {next_state, ?CURRSTATE, State};
on_invite_response([#sipmsg{class={resp,SipCode,_}}=_Resp]=P, State) when SipCode >= 200, SipCode < 300 ->
    on_invite_response_2xx(P,State);
on_invite_response([#sipmsg{class={resp,SipCode,_}}=_Resp], State) when SipCode >= 300, SipCode < 400 ->
    on_invite_response_error(State);
on_invite_response([#sipmsg{class={resp,SipCode,_}}=_Resp]=P, State) when SipCode >= 400, SipCode < 700 ->
    on_invite_response_4xx_5xx_6xx(P,State);
on_invite_response([_Resp], State) ->
    on_invite_response_error(State).

%% ===================================
%% Negative UAC response
%% ===================================

on_invite_response_error(State) ->
    #?StateDlg{reinvite=#reinvite{xreq=XReq}}=State,
    cancel_timeout_timer(State),
    send_response(?InternalError("EB2B.SC. Forward response error"), XReq, State),
    ?DIALOG:stop_error("Reinvite failed with error response code", State#?StateDlg{reinvite=undefined}).

on_invite_response_4xx_5xx_6xx([#sipmsg{class={resp,SipCode,_SipReason}}=_Resp], State) ->
    #?StateDlg{reinvite=#reinvite{xreq=XReq}}=State,
    cancel_timeout_timer(State),
    SipCode1 = SipCode, % @todo map
    send_response({SipCode1,[]}, XReq, State),
    rollback(State, fun(State1) -> {next_state, ?DIALOG_STATE, State1#?StateDlg{reinvite=undefined}} end).

%% ===================================
%% Positive UAC response
%% ===================================

on_invite_response_2xx([#sipmsg{class={resp,SipCode,_}}=Resp], State) ->
    %
    #?StateDlg{a=ASide,
               b=BSide,
               reinvite=#reinvite{xreq=XReq,
                                  xcallid=XCallId}=Reinvite}=State,
    #sdp{}=YRSdp=?U:extract_sdp(Resp),
    % media
    case ?ESG_MEDIA:fwd_reinvite_response_to_x(State, Resp) of
        {error,R} -> go_error(R, State, "EB2B.SC. Media error");
        {ok, State1, XLSdp} ->
            % @TODO @remoteparty for x on reinvite response header 'remote-party' (future from when reinvite back side)
            SipCode1 = SipCode, % @todo map
            ReplyOpts=[{body,XLSdp},
                       user_agent],
            send_response({SipCode1, ReplyOpts}, XReq, State),
            %
            cancel_timeout_timer(State1),
            {ok,YDlgHandle} = nksip_dialog:get_handle(Resp),
            State2 = case State1 of
                         #?StateDlg{a=#side{callid=XCallId}=ASide, b=BSide} ->
                            State1#?StateDlg{a=ASide#side{remotesdp=Reinvite#reinvite.xrsdp},
                                             b=BSide#side{dhandle=YDlgHandle,
                                                          remotesdp=YRSdp}};
                         #?StateDlg{a=ASide, b=#side{callid=XCallId}=BSide} ->
                            State1#?StateDlg{b=BSide#side{remotesdp=Reinvite#reinvite.xrsdp},
                                             a=ASide#side{dhandle=YDlgHandle,
                                                          remotesdp=YRSdp}}
                     end,
            %
            {next_state, ?CURRSTATE, State2}
    end.

%% ===================================
%% UAS response handler
%% ===================================

% ----
do_on_uas_dialog_response([#sipmsg{class={resp,SipCode,_},
                                   cseq={_,'INVITE'},
                                   to={_,Tag}}=Resp],
                           #?StateDlg{reinvite=#reinvite{xltag=Tag}}=State)
  when SipCode >= 200, SipCode < 300 ->
    %
    #?StateDlg{reinvite=#reinvite{xcallid=XCallId}}=State,
    {ok,XDlgHandle} = nksip_dialog:get_handle(Resp),
    State1 = case State of
                 #?StateDlg{a=#side{callid=XCallId}=ASide} ->
                      State#?StateDlg{a=ASide#side{dhandle=XDlgHandle},
                                      reinvite=undefined};
                 #?StateDlg{b=#side{callid=XCallId}=BSide} ->
                      State#?StateDlg{b=BSide#side{dhandle=XDlgHandle},
                                      reinvite=undefined}
             end,
    %
    d(State1, " -> switch to '~p'", [?DIALOG_STATE]),
    {next_state, ?DIALOG_STATE, State1};
do_on_uas_dialog_response(_P, State) ->
    {next_state, ?CURRSTATE, State}.

%% =============================================================
%% Services
%% =============================================================

rollback(State, FOk) ->
    case ?ESG_MEDIA:rollback_reinvite(State) of
        {error,R} -> go_error(R, State, "EB2.SC. Media error rollback");
        {ok,State1} -> FOk(State1)
    end.

go_error(Reason, #?StateDlg{}=State, SipReasonText) ->
    #?StateDlg{reinvite=#reinvite{xreq=XReq}}=State,
    go_error(Reason, XReq, State, SipReasonText).
go_error(Reason, #sipmsg{}=XReq, State, SipReasonText) ->
    send_response(?InternalError(SipReasonText), XReq, State),
    ?DIALOG:stop_error(Reason, State#?StateDlg{reinvite=undefined}).

% ---
send_response(SipReply, Req, State) ->
    {ok,Handle} = nksip_request:get_handle(Req),
    d(State, "send_response: ~1000tp", [reply_code(SipReply)]),
    DlgId = get_dialogid(State),
    ?U:send_sip_reply(fun() ->
                          Res = nksip_request:reply(SipReply, Handle),
                          ?LOG("EB2B fsm ~p '~p': send_response result: ~1000tp", [DlgId, ?CURRSTATE, Res])
                      end).

% ---
cancel_timeout_timer(State) ->
    #?StateDlg{reinvite=#reinvite{timer_ref=TimerRef}}=State,
    erlang:cancel_timer(TimerRef).

%% =============================================================
%% Internal functions
%% =============================================================

%% -----
d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    DlgId = get_dialogid(State),
    ?LOG("EB2B fsm ~p '~p':" ++ Fmt, [DlgId,?CURRSTATE] ++ Args).

%% -----
get_dialogid(#?StateDlg{dialogid=DlgId}) -> DlgId.

%% -----
reply_code({Code,_}) -> Code;
reply_code(SipReply) -> SipReply.