%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc
%%% @todo
%%% -------------------------------------------------------------------
%%% Required Opts:
%%%    -
%%% Optional Opts:
%%%    certdir ("/priv/ssl")
%%% -------------------------------------------------------------------

-module(r_sip_esg_cb).
-author('Peter Bukashin <tbotc@yandex.ru>').

%% -------------------------------------------------------------------
%%
%% -------------------------------------------------------------------

-export([start/0, stop/0,
         handle_call/3, handle_cast/2, handle_info/2,
         %
         init/1,
         sni_fun/1,
         %sip_get_user_pass/4, sip_authorize/3,
         sip_route/5,
         % sip_register/2, sip_update/2,
         sip_invite/2, sip_reinvite/2, sip_cancel/3, sip_ack/2, sip_bye/2, sip_refer/2,
         % sip_subscribe/2, sip_resubscribe/2, sip_publish/2,
         sip_notify/2,
         sip_info/2, sip_message/2, sip_options/2,
         sip_dialog_update/3,
         sip_session_update/3]).

-compile([export_all,nowarn_export_all]).

%% ==========================================================================
%% Types
%% ==========================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_esg.hrl").
-include("../include/r_sip_mgc.hrl").

% -include("../include/r_nksip.hrl").

%% ==========================================================================
%% API functions
%% ==========================================================================

%% --------------------------------
%% @doc Starts a new SipApp, listening on port 5060 for udp and tcp and 5061 for tls,
%% and acting as a registrar.
%%
start() ->
    %
    {ok,AppOpts} = ?APP:get_opts(?MODULE),
    ?SIPSTORE:store_u('local_contact_port', ?U:parse_contact_port(AppOpts)),
    ?SIPSTORE:store_u('local_ports', ?U:parse_ports(AppOpts)),
    ?SIPSTORE:store_u('use_srtp', ?U:parse_use_srtp(AppOpts)),
    ?SIPSTORE:store_u('certdir', CertDir=?U:parse_certdir(AppOpts)),
    ?SIPSTORE:store_u('keypass', KeyPass=?U:parse_keypass(AppOpts)),
    ?SIPSTORE:store_u('sip_alg', SipAlgAddrs = ?U:parse_sip_alg(AppOpts)),
    %
    ?SipAlg:setup(SipAlgAddrs),
    %
    Plugins = [r_plug_b2bua,
               r_plug_localdomain,
               r_plug_bestinterface,
               r_plug_filter,
               r_plug_log,
               %
               nksip_uac_auto_auth,
               nksip_uac_auto_register
               % nksip_outbound,
               % nksip_timers,
               % nksip_refer,
               % nksip_100rel,
               % nksip_gruu
              ],
    CoreOpts = #{plugins => lists:filter(fun(false) -> false; (_) -> true end, Plugins),
                tls_certfile => filename:join(CertDir, "server.crt"),
                tls_keyfile => filename:join(CertDir, "server.key"),
                tls_sni_fun => fun(ServerName) -> ?MODULE:sni_fun(ServerName) end,
                tls_ciphers => ?U:ciphers(),
                sip_allow => "INVITE, ACK, CANCEL, BYE, REFER, NOTIFY, OPTIONS, INFO, MESSAGE", % REGISTER, SUBSCRIBE, PUBLISH, UPDATE
                sip_no_100 => not ?Auto100answer, % test
                transports => ?U:parse_transports(AppOpts)},
    CoreOptsUp = case KeyPass of [] -> CoreOpts; _ -> lists:append(CoreOpts,[{tls_password, KeyPass}]) end,
    nksip:start_link(?MODULE, CoreOptsUp),
    %
    ?ENV:set_env('sipapp', ?SIPAPP),
    ?ENV:set_env('sipappmodule', ?ESG),
    %
    ?SUPV:start_child({?FILTER_SUPV, {?FILTER_SUPV, start_link, [#{services => [<<"sip">>,<<"esg">>]}]}, permanent, 1000, supervisor, [?FILTER_SUPV]}),
    ?SUPV:start_child({?ESG_SUPV, {?ESG_SUPV, start_link, []}, permanent, 1000, supervisor, [?ESG_SUPV]}).

%% --------------------------------
%% @doc Stops the SipApp.
%%
stop() ->
    nksip:stop(?SIPAPP),
    %
    ?SUPV:terminate_child(?ESG_SUPV),
    ?SUPV:delete_child(?ESG_SUPV),
    %
    ?SUPV:terminate_child(?FILTER_SUPV),
    ?SUPV:delete_child(?FILTER_SUPV).

%% --------------------------------
%% @doc Updates role configuration opts
%%
update_opts(Opts) ->
    case ?U:check_update_opts(Opts) of
        restart -> restart;
        ok -> update_opts_2(Opts)
    end.
update_opts_2(Opts) ->
    {_,D1} = ?SIPSTORE:find_u('certdir'),
    D2 = ?U:parse_certdir(Opts),
    case D1 == D2 of
        false -> restart;
        true -> update_opts_3(Opts)
    end.
update_opts_3(Opts) ->
    {_,K1} = ?SIPSTORE:find_u('keypass'),
    K2 = ?U:parse_keypass(Opts),
    case K1 == K2 of
        false -> restart;
        true -> update_opts_4(Opts)
    end.
update_opts_4(Opts) ->
    ?EXT_REGSRV:update_opts(Opts),
    ok.

%% --------------------------------
%% @doc Notified when configuration was changed
-spec update_cfg() -> ok.
%% --------------------------------
update_cfg() ->
    {ok,[SipRole]} = ?ENV:get_env(siprole),
    {ok,AppOpts} = ?APP:get_opts(SipRole),
    ?SIPSTORE:store_u('sip_alg', SipAlgAddrs = ?U:parse_sip_alg(AppOpts)),
    ?SipAlg:setup(SipAlgAddrs).

%% ==================================================================================
%% Callback functions
%% ==================================================================================

%% --------------------------------------
%% TLS SNI
%% --------------------------------------
sni_fun(ServerName) ->
    ?ENVSNICERT:sni_fun(ServerName,#{module => ?MODULE,
                                     store => ?SIPSTORE}).

%% ----------------------------------------------------------------------------------
%% @doc SipApp Callback: Initialization.
%% ----------------------------------------------------------------------------------

init([]) ->
    %erlang:start_timer(?TIME_CHECK, self(), check_speed),
    %p:put(?SIPAPP, speed, []),
    {ok, #state{auto_check=false}}.


%% ----------------------------------------------------------------------------------
%% @doc SipApp Callback: Called to check user's password.
%% ----------------------------------------------------------------------------------

%% sip_get_user_pass(User, Realm, _Req, _Call) ->
%%     ?ACCOUNTS:get_password_lazy(User, Realm).

%% ----------------------------------------------------------------------------------
%% @doc SipApp Callback: Called to check if a request should be authorized.
%% ----------------------------------------------------------------------------------

%% sip_authorize(Auth, Req, Call) ->
%%     ?AUTH:sip_authorize(Auth, Req, Call).

%% ----------------------------------------------------------------------------------
%% @doc SipApp Callback: Called to decide how to route every new request.
%% ----------------------------------------------------------------------------------

% The request is for one of our users or a SUBSCRIBE
sip_route(_Scheme, _User, <<"224.0.1.75">>, _Req, _Call) -> block; % auto provision broadcast
sip_route(Scheme, User, Domain, Req, Call) ->
    #call{call_id=CallId}=Call,
    #sipmsg{cseq={_,Method}}=Req,
    % -------
    ?U:start_monitor_simple(self(), "Sip esg call"),
    % -------
    ?LOGSIP("sip_route ~120p, ~120p, ~120p", [Method, self(), CallId]),
    ?STAT_FACADE:incoming_request(Req), % @stattrace
    ?STAT_FACADE:link_domain_filter(Method,Req#sipmsg.call_id,(element(1,Req#sipmsg.to))#uri.domain), % RP-415
    case ?U:is_local_ruri(Req) of
        false -> route_outside_domain(Scheme, User, Domain, Req, Call);
        true -> route_inside_domain(Scheme, User, Domain, Req, Call)
    end.

%% ----------------------
route_outside_domain(_Scheme, _UserR, _DomainR, Req, _Call) ->
    Method = nksip_request:method(Req),
    case Method of
        {ok, 'REGISTER'} -> {reply, ?Forbidden("EB2B. Router disabled")};
        {ok, 'SUBSCRIBE'} -> {reply, ?Forbidden("EB2B. Router disabled")};
        {ok, 'PUBLISH'} -> {reply, ?Forbidden("EB2B. Router disabled")};
        {ok, 'UPDATE'} -> {reply, ?Forbidden("EB2B. Router disabled")};

        {ok, 'INVITE'} -> process;
        {ok, 'REFER'} -> process;
        {ok, 'NOTIFY'} -> process;

        {ok, 'INFO'} -> process;
        {ok, 'MESSAGE'} -> process;
        {ok, 'ACK'} -> process;
        {ok, 'BYE'} -> process;
        {ok, 'CANCEL'} -> process;
        {ok, 'OPTIONS'} -> process;

        _ -> process
    end.

%% ----------------------
route_inside_domain(_Scheme, _User, _Domain, Req, _Call) ->
    Method = nksip_request:method(Req),
    case Method of
        {ok, 'REGISTER'} -> fail(Req), {reply, ?MethodNotAllowed("EB2B")};
        % ----------------
        {ok, 'INVITE'} -> process;
        {ok, 'CANCEL'} -> process;
        {ok, 'ACK'} -> process;
        {ok, 'BYE'} -> process;
        % ----------------
        {ok, 'REFER'} -> process;
        % ----------------
        {ok, 'NOTIFY'} -> process;
        {ok, 'SUBSCRIBE'} -> fail(Req), {reply, ?MethodNotAllowed("EB2B")};
        {ok, 'PUBLISH'} -> fail(Req), {reply, ?MethodNotAllowed("EB2B")};
        % ----------------
        {ok, 'OPTIONS'} -> process;
        {ok, 'INFO'} ->    process;
        {ok, 'MESSAGE'} -> process;
        % ----------------
        {ok, 'UPDATE'} -> fail(Req), {reply, ?MethodNotAllowed("EB2B")};
        % ----------------
        {ok, _M} ->    process
    end.

%% ----------------------------------------------------------------------------------
%% dialog_update
%% ----------------------------------------------------------------------------------

-spec(sip_dialog_update(DialogStatus, Dialog::nksip:dialog(), Call::nksip:call()) -> ok
    when DialogStatus :: start | target_update | stop |
                     {invite_status, nksip_dialog:invite_status() | {stop, nksip_dialog:stop_reason()}} |
                     {invite_refresh, SDP::nksip_sdp:sdp()} |
                     {subscription_status, nksip_subscription:status(), nksip:subscription()}).

sip_dialog_update(_, _Dialog, _Call) ->
    ok.

%% ----------------------------------------------------------------------------------
%% session_update
%% ----------------------------------------------------------------------------------
-spec(sip_session_update(SessionStatus, Dialog::nksip:dialog(), Call::nksip:call()) -> ok
    when SessionStatus :: {start, Local, Remote} | {update, Local, Remote} | stop,
                      Local::nksip_sdp:sdp(), Remote::nksip_sdp:sdp()).

sip_session_update(_, _Dialog, _Call) ->
    ok.

%% ----------------------------------------------------------------------------------
%% INVITE
%% ----------------------------------------------------------------------------------
-spec(sip_invite(Request::nksip:request(), Call::nksip:call())
      -> {reply, nksip:sipreply()} | noreply).

sip_invite(Req, _Call) ->
    #sipmsg{call_id=CallId}=Req,
    AOR = extract_ruri(Req),
    ?LOGSIP("sip_invite ~1000tp, ~1000tp, ~1000tp", [self(), CallId, AOR]),
    case ?U:extract_sdp(Req) of
        undefined ->
            ?FILTER:check_block_response_malware(no_sdp, Req, {reply, ?MediaNotSupported("EB2B. No SDP")}); % TODO RP-956
        #sdp{} ->
            ?ESG_ROUTER:sip_invite(AOR,Req)
    end.

%% ----------------------------------------------------------------------------------
%% reINVITE
%% ----------------------------------------------------------------------------------
-spec(sip_reinvite(Request::nksip:request(), Call::nksip:call())
      -> {reply, nksip:sipreply()} | noreply).

sip_reinvite(Req, _Call) ->
    #sipmsg{call_id=CallId}=Req,
    AOR = extract_ruri(Req),
    ?LOGSIP("sip_reinvite ~120p, ~120p, ~120p", [AOR, self(), CallId]),
    case ?U:extract_sdp(Req) of
        undefined -> ?FILTER:check_block_response_malware(no_sdp, Req, {reply, ?MediaNotSupported("EB2B. No SDP")});
        #sdp{} ->
            case ?ESG_DLG:pull_fsm_by_callid(CallId) of
                false -> ?FILTER:check_block_response_malware(no_dialog, Req, {reply, ?CallLegTransDoesNotExist("EB2B. Unknown Dialog")});
                DlgX ->
                    ?U:send_100_trying(Req),
                    ?ESG_DLG:sip_reinvite(DlgX, [CallId, Req]),
                    noreply
            end end.

%% ----------------------------------------------------------------------------------
%% CANCEL
%% ----------------------------------------------------------------------------------
-spec(sip_cancel(InviteRequest::nksip:request(), Request::nksip:request(), Call::nksip:call()) -> ok).

sip_cancel(InviteReq, Req, Call) ->
    #call{call_id=CallId}=Call,
    ?LOGSIP("sip_cancel ~120p, ~120p", [self(), CallId]),
    case ?ESG_DLG:pull_fsm_by_callid(CallId) of
        false -> ok;
        DlgX -> ?ESG_DLG:sip_cancel(DlgX, [CallId, Req, InviteReq]), ok
    end.

%% ----------------------------------------------------------------------------------
%% ACK
%% ----------------------------------------------------------------------------------
-spec(sip_ack(Request::nksip:request(), Call::nksip:call()) -> ok).

sip_ack(Req, Call) ->
    #call{call_id=CallId}=Call,
    ?LOGSIP("sip_ack ~120p, ~120p", [self(), CallId]),
    case ?ESG_DLG:pull_fsm_by_callid(CallId) of
        false -> ok;
        DlgX ->
            ?ESG_DLG:sip_ack(DlgX, [CallId, Req]),
            ok
    end.

%% ----------------------------------------------------------------------------------
%% BYE
%% ----------------------------------------------------------------------------------
-spec(sip_bye(Request::nksip:request(), Call::nksip:call()) ->
        {reply, nksip:sipreply()} | noreply).

sip_bye(Req, Call) ->
    #call{call_id=CallId}=Call,
    ?LOGSIP("sip_bye ~120p, ~120p", [self(), CallId]),
    case ?ESG_DLG:pull_fsm_by_callid(CallId) of
        false -> {reply, ok};
        DlgX ->
            ?ESG_DLG:sip_bye(DlgX, [CallId, Req]),
            noreply
    end.

%% ----------------------------------------------------------------------------------
%% REFER
%% ----------------------------------------------------------------------------------
-spec(sip_refer(Request::nksip:request(), Call::nksip:call()) ->
    {reply, nksip:sipreply()} | noreply).

sip_refer(Req, Call) ->
    #call{call_id=CallId}=Call,
    ?LOGSIP("sip_refer ~120p, ~120p", [self(), CallId]),
    case ?ESG_DLG:pull_fsm_by_callid(CallId) of
        false -> ?FILTER:check_block_response_malware(no_dialog, Req, {reply, ?InternalError("EB2B. Unknown dialog")});
        DlgX ->
            ?U:send_100_trying(Req),
            ?ESG_DLG:sip_refer(DlgX, [CallId, Req]),
            noreply
    end.

%% ----------------------------------------------------------------------------------
%% UPDATE
%% ----------------------------------------------------------------------------------
%% -spec(sip_update(Req::nksip:request(), Call::nksip:call()) ->
%%     {reply, nksip:sipreply()} | noreply).
%%
%% sip_update(Req, _Call) ->
%%     ?LOGSIP("sip_update"),
%%     ?FILTER:check_block_response_malware(method_not_allowed, Req, {reply, ?MethodNotAllowed("EB2B")}).

%% ----------------------------------------------------------------------------------
%% SUBSCRIBE
%% ----------------------------------------------------------------------------------
%% -spec(sip_subscribe(Request::nksip:request(), Call::nksip:call()) ->
%%         {reply, nksip:sipreply()} | noreply).
%%
%% sip_subscribe(Req, Call) ->
%%     #call{call_id=CallId}=Call,
%%     ?LOGSIP("sip_subscribe ~120p, ~120p", [self(), CallId]),
%%     ?FILTER:check_block_response_malware(method_not_allowed, Req, {reply, ?MethodNotAllowed("EB2B")}).

%% ----------------------------------------------------------------------------------
%% reSUBSCRIBE
%% ----------------------------------------------------------------------------------
%% -spec(sip_resubscribe(Request::nksip:request(), Call::nksip:call()) ->
%%         {reply, nksip:sipreply()} | noreply).
%%
%% sip_resubscribe(Req, Call) ->
%%     #call{call_id=CallId}=Call,
%%     ?LOGSIP("sip_resubscribe ~120p, ~120p", [self(), CallId]),
%%     ?FILTER:check_block_response_malware(method_not_allowed, Req, {reply, ?MethodNotAllowed("EB2B")}).

%% ----------------------------------------------------------------------------------
%% NOTIFY
%% ----------------------------------------------------------------------------------
-spec(sip_notify(Request::nksip:request(), Call::nksip:call()) ->
        {reply, nksip:sipreply()} | noreply).

sip_notify(Req, Call) ->
    #call{call_id=CallId}=Call,
    ?LOGSIP("sip_notify ~120p, ~120p", [self(), CallId]),
    case ?ESG_DLG:pull_fsm_by_callid(CallId) of
        false -> ?FILTER:check_block_response_malware(no_dialog, Req, {reply, ?Forbidden("EB2B. Unknown dialog")});
        DlgX ->
            ?ESG_DLG:sip_notify(DlgX, [CallId, Req]),
            noreply
    end.

%% ----------------------------------------------------------------------------------
%% PUBLISH
%% ----------------------------------------------------------------------------------
%% -spec(sip_publish(Request::nksip:request(), Call::nksip:call()) ->
%%         {reply, nksip:sipreply()} | noreply).
%%
%% sip_publish(Req, _Call) ->
%%     ?LOGSIP("sip_publish"),
%%     ?FILTER:check_block_response_malware(method_not_allowed, Req, {reply, ?MethodNotAllowed("EB2B")}).

%% ----------------------------------------------------------------------------------
%% INFO
%% ----------------------------------------------------------------------------------
-spec(sip_info(Req::nksip:request(), Call::nksip:call()) ->
        {reply, nksip:sipreply()} | noreply).

sip_info(Req, Call) ->
    #call{call_id=CallId}=Call,
    ?LOGSIP("sip_info ~120p, ~120p", [self(), CallId]),
    case ?ESG_DLG:pull_fsm_by_callid(CallId) of
        false -> ?FILTER:check_block_response_malware(no_dialog, Req, {reply, ?Forbidden("EB2B. Unknown dialog")});
        DlgX ->
            ?ESG_DLG:sip_info(DlgX, [CallId, Req]),
            noreply
    end.

%% ----------------------------------------------------------------------------------
%% MESSAGE
%% ----------------------------------------------------------------------------------
-spec(sip_message(Request::nksip:request(), Call::nksip:call()) ->
        {reply, nksip:sipreply()} | noreply).

sip_message(Req, Call) ->
    #call{call_id=CallId}=Call,
    ?LOGSIP("sip_message ~120p, ~120p", [self(), CallId]),
    case ?ESG_DLG:pull_fsm_by_callid(CallId) of
        false -> ?FILTER:check_block_response_malware(no_dialog, Req, {reply, ?Forbidden("EB2B. Unknown dialog")});
        DlgX ->
            ?ESG_DLG:sip_message(DlgX, [CallId, Req]),
            noreply
    end.

%% ----------------------------------------------------------------------------------
%% OPTIONS
%% ----------------------------------------------------------------------------------
-spec(sip_options(Request::nksip:request(), Call::nksip:call()) ->
        {reply, nksip:sipreply()} | noreply).

sip_options(Req, Call) ->
    #call{call_id=CallId}=Call,
    ?LOGSIP("sip_options ~120p, ~120p", [self(), CallId]),
    PutSdp = case nksip_request:header(<<"accept">>, Req) of
                 {ok, []} -> true;
                 {ok, Accept} -> find_in_binary(Accept, <<"application/sdp">>);
                 {error,_} -> false
             end,
    Opts = [{replace, {<<"Allow">>, <<"INVITE, ACK, CANCEL, BYE, REFER, OPTIONS, NOTIFY, INFO, MESSAGE">>}}, % <<"REGISTER, SUBSCRIBE, PUBLISH, UPDATE">>
            {replace, {<<"Allow-Events">>, <<"refer, presence, dialog">>}}, % <<"kpml(Zoiper) talk,hold,conferencecheck_sync(Yealink)">>
            % {replace, {<<"Accept-Encoding">>, <<"">>}},
            % {replace, {<<"Accept-Language">>, <<"en">>}},
            {replace, {<<"Supported">>, <<"replaces, timer, path">>}} % <<"outbound, norefersub(Zoiper), extended-refer(Zoiper), gruu(JsSIP)">>
            ],
    Opts1 = case PutSdp of
                true ->
                    [{replace, {<<"Accept">>, <<"application/sdp">>}},
                     {replace, {<<"Content-Type">>, <<"application/sdp">>}},
                     {body, ?M_SDP:make_options_sdp()}
                     |Opts];
                false -> Opts
            end,
    {reply, {200,Opts1}}.

% --
find_in_binary([], _Key) -> false;
find_in_binary([Item|Rest], Key) ->
    case binary:match(?EU:to_lower(Item), Key) of
        nomatch -> find_in_binary(Rest, Key);
        {_From,_Count} -> true
    end.

%% ----------------------------------------------------------------------------------
%% REGISTER
%% ----------------------------------------------------------------------------------
%-spec(sip_register(Request::nksip:request(), Call::nksip:call()) ->
%        {reply, nksip:sipreply()} | noreply).
%
%sip_register(_Req, _Call) ->
%    ?LOGSIP("sip_register"),
%    {reply, ok}.

%% ----------------------------------------------------------------------------------
%% @doc sip_b2bua plugin callback
%% ----------------------------------------------------------------------------------

%% % ------
%% b2bua({uac_pre_response, [#sipmsg{cseq={_,M}}=Resp,_UAC,_Call]=Args}=_Query, _AppId) when M=='INVITE'; M=='REFER' ->
%%     {ok,CallId} = nksip_response:call_id(Resp),
%%     case ?ESG_DLG:pull_fsm_by_callid(CallId) of
%%         false -> {continue, Args};
%%         DlgX -> {continue, [Resp,_UAC,_Call1]} = ?ESG_DLG:uac_pre_response(DlgX, Args)
%%     end;
%% b2bua({uac_pre_response, Args}, _AppId) ->
%%     {continue, Args};

% ------
b2bua({uac_response, [#sipmsg{cseq={_,M}}=Resp]=Args}=_Query, _AppId) when M=='INVITE'; M=='REFER' ->
    {ok,CallId} = nksip_response:call_id(Resp),
    case ?ESG_DLG:pull_fsm_by_callid(CallId) of
        false -> continue;
        DlgX -> ?ESG_DLG:uac_response(DlgX, Args)
    end;
b2bua({uac_response, _Args}, _AppId) ->
    continue;

% ------
b2bua({uas_dialog_response, [#sipmsg{cseq={_,M}}=Resp]=Args}=_Query, _AppId) when M=='INVITE'; M=='REFER' ->
    {ok,CallId} = nksip_response:call_id(Resp),
    case ?ESG_DLG:pull_fsm_by_callid(CallId) of
        false -> continue;
        DlgX -> ?ESG_DLG:uas_dialog_response(DlgX, Args)
    end;
b2bua({uas_dialog_response, _Args}, _AppId) ->
    continue;

b2bua({call_terminate, [Reason,Call]=Args}=_Query, _AppId) ->
    #call{call_id=CallId}=Call,
    case ?ESG_DLG:pull_fsm_by_callid(CallId) of
        false -> ok;
        DlgX ->
            ?LOGSIP("EB2B. call_terminate: ~p, ~p", [CallId,Reason]),
            ?ESG_DLG:call_terminate(DlgX,Args),
            ok
    end,
    ok;

% ------
b2bua({uas_totag, [#sipmsg{from={_,FTag}}=_Req]}=Query, AppId) ->
    case ?ESG_UTILS:build_tag() of
        FTag -> b2bua(Query, AppId);
        T -> T
    end.

%% ----------------------------------------------------------------------------------
%% @doc Called to add headers just before sending the request
%% ----------------------------------------------------------------------------------

-spec nks_sip_transport_uac_headers(nksip:request(), nksip:optslist(), nksip:scheme(),
                                 nkpacket:transport(), binary(), inet:port_number()) ->
    {ok, nksip:request()}.

nks_sip_transport_uac_headers(Req, _Opts, _Scheme, _Transp, _Host, _Port) ->
    %?LOGSIP("SIP. Border Gate. nks_sip_transport_uac_headers!"),
    TransReq = ?EXT_TRANS:trans_uac_headers(Req),
    {continue, [TransReq, _Opts, _Scheme, _Transp, _Host, _Port]}.


%% ----------------------------------------------------------------------------------
%% @doc Called when preparing a UAS dialog response
%% ----------------------------------------------------------------------------------
-spec nks_sip_uas_dialog_response(nksip:request(), nksip:response(),
                               nksip:optslist(), nksip:call()) ->
    {ok, nksip:response(), nksip:optslist()}.

nks_sip_uas_dialog_response(_Req, Resp, Opts, _Call) ->
    %?LOGSIP("SIP. Border Gate. nks_sip_uas_dialog_response!"),
    TransResp = ?EXT_TRANS:trans_uas_dialog_response(Resp),
    {continue, [_Req, TransResp, Opts, _Call]}.

%% ----------------------------------------------------------------------------------
%% @doc sip_log plugin callback
%% ----------------------------------------------------------------------------------

-spec sip_log(Query::{trn_send | trn_recv, NKPort::#nkport{}, Packet::binary()}, AppId::atom()) -> ok.

sip_log(Query, AppId) ->
    ?LOGGING:sip_log(Query, AppId).

%% ----------------------------------------------------------------------------------
%% @doc sip_filter plugin callback
%% ----------------------------------------------------------------------------------

-spec sip_filter(Query::{recv, NKPort::#nkport{}, Packet::binary()}, AppId::atom()) -> ok | block.

sip_filter(Query, AppId) ->
    ?FILTER:transport_filter(dynamic, Query, AppId).

%% ----------------------------------------------------------------------------------
%% @doc Domain Callback: Synchronous user call.
%% ----------------------------------------------------------------------------------

sip_checkclusterdomain(Domain, _AppId) ->
    ?LOCALDOMAIN:is_cluster_domain(Domain).

%% ----------------------------------------------------------------------------------
%% @doc SipApp Callback: Synchronous user call.
%% ----------------------------------------------------------------------------------

%% handle_call(get_speed, _From, State) ->
%%     {ok, Speed} = nksip:get(?SIPAPP, speed),
%%     Reply = [{Time, ?U:unparse_uri(Uri)} || {Time, Uri} <- Speed],
%%     {reply, Reply, State};

handle_call(_, _From, _State) ->
    continue.


%% ----------------------------------------------------------------------------------
%% @doc SipApp Callback: Asynchronous user cast.
%% ----------------------------------------------------------------------------------

%% handle_cast({speed_update, Speed}, State) ->
%%     ok = nksip:put(?SIPAPP, speed, Speed),
%%     erlang:start_timer(?TIME_CHECK, self(), check_speed),
%%     {noreply, State};
%%
%% handle_cast({check_speed, true}, State) ->
%%     handle_info({timeout, none, check_speed}, State#state{auto_check=true});
%%
%% handle_cast({check_speed, false}, State) ->
%%     {noreply, State#state{auto_check=false}}.

handle_cast(_, _State) ->
    %{noreply, State}.
    continue.

%% ----------------------------------------------------------------------------------
%% @doc SipApp Callback: External erlang message received.
%% The programmed timer sends a `{timeout, _Ref, check_speed}' message
%% periodically to the SipApp.
%% ----------------------------------------------------------------------------------
%%
%% handle_info({timeout, _, check_speed}, #state{auto_check=true}=State) ->
%%     Self = self(),
%%     spawn(fun() -> test_speed(Self) end),
%%     {noreply, State};
%%
%% handle_info({timeout, _, check_speed}, #state{auto_check=false}=State) ->
%%     {noreply, State}.

handle_info(_, _State) ->
     %{noreply, State}.
    continue.

%% ==========================================================================
%% Internal functions
%% ==========================================================================

% -------------------------------
% extracts ruri from request (could be shadowed by route header)
%
extract_ruri(Req) ->
    case ?ROUTE:get_top_route_uri(Req) of
        #uri{scheme=S, user=U, domain=D} ->
            % Real User and Domain from route
            {S, U, D};
        notfound ->
            % RURI User and Domain
            #sipmsg{ruri=#uri{scheme=S, user=U, domain=D}}=Req,
            {S, U, D}
    end.

% -------------------------------
% consider failed request to ban remote address
%
fail(#sipmsg{nkport=#nkport{remote_ip=Ip}}=_Req) ->
    ?FILTER:consider_failed_request_from(Ip).

% --------------------------------
%% %% @doc Gets all registered contacts and sends an OPTION to each of them
%% %% to measure its response time.
%% test_speed(Pid) ->
%%     ok.
%%
%% %% @private
%% test_speed([], Acc) ->
%%     Acc;
%% test_speed([Uri|Rest], Acc) ->
%%     case timer:tc(fun() -> nksip_uac:options(?SIPAPP, Uri, []) end) of
%%         {Time, {ok, 200, []}} ->
%%             test_speed(Rest, [{Time/1000, Uri}|Acc]);
%%         {_, _} ->
%%             test_speed(Rest, Acc)
%%     end.
