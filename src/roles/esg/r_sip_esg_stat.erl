%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc

-module(r_sip_esg_stat).
-author('Peter Bukashin <tbotc@yandex.ru>').

%% -------------------------------------------------------------------
%%
%% -------------------------------------------------------------------

-export([query/2]).

%% ==========================================================================
%% Defines
%% ==========================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_esg.hrl").

%% ====================================================================
%% API functions
%% ====================================================================


query(<<"domain">>, #{}=Map) ->
    TD = maps:get(domain, Map, undefined),
    Code = maps:get(code, Map, undefined),
    Regs = ets:foldl(fun({_,M}, Acc) -> ets_query(M, Acc, TD, Code) end, [], ?EXT_REGSRV:get_ets()),
    M1 = #{regs => lists:map(fun({_,_,V}) -> V end, lists:sort(Regs))},
    {ok, M1};

%% --------------------------------------
query(<<"info">>, #{}=Map) ->
    TD = maps:get(domain, Map, undefined),
    Attrs = [dir,extacccode],
    DlgAttrs = ?DLG_STORE:get_all_dlgs_attrs([dir,extacccode]),
    FDlg = fun(Item,Acc) ->
                   [Dir,Code] = ?EU:extract_optional_default([{dir,undef},{extacccode,undef}],Item),
                   case Dir==undef orelse Code==undef of
                       true -> Acc;
                       false ->
                           CodeAcc = maps:get(Code,Acc,#{}),
                           Acc#{Code => maps:update_with(Dir,fun(V) -> V+1 end, 1, CodeAcc)}
                   end
           end,
    InOutDlgs = lists:foldl(FDlg,#{},[lists:zip(Attrs,DlgRes) || DlgRes <- DlgAttrs]),
    Regs = maps:get(regs,active_regs(Map#{regs=>1}, #{})),
    MapRes = ?EU:json_to_map(Regs),
    F = fun(MapX) ->
                case maps:get(d,MapX) of
                    D when D==TD ->
                        Code = maps:get(code,MapX),
                        Mx = maps:with([code,regstate,onlinetrunkcount],MapX),
                        Mx2 = maps:merge(Mx,maps:get(Code,InOutDlgs,#{})),
                        {true,Mx2};
                    _ -> false
                end
        end,
    Result = lists:filtermap(F,MapRes),
    {ok,Result};

%% --------------------------------------
query(<<"active">>, #{}=Map) ->
    Fd = fun({{onlinetrunk,Domain,_Id},Cnt},Acc) ->
                 case maps:find(Domain,Acc) of
                     error -> maps:put(Domain,Cnt,Acc);
                     {_,Cnt1} -> maps:put(Domain,Cnt1+Cnt,Acc)
                 end;
            (_,Acc) -> Acc
         end,
    M = #{dlgcount => ?DLG_STORE:get_dialog_count(),
          callcount => ?DLG_STORE:get_callid_count(),
          domains => lists:filter(fun({_,0}) -> false; (_) -> true end, maps:to_list(?SIPSTORE:foldl_u(Fd,maps:new())))},
    {ok, active_regs(Map, M)};

%% --------------------------------------

%% read server's dynamic banned remote addrs
query(<<"banned_read">>, #{}=_Map) ->
    NowGS = ?EU:current_gregsecond(),
    R = ?FILTER_SRV:get_banned(),
    R1 = lists:map(fun(A) ->
                           [{<<"ip">>, ?EU:to_binary(inet:ntoa(maps:get(remoteip,A,{0,0,0,0})))},
                            {<<"expires">>, maps:get(expiregs,A)-NowGS}]
                   end, R),
    {ok, #{banned => R1}};

%% create server's dynamic banned addr
query(<<"banned_create">>, #{}=Map) ->
    case ?EU:extract_optional_props([ip,expires], maps:to_list(Map)) of
        [undefined,_] -> #{result => false};
        [_,undefined] -> #{result => false};
        [Ip,Expires] ->
            ?FILTER_SRV:ban(Ip, Expires),
            {ok, #{result => true}}
    end;

%% clear server's dynamic banned addrs
query(<<"banned_clear">>, #{}=_Map) ->
    ?FILTER_SRV:clear_banned(),
    {ok, #{result => true}};

%% delete ip from server's dynamic banned addrs
query(<<"banned_delete">>, #{}=Map) ->
    case maps:get(ip, Map, undefined) of
        undefined -> #{result => false};
        Ip ->
            ?FILTER_SRV:unban(Ip),
            {ok, #{result => true}}
    end;

%% ----------------------------------------------
%% RP-315. Test provider callerid.
query(<<"test_normalization">>, #{}=Map) ->
    case ?EU:maps_get([dir,from,fromtd,to,totd,clusterdomain,id,code],Map) of
        {error,_} -> {error, {invalid_params,<<"Invalid params. Expected 'dir', 'fromnumber', 'fromdomain', 'tonumber', 'todomain', 'clusterdomain', 'providerid'">>}};
        [Dir,FromNumber,FromTD,ToNumber,ToTD,CD,ProviderId,ProviderCode] ->
            Ftrace = ?ENVTRACE:start(), % only current site!
            PI = case {?DC:find_provider_account_by_id(CD, ProviderId), ?DC:find_provider_account_by_code(CD, ProviderCode)} of
                     {undefined,V} -> V;
                     {V,_} -> V
                 end,
            case PI of
                undefined -> Ftrace({<<"provider not found">>});
                ProviderItem when is_map(ProviderItem) ->
                    AMap = ProviderItem#{normalization_trace => Ftrace},
                    [IdP,CodeP,UserP,DP,SidP,ProxyP,ProxyPortP,RegP,EnabledP,TrunksOutP,TransportP,ExpiresP] =
                        ?EU:maps_get([id,code,username,domain,serveridxs,proxyaddr,proxyport,reg,enabled,trunksout,transport,expires],ProviderItem),
                    Ftrace({<<"start">>,CD,IdP,Dir,CodeP,UserP,DP,SidP,ProxyP,ProxyPortP,RegP,EnabledP,TrunksOutP,TransportP,ExpiresP}),
                    FromAOR = {sip,FromNumber,FromTD}, ToAOR =  {sip,ToNumber,ToTD},
                    FromUri = ?U:make_uri(FromAOR), ToUri = ?U:make_uri(ToAOR),
                    Ftrace({<<"start normalization FROM">>,FromAOR,ToAOR}),
                    case ?DC:find_provider_normalization_rule(CD, IdP, Dir, FromAOR, ToAOR, 0, Ftrace) of
                        #{}=Rule ->
                            FromUriUp = case Dir of
                                            inner -> ?ESG_UTILS:build_inner_from_uri(false, FromUri, ToUri, Rule, AMap);
                                            outer -> ?ESG_UTILS:build_outer_from_uri(false, FromUri, ToUri, Rule, AMap)
                                        end,
                            FromUriUpAOR = ?U:make_aor(FromUriUp),
                            case maps:get('mod_tonumber',Rule) of
                                <<"priority=",X/bitstring>> ->
                                    StartPriorityTo = try ?EU:to_int(X) catch _:_ -> 0 end,
                                    Ftrace({<<"start normalization TO">>,FromUriUpAOR,ToAOR,StartPriorityTo});
                                _ -> ok
                            end,
                            ToUriUp = case Dir of
                                          inner -> ?ESG_UTILS:build_inner_to_uri(false, FromUriUp, ToUri, Rule, AMap);
                                          outer -> ?ESG_UTILS:build_outer_to_uri(false, FromUriUp, ToUri, Rule, AMap)
                                      end,
                            ToUriUpAOR = ?U:make_aor(ToUriUp),
                            Ftrace({<<"final">>,FromAOR,FromUriUpAOR,ToAOR,ToUriUpAOR});
                        _ ->
                            Ftrace({<<"rule not found">>})
                    end
            end,
            ?ENVTRACE:stop(Ftrace)
    end.

%% @private
active_regs(Map, M) ->
    case ?EU:to_bool(maps:get(regs,Map,0)) of
        false -> M;
        true ->
            Regs = ets:foldl(fun({_,Mx}, Acc) -> ets_query(Mx, Acc) end, [], ?EXT_REGSRV:get_ets()),
            active_code(Map, M#{regs => lists:map(fun({_,_,V}) -> V end, lists:sort(Regs))})
    end.

%% @private
active_code(Map, M) ->
    case maps:get(code,Map,undefined) of
        undefined -> M;
        Code ->
            M#{regs := lists:filter(fun(X) ->
                                            case ?EU:get_by_key(code,X,undefined) of
                                                Code -> true;
                                                _ -> false
                                            end end, maps:get(regs,M))}
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

ets_query(M, Acc) ->
    {reg,_,_,AMap} = maps:get(accountinfo,M),
    R = [{code, maps:get(code,AMap)},
         {d, CD = maps:get(clusterdomain,AMap)},
         {user, U = maps:get(user,M)},
         {domain, maps:get(domain,M)},
         {port, maps:get(proxyport,AMap,5060)},
         {proto, maps:get(transport,AMap,udp)},
         {addrs, lists:reverse(maps:get(resolved,M))},
         {regstate, maps:get(regstate,M)},
         {pingstate, maps:get(pingstate,M)},
         {last_response, case maps:get(last_response,M,undefined) of
                             #sipmsg{}=Msg -> binary:split(?U:unparse_msg(Msg), <<"\r\n">>, [global, trim_all]);
                             undefined -> null
                         end}],
    Id = maps:get(id,AMap),
    case ?SIPSTORE:find_u({onlinetrunk,CD,Id}) of
        {_,Online} -> Online;
        _ -> Online = 0
    end,
    [{CD,U,R ++ [{onlinetrunkcount, Online}]}|Acc].

%% ----------------------------------------------
%%
%% ----------------------------------------------
ets_query(M, Acc, Domain, undefined) ->
    {reg,_,_,AMap} = maps:get(accountinfo,M),
    case maps:get(clusterdomain,AMap) of
        Domain ->
            R = [{code, maps:get(code,AMap)},
                 {d, CD = Domain},
                 {user, U = maps:get(user,M)},
                 {domain, maps:get(domain,M)},
                 {port, maps:get(proxyport,AMap,5060)},
                 {proto, maps:get(transport,AMap,udp)},
                 {addrs, lists:reverse(maps:get(resolved,M))},
                 {regstate, maps:get(regstate,M)},
                 {pingstate, maps:get(pingstate,M)},
                 {last_response, case maps:get(last_response,M,undefined) of
                             #sipmsg{}=Msg -> binary:split(?U:unparse_msg(Msg), <<"\r\n">>, [global, trim_all]);
                             undefined -> null
                         end}],
            Id = maps:get(id,AMap),
            case ?SIPSTORE:find_u({onlinetrunk,CD,Id}) of
                {_,Online} -> Online;
                _ -> Online = 0
            end,
            [{CD,U,R ++ [{onlinetrunkcount, Online}]}|Acc];
        _ ->
            Acc
    end;
%% ----
ets_query(M, Acc, Domain, Code) ->
    {reg,_,_,AMap} = maps:get(accountinfo,M),
    case {maps:get(clusterdomain,AMap), maps:get(code,AMap)} of
        {Domain, Code} ->
            R = [{code, Code},
                 {d, CD = Domain},
                 {user, U = maps:get(user,M)},
                 {domain, maps:get(domain,M)},
                 {port, maps:get(proxyport,AMap,5060)},
                 {proto, maps:get(transport,AMap,udp)},
                 {addrs, lists:reverse(maps:get(resolved,M))},
                 {regstate, maps:get(regstate,M)},
                 {pingstate, maps:get(pingstate,M)}],
            Id = maps:get(id,AMap),
            case ?SIPSTORE:find_u({onlinetrunk,CD,Id}) of
                {_,Online} -> Online;
                _ -> Online = 0
            end,
            [{CD,U,R ++ [{onlinetrunkcount, Online}]}|Acc];
        _ ->
            Acc
    end.
