%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc
%%% @todo

-module(r_sip_esg_supv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(supervisor).

-export([init/1]).
-export([start_link/0]).
-export([start_child/1, terminate_child/1, delete_child/1, restart_child/1, drop_child/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_esg.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).


start_child(ChildSpec) ->
    supervisor:start_child(?MODULE, ChildSpec).

restart_child(Id) ->
    supervisor:restart_child(?MODULE, Id).

terminate_child(Id) ->
    supervisor:terminate_child(?MODULE, Id).

delete_child(Id) ->
    supervisor:delete_child(?MODULE, Id).

drop_child(Id) ->
    catch terminate_child(Id),
    catch delete_child(Id),
    ok.

%% ====================================================================
%% OTP callbacks
%% ====================================================================

init(_State) ->
    RegSrvOpts = [{ets, ets:new(reg,[public,set])},
                  {garbage, ets:new(garbage,[public,set])}],
    RegSrvSpec = {?EXT_REGSRV, {?EXT_REGSRV, start_link, [RegSrvOpts]}, permanent, 1000, worker, [?EXT_REGSRV]},
    %
    ?LOG("ExtReg supv inited"),
    ChildSpec = [RegSrvSpec],
    {ok, {{one_for_one, 10, 2}, ChildSpec}}.


