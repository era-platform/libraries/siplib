%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Anton Makarov <anton@mastermak.ru>, Peter Bukashin <tbotc@yandex.ru>
%%% @doc


-module(r_sip_esg_translit).

-export([trans_uac_headers/1,
         trans_uas_dialog_response/1]).

%% ==========================================================================
%% Defines
%% ==========================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_esg.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ====================================================================
%% Public functions
%% ====================================================================

%% ----------------------------------------------------------------------------------
%% @doc gate translit callback interface
%% ----------------------------------------------------------------------------------

%% ----------------------------------------------------------------------------------
%% @doc Called when the UAC is preparing a request to be sent
%% ----------------------------------------------------------------------------------
%%
trans_uac_headers(#sipmsg{}=Req) ->
    do_forward_request(Req).

%% ----------------------------------------------------------------------------------
%% @doc Called when preparing a UAS dialog response
%% ----------------------------------------------------------------------------------
trans_uas_dialog_response(#sipmsg{}=Resp) ->
    do_forward_response(Resp).

%% ====================================================================
%% Internal functions
%% ====================================================================
%% ----------------------------------------------------------------------------------
%% Request Handler
%% ----------------------------------------------------------------------------------
do_forward_request(Req) ->
    case needs_translit(Req) of
        true ->
            ?TRANSLIT:translit_sip_packet(Req);
        false ->
            Req
    end.

%% ----------------------------------------------------------------------------------
%% Response Handler
%% ----------------------------------------------------------------------------------
do_forward_response(Resp) ->
    case needs_translit(Resp) of
        true ->
            ?TRANSLIT:translit_sip_packet(Resp);
        false ->
            Resp
    end.

%% ----------------------------------------------------------------------------------
%% Support Functions
%% ----------------------------------------------------------------------------------
%% Is checked only on gate so can tell by analyzing source
needs_translit(#sipmsg{call_id=CallId}=_R) ->
%%     ?OUT("is_outgoing(R=~p)", [_R]),
    case get_dlgx(CallId) of
        {ok, DlgX} ->
            Opts = [acallids, bcallids, dir, translit],
            DlgInfo = ?EXT_UTILS:get_dialog_info(DlgX,Opts),
%%             ?OUT("DlgInfo:~p",[DlgInfo]),
            [ACallIds, BCallIds, Dir, Translit] = ?EU:extract_required_props(Opts, DlgInfo),
            case Translit of
                false ->
                    false;
                true ->
                    is_outgoing(CallId, ACallIds, BCallIds, Dir)
            end;
        {error,_Err} ->
            false
    end.

is_outgoing(CallId, ACallIds, BCallIds, Dir) ->
    case {lists:member(CallId, ACallIds), lists:member(CallId, BCallIds), Dir} of
        {true, _, outside} -> false;
        {true, _, inside} -> true;
        {_, true, outside} -> true;
        {_, true, inside} -> false;
        _ -> false
    end.

get_dlgx(CallId) ->
%%     ?OUT("CallId = ~p", [CallId]),
    case ?ESG_DLG:pull_fsm_by_callid(CallId) of
        false -> {error, <<"EB2B. Unknown dialog">>};
        DlgX -> {ok, DlgX}
    end.

