%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Some routines for ext registering

-module(r_sip_esg_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([get_ext_addrs/0,
         get_nat_addrs_async/1,
         get_stun_servers/0,
         make_stun_request/1,
         parse_transport/1]).

-export([check_limit/2,
         get_active_trunk_count/1,
         reserve_active_trunk/2]).

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_esg.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

% returns ext addrs of server from app opts (or default)
get_ext_addrs() ->
    {_,SipAppModule} = ?ENV:get_env('sipappmodule'),
    {ok,AppOpts} = ?APP:get_opts(SipAppModule),
    NodeAddr = ?CFG:get_my_sipserver_addr(),
    case ?EU:extract_optional_props([<<"extaddrs">>], AppOpts) of
        [undefined] -> [NodeAddr];
        [Addrs] when is_list(Addrs) -> [NodeAddr | lists:delete(NodeAddr, lists:map(fun(A) -> ?EU:to_binary(A) end, Addrs))];
        [Addr] -> [NodeAddr | lists:delete(NodeAddr,[?EU:to_binary(Addr)])]
    end.

% make async parallel requests to obtain outer local address
get_nat_addrs_async(FunSet) ->
    StunSrvs = get_stun_servers(),
    F = fun(StunSrv) ->
                case make_stun_request(StunSrv) of
                    {ok,{_Local,_LPort},{Remote,_RPort}} -> FunSet(?EU:to_binary(inet:ntoa(Remote)));
                    R -> R
                end end,
    lists:foreach(fun(StunSrv) -> ?EU:spawn_fun(fun() -> F(StunSrv) end) end, StunSrvs).

% returns stun server from app opts or default
get_stun_servers() ->
    {_,SipAppModule} = ?ENV:get_env('sipappmodule'),
    {ok,AppOpts} = ?APP:get_opts(SipAppModule),
    Fdefault = fun() -> [] end, %?U:get_stun_addr(),
    case ?EU:get_by_key(<<"stunserver">>, AppOpts, undefined) of
        undefined -> lists:map(fun(A) -> ?EU:to_list(A) end, Fdefault());
        Addrs when is_list(Addrs) -> lists:map(fun(A) -> ?EU:to_list(A) end, Addrs);
        Addr -> [?EU:to_list(Addr)]
    end.

% makes stun request
make_stun_request(StunServer) ->
    {_,SipApp} = ?ENV:get_env('sipapp'),
    nksip_uac:stun(SipApp, ?EU:to_list(StunServer), []).

% parse transport, returns appropriate value
parse_transport(1) -> udp;
parse_transport(2) -> tcp;
parse_transport(3) -> tls;
parse_transport(T) when is_atom(T) -> T;
parse_transport(T) when is_binary(T) ->
    case string:to_lower(?EU:to_list(T)) of
        "udp" -> udp;
        "tcp" -> tcp;
        "tls" -> tls;
        "1" -> udp;
        "2" -> tcp;
        "3" -> tls;
        _ -> udp
    end.

%% ======================================================
%% Active trunk count limit operations
%% ======================================================

%% -----------------------
%% RP-493
%% Return true if outer incoming call could be handled, false otherwise
%% Checks if lic.siptrunks value is greater than all account's active calls count
%% -----------------------
check_limit(outer,#{}=AMap) ->
    Limit = maps:get(<<"siptrunks">>,maps:get(lic,AMap),0),
    Count = ?EXT_UTILS:get_active_trunk_count(AMap),
    Count < Limit;

%% -----------------------
%% RP-493
%% Return true if inner outgoing call could be handled, false otherwise
%% Check if trunksout if
%% Checks if lic.siptrunks value is greater than all account's active calls count
%% -----------------------
check_limit(inner,#{}=AMap) ->
    case maps:get(trunksout,AMap) of
        T when not is_integer(T) -> false;
        T when T >= 99999 -> true;
        T when T < 0 ->
            Limit = maps:get(<<"siptrunks">>,maps:get(lic,AMap),0),
            Count = ?EXT_UTILS:get_active_trunk_count(AMap),
            Count < Limit;
        Limit ->
            Count = ?EXT_UTILS:get_active_trunk_count(AMap),
            Count < Limit
    end.

%% -----------------------
%% returns count of active trunksout on provider
%% -----------------------
get_active_trunk_count(#{}=AMap) ->
    Key = onlinetrunk_key(AMap),
    case ?SIPSTORE:find_u(Key) of
        false -> 0;
        {_,Value} -> Value
    end.

%% @private
onlinetrunk_key(#{}=AMap) ->
    ClusterDomain = maps:get(clusterdomain,AMap),
    ExtAccId = maps:get(id,AMap),
    _Key = {onlinetrunk,ClusterDomain,ExtAccId}.

%% -----------------------
%% attempts to reserve trunk (increase by 1 in store) and prepare clear function
%% -----------------------
reserve_active_trunk(#{}=AMap, Dir) ->
    Key = onlinetrunk_key(AMap),
    case Dir of
        ?DirOutside ->
            % =======================
            % 18.08.2016 Peter
            %  No limit, it is used on B2BUA as check before route
            % =======================
            %% TrunkCount = maps:get(trunksout,AMap),
            %% FunStore = fun(undefined) -> 1;
            %%               (N) when TrunkCount==0; TrunkCount>N -> N+1;
            %%               (N) -> N
            %%            end,
            %% FunReply = fun(undefined) -> true;
            %%               (N) when TrunkCount==0; TrunkCount>N -> true;
            %%               (_) -> false
            %%            end;
            % =======================
            FunStore = fun(undefined) -> 1;
                            (N) -> N + 1
                          end,
            FunReply = fun(_) -> true end;
        ?DirInside ->
            FunStore = fun(undefined) -> 1;
                            (N) -> N+1
                          end,
            FunReply = fun(_) -> true end
    end,
    case ?SIPSTORE:func_u(Key, FunStore, FunReply) of
        false -> false;
        true ->
            FunClear = fun() ->
                               FunStore1 = fun(undefined) -> 0;
                                              (0) -> 0;
                                              (N) when is_integer(N) -> N-1
                                           end,
                               ?SIPSTORE:func_u(Key, FunStore1, fun(_) -> ok end)
                       end,
            {ok,FunClear}
    end.

%% -------------------------
get_dialog_info(DlgX,Args) ->
    DlgInfo = ?ESG_DLG:get_dialog_info(DlgX, []),
    [StoreData,Account,AsideL,BsideL,BsideFL] = ?EU:extract_required_props([store_data, account, aside, bside, bside_fork], DlgInfo),
    Acallids = parse_side_list(side, AsideL),
    Bcallids = parse_side_list(side, BsideL) ++ parse_side_list(side_fork, BsideFL),
    lists:map(fun(OptName) ->
                    case OptName of
                        acallids -> {acallids, Acallids};
                        bcallids -> {bcallids, Bcallids};
                        dir ->      {dir, maps:get(dir, StoreData)};
                        provider -> {provider, Account};
                        translit -> {translit, maps:get(translit, Account)}
                    end end, Args).

%% -----------------------
build_local_contact_addr(AMap) ->
    case maps:get(proxyaddr,AMap,undefined) of % @localcontact? ({_,_,#{domain=RouteDomain}}=make_route(Map)),
        undefined -> ?U:get_host_addr({1,1,1,1});
        <<>> -> ?U:get_host_addr({1,1,1,1});
        ProxyAddr ->
            case catch ?U:get_host_addr(ProxyAddr) of
                {'EXIT',_} -> ?U:get_host_addr({1,1,1,1});
                Addr -> Addr
            end end.

%% ====================================================================
%% Internal functions
%% ====================================================================

parse_side_list(side, SideL) ->
    [CallId || #side{callid=CallId} <- SideL];
parse_side_list(side_fork, SideL) ->
    [CallId || #side_fork{callid=CallId} <- SideL].

get_dialog_info_handle_opts([Opt|OptsT]=_Opts, Res) ->
    get_dialog_info_handle_opts([Opt|OptsT]=_Opts, Res, []).

get_dialog_info_handle_opts([{Opt,_Val}|_OptsT]=_Opts, Res, Acc) ->
    case Opt of
        acallid ->
            {ACallId}=Res,
            [{acallid, ACallId}] ++ Acc;
        bcallid -> ok;
        dir -> ok;
        provider -> ok
    end.
