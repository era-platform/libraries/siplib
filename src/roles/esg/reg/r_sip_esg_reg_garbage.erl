%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 22.01.2018.
%%% @doc Garbage utils.
%%%        Pausing registration (attach) while account is on garbage timeout after last stop_register.
%%%      (Solve nksip problem of same key stop_register and start_register on transaction retransmit)


-module(r_sip_esg_reg_garbage).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([enabled/0,
         init_garbage/2,
         seed_garbage/1,
         move_to_garbage/2,
         check_garbage/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_esg.hrl").
-include("../include/r_sip_esg_reg.hrl").

%-define(ENABLED, true).

%% ====================================================================
%% API functions
%% ====================================================================

-ifdef(ENABLED).

enabled() -> true.

%% -------------------------------------
%% delete from garbage all expired elements
%% -------------------------------------
init_garbage(Opts,State) ->
    [GarbageETS] = ?EU:extract_required_props([garbage], Opts),
    {ok,State#{garbage => GarbageETS}}.

%% -------------------------------------
%% delete from garbage all expired elements
%% -------------------------------------
seed_garbage(State) ->
    Garbage = maps:get(garbage,State),
    NowGS = ?EU:current_gregsecond(),
    ets:foldl(fun({RegId,GS},_Acc) when NowGS>GS+?MaxTransactionTimeSec -> ets:delete(Garbage, RegId);
                 (_,Acc) -> Acc
              end, ok, Garbage).

%% -------------------------------------
%% add regid key to garbage, to prevent fast attaching when transaction is active
%% -------------------------------------
move_to_garbage(RegId,State) ->
    Garbage = maps:get(garbage,State),
    NowGS = ?EU:current_gregsecond(),
    ets:insert(Garbage,{RegId,NowGS}),
    ok.

%% -------------------------------------
%% check if regid is in garbage
%% -------------------------------------
check_garbage(RegId,State) ->
    Garbage = maps:get(garbage,State),
    NowGS = ?EU:current_gregsecond(),
    case ets:lookup(Garbage,RegId) of
        [{_,TS}] when NowGS<TS+?MaxTransactionTimeSec -> true;
        _ -> false
    end.

-else.

enabled() -> false.

%% ---------
init_garbage(_Opts,State) -> {ok,State}.

%% ---------
seed_garbage(_State) -> ok.

%% ---------
move_to_garbage(_RegId,_State) -> ok.

%% ---------
check_garbage(_RegId,_State) -> false.

-endif.

%% ====================================================================
%% Internal functions
%% ====================================================================

