%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 19.01.2018.
%%% @doc Ping operations of reg_srv

-module(r_sip_esg_reg_ping).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([start_ping/3,
         on_ping_result/2,
         send_ping/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_esg.hrl").
-include("../include/r_sip_esg_reg.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% -------------------------------------
%% initiates async ping process (at start)
%% -------------------------------------
start_ping(RegId, Map, State) ->
    E = maps:get(ets, State),
    {_,_,_,AMap} = maps:get(accountinfo,Map),
    PingMode = maps:get(pingmode,AMap),
    case PingMode of
        <<"none">> ->
            Map1 = ?REGSTATE:set_pingstate('disabled',RegId,Map),
            ets:insert(E, {RegId, Map1});
        _ ->
            case ?EU:to_bool(maps:get(reg,AMap)) of
                true -> % timer (if reg)
                    Ref = make_ref(),
                    PingTimeout = maps:get(pingtimeout,AMap),
                    TimerRef = erlang:send_after(erlang:max(1,PingTimeout)*1000, self(), {ping, {RegId, Ref}}),
                    Map1 = ?REGSTATE:set_pingstate('ok',RegId,Map#{pingref := Ref,
                                                                   timerref := TimerRef}), % registered successfully   
                    ets:insert(E, {RegId, Map1});
                false -> % right now (if no reg)
                    Map1 = ?REGSTATE:set_pingstate('wait_result',RegId,Map),
                    ets:insert(E, {RegId, Map1}),
                    do_send_ping(RegId, Map, State)
            end end.

%% -------------------------------------
%% handles account start ping async result (disabled, started)
%% -------------------------------------
on_ping_result({RegId,Result,Ref}, #{}=State) when Result==disabled ->
    E = maps:get(ets,State),
    case ets:lookup(E,RegId) of
        [{_,#{pingref:=Ref}=Map}] ->
            Map1 = ?REGSTATE:set_pingstate(Result,RegId,Map),
            ets:insert(E,{RegId,Map1});
        [_] -> ok;
        [] -> ok
    end;
%% account start ping async result (false, error)
on_ping_result({RegId,Result,Ref}, #{}=State) when Result==ok; Result==error ->
    E = maps:get(ets,State),
    case ets:lookup(E,RegId) of
        [{_,#{pingref:=Ref}=Map}] ->
            {_,_,_,AMap} = maps:get(accountinfo,Map),
            {OkC,ErC} = maps:get(ping_count,Map),
            Cnt = case Result of
                      ok when OkC<2 -> {OkC+1,ErC}; % only 3 sequental ok turn account from error to ok state
                      ok -> {OkC+1,0};
                      error when ErC<2 -> {OkC,ErC+1}; % only 3 sequental error turn account from ok to error state
                      error -> {0,ErC+1}
                  end,
            PingTimeout = maps:get(pingtimeout,AMap),
            Ref1 = make_ref(),
            Timeout = case Result of
                          ok -> PingTimeout;
                          error -> erlang:min(3,PingTimeout)
                      end,
            TimerRef = erlang:send_after(erlang:max(1,Timeout)*1000, self(), {ping, {RegId, Ref1}}),
            Map1 = ?REGSTATE:set_pingstate(Result,RegId,Map#{pingref:=Ref1,
                                                             ping_count:=Cnt,
                                                             timerref:=TimerRef}),
            ets:insert(E,{RegId,Map1});
        [_] -> ok;
        [] -> ok
    end;
%%
on_ping_result(Params,_) ->
    ?LOG("ExtReg. unknown ping_result: (~120p)", [Params]),
    ok.

%% -------------------------------------
%% send async ping
%% -------------------------------------
send_ping({RegId, Ref}, #{}=State) ->
    E = maps:get(ets,State),
    case ets:lookup(E, RegId) of
        [] -> ok;
        [{_,#{pingref:=Ref}=Map}] ->
             {ok,_RegId} = do_send_ping(RegId, Map, State),
             ok;
        [_] -> ok
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -------------------------------------
%% send async ping
%% -------------------------------------
do_send_ping(RegId, Map, State) ->
    E = maps:get(ets, State),
    {_,_,_,AMap} = maps:get(accountinfo,Map),
    PingMode = maps:get(pingmode,AMap),
    ProxyN = maps:get(proxy_num,Map),
    Ref = make_ref(),
    F = fun() ->
                GenSrv = ?EXT_REGSRV:srvname(),
                case PingMode of
                    <<"none">> ->
                        gen_server:cast(GenSrv, {ping_result, {RegId, 'disabled', Ref}});
                    <<"options">> ->
                        case ping_options(AMap,ProxyN) of
                            {ok,I,_} when is_integer(I), I>=200, I<300 ->
                                gen_server:cast(GenSrv, {ping_result, {RegId, 'ok', Ref}});
                            _ ->
                                gen_server:cast(GenSrv, {ping_result, {RegId, 'error', Ref}})
                        end;
                    <<"stun">> ->
                        case ping_stun(AMap,ProxyN) of
                            {ok,_,_} ->
                                gen_server:cast(GenSrv, {ping_result, {RegId, 'ok', Ref}});
                            _ ->
                                gen_server:cast(GenSrv, {ping_result, {RegId, 'error', Ref}})
                        end;
                    <<"rn">> ->
                        case ping_rn(AMap,ProxyN) of
                            {ok,_} -> % @TODO no answer got, only send finished
                                gen_server:cast(GenSrv, {ping_result, {RegId, 'ok', Ref}});
                            _ ->
                                gen_server:cast(GenSrv, {ping_result, {RegId, 'error', Ref}})
                        end end end,
    ?EU:spawn_fun(F),
    ets:insert(E, {RegId, Map#{pingref => Ref}}),
    {ok,RegId}.

%% -------------------------------------
%% ping by rnrn
%% -------------------------------------
ping_rn(AMap,ProxyN) ->
    {_,SipApp} = ?ENV:get_env('sipapp'),
    {ok, AppId} = nkservice_server:get_srv_id(SipApp),
    {_,_,RouteUri} = ?UAC:make_route_ping(AMap,ProxyN),
    Opts = #{srv_id => {nksip, AppId},
             base_nkport => true,
             udp_to_tcp => true,
             %% == George == changed from atom to binary string
             ws_proto => <<"sip">>},
    _R = case nkpacket_dns:resolve(RouteUri) of
             {ok, []} ->
                 not_resolved;
             {ok, [{undefined, Addr, 0}]} ->
                 Dest1 = {current, {nksip_protocol, udp, Addr, 5060}},
                 case catch nkpacket_transport:send([Dest1], <<13,10,13,10>>, Opts) of _T -> _T end;
             {ok, [{undefined, Addr, Port}]} ->
                 Dest1 = {current, {nksip_protocol, udp, Addr, Port}},
                 case catch nkpacket_transport:send([Dest1], <<13,10,13,10>>, Opts) of _T -> _T end;
             {ok, [{Transp, Addr, 0}]} ->
                 Dest1 = {current, {nksip_protocol, Transp, Addr, 5060}},
                 case catch nkpacket_transport:send([Dest1], <<13,10,13,10>>, Opts) of _T -> _T end;
             {ok, [{Transp, Addr, Port}]} ->
                 Dest1 = {current, {nksip_protocol, Transp, Addr, Port}},
                 case catch nkpacket_transport:send([Dest1], <<13,10,13,10>>, Opts) of _T -> _T end
         end.

%% -------------------------------------
%% ping by stun
%% -------------------------------------
ping_stun(AMap,ProxyN) ->
    {_,_,RouteUri} = ?UAC:make_route_ping(AMap,ProxyN),
    StunServer = ?U:unparse_uri(RouteUri),
    % ?OUT("STUN -> ~p", [StunServer]),
    _R = ?EXT_UTILS:make_stun_request(StunServer),
    % ?OUT("STUN : ~p", [_R]),
    _R.

%% -------------------------------------
%% ping by options
%% -------------------------------------
ping_options(AMap,ProxyN) ->
%%    {_,SipApp} = ?ENV:get_env('sipapp'),
    %% == George == nksip.0.5
    %% {ok,AppId} = nksip:find_app_id(SipApp),
%%    {ok,AppId} = nkservice_server:get_srv_id(SipApp),
    AppId = ?ENV:get_env('sipappmodule', ?ESG),
    {RUri,RouteOpts,_} = ?UAC:make_route_ping(AMap,ProxyN),
    Domain = maps:get(domain,AMap),
    User = maps:get(username,AMap),
    %
    LoginOpts = ?ESG_UTILS:build_auth_opts_ext(AMap),
    Opts = [{from, #uri{scheme=sip,user=User,domain=Domain}}
           |LoginOpts++RouteOpts],
    _R = case catch nksip_uac:options(AppId, RUri, Opts) of _T -> _T end.

