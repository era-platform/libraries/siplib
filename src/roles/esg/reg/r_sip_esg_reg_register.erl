%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 19.01.2018
%%% @doc Register operations of reg_srv

-module(r_sip_esg_reg_register).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([on_retry_reg/2,
         on_reg_result_async/2,
         on_reg_result/2,
         on_reg_response/2,
         on_resolve_result/2,
         do_attach/3,
         do_detach/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_esg.hrl").
-include("../include/r_sip_esg_reg.hrl").
-include("../include/r_sip_headers.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% -------------------------------------
%% handle timer event to retry register
%% makes new attempt to register
%% -------------------------------------
on_retry_reg({RegId, Ref}, #{}=State) ->
    ?DLOG("ExtReg. RegRetry ~p", [RegId]),
    E = maps:get(ets,State),
    case ets:lookup(E, RegId) of
        [] -> ok;
        [{_,#{regstate:='retry',ref:=Ref}=Map}] ->
            AInfo = maps:get(accountinfo,Map),
            do_attach(RegId, AInfo, State)
    end.

%% -------------------------------------
%% handle async register result (from nksip)
%% -------------------------------------
-spec on_reg_result_async({reg_key(), reference(), SipCode::integer()}, #state{}) -> ok.
%% -------------------------------------
on_reg_result_async({RegKey, Ref, SipCode}, #{}=State) ->
    E = maps:get(ets,State),
    {ok,RegId} = ?RUTILS:parse_reg_id(RegKey),
    case ets:lookup(E, RegId) of
        [] -> ok;
        [_] when SipCode < 300 -> ok;
        [{_,#{regstate:='registered',ref:=Ref}=Map}] ->
            ?LOG("Registration (prolong) failed. Retrying..."),
            do_detach(RegId, State, true),
            AInfo = maps:get(accountinfo,Map),
            do_attach(RegId, AInfo, State);
        _ -> ok
    end.

%% -------------------------------------
%% handle account start register async result
%% -------------------------------------
on_reg_result({RegId, 'disabled'=Result, Ref}, #{}=State) ->
    ?DLOG("ExtReg. RegResult ~p ~p", [RegId, Result]),
    E = maps:get(ets,State),
    case ets:lookup(E,RegId) of
        [] -> do_detach(RegId, State, true);
        [{_,#{regstate:='wait_result',ref:=Ref}=Map}] ->
            Map1 = ?REGSTATE:set_regstate(Result,RegId,Map),
            ets:insert(E,{RegId,Map1}),
            % ping
            ?PING:start_ping(RegId, Map1, State)
    end;
%%
on_reg_result({RegId, 'registered'=Result, Ref, ProxyN}, #{}=State) ->
    ?DLOG("ExtReg. RegResult ~p ~p", [RegId, Result]),
    E = maps:get(ets,State),
    case ets:lookup(E,RegId) of
        [] -> do_detach(RegId, State, true);
        [{_,#{regstate:='wait_result',ref:=Ref}=Map}] ->
            Map1 = ?REGSTATE:set_regstate(Result,RegId,Map#{proxy_num:=ProxyN}),
            ets:insert(E,{RegId,Map1}),
            % ping
            ?PING:start_ping(RegId, Map1, State)
    end;
%% account start reg async result (false)
on_reg_result({RegId, Result, Ref}, #{}=State) when Result=='false'; Result=='error' ->
    ?DLOG("ExtReg. RegResult ~p ~p", [RegId, Result]),
    E = maps:get(ets,State),
    case ets:lookup(E,RegId) of
        [] -> do_detach(RegId, State, true);
        [{_,#{regstate:='wait_result',ref:=Ref}=Map}] ->
            do_detach(RegId, State, true),
            Map1 = ?REGSTATE:set_regstate('retry',RegId,Map#{timerref:=erlang:send_after(?TimeoutRetryReg, self(), {retry_reg,{RegId,Ref}})}),
            ets:insert(E,{RegId,Map1}); % @todo if proc restart
        _ ->
            ?DLOG("ExtReg. RegResult unexpected", [])
    end;
%%
on_reg_result(Params, _) ->
    ?LOG("ExtReg. unknown reg_result: (~120p)", [Params]).

%% -------------------------------------
%% handle account register response
%% RP-1247
%% -------------------------------------
on_reg_response({_RegId, undefined}, _State) -> ok;
on_reg_response({RegId, Response}, #{}=State) ->
    ?DLOG("ExtReg. RegResponse ~p ~120p", [RegId, Response]),
    #sipmsg{class={resp,Code,_Reason}}=Response,
    E = maps:get(ets,State),
    case ets:lookup(E,RegId) of
        [] -> ok;
        [{_,#{regstate:='registered',ref:=Ref}=Map}] when Code>=300 ->
            do_detach(RegId, State, true),
            Map1 = ?REGSTATE:set_regstate('retry',RegId,Map#{timerref:=erlang:send_after(?TimeoutRetryReg, self(), {retry_reg,{RegId,Ref}})}),
            ets:insert(E,{RegId,Map1}); % @todo if proc restart
        _ -> ok         
    end.

%% -------------------------------------
%% handle timer event to retry register
%% makes new attempt to register
%% -------------------------------------
on_resolve_result({_RegId, 'error', _Ref}, _State) -> ok;
on_resolve_result({RegId, Addrs, Ref}, #{}=State) ->
    ?DLOG("ExtReg. RegResolved ~p -> ~140p", [RegId, Addrs]),
    E = maps:get(ets,State),
    case ets:lookup(E,RegId) of
        [] -> ok;
        [{_,#{ref:=Ref}=Map}] -> ets:insert(E,{RegId,Map#{resolved:=Addrs}})
    end.

%% -------------------------------------
%% Add provider account to active list
%% -------------------------------------
-spec do_attach(reg_id(), account_item(), #state{}) -> ok.
%% -------------------------------------
do_attach(RegId, AInfo, #{}=State) ->
    % detach previously
    E = maps:get(ets,State),
    case ets:lookup(E,RegId) of
        [{_,_}] -> do_detach(RegId, State, false);
        [] -> ok
    end,
    case ?GARBAGE:check_garbage(RegId,State) of
        true -> ?DLOG("ExtReg .. do attach ~p skipped, wait for garbage", [RegId]);
        false ->
            ?DLOG("ExtReg .. do attach ~p", [RegId]),
            do_attach_1(RegId,AInfo,State),
            ok
    end.

%% @private
do_attach_1(RegId, {reg,_,_,AMap}=AInfo,#{}=State) ->
    E = maps:get(ets,State),
    RegKey = ?RUTILS:get_reg_key(RegId),
    GenSrv = ?EXT_REGSRV:srvname(),
    Ref = make_ref(),
    %
    User = maps:get(username,AMap),
    Domain = maps:get(domain,AMap),
    Pass = maps:get(pwd,AMap),
    DoReg = maps:get(reg,AMap),
    %
    F = fun() ->
                ?LOG("Resolving ~120p ...", [RegId]),
                % resolve
                case ?RUTILS:get_all_addrs(AMap) of
                    {error,_} ->
                        ?LOG("Resolve error: ~120p. Skip register, auto retry later", [RegId]),
                        gen_server:cast(GenSrv, {resolve_result, {RegId, 'error', Ref}}),
                        gen_server:cast(GenSrv, {reg_result, {RegId, 'error', Ref}});
                    {ok,Addrs} ->
                        ?LOG("Resolve ok: ~120p ~n\t ~120p", [RegId, Addrs]),
                        gen_server:cast(GenSrv, {resolve_result, {RegId, Addrs, Ref}}),
                        % register
                        case ?EU:to_bool(DoReg) of
                            false ->
                                ?LOG("Register disabled, p2p: ~120p", [RegId]),
                                gen_server:cast(GenSrv, {reg_result, {RegId, 'disabled', Ref}});
                            true ->
                                ?LOG("Registering ~120p ...", [RegId]),
                                case catch ?UAC:start_register(RegKey, AMap, {GenSrv,Ref}) of
                                    {ok, true, ProxyN} ->
                                        ?LOG("Register ok: ~120p", [RegKey]),
                                        gen_server:cast(GenSrv, {reg_result, {RegId, 'registered', Ref, ProxyN}});
                                    {ok, false} ->
                                        ?LOG("Register failed: ~120p. Auto retry later", [RegId]),
                                        gen_server:cast(GenSrv, {reg_result, {RegId, 'false', Ref}});
                                    {error,_}=Err ->
                                        ?LOG("Register error: ~120p -> ~120p. Auto retry later", [RegId, Err]),
                                        gen_server:cast(GenSrv, {reg_result, {RegId, 'error', Ref}});
                                    {'EXIT',Err} ->
                                        ?LOG("Register exception: ~120p -> ~120p. Auto retry later", [RegId, Err]),
                                        gen_server:cast(GenSrv, {reg_result, {RegId, 'error', Ref}}),
                                        ?UAC:stop_register(RegId)
                                end end end end,
    ?EU:spawn_fun(F),
    Map = ?REGSTATE:set_regstate('wait_result',RegId,
                                 #{regid => RegId,
                                   regkey => RegKey,
                                   ref => Ref,
                                   pingstate => undefined,
                                   pingref => undefined,
                                   ping_count => {10,0},% ok,err from last change
                                   timerref => undefined,
                                   proxy_num => 0,
                                   user => User,
                                   domain => Domain,
                                   pass => Pass,
                                   localport => ?U:get_local_port(),
                                   resolved => [],
                                   ext_masks => ?RUTILS:get_ext_masks(AMap),
                                   accountinfo => AInfo}),
    ets:insert(E, {RegId, Map}),
    {ok,RegKey}.

%% -------------------------------------
%% Delete provider account from active list
%% -------------------------------------
do_detach(RegId, #{}=State, DoUnreg) ->
    ?DLOG("ExtReg .. do detach ~p", [RegId]),
    E = maps:get(ets,State),
    case ets:lookup(E, RegId) of
        [] -> ok;
        [{_, #{}=Map}] ->
            ets:delete(E, RegId),
            ?EU:cancel_timer(maps:get(timerref,Map)),
            RegKey = maps:get(regkey,Map),
            case DoUnreg of
                true -> ?UAC:stop_register(RegKey);
                false -> ok
            end,
            ?REGSTATE:set_regstate('detached',RegId,Map) % RP-1247
    end,
    ?GARBAGE:move_to_garbage(RegId,State),
    ok.
