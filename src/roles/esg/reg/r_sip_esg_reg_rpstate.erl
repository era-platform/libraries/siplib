%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 15.03.2019
%%% @doc Set reg & ping state to registration RP-1247  

-module(r_sip_esg_reg_rpstate).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([check_regalive/1,
         set_regstate/3,
         set_pingstate/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_esg.hrl").
-include("../include/r_sip_esg_reg.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% --------------------------------
-spec check_regalive(Map::map()) -> ok | {error,Reason::binary()}.
%% --------------------------------
check_regalive(Map) ->
    RegState = maps:get(regstate,Map),
    PingState = maps:get(pingstate,Map),
    {OkC,_ErC} = maps:get(ping_count,Map),
    case {RegState,PingState} of
        {'disabled','disabled'} -> ok;
        {'disabled','ok'} when OkC>=3 -> ok;
        {'disabled','error'} when OkC>0 -> ok;
        {'disabled',_} -> {error, <<"pingstate">>};
        {'registered','disabled'} -> ok;
        {'registered','ok'} when OkC>=3 -> ok;
        {'registered','error'} when OkC>0 -> ok;
        _ -> {error,<<"regstate">>}
    end.

%% --------------------------------
-spec set_regstate(NewRegState::reg_state(), RegId::reg_id(), Map::map()) -> NewMap::map().
%% --------------------------------
set_regstate(NewRegState,RegId,Map) ->
    RegState = maps:get(regstate,Map,undefined),
    PingState = maps:get(pingstate,Map,undefined),
    case RegState of
        NewRegState -> Map;
        _ ->
            Map1 = Map#{regstate=>NewRegState},
            EvState0 = make_evstate(RegState,PingState),
            EvStateN = make_evstate(NewRegState,PingState),
            case {EvState0,EvStateN} of
                {A,A} -> ok;
                {_,_} -> send_event(EvStateN,RegId,Map)
            end,
            Map1
    end.
    
%% --------------------------------
-spec set_pingstate(NewPingState::ping_state(), RegId::reg_id(), Map::map()) -> NewMap::map().
%% --------------------------------
set_pingstate(NewPingState,RegId,Map) ->
    RegState = maps:get(regstate,Map,undefined),
    PingState = maps:get(pingstate,Map,undefined),
    case PingState of
        NewPingState -> Map;
        _ ->
            Map1 = Map#{pingstate=>NewPingState},
            EvState0 = make_evstate(RegState,PingState),
            EvStateN = make_evstate(RegState,NewPingState),
            case {EvState0,EvStateN} of
                {A,A} -> ok;
                {_,_} -> send_event(EvStateN,RegId,Map)
            end,
            Map1
    end.

%% @private
make_evstate('disabled','undefined') -> 'wait_result';
make_evstate('disabled','disabled') -> 'undefined';
make_evstate('disabled',PingState) -> PingState;
make_evstate('registered','error') -> 'error';
make_evstate('wait_result'=RegState,_) -> RegState;
make_evstate('retry'=RegState,_) -> RegState;
make_evstate('registered'=RegState,_) -> RegState;
make_evstate('detached'=RegState,_) -> RegState;
make_evstate(_,_) -> 'undefined'.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -------------------------
-spec send_event(NewState::registered|retry|wait_result|ok|error|undefined|detached,RegId::reg_id(),Map::map()) -> ok.
%% -------------------------
send_event(NewState,_RegId,Map) -> 
    ?ESG_EVENT:provider_state_changed(Map#{regstate=>NewState}).

