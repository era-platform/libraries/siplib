%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Ext registration server.
%%%      Initiates register of provider accounts, make pings
%%%      Checks if account is registered
%%%      Checks if account is found by request-attributes
%%%      RegState for every register: wait_result, retry, disabled, registered

-module(r_sip_esg_reg_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1,
         update_opts/1,
         print/0,
         srvname/0,
         get_ets/0, % for stat
         check_ruri/2, check_ruri/3,
         check_incoming_extaddr/2,
         rpc_check_auth/3,
         rpc_check_reg/1,
         rpc_get_active_trunk_count/1,
         rpc_check_limit_inner/1,
         rpc_get_local_addr/1,
         rpc_get_provider_addr/1,
         on_reg_response/2]).

-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_esg.hrl").
-include("../include/r_sip_esg_reg.hrl").

% -include("../include/r_nksip.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

start_link(StartArgs) ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, StartArgs, []).

%% ----------------------
update_opts(_Opts) ->
    gen_server:cast(?MODULE, ?InitRegs).

%% ----------------------
print() ->
    gen_server:cast(?MODULE, {print}).

%% ----------------------
srvname() -> ?MODULE.

%% ----------------------
get_ets() ->
    gen_server:call(?MODULE, {get_ets}).


%% ----------------------
check_ruri(User,Domain) ->
    check_ruri(User,Domain,[]).

check_ruri(User,Domain,FilterOpts) ->
    gen_server:call(?MODULE, {check_ruri, {User,Domain,FilterOpts}}).

%% ----------------------
check_incoming_extaddr(Addr,AMap) ->
    gen_server:call(?MODULE, {check_incoming_extaddr, {Addr,AMap}}).

%% ----------------------
rpc_check_auth(ToUser, _ToDomain, CallId) ->
    case ?DLG_STORE:pull_dlg(CallId) of
        false ->
            case ?SIPSTORE:find_t(CallId) of
                {_,{ToUser,{_AccUser,_AccDomain}}} -> true;
                _ -> false
            end;
        _Dlg -> true
    end.

%% ----------------------
rpc_check_reg(Acc) ->
    gen_server:call(?MODULE, {check_reg, Acc}).

%% ----------------------
rpc_get_active_trunk_count(Acc) ->
    gen_server:call(?MODULE, {get_active_trunk_count, Acc}).

%% ----------------------
rpc_check_limit_inner(Acc) ->
    gen_server:call(?MODULE, {rpc_check_limit_inner, Acc}).

%% ----------------------
rpc_get_local_addr(_Acc) ->
    % ?EU:to_binary(inet:ntoa(?U:get_host_addr())).
    ?CFG:get_my_sipserver_addr().

%% ----------------------
%% RP-713 callback from nksip_auto_register on every register response
on_reg_response(RegKey, Response) ->
    gen_server:cast(?MODULE, {reg_response, RegKey, Response}).

%% ----------------------
%% RP-2078 get addr for define fork mg alias
rpc_get_provider_addr(Provider) ->
    gen_server:call(?MODULE, {rpc_get_provider_addr, Provider}).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------------------------------------
%% Init
%% ------------------------------------------------------------
init(Opts) ->
    ?MONITOR:start_monitor(self(), "Sip esg reg", [fun(R) -> ?OUT("Proc ExtReg terminated: ~120p", [R]) end]),
    [RegETS] = ?EU:extract_required_props([ets], Opts),
    State = #{name => ?MODULE,
              ets => RegETS,
              nataddrs => [],
              natgs => undefined,
              extaddrs => [],
              initref => undefined},
    {ok,State1} = ?GARBAGE:init_garbage(Opts,State),
    gen_server:cast(?MODULE, ?InitRegs),
    ?OUT("ExtReg. inited (~p, ~p)", [self(), Opts]),
    {ok, State1}.

%% ------------------------------------------------------------
%% Call
%% ------------------------------------------------------------

%% ------------------------------------------
%% checks if ruri exists in accounts
%% ------------------------------------------
handle_call({check_ruri, {User,Domain,FilterOpts}}, _From, State) ->
    E = maps:get(ets, State),
    EsgNatAddrs = maps:get(nataddrs, State, []),
    EsgExtAddrs = maps:get(extaddrs, State, []),
    Fx = fun(FunCheck) when is_function(FunCheck,1) ->
                 F = fun(_,{true,_}=Acc) -> Acc;
                        ({_,#{}=Map},Acc) ->
                             {reg,_,_,AMap}=maps:get(accountinfo,Map),
                             AUserNames = [maps:get(user,Map) | maps:get(<<"extusernames">>, maps:get(opts,AMap), [])],
                             case lists:member(User,AUserNames)
                                 andalso lists:member(maps:get(regstate,Map), ['disabled','registered'])
                             of
                                 false -> Acc;
                                 true ->
                                     case FunCheck(Map) of
                                         false -> Acc;
                                         true ->
                                             case FilterOpts of
                                                 [] -> {true,AMap};
                                                 _ ->
                                                     case lists:foldl(fun(_,false) -> false;
                                                                         ({K,V},_) -> maps:get(catch ?EU:to_atom(K),AMap,undefined)==V
                                                                      end, true, FilterOpts)
                                                     of
                                                         true -> {true,AMap};
                                                         false -> Acc
                                                     end end end end end,
                 ets:foldl(F,false,E)
         end,
    % first, check addrs for all providers,
    % then if not, check masks (it's re: alg)
    Faddrs = fun(Map) -> lists:member(Domain,maps:get(resolved,Map)) % resolved proxy,domain of provider account and known extaddrs of provider account (for contact check)
                             orelse lists:member(Domain,EsgExtAddrs) % ext addrs of current esg server
                             orelse lists:keymember(Domain,1,EsgNatAddrs) end, % nat addrs of current esg server

    Fmasks = fun(Map) -> lists:any(fun(MaskRe) ->
                                           case re:run(Domain,MaskRe,[]) of
                                               nomatch -> false; _ -> true
                                           end end, maps:get(ext_masks,Map)) end, % known ip-addrs masks of provider account (for contact check)
    Reply = case Fx(Faddrs) of
                false -> Fx(Fmasks);
                R -> R
            end,
    {reply, Reply, State};

%% ------------------------------------------
%% checks if received addr is known by account
%% ------------------------------------------
handle_call({check_incoming_extaddr, {Addr,AMap}}, _From, State) ->
    Reply = case lists:member(Addr, [maps:get(domain,AMap), maps:get(proxyaddr,AMap)]) of
                true -> ok;
                false ->
                    E = maps:get(ets, State),
                    RegId = ?RUTILS:get_reg_id(AMap),
                    case ets:lookup(E,RegId) of
                        [] -> {error, ?EU:strbin("Account not found in local registry (~120p)",[RegId])};
                        [{_,Map}] ->
                            Resolved = maps:get(resolved,Map),
                            case lists:member(Addr,Resolved) of
                                true -> ok;
                                false ->
                                    % known ip-addrs masks of provider account (for contact check)
                                    F = fun(MaskRe) ->
                                                case re:run(Addr,MaskRe,[]) of
                                                    nomatch -> false;
                                                    _ -> true
                                                end end,
                                    case lists:any(F, maps:get(ext_masks,Map)) of
                                        true -> ok;
                                        false -> {error, ?EU:strbin("Remote addr (~120p) is not defined in account addrs (~120p)",[Addr, Resolved])}
                                    end end end end,
    {reply, Reply, State};

%% ------------------------------------------
%% checks if account can be used (registration is active, ping normal)
%% ------------------------------------------
handle_call({check_reg, AMap}, _From, #{}=State) ->
    E = maps:get(ets,State),
    RegId = ?RUTILS:get_reg_id(AMap),
    Reply = case ets:lookup(E,RegId) of
                [] -> {error, <<"not_found">>};
                [{_,Map}] ->
                    case ?REGSTATE:check_regalive(Map) of
                        ok ->
                            case maps:get(resolved,Map) of
                                [] -> {error, <<"not_resolved">>};
                                [_Addr|_] -> ok
                            end;
                        {error,_}=Err -> Err
                    end end,
    {reply, Reply, State};

%% ------------------------------------------
%% returns count of active trunks on provider account
%% ------------------------------------------
handle_call({get_active_trunk_count, AMap}, _From, #{}=State) ->
    Count = ?EXT_UTILS:get_active_trunk_count(AMap),
    {reply, {ok,Count}, State};

%% ------------------------------------------
%% check if inner outgoing call could be initiated (of active trunks on provider account and trunksout property)
%% ------------------------------------------
handle_call({rpc_check_limit_inner, AMap}, _From, #{}=State) ->
    Res = ?EXT_UTILS:check_limit(inner, AMap),
    {reply, {ok,Res}, State};

%% ------------------------------------------
%% ------------------------------------------
handle_call({rpc_get_provider_addr, Provider}, _From, #{}=State) ->
    E = maps:get(ets,State),
    RegId = ?RUTILS:get_reg_id(Provider),
    Reply = case catch ets:lookup(E,RegId) of
                {'EXIT',_}=Exit ->
                    ?LOG("get_provider_addr error: ~120tp",[Exit]),
                    {error, <<"not_found">>};
                [] -> {error, <<"not_found">>};
                [{_,Map}] ->
                    case maps:get(resolved,Map) of
                        [] -> {error, <<"not_resolved">>};
                        [Addrs|_] -> Addrs
                    end
            end,
    {reply, {ok,Reply}, State};

%% ------------------------------------------
%% get all registrations
%% ------------------------------------------
handle_call({get_ets}, _From, #{}=State) ->
    Reply = maps:get(ets,State),
    {reply, Reply, State};

%% ------------------------------------------
%% other
%% ------------------------------------------
handle_call(_Request, _From, State) ->
    ?LOG("ExtReg. handle_call(~p)", [_Request]),
    Reply = ok,
    {reply, Reply, State}.

%% ------------------------------------------------------------
%% Cast
%% ------------------------------------------------------------

%% initialize ext registrations
handle_cast(?InitRegs, #{}=State) ->
    State1 = refresh_extaddrs(State),
    State2 = case maps:get(initref,State1) of
                 undefined -> refresh_on_restart(State1);
                 _ -> State1
             end,
    init_regs(State2),
    State3 = State2#{initref := erlang:send_after(?TimeoutCheckSettings, self(), ?CheckSettings)},
    {noreply, State3};

%% ------------------------------------------
%% set nat addr
%% ------------------------------------------
handle_cast({set_nat_addr,Addr}, #{}=State) ->
    {ok,State1} = set_nat_addr(Addr,State),
    {noreply, State1};

%% ------------------------------------------
%% account resolved
%% ------------------------------------------
handle_cast({resolve_result,Params}, #{}=State) ->
    ?REGISTER:on_resolve_result(Params,State),
    {noreply, State};

%% ------------------------------------------
%% account start reg async result (disabled, registered)
%% ------------------------------------------
handle_cast({reg_result,Params}, #{}=State) ->
    ?REGISTER:on_reg_result(Params,State),
    {noreply, State};

%% ------------------------------------------
%% account start ping async result
%% ------------------------------------------
handle_cast({ping_result,{_,_,_}=Res}, #{}=State) ->
    ?PING:on_ping_result(Res,State),
    {noreply, State};

%% ------------------------------------------
%% RP-713 callback from nksip_auto_register on every register response
%%   Response::#sipmsg{} | undefined (if timeout or connection error)
%% ------------------------------------------
handle_cast({reg_response, _, undefined}, State) -> {noreply, State};
handle_cast({reg_response, RegKey, Response}, #{}=State) ->
    E = maps:get(ets,State),
    {ok,RegId} = ?RUTILS:parse_reg_id(RegKey),
    case ets:lookup(E, RegId) of
        [] -> ok;
        [{_,#{}=Map}] ->
            ets:insert(E, {RegId,Map#{last_response => Response}}),
            ?REGISTER:on_reg_response({RegId, Response},State)
    end,
    {noreply, State};

%% ------------------------------------------
%% print state
%% ------------------------------------------
handle_cast({print}, #{}=State) ->
    ?OUT("ExtReg. state: ~120p", [State]),
    {noreply, State};

%% restart
handle_cast({restart}, #{}=State) ->
    {stop, restart, State};

%%
handle_cast({}, #{}=State) ->
    {noreply, State};

%% other
handle_cast(_Request, State) ->
    ?LOG("ExtReg. handle_cast(~p)", [_Request]),
    {noreply, State}.

%% ------------------------------------------------------------
%% Info
%% ------------------------------------------------------------

%% %% when init process fin
%% handle_info({'DOWN',Ref,process,_Pid,_Reason}, #{initref:=Ref}=State) ->
%%     TimerRef = erlang:send_after(?TimeoutCheckSettings, self(), ?CheckSettings),
%%     {noreply, State#{initref:=TimerRef}};

%% ------------------------------------------
%% account retry reg
%% ------------------------------------------
handle_info({retry_reg,Params}, #{}=State) ->
    ?REGISTER:on_retry_reg(Params,State),
    {noreply, State};

%% ------------------------------------------
%% account ping
%% ------------------------------------------
handle_info({ping,Params}, #{}=State) ->
    ?PING:send_ping(Params,State),
    {noreply, State};

%% ------------------------------------------
%% reg result async
%% ------------------------------------------
handle_info({reg_result_async, Params}, #{}=State) ->
    ?REGISTER:on_reg_result_async(Params,State),
    {noreply, State};

%% ------------------------------------------
%% check settings
%% ------------------------------------------
handle_info(?CheckSettings, State) ->
    gen_server:cast(?MODULE, ?InitRegs),
    {noreply, State};

%% ------------------------------------------
%% other
%% ------------------------------------------
handle_info(_Info, State) ->
    ?LOG("ExtReg. handle_info(~p)", [_Info]),
    {noreply, State}.

%% ------------------------------------------------------------
%% Terminate
%% ------------------------------------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------------------------------------
%% Code change
%% ------------------------------------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ----------------------------------
%% Load and initialize provider accounts
%% ----------------------------------
init_regs(State) ->
    case ?CFG:check_domain_master() of
        false -> ok;
        true ->
            Regs = ?RUTILS:get_regs(),
            case Regs of
                {error,_R} ->
                    ?DLOG("ExtReg. DC found unaccessible. Refresh was delayed. ~s",[?EU:to_binary(_R)]);
                Regs when is_list(Regs) ->
                    F = fun({reg,_,_,AMap}=AInfo, Acc) ->
                                RegId = ?RUTILS:get_reg_id(AMap),
                                case lists:keyfind(RegId,1,Acc) of
                                    false -> [{RegId,AInfo}|Acc];
                                    _ -> Acc
                                end end,
                    ?GARBAGE:seed_garbage(State),
                    Regs1 = lists:foldl(F, [], Regs),
                    detach_deleted_regs(Regs1, State),
                    init_regs(Regs1, State)
            end end.

%% ----------------------------------
%% Stop provider accounts that not found in
%% ----------------------------------
detach_deleted_regs(Regs, #{}=State) ->
    Eregs = maps:get(ets,State),
    F = fun({RegId,_AInfo}, Acc) ->
                case lists:keyfind(RegId, 1, Regs) of
                    false ->
                        ?REGISTER:do_detach(RegId, State, true), Acc;
                    _ -> Acc
                end end,
    ets:foldl(F, [], Eregs).

%% ----------------------------------
init_regs([], _) -> ok;
init_regs([{RegId,{reg,_,_,AMap}=AInfo}|Rest], #{}=State) ->
    E = maps:get(ets,State),
    case ets:lookup(E, RegId) of
        [] ->
            ?REGISTER:do_attach(RegId, AInfo, State),
            pause();
        [{_,#{}=M}] ->
            {reg,_,_,Map} = maps:get(accountinfo,M),
            case ?RUTILS:compare_opts(Map, AMap) of
                true -> ok;
                false ->
                    ?REGISTER:do_detach(RegId, State, true),
                    ?REGISTER:do_attach(RegId, AInfo, State),
                    pause()
            end end,
    init_regs(Rest, State).

%% @private
pause() ->
    case erlang:phash2(make_ref()) rem 10 of
        0 -> timer:sleep(20);
        _ -> ok
    end.

%% -------------------------------------
%% Refresh timers on restart of gen_srv
%% -------------------------------------
refresh_on_restart(State) ->
    E = maps:get(ets,State),
    Frefresh = fun({RegId,Map},_) ->
                       Fping = fun() -> PingState = maps:get(pingstate,Map),
                                        case PingState of
                                            disabled -> ok;
                                            _ -> ?PING:start_ping(RegId, Map, State)
                                        end end,
                       case maps:get(regstate,Map) of
                           disabled -> Fping();
                           registered -> Fping();
                           wait_result ->
                               {X,Y,Z,AMap} = maps:get(accountinfo,Map),
                               ets:insert(E,{RegId,Map#{accountinfo:={X,Y,Z,AMap#{restart => true}}}});
                           retry ->
                               Ref = maps:get(ref,Map),
                               ets:insert(E,{RegId,Map#{timerref:=erlang:send_after(?TimeoutRetryReg, self(), {retry_reg,{RegId,Ref}})}})
                       end end,
    ets:foldl(Frefresh, ok, E),
    State.

%% -------------------------------------
%% Refresh ext addresses of current server
%% -------------------------------------
refresh_extaddrs(State) ->
    % ext addrs from opts and current node
    Addrs = ?EXT_UTILS:get_ext_addrs(),
    State1 = State#{extaddrs := Addrs},
    % nat addrs by stun request to servers from opts
    refresh_nataddrs(State1).

%% @private
refresh_nataddrs(State) ->
    NowGS = ?EU:current_gregsecond(),
    NatGS = maps:get(natgs, State),
    NatAddrs = maps:get(nataddrs, State),
    % filter expired
    State1 = State#{nataddrs := lists:filter(fun({_,GS}) when GS+180>NowGS -> true; (_) -> false end, NatAddrs)},
    CheckNat = case NatGS of
                   undefined -> async;
                   _ -> case NowGS - NatGS of
                            _N when _N > 60 -> async;
                            _ -> false
                        end end,
    case CheckNat of
        false -> State1;
        async ->
            Self = self(),
            Fset = fun(Addr) -> gen_server:cast(Self, {set_nat_addr,Addr}) end,
            ?EXT_UTILS:get_nat_addrs_async(Fset),
            State1#{natgs := NowGS}
    end.

%% @private
set_nat_addr(Addr,State) ->
    NatAddrs = maps:get(nataddrs,State),
    NowGS = ?EU:current_gregsecond(),
    {ok, State#{nataddrs := [{Addr,NowGS}|lists:keydelete(Addr, 1, NatAddrs)]}}.

% ----------------------------------
%
% ----------------------------------
