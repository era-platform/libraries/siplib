%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Provides register start/stop service routines

-module(r_sip_esg_reg_uac).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([start_register/3,
         stop_register/1,
         make_route_ping/2]).

%% ==========================================================================
%% Define
%% ==========================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_esg.hrl").
-include("../include/r_sip_esg_reg.hrl").

-define(REGSRV, r_sip_esg_reg_srv).

%% ====================================================================
%% API functions
%% ====================================================================

%% ---------------------
%% starts auto register, auto auth, auto reregister on expire
%% ---------------------
-spec start_register(reg_key(), map(), subscriber()) -> {ok,true,ProxyN::integer()} | {ok,false} | {'EXIT',R::term()}.
%% ---------------------
start_register(RegKey, AccountMap, Subscriber) ->
    start_register(RegKey, AccountMap, Subscriber, 0).

%% @private #285
start_register(RegKey, AccountMap, Subscriber, ProxyN) ->
    ?DLOG("ESG. StartRegister ~120p proxy ~p", [RegKey, ProxyN]),
    case make_opts_reg(AccountMap, ProxyN) of
        {_RUri,_Opts}=X ->
            case start_register_1(X, RegKey, Subscriber) of
                {ok, true} -> {ok, true, ProxyN};
                _Res ->
                    ?DLOG("ESG. StartRegister ~120p result : ~120p", [RegKey, _Res]),
                    catch stop_register(RegKey),
                    start_register(RegKey, AccountMap, Subscriber, ProxyN+1)
            end;
        undefined -> {ok, false}
    end.

%% @private #285
start_register_1({RUri, Opts}, RegKey, {Sub,SubRef}) ->
    Opts1 = [{subscriber,Sub}, % #285
             {subscriber_ref,SubRef}, % #285
             user_agent,
             {fun_notify_response, fun(Response) -> ?REGSRV:on_reg_response(RegKey, Response) end}, % RP-713
             {call_id,?U:luid()} % 01.09.2020
            | Opts],
    {_,SipApp} = ?ENV:get_env('sipapp'),
    ?DLOG("RUri: ~120p,~n\tOpts: ~120p", [RUri, Opts]),
    case catch nksip_uac_auto_register:start_register(SipApp, RegKey, RUri, Opts1) of
        {ok, true}=R ->
            ?DLOG("ESG. StartRegister ~120p result: TRUE", [RegKey]),
            R;
        {ok, false}=R ->
            ?DLOG("ESG. StartRegister ~120p result: FALSE", [RegKey]),
            R;
        {error, _}=E ->
            ?DLOG("ESG. StartRegister ~120p result: ERROR", [RegKey]),
            E;
        {'EXIT',_}=E ->
            ?DLOG("ESG. StartRegister ~120p result: EXIT", [RegKey]),
              E
    end.

%% ---------------------
%% stops auto register, send unregister
%% ---------------------
-spec stop_register(reg_key()) -> term().
%% ---------------------
stop_register(RegKey) ->
    {_,SipApp} = ?ENV:get_env('sipapp'),
    Res = nksip_uac_auto_register:stop_register(SipApp, RegKey),
    ?DLOG("ESG. StopRegister ~120p result: ~120p", [RegKey, Res]),
    Res.

%% ---------------------
%% makes route for ping
%% ---------------------
make_route_ping(Map,ProxyN) ->
    case maps:get(pingsrv,Map,<<>>) of
        <<>> -> make_route(Map,ProxyN);
        B when is_binary(B) ->
            [A,P] = binary:split(B, <<":">>),
            Domain = maps:get(domain,Map),
            Transport = maps:get(transport,Map,udp),
            RUri = #uri{scheme=sip,
                        domain=Domain},
            RouteUri = #uri{scheme=sip,
                            domain=A,
                            port=case ?EU:to_int(P) of 5060 -> 0; Port -> Port end,
                            opts=case Transport of udp -> []; _ -> [{<<"transport">>,Transport}] end},
            RouteOpts = [{route, ?U:unparse_uri(RouteUri#uri{opts=RouteUri#uri.opts ++ [<<"lr">>]})}],
            {RUri,RouteOpts,RouteUri}
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

% @private
% prepare opts from account map for auto register module
%  uses N-th outbound proxy
make_opts_reg(Map,ProxyN) ->
    case make_route(Map,ProxyN) of
        undefined -> undefined;
        {RUri,RouteOpts,#uri{opts=RUOpts}} ->
            User = maps:get(username,Map),
            Domain = maps:get(domain,Map),
            %
            [Transport] = ?EU:extract_optional_default([{<<"transport">>,maps:get(transport,Map,udp)}], RUOpts),
            LocalAddr = ?EXT_UTILS:build_local_contact_addr(Map),
            LocalPort = ?U:get_local_port(Transport),
            %
            Expires = maps:get(expires,Map,300),
            %
            {RUri,RouteOpts,_} = make_route(Map,ProxyN),
            %
            To = #uri{scheme=sip,
                      %disp = <<"\"",Display/bitstring,"\"">>,
                      user = User,
                      domain = Domain},
            %
            <<LTag:48/bitstring, _/bitstring>> = ?U:luid(),
            From = To#uri{ext_opts=[{<<"tag">>, LTag}]},
            %
            Contact = To#uri{domain= ?EU:to_binary(inet:ntoa(LocalAddr)),
                             port= LocalPort,
                             opts= case Transport of
                                       udp -> [];
                                       _ -> [{<<"transport">>, ?EU:to_binary(string:to_upper(?EU:to_list(Transport)))}]
                                   end},
            %
            LoginOpts = ?ESG_UTILS:build_auth_opts_ext(Map),
            Opts1 = [{to, To},
                     {from, From},
                     {contact, Contact},
                     {expires, Expires}
                    |LoginOpts++RouteOpts],
            {RUri, Opts1}
    end.

% @private
% makes request uri and route header by destination address
% default proxy|domain
make_route(Map,0) ->
    Domain = maps:get(domain,Map),
    ProxyAddr = maps:get(proxyaddr,Map,Domain),
    ProxyPort = maps:get(proxyport,Map,5060),
    Transport = maps:get(transport,Map,udp),
    make_route({Domain,ProxyAddr,ProxyPort,Transport});
% alternative proxy (proxy_n>=1) #285
make_route(Map,ProxyN) when is_integer(ProxyN), ProxyN>0 ->
    ProxyA = maps:get(alternative_proxies,Map),
    case length(ProxyA)>=ProxyN of
        false -> undefined;
        true ->
            Domain = maps:get(domain,Map),
            {Transport,ProxyAddr,ProxyPort} = lists:nth(ProxyN, ProxyA),
            make_route({Domain,ProxyAddr,ProxyPort,Transport})
    end.

% @private
make_route({Domain,ProxyAddr,ProxyPort,Transport}) ->
    RouteUri = #uri{scheme=sip,
                    port=case ProxyPort of 5060 -> 0; P -> P end,
                    opts=case Transport of udp -> []; _ -> [{<<"transport">>,Transport}] end},
    case Domain==ProxyAddr of
        true ->
            RUri = RouteUri#uri{domain=Domain},
            RouteUri1 = RUri,
            RouteOpts = [];
        false ->
            RUri = #uri{scheme=sip,
                        domain=Domain},
            RouteUri1 = RouteUri#uri{domain=ProxyAddr},
            RouteOpts = [{route, ?U:unparse_uri(RouteUri1#uri{opts=RouteUri1#uri.opts ++ [<<"lr">>]})}]
    end,
    {RUri,RouteOpts,RouteUri1}.
