%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 19.01.2018.
%%% @doc Utils for reg_srv

-module(r_sip_esg_reg_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([get_reg_id/1,
         get_reg_key/1,
         parse_reg_id/1,
         get_all_addrs/1,
         get_ext_addrs/1,
         get_ext_masks/1,
         get_regs/0,
         compare_opts/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_esg.hrl").
-include("../include/r_sip_esg_reg.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% -------------------------------------
%% Return RegId by opts
%% -------------------------------------
-spec get_reg_id(Opts::account_item()) -> reg_id().
%% -------------------------------------
get_reg_id(Opts) when is_list(Opts) ->
    [U,D] = ?EU:extract_required_props([user,domain], Opts),
    {U,D};
get_reg_id({reg,_,_,#{}=Map}=_AInfo) -> get_reg_id(Map);
get_reg_id(#{}=AMap) ->
    U = maps:get(username,AMap),
    D = maps:get(domain,AMap),
    {U,D}.

%% -------------------------------------
%% Return RegKey by RegId
%% -------------------------------------
-spec get_reg_key(reg_id()) -> reg_key().
%% -------------------------------------
get_reg_key({_U,_D}=RegId) -> {RegId,?EU:timestamp()}.

%% -------------------------------------
%% Parse RegId from RegKey
%% -------------------------------------
-spec parse_reg_id(RegKey::reg_key()) -> {ok,RegId::reg_id()} | error.
%% -------------------------------------
parse_reg_id({{_,_}=RegId,_TS}) when is_integer(_TS) -> {ok,RegId};
parse_reg_id(_) -> error.

%% -------------------------------------
%% Return resolved ip_address list of provider account (domain,proxy,ext addrs without masks)
%% -------------------------------------
get_all_addrs(AMap) ->
    Domain = maps:get(domain,AMap,undefined),
    Proxy = maps:get(proxyaddr,AMap,undefined),
    DestAddr = case [Domain,Proxy] of
                   [_,undefined] -> Domain;
                   [_,_] -> Proxy
               end,
    ExtAddrs = get_ext_addrs(AMap),
    case inet:getaddrs(?EU:to_list(DestAddr), inet) of
        {error,_}=Err -> Err;
        {ok, ResolveAddrs} ->
            F = fun(IB,Acc) when is_binary(IB) ->
                        case lists:member(IB,Acc) of
                            true -> Acc;
                            false -> [IB|Acc]
                        end end,
            InitialAddrs1 = lists:foldl(F, [Domain], [Proxy|ExtAddrs]),
            % #177 usage of localdomain
            InitialAddrs2 = case ?EU:to_bool(maps:get(reg,AMap)) of
                                true -> InitialAddrs1;
                                false ->
                                    case maps:get(localdomain,AMap,<<>>) of
                                        <<>> -> InitialAddrs1;
                                        LocalDomain -> F(LocalDomain,InitialAddrs1)
                                    end end,
            {ok, lists:foldl(fun(undefined,Acc) -> Acc;
                                (IB,Acc) when is_binary(IB) -> F(IB,Acc);
                                (I,Acc) when is_tuple(I) -> F(?EU:to_binary(inet:ntoa(I)),Acc)
                             end, InitialAddrs2, ResolveAddrs)}
    end.

%% -------------------------------------
%% Return ext known ip_address list of provider account (ext addrs )
%% -------------------------------------
get_ext_addrs(AMap) ->
    lists:filter(fun(Addr) ->
                         case re:run(Addr,<<"[*?X]">>, []) of
                             nomatch -> true;
                             _ -> false
                         end end, maps:get(extaddrs,AMap,[])).

%% -------------------------------------
%% Return ext known ip_address mask list of provider account (ext addrs masks mapped into regex patterns)
%% -------------------------------------
get_ext_masks(AMap) ->
    lists:filtermap(fun(<<"/reg/",Mask/binary>>) -> {true,Mask};
                       (Mask) ->
                            case re:run(Mask,<<"[*?X]">>, []) of
                                nomatch -> false;
                                _ ->
                                    Re = lists:flatten([case X of
                                                            $. -> "\\.";
                                                            $? -> "\\d";
                                                            $X -> "\\d";
                                                            $* -> "\\d{1,3}";
                                                            C -> C
                                                        end || X <- ?EU:to_list(Mask)]),
                                    {true, ?EU:to_binary([$^,Re,$$])}
                            end end, maps:get(extaddrs,AMap,[])).

%% -------------------------------------
%% Load provider_accounts, current_server is responsible for
%% -------------------------------------
-spec get_regs() -> [{reg,Id::binary(),ClusterDomain::binary(),Account::map()}] | {error,Reason::term()}.
%% -------------------------------------
get_regs() ->
    {_,SipAppModule} = ?ENV:get_env('sipappmodule'),
    {ok,AppOpts} = ?APP:get_opts(SipAppModule),
    case ?EU:extract_optional_props([<<"domains">>], AppOpts) of
        [undefined] -> get_regs_1([{<<"domains">>, [?EU:to_binary(?CFG:get_general_domain())]}|AppOpts]);
        _ -> get_regs_1(AppOpts)
    end.

%% --------------------------------------------------------------------------
%% NEW LOGIC OF RESERVING AND AUTOMATIC
%% --------------------------------------------------------------------------
get_regs_1(AppOpts) ->
    try
        Domains = lists:delete(?EU:to_binary(?CFG:get_general_domain()), get_responsible_domains(AppOpts)),
        SrvIdx = ?CFG:get_my_sipserver_index(),
        ?DLOG("ExtReg .. get regs from: ~120p for ~120p", [Domains,SrvIdx]),
        get_regs_3(Domains,SrvIdx)
    catch
        throw:R -> {error,R};
        exit:R -> {error,R}
    end.

get_regs_3(Domains,SrvIdx) ->
    {Regs,_} = lists:foldl(fun(Domain,{Acc,Cache}) ->
                                    try
                                        {ok,Regs1,Cache1} = load_regs(Domain,SrvIdx,Cache),
                                        {Acc ++ Regs1, Cache1}
                                    catch E:R:StackTrace ->
                                        ?LOG("Exception on loading regs in '~ts'", [Domain,{E,R,StackTrace}]),
                                        {Acc,Cache}
                                    end end, {[],#{}}, Domains),
    Regs.

%% @private
load_regs(Domain,MySrvIdx,Cache) ->
    {ok,Any,_} = ?ENVDC:get_object_nosticky(Domain,provider,[{fldeq,[{serveridxs,[]},{enabled,true}]}],false,auto),
    {ok,In,_} = ?ENVDC:get_object_nosticky(Domain,provider,[{fldin,[{serveridxs,[MySrvIdx]}]},{fldeq,[{enabled,true}]}],false,auto),
    {My,Gr} = lists:partition(fun(Item) -> maps:get(serveridxs,Item)==[MySrvIdx] end, In),
    % My - all,
    % Gr - only if less roles are unavailable
    % Any - only if domain.settings.ext.default_provider_serveridxs field, if not found - if my esg has minimal srvidx among those active esgs which handles domain.
    case {Gr, Any} of
        {[],[]} ->
            NowRegs = prepare_actual_regs(Domain,My),
            {ok,build_regs(Domain,NowRegs),Cache};
        _ ->
            ESGs = case maps:get(esgs,Cache,undefined) of
                       undefined -> active_esgs();
                       L0 -> L0
                   end,
            Cache1 = Cache#{esgs => ESGs},
            case ESGs of
                [] ->
                    NowRegs = prepare_actual_regs(Domain,My++Gr++Any),
                    {ok, build_regs(Domain, NowRegs), Cache1};
                _ ->
                    % sort Gr by serveridx list, take where my is first
                    Gr1 = filter_items_by_list(Gr,fun(Item) -> maps:get(serveridxs,Item) end,MySrvIdx,ESGs),
                    % sort Any by serveridx value
                    {ok,[ExtSett],_} = ?ENVDC:get_object_sticky(Domain,settings,[{keys,[<<"ext">>]}],false,auto),
                    DefaultSrvIdx = maps:get(<<"default_provider_serveridxs">>,maps:get(value,ExtSett,#{}),undefined),
                    Any1 = case DefaultSrvIdx of
                               undefined ->
                                   DomainSites = ?ENVCFGU:get_domain_sites(Domain),
                                   Priorities = lists:filter(fun({Site,_,Idx,_}) -> lists:member(Site,DomainSites) andalso Idx < MySrvIdx end, ESGs),
                                   case Priorities of
                                       [] -> Any;
                                       _ -> []
                                   end;
                               MySrvIdx -> Any;
                               _Idx when is_integer(_Idx) -> [];
                               List when is_list(List) -> filter_items_by_list(Any,fun(_) -> List end,MySrvIdx,ESGs)
                           end,
                    NowRegs = prepare_actual_regs(Domain,My++Gr1++Any1),
                    {ok, build_regs(Domain,NowRegs), Cache1}
            end
    end.

%% @private
%% makes pause on 1 iteration before pushing new accounts to registration after settings changes
prepare_actual_regs(Domain,Regs) ->
    Key = {local_esg_regs,Domain},
    NowRegs = case ?SIPSTORE:find_t(Key) of
                  false -> Regs;
                  {_,PrevRegs} -> ?EU:get_intersect(Regs,PrevRegs)
              end,
    ?SIPSTORE:store_t(Key,Regs,60000),
    NowRegs.

%% @private
filter_items_by_list(Items,FunStrategy,MySrvIdx,ESGs) when is_function(FunStrategy,1) ->
    lists:filter(
        fun(Item) ->
            StrategyList = FunStrategy(Item),
            List1 = lists:filter(fun(Idx) when Idx==MySrvIdx -> true;
                                    (Idx) -> case lists:keyfind(Idx,3,ESGs) of false -> false; _ -> true end
                                 end, StrategyList),
            case List1 of
                [Idx|_] -> Idx==MySrvIdx;
                _ -> false
            end end, Items).

%% @private
active_esgs() ->
    _CurSite = ?CFG:get_current_site(),
    CurIdx = ?CFG:get_my_sipserver_index(),
    L1 = ?ENVCFG:get_nodes_containing_role_dyncfg_check_free('esg'),
    F = fun%({Site,_,_,_}) when Site/=_CurSite -> false; % comment to make cross-site integrations
           ({_,_,SrvIdx,_}) when SrvIdx==CurIdx -> false;
           (_) -> true
        end,
    L2 = lists:filter(F,L1),
    Dests = [{Site,Node} || {Site,Node,_,_} <- L2],
    R1 = ?ENVMULTICALL:call_rpco(Dests,{erlang,node,[]},3000),
    [SNIA || {SNIA,{_D,R}} <- lists:zip(L2,R1), R /= undefined].

%% @private
build_regs(Domain,Items) ->
    lists:map(fun(A) -> create_opts(Domain, A) end, Items).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -------------------------------------
%% Return domains that are handled on current site/server
%% -------------------------------------
-spec get_responsible_domains(AppOpts::list()) -> [Domain::binary()].
%% -------------------------------------
get_responsible_domains(AppOpts) ->
    case ?ENVCFGU:get_domain_tree([{mode,currentsite}|AppOpts]) of
        undefined -> throw("Site domain tree access error");
        R when is_list(R) -> R
    end.

%% -------------------------------------
%% Make and return item to be attached to registrar server
%% -------------------------------------
-spec create_opts(ClusterDomain::binary(), Map::map()) -> account_item().
%% -------------------------------------
create_opts(ClusterDomain, Map) ->
    Map1 = update_reg_defaults(Map),
    Id = maps:get(id,Map),
    Map2 = Map1#{clusterdomain => ClusterDomain},
    %{Id, #{d => ClusterDomain,
    %       map => Map1}}.
    _AInfo = {reg,Id,ClusterDomain,Map2}.

%% -------------------------------------
%% Fill provider account data by default required values
%% -------------------------------------
-spec update_reg_defaults(AMap::map()) -> AMap1::map().
%% -------------------------------------
update_reg_defaults(AMap) ->
    Dom = maps:get(domain,AMap),
    PrA = maps:get(proxyaddr,AMap),
    PrP = maps:get(proxyport,AMap),
    AProxies = maps:get(alternative_proxies,AMap),
    Transp = maps:get(transport,AMap),
    Exp = maps:get(expires,AMap),
    %
    PrA1 = case PrA of
               <<>> -> Dom;
               _ -> PrA
           end,
    PrP1 = case PrP of
               _P when not is_integer(_P) -> 5060;
               _P when _P=<0, _P>65535 -> 5060;
               _ -> PrP
           end,
    AProxies1 = parse_proxies(AProxies),
    %
    Exp1 = case Exp of
               0 -> 3600;
               _E when not is_integer(_E) -> 3600;
               _E when _E < 30 -> 30;
               _E when _E > 7200 -> 7200;
               _ -> Exp
           end,
    Transp1 = ?EXT_UTILS:parse_transport(Transp),
    %
    AMap#{proxyaddr:=PrA1,
          proxyport:=PrP1,
          alternative_proxies:=AProxies1,
          transport:=Transp1,
          expires:=Exp1}.

%% -------------------------------------
%% Parse binary string, containing sequence of proxy-servers
%% -------------------------------------
-spec parse_proxies(binary()) -> [{Proto::udp|tcp|tls, Addr::binary(), Port::integer()}].
%% -------------------------------------
parse_proxies(Proxies) when is_binary(Proxies) ->
    lists:filtermap(fun(Elt) ->
                            {Proto,Addr,Port} =
                                case binary:split(Elt,<<":">>,[global]) of
                                    [<<"udp">>,A,P] -> {udp,A,P};
                                    [<<"tcp">>,A,P] -> {tcp,A,P};
                                    [<<"tls">>,A,P] -> {tls,A,P};
                                    [<<"udp">>,A] -> {udp,A,5060};
                                    [<<"tcp">>,A] -> {tcp,A,5060};
                                    [<<"tls">>,A] -> {tls,A,5060};
                                    [A] -> {udp,A,5060};
                                    [A,P] -> {udp,A,P}
                                end,
                            case catch ?EU:to_int(Port) of
                                PI when is_integer(PI) -> {true,{Proto,Addr,PI}};
                                _ -> false
                            end end,
                    binary:split(Proxies, [<<",">>,<<";">>,<<" ">>], [global,trim_all]));
parse_proxies(_) -> [].

%% -------------------------------------
%% Parse binary string, containing sequence of proxy-servers
%% -------------------------------------
-spec compare_opts(list()|map(), list()|map()) -> boolean().
%% -------------------------------------
compare_opts(O1, O2) when is_list(O1), is_list(O2), length(O1) /= length(O2) -> false;
compare_opts(O1, O2) when is_list(O1), is_list(O2) -> lists:usort(O1) == lists:usort(O2);
compare_opts(#{}=O1, #{}=O2) -> O1 == O2.
