%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>, Peter Bukashin <tbotc@yandex.ru>
%%% @date 30.09.2016
%%% @doc
%%% @todo


-module(r_sip_ivr_dialer_owner_example).
-author(['Evgeniy Grebenyuk <llceceron@gmail.com>', 'Peter Bukashin <tbotc@yandex.ru>']).

-behaviour(gen_server).

-export([start_link/1, stop/0]).

-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-export([start_srv/0, start_call/0, cancel_call/0,stop_call/0]).

%% ====================================================================
%% Types
%% ====================================================================

-record(state, {data,opts,dialerpid}).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, Opts, []).

stop() ->
    gen_server:stop(?MODULE).

%% ---------------
start_srv() ->
    start_link([]).

start_call() ->
    gen_server:call(?MODULE,{start_call}).

cancel_call() ->
    gen_server:call(?MODULE,{cancel_call}).

stop_call() ->
    gen_server:call(?MODULE,{stop_call}).

%% ====================================================================
%% Callback functions
%% ====================================================================

init(_) ->
    Mode = <<"1">>,
    OwnerPid = self(),
    FunResult = fun(EventType,Msg) ->
                        case EventType of
                            pre_res -> OwnerPid ! {dialer_response_1xx,Msg};
                            success_res -> OwnerPid ! {dialer_response_2xx,Msg};
                            unsuccess_res -> OwnerPid ! {dialer_response_failed,Msg};
                            ivr_stopped -> OwnerPid ! {dialer_ivr_stopped,Msg};
                            _ -> ok
                        end end,
    Args = [{script,undefined},
            {script_code,<<"108">>},
            {domain,<<"test.ceceron.ru">>},
            {number,<<"11">>},
            {fun_res,FunResult},
            {callername,<<>>},
            {callerid,<<"092563235">>},
            {ownerpid,self()},
            {headers,[]}],
    Opts = #{mode=>Mode,args=>Args},
    State = #state{opts=Opts},
    io:format("Dialer Example Srv inited. Local: ~p (~p)~n", [?MODULE, self()]),
    erlang:send_after(1, self(), {start_call}),
    {ok, State}.

%% ------------------------------
%% TEST METHODS
%% ------------------------------
handle_call({start_call}, _From, #state{opts=Opts}=State) ->
    case rpc:call('ivr1@192.168.0.84',r_sip_ivr_cb,start_by_mode,[Opts]) of
        {ok,DialerPid} ->
            erlang:monitor(process,DialerPid),
            Reply = DialerPid,
            State1 = State#state{dialerpid=DialerPid};
        Err ->
            Reply  = {error,Err},
            io:format("Dialer Example Srv. ERROR start dialer. Reason: ~p~n",[Err]),
            State1 = State
    end,
    {reply, Reply, State1};

handle_call({cancel_call}, _From, #state{dialerpid=DialerPid}=State) ->
    Param = {<<"1">>,DialerPid},
    Reason = <<"cancel_from_owner">>,
    Reply = rpc:call('ivr1@192.168.0.84',r_sip_ivr_cb,cancel_by_mode,[Param,Reason]),
    {reply, Reply, State};

handle_call({stop_call}, _From, #state{dialerpid=DialerPid}=State) ->
    Param = DialerPid,
    Reason = <<"stop_from_owner">>,
    Reply = rpc:call('ivr1@192.168.0.84',r_sip_ivr_fsm,stop,[Param,Reason]),
    {reply, Reply, State};

handle_call(test, _From, #state{opts=Opts}=State) ->
    Args = maps:get(args,Opts),
    {_,FunRes} = lists:keyfind(fun_res,1,Args),
    Reply = FunRes(pre_res,<<"test">>),
    {reply, Reply, State};

handle_call(_Request, _From, State) ->
    Reply = {error,<<"badrequest">>},
    {reply, Reply, State}.

%% ------------------------------
handle_cast(_Request, State) ->
    {noreply, State}.

%% ------------------------------

%% ------------------------------
%% TEST METHODS
%% ------------------------------
handle_info({start_call}, #state{opts=Opts}=State) ->
    case rpc:call('ivr1@192.168.0.84',r_sip_ivr_cb,start_by_mode,[Opts]) of
        {ok,DialerPid} ->
            erlang:monitor(process,DialerPid),
            State1 = State#state{dialerpid=DialerPid};
        Err ->
            io:format("Dialer Example Srv. ERROR start dialer. Reason: ~p~n",[Err]),
            State1 = State
    end,
    {noreply, State1};

%% ------------------------------
%% Dialer Response methods
%% ------------------------------
handle_info({dialer_response_1xx,Msg}, #state{dialerpid=DialerPid}=State) ->
    io:format("Dialer Example Srv. RCV dialer_response_1xx: ~p~nCurrent active dialer: ~p~n",[Msg,DialerPid]),
    {noreply, State};

handle_info({dialer_response_2xx,Msg}, #state{dialerpid=DialerPid}=State) ->
    io:format("Dialer Example Srv. dialer_response_2xx: ~p~nCurrent active dialer: ~p~n",[Msg,DialerPid]),
    {noreply, State};

handle_info({dialer_response_failed,Msg}, #state{dialerpid=DialerPid}=State) ->
    io:format("Dialer Example Srv. RCV dialer_response_failed: ~p~nCurrent active dialer: ~p~n",[Msg,DialerPid]),
    {noreply, State};

handle_info({dialer_ivr_stopped,Msg}, #state{dialerpid=DialerPid}=State) ->
    io:format("Dialer Example Srv. RCV dialer_ivr_stopped: ~p~nCurrent active dialer: ~p~n",[Msg,DialerPid]),
    {noreply, State};

handle_info({'DOWN', _Ref, process, Pid, Reason}, #state{dialerpid=DialerPid}=State) ->
    case Pid of
        DialerPid ->
            io:format("Dialer Example Srv. RCV DOWN from dialer (~p). Reason: ~p~n",[DialerPid,Reason]);
        _ ->
            ok
    end,
    {noreply, State#state{dialerpid=undefined}};

handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
terminate(_Reason, _State) ->
    io:format("Dialer Example Srv. Terminate Reason: ~p~n",[_Reason]),
    ok.

%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Public functions
%% ====================================================================


%% ====================================================================
%% Internal functions
%% ====================================================================

