%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc
%%% @TODO @ivr

-module(r_sip_ivr_eventing).
-author('Peter Bukashin <tbotc@yandex.ru>').

%% -------------------------------------------------------------------
%%
%% -------------------------------------------------------------------

-include("r_sip_ivr_eventing.hrl").
-include("../../../../include/r_sip_ivr_queue.hrl").

-export([ivr_init/2, ivr_start/1, ivr_stop/1, ivr_dead/2, ivr_script_start/2,
         ivr_api_start/2, ivr_api_stop/2, ivr_api_info/2, ivr_replace/2]).
-export([queue_greeting/2, queue_dequeue/2, queue_userban/2, queue_enqueue/2,
         queue_quit/2, queue_preivr_start/2, queue_start_call/2, hunt_final/2]).

%% ===================================================================
%% Defines
%% ===================================================================

%-define(OUTD(Fmt,Args), ?OUT(Fmt,Args)).
-define(OUTD(Fmt,Args), ?NOOUT(Fmt,Args)).

%% ===================================================================
%% API functions
%% ===================================================================

%% ----------------------------------------------------------------------------------------------
%% ivr events
%% ----------------------------------------------------------------------------------------------

%% -----------------------------------------------
%% when ivr init on incoming call (after routing)
%% CID::binary(), IvrId::binary(), CallId::binary(), TS::integer()
%% Opts:: #{domain,mode,called_number}
%% -----------------------------------------------
ivr_init({CID, IvrId, CallId, TS}=Id, Opts) ->
    ?OUTD("Event ivr_init ~120p", [{IvrId, TS}]),
    Opts1 = Opts#{cid => CID,
                  ivrid => IvrId,
                  ivrts => TS,
                  callid => CallId},
    Event = ?EVENTHANDLER:prepare_event(?Event_IvrInit, Opts1),
    send_event(Id,Event).

%% -----------------------------------------------
%% when ivr scriptmachine started (after init)
%% State::#state_ivr{}
%% -----------------------------------------------
ivr_start(State) ->
    ?OUTD("Event ivr_start ~120p", [ivrcid(State)]),
    Opts = update_ivr_opts(#{},State),
    Event = ?EVENTHANDLER:prepare_event(?Event_IvrStart, Opts),
    send_event(ivrcid(State),Event).

%% -----------------------------------------------
%% when ivr stops correctly
%%   stopreason: #{type:atom, reason::binary(), [operations::list(), callid::binary()]}
%% -----------------------------------------------
ivr_stop(#state_ivr{stopreason=StopResult}=State) ->
    ?OUTD("Event ivr_stop ~120p, ~120p", [StopResult, ivrcid(State)]),
    Opts = update_ivr_opts(#{},State),
    Event = ?EVENTHANDLER:prepare_event(?Event_IvrStop, Opts),
    send_event(ivrcid(State),Event).

%% -----------------------------------------------
%% when ivr dead
%% CID::binary(), IvrId::binary(), CallId::binary(), TS::integer()
%% Opts:: #{domain}
%% -----------------------------------------------
ivr_dead({CID, IvrId, CallId, TS}=Id, Opts) ->
    ?OUTD("Event ivr_dead ~120p", [{IvrId, TS}]),
    Opts1 = Opts#{cid => CID,
                  ivrid => IvrId,
                  ivrts => TS,
                  callid => CallId},
    Event = ?EVENTHANDLER:prepare_event(?Event_IvrDead, Opts1),
    send_event(Id,Event).

%% -----------------------------------------------
%% when ivr script started, continued from stack, replaced, or post_branch (could be several scripts inside one)
%% State::#state_ivr{} | {CID,IvrId,CallId,TS,Domain}
%% Opts:: #{mode::<<"start">>|<<"continue">>|<<"post">>, code, name, componentid, componentname}
%% -----------------------------------------------
ivr_script_start(State,Opts) ->
    ?OUTD("ivr_script_start ~1000p", [ivrcid(State)]),
    Opts1 = update_ivr_opts(Opts,State),
    Event = ?EVENTHANDLER:prepare_event(?Event_ScriptStart, Opts1),
    send_event(ivrcid(State),Event).

%%%% -----------------------------------------------
%%%% when ivr component inits
%%%% State::#state_ivr{}
%%%% Opts:: #{}
%%%% -----------------------------------------------
%%ivr_component(State,_Opts) ->
%%    ?OUTD("ivr_component ~1000p", [ivrcid(State)]).

%% -----------------------------------------------
%% when ivr api management started
%% State::#state_ivr{} | {CID,IvrId,CallId,TS,Domain}
%% Opts:: #{componentid, componentname, key, params}
%% -----------------------------------------------
ivr_api_start(State,Opts) ->
    ?OUTD("ivr_api_start ~1000p", [ivrcid(State)]),
    Opts1 = update_ivr_opts(Opts,State),
    Event = ?EVENTHANDLER:prepare_event(?Event_ApiStart, Opts1),
    send_event(ivrcid(State),Event).

%% -----------------------------------------------
%% when ivr api management stopped
%% State::#state_ivr{} | {CID,IvrId,CallId,TS,Domain}
%% Opts:: #{componentid, componentname, key, reason}
%% -----------------------------------------------
ivr_api_stop(State,Opts) ->
    ?OUTD("ivr_api_stop ~1000p", [ivrcid(State)]),
    Opts1 = update_ivr_opts(Opts,State),
    Event = ?EVENTHANDLER:prepare_event(?Event_ApiStop, Opts1),
    send_event(ivrcid(State),Event).

%% -----------------------------------------------
%% when ivr api management send info
%% State::#state_ivr{} | {CID,IvrId,CallId,TS,Domain}
%% Opts:: #{componentid, componentname, key, reason}
%% -----------------------------------------------
ivr_api_info(State,Opts) ->
    ?OUTD("ivr_api_info ~1000p", [ivrcid(State)]),
    Opts1 = update_ivr_opts(Opts,State),
    Event = ?EVENTHANDLER:prepare_event(?Event_ApiInfo, Opts1),
    send_event(ivrcid(State),Event).

%% -----------------------------------------------
%% when ivr replaces call on got refer or invite+replaces
%% Opts:: #{mode::<<"refer">>|<<"replaces">>}
%% -----------------------------------------------
ivr_replace(State,Opts) ->
    ?OUTD("ivr_replace ~1000p", [ivrcid(State)]),
    Opts1 = update_ivr_opts(Opts,State),
    Event = ?EVENTHANDLER:prepare_event(?Event_Replace, Opts1),
    send_event(ivrcid(State),Event).

%% -----------------------------------------------
%% @private
update_ivr_opts(Opts,#state_ivr{cid=CID,ivrid=IvrId,acallid=CallId,invite_ts=TS,domain=Domain,scriptmachine=SM}) ->
    Opts#{cid => CID,
          ivrid => IvrId,
          ivrts => TS,
          callid => CallId,
          domain => Domain,
          sm_id => case is_map(SM) of true -> maps:get(id,SM); false -> <<"undefined">> end};
update_ivr_opts(Opts,{CID,IvrId,_,TS,Domain}) ->
    Opts#{cid => CID,
          ivrid => IvrId,
          ivrts => TS,
          domain => Domain}.

%% ----------------------------------------------------------------------------------------------
%% ivr_huntq_events
%% ----------------------------------------------------------------------------------------------

%% -----------------------------------------------
-spec queue_greeting(map(), #sipmsg{}) -> ok.
%% -----------------------------------------------
queue_greeting(Opts, #sipmsg{}=Req) when is_map(Opts) ->
    Event = ?EVENTHANDLER:prepare_event(?Event_Greeting, Opts, Req),
    send_event(Event).

%% -----------------------------------------------
-spec queue_dequeue(map(), #state_queue{}) -> ok.
%% -----------------------------------------------
queue_dequeue(Opts, #state_queue{}=State) when is_map(Opts) ->
    Event = ?EVENTHANDLER:prepare_event(?Event_Dequeue, Opts, State),
    send_event(Event).

%% -----------------------------------------------
-spec queue_userban(map(), #state_queue{}) -> ok.
%% -----------------------------------------------
queue_userban(Opts, #state_queue{}=State) when is_map(Opts) ->
    Event = ?EVENTHANDLER:prepare_event(?Event_Userban, Opts, State),
    send_event(Event).

%% -----------------------------------------------
-spec queue_enqueue(map(), #state_queue{}) -> ok.
%% -----------------------------------------------
queue_enqueue(Opts, #state_queue{}=State) when is_map(Opts) ->
    Event = ?EVENTHANDLER:prepare_event(?Event_Enqueue, Opts, State),
    send_event(Event).

%% -----------------------------------------------
-spec queue_quit(map(), #state_queue{}) -> ok.
%% -----------------------------------------------
queue_quit(Opts, #state_queue{}=State) when is_map(Opts) ->
    Event = ?EVENTHANDLER:prepare_event(?Event_Quit, Opts, State),
    send_event(Event).

%% -----------------------------------------------
-spec queue_preivr_start(map(), #state_queue{}) -> ok.
%% -----------------------------------------------
queue_preivr_start(Opts, #state_queue{}=State) when is_map(Opts) ->
    Event = ?EVENTHANDLER:prepare_event(?Event_PreivrStart, Opts, State),
    send_event(Event).

%% -----------------------------------------------
-spec queue_start_call(map(), #state_queue{}) -> ok.
%% -----------------------------------------------
queue_start_call(Opts, #state_queue{}=State) when is_map(Opts) ->
    Event = ?EVENTHANDLER:prepare_event(?Event_StartCall, Opts, State),
    send_event(Event).

%% -----------------------------------------------
-spec hunt_final(map(), #sipmsg{}) -> ok.
%% -----------------------------------------------
hunt_final(Opts, #sipmsg{}=Req) when is_map(Opts) ->
    Event = ?EVENTHANDLER:prepare_event(?Event_Final, Opts, Req),
    send_event(Event).

%% ===================================================================
%% Internal functions
%% ===================================================================

%% -----------------------------------------------
ivrcid(#state_ivr{ivrid=IvrId,invite_ts=TS}=_State) -> {IvrId, TS};
ivrcid({_CID,IvrId,_CallId,TS,_Domain}) -> {IvrId, TS}.

%% -----------------------------------------------
send_event({Pattern, EventHeader, _From}=Event) ->
    Data = maps:get(<<"data">>, EventHeader),
    Key = maps:get(<<"qid">>, Data, maps:get(<<"callid">>, Data)),
    ?WORKER:cast_workf(Key, fun() -> send_event_worker(Pattern, Event) end).
send_event(Key, {Pattern,_,_}=Event) ->
    ?WORKER:cast_workf(Key, fun() -> send_event_worker(Pattern, Event) end).

%% @private
send_event_worker({ClassPattern,_,Domain}, Event) ->
    case ?ENVLIC:is_cdr_allowed() of
        true -> ?ENVEVENTGATE:send_event(Event, make_destinations(ClassPattern,Domain));
        false -> ok
    end.


%% @private
make_destinations(<<"ivrhuntqevents">>,_Domain) ->
    #{kafka => [], % RP-1853
      sq => false}; % DEBUG 2021-11-25
make_destinations(<<"ivrevents">>,Domain) ->
    #{kafka => [], % RP-1853
      wssubscr => [{Domain,[{ivr,<<>>}]}],
      sq => false}. % DEBUG 2021-11-25
