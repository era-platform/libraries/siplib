%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Pavel Abramov, Peter Bukashin <tbotc@yandex.ru>
%%% @date 14.03.2019
%%% @doc RP-1181. Send event to CDR.

-author(['Pavel Abramov','Peter Bukashin <tbotc@yandex.ru>']).


-define(IvrClassPattern, <<"ivrevents">>).
-define(IvrClassType, domain).

-define(QueueClassPattern, <<"ivrhuntqevents">>).
-define(QueueClassType, domain).

-define(Event_IvrInit, <<"init">>).
-define(Event_IvrStart, <<"sm_start">>).
-define(Event_IvrStop, <<"sm_stop">>).
-define(Event_IvrDead, <<"sm_dead">>).
-define(Event_ScriptStart, <<"script_start">>).
-define(Event_ApiStart, <<"api_start">>).
-define(Event_ApiStop, <<"api_stop">>).
-define(Event_ApiInfo, <<"api_info">>).
-define(Event_Replace, <<"call_replace">>).

-define(Event_Greeting, <<"queue_greeting">>).
-define(Event_Dequeue, <<"queue_dequeue">>).
-define(Event_Userban, <<"queue_userban">>).
-define(Event_Enqueue, <<"queue_enqueue">>).
-define(Event_Quit, <<"queue_quit">>).
-define(Event_PreivrStart, <<"queue_preivr_start">>).
-define(Event_StartCall, <<"queue_start_call">>).
-define(Event_Final, <<"final">>).

-define(EVENTHANDLER, r_sip_ivr_eventing_handler).