%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Pavel Abramov, Peter Bukashin <tbotc@yandex.ru>
%%% @date 14.03.2019
%%% @doc RP-1181. Make event body.

-module(r_sip_ivr_eventing_handler).
-author(['Pavel Abramov','Peter Bukashin <tbotc@yandex.ru>']).

-export([prepare_event/2, prepare_event/3]).

%% ===================================================================
%% Defines
%% ===================================================================

-include("r_sip_ivr_eventing.hrl").
-include("../../../../include/r_sip_ivr_queue.hrl").

%% ===================================================================
%% API functions
%% ===================================================================

%% -----------------------------------------------
-spec prepare_event(binary(), map()) -> tuple().
%% -----------------------------------------------
prepare_event(EventType, Opts) when is_binary(EventType), is_map(Opts) ->
    {ClassPattern,ClassType,Fields} = get_fields(EventType),
    Domain = maps:get(domain, Opts),
    prepare_event_header(ClassPattern, ClassType, EventType, Domain, maps:with(Fields,Opts)).

%% -----------------------------------------------
-spec prepare_event(binary(), map(), #state_queue{} | #sipmsg{}) -> tuple().
%% -----------------------------------------------
prepare_event(EventType, Opts, #state_queue{domain=Domain}=State) when is_binary(EventType), is_map(Opts) ->
    {ClassPattern,ClassType,Fields} = make_event(EventType, Opts, State),
    prepare_event_header(ClassPattern, ClassType, EventType, Domain, Fields);

prepare_event(EventType, Opts, #sipmsg{}=State) when is_binary(EventType), is_map(Opts) ->
    {ClassPattern,ClassType,Fields} = make_event(EventType, Opts, State),
    Domain = maps:get(<<"domain">>, Opts),
    prepare_event_header(ClassPattern, ClassType, EventType, Domain, Fields).

%% ===================================================================
%% Internal functions
%% ===================================================================

% by opts and state
make_event(EventType, Opts, State) ->
    {ClassPattern,ClassType,Fields} = get_fields(EventType),
    F = fun(Field, Acc) ->
                Value = fill_field(EventType, Field, Opts, State),
                append_field(EventType, Value, Acc)
        end,
    {ClassPattern,ClassType,lists:foldl(F, #{}, Fields)}.

%% ---------------------------------------------------------------
%% ivrevents
%% ---------------------------------------------------------------

get_fields(?Event_IvrInit) -> {?IvrClassPattern,?IvrClassType,[cid,ivrid,ivrts,callid,rusername,lusername,called_number]};
get_fields(?Event_IvrStart) -> {?IvrClassPattern,?IvrClassType,[cid,ivrid,ivrts,callid,sm_id]};
get_fields(?Event_IvrStop) -> {?IvrClassPattern,?IvrClassType,[cid,ivrid,ivrts,callid,sm_id]};
get_fields(?Event_IvrDead) -> {?IvrClassPattern,?IvrClassType,[cid,ivrid,ivrts,callid]};
get_fields(?Event_ScriptStart) -> {?IvrClassPattern,?IvrClassType,[cid,ivrid,ivrts,callid,sm_id,mode,code,name,componentid,componentname]};
get_fields(?Event_ApiStart) -> {?IvrClassPattern,?IvrClassType,[cid,ivrid,ivrts,callid,sm_id,componentid,componentname,key,params,state]};
get_fields(?Event_ApiStop) -> {?IvrClassPattern,?IvrClassType,[cid,ivrid,ivrts,callid,sm_id,componentid,componentname,key,reason]};
get_fields(?Event_ApiInfo) -> {?IvrClassPattern,?IvrClassType,[cid,ivrid,ivrts,callid,sm_id,componentid,componentname,key,params,state]};
get_fields(?Event_Replace) -> {?IvrClassPattern,?IvrClassType,[cid,ivrid,ivrts,callid,sm_id,mode,rusername,lusername]};

%% ---------------------------------------------------------------
%% queue ivrhuntqevents
%% ---------------------------------------------------------------
get_fields(?Event_Greeting) ->
    {?QueueClassPattern,
     ?QueueClassType,
     [<<"huntq_id">>,
      <<"inviteid">>,
      <<"esgdlg">>,
      <<"qivrscript">>,
      <<"callid">>,
      <<"from_user">>,
      <<"from_displayname">>]};
get_fields(?Event_Dequeue) ->
    {?QueueClassPattern,
     ?QueueClassType,
     [<<"qid">>,
      <<"huntq_id">>,
      <<"inviteid">>,
      <<"esgdlg">>,
      <<"from_user">>,
      <<"from_displayname">>,
      <<"callid">>,
      <<"result">>,
      <<"reason">>]};
get_fields(?Event_Userban) ->
    {?QueueClassPattern,
     ?QueueClassType,
     [<<"qid">>,
      <<"huntq_id">>,
      <<"inviteid">>,
      <<"esgdlg">>,
      <<"callid">>,
      <<"userid">>]};
get_fields(?Event_Enqueue) ->
    {?QueueClassPattern,
     ?QueueClassType,
     [<<"qid">>,
      <<"huntq_id">>,
      <<"inviteid">>,
      <<"esgdlg">>,
      <<"from_user">>,
      <<"callid">>,
      <<"result">>]};
get_fields(?Event_Quit) ->
    {?QueueClassPattern,
     ?QueueClassType,
     [<<"qid">>,
      <<"inviteid">>,
      <<"esgdlg">>,
      <<"callid">>,
      <<"from_user">>]};
get_fields(?Event_PreivrStart) ->
    {?QueueClassPattern,
     ?QueueClassType,
     [<<"qid">>,
      <<"inviteid">>,
      <<"esgdlg">>,
      <<"callid">>,
      <<"userid">>]};
get_fields(?Event_StartCall) ->
    {?QueueClassPattern,
     ?QueueClassType,
     [<<"qid">>,
      <<"inviteid">>,
      <<"esgdlg">>,
      <<"callid">>,
      <<"userid">>]};
get_fields(?Event_Final) ->
    {?QueueClassPattern,
     ?QueueClassType,
     [<<"huntq_id">>,
      <<"inviteid">>,
      <<"esgdlg">>,
      <<"callid">>,
      <<"stopreason">>]}.

%% -----------------------------------------------
%% by state
fill_field(?Event_Greeting, <<"callid">> =Field, _, #sipmsg{call_id=CallId}) -> {Field, CallId};
fill_field(?Event_Greeting, <<"from_user">> =Field, _, #sipmsg{from={From,_}}) -> {Field, From#uri.user};
fill_field(?Event_Greeting, <<"from_displayname">> =Field, _, #sipmsg{from={From,_}}) -> {Field, ?U:unquote_display(From#uri.disp)};
fill_field(?Event_Final, <<"callid">> =Field, _, #sipmsg{call_id=CallId}) -> {Field, CallId};
fill_field(?Event_Final, <<"stopreason">> =Field, Map, _) ->
    StopReason =
        case maps:get(Field,Map) of
            normal -> <<"ok">>;
            _ -> <<"error">>
        end,
    {Field, StopReason};
fill_field(_, <<"callid">> =Field, _, #state_queue{xinfo=X}) -> {Field, maps:get(call_id,X)};
fill_field(_, <<"inviteid">> =Field, _, #state_queue{xinfo=X}) -> {Field, maps:get(inviteid,X)};
fill_field(_, <<"esgdlg">> =Field, _, #state_queue{xinfo=X}) -> {Field, maps:get(esgdlg,X)};
fill_field(_, <<"huntq_id">> =Field, _, #state_queue{huntq_id=Id}) -> {Field, Id};
fill_field(_, <<"from_user">> =Field, _, #state_queue{from_user=From}) -> {Field, From};
fill_field(_, <<"qid">> =Field, _, #state_queue{current_queue_id=Qid}) -> {Field, Qid};
fill_field(_, <<"from_displayname">> =Field, _, #state_queue{from_dn=DN}) -> {Field, DN};
fill_field(_, Field, Map, _) -> {Field, maps:get(Field,Map)}.

%% -----------------------------------------------
append_field(_, {K,V}, Acc) -> maps:put(K, V, Acc).

%% -----------------------------------------------
prepare_event_header(ClassPattern, ClassType, EventType, Domain, Event) ->
    Pattern = {ClassPattern, ClassType, Domain},
    EventHeader = #{<<"class">> => ClassPattern,
                    <<"type">> => EventType,
                    <<"data">> => Event,
                    <<"eventts">> => ?EU:timestamp()},
    From = self(),
    {Pattern, EventHeader, From}.
