%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc
%%% @TODO @ivr

-module(r_sip_ivr_eventing_log).
-author('Peter Bukashin <tbotc@yandex.ru>').

%% -------------------------------------------------------------------
%%
%% -------------------------------------------------------------------

-export([]).

-compile([export_all,nowarn_export_all]).

%% ===================================================================
%% Defines
%% ===================================================================

-include("../../../../include/r_sip_ivr_queue.hrl").

%-define(OUTD(Fmt,Args), ok).
-define(OUTD(Fmt,Args), ?LOGCDR(Fmt,Args)).

%% ===================================================================
%% API functions
%% ===================================================================

%% -----------------------------------------------------------------------------------------
%%
%% -----------------------------------------------------------------------------------------

%% -----------------------------------------------
%% when ivr init (after routing)
%% IvrId::binary(), TS::integer()
%% Opts:: #{domain,mode,called_number}
%% -----------------------------------------------
ivr_init({_CID, _IvrId, _CallId, _TS}=Id,Opts) ->
    ?OUTD("ivr_init ~1000p", [id(Id)]),
    ?EVENT_CDR:ivr_init(Id,Opts).

%% -----------------------------------------------
%% when ivr start (after init)
%% State::#state_ivr{}
%% -----------------------------------------------
ivr_start(State) ->
    ?OUTD("ivr_start ~1000p", [id(ivrcid(State))]),
    ?EVENT_CDR:ivr_start(State).

%% -----------------------------------------------
%% when ivr stops correctly
%%   stopreason: #{type:atom, reason::binary(), [operations::list(), callid::binary()]}
%% -----------------------------------------------
ivr_stop(#state_ivr{stopreason=StopResult}=State) ->
    ?OUTD("ivr_stop ~1000p, ~1000p", [id(ivrcid(State)), StopResult]),
    ?EVENT_CDR:ivr_stop(State).

%% -----------------------------------------------
%% when ivr dead
%% IvrId::binary(), TS::integer()
%% -----------------------------------------------
ivr_dead({_CID, _IvrId, _CallId, _TS}=Id,Opts) ->
    ?OUTD("ivr_dead ~1000p", [id(Id)]),
    ?EVENT_CDR:ivr_dead(Id,Opts).

%% -----------------------------------------------
%% when ivr script started (could be several scripts inside one)
%% State::#state_ivr{} | {CID,IvrId,CallId,TS,Domain}
%% -----------------------------------------------
ivr_script_start(State,Opts) ->
    ?OUTD("ivr_script_start ~1000p", [id(ivrcid(State))]),
    ?EVENT_CDR:ivr_script_start(State,Opts).

%%%% -----------------------------------------------
%%%% when ivr component inits
%%%% State::#state_ivr{}
%%%% -----------------------------------------------
%%ivr_component(State) ->
%%    ?OUTD("ivr_component ~1000p", [id(ivrcid(State))]),
%%    ?EVENT_CDR:ivr_component(State).

%% -----------------------------------------------
%% when ivr api management started
%% State::#state_ivr{} | {CID,IvrId,CallId,TS,Domain}
%% -----------------------------------------------
ivr_api_start(State,Opts) ->
    ?OUTD("ivr_api_start ~1000p", [ivrcid(State)]),
    ?EVENT_CDR:ivr_api_start(State,Opts).

%% -----------------------------------------------
%% when ivr api management stopped
%% State::#state_ivr{} | {CID,IvrId,CallId,TS,Domain}
%% -----------------------------------------------
ivr_api_stop(State,Opts) ->
    ?OUTD("ivr_api_stop ~1000p", [ivrcid(State)]),
    ?EVENT_CDR:ivr_api_stop(State,Opts).

%% -----------------------------------------------
%% when ivr api management info
%% State::#state_ivr{} | {CID,IvrId,CallId,TS,Domain}
%% -----------------------------------------------
ivr_api_info(State,Opts) ->
    ?OUTD("ivr_api_info ~1000p", [ivrcid(State)]),
    ?EVENT_CDR:ivr_api_info(State,Opts).

%% -----------------------------------------------
%% when ivr replace call on got refer or invite+replaces
%% State::#state_ivr{}
%% -----------------------------------------------
ivr_replace(State,Opts) ->
    ?OUTD("ivr_api_stop ~1000p", [ivrcid(State)]),
    ?EVENT_CDR:ivr_api_stop(State,Opts).

%% -----------------------------------------------------------------------------------------
%%
%% -----------------------------------------------------------------------------------------

%% -----------------------------------------------
-spec queue_greeting(map(), #sipmsg{}) -> ok.
%% -----------------------------------------------
queue_greeting(Opts, #sipmsg{call_id=Id}=Req) when is_map(Opts) ->
    ?OUTD("queue_greeting ~1000p", [Id]),
    ?EVENT_CDR:queue_greeting(Opts, Req).

%% -----------------------------------------------
-spec queue_dequeue(map(), #state_queue{}) -> ok.
%% -----------------------------------------------
queue_dequeue(Opts, #state_queue{xinfo=X}=State) when is_map(Opts) ->
    ?OUTD("queue_dequeue ~1000p", [maps:get(call_id, X)]),
    ?EVENT_CDR:queue_dequeue(Opts, State).

%% -----------------------------------------------
-spec queue_userban(map(), #state_queue{}) -> ok.
%% -----------------------------------------------
queue_userban(Opts, #state_queue{xinfo=X}=State) when is_map(Opts) ->
    ?OUTD("queue_userban ~1000p", [maps:get(call_id, X)]),
    ?EVENT_CDR:queue_userban(Opts, State).

%% -----------------------------------------------
-spec queue_enqueue(map(), #state_queue{}) -> ok.
%% -----------------------------------------------
queue_enqueue(Opts, #state_queue{xinfo=X}=State) when is_map(Opts) ->
    ?OUTD("queue_enqueue ~1000p", [maps:get(call_id, X)]),
    ?EVENT_CDR:queue_enqueue(Opts, State).

%% -----------------------------------------------
-spec queue_quit(map(), #state_queue{}) -> ok.
%% -----------------------------------------------
queue_quit(Opts, #state_queue{xinfo=X}=State) when is_map(Opts) ->
    ?OUTD("queue_quit ~1000p", [maps:get(call_id, X)]),
    ?EVENT_CDR:queue_quit(Opts, State).

%% -----------------------------------------------
-spec queue_preivr_start(map(), #state_queue{}) -> ok.
%% -----------------------------------------------
queue_preivr_start(Opts, #state_queue{xinfo=X}=State) when is_map(Opts) ->
    ?OUTD("queue_preivr_start ~1000p", [maps:get(call_id, X)]),
    ?EVENT_CDR:queue_preivr_start(Opts, State).

%% -----------------------------------------------
-spec queue_start_call(map(), #state_queue{}) -> ok.
%% -----------------------------------------------
queue_start_call(Opts, #state_queue{xinfo=X}=State) when is_map(Opts) ->
    ?OUTD("queue_start_call ~1000p", [maps:get(call_id, X)]),
    ?EVENT_CDR:queue_start_call(Opts, State).

%% -----------------------------------------------
-spec hunt_final(map(), #sipmsg{}) -> ok.
%% -----------------------------------------------
hunt_final(Opts, #sipmsg{call_id=Id}=Req) when is_map(Opts) ->
    ?OUTD("hunt_final ~1000p", [Id]),
    ?EVENT_CDR:hunt_final(Opts, Req).

%% ===================================================================
%% Internal functions
%% ===================================================================

%% -----------------------------------------------
id({CallId,TS}) -> {CallId, ?EU:timestamp(TS)};
id({IvrId,_CallId,TS}) -> {IvrId, ?EU:timestamp(TS)};
id({_CID,IvrId,_CallId,TS}) -> {IvrId,?EU:timestamp(TS)};
id({_CID,IvrId,_CallId,TS,_Domain}) -> {IvrId,?EU:timestamp(TS)}.

%% -----------------------------------------------
ivrcid(#state_ivr{ivrid=IvrId,acallid=ACallId,invite_ts=ITS}=_State) -> {IvrId, ACallId, ITS}.

%% -----------------------------------------------
side(#side{callid=CallId,remoteuri=RUri,localtag=LTag}) -> {?U:unparse_uri(?U:clear_uri(RUri)),LTag,CallId}.
