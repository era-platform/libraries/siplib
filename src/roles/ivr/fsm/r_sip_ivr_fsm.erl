%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 18.04.2016
%%% @doc
%%% @todo

-module(r_sip_ivr_fsm).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([init/1, terminate/3, code_change/4]).
-export([callback_mode/0, handle_event/4]). % gen_statem
-export([handle_event/3, handle_sync_event/4, handle_info/3]). % gen_fsm

-export([active/2, active/3,
         dialing/2,
         stopping/2]).

-export([start/1,
         supv_start_link/1,
         start_by_mode/1,
         stop/1, stop/2,
         cancel_by_mode/2,
         stop_forcely/1,
         pull_fsm_by_callid/1,
         pull_fsm_by_ivrid/1,
         test/1,
         build_tag/2,
         replace_abonent/2,
         save_callpid/2,
         get_call_info_ivr/1,
         get_info/2]).

-export([uac_response/2,
         uas_dialog_response/2,
         sip_ack/2,
         sip_cancel/2,
         sip_bye/2,
         sip_reinvite/2,
         sip_refer/2,
         sip_notify/2,
         sip_info/2,
         sip_message/2,
         call_terminate/2]).

-export([get_current_media_link/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_ivr.hrl").
-include("../include/r_sip_mgc.hrl").
-include("../include/r_sip_headers.hrl").

% -include("../include/r_nksip.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

% --
% start new conf
%
start(Opts) ->
    {IvrId, D, IvrIdHash} = generate_ids(),
    Opts1 = [{ivrid,IvrId}, {ivrnum,D}, {ivridhash,IvrIdHash} | Opts],
    ChildSpec = {IvrId, {?MODULE, supv_start_link, [Opts1]}, temporary, 1000, worker, [?MODULE]},
    ?IVR_SUPV:start_child(ChildSpec).

supv_start_link(Opts) ->
    start_link(?MODULE, Opts, []).

start_by_mode(#{}=Map) ->
    case maps:get(mode,Map,undefined) of
        Mode when Mode==<<"1">> ->
            Args = maps:get(args,Map),
            {IvrId, D, IvrIdHash} = generate_ids(),
            NewArgs = [{ivrid,IvrId}, {ivrnum,D}, {ivridhash,IvrIdHash} | Args],
            ChildSpec = {IvrId, {?MODULE, supv_start_link, [{Mode,NewArgs}]}, temporary, 1000, worker, [?MODULE]},
            ?IVR_SUPV:start_child(ChildSpec);
        _ -> {error,invalid_params}
    end.

% --
% stop existing conf
%
stop(IvrX) ->
    stop(IvrX, "Undefined").

stop({_IvrId,Pid}, Reason) when is_pid(Pid) ->
    send_event(Pid, {stop_external, ?IVR_UTILS:reason_external(Reason)});
stop(Pid, Reason) when is_pid(Pid) ->
    send_event(Pid, {stop_external, ?IVR_UTILS:reason_external(Reason)});
stop(IvrId, Reason) when is_binary(IvrId) ->
    to_fsm(IvrId, fun(Pid) -> send_event(Pid, {stop_external, ?IVR_UTILS:reason_external(Reason)}) end).

% --
stop_forcely({_IvrId,Pid}) when is_pid(Pid) ->
    send_all_state_event(Pid, {stop_forcely});
stop_forcely(IvrId) when is_binary(IvrId) ->
    to_fsm(IvrId, fun(Pid) -> send_all_state_event(Pid, {stop_forcely}) end).

% --
% cancel existing dialing
%
cancel_by_mode({Mode,Pid},Reason) when is_pid(Pid) ->
    send_event(Pid, {cancel_by_mode, {Mode, Reason}});
cancel_by_mode({Mode,IvrId},Reason) when is_binary(IvrId) ->
    to_fsm(IvrId, fun(Pid) -> send_event(Pid, {cancel_by_mode, {Mode, Reason}}) end).

% --
pull_fsm_by_callid(CallId) ->
    ?DLG_STORE:pull_dlg(CallId).

% --
pull_fsm_by_ivrid(IvrId) ->
    case ?DLG_STORE:find_t(IvrId) of
        false -> false;
        {_,#{link:=Lnk}} -> Lnk
    end.

% --
test({_IvrId,Pid}) when is_pid(Pid) ->
    send_all_state_event(Pid, {test}).

% --------------------

%% --
%% builds_tag which doesn't exist. SYNC, wait response
%%
build_tag({_IvrId,Pid},Req) when is_pid(Pid) ->
    sync_send_event(Pid, {build_tag,[Req]}).

replace_abonent({_IvrId,Pid},Req) when is_pid(Pid) ->
    send_event(Pid, {replace_abonent,[Req]}).

%% --------------------

uac_response({_DlgId,Pid},[_Resp]=Args) when is_pid(Pid) ->
    send_event(Pid, {uac_response, Args}),
    continue.

save_callpid({_DlgId,Pid},[_Resp]=Args) when is_pid(Pid) ->
    send_all_state_event(Pid, {save_callpid, Args}),
    continue.

uas_dialog_response({_DlgId,Pid},[_Resp]=Args) when is_pid(Pid) ->
    send_event(Pid, {uas_dialog_response, Args}),
    continue.

sip_ack({_DlgId,Pid},Args) when is_pid(Pid) ->
    send_event(Pid, {sip_ack,Args}).

sip_cancel({_DlgId,Pid},Args) when is_pid(Pid) ->
    send_event(Pid, {sip_cancel,Args}).

sip_bye({_DlgId,Pid},Args) when is_pid(Pid) ->
    send_event(Pid, {sip_bye,Args}).

sip_reinvite({_DlgId,Pid},Args) when is_pid(Pid) ->
    send_event(Pid, {sip_reinvite,Args}).

sip_refer({_DlgId,Pid},Args) when is_pid(Pid) ->
    send_event(Pid, {sip_refer,Args}).

sip_notify({_DlgId,Pid},Args) when is_pid(Pid) ->
    send_all_state_event(Pid, {sip_notify,Args}).

sip_info({_DlgId,Pid},Args) when is_pid(Pid) ->
    send_all_state_event(Pid, {sip_info,Args}).

sip_message({_DlgId,Pid},Args) when is_pid(Pid) ->
    send_all_state_event(Pid, {sip_message,Args}).

call_terminate({_DlgId,Pid},Args) when is_pid(Pid) ->
    send_event(Pid, {call_terminate,Args}).

% --------------------------------
get_current_media_link({_DlgId,Pid}) when is_pid(Pid) ->
    sync_send_all_state_event(Pid, {get_current_media_link}).

%% --------------------------------
%% Return active call info. SYNC.
%% --------------------------------
-spec get_call_info_ivr(IvrId::binary()) -> {ToUri::binary(), FromUri::binary(), CallId::binary(), ToTag::binary(), FromTag::binary()}.
%% --------------------------------
get_call_info_ivr(IvrId) ->
    case pull_fsm_by_ivrid(IvrId) of
        {_DlgId,Pid} when is_pid(Pid) ->
            sync_send_all_state_event(Pid, {get_call_info_ivr});
        _ -> false
    end.

%% --------------------------------
%% Return dialog info. SYNC.
%% --------------------------------
-spec get_info(IvrId::binary(), Mode::term()) -> {ok,Map::map()} | false.
%% --------------------------------
get_info(IvrId,Mode) ->
    case pull_fsm_by_ivrid(IvrId) of
        {_DlgId,Pid} when is_pid(Pid) ->
            sync_send_all_state_event(Pid, {get_info,Mode});
        _ -> false
    end.

%% ====================================================================
%% gen_fsm => gen_statem translation functions
%% ====================================================================

% --------------------
% gen_fsm calls
% --------------------
start_link(Module,Args,Opts) ->
    ?FSMT:start_link(Module,Args,Opts).

send_event(FsmRef,Event) ->
    ?FSMT:send_event(FsmRef,Event).

sync_send_event(FsmRef,Event) ->
    ?FSMT:sync_send_event(FsmRef,Event).

send_all_state_event(FsmRef,Event) ->
    ?FSMT:send_all_state_event(FsmRef,Event).

sync_send_all_state_event(FsmRef,Event) ->
    ?FSMT:sync_send_all_state_event(FsmRef,Event).

% --------------------
% gen_statem callbacks
% --------------------
callback_mode() ->
    ?FSMT:callback_mode(?MODULE).

%% RP-1470
handle_event(EventType, EventContent, StateName, #state_ivr{ext_handler_fun=Fun}=StateData) when is_function(Fun,4) ->
    %?OUT("HANDLE EVENT IVR FSM: ~120p, ~120p", [EventType, EventContent]),
    case Fun(EventType, EventContent, StateName, StateData) of
        continue -> do_handle_event(EventType, EventContent, StateName, StateData);
        Result -> Result
    end;
handle_event(EventType, EventContent, StateName, StateData)->
    do_handle_event(EventType, EventContent, StateName, StateData).

%% @private
do_handle_event(EventType, EventContent, StateName, StateData) ->
    case catch ?FSMT:handle_event(?MODULE,EventType,EventContent,StateName,StateData) of
        {'EXIT',_}=E -> ?OUT("Error: ~120p", [E]), stop;
        T -> T
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

init(Arg) ->
    self() ! 'do_init',
    ?LOGSIP("IVR fsm 'initial'", []),
    {ok, 'initial', {'initial',Arg}}.

% service init (outgoing call)
init_internal({<<"1">>=Mode,Opts0}) ->
    CID = ?EU:uuid_srvidx(),
    Self = self(),
    [App,Domain,Script,IvrId,IvrIdHash,OwnerPid,OwnerMode,Number,CallerId] =
        ?EU:extract_required_props([app,domain,script,ivrid,ivridhash,ownerpid,ownermode,number,callerid],Opts0),
    NowTS = os:timestamp(),
    TS = ?EU:timestamp(NowTS),
    ACallId = ?IVR_UTILS:build_out_callid(),
    % state0
    StateData0 = #state_ivr{cid=CID,
                            ivrid=IvrId,
                            ivridhash=IvrIdHash, % #370
                            invite_ts=TS,
                            acallid=ACallId,
                            domain = Domain,
                            starttime=NowTS},
    d(StateData0, 'init', "srv initing by Mode=~120p",[Mode]),
    % wait for owner acknowledge (to avoid mixing of events from processes)
    wait_acknowledge(Opts0,StateData0),
    % event
    ?EVENT:ivr_init({CID,IvrId,ACallId,TS},#{domain => Domain,
                                             mode => <<"out">>,
                                             rusername => Number,
                                             lusername => CallerId,
                                             called_number => Number}),
    % store
    LStoreData = #{pid => Self,
                   domain => Domain,
                   timestart => erlang:time(),
                   id => IvrId, % binary()
                   link => {IvrId,Self},
                   callids => [], % used only for log
                   fun_cleanup => fun() -> ok end,
                   supv => ?IVR_SUPV},
    ?DLG_STORE:store_t(IvrId, LStoreData, ?STORE_TIMEOUT),
    erlang:send_after(?STORE_REFRESH, Self, {refresh_at_store_local}),
    Opts = lists:keyreplace(number, 1, Opts0, {<<"number">>,Number}),
    %
    OwnerRef = case OwnerMode of
                   0 -> undefined;
                   _ -> erlang:monitor(process,OwnerPid)
               end,
    %
    StateData1 = StateData0#state_ivr{timer_ref=erlang:send_after(?IVR_TIMEOUT, self(), ?IVR_TIMEOUT_MSG),
                                      map=#{app => App,
                                            mode => Mode,
                                            ownerpid => OwnerPid,
                                            ownerref => OwnerRef,
                                            opts => lists:keydelete(script,1,Opts),
                                            aor => {sip,CallerId,Domain}, % #277
                                            domain => Domain,
                                            script => Script,
                                            call_params => [],
                                            fun_res => ?EU:get_by_key(fun_res,Opts0,undefined), % RP-1220
                                            callback_funs => ?EU:get_by_key(callback_funs,Opts0,#{}), % RP-1481
                                            expression_funs => ?EU:get_by_key(expression_funs,Opts,[]),
                                            startparams => #{1 => ?EU:get_by_key(startparam1,Opts,undefined)}, % RP-759
                                            l_store => {IvrId, LStoreData}}},
    d(StateData1, 'init', "srv inited by Mode=~120p",[Mode]),
    self() ! {start_dialing, IvrId},
    {ok, ?DIALING_STATE, debug(StateData1)};

% default init (incoming call)
init_internal(Opts) ->
    CID = ?EU:uuid_srvidx(),
    Self = self(),
    [App,IvrId,IvrNum,Req,ToAOR,Domain,Script,CallParams,CallPid] = ?EU:extract_required_props([app,ivrid,ivrnum,req,aor,domain,script,call_params,call_pid], Opts),
    #sipmsg{call_id=XCallId}=Req,
    NowTS = os:timestamp(),
    TS = ?EU:timestamp(NowTS),
    ?EVENT:ivr_init({CID,IvrId,XCallId,TS},#{domain => Domain,
                                             mode => <<"in">>,
                                             rusername => (element(1,(Req#sipmsg.from)))#uri.user,
                                             lusername => element(2,ToAOR),
                                             called_number => IvrNum}),
    % Local node storage
    LStoreData = #{pid => Self,
                   timestart => erlang:time(),
                   domain => Domain,
                   id => IvrId, % binary()
                   link => {IvrId,Self},
                   callids => [XCallId], % used only for log
                   fun_cleanup => fun() -> ok end},
    ?DLG_STORE:store_t(IvrId, LStoreData, ?STORE_TIMEOUT),
    erlang:send_after(?STORE_REFRESH, Self, {refresh_at_store_local}),
    ?DLG_STORE:push_dlg(XCallId, {IvrId,Self}),
    % Monitor to clear
    Flog = fun(StopReason) -> ?OUT("Process ~p terminated (ivr=~p): ~120p", [Self, IvrNum, StopReason]) end,
    Fclear = fun() -> ?DLG_STORE:unlink_dlg(XCallId),
                      ?DLG_STORE:delete_t(IvrId),
                      ?EVENT:ivr_dead({IvrId,XCallId,TS},#{domain => Domain}) end,
    ?MONITOR:start_monitor(Self, "Sip ivr dlg", [Flog, Fclear]),
    % build state
    State = #state_ivr{cid=CID,
                       ivrid=IvrId,
                       invite_ts=TS,
                       acallid = XCallId,
                       domain = Domain,
                       map=#{app => App,
                             opts => lists:keydelete(script,1,Opts),
                             init_req => Req,
                             aor => ToAOR,
                             domain => Domain,
                             script => Script,
                             callpid => CallPid,
                             call_params => CallParams,
                             trace_events => ?EU:get_by_key(trace_events,Opts,#{}), % supra system's event catchers
                             fun_res => ?EU:get_by_key(fun_res,Opts,undefined), % RP-1220
                             callback_funs => ?EU:get_by_key(callback_funs,Opts,#{}), % RP-1481
                             expression_funs => ?EU:get_by_key(expression_funs,Opts,[]), % supra system's expression funs
                             startparams => #{},
                             l_store => {IvrId, LStoreData}},
                       starttime=NowTS},
    %
    State1 = ?ACTIVE:init([Opts], State),
    d(State1, 'init', "srv inited by XCallId=~120p",[XCallId]),
    {ok, ?ACTIVE_STATE, debug(State1)}.

% @private #350
debug(State) ->
    State#state_ivr{use_media=?U:is_usemedia()}.

% @private
wait_acknowledge(Opts,State) ->
    AckRef = ?EU:get_by_key(acknowledge_ref,Opts,undefined),
    AckTimeout = ?EU:get_by_key(acknowledge_timeout,Opts,undefined),
    case {AckRef,AckTimeout} of
        {undefined,_} -> ok;
        {AckRef,undefined} ->
            d(State, 'init', "wait acknowledge...",[]),
            receive {ack,AckRef} -> d(State, 'init', "got acknowledge!",[])
            after 1000 -> d(State, 'init', "acknowledge timeout",[])
            end;
        _ ->
            d(State, 'init', "wait acknowledge...",[]),
            receive {ack,AckRef} -> d(State, 'init', "got acknowledge!",[])
            after AckTimeout -> d(State, 'init', "acknowledge timeout",[])
            end
    end,
    ok.

% -------------------------
% 'forking' events
% -------------------------

active({replace_abonent, Args}=_Event, StateData) ->
    ?ACTIVE:replace_abonent(Args, StateData);

active({sip_ack, _Args}=_Event, StateData) ->
    {next_state, ?ACTIVE_STATE, StateData#state_ivr{acked=true}};

active({sip_cancel, Args}=_Event, StateData) ->
    ?ACTIVE:sip_cancel(Args, StateData);

active({sip_bye, Args}=_Event, StateData) ->
    ?ACTIVE:sip_bye(Args, StateData);

active({sip_reinvite, [_CallId, Req]=Args}=_Event, #state_ivr{reinvite=#reinvite{}}=StateData) ->
    StateData1 = ?ACTIVE_REINVITE:pending_reinvite(Args, StateData),
    send_response(?Pending("IVR. Session changing."), Req, {?ACTIVE_STATE, StateData1});

active({sip_reinvite, Args}=_Event, StateData) ->
    ?ACTIVE:sip_reinvite(Args, StateData);

%% active({sip_refer, [_CallId, Req]}=_Event, StateData) ->
%%     send_response(?Pending("IVR. Session changing."), Req, {?ACTIVE_STATE, StateData});

active({sip_refer, Args}=_Event, StateData) ->
    ?ACTIVE_REFER:sip_refer(Args, StateData);

active({sip_notify, Args}=_Event, StateData) ->
    ?ACTIVE:sip_notify(Args, StateData);

active({sip_info, Args}=_Event, StateData) ->
    ?ACTIVE:sip_info(Args, StateData);

active({sip_message, Args}=_Event, StateData) ->
    ?ACTIVE:sip_message(Args, StateData);

active({uac_response,Args}, #state_ivr{reinvite=#reinvite{}}=StateData) ->
    ?ACTIVE_REINVITE:uac_response(Args, StateData);

active({uac_response,Args}, StateData) ->
    ?ACTIVE:uac_response(Args, StateData);

active({uas_dialog_response,Args}, StateData) ->
    ?ACTIVE:uas_dialog_response(Args, StateData);

active({call_terminate, Args}=_Event, StateData) ->
    ?ACTIVE:call_terminate(Args, StateData);

active({stop_external, Reason}, StateData) ->
    ?ACTIVE:stop_external(Reason, StateData);

active({test}, StateData) ->
    ?ACTIVE:test(StateData).

active({build_tag, Args}=_Event, _From, StateData) ->
    ?ACTIVE:build_tag_sync(Args, StateData).

% ------------------------
% 'dialing' events
% ------------------------
% #304 commented
%% dialing({start, Args}=_Event, StateData) ->
%%     ?DIALING:start(Args,StateData);

dialing({cancel_by_mode,{_Mode,_Reason}=Args}, StateData) ->
    ?DIALING:cancel_by_mode(Args,StateData);

dialing({stop_external,Reason}, StateData) ->
    ?DIALING:stop_external(Reason,StateData);

dialing({uac_response,Args}, StateData) ->
    ?DIALING:uac_response(Args, StateData);

dialing({sip_cancel, Args}=_Event, StateData) ->
    ?DIALING:sip_cancel(Args, StateData);

dialing({sip_bye, Args}=_Event, StateData) ->
    ?DIALING:sip_bye(Args, StateData);

dialing({sip_reinvite, Args}=_Event, StateData) ->
    ?DIALING:sip_reinvite(Args, StateData);

dialing({call_terminate, Args}=_Event, StateData) ->
    ?DIALING:call_terminate(Args, StateData).

% ------------------------
% 'stopping' events
% ------------------------
stopping({replace_abonent, [Req]}=_Event, StateData) ->
    send_response(?Forbidden("IVR. Session stopping"), Req, {?STOPPING_STATE, StateData}),
    {next_state, ?STOPPING_STATE, StateData};

stopping({sip_bye, [_CallId, Req]}=_Event, StateData) ->
    {ok,ReqHandle} = nksip_request:get_handle(Req),
    ?U:send_sip_reply(fun() -> nksip_request:reply(accepted, ReqHandle) end),
    {next_state, ?STOPPING_STATE, StateData};

stopping({sip_reinvite, [_CallId, Req]}=_Event, StateData) ->
    {ok,ReqHandle} = nksip_request:get_handle(Req),
    ?U:send_sip_reply(fun() -> nksip_request:reply(?TemporarilyUnavailable("IVR. Stopping"), ReqHandle) end),
    {next_state, ?STOPPING_STATE, StateData};

stopping({sip_refer, [_CallId, Req]}=_Event, StateData) ->
    {ok,ReqHandle} = nksip_request:get_handle(Req),
    ?U:send_sip_reply(fun() -> nksip_request:reply(?TemporarilyUnavailable("IVR. Stopping"), ReqHandle) end),
    {next_state, ?STOPPING_STATE, StateData};

stopping(Event, StateData) ->
    E = case is_tuple(Event) of
            true -> erlang:element(1, Event);
            false -> Event
        end,
    d(StateData, ?STOPPING_STATE, "event ~120p", [E]),
    {next_state, ?STOPPING_STATE, StateData}.

% ------------------------
% All states events
% ------------------------

handle_event({sip_notify, [_CallId,Req]=Args}=_Event, StateName, StateData) ->
    case StateName of
        ?STOPPING_STATE -> send_response(?Forbidden("IVR. Session stopping"), Req, {StateName, StateData});
        ?ACTIVE_STATE -> ?ACTIVE:sip_notify(Args, StateData)
    end;

handle_event({sip_info, [_CallId,Req]=Args}=_Event, StateName, StateData) ->
    case StateName of
        ?STOPPING_STATE -> send_response(?Forbidden("IVR. Session stopping"), Req, {StateName, StateData});
        ?ACTIVE_STATE -> ?ACTIVE:sip_info(Args, StateData)
    end;

handle_event({sip_message, [_CallId,Req]=Args}=_Event, StateName, StateData) ->
    case StateName of
        ?STOPPING_STATE -> send_response(?Forbidden("IVR. Session stopping"), Req, {StateName, StateData});
        ?ACTIVE_STATE -> ?ACTIVE:sip_message(Args, StateData)
    end;

handle_event({stop_forcely}, _StateName, StateData) ->
    {stop, normal, StateData};

handle_event({save_callpid, [{callpid,CallPid}]}=_Event, StateName, #state_ivr{map=Map}=StateData) ->
    case maps:get(callpid,Map,undefined)=:=undefined of
        true ->
            NewMap = Map#{callpid=>CallPid},
            NewStateData = StateData#state_ivr{map=NewMap},
            {next_state, StateName, NewStateData};
        false ->
            {next_state, StateName, StateData}
    end;

handle_event(Event, StateName, StateData) ->
    E = case is_tuple(Event) of
            true -> erlang:element(1, Event);
            false -> Event
        end,
    d(StateData, StateName, "handle_event ~120p", [E]),
    {next_state, StateName, StateData}.

% -------------------------
% 'active' requests
% 'stopping' requests
% -------------------------

% -------------------------
% All states requests
% -------------------------

%%
handle_sync_event({get_current_media_link}, _From, StateName, StateData) ->
    Reply = ?IVR_MEDIA:get_current_media_link(StateData),
    {reply, Reply, StateName, StateData};

%% ----
handle_sync_event({get_call_info_ivr}, _From, StateName, #state_ivr{a=undefined,forks=[{CallId,#{fork:=Fork}}|_]}=StateData) ->
    #side_fork{remoteuri=ToUri,remotetag=ToTag,localuri=FromUri,localtag=FromTag}=Fork,
    Reply = {ToUri, FromUri, CallId, ToTag, FromTag},
    {reply, Reply, StateName, StateData};

handle_sync_event({get_call_info_ivr}, _From, StateName, #state_ivr{a=Side}=StateData) ->
    #side{callid=CallId,remoteuri=ToUri,remotetag=ToTag,localuri=FromUri,localtag=FromTag}=Side,
    Reply = {ToUri, FromUri, CallId, ToTag, FromTag},
    {reply, Reply, StateName, StateData};

%% ----
%% RP-1344
handle_sync_event({get_info, Opts}, From, StateName, #state_ivr{a=undefined,forks=[{CallId,#{fork:=Fork}}|_]}=StateData) ->
    #side_fork{remoteuri=RUri,remotetag=RTag,localuri=LUri,localtag=LTag}=Fork,
    handle_sync_event({get_info,Opts,{'out',CallId,RUri,RTag,LUri,LTag}}, From, StateName, StateData);

handle_sync_event({get_info, Opts}, From, StateName, #state_ivr{a=Side}=StateData) ->
    #side{dir=Dir,callid=CallId,remoteuri=RUri,remotetag=RTag,localuri=LUri,localtag=LTag} = Side,
    handle_sync_event({get_info,Opts,{Dir,CallId,RUri,RTag,LUri,LTag}}, From, StateName, StateData);

handle_sync_event({get_info, Opts, {Dir,CallId,RUri,RTag,LUri,LTag}}, _From, StateName, StateData) ->
    #state_ivr{scriptmachine=SMInfo,
               map=Map,
               starttime=ITS}=StateData,
    FUri = case lists:member(unparse,Opts) of
               true -> fun(URI) -> ?U:unparse_uri(URI) end;
               false -> fun(URI) -> URI end
           end,
    M = case SMInfo of
            undefined -> #{state => dialing};
            #{} -> #{state => active,
                     script => #{pid => maps:get(pid,SMInfo),
                                 id => maps:get(id,SMInfo)}}
        end,
    Reply = M#{dir => Dir,
               domain => maps:get(domain,Map),
               callid => CallId,
               ruri => FUri(RUri),
               rtag => RTag,
               luri => FUri(LUri),
               ltag => LTag,
               fsmpid => self(),
               call_params => case maps:get(call_params,Map) of [] -> #{}; CP -> ?EU:json_to_map(CP) end,
               startparams => maps:get(startparams,Map),
               starttime => ITS},
    {reply, {ok,Reply}, StateName, StateData};

%% ----
handle_sync_event(Event, _From, StateName, StateData) ->
    E = case is_tuple(Event) of
            true -> erlang:element(1, Event);
            false -> Event
        end,
    d(StateData, StateName, "handle_sync_event ~120p", [E]),
    {reply, b2bua_sync_event_default, StateName, StateData}.

% -------------------------
% Process messages
% -------------------------

%%
handle_info('do_init', 'initial', {'initial',Opts}) ->
    {ok,StateName,StateData} = init_internal(Opts),
    {next_state,StateName,StateData};

% async start when incoming mode
handle_info({start, Ref}, ?ACTIVE_STATE, #state_ivr{ref=Ref}=StateData) ->
    ?ACTIVE:start(StateData);

% #304 async start when dialing mode
handle_info({start_dialing, IvrId}, ?DIALING_STATE, #state_ivr{ivrid=IvrId}=StateData) ->
    case ?DIALING:start(StateData) of
        {ok,StateData1} ->
            {next_state, ?DIALING_STATE, StateData1};
        {{error,_},StateData1} ->
            % #304 parse error
            ?DIALING_UTILS:send_result(StateData, 'unsuccess_res', {self(),#{sipcode=>500}}),
            ?IVR_UTILS:error_mgc_final("Init error, probably mgc", StateData1, undefined);
        {next_state,_,_}=Res -> Res
    end;

handle_info({refresh_at_store_local}, StateName, StateData) ->
    d(StateData, StateName, "refresh_at_store"),
    #state_ivr{map=#{l_store:={LStoreKey,LStoreData}}}=StateData,
    ?DLG_STORE:store_t(LStoreKey, LStoreData, ?STORE_TIMEOUT),
    erlang:send_after(?STORE_REFRESH, self(), {refresh_at_store_local}),
    {next_state, StateName, StateData};

handle_info(?IVR_TIMEOUT_MSG, StateName, StateData) ->
    d(StateData, StateName, "ivr_timeout"),
    StopReason = ?IVR_UTILS:reason_timeout([ivr], <<>>, <<"Global timeout">>),
    ?MODULE:stop(self(), StopReason),
    {next_state, StateName, StateData};

%% B2B would timeout fork itself
%% handle_info({?FORK_TIMEOUT_MSG, _Args}, ?DIALING_STATE, StateData) ->
%%     %?DIALING:fork_timeout(Args, StateData);
%%     {next_state, ?DIALING_STATE, StateData};
%%
%% handle_info({?FORK_TIMEOUT_MSG, _Args}, ?ACTIVE_STATE, StateData) ->
%%     %?ACTIVE:fork_timeout(Args, StateData);
%%     {next_state, ?ACTIVE_STATE, StateData};

handle_info({stopping_timeout}, ?STOPPING_STATE, StateData) ->
    final(StateData),
    {stop, normal, StateData};

handle_info({mg_dtmf, Args}, ?ACTIVE_STATE, StateData) ->
    ?ACTIVE:mg_dtmf(Args, StateData);

handle_info({mg_event, Args}, ?ACTIVE_STATE, StateData) ->
    ?ACTIVE:mg_event(Args, StateData);

handle_info({mg_trunknovoice, _Args}, StateName, StateData) ->
    {next_state, StateName, StateData};

% ----------
handle_info({mg_disconnected, Args}, StateName, StateData) ->
    self() ! {mg_migrate_priv, Args, 0, 64},
    %erlang:send_after(?EU:random(200), self(), {mg_migrate_priv, Args, 0, 64}),
    {next_state, StateName, StateData};

% -
handle_info({mg_migrate, Args}, StateName, StateData) ->
    self() ! {mg_migrate_priv, Args, 0, 64},
    %erlang:send_after(?EU:random(200), self(), {mg_migrate_priv, Args, 0, 64}),
    {next_state, StateName, StateData};

% -
handle_info({mg_migrate_priv,Args,I,T}, StateName, StateData) ->
    d(StateData, StateName, "MG found disconnected priv"),
    Count = 1,
    case ?IVR_MEDIA:media_check_mg(StateData, Args) of
        false -> ok;
        true -> case ?IVR_MEDIA:check_available_slots(StateData) of
                    {ok,_MgCnt,CtxCnt} when CtxCnt > 100, CtxCnt > Count -> self() ! {change_mg};
                    _ -> case I > 11 of % 64 + 128 + ... + 1024 = 8 sec
                             false ->
                                 P = {mg_migrate_priv, Args, I+1, case T>=1000 of true->T;false->T*2 end},
                                 erlang:send_after(T, self(), P);
                             true ->
                                 StopReason = ?IVR_UTILS:reason_timeout([migrate, media, wait_available], <<>>, <<"Available slots not found for migration">>),
                                 stop(self(), StopReason)
                         end end end,
    {next_state, StateName, StateData};

% -
handle_info({change_mg}, ?STOPPING_STATE, StateData) ->
    {next_state, ?STOPPING_STATE, StateData};
handle_info({change_mg}, ?ACTIVE_STATE, StateData) ->
    ?ACTIVE_MIGRATING:migrate(StateData);
handle_info({change_mg}, ?DIALING_STATE, StateData) ->
    ?DIALING_MIGRATING:migrate(StateData);
% -----

handle_info({scriptmachine,{sm_events,_}=Event}, CurrentState, StateData) ->
    ?ACTIVE:scriptmachine_event({Event, CurrentState}, StateData);

handle_info({scriptmachine,Event}, ?ACTIVE_STATE, StateData) ->
    ?ACTIVE:scriptmachine_event(Event, StateData);

% -----

%% RP-1470 extensions were handled earlier in workflow
handle_info({extension,_}, StateName, StateData)  ->
    {next_state, StateName, StateData};

% -----

handle_info({reinvite_timeout,Args}, ?ACTIVE_STATE, #state_ivr{reinvite=#reinvite{}}=StateData) ->
    ?ACTIVE_REINVITE:reinvite_timeout(Args, StateData);

% -----

%% down of owner (proc is linked, and auto down instead of message)
handle_info({'DOWN', Ref, process, _Pid, Result}, ?ACTIVE_STATE, #state_ivr{scriptmachine=#{monitorref:=Ref}}=StateData) ->
    ?ACTIVE:scriptmachine_down(Result, StateData);

% -----

%% down of owner for dialing by mode
handle_info({'DOWN', _Ref, process, Pid, _Result}, ?DIALING_STATE, #state_ivr{map=Map}=StateData) ->
    %Mode = maps:get(mode,Map),
    Opts = maps:get(opts,Map),
    [OwnerPid] = ?EU:extract_optional_props([ownerpid],Opts),
    case Pid of
        OwnerPid -> ?DIALING:stop_external(<<"Owner_down">>,StateData);
        _ -> {next_state, ?DIALING_STATE, StateData}
    end;

handle_info({'DOWN', Ref, process, _Pid, Result}, ?STOPPING_STATE, #state_ivr{scriptmachine=#{monitorref:=Ref},map=#{mode:=<<"1">>}}=StateData) ->
    #state_ivr{a=#side{callid=ACallId,
        remotetag=RemoteTag,
        localtag=LocalTag}}=StateData,
    Msg = {self(),#{ivr_result => Result,
        callid => ACallId,
        remotetag => RemoteTag,
        localtag => LocalTag}},
    ?DIALING_UTILS:send_result(StateData, 'ivr_stopped', Msg),
    {next_state, ?STOPPING_STATE, StateData};

% -----

handle_info(Event, StateName, StateData) ->
    E = case is_tuple(Event) of
            true -> erlang:element(1, Event);
            false -> Event
        end,
    d(StateData, StateName, "handle_info ~120p", [E]),
    {next_state, StateName, StateData}.

% -------------------------
%
% -------------------------
terminate(_Reason, _StateName, _StateData) ->
    ok.

code_change(_OldVsn, _StateName, _StateData, _Extra) ->
    ok.

%% ====================================================================
%% Internal functions
%% ====================================================================

% ---
generate_ids() ->
    <<B6:48/bitstring, _Rest/bitstring>> = ?U:luid(),
    SrvCode = ?U:get_current_srv_textcode(),
    DlgId = <<"rDlg-", SrvCode/bitstring, "-", B6/bitstring>>,
    case ?DLG_STORE:find_t(DlgId) of
        false -> {DlgId, B6, ?U:int64_hash(<<SrvCode/binary,B6/binary>>)}; % #370
        _ -> generate_ids()
    end.

% ---
send_response(SipReply, Req, {StateName, StateData}) ->
    {ok,ReqHandle} = nksip_request:get_handle(Req),
    ?U:send_sip_reply(fun() -> nksip_request:reply(SipReply, ReqHandle) end),
    {next_state, StateName, StateData}.

% ---
to_fsm(IvrId, Fun) ->
    case ?DLG_STORE:find_t(IvrId) of
        false -> {error, not_found};
        {_,Pid} -> Fun(Pid)
    end.

% ---
final(StateData) ->
    d(StateData, ?STOPPING_STATE, "final"),
    #state_ivr{ivrid=IvrId}=StateData,
    {ok,_} = ?IVR_MEDIA:media_stop(StateData), % media
    ?DLG_STORE:delete_t(IvrId), % store
    ?MONITOR:stop_monitor(self()), % monitor
    ?IVR_SUPV:drop_child(IvrId). % supv

% -----
d(StateData, StateName, Text) -> d(StateData, StateName, Text, []).
d(StateData, StateName, Fmt, Args) ->
    #state_ivr{ivrid=IvrId}=StateData,
    ?LOGSIP("IVR fsm ~p '~p':" ++ Fmt, [IvrId,StateName] ++ Args).
