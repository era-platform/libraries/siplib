%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Presents 'active' state of IVR FSM
%%% @todo

-module(r_sip_ivr_fsm_active).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([init/2,
         start/1,
         stop_external/2,
         test/1,
         build_tag_sync/2,
         replace_abonent/2,
         sip_cancel/2,
         sip_bye/2,
         sip_reinvite/2,
         sip_notify/2,
         sip_info/2,
         sip_message/2,
         uas_dialog_response/2,
         call_terminate/2,
         fork_timeout/2,
         uac_response/2,
         mg_dtmf/2,
         mg_event/2,
         scriptmachine_event/2,
         scriptmachine_down/2]).

-export([send_sipinfo/4]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_ivr.hrl").
-include("../include/r_sip_mgc.hrl").
-include("../include/r_sip_headers.hrl").

% -include("../include/r_nksip.hrl").

-define(CURRSTATE, active).

%% ====================================================================
%% API functions
%% ====================================================================

init(Args, State) ->
    do_init(Args, State).

start(State) ->
    do_start(State).

% ----
stop_external(Reason, State) ->
    d(State, "stop_external(~p)", [Reason]),
    % @todo bye/4xx to a, cancel forks
    #state_ivr{a=Side}=State,
    case Side of
        #side{} -> ?IVR_UTILS:send_bye(Side, State);
        _ -> ok
    end,
    StopReason = ?IVR_UTILS:reason_external(Reason),
    ?IVR_UTILS:return_stopping(?IVR_UTILS:do_finalize_ivr(State#state_ivr{stopreason=StopReason})).

% ----
test(State) ->
    d(State, "test"),
    {next_state, ?CURRSTATE, State}.

% ----
build_tag_sync([_Req], State) ->
    {Tag,State1} = ?IVR_UTILS:generate_tag(State),
    Reply = Tag,
    {reply, Reply, ?CURRSTATE, State1}.

% ----
replace_abonent([#sipmsg{call_id=CallId}=Req], State) ->
    d(State, "replace, CallId=~120p", [CallId]),
    do_replace_abonent([CallId,Req], State).

%% =========================

% ----
sip_cancel([CallId, _Req, _InviteReq], State) ->
    d(State, "sip_cancel, CallId=~120p", [CallId]),
    %
    #state_ivr{a=Side}=State,
    do_stop(CallId, State#state_ivr{a=Side#side{state=cancelled}}).

% ----
sip_bye([CallId, Req], State) ->
    d(State, "sip_bye, CallId=~120p", [CallId]),
    State1 = ?IVR_UTILS:send_response(Req, ok, State),
    % #300
    case State of
        #state_ivr{a=#side{callid=CallId}=Side} ->
            do_stop(CallId, State1#state_ivr{a=Side#side{state=bye}});
        _ ->
            % from store
            ?DLG_STORE:unlink_dlg(CallId),
            {next_state, ?CURRSTATE, State}
    end.

%% =========================

% ----
sip_reinvite([CallId, _Req]=P, State) ->
    d(State, "sip_reinvite, CallId=~120p", [CallId]),
    do_reinvite_incoming(P, State).

% ----
sip_notify([CallId, Req]=P, State) ->
    d(State, "sip_notify, CallId=~120p", [CallId]),
    % RP-1707 gen_event to owner about referring dialog callid
    case nksip_request:header(?ReferringCallIdLow, Req) of
        {ok,[RefCallId|_]} ->
            Content = case Req of
                          #sipmsg{content_type={<<"message/sipfrag">>,_}=_CT,body=Body} -> Body;
                          _ -> <<>>
                      end,
            ?DIALING_UTILS:send_result(State, 'refer_notify', {self(),#{callid => RefCallId, content => Content}});
        _ -> ok
    end,
    %
    State1 = ?IVR_UTILS:send_response(Req, {200,[]}, State),
    ?ACTIVE_SM:sip_notify(P, State1).

% ----
sip_info([CallId, Req]=P, State) ->
    d(State, "sip_info, CallId=~120p", [CallId]),
    State1 = ?IVR_UTILS:send_response(Req, {200,[]}, State),
    ?ACTIVE_SM:sip_info(P, State1).

% ----
sip_message([CallId,Req]=P, State) ->
    d(State, "sip_message, CallId=~120p", [CallId]),
    State1 = ?IVR_UTILS:send_response(Req, {200,[]}, State),
    ?ACTIVE_SM:sip_message(P, State1).

%% =========================

% ----
uas_dialog_response([_Resp]=P, State) ->
    #sipmsg{class={resp,SipCode,_},
            cseq={CSeq,Method},
            call_id=CallId}=_Resp,
    d(State, "uas_dialog_response ~p:~p -> ~p, CallId=~120p", [Method,CSeq,SipCode,CallId]),
    do_on_uas_dialog_response(P, State).

% ----
call_terminate([Reason, Call], State) ->
    #call{call_id=CallId}=Call,
    case State of
        #state_ivr{acallid=CallId} ->
            d(State, "call_terminate, CallId=~120p, Reason=~120p", [CallId, Reason]),
            do_stop(CallId, State);
        _ -> {next_state, ?CURRSTATE, State}
    end.

% ============================

% ----
fork_timeout([CallId, BLTag]=P, State) ->
    d(State, "fork_timeout BLTag=~120p, CallId=~120p", [BLTag, CallId]),
    do_on_fork_timeout(P, State).

% ----
uac_response([_Resp]=Args, State) ->
    #sipmsg{class={resp,SipCode,SipReason},
            cseq={CSeq,Method},
            call_id=CallId}=_Resp,
    d(State, "uac_response ~p:~p -> ~p ~p, CallId=~120p", [Method,CSeq,SipCode,SipReason,CallId]),
    do_on_uac_response(Args, State).

% ============================
mg_dtmf([_CtxId, CtxTermId, #{}=Event]=_Args, State) ->
    ?LOGSIP("DTMF RFC2833/inband in ivr: ~p from ~p", [maps:get(dtmf,Event), CtxTermId]),
    ?ACTIVE_SM:mg_dtmf(Event, State).

mg_event([_CtxId, CtxTermId, EType, #{}=Event]=_Args, State) ->
    ?LOGSIP("Event ~s in ivr from ~p", [EType, CtxTermId]),
    ?ACTIVE_SM:mg_event(EType, Event, CtxTermId, State).

% ============================
scriptmachine_event(Event, State) ->
    ?ACTIVE_SM:handle_event(Event, State).

scriptmachine_down(Result, State) ->
    ?ACTIVE_SM:handle_down(Result, State).

% ============================
send_sipinfo(ContentType, Body, FunResult, State) ->
    %d(State, "send_sipinfo ~120tp", [Body]),
    do_send_sipinfo(ContentType, Body, FunResult, State).

%% ====================================================================
%% Internal functions
%% ====================================================================

% --------
% when init ivr by incoming invite
do_init([Opts]=_Args, State) ->
    [Req] = ?EU:extract_required_props([req], Opts),
    Ref = make_ref(),
    {ASide,State1} = ?ACTIVE_UTILS:prepare_side(Req, State),
    State2 = State1#state_ivr{a=ASide,
                              ref=Ref,
                              timer_ref=erlang:send_after(?IVR_TIMEOUT, self(), ?IVR_TIMEOUT_MSG)},
    %
    #side{callid=ACallId,localuri=#uri{domain=ADomain}}=ASide,
    ?STAT_FACADE:link_domain(ACallId, ADomain), % RP-415
    ?IVR_UTILS:log_callid(incoming,State#state_ivr.ivrid,ASide#side.callid,ADomain), % #354, RP-415
    %
    self() ! {start, Ref},
    State2.

% --------
% first queue message to make ivr active (check MG connect)
do_start(State) ->
    #state_ivr{a=Side,
               map=Map}=State,
    case ?IVR_MEDIA:check_available(State) of
        {error,mgc_overload} ->
            ?IVR_UTILS:error_mgc_final("MGC overload", State, Side, ?InternalError("IVR. MGC overload"));
        {error,_} ->
            ?IVR_UTILS:error_mgc_final("MGC not ready", State, Side, ?InternalError("IVR. MGC not ready"));
        {ok,false} ->
            ?IVR_UTILS:error_mgc_final("No media trunk available", State, Side, ?InternalError("IVR. No media trunk available"));
        {ok,true} ->
            State1 = ?ACTIVE_SM:start_script(State),
            {next_state, ?CURRSTATE, State1#state_ivr{map=maps:without([script],Map)}}
    end.

% -------
% stops ivr session
do_stop(_CallId, State) ->
    ?IVR_UTILS:return_stopping(?IVR_UTILS:do_finalize_ivr(State)).

% -------
% replaces
do_replace_abonent([XCallId, XReq], State) ->
    Self = self(),
    #state_ivr{ivrid=IvrId,
               a=#side{callid=ACallId,state=S,responses=R,answertime=A}=ASide}=State,
    % new to store
    ?DLG_STORE:push_dlg(XCallId, {IvrId,Self}),
    ?MONITOR:append_fun(self(), fun() -> ?DLG_STORE:unlink_dlg(XCallId) end),
    % prev from store
    ?IVR_UTILS:send_bye(ASide, State),
    ?DLG_STORE:unlink_dlg(ACallId),
    % new side
    {XSide1,State1} = ?ACTIVE_UTILS:prepare_side(XReq, State),
    XSide2 = XSide1#side{state=S,
                         answertime=A},
    State2 = State1#state_ivr{a=XSide2,
                              acallid=XCallId},
    ?EVENT:ivr_replace(State2, #{mode => <<"replaces">>,
                                 rusername => (element(1,(XReq#sipmsg.from)))#uri.user,
                                 lusername => (element(1,(XReq#sipmsg.from)))#uri.user}),
    SipReply = case R of
                   [SR|_] -> SR;
                   _ -> undefined
               end,
    case S of
        'incoming' -> {next_state, ?CURRSTATE, State2};
        'ringing' -> r_sip_ivr_script_component_answer:do_answer_nosdp({null,null,null},{S,SipReply},State2);
        'early' -> r_sip_ivr_script_component_answer:do_answer_sdp_replace(ACallId,{S,SipReply},State2);
        'dialog' -> r_sip_ivr_script_component_answer:do_answer_sdp_replace(ACallId,{S,SipReply},State2)
    end.

% --------
% modifies participant's media by incoming reinvite
do_reinvite_incoming([CallId, Req]=P, State) ->
    #state_ivr{a=#side{callid=CallId}=Side}=State,
    #sdp{}=RSdp=?U:extract_sdp(Req),
    case ?IVR_MEDIA:media_sessionchange(Req, Side, State) of
        {error, SipReply} ->
            State1 = ?IVR_UTILS:send_response(Req, SipReply, State),
            {next_state, ?CURRSTATE, State1};
        {ok, State1, LSdp} ->
            Opts = [{body, LSdp},
                    user_agent],
            State2 = ?IVR_UTILS:send_response(Req, {200,Opts}, State1),
            State3 = State2#state_ivr{a=Side#side{remotesdp=RSdp}},
            ?ACTIVE_SM:sip_reinvite_incoming(P, State3)
    end.

%% ====================================================================

%% -----
%% UAC fork timeout
%%
do_on_fork_timeout([CallId, _LTag]=P, State) ->
    #state_ivr{forks=Forks}=State,
    case lists:keyfind(CallId,1,Forks) of
        false ->
            {next_state, ?CURRSTATE, State};
        {_,#side_fork{}=_Side} ->
            {next_state, ?CURRSTATE, State};
        {_,#{type:='refer'}=Refer} ->
            ?ACTIVE_REFER:fork_timeout(P, Refer, State)
    end.

%% -----
%% UAC response handlers
%%
do_on_uac_response([#sipmsg{cseq={_,Method}}=Resp]=P, State) ->
    #sipmsg{class={resp,SipCode,_}, call_id=CallId}=Resp,
    d(State, "uac_response(~p) ~p from ~p", [Method, SipCode, CallId]),
    {ok,State1} = ?ACTIVE_SM:uac_response(Resp, State),
    do_on_uac_response_1(P, State1).

%
do_on_uac_response_1([#sipmsg{cseq={_,'INVITE'}, call_id=CallId}=Resp]=P, State) ->
    #sipmsg{class={resp,SipCode,_}}=Resp,
    #state_ivr{forks=Forks}=State,
    d(State, "uac_response('INVITE') ~p from Z: ~p", [SipCode, CallId]),
    case lists:keyfind(CallId,1,Forks) of
        false ->
            {next_state, ?CURRSTATE, State};
        {_,#side_fork{}=_Side} ->
            {next_state, ?CURRSTATE, State};
        {_,#{type:='refer'}=Refer} ->
            ?ACTIVE_REFER:uac_response(P, Refer, State)
    end;
do_on_uac_response_1(_, State) ->
    {next_state, ?CURRSTATE, State}.

%% ====================================================================

%% ============================
%% UAS response, dialog ready
%% ============================

do_on_uas_dialog_response([#sipmsg{class={resp,SipCode,_}, cseq={_,'INVITE'}}=Resp], State)
  when SipCode >= 200, SipCode < 300 ->
    %#sipmsg{to={_,ToTag}}=Resp,
    %{ok,XCallId} = nksip_response:call_id(Resp),
    {ok,XDlgHandle} = nksip_dialog:get_handle(Resp),
    #state_ivr{a=Side}=State,
    State1 = State#state_ivr{a=Side#side{dhandle=XDlgHandle}},
    {next_state, ?CURRSTATE, State1};
do_on_uas_dialog_response(_P, State) ->
    {next_state, ?CURRSTATE, State}.

%% =====================================================================

do_send_sipinfo(ContentType, Body, FunResult, #state_ivr{a=A}=State) ->
    #side{dhandle=DlgHandle,
          localtag=LTag}=A,
    case catch nksip_uac:info(DlgHandle, [async,{content_type,ContentType},{body,Body}]) of
        {'EXIT',Err} ->
            d(State, "send_reinvite_offer to LTag=~p, Caught error=~120p", [LTag, Err]),
            FunResult({error,exit});
        {error,_R}=Err ->
            d(State, "send_reinvite_offer to LTag=~p, Error=~120p", [LTag, Err]),
            FunResult(Err);
        {async,_ReqHandle} ->
            d(State, "send_reinvite_offer to LTag=~p", [LTag]),
            FunResult({ok,async})
    end,
    {next_state, ?CURRSTATE, State}.

%% =====================================================================

% @private ----
d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    #state_ivr{ivrid=IvrId}=State,
    ?LOGSIP("IVR fsm ~p '~p':" ++ Fmt, [IvrId,?CURRSTATE] ++ Args).
