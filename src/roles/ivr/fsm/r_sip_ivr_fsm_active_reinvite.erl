%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 20.06.2016
%%% @doc make outgoing reinvite to change session
%%% @todo

-module(r_sip_ivr_fsm_active_reinvite).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([send_reinvite/4,
         pending_reinvite/2,
         uac_response/2,
         reinvite_timeout/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_ivr.hrl").
-include("../include/r_sip_mgc.hrl").

% -include("../include/r_nksip.hrl").

-define(CURRSTATE, 'active').
-define(ACTIVESTATE, 'active').

%% ====================================================================
%% API functions
%% ====================================================================

send_reinvite(Mode, FunBackupL, FunResult, State)
  when Mode=='t30'; Mode=='t38';
       Mode=='sendrecv'; Mode=='sendonly'; Mode=='inactive' ->
    d(State, "RE. send_reinvite ~120p", [Mode]),
    send_reinvite_1(Mode, FunBackupL, FunResult, State);
send_reinvite(#sdp{}=LSdp, FunBackupL, FunResult, State) ->
    d(State, "RE. send_reinvite by lsdp", []),
    send_reinvite_1(LSdp, FunBackupL, FunResult, State).
% @private
send_reinvite_1(P, FunBackupL, FunResult, State) ->
    try do_send_reinvite(P, FunBackupL, FunResult, State)
    catch _:R ->
              operation_response(R, FunResult),
              {next_state, ?ACTIVESTATE, State}
    end.

% ----
pending_reinvite([_CallId, _Req], State) ->
    d(State, "RE pending_reinvite, CallId=~120p", [_CallId]),
    do_on_pending_reinvite(State).

% ----
uac_response([_Resp]=P, State) ->
    #sipmsg{class={resp,SipCode,SipReason},
            cseq={CSeq,Method},
            call_id=CallId}=_Resp,
    d(State, "RE uac_response ~p:~p -> ~p ~p, CallId=~120p", [Method,CSeq,SipCode,SipReason,CallId]),
    do_on_uac_response(P, State).

% ----
reinvite_timeout([<<"total">>|_]=Args, State) ->
    d(State, "RE timeout total", []),
    do_on_uac_timeout(Args, State);

reinvite_timeout([<<"pending">>]=Args, State) ->
    d(State, "RE timeout pending", []),
    do_on_pending_timeout(Args, State).

%% ====================================================================
%% Internal functions
%% ====================================================================

%
do_send_reinvite(Mode, FunBackupL, FunResult, State)
  when Mode=='t30'; Mode=='t38';
       Mode=='sendrecv'; Mode=='sendonly'; Mode=='inactive' ->
    {ok,State1,LSdp} = get_lsdp(State),
    case is_function(FunBackupL) of true -> FunBackupL(LSdp); false -> ok end,
    LSdp1 = case Mode of
                't38' -> ?M_SDP:update_by_t38(LSdp);
                't30' -> ?M_SDP:update_by_t30(LSdp);
                'sendrecv' -> ?M_SDP:sendrecv(LSdp);
                'sendonly' -> ?M_SDP:sendonly(LSdp);
                'inactive' -> ?M_SDP:inactive(LSdp)
            end,
    {ok,State2,LSdp2} = set_lsdp(LSdp1,State1),
    do_send_reinvite_1(LSdp2,FunResult,State2);
%
do_send_reinvite(LSdp, _, FunResult, State) ->
    {ok,State1,LSdp1} = set_lsdp(LSdp,State),
    do_send_reinvite_1(LSdp1,FunResult,State1).


% @private
get_lsdp(State) ->
    case ?IVR_MEDIA:get_local_sdp(State, true) of
        {error,_}=Err -> throw(Err);
        {ok,State1,LSdp} -> {ok,State1,LSdp}
    end.

% @private
set_lsdp(LSdp, State) ->
    case ?IVR_MEDIA:set_local_sdp(LSdp, State) of
        {error,_}=Err -> throw(Err);
        {ok,State1,LSdp1} -> {ok,State1,LSdp1}
    end.

%% =============================================================
%% Start reinvite
%% =============================================================

% ----
do_send_reinvite_1(LSdp,FunResult,State) ->
    InviteOpts = [{body,LSdp},
                  user_agent,
                  auto_2xx_ack],
    case send_reinvite_offer(InviteOpts, State) of
        error ->
            operation_response({error,<<"Error sending invite request">>}, FunResult),
            {next_state, ?ACTIVESTATE, State};
        {ok,ReqHandle} ->
            StateReinvite = #reinvite{timestart=os:timestamp(),
                                      timer_ref=erlang:send_after(?REINVITE_TIMEOUT, self(), {reinvite_timeout, [<<"total">>]}),
                                      rhandle=ReqHandle,
                                      lsdp=LSdp,
                                      offer_opts=InviteOpts,
                                      funresult=FunResult},
            State1 = State#state_ivr{reinvite=StateReinvite},
            %
            d(State1, " -> switch to '~p'", [?CURRSTATE]),
            {next_state, ?CURRSTATE, State1}
    end.

% ----
send_reinvite_offer(InviteOpts, #state_ivr{a=A}=State) ->
    #side{dhandle=DlgHandle,
          localtag=LTag}=A,
    case catch nksip_uac:invite(DlgHandle, [async|InviteOpts]) of
        {'EXIT',Err} ->
            d(State, "send_reinvite_offer to LTag=~p, Caught error=~120p", [LTag, Err]),
            error;
        {error,_R}=Err ->
            d(State, "send_reinvite_offer to LTag=~p, Error=~120p", [LTag, Err]),
            error;
        {async,ReqHandle} ->
            d(State, "send_reinvite_offer to LTag=~p", [LTag]),
            {ok,ReqHandle}
    end.

% ----
% @private nksip's bug, when recv incoming opposite reinvite while offering, offer is not being sent.
%
do_on_pending_reinvite(#state_ivr{reinvite=#reinvite{pending_ref=undefined}=Re}=State) ->
    TimerRef = erlang:send_after(1000, self(), {reinvite_timeout, [<<"pending">>]}),
    State#state_ivr{reinvite=Re#reinvite{pending_ref=TimerRef}};
do_on_pending_reinvite(State) ->
    State.

do_on_pending_timeout(_Args, State) ->
    #state_ivr{reinvite=#reinvite{offer_opts=InviteOpts}=Re}=State,
    case send_reinvite_offer(InviteOpts, State) of
        error ->
            operation_response({error,<<"Error pending invite request">>}, State),
            {next_state, ?ACTIVESTATE, State};
        {ok,ReqHandle} ->
            State1 = State#state_ivr{reinvite=Re#reinvite{rhandle=ReqHandle,
                                                          pending_ref=undefined}},
            {next_state, ?CURRSTATE, State1}
    end.

%% =============================================================
%% Handling UAC responses
%% =============================================================

% ----
do_on_uac_timeout([CallId, _YLTag], State) ->
    operation_response({error,<<"Invite timeout">>}, State),
    StopReason = ?B2BUA_UTILS:reason_timeout([sessionchange, invite], CallId, <<"Reinvite timeout">>),
    go_error(StopReason, State#state_ivr{reinvite=undefined}).

% -----
do_on_uac_response([#sipmsg{cseq={_,'INVITE'}}]=P, State) ->
    on_invite_response(P, State);

do_on_uac_response([#sipmsg{cseq={_,Method}}=_Resp], State) ->
    d(State, "RE uac_response('~p') skipped", [Method]),
    {next_state, ?CURRSTATE, State}.

% -----
on_invite_response([#sipmsg{class={resp,SipCode,_}}=_Resp], State) when SipCode >= 100, SipCode < 200 ->
    {next_state, ?CURRSTATE, State};
on_invite_response([#sipmsg{class={resp,SipCode,_}}=_Resp]=P, State) when SipCode >= 200, SipCode < 300 ->
    on_invite_response_2xx(P,State);
on_invite_response([#sipmsg{class={resp,SipCode,_}}=_Resp]=P, State) when SipCode >= 300, SipCode < 400 ->
    on_invite_response_error(P, State);
on_invite_response([#sipmsg{class={resp,SipCode,_}}=_Resp]=P, State) when SipCode >= 400, SipCode < 700 ->
    on_invite_response_4xx_5xx_6xx(P,State);
on_invite_response([_Resp]=P, State) ->
    on_invite_response_error(P, State).

%% ===================================
%% Negative UAC response
%% ===================================

on_invite_response_error([#sipmsg{call_id=CallId}=_Resp], State) ->
    cancel_timeout_timer(State),
    operation_response({error,<<"Invalid response code">>}, State),
    StopReason = ?B2BUA_UTILS:reason_error([sessionchange, invite], CallId, <<"Reinvite unexpected response code">>),
    go_error(StopReason, State#state_ivr{reinvite=undefined}).

on_invite_response_4xx_5xx_6xx([#sipmsg{class={resp,SipCode,_SipReason}}=_Resp], State) ->
    cancel_timeout_timer(State),
    #state_ivr{reinvite=#reinvite{pending_ref=PRef}=Re}=State,
    case SipCode of
        491 when PRef==undefined ->
            TimerRef = erlang:send_after(300+?EU:random(400), self(), {reinvite_timeout, [<<"pending">>]}),
            {next_state, ?ACTIVESTATE, State#state_ivr{reinvite=Re#reinvite{pending_ref=TimerRef}}};
        491 ->
            {next_state, ?ACTIVESTATE, State};
        _ ->
            operation_response({error,{rejected,SipCode}}, State),
            {next_state, ?ACTIVESTATE, State#state_ivr{reinvite=undefined}}
    end.

%% ===================================
%% Positive UAC response
%% ===================================

on_invite_response_2xx([#sipmsg{class={resp,_SipCode,_}}=Resp], State) ->
    #state_ivr{a=#side{}=A, reinvite=#reinvite{lsdp=LSdp}}=State,
    cancel_timeout_timer(State),
    #sdp{}=RSdp=?U:extract_sdp(Resp),
    % media
    case ?IVR_MEDIA:media_sessionchanged({RSdp,LSdp}, State) of
        {error,_}=Err ->
            operation_response(Err,State),
            {next_state, ?ACTIVESTATE, State};
        {ok, State1} ->
            operation_response({ok,RSdp}, State1),
            {ok,DlgHandle} = nksip_dialog:get_handle(Resp),
            State2 = State1#state_ivr{a=A#side{dhandle=DlgHandle,
                                                remotesdp=RSdp},
                                     reinvite=undefined},
            {next_state, ?ACTIVESTATE, State2}
    end.

%% =============================================================
%% Services
%% =============================================================

operation_response(R,#state_ivr{reinvite=#reinvite{funresult=FunResult}}=_State) -> operation_response(R,FunResult);
operation_response(R,FunResult) when is_function(FunResult) -> FunResult(R);
operation_response(_,_) -> ok.

% ---
go_error(StopReason, #state_ivr{}=State) ->
    {ok,State1} = ?ACTIVE_UTILS:do_stop_bye(State#state_ivr{stopreason=StopReason}),
    ?IVR_UTILS:return_stopping(?IVR_UTILS:do_finalize_ivr(State1)).

% ---
cancel_timeout_timer(State) ->
    #state_ivr{reinvite=#reinvite{timer_ref=TimerRef}}=State,
    erlang:cancel_timer(TimerRef).

% -----
%d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    #state_ivr{ivrid=IvrId}=State,
    ?LOGSIP("IVR fsm ~p '~p':" ++ Fmt, [IvrId,?CURRSTATE] ++ Args).
