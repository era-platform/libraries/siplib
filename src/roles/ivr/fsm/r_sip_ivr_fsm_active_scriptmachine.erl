%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc ScriptMachine interaction utils of 'active' state of IVR FSM
%%% @todo

-module(r_sip_ivr_fsm_active_scriptmachine).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([start_script/1,
         stop_script/1,
         handle_event/2,
         handle_down/2,
         %
         uac_response/2,
         sip_notify/2,
         sip_info/2,
         sip_message/2,
         sip_reinvite_incoming/2,
         mg_dtmf/2,
         mg_event/4]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_ivr.hrl").
-include("../include/r_sip_mgc.hrl").
-include("../include/r_sip_headers.hrl").

% -include("../include/r_nksip.hrl").

-define(CURRSTATE, active).

%% ====================================================================
%% API functions
%% ====================================================================

%% --------------------------
%% starts script machine
%%
start_script(State) ->
    #state_ivr{cid=CID,ivrid=IvrId,invite_ts=IvrTS,map=Map}=State,
    [Domain,Script,Opts] = ?EU:maps_get([domain,script,opts],Map),
    %
    Meta = #{domain => Domain,
             scripts => fun(ScrCode) -> ?IVR_UTILS:get_script(Domain, ScrCode, 'ivr') end, % sm gets ext scripts
             script_opts => fun(ScrCode) -> case ?ENVDC:get_object_sticky(Domain, ivrscript, [{keys,[ScrCode]},{fields,[opts]}], auto) of
                                                {ok,[Item],_} -> {ok,maps:get(opts,Item)};
                                                _ -> false
                                            end end,
             components => ?IVR_METADATA:components(), % sm gets ext components
             functions => ?IVR_METADATA:expression_funs(State), % sm gets ext funs
             monitor => maps:get(trace_events,Map,#{}), % sm notifies ext monitor (#{key=>fun}) by known keys, ex. 'script_start', 'component_start', 'script_stop'
             fun_res => maps:get(fun_res,Map,undefined), % RP-1220 to send message to owner (as it is to notify sip call result)
             callback_funs => maps:get(callback_funs,Map,#{})}, % RP-1481
    %
    Self = self(),
    Ids = {CID,IvrId,<<>>,IvrTS,Domain},
    LogLevel = get_loglevel(Script),
    SMOpts = [{domain,Domain},
              {type,ivr},
              {script,Script},
              {is_generated,maps:get(is_generated,Script,false)},
              {code,maps:get(code,Script,undefined)},
              {meta,Meta},
              {ownerpid,self()},
              {use_owner,true},
              {initial_var_values,?EU:get_by_key(initial_var_values,Opts,[])}, % #320
              {loglevel, LogLevel},
              {fun_events, fun(Event) -> on_sm_events(Ids,Event,Self) end} % sm notifies owner (fun()) by known keys, ex.  'sm_start', 'sm_stop', 'script_start', 'script_continue', 'script_post_branch', 'api_start', 'api_stop', 'api_info'
             ],
    {ok, ScrId, Pid} = ?APP_SCRIPT:sm_start(SMOpts),
    MonRef = erlang:monitor(process,Pid),
    %
    #state_ivr{a=#side{callid=ACallId,
                       remotetag=RemoteTag,
                       localtag=LocalTag}}=State,
    Msg = {self(),#{callid => ACallId,
                    remotetag => RemoteTag,
                    localtag => LocalTag,
                    scrid => ScrId,
                    scrpid => Pid}},
    ?DIALING_UTILS:send_result(State, 'ivr_started', Msg),
    %
    State1 = State#state_ivr{map=maps:without([script], Map),
                             scriptmachine=#{pid => Pid,
                                             id => ScrId,
                                             monitorref => MonRef}},
    ?EVENT:ivr_start(State1),
    State1.

%%----------------------------------------------------
get_loglevel(#{opts := Opts}) when is_map(Opts) ->
    maps:get(<<"loglevel">>, Opts, 4);
get_loglevel(_) ->
    5.

%% --------------------------
%% stops script machine
%%
stop_script(#state_ivr{scriptmachine=undefined}=State) -> State;
stop_script(#state_ivr{scriptmachine=Props}=_State) ->
    Pid = maps:get(pid, Props),
    ?APP_SCRIPT:sm_stop(Pid, {stop_external,owner}).

%% --------------------------
%% handle script machine events
%%

handle_event({inited, #{pid:=Pid}=Props}, #state_ivr{scriptmachine=#{pid:=Pid}=P}=State) ->
    Props1 = Props#{monitorref => maps:get(monitorref,P)},
    {next_state, ?CURRSTATE, State#state_ivr{scriptmachine=Props1}};

handle_event({ivr_cmd, Ref, {ext, Module, Command, FromPid, OptsMap}}, State) ->
    % ?OUT("IVR got cmd ~p:~p", [Module, Command]),
    case erlang:function_exported(Module, Command, 2) of
        false -> {next_state, ?CURRSTATE, State};
        true ->
            F = fun(A,B) -> Module:Command(A,B) end,
            handle_event({ivr_cmd, Ref, {extf, F, FromPid, OptsMap}}, State)
    end;

handle_event({ivr_cmd, Ref, {extf, F, FromPid, OptsMap}}, State) when is_function(F,2) ->
    % ?OUT("IVR got cmd ~p:~p", [Module, Command]),
    case F({Ref, FromPid, OptsMap}, State) of
        ok -> {next_state, ?CURRSTATE, State};
        {next_state,#state_ivr{}=State1} -> {next_state, ?CURRSTATE, State1};
        {next_state,_,_}=R -> R;
        {next_state,_,_,_}=R -> R
    end;

handle_event({{sm_events, Event}, CurrentState}, State) ->
    do_sm_events(Event,State),
    {next_state, CurrentState, State};

handle_event({ext_event,Message}, #state_ivr{scriptmachine=Props}=State) ->
    Pid = maps:get(pid, Props),
    ?APP_SCRIPT:sm_event(Pid, {ext_event,Message}),
    {next_state, ?CURRSTATE, State};

handle_event(_Event, State) ->
    ?OUT("IVR got unknown event from scriptmachine: ~p", [_Event]),
    {next_state, ?CURRSTATE, State}.

%% --------------------------
%% handle script machine down
%%
handle_down(Result, #state_ivr{a=#side{state=S}=Side,map=#{mode:=<<"1">>}=Map}=State) ->
    #state_ivr{a=#side{callid=ACallId,
                       remotetag=RemoteTag,
                       localtag=LocalTag}}=State,
    %
    Msg = {self(),#{ivr_result => Result,
                    callid => ACallId,
                    remotetag => RemoteTag,
                    localtag => LocalTag}},
    ?DIALING_UTILS:send_result(State, 'ivr_stopped', Msg),
    %
    Opts = maps:get(opts,Map),
    case lists:keyfind(ownermode, 1, Opts) of
        {_,X}  when S=:='dialog' andalso (X=:=0 orelse X=:=1 orelse X=:=2) ->
            {ok,State1} = ?ACTIVE_UTILS:do_stop_bye(State),
            State2 = State1#state_ivr{a=Side#side{state=bye,
                                                  finaltime=os:timestamp()}},
            ?IVR_UTILS:return_stopping(?IVR_UTILS:do_finalize_ivr(State2));
        _ -> {next_state, ?CURRSTATE, State}
    end;
handle_down(_Result, #state_ivr{a=#side{state=S}=Side}=State) when S=='dialog' ->
    {ok,State1} = ?ACTIVE_UTILS:do_stop_bye(State),
    State2 = State1#state_ivr{a=Side#side{state=bye,
                                          finaltime=os:timestamp()}},
    ?IVR_UTILS:return_stopping(?IVR_UTILS:do_finalize_ivr(State2));
handle_down(_Result, #state_ivr{a=#side{state=S}=Side}=State) when S=='incoming'; S=='ringing'; S=='early' ->
    #side{rhandle=RHandle, responses=Resp}=Side,
    SipReply = ?InternalError(<<"IVR. Script machine down">>),
    State1 = ?IVR_UTILS:send_response(RHandle, SipReply, State),
    State2 = State1#state_ivr{a=Side#side{state='declined', responses=[SipReply|Resp]}},
    ?IVR_UTILS:return_stopping(?IVR_UTILS:do_finalize_ivr(State2));
handle_down(_Result, State) ->
    ?IVR_UTILS:return_stopping(?IVR_UTILS:do_finalize_ivr(State)).

%% --------------------------
%% when uac got response
%%
uac_response(_, #state_ivr{scriptmachine=undefined}=State) -> {ok, State};
uac_response(Resp, #state_ivr{scriptmachine=Props}=State) ->
    Pid = maps:get(pid, Props),
    ?APP_SCRIPT:sm_event(Pid, {uac_response, Resp}),
    {ok, State}.

%% --------------------------
%% when notify received
%%
sip_notify(_, #state_ivr{scriptmachine=undefined}=State) -> {next_state, ?CURRSTATE, State};
sip_notify([_CallId, Req], #state_ivr{scriptmachine=Props}=State) ->
    Pid = maps:get(pid, Props),
    ?APP_SCRIPT:sm_event(Pid, {sip_notify, Req}),
    {next_state, ?CURRSTATE, State}.

%% --------------------------
%% when info received
%%
sip_info(_, #state_ivr{scriptmachine=undefined}=State) -> {next_state, ?CURRSTATE, State};
sip_info([_CallId, Req], #state_ivr{scriptmachine=Props}=State) ->
    Pid = maps:get(pid, Props),
    ?APP_SCRIPT:sm_event(Pid, {sip_info, Req}),
    {next_state, ?CURRSTATE, State}.

%% --------------------------
%% when message received
%%
sip_message(_, #state_ivr{scriptmachine=undefined}=State) -> {next_state, ?CURRSTATE, State};
sip_message([_CallId, Req], #state_ivr{scriptmachine=Props}=State) ->
    Pid = maps:get(pid, Props),
    ?APP_SCRIPT:sm_event(Pid, {sip_message, Req}),
    {next_state, ?CURRSTATE, State}.

%% --------------------------
%% when reinvite received and answered
%%
sip_reinvite_incoming(_, #state_ivr{scriptmachine=undefined}=State) -> {next_state, ?CURRSTATE, State};
sip_reinvite_incoming(P, #state_ivr{scriptmachine=Props}=State) ->
    Pid = maps:get(pid, Props),
    ?APP_SCRIPT:sm_event(Pid, {sip_reinvite_incoming, P}),
    {next_state, ?CURRSTATE, State}.

%% --------------------------
%% when dtmf from mg received
%%
mg_dtmf(_, #state_ivr{scriptmachine=undefined}=State) -> {next_state, ?CURRSTATE, State};
mg_dtmf(#{}=Event, #state_ivr{scriptmachine=Props}=State) ->
    Pid = maps:get(pid, Props),
    ?APP_SCRIPT:sm_event(Pid, {mg_dtmf, maps:get(dtmf,Event)}),
    {next_state, ?CURRSTATE, State}.

%% --------------------------
%% when some event from mg received
%%   ex. endplay
%%
mg_event(_, _, _, #state_ivr{scriptmachine=undefined}=State) -> {next_state, ?CURRSTATE, State};
% end player
mg_event("endplay", #{}=Event, CtxTermId, #state_ivr{scriptmachine=Props}=State) ->
    Pid = maps:get(pid, Props),
    case ?IVR_MEDIA:media_find_playerid(CtxTermId, State) of
        {ok,PlayerId} -> ?APP_SCRIPT:sm_event(Pid, {play_stopped, PlayerId, Event});
        _ -> ok
    end,
    {next_state, ?CURRSTATE, State};
% end fax
mg_event("endfax", #{}=Event, CtxTermId, #state_ivr{scriptmachine=Props}=State) ->
    Pid = maps:get(pid, Props),
    case ?IVR_MEDIA:media_check_faxid(CtxTermId, State) of
        ok -> ?APP_SCRIPT:sm_event(Pid, {fax_stopped, Event});
        _ -> ok
    end,
    {next_state, ?CURRSTATE, State};
% vad_threshold
mg_event("vad_threshold", #{}=Event, CtxTermId, #state_ivr{scriptmachine=Props}=State) ->
    Pid = maps:get(pid, Props),
    ?APP_SCRIPT:sm_event(Pid, {vad_threshold, CtxTermId, Event}),
    {next_state, ?CURRSTATE, State};
% sent rfc2833
mg_event("rfc2833/sent", #{}=Event, CtxTermId, #state_ivr{scriptmachine=Props}=State) ->
    Pid = maps:get(pid, Props),
    ?APP_SCRIPT:sm_event(Pid, {rfc2833_sent, CtxTermId, Event}),
    {next_state, ?CURRSTATE, State};
% other
mg_event(EType, #{}=Event, _CtxTermId, #state_ivr{scriptmachine=Props}=State) ->
    Pid = maps:get(pid, Props),
    ?APP_SCRIPT:sm_event(Pid, {mg_event, EType, Event}),
    {next_state, ?CURRSTATE, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% handle sm events
on_sm_events(Ids, Event, FsmPid) ->
    %?OUT("DEBUG. on_sm_event ~120tp", [Event]),
    case process_info(FsmPid,status) of
        undefined -> do_sm_events(Event, Ids);
        _ -> FsmPid ! {scriptmachine, {sm_events, Event}}
    end,
    ok.

%% handle sm events in fsm process
do_sm_events(Event, IdOrState) ->
    %?OUT("DEBUG. do_sm_event ~120tp", [Event]),
    case Event of
        {sm_start,_MapOpts} -> ok;
        {script_start,MapOpts} -> ?EVENT:ivr_script_start(IdOrState,MapOpts#{mode => <<"start">>});
        {script_continue,MapOpts} -> ?EVENT:ivr_script_start(IdOrState,MapOpts#{mode => <<"continue">>});
        {script_post_branch,MapOpts} -> ?EVENT:ivr_script_start(IdOrState,MapOpts#{mode => <<"post">>});
        {api_management_start,MapOpts} -> ?EVENT:ivr_api_start(IdOrState,MapOpts);
        {api_management_stop,MapOpts} -> ?EVENT:ivr_api_stop(IdOrState,MapOpts);
        {api_management_info,MapOpts} -> ?EVENT:ivr_api_info(IdOrState,MapOpts);
        {sm_stop,_MapOpts} -> ok
    end.