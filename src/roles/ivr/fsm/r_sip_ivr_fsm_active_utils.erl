%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Service utils of 'active' state of IVR FSM
%%% @todo

-module(r_sip_ivr_fsm_active_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_ivr.hrl").
-include("../include/r_sip_mgc.hrl").

% -include("../include/r_nksip.hrl").

-define(CURRSTATE, 'active').

%% ====================================================================
%% API functions
%% ====================================================================


%% ---------------------------------
%% prepare side rec by opts for new participant
%% ---------------------------------
-spec prepare_side(Opts::list(), State::#state_ivr{}) -> {XSide::#side{}, NewState::#state_ivr{}}.

prepare_side(Req, State) ->
    Now = os:timestamp(),
    % -------
    #sipmsg{call_id=XCallId,
            from={XRUri,XRTag}}=Req,
    {ok, XReqHandle} = nksip_request:get_handle(Req),
    #sdp{}=XRSdp=?U:extract_sdp(Req),
    %
    #sipmsg{to={#uri{ext_opts=XLExtOpts}=XLUri,_},
            contacts=XRContacts,
            to_tag_candidate=ToTagCandidate}=Req,
    % side local uri
    XLTag1 = ToTagCandidate,
    State1 = case State of
                 #state_ivr{usedtags=[]} ->
                     State#state_ivr{usedtags=[XRTag, XLTag1]};
                 _ -> State
             end,
    XLUri1 = XLUri#uri{ext_opts = ?U:store_value(<<"tag">>, XLTag1, XLExtOpts)},
    % side local contact
    XLContact = ?U:build_local_contact(XLUri), % @localcontact (cfg=default | incoming: top via's domain | outgoing: uri's route)
    % side rec
    XSide = #side{callid=XCallId,
                  rhandle=XReqHandle,
                  dhandle=undefined,
                  dir='in',
                  state=incoming,
                  req=Req,
                  %
                  remoteuri=XRUri,
                  remotetag=XRTag,
                  remotecontacts=XRContacts,
                  remotesdp=XRSdp,
                  %
                  localuri=XLUri1,
                  localtag=XLTag1,
                  localcontact=XLContact,
                  %
                  starttime=Now
                 },
    State2 = State1#state_ivr{a=XSide},
    {XSide,State2}.

%% ---------------------------------
%% make side rec by fork of new participant
%% ---------------------------------
-spec make_side_by_fork(Fork::#side_fork{}, Resp::#sipmsg{}) -> Side::#side{}.

make_side_by_fork(#side_fork{}=Fork, #sipmsg{class={resp,_,_}}=Resp) ->
    #side_fork{}=Fork,
    {ok,DlgHandle} = nksip_dialog:get_handle(Resp),
    #sipmsg{contacts=RContacts}=Resp,
    #sdp{}=RSdp=?U:extract_sdp(Resp),
    Now = os:timestamp(),
    XSide = #side{callid=Fork#side_fork.callid,
                  rhandle=Fork#side_fork.rhandle,
                  dhandle=DlgHandle,
                  dir='out',
                  req=Resp,
                  %
                  remoteuri=Fork#side_fork.remoteuri,
                  remotetag=Fork#side_fork.remotetag,
                  remotecontacts=RContacts,
                  remotesdp=RSdp,
                  %
                  localuri=Fork#side_fork.localuri,
                  localtag=Fork#side_fork.localtag,
                  localcontact=Fork#side_fork.localcontact,
                  %
                  starttime=Fork#side_fork.starttime,
                  answertime=Now
                 },
    XSide.

%% ---------------------------------
%% update fork_side rec of new participant by 1xx response with sdp
%% ---------------------------------
-spec update_side_fork(Fork::#side_fork{}, Resp::#sipmsg{}) -> UpFork::#side_fork{}.

update_side_fork(#side_fork{}=Fork, #sipmsg{class={resp,SipCode,_}}=Resp) ->
    #side_fork{}=Fork,
    {ok,DlgHandle} = nksip_dialog:get_handle(Resp),
    #sipmsg{contacts=RContacts}=Resp,
    RSdp = ?U:extract_sdp(Resp),
    Fork#side_fork{dhandle=DlgHandle,
                   last_response_code=SipCode,
                   remotecontacts=RContacts,
                   remotesdp=RSdp}.

%% ---------------------------------
%% send response to participant's invite
%% ---------------------------------
-spec send_sdp_to_inviter(SipCode::integer(), LSdp::#sdp{}, Side::#side{}, Opts::list(), State::#state_ivr{}) -> State1::#state_ivr{}.

send_sdp_to_inviter(SipCode, LSdp, Side, Opts, State) ->
    #side{rhandle=ReqHandle}=Side,
    #state_ivr{a=Side}=State,
    Opts1 = lists:keydelete(body, 1, lists:delete(user_agent, Opts)),
    %
    ROpts = [{body, LSdp},
             user_agent
            |Opts1],
    State1 = ?IVR_UTILS:send_response(ReqHandle, {SipCode, ROpts}, State),
    %
    State1#state_ivr{a=Side#side{answertime=os:timestamp()}}.

%% ---------------------------------
%% detach existing participant
%% ---------------------------------

% send bye and detach current participant
-spec do_stop_bye(State::#state_ivr{}) -> {ok, State::#state_ivr{}}.

do_stop_bye(State) ->
    #state_ivr{a=Side}=State,
    ?IVR_UTILS:send_bye(Side, State),
    do_stop(State).

% send bye and detach selected participant
-spec do_stop_bye(Side::#side{}, State::#state_ivr{}) -> {ok, State::#state_ivr{}}.

do_stop_bye(#side{callid=CallId}=Side, State) ->
    ?IVR_UTILS:send_bye(Side, State),
    ?DLG_STORE:unlink_dlg(CallId),
    {ok,State}.

% detach participant from conference (last should destroy conference)
-spec do_stop(State::#state_ivr{}) -> {ok, State::#state_ivr{}}.

do_stop(State) ->
    #state_ivr{a=#side{callid=CallId}=_Side}=State,
    % from store
    ?DLG_STORE:unlink_dlg(CallId),
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

% @private ----
d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    #state_ivr{ivrid=IvrId}=State,
    ?LOGSIP("IVR fsm ~p '~p':" ++ Fmt, [IvrId,?CURRSTATE] ++ Args).
