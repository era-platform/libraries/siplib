%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>, Peter Bukashin <tbotc@yandex.ru>
%%% @date 26.09.2016
%%% @doc
%%% @todo

-module(r_sip_ivr_fsm_dialing).
-author(['Evgeniy Grebenyuk <llceceron@gmail.com>', 'Peter Bukashin <tbotc@yandex.ru>']).

%% ====================================================================
%% Exports
%% ====================================================================

-export([start/1,
         cancel_by_mode/2,
         stop_external/2]).

-export([uac_response/2,
         sip_cancel/2,
         sip_bye/2,
         sip_reinvite/2,
         call_terminate/2]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_ivr.hrl").
-include("../include/r_sip_mgc.hrl").

-define(CURRSTATE, dialing).

%% ====================================================================
%% Types
%% ====================================================================

%% ====================================================================
%% API functions
%% ====================================================================

%% ====================================================================
%% Public functions
%% ====================================================================

start(#state_ivr{map=Map}=State) ->
    case lists:keyfind(<<"number">>, 1, maps:get(opts,Map)) of
        {_,To} ->
            ?DIALING_CALL:start_outgoing_call([To],State)
    end.

%% =========================

% ----
cancel_by_mode({<<"1">> =_Mode,_Reason}=_Args,StateData) ->
    #state_ivr{forks=Forks}=StateData,
    lists:foreach(fun({_,#{}=ForkCall}) -> ?DIALING_UTILS:cancel_active_forks([maps:get(fork,ForkCall)], [], StateData) end, Forks),
    NewStateData = StateData#state_ivr{forks=[]},
    {next_state, ?CURRSTATE, NewStateData}.

%% =========================

% ----
stop_external(Reason, State) ->
    d(State, "stop_external(~p)", [Reason]),
    #state_ivr{a=Side,forks=Forks}=State,
    lists:foreach(fun({_,#{}=ForkCall}) -> ?DIALING_UTILS:cancel_active_forks([maps:get(fork,ForkCall)], [], State) end, Forks),
    case Side of
        #side{} -> ?IVR_UTILS:send_bye(Side, State);
        _ -> ok
    end,
    StopReason = ?IVR_UTILS:reason_external(Reason),
    ?IVR_UTILS:return_stopping(?IVR_UTILS:do_finalize_ivr(State#state_ivr{stopreason=StopReason})).

%% =========================

% ----
uac_response([_Resp]=Args, State) ->
    #sipmsg{class={resp,SipCode,SipReason},
            cseq={CSeq,Method},
            call_id=CallId}=_Resp,
    d(State, "uac_response ~p:~p -> ~p ~p, CallId=~120p", [Method,CSeq,SipCode,SipReason,CallId]),
    do_on_uac_response(Args, State).

%% =========================
% ----
sip_cancel([CallId, _Req, _InviteReq], State) ->
    d(State, "sip_cancel, CallId=~120p", [CallId]),
    %
    #state_ivr{a=Side}=State,
    do_stop(CallId, State#state_ivr{a=Side#side{state=cancelled}}).

% ----
sip_bye([CallId, Req], State) ->
    d(State, "sip_bye, CallId=~120p", [CallId]),
    State1 = ?IVR_UTILS:send_response(Req, ok, State),
    %
    #state_ivr{a=Side}=State,
    do_stop(CallId, State1#state_ivr{a=Side#side{state=bye}}).

%% =========================
% ----
sip_reinvite([CallId, _Req]=P, State) ->
    d(State, "sip_reinvite, CallId=~120p", [CallId]),
    do_reinvite_incoming(P, State).

%% =========================
% ----
call_terminate([Reason, Call], State) ->
    #call{call_id=CallId}=Call,
    d(State, "call_terminate, CallId=~120p, Reason=~120p", [CallId, Reason]),
    do_stop(CallId, State).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -----
%% UAC response handlers
%%
do_on_uac_response([#sipmsg{cseq={_,'INVITE'}, call_id=CallId}=Resp]=P, State) ->
    #sipmsg{class={resp,SipCode,_}}=Resp,
    #state_ivr{forks=Forks}=State,
    d(State, "uac_response('INVITE') ~p from Z: ~p", [SipCode, CallId]),
    case lists:keyfind(CallId,1,Forks) of
        false ->
            {next_state, ?CURRSTATE, State};
        {_,#side_fork{}=_Side} ->
            {next_state, ?CURRSTATE, State};
%%         {_,#{type:='refer'}=Refer} ->
%%             ?ACTIVE_REFER:uac_response(P, Refer, State);
        {_,#{type:='call'}=ForkCall} ->
            ?DIALING_CALL:uac_response(P, ForkCall, State) %%@@todo check
    end;
%
do_on_uac_response([#sipmsg{cseq={_,'NOTIFY'}}=Resp], State) ->
    #sipmsg{class={resp,SipCode,_}, call_id=CallId}=Resp,
    d(State, "uac_response('NOTIFY') ~p from ~p", [SipCode, CallId]),
    {next_state, ?CURRSTATE, State};
%
do_on_uac_response([#sipmsg{cseq={_,Method}}=_Resp], State) ->
    d(State, "uac_response(~p) skipped", [Method]),
    {next_state, ?CURRSTATE, State}.

% -------
% stops ivr session
do_stop(_CallId, State) ->
    ?IVR_UTILS:return_stopping(?IVR_UTILS:do_finalize_ivr(State)).

% --------
% modifies participant's media by incoming reinvite
do_reinvite_incoming([CallId, Req]=_P, State) ->
    #state_ivr{a=#side{callid=CallId}=Side}=State,
    #sdp{}=RSdp=?U:extract_sdp(Req),
    case ?IVR_MEDIA:media_sessionchange(Req, Side, State) of
        {error, SipReply} ->
            State1 = ?IVR_UTILS:send_response(Req, SipReply, State),
            {next_state, ?CURRSTATE, State1};
        {ok, State1, LSdp} ->
            Opts = [{body, LSdp},
                    user_agent],
            State2 = ?IVR_UTILS:send_response(Req, {200,Opts}, State1),
            State3 = State2#state_ivr{a=Side#side{remotesdp=RSdp}},
            {next_state, ?CURRSTATE, State3}
    end.


%% =====================================================================

% @private ----
%% d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    #state_ivr{ivrid=IvrId}=State,
    ?LOGSIP("IVR fsm ~p '~p':" ++ Fmt, [IvrId,?CURRSTATE] ++ Args).
