%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>, Peter Bukashin <tbotc@yandex.ru>
%%% @date 26.09.2016
%%% @doc
%%% @todo


-module(r_sip_ivr_fsm_dialing_call).
-author(['Evgeniy Grebenyuk <llceceron@gmail.com>', 'Peter Bukashin <tbotc@yandex.ru>']).

%% ====================================================================
%% Exports
%% ====================================================================

-export([start_outgoing_call/2,
         uac_response/3]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_headers.hrl").
-include("../include/r_sip_ivr.hrl").
-include("../include/r_sip_mgc.hrl").

-define(CURRSTATE, dialing).

%% ====================================================================
%% Types
%% ====================================================================

%% ====================================================================
%% API functions
%% ====================================================================

start_outgoing_call([To], State) ->
    d(State, "Start outgoing call ~p", [To]),
    ZCallId = ?IVR_UTILS:build_out_callid(State),
    ?DIALING_UTILS:send_result(State, 'call_inited', {self(),#{node=>node(),callid=>ZCallId}}),
    FRes = fun({error,_}=Result, ResState) -> ?DLG_STORE:unlink_dlg(ZCallId), {Result,ResState}; % #304, #343
              (Result, ResState) -> {Result,ResState} end,% #304 return result
    do_start_call([To], ZCallId, State, FRes).

uac_response(P, Refer, State) ->
    do_on_uac_response(P, Refer, State).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ----
%% find b2bua
do_start_call(P, ZCallId, State, FRes) ->
    case ?SERVERS:find_responsible_b2bua_nostore(ZCallId) of
        false ->
            FRes({error, {internal_error, <<"B2BUA server not found">>}}, State);
        RouteDomain ->
            do_start_call_1(P, [ZCallId, RouteDomain], State, FRes)
    end.

%% ----
%% create media context
do_start_call_1(P, [ZCallId, RouteDomain], State, FRes) ->
    #state_ivr{a=Side}=State,
    case ?IVR_MEDIA:check_available(State) of
        {error,mgc_overload} ->
            ?IVR_UTILS:error_mgc_final("MGC overload", State, Side, ?InternalError("IVR. MGC overload"));
        {error,_} ->
            ?IVR_UTILS:error_mgc_final("MGC not ready", State, Side, ?InternalError("IVR. MGC not ready"));
        {ok,false} ->
            ?IVR_UTILS:error_mgc_final("No media trunk available", State, Side, ?InternalError("IVR. No media trunk available"));
        {ok,true} ->
            case ?IVR_MEDIA:media_prepare_start(State) of
                {error, Reply} ->
                    ?IVR_UTILS:error_mgc_final("", State, Side, Reply);
                {ok,State1} ->
                    case ?IVR_MEDIA:media_add_outgoing(ZCallId, State1) of
                        {error, _}=Err -> FRes(Err, State1);
                        {ok, State2, ZLSdp} ->
                            do_start_call_2(P, [ZCallId, RouteDomain, ZLSdp], State2, FRes)
                    end end end.

%% ----
%% make invite options (inside cluster call)
do_start_call_2([To], [ZCallId, RouteDomain, ZLSdp], State, FRes) ->
    #state_ivr{ivrid=IvrId,map=MapArgs}=State,
    FrD = maps:get(domain,MapArgs),
    SrvIdxT = ?U:get_current_srv_textcode(),
    %
    ?DLG_STORE:push_dlg(ZCallId, {IvrId,self()}),
    ?MONITOR:append_fun(self(), fun() -> ?DLG_STORE:unlink_dlg(ZCallId) end),
    %
    Opts = maps:get(opts,MapArgs),
    CallerId = ?EU:get_by_key(callerid,Opts),
    CallerName = ?EU:get_by_key(callername,Opts,<<>>),
    ExtHeaders = ?EU:get_by_key(headers,Opts,[]),
    HeadersToAdd = [{add,{HK,HV}} || {HK,HV} <- ExtHeaders, is_binary(HK) andalso is_binary(HV)],
    %
    ZRToUri = case To of
                  {ToS,ToU,ToD} when is_atom(ToS), is_binary(ToU), is_binary(ToD) -> #uri{scheme=ToS, user=ToU, domain=ToD};
                  {ToU,ToD} when is_binary(ToU), is_binary(ToD) -> #uri{scheme=sip, user=ToU, domain=ToD};
                  ToU when is_binary(ToU) ->
                      case catch ?U:parse_uris(ToU) of
                          [Uri|_] -> Uri; % RP-1297
                          _ -> #uri{scheme=sip, user=ToU, domain=FrD}
                      end;
                  #uri{} -> To
              end,
    ZLUri = #uri{scheme=sip, disp=?U:quote_display(CallerName), user=CallerId, domain=FrD},
    {ZLTag,State1} = ?IVR_UTILS:generate_tag(State),
    ZLFromUri = ZLUri#uri{ext_opts=[{<<"tag">>,ZLTag}]},
    ZLContact = ?U:build_local_contact(ZLUri), % @localcontact (cfg=default | RouteDomain)
    %
    ?STAT_FACADE:link_fork(ZCallId), % @stattrace
    ?STAT_FACADE:link_domain(ZCallId, FrD), % RP-415
    ?IVR_UTILS:log_callid(outgoing,IvrId,ZCallId,FrD), % #354, RP-415
    Timeout = 60000,
    %
    Route = {route, ?U:unparse_uri(#uri{scheme=sip, domain=RouteDomain, opts=?CFG:get_transport_opts()++[<<"lr">>]})},
    CmnOpts = [{call_id,ZCallId},
               {from,ZLFromUri},
               {to,ZRToUri},
               {contact,ZLContact},
               Route,
               user_agent],
    InviteOpts = [{body,ZLSdp},
                  auto_2xx_ack,
                  %record_route, % #244
                  {cseq_num,1},
                  {add, {?OwnerHeader, <<"rIV-", SrvIdxT/bitstring>>}},
                  {add, {?CallerTypeHeader, <<"ivr">>}}] ++ HeadersToAdd,
    % #350
    InviteOpts1 = case ?U:is_b2bmedia() of
                      false ->  [{replace, {?B2BHeader, <<"media=0">>}} | InviteOpts];
                      true -> InviteOpts
                  end,
    %
    ZFork = #side_fork{callid=ZCallId,
                       %
                       localuri=ZLFromUri,localtag=ZLTag,localcontact=ZLContact,
                       remoteuri=ZRToUri,
                       requesturi=ZRToUri,
                       rule_timeout=Timeout,
                       cmnopts=CmnOpts,
                       inviteopts=InviteOpts1,
                       starttime=os:timestamp()},
    %

    do_start_call_invite(ZFork, State1, FRes).

%% =================================================
%% Invite services
%% =================================================

%% -----
%% send invite and initialize state
%%
do_start_call_invite(Fork, State, FRes) ->
%%     ?EVENT:fork_start(Fork, State),
    #state_ivr{forks=Forks}=State,
    #side_fork{callid=ZCallId}=Fork,
    case send_invite(Fork, State) of
        {error,Err2}=Err1 ->
            {ok, State1} = ?IVR_MEDIA:media_detach(ZCallId, State),
            Err = case Err2 of {error,_} -> Err2; _ -> Err1 end,
            FRes(Err, State1);
        {ok, Fork1} ->
            ForkCall = #{type => 'call',
                         fork => Fork1,
                         zcallid => ZCallId,
                         startgs => ?EU:current_gregsecond()},
            State1 = State#state_ivr{forks=[{ZCallId,ForkCall}|Forks]},
            FRes(ok, State1)
    end.

%% ----
%% sends invite to Z
%%
send_invite(Fork, State) ->
    #state_ivr{map=#{app:=App}}=State,
    #side_fork{callid=ZCallId,
               requesturi=Uri,
               localtag=ZLTag,
               rule_timeout=Timeout,
               cmnopts=CmnOpts,
               inviteopts=InviteOpts}=Fork,
    %
    Now = os:timestamp(),
    %
    case catch nksip_uac:invite(App, Uri, [async|CmnOpts++InviteOpts]) of
        {'EXIT',Err} ->
            d(State, "invite_fork BLTag=~p, Caught error=~120p", [ZLTag, Err]),
            {error, Err};
        {error,_R}=Err ->
            d(State, "invite_fork BLTag=~p, Error=~120p", [ZLTag, Err]),
            {error, Err};
        {async,ReqHandle} ->
            d(State, "invite_fork BLTag=~p", [ZLTag]),
            % @todo account early side b
            Fork1 = Fork#side_fork{rhandle=ReqHandle,
                                   starttime=Now,
                                   rule_timer_ref=erlang:send_after(Timeout, self(), {?FORK_TIMEOUT_MSG, [ZCallId, ZLTag]})},
            {ok, Fork1}
    end.

%% =================================================

%% -------------------------------------
%% UAC response (only invite filtered)
%% -------------------------------------
do_on_uac_response([#sipmsg{cseq={_,'INVITE'}, call_id=CallId}=Req]=_P, ForkCall, State) ->
    fork_invite_response([CallId, Req], ForkCall, State).

%% -------------------------------------
%% @private Fork invite response
%% -------------------------------------
fork_invite_response([_CallId, #sipmsg{class={resp,SipCode,_}}=_Resp]=P, ForkCall, State) when SipCode >= 100, SipCode < 200 ->
    fork_invite_response_1xx(P,ForkCall,State);

fork_invite_response([_CallId, #sipmsg{class={resp,SipCode,_}}=_Resp]=P, ForkCall, State) when SipCode >= 200, SipCode < 300 ->
    fork_invite_response_2xx(P,ForkCall,State);

fork_invite_response([CallId, #sipmsg{class={resp,SipCode,_}}=Resp], ForkCall, State) when SipCode >= 300, SipCode < 400 ->
    fork_error(CallId, ForkCall, Resp, State);

fork_invite_response([CallId, #sipmsg{class={resp,SipCode,_}}=Resp], ForkCall, State) when SipCode >= 400, SipCode < 700 ->
    fork_error(CallId, ForkCall, Resp, State);

fork_invite_response([CallId, #sipmsg{class={resp,_SipCode,_}}=Resp], ForkCall, State) ->
    fork_error(CallId, ForkCall, Resp, State).

%% -------------------------------------
%% @private Fork invite response 1xx
%% -------------------------------------
fork_invite_response_1xx([CallId,#sipmsg{class={resp,SipCode,_SipReason}}=Resp],ForkCall,State) ->
    #state_ivr{map=Map0,forks=Forks}=State,
    Fork = maps:get(fork,ForkCall),
    Fork1 = ?DIALING_UTILS:update_side_fork(Fork,Resp),
    ForkCall1 = ForkCall#{fork:=Fork1},
    State1 = State#state_ivr{forks=lists:keyreplace(CallId, 1, Forks, {CallId,ForkCall1})},
    case SipCode/=100 of
        true -> ok;%%?EVENT:fork_preanswer(Fork1, {resp,SipCode,SipReason}, State1);
        false -> ok
    end,
    %% demonitor by ownermode
    ?DIALING_UTILS:demonitor_owner(Map0,1),
    ?DIALING_UTILS:send_result(State1, 'pre_res', {self(),#{sipcode => SipCode}}),
    case ?U:extract_sdp(Resp) of
        #sdp{}=RSdp -> fork_invite_response_1xx_connect([CallId,Resp,RSdp],ForkCall1,State1);
        _ -> {next_state, ?CURRSTATE, State1}
    end.

% @private
fork_invite_response_1xx_connect([CallId,#sipmsg{}=Resp,RSdp],ForkCall,State) ->
    #state_ivr{forks=Forks}=State,
    #side_fork{rhandle=ReqHandle}=Fork=maps:get(fork,ForkCall),
    case ?IVR_MEDIA:media_update_outgoing_by_remote_ext(Fork, RSdp, State) of
        {error,_E} ->
            State1 = ?IVR_UTILS:send_response(ReqHandle, ?InternalError("IVR. Attach media error"), State),
            State2 = State1#state_ivr{forks=lists:keydelete(CallId,1,Forks)},
            {next_state, ?CURRSTATE, State2};
        {ok, State1} ->
            Fork1 = ?DIALING_UTILS:update_side_fork(Fork,Resp),
            State2 = State1#state_ivr{forks=lists:keyreplace(CallId, 1, Forks, {CallId,ForkCall#{fork:=Fork1}})},
            {next_state, ?CURRSTATE, State2}
    end.

%% -------------------------------------
%% @private Fork invite response 2xx
%% -------------------------------------
fork_invite_response_2xx([CallId,#sipmsg{class={resp,SipCode,_},to={_,ToTag}}=Resp],ForkCall,State) ->
    #state_ivr{map=Map0,forks=Forks}=State,
    Fork = maps:get(fork,ForkCall),
%%     ?EVENT:fork_answer(Fork, State),
    Side = ?DIALING_UTILS:make_side_by_fork(Fork,Resp),
    State1 = State#state_ivr{forks=lists:keydelete(CallId, 1, Forks),
                             a=Side#side{state=dialog,remotetag=ToTag}},
    #sdp{}=RSdp=?U:extract_sdp(Resp),
    case ?IVR_MEDIA:media_update_outgoing_by_remote_ext(Fork, RSdp, State1) of
        {error,_E} ->
            {ok,State2} = ?DIALING_UTILS:do_stop_bye(Side, State1),
            ?IVR_UTILS:return_stopping(?IVR_UTILS:do_finalize_ivr(State2));
        {ok, State2} ->
%%             ?EVENT:call_attach(Side, State2),
            %% demonitor by ownermode
            ?DIALING_UTILS:demonitor_owner(Map0,2),
            ?DIALING_UTILS:send_result(State2, 'success_res', {self(),#{sipcode => SipCode}}),
            State3 = ?DIALING_UTILS:prepare_to_start_active(CallId,State2),
            #state_ivr{map=Map}=State3,
            {next_state, ?ACTIVE_STATE, State3#state_ivr{map=maps:without([script],Map)}}
    end.

%% -------------------------------------
%% @private Delete fork on error
%% -------------------------------------
fork_error(CallId, ForkCall, #sipmsg{class={resp,SipCode,Descr}}=_Resp, State) ->
    {ok, State1} = ?IVR_MEDIA:media_detach(CallId, State),
    #state_ivr{forks=Forks}=State1,
    State2 = case lists:keytake(CallId, 1, Forks) of
                 false -> State1;
                 {value,_,Forks1} ->
                     Fork = maps:get(fork,ForkCall),
%%                      ?EVENT:fork_stop(Fork#side_fork{res=ForkResult,
%%                                                      finaltime=os:timestamp()}, State1),
                     % remove fork
                     #side_fork{rule_timer_ref=TimerRef}=Fork,
                     erlang:cancel_timer(TimerRef),
                     ?DLG_STORE:unlink_dlg(CallId), % #304
                     State1#state_ivr{forks=Forks1}
             end,
    Msg = {self(),#{sipcode => SipCode,
                    sipdescr => Descr,
                    sipheaders => <<"ok">>}},
    ?DIALING_UTILS:send_result(State2, 'unsuccess_res', Msg),
    ?IVR_UTILS:return_stopping(?IVR_UTILS:do_finalize_ivr(State2)).


% @private ----
%% d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    #state_ivr{ivrid=IvrId}=State,
    ?LOGSIP("IVR fsm ~p '~p':" ++ Fmt, [IvrId,?CURRSTATE] ++ Args).
