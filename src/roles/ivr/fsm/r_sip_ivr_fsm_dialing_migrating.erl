%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>, Peter Bukashin <tbotc@yandex.ru>
%%% @date 27.09.2016
%%% @doc
%%% @todo


-module(r_sip_ivr_fsm_dialing_migrating).
-author(['Evgeniy Grebenyuk <llceceron@gmail.com>', 'Peter Bukashin <tbotc@yandex.ru>']).

%% ====================================================================
%% Exports
%% ====================================================================
-export([migrate/1]).



%% ====================================================================
%% Defines
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_ivr.hrl").
-include("../include/r_sip_mgc.hrl").

% -include("../include/r_nksip.hrl").

-define(CURRSTATE, dialing).

%% ====================================================================
%% Types
%% ====================================================================

%% ====================================================================
%% API functions
%% ====================================================================

migrate(State) ->
    d(State, "migrate"),
    do_migrate(State).

%% ====================================================================
%% Public functions
%% ====================================================================

%% ====================================================================
%% Internal functions
%% ====================================================================

do_migrate(State) ->
    % @TODO
    %  outgoing reinvite to all participants,
    %  cancel forks, new invite to forks,
    %  back to active,
    %  wait for answers/timeouts
    %    491 -> to resend reinvite,
    %    4xx,5xx,6xx - participant detach
    StopReason = ?IVR_UTILS:reason_error([migrate], <<>>, <<"Migration not implemented">>),
    ?DIALING:stop_external(StopReason, State).

% @private ----
d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    #state_ivr{ivrid=IvrId}=State,
    ?LOGSIP("IVR fsm ~p '~p':" ++ Fmt, [IvrId,?CURRSTATE] ++ Args).


