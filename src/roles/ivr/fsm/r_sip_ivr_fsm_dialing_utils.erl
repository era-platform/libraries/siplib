%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>, Peter Bukashin <tbotc@yandex.ru>
%%% @date 27.09.2016
%%% @doc
%%% @todo


-module(r_sip_ivr_fsm_dialing_utils).
-author(['Evgeniy Grebenyuk <llceceron@gmail.com>', 'Peter Bukashin <tbotc@yandex.ru>']).

%% ====================================================================
%% Exports
%% ====================================================================

-export([update_side_fork/2,
         make_side_by_fork/2,
         cancel_active_forks/3,
         do_stop_bye/1,
         do_stop_bye/2,
         do_stop/1,
         send_result/3,
         demonitor_owner/2,
         prepare_to_start_active/2]).


%% ====================================================================
%% Defines
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_ivr.hrl").
-include("../include/r_sip_mgc.hrl").

% -include("../include/r_nksip.hrl").

-define(CURRSTATE, 'active').

%% ====================================================================
%% Types
%% ====================================================================

%% ====================================================================
%% API functions
%% ====================================================================

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---------------------------------
%% make side rec by fork of new participant
%% ---------------------------------
-spec make_side_by_fork(Fork::#side_fork{}, Resp::#sipmsg{}) -> Side::#side{}.

make_side_by_fork(#side_fork{}=Fork, #sipmsg{class={resp,_,_}}=Resp) ->
    #side_fork{}=Fork,
    {ok,DlgHandle} = nksip_dialog:get_handle(Resp),
    #sipmsg{contacts=RContacts}=Resp,
    #sdp{}=RSdp=?U:extract_sdp(Resp),
    Now = os:timestamp(),
    XSide = #side{callid=Fork#side_fork.callid,
                  rhandle=Fork#side_fork.rhandle,
                  dhandle=DlgHandle,
                  dir='out',
                  req=Resp,
                  %
                  remoteuri=Fork#side_fork.remoteuri,
                  remotetag=Fork#side_fork.remotetag,
                  remotecontacts=RContacts,
                  remotesdp=RSdp,
                  %
                  localuri=Fork#side_fork.localuri,
                  localtag=Fork#side_fork.localtag,
                  localcontact=Fork#side_fork.localcontact,
                  %
                  starttime=Fork#side_fork.starttime,
                  answertime=Now
                 },
    XSide.

%% ---------------------------------
%% update fork_side rec of new participant by 1xx response with sdp
%% ---------------------------------
-spec update_side_fork(Fork::#side_fork{}, Resp::#sipmsg{}) -> UpFork::#side_fork{}.

update_side_fork(#side_fork{}=Fork, #sipmsg{class={resp,SipCode,_}}=Resp) ->
    #side_fork{}=Fork,
    {ok,DlgHandle} = nksip_dialog:get_handle(Resp),
    #sipmsg{contacts=RContacts}=Resp,
    RSdp = ?U:extract_sdp(Resp),
    Fork#side_fork{dhandle=DlgHandle,
                   last_response_code=SipCode,
                   remotecontacts=RContacts,
                   remotesdp=RSdp}.

%% -------------------
%% Cancel active forks
%%
cancel_active_forks([],_,_) -> ok;
cancel_active_forks([Fork|Rest], Opts, State) ->
    #side_fork{rhandle=ReqHandle,
               cmnopts=CmnOpts}=Fork,
    cancel_timer(Fork),
    catch nksip_uac:cancel(ReqHandle, [async|Opts++CmnOpts]),
    cancel_active_forks(Rest, Opts, State).
% @private
cancel_timer(#side_fork{rule_timer_ref=T1, resp_timer_ref=T2}=Fork) ->
    case T1 of undefined -> false; _ -> erlang:cancel_timer(T1) end,
    case T2 of undefined -> false; _ -> erlang:cancel_timer(T2) end,
    Fork#side_fork{rule_timer_ref=undefined,
                   resp_timer_ref=undefined}.

%% ---------------------------------
%% detach existing participant
%% ---------------------------------

% send bye and detach current participant
-spec do_stop_bye(State::#state_ivr{}) -> {ok, State::#state_ivr{}}.

do_stop_bye(State) ->
    #state_ivr{a=Side}=State,
    ?IVR_UTILS:send_bye(Side, State),
    do_stop(State).

% send bye and detach selectd participant
-spec do_stop_bye(Side::#side{}, State::#state_ivr{}) -> {ok, State::#state_ivr{}}.

do_stop_bye(Side, State) ->
    ?IVR_UTILS:send_bye(Side, State),
    do_stop(State).

% detach participant from conference (last should destroy conference)
-spec do_stop(State::#state_ivr{}) -> {ok, State::#state_ivr{}}.

do_stop(State) ->
    #state_ivr{a=#side{callid=CallId}=_Side}=State,
    % from store
    ?DLG_STORE:unlink_dlg(CallId),
    {ok, State}.

%% ----------------------------------------------------------
%% Prepare after success dialing to start active
%% ----------------------------------------------------------
prepare_to_start_active(CallId,#state_ivr{ivrid=IvrId,
                                          invite_ts=TS,
                                          map=Map}=State) ->
    Self = self(),
    % Local node storage
    {IvrId, LStoreData} = maps:get(l_store,Map),
    NewLStoreData = LStoreData#{callids:=[CallId]},
    ?DLG_STORE:store_t(IvrId, NewLStoreData, ?STORE_TIMEOUT),
    erlang:send_after(?STORE_REFRESH, Self, {refresh_at_store_local}),
    ?DLG_STORE:push_dlg(CallId, {IvrId,Self}),
    Opts = maps:get(opts,Map),
    Domain = maps:get(domain,Map),
    [IvrNum] = ?EU:extract_required_props([ivrnum],Opts),
    % Monitor to clear
    Flog = fun(StopReason) -> ?OUT("Process ~p terminated (ivr=~p): ~120p", [Self, IvrNum, StopReason]) end,
    Fclear = fun() -> ?DLG_STORE:unlink_dlg(CallId),
                      ?DLG_STORE:delete_t(IvrId),
                      ?EVENT:ivr_dead({IvrId,CallId,TS},#{domain => Domain}) end,
    [?MONITOR:append_fun(Self, FunX) || FunX <- [Flog, Fclear]],
    State1 = State#state_ivr{acallid = CallId,
                             map=Map#{l_store:={IvrId,NewLStoreData},
                                      opts:=lists:keydelete(script,1,Opts)}},
    ?ACTIVE_SM:start_script(State1).

%% -------------------------------------
%% Send result to owner for start_by_mode
%% -------------------------------------
send_result(#state_ivr{map=Map}=_State,Type, Msg) ->
    FunRes = maps:get(fun_res, Map, fun(_,_) -> ok end),
    Mode = maps:get(mode,Map,undefined),
    do_send_res_by_mode(Mode,FunRes,Type,Msg).

do_send_res_by_mode(<<"1">>=_Mode,FunRes,Type,Msg) -> FunRes(Type,Msg);
do_send_res_by_mode(_Mode,_FunRes,_Type,_Msg) -> ok.

%% -------------------------------------
%% demonitor by ownermode
%% -------------------------------------
demonitor_owner(Map,CurrOperationMode) ->
    Opts = maps:get(opts,Map),
    case lists:keyfind(ownermode, 1, Opts) of
        {_,CurrOperationMode} -> try erlang:demonitor(maps:get(ownerref,Map)) catch _:Err -> {error,Err} end;
        _ -> ok
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================


