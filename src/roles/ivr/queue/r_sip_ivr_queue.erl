%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Pavel Abramov, Peter Bukashin <tbotc@yandex.ru>
%%% @date 24.11.2016
%%% @doc

-module(r_sip_ivr_queue).

-export([start/1,
         router_registered/2,
         fsm_bye/1, fsm_error/1,
         scriptmachine_start/2, scriptmachine_stop/1, scriptmachine_component_start/2,
         dial_response/2, dial_call_successful/2, dial_call_error/2,
         hunt_cancel/2]).

% callback functions for queue decoration purposes
-export([get_script_callback_functions/0]).

% meta params
-export([get_trace_events/1,
         get_expression_funs/0, get_expression_funs/4]).

% control funs (called by sm expressions)
-export([queuepriority/4, queuepriority/3,
         queueposition/3,
         queueestimatedsec/3,
         queuerescount/3, queuerescount/4,
         queuestate/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip_ivr_queue.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ---------------------------------
-spec start(Opts::map()) -> {ok, pid()} | {error, {already_started, pid()}} | {error, Reason :: list()}.
%% ---------------------------------
start(Opts) when is_map(Opts) ->
    gen_server:start(?QUEUE_SRV, Opts, []);
start(Opts) -> wrong_param("start",Opts).

%% ---------------------------------
-spec get_trace_events(Pid::pid()) -> Meta :: #{script_start=>FunStart::fun(), script_stop=>FunStop::fun(), component_start=>FunComponentStart::fun()}.
%% ---------------------------------
get_trace_events(Pid) ->
    #{'script_start' => fun(ScriptPid) -> scriptmachine_start(Pid, ScriptPid) end,
      'script_stop' => fun() -> scriptmachine_stop(Pid) end,
      'component_start' => fun(Type) -> scriptmachine_component_start(Pid, Type) end}.

%% ---------------------------------
%% register pid of IVR FSM
%% ---------------------------------
-spec router_registered(Pid::pid(), IVRFSMPid::pid()) -> ok.
%% ---------------------------------
router_registered(Pid, IVRFSMPid) when is_pid(Pid) and is_pid(IVRFSMPid)->
    gen_server:cast(Pid, {?Q_1_R_REG, IVRFSMPid});
router_registered(_,_) -> wrong_param("router_registered").

%% ---------------------------------
%% call is finished
%% ---------------------------------
-spec fsm_bye(Pid::pid()) -> ok.
%% ---------------------------------
fsm_bye(Pid) when is_pid(Pid) ->
    gen_server:cast(Pid, ?Q_2_FSM_BYE);
fsm_bye(Pid) -> wrong_param("fsm_bye",Pid).

%% ---------------------------------
%% fsm dialog's pid is down
%% @TODO not used yet, DOWN got in handle_info.
%% ---------------------------------
-spec fsm_error(Pid::pid()) -> ok.
%% ---------------------------------
fsm_error(Pid) when is_pid(Pid) ->
    gen_server:cast(Pid, ?Q_3_FSM_ERROR);
fsm_error(Pid) -> wrong_param("fsm_error",Pid).

%% ---------------------------------
%% start of script
%% ---------------------------------
-spec scriptmachine_start(Pid::pid(), ScmPid::pid()) -> ok.
%% ---------------------------------
scriptmachine_start(Pid, ScmPid) when is_pid(Pid) ->
    gen_server:cast(Pid, {?Q_4_SCR_START, ScmPid});
scriptmachine_start(Pid, _) -> wrong_param("scriptmachine_start",Pid).

%% ---------------------------------
%% stop of script
%% ---------------------------------
-spec scriptmachine_stop(Pid::pid()) -> ok.
%% ---------------------------------
scriptmachine_stop(Pid) when is_pid(Pid) ->
    gen_server:cast(Pid, ?Q_5_SCR_STOP);
scriptmachine_stop(Pid) -> wrong_param("scriptmachine_stop",Pid).

%----------------------------------------------------------------------

%% ---------------------------------
%% new script's component started work
%% ---------------------------------
-spec scriptmachine_component_start(Pid::pid(), Type::non_neg_integer()) -> ok.
%% ---------------------------------
scriptmachine_component_start(Pid, Type) when is_pid(Pid) and is_integer(Type) and (Type > 0) ->
    case Type of
        201 -> gen_server:cast(Pid, ?Q_6_SCR_REFER);
        203 -> gen_server:cast(Pid, ?Q_6_SCR_REFER);
        215 -> gen_server:cast(Pid, ?Q_6_SCR_REFER);
        217 -> gen_server:cast(Pid, ?Q_6_SCR_REFER);
        _ -> ok
    end;
scriptmachine_component_start(_, Type) -> wrong_param("scriptmachine_component_start",Type).

%% ---------------------------------
%% hunt's rejection
%% @TODO not used yet. Hunt sends message directrly (pid ! {huntq_error, Report})
%% ---------------------------------
-spec hunt_cancel(Pid::pid(),Report::term()) -> ok.
%% ---------------------------------
hunt_cancel(Pid, Report) when is_pid(Pid) ->
    gen_server:cast(Pid, {?Q_8_HUNT_ERROR, Report});
hunt_cancel(Pid, _) -> wrong_param("hunt_cancel",Pid).

%% ---------------------------------
%% dialing got answer
%% ---------------------------------
-spec dial_response(Pid::pid(),Response::term()) -> ok.
%% ---------------------------------
dial_response(Pid, Response) when is_pid(Pid) ->
    gen_server:cast(Pid, {?Q_10_11_DIAL_RESPONSE, Response});
dial_response(Pid, _) -> wrong_param("dial_response",Pid).

%% ---------------------------------
%% dialing finished with 2xx answer
%% ---------------------------------
-spec dial_call_successful(Pid::pid(),CallParam::term()) -> ok.
%% ---------------------------------
dial_call_successful(Pid, CallParam) when is_pid(Pid) ->
    gen_server:cast(Pid, {?Q_12_DIAL_SUCCESSFUL, CallParam});
dial_call_successful(Pid, _) -> wrong_param("dial_call_successful",Pid).

%% ---------------------------------
%% dialing finished with non 2xx, or timeout.
%% ---------------------------------
-spec dial_call_error(Pid::term(),ErrorParam::term()) -> ok.
%% ---------------------------------
dial_call_error(Pid, ErrorParam) when is_pid(Pid) ->
    gen_server:cast(Pid, {?Q_13_DIAL_CALL_ERROR, ErrorParam});
dial_call_error(Pid, _) -> wrong_param("dial_call_error",Pid).

%% ====================================================================
%% Script callback functions
%% ====================================================================

%% ----------------------------
%% return function to handle wivrscript and qivrscript callbacks (ComponentType::integer, State::#state_scr{}) -> State::#state_scr{}.)
%% ----------------------------
-spec get_script_callback_functions() -> map(). % atom() -> function()
%% ----------------------------
get_script_callback_functions() -> ?QUEUE_CB:get_script_callback_functions().

%% ====================================================================
%% Control functions
%% ====================================================================

%% ---------------------------------
%% dummy script-expression functions
%% ---------------------------------
-spec get_expression_funs() -> Functions::[function()].
%% ---------------------------------
get_expression_funs() ->
    F_invalid0 = fun() -> <<"invalid_mode">> end,
    F_invalid1 = fun(_) -> <<"invalid_mode">> end,
    [
     {{?Q_STYPE_HuntId, 0}, F_invalid0},
     {{?Q_STYPE_HuntObjId, 0}, F_invalid0},
     {{?Q_STYPE_Priority, 1}, F_invalid1},
     {{?Q_STYPE_Priority, 0}, F_invalid0},
     {{?Q_STYPE_Position, 0}, F_invalid0},
     {{?Q_STYPE_Estimated, 0}, F_invalid0},
     {{?Q_STYPE_Count, 0}, F_invalid0},
     {{?Q_STYPE_Count, 1}, F_invalid1},
     {{?Q_STYPE_State, 0}, F_invalid0}
    ].

%% ---------------------------------
-spec get_expression_funs(Pid::pid(), Domain::binary(), HuntId::binary(), QueueId::binary()) -> Functions::[function()] | false.
%% ---------------------------------
get_expression_funs(Pid, Domain, HuntId, QueueId) ->
    [
     {{?Q_STYPE_HuntId, 0}, fun() -> HuntId end},
     {{?Q_STYPE_HuntObjId, 0}, fun() -> QueueId end},
     {{?Q_STYPE_Priority, 1}, fun(P) -> queuepriority(P, Domain, HuntId, QueueId) end},
     {{?Q_STYPE_Priority, 0}, fun() -> queuepriority(Domain, HuntId, QueueId) end},
     {{?Q_STYPE_Position, 0}, fun() -> queueposition(Domain, HuntId, QueueId) end},
     {{?Q_STYPE_Estimated, 0}, fun() -> queueestimatedsec(Domain, HuntId, QueueId) end},
     {{?Q_STYPE_Count, 0}, fun() -> queuerescount(Domain, HuntId, QueueId) end},
     {{?Q_STYPE_Count, 1}, fun(T) -> queuerescount(T, Domain, HuntId, QueueId) end},
     {{?Q_STYPE_State, 0}, fun() -> queuestate(Pid) end}
    ].

%% ---------------------------------
%% Change priority of element in queue
%% Makes request to huntq in selected fsm (huntq fsm) to modify priority
%% Huntq moves element and return new position
%% ---------------------------------
-spec queuepriority(NewPriority::integer(), Domain::binary(), HuntId::binary(), QueueId::binary()) -> NewPosition::integer().
%% ---------------------------------
queuepriority(NewPriority,Domain, HuntId, QueueId) ->
    Priority = ?APP_SCRIPT:expr_u_to_int(NewPriority),
    ?QUEUE_HUNTQ:huntq_change_priority(Priority,Domain, HuntId, QueueId).

%% ---------------------------------
%% Get priority of element in queue.
%% Makes request to huntq into selected fsm (huntq fsm)
%% ---------------------------------
-spec queuepriority(Domain::binary(), HuntId::binary(), QueueId::binary()) -> CurrentPriority::integer().
%% ---------------------------------
queuepriority(Domain, HuntId, QueueId) -> ?QUEUE_HUNTQ:huntq_get_priority(Domain, HuntId, QueueId).

%% ---------------------------------
%% Get position in queue
%% Makes request to huntq into selected fsm (huntq fsm)
%% ---------------------------------
-spec queueposition(Domain::binary(), HuntId::binary(), QueueId::binary()) -> Position::integer().
%% ---------------------------------
queueposition(Domain, HuntId, QueueId) -> ?QUEUE_HUNTQ:huntq_get_position(Domain, HuntId, QueueId).

%% ---------------------------------
%% Get estimated time to wait for element in queue
%% Makes request to huntq into selected fsm (huntq fsm)
%% ---------------------------------
-spec queueestimatedsec(Domain::binary(), HuntId::binary(), QueueId::binary()) -> Seconds::integer().
%% ---------------------------------
queueestimatedsec(Domain, HuntId, QueueId) -> ?QUEUE_HUNTQ:huntq_get_estimated_seconds(Domain, HuntId, QueueId).

%% ---------------------------------
%% Get online resource count in group
%% ---------------------------------
-spec queuerescount(Domain::binary(), HuntId::binary(), QueueId::binary()) -> Total::integer().
%% ---------------------------------
queuerescount(Domain, HuntId, QueueId) -> ?QUEUE_HUNTQ:huntq_get_resourses_count_total(Domain, HuntId, QueueId).

%% ---------------------------------
%% Get online resource count in group by type
%% ---------------------------------
-spec queuerescount(Type::online|free, Domain::binary(), HuntId::binary(), QueueId::binary()) -> Count::integer().
%% ---------------------------------
queuerescount(online,Domain, HuntId, QueueId) -> ?QUEUE_HUNTQ:huntq_get_resourses_count_online(Domain, HuntId, QueueId);
queuerescount(free,Domain, HuntId, QueueId) -> ?QUEUE_HUNTQ:huntq_get_resourses_count_free(Domain, HuntId, QueueId).

%% ---------------------------------
%%  Get element's state in queue
%% ---------------------------------
-spec queuestate(Pid::pid()) -> State::binary().
%% ---------------------------------
queuestate(Pid) ->
    State = gen_server:call(Pid, ?Q_GET_STATE),
    ?EU:to_binary(State#state_queue.state).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ---------------------------------
%% Prepare error term
%% ---------------------------------
-spec wrong_param(Method::list(), Param::any()) -> {error, Reason::list()}.
%% ---------------------------------
wrong_param(Method, _Param) ->
    {error, ?EU:str("~p -- ~p: wrong param.", [?MODULE,Method])}.
wrong_param(Method) -> wrong_param(Method, "").
