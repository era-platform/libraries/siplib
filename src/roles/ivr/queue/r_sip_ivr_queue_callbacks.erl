%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 23.07.2019
%%% @doc RP-1481 Implements callback functions to handle callbacks from wivrscript and qivrscript of hunt
%%%      Overrides REFER ivr component behaviour

-module(r_sip_ivr_queue_callbacks).
-author('Peter Bukashin <tbotc@yandex.ru>').

%%-export([get_script_callback_functions/0]).
%%
%%%% ====================================================================
%%%% Define
%%%% ====================================================================
%%
%%-include("../../era_script/include/r_script.hrl").
%%
%%%% ====================================================================
%%%% Public functions
%%%% ====================================================================
%%
%%%% ----------------------------
%%-spec get_script_callback_functions() -> map(). % atom() -> function()
%%%% ----------------------------
%%get_script_callback_functions() -> #{'fun_after_init_component' => fun fun_after_init_component/2,
%%                                     'fun_after_component' => fun fun_after_component/2}.
%%
%%%% ====================================================================
%%%% Internal functions
%%%% ====================================================================
%%
%%%% handles every async component after init (designed for wivrscript and qivrscript),
%%%%   detects HuntGhost, stores huntid and elementid.
%%%%   when after that founds any refer component then destroy huntq task and stop call to resource agent
%%fun_after_init_component(Type,#state_scr{external_data=ED}=State) ->
%%    ED1 = case Type of
%%              50001 -> % ghost hunt
%%                  #state_scr{active_component=#component{state=CS}}=State,
%%                  [QPid,HuntId,ElementId] = ?EU:maps_get([qpid,hunt_id,element_id],CS),
%%                  #{qpid => QPid,
%%                    hunt_id => HuntId,
%%                    element_id => ElementId};
%%              X when (X==203 orelse X==217 orelse X==215) andalso is_map(ED) -> % 203 refer to, 217 refer attended, 215 refer replaces,
%%                  QPid = maps:get(qpid,ED,undefined),
%%                  HuntId = maps:get(hunt_id,ED,undefined),
%%                  ElementId = maps:get(element_id,ED,undefined),
%%                  if (HuntId/=undefined andalso ElementId/=undefined andalso is_pid(QPid)) ->
%%                          Object = #{id=>ElementId, result=>work_ended},
%%                          Req = {hq_request, {'sip',<<"undefined">>}, HuntId, report, Object},
%%                          QPid ! cancel_resources,
%%                          ?ENVCALL:call_huntq_local(State#state_scr.domain, Req, undefined),
%%                          ED;
%%                      true -> ED
%%                  end;
%%              _ -> ED
%%          end,
%%    State#state_scr{external_data=ED1}.
%%
%%%% handles every component before terminate (designed for wivrscript and qivrscript)
%%%%   detects refers after huntq and throws normal exit state to script machine.
%%fun_after_component(Type, #state_scr{external_data=ED}=State) ->
%%    case Type of
%%        X when (X==203 orelse X==217 orelse X==215) andalso is_map(ED) -> % 203 refer to, 217 refer attended, 215 refer replaces
%%            HuntId = maps:get(hunt_id,ED,undefined),
%%            ElementId = maps:get(element_id,ED,undefined),
%%            if HuntId/=undefined andalso ElementId/=undefined -> gen_server:cast(self(), {'stop',<<"Abort queue after refer">>});
%%                true -> ED
%%            end;
%%        _ -> ED
%%    end,
%%    State.