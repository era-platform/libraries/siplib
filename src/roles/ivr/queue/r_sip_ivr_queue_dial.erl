%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Pavel Abramov, Peter Bukashin <tbotc@yandex.ru>
%%% @date 24.11.2016
%%% @doc
%%% @todo

-module(r_sip_ivr_queue_dial).
-export([dial_call/2, dial_clear_calls/1, dial_stop_other_calls/2,dial_get_replaced_info/2,
         dial_get_response_code/1, dial_call_error/2, dial_get_user_id_from_response/1, dial_cancel/1,
         terminate/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip_ivr_queue.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% -----------------------------------------------
%% Пытаемся совершить звонок. Если удачно добавляем в ресурсы и добавляем в монитор.
%% -----------------------------------------------
-spec dial_call(UserId::binary(), #state_queue{}) -> {ok, #state_queue{}} | {error, Reason::list()}.
%% -----------------------------------------------
dial_call(UserId, #state_queue{domain=D, resourses=Resources}=State) ->
    Self = self(),
    case ?ACCOUNTS:get_sipuser_by_id(UserId, D) of
        undefined -> {error, "phonenumber not found"};
        {ok, Item} ->
            Number = maps:get(phonenumber,Item),
            FunRes = fun(call_inited, _) -> ok;
                        (pre_res, Msg) -> ?QUEUE:dial_response(Self,{{'dialer_response_1xx', UserId},Msg});
                        (success_res, Msg) -> ?QUEUE:dial_response(Self,{{'dialer_response_2xx', UserId},Msg});
                        (unsuccess_res, Msg) -> ?QUEUE:dial_call_error(Self,{{'dialer_response_failed',UserId},Msg});
                        (refer_notify, _) -> ok;
                        (ivr_started, _) -> ok;
                        (ivr_msg, _) -> ok;
                        (ivr_stopped, Msg) -> ?QUEUE:dial_call_successful(Self,{{'dialer_ivr_stopped',UserId},Msg})
                     end,
            case dial_call_on_number(Number, FunRes, State) of
                {ok, Data} -> {ok, State#state_queue{resourses=lists:append([{UserId, Data}], Resources)}};
                Error -> Error
            end
    end.

%% -----------------------------------------------
%% Функция отпавки cancel или bye всем юзерам в списке.
%% Список не очищается.
%% -----------------------------------------------
-spec dial_clear_calls(State::#state_queue{}) -> ok.
%% -----------------------------------------------
dial_clear_calls(#state_queue{resourses=Resources}=_State) ->
    lists:foreach(fun(Resource) -> dial_call_stop(Resource) end, Resources).

%% -----------------------------------------------
%% Функция отпавки cancel или bye всем юзерам в списке, кроме переданного.
%% Список не очищается.
%% -----------------------------------------------
-spec dial_stop_other_calls(UserId::binary(), State::#state_queue{}) -> ok.
%% -----------------------------------------------
dial_stop_other_calls(UserId, #state_queue{resourses=Resources}=_State) ->
    lists:foreach(
      fun({UId, _Data}=Resource) ->
              case UId of
                  UserId -> ok;
                  _ -> dial_call_stop(Resource)
              end
      end, Resources).

%% -----------------------------------------------
%% Функция получения кода из ответа диалера.
%% -----------------------------------------------
-spec dial_get_response_code({{ResponseCode, UserId::binary()}, {pid(), map()}}) -> Code::non_neg_integer() when
    ResponseCode :: 'dialer_response_1xx' | 'dialer_response_2xx'.
%% -----------------------------------------------
dial_get_response_code({{'dialer_response_1xx',_UserId}, {_FromDialerPid, #{}=Data}}) ->
    get_sipcode_from_data(Data);
dial_get_response_code({{'dialer_response_2xx',_UserId}, {_FromDialerPid, #{}=Data}}) ->
    get_sipcode_from_data(Data);
dial_get_response_code(_Response) -> 0.

%% -----------------------------------------------
%% Функция получения UserId из ответа диалера.
%% -----------------------------------------------
-spec dial_get_user_id_from_response({{ResponseCode, binary()}, {pid(), map()}}) -> UserId::binary() when
    ResponseCode :: 'dialer_response_1xx' | 'dialer_response_2xx'.
%% -----------------------------------------------
dial_get_user_id_from_response({{_ResponseCode, UserId}, _Msg}) -> UserId;
dial_get_user_id_from_response(_Response) -> <<>>.

%% -----------------------------------------------
%% Функция обработки ошибок от диалера.
%% Пробегаем по списку ресурсов и находим сопоставление с сообщением о ошибке.
%% Возвращаем false при непонятном сообщении(не для диалера или не нашли юзера по текущему сообщению)
%% При соответствии удаляем ресурс из списка.
%% -----------------------------------------------
-spec dial_call_error(Msg, #state_queue{}) -> {UserId, UpdateState::#state_queue{}} | false when
    Msg::{{'dialer_response_failed',UserId}, {pid(), map()}} | {'DOWN', reference(), process, pid(), list()},
    UserId::binary().
%% -----------------------------------------------
dial_call_error({'DOWN', RefDialer, process, _DialerPid, _Result}, #state_queue{resourses=Resources}=State) ->
    case dial_call_down_find_1(Resources, RefDialer) of
        {false,_} -> false;
        {UserId, UpdateResources} -> {UserId, State#state_queue{resourses=UpdateResources}}
    end;
dial_call_error({{'dialer_response_failed', UserId}, _DialerMsg}, #state_queue{resourses=Resources}=State) ->
    case dial_call_down_find_2(Resources, UserId) of
        {false,_,_} -> false;
        {UserId, RefDialer, UpdateResources} ->
            erlang:demonitor(RefDialer),
            {UserId, State#state_queue{resourses=UpdateResources}}
    end;
dial_call_error({resdialtimeout, RefDialer}, #state_queue{resourses=Resources}=State) ->
    ?LOG("~p -- timeout ref: ~120p", [?MODULE, RefDialer]),
    F = fun({_, {_,_,Ref,_}}=R, {false, Acc}) -> case Ref of RefDialer -> {R, Acc}; _ -> {false, lists:append(Acc, [R])} end;
           (R, {X, Acc}) -> {X, lists:append(Acc, [R])}
        end,
    case lists:foldl(F, {false,[]}, Resources) of
        {false,_} -> false;
        {{UserId,_}=Resource, UpdateResources} ->
            dial_call_stop(Resource),
            {UserId, State#state_queue{resourses=UpdateResources}}
    end.

%% -----------------------------------------------
dial_cancel(#state_queue{resourses=Resources}=_State) ->
    ?LOG("~p -- cancel all", [?MODULE]),
    lists:foreach(fun(Resource) -> dial_call_stop(Resource) end, Resources).

%% -----------------------------------------------
%% Получаем необходимы параметры для формирования скрипта из ответа диалера.
%% -----------------------------------------------
-spec dial_get_replaced_info(CallParam, State) -> false | {ReferToUri, ReplaceCallId, ReplaceToTag, ReplaceFromTag} when
    CallParam :: {{'dialer_ivr_stopped',UserId}, {pid(), map()}},
    UserId :: binary(),
    State :: #state_queue{},
    ReferToUri :: binary(),
    ReplaceCallId :: binary(),
    ReplaceToTag :: binary(),
    ReplaceFromTag :: binary().
%% -----------------------------------------------
dial_get_replaced_info({{'dialer_ivr_stopped',UserId},{_FromDialerPid, #{}=Data}}, #state_queue{resourses=Resources,domain=D}=_State) ->
    case lists:keysearch(UserId, 1, Resources) of
        {value, {_, {Number,_DialerPid,_RefDialer,_IvrNode}}} ->
            ReferToUri = <<"sip:", Number/binary, $@, D/binary >>,
            ReplaceCallId = maps:get(callid, Data),
            ReplaceToTag = maps:get(remotetag, Data),
            ReplaceFromTag = maps:get(localtag, Data),
            {ReferToUri, ReplaceCallId, ReplaceToTag, ReplaceFromTag};
        false -> false
    end.

%% -----------------------------------------------
%%
%% -----------------------------------------------
-spec terminate(State::#state_queue{}) -> ok.
%% -----------------------------------------------
terminate(#state_queue{}=State) -> dial_clear_calls(State).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -----------------------------------------------
%%
%% -----------------------------------------------
-spec dial_call_down_find_1(list(), reference()) -> {UserId, list()} | {false, list()}  when UserId :: binary().
%% -----------------------------------------------
dial_call_down_find_1(Resources, RefDialer) ->
    dial_call_down_find_Acc_1(Resources, RefDialer, {false,[]}).
%% @private
dial_call_down_find_Acc_1([], _, Acc) -> Acc;
dial_call_down_find_Acc_1([{UserId, Data}=Res|T], RefDialer, {U, Acc}) ->
    case Data of
        {_,_,RefDialer,_} -> {UserId, lists:append([T,Acc])};
        _ -> dial_call_down_find_Acc_1(T, RefDialer, {U, lists:append([Res], Acc)})
    end.

%% -----------------------------------------------
%%
%% -----------------------------------------------
-spec dial_call_down_find_2(list(), UserId) -> {UserId, reference(), list()} | {false, undefined, list()} when UserId :: binary().
%% -----------------------------------------------
dial_call_down_find_2(Resources, UserId) ->
    dial_call_down_find_Acc_2(Resources, UserId, {false, undefined, []}).

%% @private
dial_call_down_find_Acc_2([],_,Acc) -> Acc;
dial_call_down_find_Acc_2([{UserId, {_Number,_DialerPid,RefDialer,_IvrNode}}|T], UserId, {_,_,Acc}) ->
     {UserId, RefDialer, lists:append([T,Acc])};
dial_call_down_find_Acc_2([{_UserId, _Data}=Res|T], UserId, {U,R,Acc}) ->
    dial_call_down_find_Acc_2(T,UserId,{U, R,lists:append([Res], Acc)}).

%% -----------------------------------------------
%%
%% -----------------------------------------------
-spec get_sipcode_from_data(map()) -> Code::non_neg_integer().
%% -----------------------------------------------
get_sipcode_from_data(#{}=Data) ->
    ErrSipCode = maps:get(sipcode,Data),
    ?EU:to_int(ErrSipCode).

%% -----------------------------------------------
%%
%% -----------------------------------------------
-spec dial_call_stop({UserId, {binary(), pid(), reference(), node()}}) -> ok when
    UserId :: binary().
%% -----------------------------------------------
dial_call_stop({UserId, {_Number,DialerPid,_RefDialer,IvrNode}}) ->
    case ?ENVCROSS:call_node(IvrNode, {?IVR_SRV, stop, [DialerPid, <<"Cancel from queue">>]}, error, 3000) of
        error -> ?LOG("~120p -- dial_stop_other_calls: error (~120p).", [?MODULE, UserId]), ok;
        _ -> ok
    end.

%% -----------------------------------------------
%%
%% -----------------------------------------------
dial_call_on_number(Number, FunRes, #state_queue{domain=D, from_user=FU, from_dn=FDN, huntq_id=HuntId, parent_functions=ParentFunctions}=State) ->
    Self = self(),
    Mode = <<"1">>,
    Code = State#state_queue.remote_ivr_script_code,
    QId = State#state_queue.current_queue_id,
    Timeout = State#state_queue.resdialtimeout,
    Args = [{script, undefined},
            {script_code, Code},
            {domain, D},
            {number, Number},
            {fun_res, FunRes},
            {callername, FDN},
            {callerid, FU},
            {ownerpid, Self},
            {ownermode, 3},
            {headers, [{?B2BHeader, <<"redirect=0;">>}]},
            {expression_funs, [lists:keyfind({'remotenum',0},1,ParentFunctions), % RP-1468
                               lists:keyfind({'calledext',0},1,ParentFunctions), % RP-1468
                               lists:keyfind({'callednum',0},1,ParentFunctions), % RP-1468
                               lists:keyfind({'ivrparam',1},1,ParentFunctions), % RP-1468
                               {{'q_displayname',0}, fun() -> FDN end}, % RP-1468
                               {{'q_username',0}, fun() -> FU end}, % RP-1468
                               {{'q_domain',0}, fun() -> D end}, % RP-1468
                               {{'r_number',0}, fun() -> Number end} % RP-1468
                               |?QUEUE:get_expression_funs(Self, D, HuntId, QId)]}],
    Params = #{mode=>Mode,args=>Args},
    IvrNode = node(),
    case ?ENVCROSS:call_node(IvrNode, {?IVR,start_by_mode,[Params]}, undefined, 3000) of
        {ok,DialerPid} ->
            RefDialer = erlang:monitor(process, DialerPid),
            case Timeout of T  when T > 0 -> timer:send_after(Timeout, Self, {resdialtimeout, RefDialer}); _ -> ok end,
            {ok, {Number, DialerPid, RefDialer, IvrNode}};
        _ -> {error, "error start dialing"}
    end.
