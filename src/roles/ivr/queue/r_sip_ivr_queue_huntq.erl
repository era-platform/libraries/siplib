%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Pavel Abramov, Peter Bukashin <tbotc@yandex.ru>
%%% @date 24.11.2016
%%% @doc
%%% @todo

-module(r_sip_ivr_queue_huntq).
-export([huntq_put_in_queue/1, huntq_remove_from_queue/1, huntq_user_ban/2, huntq_unsubscribe_other_resourse/2, terminate/1]).
% Control functions
-export([huntq_change_priority/4, huntq_get_priority/3, huntq_get_position/3, huntq_get_estimated_seconds/3,
         huntq_get_resourses_count_total/3, huntq_get_resourses_count_online/3, huntq_get_resourses_count_free/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip_ivr_queue.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

% Пытаемся разместиться в очереди. Возможен таймаут.
-spec huntq_put_in_queue(State::#state_queue{}) ->  accepted | empty_resources | empty_resources_still_wait | error.

huntq_put_in_queue(#state_queue{site=Site,domain=Domain,huntq_id=HId,current_queue_id=QId}=State) ->
    O = #{id => QId, priority => 0, owner_site => Site, owner_process => self()},
    case huntq_request_put(Domain, HId, O) of
        {error, empty_resources} ->
            ?EVENT:queue_enqueue(#{<<"result">> => <<"empty_resources">>}, State),
            empty_resources;
        {error, empty_resources_still_wait} ->
            ?EVENT:queue_enqueue(#{<<"result">> => <<"empty_resources_still_wait">>}, State),
            empty_resources_still_wait;
        {error, _} ->
            ?EVENT:queue_enqueue(#{<<"result">> => <<"error">>}, State),
            error;
        Result ->
            ?EVENT:queue_enqueue(#{<<"result">> => ?EU:to_binary(Result)}, State),
            Result
    end.

%----------------------------------------------------------------------
% Удаляем все ресурсы в списке. Очищаем список.
% @TODO Возможен таймаут при обращении к ген серверу.
-spec huntq_remove_from_queue(State::#state_queue{}) -> UpdateState::#state_queue{}.

huntq_remove_from_queue(#state_queue{resourses=[]}=State) -> State;
huntq_remove_from_queue(#state_queue{domain=Domain,huntq_id=HId,current_queue_id=QId}=State) ->
    huntq_work_end(Domain, HId, QId),
    State#state_queue{resourses=[]}.

%----------------------------------------------------------------------
% Баним ресурс.
% @TODO Возможен таймаут при обращении к ген серверу.
-spec huntq_user_ban(UserId::binary(), State::#state_queue{}) -> ok.

huntq_user_ban(UserId, #state_queue{domain=Domain,huntq_id=HId,current_queue_id=QId}=State) ->
    ?EVENT:queue_userban(#{<<"userid">> => UserId}, State),
    O = #{id=>QId,rid=>{{'sip',<<"undefined">>},UserId},result=>hunt_bad_resource},
    huntq_request_report(Domain, HId, O),
    ok.

%----------------------------------------------------------------------
% Отписываемся от всех ресурсов кроме переданного. Очищаем список ресурсов.
% Должен остаться только один.
% @TODO Возможен таймаут при обращении к ген серверу.
-spec huntq_unsubscribe_other_resourse(UserId::binary(), State::#state_queue{}) -> UpdateState::#state_queue{}.

huntq_unsubscribe_other_resourse(UserId, #state_queue{domain=Domain,huntq_id=HId,current_queue_id=QId,resourses=Resourses}=State) ->
    UpdateResourses = lists:filter(fun({UId,_}) -> case UId of UserId -> true; _ ->huntq_free_resourse(Domain, HId, UserId, QId), false end end, Resourses),
    State#state_queue{resourses=UpdateResourses}.

%----------------------------------------------------------------------

-spec terminate(State::#state_queue{}) -> ok.

terminate(#state_queue{}=_State) ->
    %huntq_remove_from_queue(State), %@TODO
    ok.

%% ====================================================================
%% Control functions
%% ====================================================================

% Изменить приоритет очереди.
-spec huntq_change_priority(Priority::integer(), Domain::binary(), HuntId::binary(), QueueId::binary()) -> NewPriority::integer().

huntq_change_priority(Priority, Domain, HuntId, QueueId) ->
    Site = ?CFG:get_current_site(),
    O = #{id => QueueId, priority => Priority, owner_site => Site, owner_process => self()},
    case huntq_request_update(Domain, HuntId, O) of
        {error, _} -> -1;
        _ -> huntq_get_position(Domain, HuntId, QueueId)
    end.

%----------------------------------------------------------------------
% Узнать приоритет очереди.
-spec huntq_get_priority(Domain::binary(), HuntId::binary(), QueueId::binary()) -> Priority::integer().

huntq_get_priority(Domain, HuntId, QueueId) ->
    O = #{id => QueueId},
    case huntq_statistic(Domain, HuntId, ?Q_STYPE_Priority, O) of
        Result when is_integer(Result) -> Result;
        _ -> -1
    end.

%----------------------------------------------------------------------
% Узнать позицию.
-spec huntq_get_position(Domain::binary(), HuntId::binary(), QueueId::binary()) -> Position::integer().

huntq_get_position(Domain, HuntId, QueueId) ->
    O = #{id => QueueId},
    case huntq_statistic(Domain, HuntId, ?Q_STYPE_Position, O) of
        Result when is_integer(Result) -> Result;
        _ -> -1
    end.

%----------------------------------------------------------------------
% Узнать приблизительное время ожидания.
-spec huntq_get_estimated_seconds(Domain::binary(), HuntId::binary(), QueueId::binary()) -> Seconds::integer().

huntq_get_estimated_seconds(Domain, HuntId, QueueId) ->
    O = #{id => QueueId},
    case huntq_statistic(Domain, HuntId, ?Q_STYPE_Estimated, O) of
        Result when is_integer(Result) -> Result;
        _ -> -1
    end.

%----------------------------------------------------------------------
% Узнать количество обслуживающих ресурсов в группе всего.
-spec huntq_get_resourses_count_total(Domain::binary(), HuntId::binary(), QueueId::binary()) -> Count::integer().

huntq_get_resourses_count_total(Domain, HuntId, QueueId) ->
    O = #{id => QueueId, type => total},
    case huntq_statistic(Domain, HuntId, ?Q_STYPE_Count, O) of
        Result when is_integer(Result) -> Result;
        _ -> -1
    end.

%----------------------------------------------------------------------
% Узнать количество обслуживающих ресурсов в группе онлайн.
-spec huntq_get_resourses_count_online(Domain::binary(), HuntId::binary(), QueueId::binary()) -> Count::integer().

huntq_get_resourses_count_online(Domain, HuntId, QueueId) ->
    O = #{id => QueueId, type => online},
    case huntq_statistic(Domain, HuntId, ?Q_STYPE_Count, O) of
        Result when is_integer(Result) -> Result;
        _ -> -1
    end.

%----------------------------------------------------------------------
% Узнать количество обслуживающих ресурсов в группе свободных.
-spec huntq_get_resourses_count_free(Domain::binary(), HuntId::binary(), QueueId::binary()) -> Count::integer().

huntq_get_resourses_count_free(Domain, HuntId, QueueId) ->
    O = #{id => QueueId, type => free},
    case huntq_statistic(Domain, HuntId, ?Q_STYPE_Count, O) of
        Result when is_integer(Result) -> Result;
        _ -> -1
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%@private
-spec huntq_work_end(Domain::binary(), HuntId::binary(), ResourseId::binary()) -> ok.

huntq_work_end(Domain, HuntId, QId) ->
    O = #{id=>QId, result=>work_ended},
    huntq_request_report(Domain, HuntId, O),
    ok.

%----------------------------------------------------------------------
%@private
-spec huntq_free_resourse(Domain::binary(), HuntId::binary(), UserId::binary(), ResourseId::binary()) -> ok.

huntq_free_resourse(Domain, HuntId, UserId, ResourseId) ->
    O = #{id=>ResourseId, rid=>{{'sip',<<"undefined">>},UserId}, result=>freer},
    huntq_request_report(Domain, HuntId, O),
    ok.

%----------------------------------------------------------------------
%@private
-spec huntq_request_report(Domain::binary(), HuntId::binary(), Object::map()) -> Result | {error, empty_resources} | {error, Reason::list()} when Result :: term().

huntq_request_report(Domain, HuntId, Object) ->
    Req = {hq_request, {'sip',<<"undefined">>}, HuntId, report, Object},
    huntq_call_server(Domain, Req).

%----------------------------------------------------------------------
%@private
-spec huntq_request_put(Domain::binary(), HuntId::binary(), Object::map()) -> Result | {error, empty_resources} | {error, Reason::list()} when Result :: term().

huntq_request_put(Domain, HuntId, Object) ->
    Req = {hq_request, {'sip',<<"undefined">>}, HuntId, put, Object},
    huntq_call_server(Domain, Req).

%----------------------------------------------------------------------
%@private
-spec huntq_request_update(Domain::binary(), HuntId::binary(), Object::map()) -> Result | {error, empty_resources} | {error, Reason:: list()} when Result :: term().

huntq_request_update(Domain, HuntId, Object) ->
    Req = {hq_request, {'sip',<<"undefined">>}, HuntId, update, Object},
    huntq_call_server(Domain, Req).

%----------------------------------------------------------------------
%@private
-spec huntq_call_server(Domain::binary(), Req::tuple()) -> Result | {error, empty_resources} | {error, Reason::list()} when Result :: term() | integer().

huntq_call_server(Domain, Req) ->
    case ?ENVCALL:call_huntq_local(Domain, Req, undefined) of
        {ok, Result} -> Result;
        {error, empty_resources} -> {error, empty_resources};
        {error, empty_resources_still_wait} -> {error, empty_resources_still_wait};
        I when is_integer(I) -> I;
        _A -> {error, "other error"}
    end.

%----------------------------------------------------------------------
%@private
-spec huntq_statistic(Domain::binary(), HuntId::binary(), InfoType::atom(), Args::list()) -> Result | {error, Reason::list()} when Result :: term().

huntq_statistic(Domain, HuntId, InfoType, Args) ->
    Req = {hq_stat, {'sip',<<"undefined">>}, HuntId, InfoType, Args},
    case huntq_call_server(Domain, Req) of
        {error, _} -> {error, "huntq_call_server error"};
        Result -> Result
    end.
