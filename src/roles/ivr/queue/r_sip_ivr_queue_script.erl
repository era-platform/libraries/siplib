%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Pavel Abramov, Peter Bukashin <tbotc@yandex.ru>
%%% @date 24.11.2016
%%% @doc

-module(r_sip_ivr_queue_script).

-export([create_refer_replace_script/2,
         create_refer_script/2,
         create_hunt_queue_script/3]).
-export([script_replace/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip_ivr_queue.hrl").

-define(OIDStart, 1).
-define(OIDAnswer, 2).
-define(OIDPause, 3).
-define(OIDRefer, 4).
-define(OIDBye, 5).
-define(OIDStop, 6).
-define(OIDGhost, 7).
-define(OIDExecscriptW, 8).
-define(OIDExecscriptQ, 9).
-define(OIDPause2, 10).

%% ====================================================================
%% API functions
%% ====================================================================

%% --------------------------------------
%%
%% --------------------------------------
-spec create_refer_replace_script(CallParam, #state_queue{}) -> {script, map()} when
      CallParam :: term(). %ответ от диалера
%% --------------------------------------
create_refer_replace_script(CallParam, #state_queue{hunt_block=HB, hunt_block_code=HBC}=State) ->
    {ReferToUri, ReplaceCallId, ReplaceToTag, ReplaceFromTag} =
        case ?QUEUE_DIAL:dial_get_replaced_info(CallParam, State) of
            false -> {<<"">>,<<"">>,<<"">>,<<"">>};
            Info -> Info
        end,
    {script, refer_script({ReferToUri,ReplaceCallId,ReplaceToTag,ReplaceFromTag,{HB,HBC}})}.

%% --------------------------------------
%%
%% --------------------------------------
-spec create_refer_script(Number::binary(), #state_queue{}) -> {script, map()}.
%% --------------------------------------
create_refer_script(Number, #state_queue{}=_State) ->
    {script, refer_script(Number)}.

%% --------------------------------------
%%
%% --------------------------------------
-spec script_replace({script, map()}, #state_queue{}) -> ok | {error, list()} .
%% --------------------------------------
script_replace(Script, #state_queue{scm_pid=Pid}=_State) ->
    ?APP_SCRIPT:sm_replace_script(Pid, Script).

%% --------------------------------------------------------------------
%% RP-893
%% --------------------------------------------------------------------
-spec create_hunt_queue_script(map(), binary(), binary()) -> {ok, map()}.
%% --------------------------------------------------------------------
create_hunt_queue_script(ItemHunt, FromUser, FromDN) ->
    {ok,
     #{
       checksum => 0,
       code => <<"ghost_queue_hunt">>,
       name => <<"ghost_queue_hunt">>,
       is_generated => true,
       id => <<"gggggggg-hhhh-oooo-ssss-tttttttttttt">>,
       opts =>
           #{<<"comment">> => <<>>,
             <<"loglevel">> => 3,
             <<"title">> => <<>>},
       projectid => <<"00000000-0000-0000-0000-000000000000">>,
       scriptdata => hunt_queue_scriptdata(ItemHunt, FromUser, FromDN)
      }
    }.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% --------------------------------------
%%
%% --------------------------------------
-spec refer_script(Param::tuple()|binary()) -> map().
%% --------------------------------------
refer_script(Param) ->
    #{scriptdata =>
          [
           {<<"objects">>,
            [
             default_component_start(),
             default_component_answer(?OIDRefer),
             %default_component_pause(),
             default_component_refer(Param),
             default_component_stop(1)
            ]}
          ]
     }.


%% --------------------------------------------------------------------
%% Hunt Queue
%% --------------------------------------------------------------------
hunt_queue_scriptdata(ItemHunt, FromUser, FromDN) ->
    WelcomeIvrCode = maps:get(wivrscript,ItemHunt),
    QueueIvrCode = maps:get(qivrscript,ItemHunt),
    %
    Objects = [default_component_start()] ++
              case WelcomeIvrCode of
                  <<>> -> [default_component_answer(?OIDPause)];
                  _ -> [default_component_answer(?OIDExecscriptW),
                        hunt_queue_component_execscript_w(WelcomeIvrCode)]
              end ++
              [default_component_pause(?OIDPause,?OIDGhost),
               hunt_queue_component_ghost(FromUser, FromDN, ItemHunt),
               default_component_pause(?OIDPause2,?OIDExecscriptQ),
               hunt_queue_component_execscript_q(QueueIvrCode),
               default_component_bye()],
    [{<<"objects">>, Objects}].


%% --------------------------------------
%%
%% --------------------------------------
default_component_start() ->
    [
     {<<"oId">>, ?OIDStart},
     {<<"oType">>,101},
     {<<"name">>,<<"start">>},
     {<<"transfer">>, ?OIDAnswer}
    ].

%% --------------------------------------
%%
%% --------------------------------------
default_component_answer(NextId) ->
    [
     {<<"oId">>,?OIDAnswer},
     {<<"oType">>,202},
     {<<"name">>,<<"answer200">>},
     {<<"sipCode">>,[{<<"value">>,<<"200">>},{<<"varType">>,1},{<<"argType">>,1}]},
     {<<"transfer">>, NextId}
    ].

%% --------------------------------------
%% RP-1454. Between ghost_queue_hunt components to avoid looping without pause. Incorrectly configured route, the call is looped on the hunt
%% --------------------------------------
default_component_pause(OId,NextId) ->
    [
     {<<"oId">>,OId},
     {<<"oType">>,103},
     {<<"name">>,<<"pause">>},
     {<<"timeoutMs">>,[{<<"value">>,<<"200">>},{<<"varType">>,1},{<<"argType">>,1}]},
     {<<"transfer">>,NextId}
    ].

%% --------------------------------------
%%
%% --------------------------------------
default_component_refer({ReferToUri,ReplaceCallId,ReplaceToTag,ReplaceFromTag,{HB,HBC}}) ->
    [
     {<<"oId">>,?OIDRefer},
     {<<"oType">>,215},
     {<<"name">>, <<"refer replace">>},
     {<<"referToUri">>, [{<<"value">>,ReferToUri},{<<"varType">>,2},{<<"argType">>,1}]},
     {<<"replacesCallId">>,[{<<"value">>,ReplaceCallId},{<<"varType">>,2},{<<"argType">>,1}]},
     {<<"replacesToTag">>,[{<<"value">>,ReplaceToTag},{<<"varType">>,2},{<<"argType">>,1}]},
     {<<"replacesFromTag">>,[{<<"value">>,ReplaceFromTag},{<<"varType">>,2},{<<"argType">>,1}]},
     {<<"huntblockenabled">>,[{<<"value">>,?EU:to_binary(HB)},{<<"varType">>,2},{<<"argType">>,1}]},
     {<<"huntblockcode">>,[{<<"value">>,?EU:to_binary(HBC)},{<<"varType">>,2},{<<"argType">>,1}]},
     {<<"transferSubTerminated">>,?OIDStop},
     {<<"transferTimeout">>,?OIDStop},
     {<<"transferRejected">>,?OIDStop}
    ];
default_component_refer(Number) ->
    [
     {<<"oId">>,?OIDRefer},
     {<<"oType">>,203},
     {<<"name">>, <<"refer">>},
     {<<"number">>, [{<<"value">>,Number},{<<"varType">>,2},{<<"argType">>,1}]},
     {<<"transferSubTerminated">>,?OIDStop},
     {<<"transferTimeout">>,?OIDStop},
     {<<"transferRejected">>,?OIDStop}
    ].

%% --------------------------------------
%%
%% --------------------------------------
default_component_bye() ->
    [
     {<<"oId">>,?OIDBye},
     {<<"oType">>,201},
     {<<"name">>,<<"bye">>}
    ].

%% --------------------------------------
%%
%% --------------------------------------
default_component_stop(CallStack) ->
    [
     {<<"oId">>,?OIDStop},
     {<<"oType">>,102},
     {<<"name">>,<<"stop">>},
     {<<"callStack">>,CallStack}
    ].

%% --------------------------------------
%%
%% --------------------------------------
hunt_queue_component_execscript_w(WelcomIvrCode) ->
    [
     {<<"oId">>, ?OIDExecscriptW},
     {<<"oType">>,107},
     {<<"name">>,<<"welcome script">>},
     {<<"startMode">>,0},
     {<<"scriptMode">>,1},
     {<<"scriptCodeStr">>,[{<<"argType">>,1},{<<"varType">>,2},{<<"value">>,WelcomIvrCode}]},
     {<<"takeOver">>,1},
     {<<"transfer">>, ?OIDPause},
     {<<"transferError">>, ?OIDBye}
    ].

%% --------------------------------------
%%
%% --------------------------------------
hunt_queue_component_execscript_q(QueueIvrCode) ->
    [
     {<<"oId">>, ?OIDExecscriptQ},
     {<<"oType">>,107},
     {<<"name">>,<<"queue script">>},
     {<<"startMode">>,0},
     {<<"scriptMode">>,1},
     {<<"scriptCodeStr">>,[{<<"argType">>,1},{<<"varType">>,2},{<<"value">>,QueueIvrCode}]},
     {<<"takeOver">>,1},
     {<<"transfer">>, ?OIDBye},
     {<<"transferError">>, ?OIDBye}
    ].

%% --------------------------------------
%%
%% --------------------------------------
hunt_queue_component_ghost(FromUser, FromDN, ItemHunt) ->
    HuntId = maps:get(id, ItemHunt),
    PreIvrCode = maps:get(preivrscript,ItemHunt),
    DisplayName = case maps:get(displayname,ItemHunt) of <<>> -> FromDN; DN -> DN end,
    Timeout = maps:get(resdialtimeout,ItemHunt),
    MaxAttNum = maps:get(maxattemptstrannumber,ItemHunt),
    TimeNum = maps:get(timeouttrannumber,ItemHunt),
    HuntBlock = maps:get(huntblock,ItemHunt,#{}),
    HuntBlockEnable = maps:get(<<"enabled">>, HuntBlock, <<"false">>),
    HuntBlockCode = maps:get(code, ItemHunt, <<>>),
    [
     {<<"oId">>, ?OIDGhost},
     {<<"oType">>,50001},
     {<<"name">>,<<"ghost">>},
     {<<"huntid">>,[{<<"argType">>,1},{<<"varType">>,2},{<<"value">>,HuntId}]},
     {<<"preivrscript">>,[{<<"argType">>,1},{<<"varType">>,2},{<<"value">>,PreIvrCode}]},
     {<<"fromuser">>,[{<<"argType">>,1},{<<"varType">>,2},{<<"value">>,FromUser}]},
     {<<"displayname">>,[{<<"argType">>,1},{<<"varType">>,2},{<<"value">>,DisplayName}]},
     {<<"huntblockenabled">>,[{<<"argType">>,1},{<<"varType">>,2},{<<"value">>,HuntBlockEnable}]},
     {<<"huntblockcode">>,[{<<"argType">>,1},{<<"varType">>,2},{<<"value">>,HuntBlockCode}]},
     {<<"maxattemptstrannumber">>,[{<<"argType">>,1},{<<"varType">>,2},{<<"value">>,MaxAttNum}]},
     {<<"timeouttrannumber">>,[{<<"argType">>,1},{<<"varType">>,2},{<<"value">>,TimeNum}]},
     {<<"resdialtimeout">>,[{<<"argType">>,1},{<<"varType">>,1},{<<"value">>,Timeout}]},
     {<<"transfer">>, ?OIDPause2},
     {<<"transferError">>, ?OIDBye}
    ].
