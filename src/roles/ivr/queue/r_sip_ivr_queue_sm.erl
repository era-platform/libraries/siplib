%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Pavel Abramov, Peter Bukashin <tbotc@yandex.ru>
%%% @date 24.11.2016
%%% @doc
%%% @todo

-module(r_sip_ivr_queue_sm).
-export([link/2, enqueue/2, waitqueue/2, call/2, play/2, refer/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip_ivr_queue.hrl").

%% ====================================================================
%% State link
%% ====================================================================

%% -----------------------------------------------
-spec link(Msg,State::#state_queue{}) -> {ok, UpdateState::#state_queue{}} | {stop, Reason::list()}
    when Msg :: {router_registered, IVRFSMPid::pid()} | scriptmachine_start | fsm_bye | fsm_error | scriptmachine_stop | scriptmachine_refer.
%% -----------------------------------------------
link({?Q_1_R_REG, IVRFSMPid}, #state_queue{scm_pid=undefined}=State) ->
    {ok, State#state_queue{ivr_fsm_pid=IVRFSMPid}};
link({?Q_1_R_REG, IVRFSMPid}, #state_queue{}=State) ->
    UpdateState = send_put_event(500,State#state_queue{ivr_fsm_pid=IVRFSMPid}),
    {ok, UpdateState#state_queue{state=enqueue}};
link({?Q_4_SCR_START, ScmPid}, #state_queue{ivr_fsm_pid=undefined}=State) ->
    erlang:monitor(process,ScmPid),
    {ok, State#state_queue{scm_pid=ScmPid}};
link({?Q_4_SCR_START, ScmPid}, #state_queue{}=State) ->
    erlang:monitor(process,ScmPid),
    UpdateState = send_put_event(500,State#state_queue{scm_pid=ScmPid}),
    {ok, UpdateState#state_queue{state=enqueue}};
link(?Q_2_FSM_BYE, #state_queue{}=_State) ->
    send_stop("fsm_bye");
link(?Q_3_FSM_ERROR, #state_queue{}=_State) ->
    send_stop("fsm_error");
link(?Q_5_SCR_STOP, #state_queue{}=_State) ->
    send_stop("scriptmachine_stop");
link(?Q_6_SCR_REFER, #state_queue{}=_State) ->
    send_stop("scriptmachine_refer");
link(cancel_resources, State) -> {ok, State}.

%% ====================================================================
%% State enqueue
%% ====================================================================

%% -----------------------------------------------
-spec enqueue(Msg,State::#state_queue{}) -> {ok, UpdateState::#state_queue{}} | {stop, Reason::list()}
    when Msg :: fsm_bye | fsm_error | scriptmachine_stop | scriptmachine_refer | {huntq_report, Report} | huntq_error | huntq_put,
         Report :: {ok, Result::binary()} | {error, Reason::list()}.
%% -----------------------------------------------
enqueue(?Q_2_FSM_BYE, #state_queue{}=State) ->
    ?QUEUE_HUNTQ:huntq_remove_from_queue(State),
    ?EVENT:queue_dequeue(#{<<"result">> => <<"error">>, <<"reason">> => <<"fsm_bye">>}, State),
    send_stop("fsm_bye");
enqueue(?Q_3_FSM_ERROR, #state_queue{}=State) ->
    ?QUEUE_HUNTQ:huntq_remove_from_queue(State),
    ?EVENT:queue_dequeue(#{<<"result">> => <<"error">>, <<"reason">> => <<"fsm_error">>}, State),
    send_stop("fsm_error");
enqueue(?Q_5_SCR_STOP, #state_queue{}=State) ->
    ?QUEUE_HUNTQ:huntq_remove_from_queue(State),
    ?EVENT:queue_dequeue(#{<<"result">> => <<"error">>, <<"reason">> => <<"scriptmachine_stop">>}, State),
    send_stop("scriptmachine_stop");
enqueue(?Q_6_SCR_REFER, #state_queue{}=State) ->
    ?QUEUE_HUNTQ:huntq_remove_from_queue(State),
    ?EVENT:queue_dequeue(#{<<"result">> => <<"error">>, <<"reason">> => <<"scriptmachine_refer">>}, State),
    send_stop("scriptmachine_refer");
enqueue({?Q_7_HUNT_REPORT, {error, Reason}}, #state_queue{}=State) ->
    send_bye(State),
    ?QUEUE_HUNTQ:huntq_remove_from_queue(State),
    ?EVENT:queue_dequeue(#{<<"result">> => <<"error">>, <<"reason">> => ?EU:to_binary(Reason)}, State),
    send_stop(Reason);
enqueue({?Q_7_HUNT_REPORT, {ok, _Result}}, #state_queue{}=State) ->
    {ok, State#state_queue{state=waitqueue}};
enqueue({?Q_15_HUNT_PUT, _Ref}, #state_queue{}=State) ->
    case ?QUEUE_HUNTQ:huntq_put_in_queue(State) of
        accepted ->
            send_event_put(State),
            {ok, State#state_queue{state=waitqueue}};
        empty_resources ->
            send_bye(State),
            send_stop("empty_resources");
        empty_resources_still_wait ->
            ?LOG("~p -- put in queue hunt return : empty_resources_still_wait", [?MODULE]),
            {ok, send_put_event(3000, State)};
        error -> {ok, send_put_event(3000, State)}
    end;
enqueue({?Q_8_HUNT_ERROR, Reason}, #state_queue{}=State) ->
    hunt_error(Reason, State);
enqueue({?Q_16_HUNT_EVENT, Reason}, #state_queue{}=State) ->
    hunt_event(Reason, State);
enqueue(cancel_resources, State) -> {ok, State}.

%% ====================================================================
%% State waitqueue
%% ====================================================================

%% -----------------------------------------------
-spec waitqueue(Msg,State::#state_queue{}) -> {ok, UpdateState::#state_queue{}} | {stop, Reason::list()}
    when Msg :: fsm_bye | fsm_error | scriptmachine_stop | scriptmachine_refer | huntq_error | {apply_new_pair, UserId::binary()}.
%% -----------------------------------------------
waitqueue(?Q_2_FSM_BYE, #state_queue{}=State) ->
    ?QUEUE_HUNTQ:huntq_remove_from_queue(State),
    ?EVENT:queue_dequeue(#{<<"result">> => <<"error">>, <<"reason">> => <<"fsm_bye">>}, State),
    send_stop("fsm_bye");
waitqueue(?Q_3_FSM_ERROR, #state_queue{}=State) ->
    ?QUEUE_HUNTQ:huntq_remove_from_queue(State),
    ?EVENT:queue_dequeue(#{<<"result">> => <<"error">>, <<"reason">> => <<"fsm_error">>}, State),
    send_stop("fsm_error");
waitqueue(?Q_5_SCR_STOP, #state_queue{}=State) ->
    ?QUEUE_HUNTQ:huntq_remove_from_queue(State),
    ?EVENT:queue_dequeue(#{<<"result">> => <<"error">>, <<"reason">> => <<"scriptmachine_stop">>}, State),
    send_stop("scriptmachine_stop");
waitqueue(?Q_6_SCR_REFER, #state_queue{}=State) ->
    ?QUEUE_HUNTQ:huntq_remove_from_queue(State),
    ?EVENT:queue_dequeue(#{<<"result">> => <<"error">>, <<"reason">> => <<"scriptmachine_refer">>}, State),
    send_stop("scriptmachine_refer");
waitqueue({?Q_8_HUNT_ERROR, Reason}, #state_queue{}=State) ->
    hunt_error(Reason, State);
waitqueue({?Q_9_HUNT_RESERVED, UserId}, #state_queue{}=State) ->
    {ok, UpdateState} = dial_call(UserId, State),
    ?EVENT:queue_start_call(#{<<"userid">> => UserId}, State),
    case get_count_call(UpdateState) of
        0 -> {ok, UpdateState};
        _ -> {ok, UpdateState#state_queue{state=call}}
    end;
waitqueue({?Q_16_HUNT_EVENT, Reason}, #state_queue{}=State) ->
    hunt_event(Reason, State);
waitqueue(_Msg, State) ->
    {ok, State}.

%% ====================================================================
%% State call
%% ====================================================================

%% -----------------------------------------------
-spec call(Msg,State::#state_queue{}) -> {ok, UpdateState::#state_queue{}} | {stop, Reason::list()}
    when Msg :: fsm_bye | fsm_error | scriptmachine_stop | scriptmachine_refer |    huntq_error | {apply_new_pair, UserId::binary()} |
             {dial_response, Response::term()} | {dial_call_error, ErrorParam::term()} | {dial_error, DownParam::term()}.
%% -----------------------------------------------
call(?Q_2_FSM_BYE, #state_queue{}=State) ->
    ?QUEUE_HUNTQ:huntq_remove_from_queue(State),
    ?EVENT:queue_dequeue(#{<<"result">> => <<"error">>, <<"reason">> => <<"fsm_bye">>}, State),
    ?QUEUE_DIAL:dial_clear_calls(State),
    send_stop("fsm_bye");
call(?Q_3_FSM_ERROR, #state_queue{}=State) ->
    ?QUEUE_HUNTQ:huntq_remove_from_queue(State),
    ?EVENT:queue_dequeue(#{<<"result">> => <<"error">>, <<"reason">> => <<"fsm_error">>}, State),
    ?QUEUE_DIAL:dial_clear_calls(State),
    send_stop("fsm_error");
call(?Q_5_SCR_STOP, #state_queue{}=State) ->
    ?QUEUE_HUNTQ:huntq_remove_from_queue(State),
    ?EVENT:queue_dequeue(#{<<"result">> => <<"error">>, <<"reason">> => <<"scriptmachine_stop">>}, State),
    ?QUEUE_DIAL:dial_clear_calls(State),
    send_stop("scriptmachine_stop");
call(?Q_6_SCR_REFER, #state_queue{}=State) ->
    ?QUEUE_HUNTQ:huntq_remove_from_queue(State),
    ?EVENT:queue_dequeue(#{<<"result">> => <<"error">>, <<"reason">> => <<"scriptmachine_refer">>}, State),
    ?QUEUE_DIAL:dial_clear_calls(State),
    send_stop("scriptmachine_refer");
call({?Q_9_HUNT_RESERVED, UserId}, #state_queue{}=State) ->
    dial_call(UserId, State);
call({?Q_10_11_DIAL_RESPONSE, Response}, #state_queue{}=State) ->
    Code = ?QUEUE_DIAL:dial_get_response_code(Response),
    case Code of
        C when (C >= 200) and (C < 300) -> dial_response_2xx(Response, State);
        _ -> {ok, State}
    end;
call({?Q_13_DIAL_CALL_ERROR, Param}, #state_queue{}=State) ->
    dial_call_error(Param, State);
call({?Q_14_DIAL_ERROR, Param}, #state_queue{}=State) ->
    dial_call_error(Param, State);
call({?Q_8_HUNT_ERROR, Reason}, #state_queue{}=State) ->
    hunt_error(Reason, State);
call({?Q_16_HUNT_EVENT, Reason}, #state_queue{}=State) ->
    hunt_event(Reason, State);
call(cancel_resources, #state_queue{}=State) ->
    dial_cancel(State);
call(_Msg, #state_queue{}=State) ->
    {ok, State}.

%% ====================================================================
%% State play
%% ====================================================================

%% -----------------------------------------------
-spec play(Msg,State::#state_queue{}) -> {ok, UpdateState::#state_queue{}} | {stop, Reason::list()}
    when Msg :: fsm_bye | fsm_error | scriptmachine_stop | scriptmachine_refer | huntq_error |
             {dial_call_successful, CallParam::term()} | {dial_error, DownParam::term()}.
%% -----------------------------------------------
play(?Q_2_FSM_BYE, #state_queue{}=State) ->
    ?QUEUE_HUNTQ:huntq_remove_from_queue(State),
    ?EVENT:queue_dequeue(#{<<"result">> => <<"error">>, <<"reason">> => <<"fsm_bye">>}, State),
    ?QUEUE_DIAL:dial_clear_calls(State),
    send_stop("fsm_bye");
play(?Q_3_FSM_ERROR, #state_queue{}=State) ->
    ?QUEUE_HUNTQ:huntq_remove_from_queue(State),
    ?EVENT:queue_dequeue(#{<<"result">> => <<"error">>, <<"reason">> => <<"fsm_error">>}, State),
    ?QUEUE_DIAL:dial_clear_calls(State),
    send_stop("fsm_error");
play(?Q_5_SCR_STOP, #state_queue{}=State) ->
    ?QUEUE_HUNTQ:huntq_remove_from_queue(State),
    ?EVENT:queue_dequeue(#{<<"result">> => <<"error">>, <<"reason">> => <<"scriptmachine_stop">>}, State),
    ?QUEUE_DIAL:dial_clear_calls(State),
    send_stop("scriptmachine_stop");
play(?Q_6_SCR_REFER, #state_queue{}=State) ->
    ?QUEUE_HUNTQ:huntq_remove_from_queue(State),
    ?EVENT:queue_dequeue(#{<<"result">> => <<"error">>, <<"reason">> => <<"scriptmachine_refer">>}, State),
    ?QUEUE_DIAL:dial_clear_calls(State),
    send_stop("scriptmachine_refer");
play({?Q_12_DIAL_SUCCESSFUL, CallParam}, #state_queue{}=State) ->
    Script = ?QUEUE_SCRIPT:create_refer_replace_script(CallParam, State),
    replace_script_on_refer(Script, State);
play({?Q_14_DIAL_ERROR, DownParam}, #state_queue{}=State) ->
    dial_call_error(DownParam, State);
play({?Q_8_HUNT_ERROR, Reason}, #state_queue{}=State) ->
    hunt_error(Reason, State);
play({?Q_16_HUNT_EVENT, Reason}, #state_queue{}=State) ->
    hunt_event(Reason, State);
play(_Msg, #state_queue{}=State) ->
    {ok, State}.

%% ====================================================================
%% State refer
%% ====================================================================

%% -----------------------------------------------
-spec refer(Msg,State::#state_queue{}) -> {ok, UpdateState::#state_queue{}} | {stop, Reason::list()}
    when Msg :: fsm_bye | fsm_error | scriptmachine_stop | scriptmachine_refer | huntq_error.
%% -----------------------------------------------
refer(?Q_2_FSM_BYE, #state_queue{}=State) ->
    ?QUEUE_HUNTQ:huntq_remove_from_queue(State),
    ?QUEUE_DIAL:dial_clear_calls(State),
    send_stop("fsm_bye");
refer(?Q_3_FSM_ERROR, #state_queue{}=State) ->
    ?QUEUE_HUNTQ:huntq_remove_from_queue(State),
    ?QUEUE_DIAL:dial_clear_calls(State),
    send_stop("fsm_error");
refer(?Q_5_SCR_STOP, #state_queue{}=State) ->
    ?QUEUE_HUNTQ:huntq_remove_from_queue(State),
    ?QUEUE_DIAL:dial_clear_calls(State),
    send_stop("scriptmachine_stop");
refer({?Q_14_DIAL_ERROR, _Msg}, #state_queue{}=State) ->
    ?QUEUE_DIAL:dial_clear_calls(State),
    send_stop("down call");
refer(_Msg, #state_queue{}=State) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -----------------------------------------------
%% make call to resource.
%% Switch to state 'call' only if call INVITE was sent and it is the first INVITE of this queue element (could be multiple calls).
%% If something wrong then resource should be banned
%% @TODO Возможен постоянный бан если диалер не работает.
%% -----------------------------------------------
-spec dial_call(UserId::binary(), State::#state_queue{}) -> {ok, State::#state_queue{}}.
%% -----------------------------------------------
dial_call(UserId, #state_queue{}=State) ->
    case ?QUEUE_DIAL:dial_call(UserId, State) of
        {ok, UpdateState} -> {ok, UpdateState};
        {error, _Reason} ->
            ?QUEUE_HUNTQ:huntq_user_ban(UserId, State),
            {ok, State}
    end.

%% -----------------------------------------------
%%
%% -----------------------------------------------
-spec get_count_call(State::#state_queue{}) -> Count::non_neg_integer().
%% -----------------------------------------------
get_count_call(#state_queue{resourses=Resourses}=_State) ->
    erlang:length(Resourses).

%% -----------------------------------------------
%% handle 2хх answer.
%% Switch to 'play' state if anything is ok.
%% @TODO Может не найти юзера, тогда остаемся в том же состоянии.
%% @TODO Может не остановить остальных юзеров, тогда остаемся в том же состоянии.
%% -----------------------------------------------
-spec dial_response_2xx(Response, State::#state_queue{}) -> {ok, UpdateState::#state_queue{}}
    when Response :: term(). % терм от диалера
%% -----------------------------------------------
dial_response_2xx(Response, #state_queue{}=State) ->
    case ?QUEUE_DIAL:dial_get_user_id_from_response(Response) of
        <<>> -> {ok, State};
        UserId ->
            ?EVENT:queue_preivr_start(#{<<"userid">> => UserId}, State),
            ?QUEUE_DIAL:dial_stop_other_calls(UserId, State),
            UpdateState = ?QUEUE_HUNTQ:huntq_unsubscribe_other_resourse(UserId, State),
            {ok, UpdateState#state_queue{state=play}}
    end.

%% -----------------------------------------------
%%
%% -----------------------------------------------
-spec dial_call_error(Param::term(), State::#state_queue{}) -> {ok, UpdateState::#state_queue{}}.
%% -----------------------------------------------
dial_call_error(Param, #state_queue{}=State) ->
    case ?QUEUE_DIAL:dial_call_error(Param, State) of
        false -> {ok, State};
        {UserId, UpdateState} ->
            ?QUEUE_HUNTQ:huntq_user_ban(UserId, UpdateState),
            case get_count_call(UpdateState) of
                0 -> {ok, UpdateState#state_queue{state=waitqueue}};
                _ -> {ok, UpdateState}
            end
    end.

%% -----------------------------------------------
%%
%% -----------------------------------------------
dial_cancel(State) ->
    ?QUEUE_DIAL:dial_cancel(State),
    {ok,State}.

%% -----------------------------------------------
%%
%% -----------------------------------------------
replace_script_on_refer(Script, State) ->
    ?QUEUE_HUNTQ:huntq_remove_from_queue(State),
    F = fun(Res, Rea) -> ?EVENT:queue_dequeue(#{<<"result">> => Res, <<"reason">> => Rea}, State) end,
    Result =
        case ?QUEUE_SCRIPT:script_replace(Script, State) of
            ok ->
                F(<<"ok">>, <<"script replace">>),
                {ok, State#state_queue{state=refer}};
            _ ->
                ?QUEUE_DIAL:dial_clear_calls(State),
                F(<<"error">>, <<"script replace fail">>),
                send_stop("script replace fail")
        end,
    ?EVENT:queue_quit(#{}, State),
    Result.

%% -----------------------------------------------
%% handle huntq error
%% -----------------------------------------------
hunt_error(max_waittime_reached, #state_queue{timeouttrannumber = <<>>}=State) ->
    hunt_error("max_waittime_reached", State);
hunt_error(max_waittime_reached, #state_queue{timeouttrannumber=Num}=State) ->
    transfer(Num,State);
hunt_error(max_attempt_reached, #state_queue{maxattemptstrannumber = <<>>}=State) ->
    hunt_error("max_attempt_reached", State);
hunt_error(max_attempt_reached, #state_queue{maxattemptstrannumber=Num}=State) ->
    transfer(Num,State);
hunt_error(Reason, State) when is_list(Reason)->
    ?EVENT:queue_dequeue(#{<<"result">> => <<"error">>, <<"reason">> => ?EU:to_binary(Reason)}, State),
    send_bye(State),
    send_stop(Reason);
hunt_error(Reason, State) ->
    ?LOG("~p -- Hunt error :~120p", [?MODULE, Reason]),
    ?EVENT:queue_dequeue(#{<<"result">> => <<"error">>, <<"reason">> => <<"hunt_error">>}, State),
    send_bye(State),
    send_stop("hunt_error").

%% @private
transfer(Num, State) ->
    ?QUEUE_DIAL:dial_clear_calls(State),
    UpSatate = ?QUEUE_HUNTQ:huntq_remove_from_queue(State),
    ?LOG("~p -- refer to number ~120p", [?MODULE, Num]),
    Script = ?QUEUE_SCRIPT:create_refer_script(Num, UpSatate),
    replace_script_on_refer(Script, UpSatate).

%% -----------------------------------------------
%% handle huntq event
%% -----------------------------------------------
hunt_event('empty_resources_still_wait'=Reason, State) ->
    ?LOG("~p -- Hunt event :~120p", [?MODULE, Reason]),
    {ok,State};
hunt_event(Reason, State) ->
    ?LOG("~p -- Hunt event :~120p", [?MODULE, Reason]),
    {ok,State}.

%% -----------------------------------------------
%% send event to self pid to make new attempt of enqueueing in huntq
%% -----------------------------------------------
-spec send_put_event(Time::non_neg_integer(), State::#state_queue{}) -> UpdateState::#state_queue{}.
%% -----------------------------------------------
send_put_event(Time, #state_queue{}=State) ->
    Ref = erlang:make_ref(),
    erlang:send_after(Time, self(), {?Q_15_HUNT_PUT, Ref}),
    State#state_queue{reput_huntq_ref=Ref}.

%% -----------------------------------------------
send_bye(#state_queue{ivr_fsm_pid=Pid}=_State) -> ?QUEUE_SRV:send_bye(Pid).

%% -----------------------------------------------
send_stop(Reason) -> {stop, Reason}.

%% -----------------------------------------------
send_event_put(#state_queue{scm_pid=Pid, current_queue_id=QId}=_State) ->
    ?QUEUE_SRV:send_event(Pid, {put, QId}).
