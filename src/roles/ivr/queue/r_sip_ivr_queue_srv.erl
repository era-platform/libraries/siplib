%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Pavel Abramov, Peter Bukashin <tbotc@yandex.ru>
%%% @date 24.11.2016
%%% @doc

-module(r_sip_ivr_queue_srv).

-behaviour(gen_server).

-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).
-export([send_bye/1, send_event/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip_ivr_queue.hrl").

-define(DOUT(Text), ok).
-define(DOUT(Fmt,Args), ok).
%% -define(DOUT(Fmt,Args), ?OUT(Fmt,Args)).
%% -define(DOUT(Text), ?OUT(Text)).

%% ====================================================================
%% Behavioural functions
%% ====================================================================

%% ---------------------------------
init(Opts) ->
    {Block, Code} = maps:get(huntblock,Opts),
    {ok, #state_queue{site = ?CFG:get_current_site(),
                      domain = maps:get(domain,Opts),
                      huntq_id = maps:get(huntid,Opts),
                      from_user = maps:get(from_user,Opts),
                      from_dn = maps:get(from_dn,Opts),
                      remote_ivr_script_code = maps:get(remote_script,Opts),
                      current_queue_id = maps:get(element_id,Opts),
                      maxattemptstrannumber = maps:get(maxattemptstrannumber,Opts),
                      timeouttrannumber = maps:get(timeouttrannumber,Opts),
                      resdialtimeout = maps:get(resdialtimeout,Opts) * 1000,
                      hunt_block = Block,
                      hunt_block_code = Code,
                      xinfo = maps:get(xinfo,Opts),
                      parent_functions = maps:get(parent_functions,Opts)}}.

%% -----------------------------------------------
handle_call(?Q_GET_STATE, _From, State) ->
    ?DOUT("call GET_STATE: ~p",[State]),
    {reply, State, State};

handle_call({get_xinfo, _Objid}, _From,  #state_queue{xinfo=XInfo}=State) ->
    ?DOUT("call get_xinfo: ~p",[_Objid]),
    {reply, XInfo, State};
handle_call(_Request, _From, State) ->
    ?DOUT("call Request: ~p",[_Request]),
    Reply = ok,
    {reply, Reply, State}.

%% -----------------------------------------------
handle_cast(Msg, #state_queue{state=S}=State) ->
    ?DOUT("cast S:~p, Msg: ~p",[S,Msg]),
    result(Msg, ?QUEUE_SM:S(Msg,State), State).

%% -----------------------------------------------
handle_info({?Q_15_HUNT_PUT, Ref}=Msg, #state_queue{reput_huntq_ref=Ref,state=S}=State) ->
    ?DOUT("info: ~p:~p",[S,Msg]),
    result(Msg, ?QUEUE_SM:S(Msg,State#state_queue{reput_huntq_ref=undefined}), State);

handle_info({huntq_resp,?Q_9_HUNT_RESERVED, {{sip,<<"undefined">>}, UserId}, _}=Msg, #state_queue{state=S}=State) ->
    ?DOUT("info: ~p:~p",[S,Msg]),
    result(Msg, ?QUEUE_SM:S({?Q_9_HUNT_RESERVED, UserId}, State), State);

%% TODO
handle_info({huntq_event, object_deleted}=Msg, #state_queue{state=S}=State) ->
    ?DOUT("info: ~p:~p",[S,Msg]),
    result(Msg, ?QUEUE_SM:S(?Q_8_HUNT_ERROR, State), State);

handle_info({'DOWN', _Ref, process, Pid, _Result}=Msg, #state_queue{state=S, scm_pid=Pid}=State) ->
    ?DOUT("info: ~p:~p",[S,Msg]),
    result(Msg, ?QUEUE_SM:S(?Q_3_FSM_ERROR, State), State);

handle_info({'DOWN', _Ref, process, _Pid, _Result}=Msg, #state_queue{state=S}=State) ->
    ?DOUT("info 2: ~p:~p",[S,Msg]),
    result(Msg, ?QUEUE_SM:S({?Q_14_DIAL_ERROR, Msg}, State), State);

handle_info({?Q_8_HUNT_ERROR, _Reason}=Msg, #state_queue{state=S}=State) ->
    ?DOUT("info: ~p:~p",[S,Msg]),
    ?EVENT:queue_dequeue(#{<<"result">> => <<"error">>, <<"reason">> => ?EU:to_binary(?Q_8_HUNT_ERROR)}, State),
    result(Msg, ?QUEUE_SM:S(Msg, State), State);

handle_info({?Q_16_HUNT_EVENT, _Reason}=Msg, #state_queue{state=S}=State) ->
    ?DOUT("info: ~p:~p",[S,Msg]),
    result(Msg, ?QUEUE_SM:S(Msg, State), State);

handle_info({resdialtimeout, _RefDialer}=Msg, #state_queue{state=S}=State) ->
    ?DOUT("info: ~p:~p",[S,Msg]),
    result(Msg, ?QUEUE_SM:S({?Q_13_DIAL_CALL_ERROR, Msg}, State), State);

handle_info(cancel_resources=Msg, #state_queue{state=S}=State) ->
    result(Msg, ?QUEUE_SM:S(Msg, State), State);

handle_info(_Info, State) ->
    ?DOUT("info Msg: ~p",[_Info]),
    {noreply, State}.

%% -----------------------------------------------
terminate(_Reason, State) ->
    ?DOUT("terminate: ~p",[_Reason]),
    ?QUEUE_HUNTQ:terminate(State),
    ?QUEUE_DIAL:terminate(State).

%% -----------------------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Events
%% ====================================================================

send_bye(undefined) -> ok;
send_bye(PidFSM) ->
    ?ENVCROSS:call_node(node(), {?IVR_SRV, stop, [PidFSM, stop_msg()]}, error, 3000).

%% --------------------------------------
%%
%% --------------------------------------
send_event(undefined, _) -> ok;
send_event(PidSCM, Event) ->
    ?SCR_COMP:event(PidSCM, Event).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -----------------------------------------------
result({?Q_1_R_REG, IVRFSMPid}, {ok, UpdateState}, _State) ->
    F = fun() -> send_bye(IVRFSMPid) end,
    ?ENVMON:start_monitor(self(),[F]),
    {noreply, UpdateState};
result(_Event, {ok, UpdateState}, _State) ->
    {noreply, UpdateState};
result(Event, {stop, Reason}, #state_queue{state=S}=State) ->
    Msg = stop_sm("handle_cast", S, Event, Reason),
    {stop, Msg, State}.

%% @private
stop_sm(_Fun, _S, _Event, _Error) -> stop_msg().

%% @private
stop_msg() -> {shutdown,"QUEUE FSM was stopped"}.
