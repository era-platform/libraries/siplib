%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc
%%% @todo

-module(r_sip_ivr_cb).
-author('Peter Bukashin <tbotc@yandex.ru>').

%% -------------------------------------------------------------------
%%
%% -------------------------------------------------------------------

-export([start/0, stop/0, start_by_mode/1,
         handle_call/3, handle_cast/2, handle_info/2,
         %
         init/1,
         %sip_get_user_pass/4, sip_authorize/3,
         sip_route/5,
         % sip_register/2, sip_update/2,
         sip_invite/2, sip_reinvite/2, sip_cancel/3, sip_ack/2, sip_bye/2, sip_refer/2,
         % sip_subscribe/2, sip_resubscribe/2, sip_publish/2,
         sip_notify/2,
         sip_info/2, sip_message/2, sip_options/2,
         sip_dialog_update/3,
         sip_session_update/3]).

-compile([export_all,nowarn_export_all]).

%% ==========================================================================
%% Types
%% ==========================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_ivr.hrl").
-include("../include/r_sip_mgc.hrl").

% -include("../include/r_nksip.hrl").

%% ==========================================================================
%% API functions
%% ==========================================================================

%% --------------------------------
%% @doc Starts a new SipApp, listening on port 5060 for udp and tcp and 5061 for tls,
%% and acting as a registrar.
%%
start() ->
    {ok,AppOpts} = ?APP:get_opts(?MODULE),
    ?SIPSTORE:store_u('local_contact_port', ?U:parse_contact_port(AppOpts)),
    ?SIPSTORE:store_u('local_ports', ?U:parse_ports(AppOpts)),
    %
    Plugins = [r_plug_b2bua,
               r_plug_localdomain,
               r_plug_bestinterface,
               r_plug_filter,
               r_plug_log,
               %
               nksip_uac_auto_auth
              ],
    CoreOpts = #{plugins => lists:filter(fun(false) -> false; (_) -> true end, Plugins),
                certfile => <<"">>, %certfile => code:priv_dir(era_sip) ++ "/ssl/server.crt",
                keyfile => <<"">>, %keyfile => code:priv_dir(era_sip) ++ "/ssl/server.key",
                %{sip_allow, "INVITE, ACK, CANCEL, BYE, REFER, NOTIFY, OPTIONS, INFO, MESSAGE", % REGISTER, SUBSCRIBE, PUBLISH, UPDATE
                sip_no_100 => not ?Auto100answer, % test
                transports => ?U:parse_transports(AppOpts)},
    nksip:start_link(?MODULE, CoreOpts),
    %
    ?ENV:set_env('sipapp', ?SIPAPP),
    ?ENV:set_env('sipappmodule', ?IVR),
    %
    ?SUPV:start_child({?IVR_SUPV, {?IVR_SUPV, start_link, []}, permanent, 1000, supervisor, [?IVR_SUPV]}).

%% --------------------------------
%% @doc Start call to number
%%
start_by_mode(#{}=Map) ->
    case maps:get(mode,Map) of
        <<"1">> ->
            % mode, args
            ?IVR_MODE_ROUTER:start_by_mode(Map);
        _ -> {error,invalid_params}
    end.

%% --------------------------------
%% @doc Stops the SipApp.
%%
stop() ->
    ?SUPV:terminate_child(?IVR_SUPV),
    ?SUPV:delete_child(?IVR_SUPV),
    nksip:stop(?SIPAPP).

%% --------------------------------
%% @doc Updates role configuration opts
%%
update_opts(Opts) ->
    ?U:check_update_opts(Opts).

%% ==================================================================================
%% Callback functions
%% ==================================================================================

%% ----------------------------------------------------------------------------------
%% @doc SipApp Callback: Initialization.
%% ----------------------------------------------------------------------------------

init([]) ->
    %erlang:start_timer(?TIME_CHECK, self(), check_speed),
    %p:put(?SIPAPP, speed, []),
    {ok, #state{auto_check=false}}.


%% ----------------------------------------------------------------------------------
%% @doc SipApp Callback: Called to check user's password.
%% ----------------------------------------------------------------------------------

%% sip_get_user_pass(User, Realm, _Req, _Call) ->
%%     ?ACCOUNTS:get_password_lazy(User, Realm).

%% ----------------------------------------------------------------------------------
%% @doc SipApp Callback: Called to check if a request should be authorized.
%% ----------------------------------------------------------------------------------

%% sip_authorize(Auth, Req, Call) ->
%%     ?AUTH:sip_authorize(Auth, Req, Call).

%% ----------------------------------------------------------------------------------
%% @doc SipApp Callback: Called to decide how to route every new request.
%% ----------------------------------------------------------------------------------

% The request is for one of our users or a SUBSCRIBE
sip_route(_Scheme, _User, <<"224.0.1.75">>, _Req, _Call) -> block; % auto provision broadcast
sip_route(Scheme, User, Domain, Req, Call) ->
    #call{call_id=CallId}=Call,
    #sipmsg{cseq={_,Method}}=Req,
    % -------
    ?U:start_monitor_simple(self(), "Sip ivr call"),
    % -------
    ?LOGSIP("sip_route ~120p, ~120p, ~120p", [Method, self(), CallId]),
    ?STAT_FACADE:incoming_request(Req), % @stattrace
    case ?U:is_local_ruri(Req) of
        false -> route_outside_domain(Scheme, User, Domain, Req, Call);
        true -> route_inside_domain(Scheme, User, Domain, Req, Call)
    end.

%% ----------------------
route_outside_domain(_Scheme, _UserR, _DomainR, Req, _Call) ->
    Method = nksip_request:method(Req),
    case Method of
        {ok, 'REGISTER'} -> ?FILTER:check_block_response_malware(method_not_allowed, Req, {reply, ?Forbidden("IVR. Router disabled")});
        {ok, 'SUBSCRIBE'} -> ?FILTER:check_block_response_malware(method_not_allowed, Req, {reply, ?Forbidden("IVR. Router disabled")});
        {ok, 'PUBLISH'} -> ?FILTER:check_block_response_malware(method_not_allowed, Req, {reply, ?Forbidden("IVR. Router disabled")});
        {ok, 'UPDATE'} -> ?FILTER:check_block_response_malware(method_not_allowed, Req, {reply, ?Forbidden("IVR. Router disabled")});

        {ok, 'INVITE'} -> process;
        {ok, 'REFER'} -> process;
        {ok, 'NOTIFY'} -> process;

        {ok, 'INFO'} -> process;
        {ok, 'MESSAGE'} -> process;
        {ok, 'ACK'} -> process;
        {ok, 'BYE'} -> process;
        {ok, 'CANCEL'} -> process;
        {ok, 'OPTIONS'} -> process;

        _ -> process
    end.

%% ----------------------
route_inside_domain(_Scheme, _User, _Domain, Req, _Call) ->
    Method = nksip_request:method(Req),
    case Method of
        {ok, 'REGISTER'} -> ?FILTER:check_block_response_malware(method_not_allowed, Req, {reply, ?MethodNotAllowed("IVR")});
        % ----------------
        {ok, 'INVITE'} -> process;
        {ok, 'CANCEL'} -> process;
        {ok, 'ACK'} -> process;
        {ok, 'BYE'} -> process;
        % ----------------
        {ok, 'REFER'} -> process;
        % ----------------
        {ok, 'NOTIFY'} -> process;
        {ok, 'SUBSCRIBE'} -> ?FILTER:check_block_response_malware(method_not_allowed, Req, {reply, ?MethodNotAllowed("IVR")});
        {ok, 'PUBLISH'} -> ?FILTER:check_block_response_malware(method_not_allowed, Req, {reply, ?MethodNotAllowed("IVR")});
        % ----------------
        {ok, 'OPTIONS'} -> process;
        {ok, 'INFO'} ->    process;
        {ok, 'MESSAGE'} -> process;
        % ----------------
        {ok, 'UPDATE'} -> ?FILTER:check_block_response_malware(method_not_allowed, Req, {reply, ?MethodNotAllowed("IVR")});
        % ----------------
        {ok, _M} ->    process
    end.

%% ----------------------------------------------------------------------------------
%% dialog_update
%% ----------------------------------------------------------------------------------

-spec(sip_dialog_update(DialogStatus, Dialog::nksip:dialog(), Call::nksip:call()) ->
        ok
        when DialogStatus :: start | target_update | stop |
                         {invite_status, nksip_dialog:invite_status() | {stop, nksip_dialog:stop_reason()}} |
                         {invite_refresh, SDP::nksip_sdp:sdp()} |
                         {subscription_status, nksip_subscription:status(), nksip:subscription()}).

sip_dialog_update(_, _Dialog, _Call) ->
    ok.

%% ----------------------------------------------------------------------------------
%% session_update
%% ----------------------------------------------------------------------------------
-spec(sip_session_update(SessionStatus, Dialog::nksip:dialog(), Call::nksip:call()) ->
        ok
        when SessionStatus :: {start, Local, Remote} | {update, Local, Remote} | stop,
                          Local::nksip_sdp:sdp(), Remote::nksip_sdp:sdp()).

sip_session_update(_, _Dialog, _Call) ->
    ok.

%% ----------------------------------------------------------------------------------
%% INVITE
%% ----------------------------------------------------------------------------------
-spec(sip_invite(Request::nksip:request(), Call::nksip:call()) ->
        {reply, nksip:sipreply()} | noreply).

sip_invite(Req, _Call) ->
    #sipmsg{call_id=CallId}=Req,
    AOR = extract_ruri(Req),
    ?LOGSIP("sip_invite ~1000tp, ~1000tp, ~1000tp", [self(), CallId, AOR]),
    case ?U:extract_sdp(Req) of
        undefined ->
            ?FILTER:check_block_response_malware(no_sdp, Req, {reply, ?MediaNotSupported("IVR. No SDP")});
        #sdp{} ->
            ?U:send_100_trying(Req),
            ?IVR_ROUTER:sip_invite(AOR,Req)
    end.

%% ----------------------------------------------------------------------------------
%% reINVITE
%% ----------------------------------------------------------------------------------
-spec(sip_reinvite(Request::nksip:request(), Call::nksip:call()) ->
        {reply, nksip:sipreply()} | noreply).

sip_reinvite(Req, _Call) ->
    #sipmsg{call_id=CallId}=Req,
    AOR = extract_ruri(Req),
    ?LOGSIP("sip_reinvite ~80p, ~p, ~120p", [AOR, self(), CallId]),
    case ?U:extract_sdp(Req) of
        undefined -> ?FILTER:check_block_response_malware(no_sdp, Req, {reply, ?MediaNotSupported("IVR. No SDP")});
        #sdp{} ->
            case ?IVR_SRV:pull_fsm_by_callid(CallId) of
                false -> ?FILTER:check_block_response_malware(no_dialog, Req, {reply, ?CallLegTransDoesNotExist("IVR. Unknown Dialog")});
                DlgX ->
                    ?IVR_SRV:sip_reinvite(DlgX, [CallId, Req]),
                    noreply
            end
    end.

%% ----------------------------------------------------------------------------------
%% CANCEL
%% ----------------------------------------------------------------------------------
-spec(sip_cancel(InviteRequest::nksip:request(),
                 Request::nksip:request(),
                 Call::nksip:call()) -> ok).

sip_cancel(InviteReq, Req, Call) ->
    #call{call_id=CallId}=Call,
    ?LOGSIP("sip_cancel ~p, ~p", [self(), CallId]),
    case ?IVR_SRV:pull_fsm_by_callid(CallId) of
        false -> ok;
        DlgX -> ?IVR_SRV:sip_cancel(DlgX, [CallId, Req, InviteReq]), ok
    end.

%% ----------------------------------------------------------------------------------
%% ACK
%% ----------------------------------------------------------------------------------
-spec(sip_ack(Request::nksip:request(), Call::nksip:call()) -> ok).

sip_ack(Req, Call) ->
    #call{call_id=CallId}=Call,
    ?LOGSIP("sip_ack ~p, ~p", [self(), CallId]),
    case ?IVR_SRV:pull_fsm_by_callid(CallId) of
        false -> ok;
        DlgX ->
            ?IVR_SRV:sip_ack(DlgX, [CallId, Req]),
            ok
    end.

%% ----------------------------------------------------------------------------------
%% BYE
%% ----------------------------------------------------------------------------------
-spec(sip_bye(Request::nksip:request(), Call::nksip:call()) ->
        {reply, nksip:sipreply()} | noreply).

sip_bye(Req, Call) ->
    #call{call_id=CallId}=Call,
    ?LOGSIP("sip_bye ~p, ~p", [self(), CallId]),
    case ?IVR_SRV:pull_fsm_by_callid(CallId) of
        false -> {reply, ok};
        DlgX ->
            ?IVR_SRV:sip_bye(DlgX, [CallId, Req]),
            noreply
    end.

%% ----------------------------------------------------------------------------------
%% REFER
%% ----------------------------------------------------------------------------------
-spec(sip_refer(Request::nksip:request(), Call::nksip:call()) ->
    {reply, nksip:sipreply()} | noreply).

sip_refer(Req, Call) ->
    #call{call_id=CallId}=Call,
    ?LOGSIP("sip_refer ~p, ~p", [self(), CallId]),
    case ?IVR_SRV:pull_fsm_by_callid(CallId) of
        false -> ?FILTER:check_block_response_malware(no_dialog, Req, {reply, ?InternalError("IVR. Unknown dialog")});
        DlgX ->
            ?IVR_SRV:sip_refer(DlgX, [CallId, Req]),
            noreply
    end.

%% ----------------------------------------------------------------------------------
%% UPDATE
%% ----------------------------------------------------------------------------------
%% -spec(sip_update(Req::nksip:request(), Call::nksip:call()) ->
%%     {reply, nksip:sipreply()} | noreply).
%%
%% sip_update(Req, _Call) ->
%%     ?LOGSIP("sip_update"),
%%     ?FILTER:check_block_response_malware(method_not_allowed, Req, {reply, ?MethodNotAllowed("IVR")}).

%% ----------------------------------------------------------------------------------
%% SUBSCRIBE
%% ----------------------------------------------------------------------------------
%% -spec(sip_subscribe(Request::nksip:request(), Call::nksip:call()) ->
%%         {reply, nksip:sipreply()} | noreply).
%%
%% sip_subscribe(Req, Call) ->
%%     #call{call_id=CallId}=Call,
%%     ?LOGSIP("sip_subscribe ~p, ~p", [self(), CallId]),
%%     ?FILTER:check_block_response_malware(method_not_allowed, Req, {reply, ?MethodNotAllowed("IVR")}).

%% ----------------------------------------------------------------------------------
%% reSUBSCRIBE
%% ----------------------------------------------------------------------------------
%% -spec(sip_resubscribe(Request::nksip:request(), Call::nksip:call()) ->
%%         {reply, nksip:sipreply()} | noreply).
%%
%% sip_resubscribe(Req, Call) ->
%%     #call{call_id=CallId}=Call,
%%     ?LOGSIP("sip_resubscribe ~p, ~p", [self(), CallId]),
%%     ?FILTER:check_block_response_malware(method_not_allowed, Req, {reply, ?MethodNotAllowed("IVR")}).

%% ----------------------------------------------------------------------------------
%% NOTIFY
%% ----------------------------------------------------------------------------------
-spec(sip_notify(Request::nksip:request(), Call::nksip:call()) ->
        {reply, nksip:sipreply()} | noreply).

sip_notify(Req, Call) ->
    #call{call_id=CallId}=Call,
    ?LOGSIP("sip_notify ~p, ~p", [self(), CallId]),
    case ?IVR_SRV:pull_fsm_by_callid(CallId) of
        false -> ?FILTER:check_block_response_malware(no_dialog, Req, {reply, ?Forbidden("IVR. Unknown dialog")});
        DlgX ->
            ?IVR_SRV:sip_notify(DlgX, [CallId, Req]),
            noreply
    end.

%% ----------------------------------------------------------------------------------
%% PUBLISH
%% ----------------------------------------------------------------------------------
%% -spec(sip_publish(Request::nksip:request(), Call::nksip:call()) ->
%%         {reply, nksip:sipreply()} | noreply).
%%
%% sip_publish(_Req, _Call) ->
%%     ?LOGSIP("sip_publish"),
%%     {reply, ?MethodNotAllowed("IVR")}.

%% ----------------------------------------------------------------------------------
%% INFO
%% ----------------------------------------------------------------------------------
-spec(sip_info(Req::nksip:request(), Call::nksip:call()) ->
        {reply, nksip:sipreply()} | noreply).

sip_info(Req, Call) ->
    #call{call_id=CallId}=Call,
    ?LOGSIP("sip_info ~p, ~p", [self(), CallId]),
    case ?IVR_SRV:pull_fsm_by_callid(CallId) of
        false -> ?FILTER:check_block_response_malware(no_dialog, Req, {reply, ?Forbidden("IVR. Unknown dialog")});
        DlgX ->
            ?IVR_SRV:sip_info(DlgX, [CallId, Req]),
            noreply
    end.

%% ----------------------------------------------------------------------------------
%% MESSAGE
%% ----------------------------------------------------------------------------------
-spec(sip_message(Request::nksip:request(), Call::nksip:call()) ->
        {reply, nksip:sipreply()} | noreply).

sip_message(Req, Call) ->
    #call{call_id=CallId}=Call,
    ?LOGSIP("sip_message ~p, ~p", [self(), CallId]),
    case ?IVR_SRV:pull_fsm_by_callid(CallId) of
        false -> ?FILTER:check_block_response_malware(no_dialog, Req, {reply, ?Forbidden("IVR. Unknown dialog")});
        DlgX ->
            ?IVR_SRV:sip_message(DlgX, [CallId, Req]),
            noreply
    end.

%% ----------------------------------------------------------------------------------
%% OPTIONS
%% ----------------------------------------------------------------------------------
-spec(sip_options(Request::nksip:request(), Call::nksip:call()) ->
        {reply, nksip:sipreply()} | noreply).

sip_options(Req, Call) ->
    #call{call_id=CallId}=Call,
    ?LOGSIP("sip_options ~p, ~p", [self(), CallId]),
    PutSdp = case nksip_request:header(<<"accept">>, Req) of
                 {ok, []} -> true;
                  {ok, Accept} -> find_in_binary(Accept, <<"application/sdp">>);
                 {error,_} -> false
             end,
    Opts = [{replace, {<<"Allow">>, <<"INVITE, ACK, CANCEL, BYE, REFER, OPTIONS, NOTIFY, INFO, MESSAGE">>}}, % <<"REGISTER, SUBSCRIBE, PUBLISH, UPDATE">>
            {replace, {<<"Allow-Events">>, <<"refer, presence, dialog">>}}, % <<"kpml(Zoiper) talk,hold,conferencecheck_sync(Yealink)">>
            % {replace, {<<"Accept-Encoding">>, <<"">>}},
            % {replace, {<<"Accept-Language">>, <<"en">>}},
            {replace, {<<"Supported">>, <<"replaces, timer, path">>}} % <<"outbound, norefersub(Zoiper), extended-refer(Zoiper), gruu(JsSIP)">>
            ],
    Opts1 = case PutSdp of
                true ->
                    [{replace, {<<"Accept">>, <<"application/sdp">>}},
                     {replace, {<<"Content-Type">>, <<"application/sdp">>}},
                     {body, ?M_SDP:make_options_sdp()}
                     |Opts];
                false -> Opts
            end,
    {reply, {200,Opts1}}.

% --
find_in_binary([], _Key) -> false;
find_in_binary([Item|Rest], Key) ->
    case binary:match(?EU:to_lower(Item), Key) of
        nomatch -> find_in_binary(Rest, Key);
        {_From,_Count} -> true
    end.

%% ----------------------------------------------------------------------------------
%% REGISTER
%% ----------------------------------------------------------------------------------
%-spec(sip_register(Request::nksip:request(), Call::nksip:call()) ->
%        {reply, nksip:sipreply()} | noreply).
%
%sip_register(_Req, _Call) ->
%    ?LOGSIP("sip_register"),
%    {reply, ok}.


%% ----------------------------------------------------------------------------------
%% @doc sip_b2bua plugin callback
%% ----------------------------------------------------------------------------------

%% % ------
%% b2bua({uac_pre_response, [#sipmsg{cseq={_,M}}=Resp,_UAC,_Call]=Args}=_Query, _AppId) when M=='INVITE'; M=='REFER' ->
%%     {ok,CallId} = nksip_response:call_id(Resp),
%%     case ?IVR_SRV:pull_fsm_by_callid(CallId) of
%%         false -> {continue, Args};
%%         DlgX -> {continue, [Resp,_UAC,_Call1]} = ?IVR_SRV:uac_pre_response(DlgX, Args)
%%     end;
%% b2bua({uac_pre_response, Args}, _AppId) ->
%%     {continue, Args};

% ------
b2bua({uac_response, [#sipmsg{cseq={_,M}}=Resp]=Args}=_Query, _AppId) when M=='INVITE'; M=='REFER' ->
    {ok,CallId} = nksip_response:call_id(Resp),
    case ?IVR_SRV:pull_fsm_by_callid(CallId) of
        false -> continue;
        DlgX -> ?IVR_SRV:uac_response(DlgX, Args),
                Self = self(),
                ?IVR_SRV:save_callpid(DlgX, [{callpid,Self}])
    end;
b2bua({uac_response, _Args}, _AppId) ->
    continue;

% ------
b2bua({uas_dialog_response, [#sipmsg{cseq={_,M}}=Resp]=Args}=_Query, _AppId) when M=='INVITE'; M=='REFER' ->
    {ok,CallId} = nksip_response:call_id(Resp),
    case ?IVR_SRV:pull_fsm_by_callid(CallId) of
        false -> continue;
        DlgX -> ?IVR_SRV:uas_dialog_response(DlgX, Args)
    end;
b2bua({uas_dialog_response, _Args}, _AppId) ->
    continue;

% ------
b2bua({call_terminate, [Reason,Call]=Args}=_Query, _AppId) ->
    #call{call_id=CallId}=Call,
    case ?IVR_SRV:pull_fsm_by_callid(CallId) of
        false -> ok;
        DlgX ->
            ?LOGSIP("IVR. call_terminate: ~p, ~p", [CallId,Reason]),
            ?IVR_SRV:call_terminate(DlgX,Args),
            ok
    end,
    ok;

% ------
b2bua({uas_totag, [Req]}=_Query, _AppId) ->
    #sipmsg{call_id=CallId}=Req,
    case ?IVR_SRV:pull_fsm_by_callid(CallId) of
        false -> ?IVR_UTILS:build_tag();
        DlgX -> ?IVR_SRV:build_tag(DlgX,Req)
    end.

%% ----------------------------------------------------------------------------------
%% @doc sip_log plugin callback
%% ----------------------------------------------------------------------------------

sip_log(Query, AppId) ->
    ?LOGGING:sip_log(Query, AppId).

%% ----------------------------------------------------------------------------------
%% @doc sip_filter plugin callback
%% ----------------------------------------------------------------------------------

sip_filter(Query, AppId) ->
    % RP-1470 - filter should be disabled to receive packets from mrcp server directly to ivr node
    % ok.
    ?FILTER:transport_filter(internal, Query, AppId).

%% ----------------------------------------------------------------------------------
%% @doc Domain Callback: Synchronous user call.
%% ----------------------------------------------------------------------------------

sip_checkclusterdomain(Domain, _AppId) ->
    ?LOCALDOMAIN:is_cluster_domain(Domain).

%% ----------------------------------------------------------------------------------
%% @doc SipApp Callback: Synchronous user call.
%% ----------------------------------------------------------------------------------

%% handle_call(get_speed, _From, State) ->
%%     {ok, Speed} = nksip:get(?SIPAPP, speed),
%%     Reply = [{Time, ?U:unparse_uri(Uri)} || {Time, Uri} <- Speed],
%%     {reply, Reply, State};

handle_call(_, _From, _State) ->
    continue.


%% ----------------------------------------------------------------------------------
%% @doc SipApp Callback: Asynchronous user cast.
%% ----------------------------------------------------------------------------------

%% handle_cast({speed_update, Speed}, State) ->
%%     ok = nksip:put(?SIPAPP, speed, Speed),
%%     erlang:start_timer(?TIME_CHECK, self(), check_speed),
%%     {noreply, State};
%%
%% handle_cast({check_speed, true}, State) ->
%%     handle_info({timeout, none, check_speed}, State#state{auto_check=true});
%%
%% handle_cast({check_speed, false}, State) ->
%%     {noreply, State#state{auto_check=false}}.

handle_cast(_, _State) ->
    %%{noreply, State}.
    continue.

%% ----------------------------------------------------------------------------------
%% @doc SipApp Callback: External erlang message received.
%% The programmed timer sends a `{timeout, _Ref, check_speed}' message
%% periodically to the SipApp.
%% ----------------------------------------------------------------------------------
%%
%% handle_info({timeout, _, check_speed}, #state{auto_check=true}=State) ->
%%     Self = self(),
%%     spawn(fun() -> test_speed(Self) end),
%%     {noreply, State};
%%
%% handle_info({timeout, _, check_speed}, #state{auto_check=false}=State) ->
%%     {noreply, State}.

handle_info(_, _State) ->
     %{noreply, State}.
    continue.

%% ==========================================================================
%% Internal functions
%% ==========================================================================

% -------------------------------
% extracts ruri from request (could be shadowed by route header)
%
extract_ruri(Req) ->
    case ?ROUTE:get_top_route_uri(Req) of
        #uri{scheme=S, user=U, domain=D} ->
            % Real User and Domain from route
            {S, U, D};
        notfound ->
            % RURI User and Domain
            #sipmsg{ruri=#uri{scheme=S, user=U, domain=D}}=Req,
            {S, U, D}
    end.

% --------------------------------
%% %% @doc Gets all registered contacts and sends an OPTION to each of them
%% %% to measure its response time.
%% test_speed(Pid) ->
%%     ok.
%%
%% %% @private
%% test_speed([], Acc) ->
%%     Acc;
%% test_speed([Uri|Rest], Acc) ->
%%     case timer:tc(fun() -> nksip_uac:options(?SIPAPP, Uri, []) end) of
%%         {Time, {ok, 200, []}} ->
%%             test_speed(Rest, [{Time/1000, Uri}|Acc]);
%%         {_, _} ->
%%             test_speed(Rest, Acc)
%%     end.
