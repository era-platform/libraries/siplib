%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc
%%% @todo

-module(r_sip_ivr_invite_router).
-author('Peter Bukashin <tbotc@yandex.ru>').

%% -------------------------------------------------------------------
%%
%% -------------------------------------------------------------------

-export([sip_invite/2]).

%-compile([export_all,nowarn_export_all]).

%% ==========================================================================
%% Defines
%% ==========================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_ivr.hrl").
-include("../include/r_sip_headers.hrl").

% -include("../include/r_nksip.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% -----------------------------------
-spec(sip_invite(AOR::tuple(), Request::nksip:request()) -> {reply, nksip:sipreply()} | noreply | block).
%% -----------------------------------
sip_invite(AOR, Req) ->
    % {reply, ?InternalError("IVR. Not implemented")}.
    case build_route_opts(AOR, Req) of
        {reply,_}=R -> R;
        noreply -> noreply;
        block -> block
    end.

%% -----------------------------------
-spec(build_route_opts(AOR::tuple(), Request::nksip:request()) -> {reply, nksip:sipreply()} | noreply | block).
%% -----------------------------------
build_route_opts(AOR, Req) ->
    #sipmsg{ruri=#uri{domain=_RUriDomain}=_RUri,
            vias=[#via{domain=ViaDomain, port=_ViaPort}|_],
            nkport=#nkport{remote_ip=RemoteIp}}=Req,
    % from inside or from outside?
    % check via domain existence to detect is this call from inside or outside
    Insiders = ?CFG:get_all_sipserver_addrs(),
    BRemoteIp = ?EU:to_binary(inet:ntoa(RemoteIp)),
    case {lists:member(ViaDomain,Insiders), lists:member(BRemoteIp,Insiders)} of
        {false,_} -> ?FILTER:check_block_response_malware(invalid_address, Req, {reply, ?InternalError("IVR. Access denied, remote address is unknown")});
        {true,true} -> do_invite_ivr(AOR, Req);
        {true,false} ->
            ?OUT("IVR. Incoming request forbidden, via ~120p is cluster address, but remote ~120p is not", [ViaDomain, BRemoteIp]),
            ?FILTER:check_block_response_malware(invalid_address, Req, {reply, ?Forbidden("IVR. Invalid remote address")})
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -----------------------------------
%% start new ivr or replace user
%% -----------------------------------
do_invite_ivr({_,U,_}=AOR, Req) ->
    case ?U:parse_ivr_user(U) of
        {ok,{ivr,_}} ->
            case check_referred(Req) of
                {true,true,[_,H2]} -> do_replace_ivr(AOR, H2, Req);
                _ -> do_start_ivr(AOR, Req)
            end;
        {ok,{hunt,_}} ->
            case check_referred(Req) of
                {true,true,[_,H2]} -> do_replace_ivr(AOR, H2, Req);
                _ -> do_start_hunt(AOR, Req)
            end;
        _ ->
            ?FILTER:check_block_response_malware(unknown_account, Req, {reply, ?Forbidden("IVR. User not found")})
    end.

%% -----------------------------------
%% replace existing ivr session
%% @TODO @ivr
%% -----------------------------------
do_replace_ivr(_AOR, Replaces, Req) ->
    RepMap = ?U:parse_replaces(Replaces),
    RCallId = maps:get(callid,RepMap),
    case ?IVR_SRV:pull_fsm_by_callid(RCallId) of
        false -> {reply, ?CallLegTransDoesNotExist("IVR. Replacing falure. Session not found")};
        DlgX ->
            ?STAT_FACADE:link_replaced(RCallId, Req#sipmsg.call_id), % @stattrace
            ?IVR_SRV:replace_abonent(DlgX, Req),
            noreply
    end.

%% -----------------------------------
%% create new ivr session
%% -----------------------------------
do_start_ivr(AOR, Req) ->
    case nksip_request:header(?IvrHeaderLow, Req) of
        {ok,[IvrParamsH|_]} ->
            IvrParamsL = lists:map(fun({K,V}) -> {K,?URLENCODE:decode(V)}; (T) -> T end, ?U:parse_params(IvrParamsH)),
            do_start_ivr_1(AOR, IvrParamsL, Req);
        _ -> {reply, ?ServiceUnavailable("IVR. Script not defined (a)")}
    end.

%% RP-1344
%% get dummy scripts
do_start_ivr_1({_,<<?DummyIvrPrefix,_/binary>>=UN,D}=AOR, IvrParamsL, Req) ->
    DummyKey = {D,UN},
    case ?U:call_store(DummyKey,fun() -> {get,[DummyKey]} end, 5000) of
        false -> {reply, ?ServiceUnavailable("IVR. Dummy not found")};
        {ok,#{}=Item} -> do_start_ivr_fsm(Item, AOR, IvrParamsL, Req)
    end;
%% get scripts from dc
do_start_ivr_1({_,_U,D}=AOR, IvrParamsL, Req) ->
    [IvrCode] = ?EU:extract_optional_default([{<<"code">>,<<>>}], IvrParamsL),
    case ?IVR_UTILS:get_script(D, IvrCode, 'ivr') of
        undefined -> {reply, ?ServiceUnavailable("IVR. DC error")};
        not_found -> {reply, ?ServiceUnavailable("IVR. Script not defined (b)")};
        #{}=Item -> do_start_ivr_fsm(Item, AOR, IvrParamsL, Req)
    end.

%% @private
do_start_ivr_fsm(Item, {_,_U,D}=AOR, IvrParamsL, Req) ->
    Opts = [{app,?SIPAPP},
            {aor,AOR},
            {req,Req},
            {domain,D},
            {script,Item},
            {call_params,IvrParamsL},
            {call_pid,self()}],
    ?IVR_SRV:start(Opts),
    noreply.

%% -----------------------------------
%% create new hunt session
%% -----------------------------------
do_start_hunt(AOR, Req) ->
    case nksip_request:header(?HuntHeaderLow, Req) of
        {ok,[HuntId|_]} -> do_start_hunt_1(AOR, HuntId, Req);
        _ -> {reply, ?ServiceUnavailable("IVR. Hunt not defined 1")}
    end.
%%
do_start_hunt_1({_,_U,D}=AOR, HuntParamsH, #sipmsg{from={From,_},call_id=CallId}=Req) ->
    HuntParamsL = lists:map(fun({K,V}) -> {K,?URLENCODE:decode(V)}; (T) -> T end, ?U:parse_params(HuntParamsH)),
    [HuntId] = ?EU:extract_optional_default([{<<"huntid">>,<<>>}], HuntParamsL),
    case ?IVR_UTILS:get_huntsip(D,HuntId) of
        undefined -> {reply, ?ServiceUnavailable("IVR. DC error")};
        not_found -> {reply, ?ServiceUnavailable("IVR. Hunt not defined 2")};
        #{}=ItemHunt ->
            CallInfo = ?IVR_UTILS:get_b2b_call_info(CallId,D),
            [InviteId,EsgDlg] = ?EU:maps_get_default([{<<"inviteid">>,undefined},{<<"esgdlg">>,<<>>}],CallInfo),
            QueueIvrCode = maps:get(qivrscript,ItemHunt),
            EventOpts = #{<<"huntq_id">> => HuntId, <<"qivrscript">> => QueueIvrCode, <<"domain">> => D, <<"inviteid">> => InviteId,<<"esgdlg">> => EsgDlg},
            ?EVENT:queue_greeting(EventOpts, Req),
            {ok, #{}=Item} = ?QUEUE_SCRIPT:create_hunt_queue_script(ItemHunt, From#uri.user, ?U:unquote_display(From#uri.disp)),
            Opts = [{app,?SIPAPP},
                    {aor,AOR},
                    {req,Req},
                    {domain,D},
                    {script,Item},
                    {call_params,[{<<"code">>,QueueIvrCode},{<<"inviteid">>,InviteId}|HuntParamsL]},
                    {call_pid,self()},
                    {callback_funs,?QUEUE:get_script_callback_functions()},
                    {expression_funs, ?QUEUE:get_expression_funs()}],
            case ?IVR_SRV:start(Opts) of
                {ok, PidFSM} ->
                    Fevent = fun(StopReason) -> ?EVENT:hunt_final(EventOpts#{<<"stopreason">> => StopReason}, Req) end,
                    ?MONITOR:append_final_fun(PidFSM, <<"ivrhuntq_final">>, Fevent),
                    noreply;
                _ -> {reply, ?ServiceUnavailable("IVR. Script not started")}
            end
    end.

%% -----------------------------------
%% check request is referred
%% -----------------------------------
-spec check_referred(Req::#sipmsg{}) -> {HasReferred::boolean(),HasReplaces::boolean(),H::list()} | error .
%% -----------------------------------
check_referred(Req) ->
    case {nksip_request:header("referred-by", Req), nksip_request:header("replaces", Req)} of
        {{ok,[]},{ok,[]}} -> {false, false, []};
        {{ok,[H1|_]},{ok,[]}} -> {true, false, [H1]};
        {{ok,[]},{ok,_}} -> error;
        {{ok,[H1|_]},{ok,[H2|_]}} -> {true, true, [H1,H2]};
        _ -> error
    end.
