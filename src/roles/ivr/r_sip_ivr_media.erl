%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Media utils of 'active' state of IVR FSM
%%% @TODO @ivr

-module(r_sip_ivr_media).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_ivr.hrl").
-include("../include/r_sip_mgc.hrl").

% -include("../include/r_nksip.hrl").

-define(CURRSTATE, 'active').

%% ====================================================================
%% API functions
%% ====================================================================

%% ---------------------------------
%% checks if media is available (mgc-mg pair, or no media is need)
%% ---------------------------------
-spec check_available(State::#state_ivr{}) -> {ok, true|false} | {error,Reason::term()}.

% @testmode
check_available(#state_ivr{use_media=false}) -> {ok, true};

check_available(State) ->
    ?MGC:mg_check_any_available(get_mg_opts(State)).

%% ---------------------------------
%% checks if media is available and return count of  (mgc-mg pair, or no media is need)
%% ---------------------------------
-spec check_available_slots(State::#state_ivr{}) -> {ok, MGCnt::integer(), CtxCnt::integer()} | {error,Reason::term()}.

% @testmode
check_available_slots(#state_ivr{use_media=false}) -> {ok, 10, 10};

check_available_slots(State) ->
    ?MGC:mg_check_available_slots(get_mg_opts(State)).

%% ---------------------------------
%% prepare media by creating new context (player termination or first participant)
%% ---------------------------------

-spec media_prepare_start(State::#state_ivr{}) -> {ok, NewState::#state_ivr{}} | {error, SipReply::{SipCode::integer(),SipOpts::list()}}.

% @testmode
media_prepare_start(#state_ivr{use_media=false}=State) -> {ok,State};

media_prepare_start(State) ->
    #state_ivr{ivrid=IvrId}=State,
    <<B4:32/bitstring, _/bitstring>> = ?U:luid(),
    MSID = <<IvrId/bitstring, "_", B4/bitstring>>,
    MediaOpts = #{rec => true, % record enabled
                  mg_opts => get_mg_opts(State),
                  play => #{files => []}}, % #{file => "files/conf/snd_knock.wav"}}
    {Mks,Res} = timer:tc(fun() -> ?MEDIA_IVR:start_media(MSID, MediaOpts) end),
    d(State, " .. prepare_media (~p ms)", [Mks/1000]),
    case Res of
        {reply, SipReply} when is_tuple(SipReply) ->
            d(State, "MGC error: create media context failure"),
            {error, SipReply};
        {ok, Media} ->
            State1 = State#state_ivr{media=Media},
            {ok, State1}
    end.

%% ---------------------------------
%% prepare media by creating new context (first participant)
%% ---------------------------------
-spec media_prepare_start(XSide::#side{}, State::#state_ivr{}) -> {ok, NewState::#state_ivr{}, LSdp::#sdp{}} | {error, SipReply::{SipCode::integer(),SipOpts::list()}}.

% @testmode
media_prepare_start(#side{remotesdp=RSdp}, #state_ivr{use_media=false}=State) ->
    {ok, State, ?M_SDP:lo_reduce(?M_SDP:make_offer_sdp(),RSdp)};

% when media is undefined
media_prepare_start(Side, #state_ivr{media=undefined}=State) ->
    #side{remotesdp=RSdp,
          req=Req}=Side,
    #state_ivr{ivrid=IVRId}=State,
    case ?M_SDP:check_known_fmt(RSdp) of
        false ->
            d(State, "MGC error: Not supported media"),
            {error, ?MediaNotSupported("IVR. Unknown audio formats")};
        true ->
            <<B4:32/bitstring, _/bitstring>> = ?U:luid(),
            MSID = <<IVRId/bitstring, "_", B4/bitstring>>,
            MediaOpts = #{rec => false, % record disabled
                          mg_opts => get_mg_opts(State)},
            {Mks,Res} = timer:tc(fun() -> ?MEDIA_IVR:start_media(MSID, Req, MediaOpts) end),
            d(State, " .. prepare_media (~p ms)", [Mks/1000]),
            case Res of
                {reply, SipReply} when is_tuple(SipReply) ->
                    d(State, "MGC error: create media context failure"),
                    {error, SipReply};
                {ok, Media, LSdp} ->
                    State1 = State#state_ivr{media=Media},
                    {ok, State1, LSdp}
            end
    end;

% when media already exists (second response should be sent)
media_prepare_start(#side{req=Req}=_Side, #state_ivr{media=Media}=State) ->
    case ?MEDIA_IVR:modify_term(Media, Req) of
        {error, Reason} ->
            d(State, "MGC attach error: ~120p", [Reason]),
            {error, ?InternalError("IVR. MGC error")};
        {ok, Media1, LSdp} ->
            State1 = State#state_ivr{media=Media1},
            {ok, State1, LSdp}
    end.

%% ---------------------------------
%% replace termination when replacing
%% ---------------------------------
-spec media_replace(ACallId::binary(), Side::#side{}, State::#state_ivr{}) -> {ok, NewState::#state_ivr{}, LSdp::#sdp{}} | {error, SipReply::{SipCode::integer(),SipOpts::list()}}.

% @testmode
media_replace(_ACallId, #side{remotesdp=RSdp}, #state_ivr{use_media=false}=State) ->
    {ok, State, ?M_SDP:lo_reduce(?M_SDP:make_offer_sdp(),RSdp)};

media_replace(ACallId, #side{req=XReq}=_Side, #state_ivr{media=Media}=State) ->
    case ?MEDIA_IVR:add_term(Media,XReq) of
        {error,_}=Err -> Err;
        {ok,Media1,LSdp} ->
            case ?MEDIA_IVR:del_term(Media1,ACallId) of
                {error,_}=Err -> Err;
                {ok,Media2} ->
                    State1 = State#state_ivr{media=Media2},
                    {ok, State1, LSdp}
            end end.

%% ---------------------------------
%% stops existing media context
%% ---------------------------------
-spec media_stop(State::#state_ivr{}) -> {ok, NewState::#state_ivr{}}.

media_stop(#state_ivr{media=undefined}=State) -> {ok, State};
media_stop(#state_ivr{media=Media}=State) ->
    ?MEDIA_IVR:stop_media(Media),
    {ok, State#state_ivr{media=undefined}}.


%% ---------------------------------
%% returns map with current media properties (mgc/mg/ctxid)
%% ---------------------------------
-spec get_current_media_link(State::#state_ivr{}) -> {ok, Map::map()} | undefined.

get_current_media_link(#state_ivr{media=undefined}=_State) -> undefined;
get_current_media_link(#state_ivr{media=Media}=_State) ->
    #media_ivr{mgc=MGC, msid=MSID, mgid=MG, ctx=CtxId}=Media,
    {ok, #{mgc=>MGC, msid=>MSID, mgid=>MG, ctx=>CtxId}}.

%% %% ---------------------------------
%% %% modify media by attaching new participant
%% %% ---------------------------------
%% -spec media_attach(XSide::#side{}, State::#state_ivr{}) -> {ok, NewState::#state_ivr{}, LSdp::#sdp{}} | {error, SipReply::{SipCode::integer(),SipOpts::list()}}.
%%
%% media_attach(Side, State) ->
%%     #side{remotesdp=RSdp,
%%           req=Req}=Side,
%%     case ?M_SDP:check_known_fmt(RSdp) of
%%         false ->
%%             d(State, "MGC error: Not supported media"),
%%             {error, ?MediaNotSupported("IVR. Unknown audio formats")};
%%         true ->
%%             #state_ivr{media=Media}=State,
%%             case ?MEDIA_IVR:attach_term(Media, Req) of
%%                 {error, Reason} ->
%%                     d(State, "MGC attach error: ~120p", [Reason]),
%%                     {error, ?InternalError("IVR. MGC error")};
%%                 {ok, Media1, LSdp} ->
%%                     State1 = State#state_ivr{media=Media1},
%%                     {ok, State1, LSdp}
%%             end
%%     end.

%% ---------------------------------
%% modify media by detaching participant
%% ---------------------------------
-spec media_detach(XSide::#side{}|binary(), State::#state_ivr{}) -> {ok, NewState::#state_ivr{}}.

% @testmode
media_detach(#side{}, #state_ivr{use_media=false}=State) -> {ok,State};

media_detach(#side{callid=CallId}=_Side, State) -> media_detach(CallId, State);

media_detach(CallId, State) when is_binary(CallId) ->
    #state_ivr{media=Media}=State,
    case ?MEDIA_IVR:del_term(Media, CallId) of
        {error, Reason} ->
            d(State, "MGC del_term error: ~120p", [Reason]),
            {ok, State};
        {ok, Media1} ->
            {ok, State#state_ivr{media=Media1}}
    end.

%% ---------------------------------
%% modify media by participant's reinvite
%% ---------------------------------
-spec media_sessionchange(Req::#sipmsg{}, XSide::#side{}, State::#state_ivr{}) -> {ok, NewState::#state_ivr{}, LSdp::#sdp{}} | {error, SipReply::{SipCode::integer(),SipOpts::list()}}.

% @testmode
media_sessionchange(Req, _XSide, #state_ivr{use_media=false}=State) ->
    #sdp{}=RSdp=?U:extract_sdp(Req),
    {ok, State, ?M_SDP:setmode(?M_SDP:lo_reduce(?M_SDP:make_offer_sdp(),RSdp), ?M_SDP:reverse_stream_mode(?M_SDP:get_audio_mode(RSdp)))};

media_sessionchange(Req, _XSide, State) ->
    #sdp{}=RSdp=?U:extract_sdp(Req),
    case ?M_SDP:check_known_fmt(RSdp) of
        false ->
            d(State, "MGC error: Not supported media"),
            {error, ?MediaNotSupported("IVR. Unknown audio formats")};
        true ->
            #state_ivr{media=Media}=State,
            case ?MEDIA_IVR:modify_term(Media, Req) of
                {error, Reason} ->
                    d(State, "MGC sessionchange error: ~120p", [Reason]),
                    {error, ?InternalError("IVR. MGC error")};
                {ok, Media1, LSdp} ->
                    d(State, " -> session changed"),
                    State1 = State#state_ivr{media=Media1},
                    {ok, State1, LSdp}
            end
    end.

%% ---------------------------------
%% update term by both remote and local sdp (after outgoing reinvite succeed)
%% ---------------------------------
-spec media_sessionchanged({RSdp::#sdp{}, LSdp::#sdp{}}, State::#state_ivr{}) -> {ok, NewState::#state_ivr{}} | {error, SipReply::{SipCode::integer(),SipOpts::list()}}.

% @testmode
media_sessionchanged(_, #state_ivr{use_media=false}=State) -> {ok, State};

media_sessionchanged({RSdp, LSdp}, #state_ivr{a=#side{callid=CallId}}=State) ->
    #state_ivr{media=Media}=State,
    case ?MEDIA_IVR:modify_term(Media, {RSdp, LSdp}, CallId) of
        {error, Reason} ->
            d(State, "MGC sessionchange error: ~120p", [Reason]),
            {error, ?InternalError("IVR. MGC error")};
        {ok, Media1} ->
            d(State, " -> session changed"),
            State1 = State#state_ivr{media=Media1},
            {ok, State1}
    end.

%% ---------------------------------
%% RP-1466
%% Modify term's opts (does not concern SDPs, only opts.
%% Used for Events descriptor for termination
%% ---------------------------------
modify_term_events({RequestId,Events}=Ev, State) when is_integer(RequestId), is_list(Events) ->
    #state_ivr{acallid=CallId,media=Media}=State,
    case ?MEDIA_IVR:modify_term_events(Media, CallId, Ev) of
        {error, Reason} ->
            d(State, "MGC sessionchange error: ~120p", [Reason]),
            {error, ?InternalError("IVR. MGC error")};
        {ok,Media1} ->
            State1 = State#state_ivr{media=Media1},
            {ok, State1}
    end.

%% ---------------------------------
%% get current localsdp, could update o= by next session id (prepare outgoing reinvite)
%% ---------------------------------
-spec get_local_sdp(State::#state_ivr{}, SetNextO::boolean()) -> {ok, State::#state_ivr{}, LSdp::#sdp{}} | {error, Reason::term()}.

% @testmode
get_local_sdp(#state_ivr{use_media=false}=State, _SetNextO) ->
    {ok, State, ?M_SDP:make_offer_sdp()};

get_local_sdp(State, SetNextO) ->
    #state_ivr{media=Media, a=#side{callid=CallId}}=State,
    case ?MEDIA_IVR:get_local_sdp(Media, CallId, SetNextO) of
        {error, Reason} ->
            d(State, "MGC prepare fax sdp: ~120p", [Reason]),
            {error, ?InternalError("IVR. MGC error")};
        {ok, Media1, LSdp} ->
            d(State, " -> fax sdp prepared"),
            State1 = State#state_ivr{media=Media1},
            {ok, State1, LSdp}
    end.

%% ---------------------------------
%% set updated localsdp (through templating), update o= by next session id (prepare outgoing reinvite)
%% ---------------------------------
-spec set_local_sdp(LSdp::#sdp{}, State::#state_ivr{}) -> {ok, State::#state_ivr{}, LSdp::#sdp{}} | {error, Reason::term()}.

% @testmode
set_local_sdp(LSdp, #state_ivr{use_media=false}=State) -> {ok, State, LSdp};

set_local_sdp(LSdp, State) ->
    #state_ivr{media=Media, a=#side{callid=CallId}}=State,
    case ?MEDIA_IVR:set_local_sdp(Media, CallId, LSdp) of
        {error, Reason} ->
            d(State, "MGC prepare fax sdp: ~120p", [Reason]),
            {error, ?InternalError("IVR. MGC error")};
        {ok, Media1, LSdp1} ->
            d(State, " -> fax sdp prepared"),
            State1 = State#state_ivr{media=Media1},
            {ok, State1, LSdp1}
    end.

%% ---------------------------------
%% modify media by adding new participant's outgoing call (only local side)
%% ---------------------------------
-spec media_add_outgoing(CallId::binary(), State::#state_ivr{}) -> {ok, NewState::#state_ivr{}, LSdp::#sdp{}} | {error, SipReply::{SipCode::integer(),SipOpts::list()}}.

% @testmode
media_add_outgoing(_, #state_ivr{use_media=false}=State) -> {ok, State, ?M_SDP:make_offer_sdp()};

media_add_outgoing(CallId, State) ->
    #state_ivr{media=Media}=State,
    case ?MEDIA_IVR:add_outgoing_term(Media, CallId) of
        {error, Reason} ->
            d(State, "MGC error: ~120p", [Reason]),
            {error, ?InternalError("IVR. MGC error")};
        {ok, Media1, LSdp} ->
            d(State, " -> added local term"),
            State1 = State#state_ivr{media=Media1},
            {ok, State1, LSdp}
    end.

%%---------
-spec media_update_outgoing_by_remote_ext(Fork::#side_fork{}, RSdp::#sdp{}, State::#state_ivr{}) -> {ok, NewState::#state_ivr{}} | {error, Reason::term()}.

% @testmode
media_update_outgoing_by_remote_ext(_, _, #state_ivr{use_media=false}=State) -> {ok, State};

media_update_outgoing_by_remote_ext(Fork, RSdp, State) ->
    #side_fork{callid=CallId}=Fork,
    #state_ivr{media=Media}=State,
    case ?MEDIA_IVR:update_outgoing_term_by_remote(Media, CallId, RSdp) of
        {error, Reason} ->
            d(State, "MGC error: ~120p", [Reason]),
            {error, Reason};
        {ok, Media1} ->
            d(State, " -> modified local term"),
            State1 = State#state_ivr{media=Media1},
            {ok, State1}
    end.

%%---------
-spec media_update_outgoing_by_remote(CallId::binary(), RSdp::#sdp{}, State::#state_ivr{}) -> {ok, NewState::#state_ivr{}} | {error, Reason::term()}.

% @testmode
media_update_outgoing_by_remote(_, _, #state_ivr{use_media=false}=State) -> {ok, State};

media_update_outgoing_by_remote(CallId, RSdp, State) ->
    #state_ivr{media=Media}=State,
    case ?MEDIA_IVR:update_outgoing_term_by_remote(Media, CallId, RSdp) of
        {error, Reason} ->
            d(State, "MGC error: ~120p", [Reason]),
            {error, Reason};
        {ok, Media1} ->
            d(State, " -> modified local term"),
            State1 = State#state_ivr{media=Media1},
            {ok, State1}
    end.

%% ---------------------------------
%% modify topology
%% ---------------------------------
-spec modify_topology(State::#state_ivr{}, Topology::[{FromCallId::binary(), ToCallId::binary(), bothway|oneway|isolate}]) ->
          {ok, NewState::#state_ivr{}} | {error,R::term()}.

% testmode
modify_topology(#state_ivr{use_media=false}=State, []) -> {ok, State};

modify_topology(State, [_|_]=TopologyList) ->
    #state_ivr{media=Media}=State,
    case ?MEDIA_IVR:modify_topology(Media, TopologyList) of
        {error, Reason} ->
            d(State, "MGC error: ~120p", [Reason]),
            {error, Reason};
        {ok, Media1} ->
            d(State, " -> topology modified"),
            State1 = State#state_ivr{media=Media1},
            {ok, State1}
    end.

%% ---------------------------------
%% update context by adding/removing ivr termination with player
%% ---------------------------------
-spec media_attach_player(Opts::list(), State::#state_ivr{}) -> {ok, NewState::#state_ivr{}} | {error, Reason::term()}.

% @testmode
media_attach_player(_, #state_ivr{use_media=false}=State) -> {ok, State};

media_attach_player(Opts, #state_ivr{media=Media}=State) ->
    case ?MEDIA_IVR:ivr_attach_player(Media, Opts) of
        {error,_}=Err -> Err;
        {ok,Media1} ->
            {ok, State#state_ivr{media=Media1}}
    end.

%% ----
-spec media_detach_player(Opts::list(), State::#state_ivr{}) -> {ok, NewState::#state_ivr{}} | {error, Reason::term()}.

% @testmode
media_detach_player(_, #state_ivr{use_media=false}=State) -> {ok, State};

media_detach_player(Opts, #state_ivr{media=Media}=State) ->
    case ?MEDIA_IVR:ivr_detach_player(Media, Opts) of
        {error,_}=Err -> Err;
        {ok,Media1} ->
            {ok, State#state_ivr{media=Media1}}
    end.

%% ----
-spec media_modify_player(Opts::list(), State::#state_ivr{}) -> {ok, NewState::#state_ivr{}} | {error, Reason::term()}.

% @testmode
media_modify_player(_, #state_ivr{use_media=false}=State) -> {ok, State};

media_modify_player(Opts, #state_ivr{media=Media}=State) ->
    case ?MEDIA_IVR:ivr_modify_player(Media, Opts) of
        {error,_}=Err -> Err;
        {ok,Media1} ->
            {ok, State#state_ivr{media=Media1}}
    end.

%% ----
-spec media_find_playerid(TermId::term(), State::#state_ivr{}) -> {ok, PlayerId::term()} | undefined.

% @testmode
media_find_playerid(_, #state_ivr{use_media=false}) -> undefined;

media_find_playerid(TermId, #state_ivr{media=Media}=_State) ->
    ?MEDIA_IVR:ivr_find_player_id(TermId, Media).

%% ---------------------------------
%% update context by adding/removing ivr termination with player
%% ---------------------------------
-spec media_attach_recorder(Opts::list(), State::#state_ivr{}) -> {ok, NewState::#state_ivr{}} | {error, Reason::term()}.

% @testmode
media_attach_recorder(_, #state_ivr{use_media=false}=State) -> {ok, State};

media_attach_recorder(Opts, #state_ivr{media=Media}=State) ->
    case ?MEDIA_IVR:ivr_attach_recorder(Media, Opts) of
        {error,_}=Err -> Err;
        {ok,Media1} ->
            {ok, State#state_ivr{media=Media1}}
    end.

%% ----
-spec media_detach_recorder(Opts::list(), State::#state_ivr{}) -> {ok, NewState::#state_ivr{}} | {error, Reason::term()}.

% @testmode
media_detach_recorder(_, #state_ivr{use_media=false}=State) -> {ok, State};

media_detach_recorder(Opts, #state_ivr{media=Media}=State) ->
    case ?MEDIA_IVR:ivr_detach_recorder(Media, Opts) of
        {error,_}=Err -> Err;
        {ok,Media1} ->
            {ok, State#state_ivr{media=Media1}}
    end.

%% ----
-spec media_find_recorderid(TermId::term(), State::#state_ivr{}) -> {ok, RecorderId::term()} | undefined.

% @testmode
media_find_recorderid(_, #state_ivr{use_media=false}) -> undefined;

media_find_recorderid(TermId, #state_ivr{media=Media}=_State) ->
    ?MEDIA_IVR:ivr_find_recorder_id(TermId, Media).

%% ----
-spec media_check_no_playrec(State::#state_ivr{}) -> true | false.

% @testmode
media_check_no_playrec(#state_ivr{use_media=false}) -> true;

media_check_no_playrec(#state_ivr{media=Media}=_State) ->
    ?MEDIA_IVR:ivr_check_no_playrec(Media).

%% ---------------------------------
%% update context by adding/removing ivr termination with fax
%% ---------------------------------
-spec media_attach_fax(Opts::list(), State::#state_ivr{}) -> {ok, NewState::#state_ivr{}} | {error, Reason::term()}.

% @testmode
media_attach_fax(_, #state_ivr{use_media=false}=State) -> {ok, State};

media_attach_fax(Opts, #state_ivr{media=Media}=State) ->
    case ?MEDIA_IVR:ivr_attach_fax(Media, Opts) of
        {error,_}=Err -> Err;
        {ok,Media1} ->
            {ok, State#state_ivr{media=Media1}}
    end.

%% ----
-spec media_detach_fax(Opts::list(), State::#state_ivr{}) -> {ok, NewState::#state_ivr{}} | {error, Reason::term()}.

% @testmode
media_detach_fax(_, #state_ivr{use_media=false}=State) -> {ok, State};

media_detach_fax(Opts, #state_ivr{media=Media}=State) ->
    case ?MEDIA_IVR:ivr_detach_fax(Media, Opts) of
        {error,_}=Err -> Err;
        {ok,Media1} ->
            {ok, State#state_ivr{media=Media1}}
    end.

%% ----
-spec media_check_faxid(TermId::term(), State::#state_ivr{}) -> ok | undefined.

% @testmode
media_check_faxid(_, #state_ivr{use_media=false}) -> undefined;

media_check_faxid(TermId, #state_ivr{media=Media}=_State) ->
    ?MEDIA_IVR:ivr_check_fax_id(TermId, Media).

%% ---------------------------------
%% checks if MGC/MG pair, linked in current state data corresponds to args
%% ---------------------------------
-spec media_check_mg(State::#state_ivr{}, Args::list()) -> true | false.

% @testmode
media_check_mg(#state_ivr{use_media=false}, _) -> true;

media_check_mg(State, Args) ->
    #state_ivr{media=Media}=State,
    ?MEDIA_IVR:check_media_mg(Media, Args).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
%% returns mg opts for call to search mg (mgsfx, mgaddr)
get_mg_opts(_State) -> get_mg_opts().
get_mg_opts() ->
    [{mgsfx,?U:mg_postfix(<<"def">>)}, % suffix for internal calls (not sg/esg)
     get_ua_addrs_option() % local mg is less priority, used only if 1-server mode.
    ].

%% @private
get_ua_addrs_option() ->
    Key = server_interfaces,
    FAddrs = fun() -> lists:usort(lists:filtermap(fun({_,_,_,_}=A) -> {true,?EU:to_binary(inet:ntoa(A))}; (_) -> false end, ?ENVCFG:get_current_server_addrs())) end,
    Addrs = ?ENVSTORE:lazy_t(Key, FAddrs, {60000,30000,2000}, {true,true}),
    {ua_addrs,Addrs}.

%% @private ----
d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    #state_ivr{ivrid=ConfId}=State,
    ?LOG("IVR fsm ~p '~p':" ++ Fmt, [ConfId,?CURRSTATE] ++ Args).
