%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>, Peter Bukashin <tbotc@yandex.ru>
%%% @date 26.09.2016
%%% @doc WHAT IS THIS? WHAT'S THE RESPONSIBILITIES?

-module(r_sip_ivr_mode_router).
-author(['Evgeniy Grebenyuk <llceceron@gmail.com>', 'Peter Bukashin <tbotc@yandex.ru>']).

%% ====================================================================
%% Exports
%% ====================================================================

-export([start_by_mode/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_ivr.hrl").

%% ====================================================================
%% Types
%% ====================================================================

%% ====================================================================
%% API functions
%% ====================================================================

%% ====================================================================
%% Public functions
%% ====================================================================

start_by_mode(#{}=Map) ->
    case maps:get(mode,Map,undefined) of
        <<"1">> ->
            % mode, args
            maps:get(args,Map), % assert
            start_by_mode_1_1(Map);
        _ -> {error,invalid_params}
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -------------------------------------------------------------------
start_by_mode_1_1(#{}=Map) ->
    Args = maps:get(args,Map),
    case check_args(Args) of
        {ok,D,Scr,SCode} -> start_by_mode_1_2(D,Scr,SCode,Map);
        {error,_}=Err -> Err
    end.

%% @private
check_args(Args) ->
    [Dom,Scr,SCode,FRes,CallerId,OwnerPid] =
        ?EU:extract_optional_default([domain,script,script_code,fun_res,callerid,ownerpid], Args,
            [undefined,undefined,undefined,undefined,undefined,undefined]),
    if (Dom==undefined) -> {error, <<"domain_not_defined">>};
       (OwnerPid==undefined) -> {error, <<"ownerpid_not_defined">>};
       (CallerId==undefined) -> {error, <<"callerid_not_defined">>};
       (FRes==undefined) -> {error, <<"funresult_not_defined">>};
       (Scr==undefined andalso SCode==undefined) -> {error, <<"scr_and_scrcode_not_defined">>};
       true -> {ok,Dom,Scr,SCode}
    end.

%% -------------------------------------------------------------------
start_by_mode_1_2(D,Scr,SCode,MapOpts) ->
    Args = maps:get(args,MapOpts),
    FStart = fun(Script) ->
                    NewArgs = add_or_replace_to_opt_list([{app,?SIPAPP},{script,Script}],Args),
                    NewMapOpts = MapOpts#{args:=NewArgs},
                    ?IVR_SRV:start_by_mode(NewMapOpts)
             end,
    case Scr of
        undefined when SCode/=<<>> ->
            case get_script_by_code(D,SCode) of
                {ok,Script} -> FStart(Script);
                {error,_}=Err -> Err
            end;
        undefined -> FStart(empty_script());
        <<>> -> FStart(empty_script());
        _ -> FStart(Scr)
    end.

%% @private
empty_script() ->
    CStart = [{<<"oId">>,1},
              {<<"oType">>,101},
              {<<"name">>,<<>>}],% #436
    #{checksum => 0,
      code => <<>>,
      id => ?EU:emptyid(),
      name => <<>>,
      scriptdata => [{<<"objects">>,[CStart]},
                     {<<"variables">>,[]}]}.

%% -------------------------------------------------------------------
get_script_by_code(D,IvrCode) ->
    case ?IVR_UTILS:get_script(D, IvrCode, 'ivr') of
        undefined -> {error,<<"IVR. DC error">>};
        not_found -> {error,<<"IVR. Script not defined (c)">>};
        #{}=Item -> {ok,Item}
    end.


add_or_replace_to_opt_list([],OptList) -> OptList;
add_or_replace_to_opt_list([{K,V}|T],OptList) ->
    NewOptList =
        case lists:keyfind(K, 1, OptList) of
            {K,_} -> lists:keyreplace(K, 1, OptList,{K,V});
            false -> [{K,V}|OptList]
        end,
    add_or_replace_to_opt_list(T,NewOptList).
