%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc
%%% @todo

-module(r_sip_ivr_stat).
-author('Peter Bukashin <tbotc@yandex.ru>').

%% -------------------------------------------------------------------
%%
%% -------------------------------------------------------------------

-compile([export_all,nowarn_export_all]).

%% ==========================================================================
%% Defines
%% ==========================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_ivr.hrl").
-include("../include/r_sip_headers.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

query(<<"info">>, #{}=Map) ->
    Domain = maps:get(domain,Map,undefined),
    DlgIds = lists:filtermap(fun({DlgId,{_,#{domain:=D},_,_,_}}) when D==Domain; Domain==undefined -> {true,DlgId};
                                (_) -> false
                             end, ?DLG_STORE:get_all_dlgs()),
    FTime = fun(T) -> r_env_utils:strdatetime3339(calendar:now_to_datetime(T)) end,
    F = fun(DlgId) ->
            {ok,DlgMap} = ?IVR_SRV:get_info(DlgId,[unparse]),
            DlgMap1 = DlgMap#{starttime => FTime(maps:get(starttime,DlgMap))},
            case maps:get(state,DlgMap1) of
                dialing -> {true,DlgMap1};
                _ ->
                    ScrMap = maps:get(script,DlgMap1,#{}),
                    ScrPid = maps:get(pid,ScrMap,undefined),
                    case catch gen_server:call(ScrPid,{get_info}) of
                        {ok,ScrMapX} -> {true,DlgMap1#{script => maps:merge(ScrMap,ScrMapX#{starttime => FTime(maps:get(starttime,ScrMapX))})}};
                        _ -> {true,DlgMap1#{process_info => case process_info(ScrPid) of undefined -> undefined; PI -> ?EU:json_to_map(PI) end}}
                    end end end,
    List = lists:filtermap(F, DlgIds),
    M = #{<<"dialogs">> => List},
    {ok, M};

query(_Mode, #{}=_Map) ->
    M = #{dlgcount => ?DLG_STORE:get_dialog_count(),
          callcount => ?DLG_STORE:get_callid_count()},
    {ok, M}.

%% ====================================================================
%% Internal functions
%% ====================================================================


