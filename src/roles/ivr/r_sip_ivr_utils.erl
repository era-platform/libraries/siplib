%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc
%%% @TODO @ivr

-module(r_sip_ivr_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([]).

-compile([export_all,nowarn_export_all]).

%%% ====================================================================
%%% Define
%%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_ivr.hrl").
-include("../include/r_sip_mgc.hrl").
-include("../include/r_sip_headers.hrl").

%%% ====================================================================
%%% API functions
%%% ====================================================================

%%% ---------------------------------------------
%%% Local Tags
%%% ---------------------------------------------

build_tag() ->
    CurSrvCode = ?U:get_current_srv_textcode(),
    <<Tag0:48/bitstring, _/bitstring>> = ?U:luid(),
    <<"rIV-", CurSrvCode/bitstring, "-", Tag0/bitstring>>.

%% --------------------------
%% Generates non existing tag
%% --------------------------
generate_tag(#state_ivr{usedtags=Tags}=State) ->
    Tag = build_tag(),
    case lists:member(Tag, Tags) of
        false -> {Tag, State#state_ivr{usedtags=[Tag|Tags]}};
        true -> generate_tag(State)
    end.

%% --------------------------
%% Extract server idx from ivr's tag
%% --------------------------
extract_ivr_server_index(<<"rIV-", SrvCode:24/bitstring, "-", _/bitstring>>) -> {true, ?EU:parse_textcode_to_index(SrvCode)};
extract_ivr_server_index(_) -> false.

%% --------------------------
%% Extract server idx from prompt's tag
%% --------------------------
extract_prompt_server_index(<<"rPR-", SrvCode:24/bitstring, "-", _/bitstring>>) -> {true, ?EU:parse_textcode_to_index(SrvCode)};
extract_prompt_server_index(_) -> false.

%% --------------------------
%% Build call id for out call. Checks for non exist in current conf
%% --------------------------
build_out_callid() ->
    CurSrvCode = ?U:get_current_srv_textcode(),
    CallId = ?U:luid(),
    <<"rIV-", CurSrvCode/bitstring, "-", CallId/bitstring>>.

build_out_callid(#state_ivr{a=undefined,acallid=ACallId}) when ACallId/=<<>>, ACallId/=undefined -> ACallId;
build_out_callid(#state_ivr{a=#side{callid=ACallId},forks=Forks}=State) when is_list(Forks) ->
    CurSrvCode = ?U:get_current_srv_textcode(),
    CallId = ?U:luid(),
    case lists:keyfind(CallId, 1, Forks) of
        false when ACallId/=CallId -> <<"rIV-", CurSrvCode/bitstring, "-", CallId/bitstring>>;
        _ -> build_out_callid(State)
    end;
build_out_callid(#state_ivr{a=undefined,forks=Forks}=State) when is_list(Forks) ->
    CurSrvCode = ?U:get_current_srv_textcode(),
    CallId = ?U:luid(),
    case lists:keyfind(CallId, 1, Forks) of
        false -> <<"rIV-", CurSrvCode/bitstring, "-", CallId/bitstring>>;
        _ -> build_out_callid(State)
    end;
build_out_callid(#state_ivr{a=undefined}) -> build_out_callid().

%%% ---------------------------------------------
%%% Log to callid-link log
%%% ---------------------------------------------

%% #307, #354, RP-415
log_callid(outgoing,DlgNum,CallId,Domain) ->
    ?BLlog:write({sip,callid}, {"dlg='~s'; callid='~s'; domain='~s'",[DlgNum,CallId,Domain]});
log_callid(incoming,DlgNum,CallId,Domain) ->
    ?BLlog:write({sip,callid}, {"dlg='~s'; callid='~s'; domain='~s'",[DlgNum,CallId,Domain]}).
log_callid(referred,DlgNum,CallId,RCallId,Domain) ->
    ?BLlog:write({sip,callid}, {"dlg='~s'; callid='~s'; rfcallid='~s'; domain='~s'",[DlgNum,CallId,RCallId,Domain]});
log_callid(replaces,DlgNum,CallId,RCallId,Domain) ->
    ?BLlog:write({sip,callid}, {"dlg='~s'; callid='~s'; rpcallid='~s'; domain='~s'",[DlgNum,CallId,RCallId,Domain]}).

%% --------------------------
%% Build IVR display name
%% --------------------------
make_ivr_display(#state_ivr{map=Map}=_State) ->
    D = case lists:keyfind(<<"displayname">>, 1, maps:get(opts,Map)) of
            false -> ?U:make_ivr_display();
            {_,ConfDisp} -> ConfDisp
        end,
    ?U:quote_display(D).

%% --------------------------
%% Send sip response to incoming request
%% --------------------------
send_response(#sipmsg{class={req,_}}=Req, SipReply, State) ->
    {ok,ReqHandle} = nksip_request:get_handle(Req),
    send_response(ReqHandle, SipReply, State);
send_response(ReqHandle, {SipCode,_Opts}=SipReply, State) ->
    d(State, "send_response ~p", [SipCode]),
    ?U:send_sip_reply(fun() -> nksip_request:reply(SipReply, ReqHandle) end),
    State;
send_response(ReqHandle, SipReply, State)  ->
    ?U:send_sip_reply(fun() -> nksip_request:reply(SipReply, ReqHandle) end),
    State.

%% --------------------------
%% Finalize ivr
%% --------------------------
do_finalize_ivr(State) ->
    % life timer
    #state_ivr{timer_ref=TimerRef}=State,
    erlang:cancel_timer(TimerRef),
    % unlink callid
    unlink_callids(State),
    State.

%% @private
%% Unlink callids
unlink_callids(#state_ivr{acallid=ACallId, forks=Frk}=_State) ->
    F = fun(CallId, Acc) ->
                case lists:member(CallId, Acc) of
                    false -> ?DLG_STORE:unlink_dlg(CallId), [CallId|Acc];
                    true -> Acc
                end end,
    X0 = [],
    X1 = F(ACallId, X0),
    _X2 = lists:foldl(fun({CallId,_}, Acc) -> F(CallId, Acc) end, X1, Frk).

%% --------------------------
%% Switch ivr to stopping state
%% --------------------------
return_stopping(State) ->
    ?LOG("STOPPING"),
    % event
    ?EVENT:ivr_stop(State),
    % stop script
    ?ACTIVE_SM:stop_script(State),
    % from media
    {ok,State1} = ?IVR_MEDIA:media_stop(State),
    % timeout
    erlang:send_after(?STOPPING_TIMEOUT, self(), {stopping_timeout}),
    d(State, " -> switch to '~p'", [?STOPPING_STATE]),
    {next_state, ?STOPPING_STATE, State1}.

%% --------------------------
%% finish with error of mgc
%% --------------------------
error_mgc_final(Reason, State, Side) ->
    error_mgc_final(Reason, State, Side, ?InternalError("IVR. MGC error")).
error_mgc_final(Reason, State, #side{rhandle=RHandle}=_Side, SipReply) ->
    case Reason of
        "" -> ok;
        _ -> d(State, "MGC error: ~120p", [Reason])
    end,
    State1 = ?IVR_UTILS:send_response(RHandle, SipReply, State),
    ?IVR_UTILS:return_stopping(?IVR_UTILS:do_finalize_ivr(State1));
error_mgc_final(_Reason, State, undefined, _SipReply) ->
    ?IVR_UTILS:return_stopping(?IVR_UTILS:do_finalize_ivr(State)).

%% --------------------------
%% send bye to participant
%% --------------------------
send_bye(Side, _State) ->
    #side{dhandle=DlgHandle}=Side,
    Opts = [user_agent],
    catch nksip_uac:bye(DlgHandle, [async|Opts]).

%%% ---------------------------------------------
%%% Return objects from DC
%%% ---------------------------------------------

%% --------------------------
%% Get script by code (cached)
%% --------------------------
-spec get_script(Domain :: binary(), Code :: binary(), Type :: 'ivr') -> map() | not_found | undefined.
%% --------------------------
get_script(Domain, Code, 'ivr'=Type) ->
    ?ENVDC:get_script_sticky(Domain, Type, Code).

%% --------------------------
%% Get hunt by id (cached)
%% --------------------------
-spec get_huntsip(Domain :: binary(), HuntId :: binary()) -> map() | not_found | undefined.
%% --------------------------
get_huntsip(Domain, HuntId) ->
    case ?ENVDC:get_object_nosticky(Domain, 'hunt', [{ids,[HuntId]}], auto) of
        {error,undefined} -> undefined;
        {error,_} -> undefined;
        {ok,[],_NewCheckSum} -> not_found;
        {ok,[Item],_NewCheckSum} -> Item
    end.

%% --------------------------
-spec get_b2b_call_info(CallId::binary(),Domain::binary()) -> Info::map().
%% --------------------------
get_b2b_call_info(CallId,Domain) ->
    do_get_b2b_call_info(CallId,Domain,25,6,0).

do_get_b2b_call_info(_,_,_,0,_) -> #{};
do_get_b2b_call_info(CallId,Domain,BaseTimeout,Attempts,Retry) ->
    FNext = fun() ->
                    Timeout = ?EU:growing_pause(Retry,6,BaseTimeout),
                    timer:sleep(Timeout),
                    do_get_b2b_call_info(CallId,Domain,BaseTimeout,Attempts-1,Retry+1)
            end,
    case ?ENVCALL:call_callstore_local(Domain,{search_list,[fcallid,CallId]},undefined) of
        {ok,DlgIds} ->
            case ?ENVCALL:call_callstore_local(Domain,{get_list,DlgIds},undefined) of
                {ok,[CallInfo|_]} when is_map(CallInfo) -> maps:get(data,CallInfo,#{});
                _ -> FNext()
            end;
        _ -> FNext()
    end.
%%% ---------------------------------------------
%%% Make stop reason
%%% ---------------------------------------------

%% --------------------------
reason_error(_, _, #{}=Reason) -> Reason;
reason_error(Operations, <<>>, Reason) ->
    #{type => error,
      operations => Operations,
      reason => Reason};
reason_error(Operations, CallId, Reason) ->
    #{type => error,
      operations => Operations,
      callid => CallId,
      reason => Reason}.

%% --------------------------
reason_timeout(Operations, <<>>, Reason) ->
    #{type => timeout,
      operations => Operations,
      reason => Reason};
reason_timeout(Operations, CallId, Reason) ->
    #{type => timeout,
      operations => Operations,
      callid => CallId,
      reason => Reason}.

%% --------------------------
reason_lastdetach(CallId) ->
    #{type => lastdetach,
      callid => CallId,
      reason => <<"Last participant detached">>}.

%% --------------------------
reason_external(#{}=Reason) -> Reason;
reason_external(Reason) ->
    #{type => external,
      reason => Reason}.

%%% ====================================================================
%%% Internal functions
%%% ====================================================================

%% --------------------------
d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    #state_ivr{ivrid=ConfId}=State,
    ?LOG("IVR fsm ~p '~p':" ++ Fmt, [ConfId,?ACTIVE_STATE] ++ Args).
