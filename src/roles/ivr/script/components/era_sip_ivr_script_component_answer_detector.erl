%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2022 Evgeniy Grebenyuk.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>
%%% @date 10.06.2022 14:11
%%% @doc Компонент "Детектор ответов"

-module(era_sip_ivr_script_component_answer_detector).
-author('Evgeniy Grebenyuk <llceceron@gmail.com>').

%%-export([init/1,
%%         handle_event/2,
%%         terminate/1,
%%         metadata/1,
%%         %
%%         cmd_start_media/2,
%%         cmd_setup_topology/2,
%%         cmd_abort_media/2,
%%         cmd_stop_media/2]).
%%
%%%% ====================================================================
%%%% Define
%%%% ====================================================================
%%
%%%-include_lib("era_script/include/r_script.hrl").
%%-include("../../era_script/include/r_script.hrl").
%%-include_lib("kernel/include/file.hrl").
%%
%%-include("r_sip_ivr.hrl").
%%-include("r_sip_mgc.hrl").
%%
%%-define(U, r_sip_utils).
%%-define(CFG, r_sip_config).
%%-define(SipApp, era_sip).
%%
%%-record(data, {
%%               fsm_state :: starting | performing | stopping | final,
%%               %
%%               recorder_id :: binary() | undefined,
%%               mg_file = <<>> :: binary(),
%%               %
%%               collect_sec :: integer(),
%%               is_save_record=false :: boolean(),
%%
%%               %
%%               mgid :: term(),
%%               mgfilemap :: map(),
%%               detect_worker :: pid(),
%%               detect_result :: term() | undefined, % stored audiomatcher result (if exists), can be missing on crash.
%%               detect_result_info :: term() | undefined,
%%               %
%%               pid :: pid(), % script pid
%%               ref :: reference(),
%%               ticket_timer_ref :: reference() | undefined, % media start/stop timer
%%               collect_timer_ref :: reference()
%%              }).
%%
%%
%%-define(TIMEOUT_TICKET, 10000). % wait for media recording start
%%
%%-define(IvrActiveState, 'active').
%%
%%-define(S_STARTING, starting). % before media recording is started
%%-define(S_PERFORMING, performing). % media is being recorded
%%-define(S_STOPPING, stopping).
%%-define(S_FINAL, final).
%%
%%-define(AdFolder, <<"answer_detector_records">>).
%%
%%-define(SampleRateHz, 8000).
%%-define(MixCodec, "L16 "++?EU:to_list(?SampleRateHz)++" 1").
%%
%%-define(ResultMatchOk, <<"match_ok">>).
%%-define(ResultMatchErr, <<"match_error">>).
%%-define(ResultErr, <<"error">>).
%%
%%%% ====================================================================
%%%% Public functions
%%%% ====================================================================
%%
%%%% ---
%%%% returns descriptions of used properties
%%%% ---
%%metadata(_ScriptType) ->
%%    [{<<"collectTimeSec">>, #{type => <<"argument">>}},
%%     {<<"saveRecord">>, #{type => <<"list">>,
%%                          items => [{0,<<"no">>},
%%                                    {1,<<"yes">>}] }},
%%     {<<"result">>, #{type => <<"variable">>}},
%%     %
%%     {<<"transferFound">>, #{type => <<"transfer">>,
%%                             title => <<"found">>}},
%%     {<<"transferNotFound">>, #{type => <<"transfer">>,
%%                                title => <<"not_found">>}},
%%     {<<"transferError">>, #{type => <<"transfer">>,
%%                             title => <<"error">>}} ].
%%
%%%% ---
%%init(#state_scr{ownerpid=undefined}=State) ->
%%    State1 = assign_field(<<"result">>, <<"no owner">>, State),
%%    ?SCR_COMP:next(<<"transferError">>, State1);
%%init(#state_scr{active_component=Comp}=State) ->
%%    case prepare_param(State) of
%%        {error,Reason} ->
%%            State1 = assign_field(<<"result">>, format_reason(Reason), State),
%%            ?SCR_COMP:next(<<"transferError">>, State1);
%%        {ok,CS} ->
%%            CS1 = CS#data{fsm_state = ?S_STARTING, pid = self(), ref = make_ref()},
%%            State1 = State#state_scr{active_component=Comp#component{state=CS1}},
%%            fsm(init, State1)
%%    end.
%%
%%%% @private
%%format_reason(Reason) when is_atom(Reason) orelse is_binary(Reason) -> ?EU:to_binary(Reason);
%%format_reason(Reason) when is_list(Reason) ->
%%    case io_lib:char_list(Reason) of
%%        true -> ?EU:to_binary(Reason);
%%        false -> ?EU:strbin("~120tp", [Reason])
%%    end.
%%
%%%% @private
%%prepare_param(#state_scr{scriptid=IvrId, active_component=#component{data=CompData}=_Comp}=State) ->
%%    {TS,RecPath} = ?U:get_media_context_rec_opts(),
%%    CollectSec =
%%        case max(0, min(60, ?SCR_ARG:get_int(<<"collectTimeSec">>, CompData, 0, State))) of
%%            0 -> 5000; % default
%%            V -> V*1000
%%        end,
%%    IsSaveRecord = ?EU:to_bool(?SCR_COMP:get(<<"saveRecord">>, CompData, 0)),
%%    MGFile = filename:join([RecPath, ?EU:str("ivrr_~s_~s.rtp", [?EU:to_list(IvrId), ?EU:to_list(TS)])]),
%%    State1 =
%%        #data{recorder_id = ?EU:to_binary(1100000000 + ?EU:random(100000000)),
%%              mg_file = MGFile,
%%              collect_sec = CollectSec,
%%              is_save_record = IsSaveRecord},
%%    {ok, State1}.
%%
%%%% ---
%%handle_event(Event, State) ->
%%    handle_event_1(Event, State).
%%
%%%% ---
%%handle_event_1({'ticket_direction_error',Ref,Err}, #state_scr{active_component=#component{state=#data{ref=Ref}}}=State) ->
%%    fsm({'ticket_direction_error',Err}, State);
%%
%%%% ---
%%handle_event_1({'ticket_started_ok',Ref,MGID}, #state_scr{active_component=#component{state=#data{fsm_state=?S_STARTING,ref=Ref}}}=State) ->
%%    fsm({'ticket_started_ok',MGID}, State);
%%handle_event_1({'ticket_started_error',Ref,Err}, #state_scr{active_component=#component{state=#data{fsm_state=?S_STARTING,ref=Ref}}}=State) ->
%%    fsm({'ticket_started_error',Err}, State);
%%handle_event_1({'ticket_started_timeout',Ref,S}, #state_scr{active_component=#component{state=#data{fsm_state=S=?S_STARTING,ref=Ref}}}=State) ->
%%    fsm('ticket_started_timeout', State);
%%
%%%% ---
%%handle_event_1({'collect_timer',Ref,_}, #state_scr{active_component=#component{state=#data{fsm_state=?S_PERFORMING,ref=Ref}}}=State) ->
%%    fsm('collect_timer', State);
%%
%%%% ---
%%handle_event_1({'ticket_stopped_ok',Ref,S}, #state_scr{active_component=#component{state=#data{fsm_state=S=?S_STOPPING,ref=Ref}}}=State) ->
%%    fsm('ticket_stopped_ok', State);
%%
%%%% ---
%%handle_event_1({'ticket_stopped_error',Ref,Err}, #state_scr{active_component=#component{state=#data{fsm_state=?S_STOPPING,ref=Ref}}}=State) ->
%%    fsm({'ticket_stopped_error',Err}, State);
%%
%%%% ---
%%handle_event_1({'ticket_stopped_timeout',Ref,S}, #state_scr{active_component=#component{state=#data{fsm_state=S=?S_STOPPING,ref=Ref}}}=State) ->
%%    fsm('ticket_stopped_timeout', State);
%%
%%%% ---
%%handle_event_1({'answer_detector_result',Ref,AdResult}, #state_scr{active_component=#component{state=#data{ref=Ref}}}=State) ->
%%    fsm({'answer_detector_result',AdResult}, State);
%%
%%%% ---
%%handle_event_1({'answer_detector_down',Ref,AdWorkerPid,Reason}, #state_scr{active_component=#component{state=#data{detect_worker=Pid,ref=Ref}}}=State)
%%  when AdWorkerPid==Pid ->
%%    fsm({'answer_detector_down',Reason}, State);
%%
%%%% ---
%%handle_event_1(_Event, State) ->
%%    {ok, State}.
%%
%%%% ---
%%terminate(State) ->
%%    abort_internal(State).
%%
%%%% ====================================================================
%%%% Internal FSM implementation
%%%% ====================================================================
%%
%%%% ---
%%-spec fsm(Event::term(), State::#state_scr{}) -> {ok,State::#state_scr{}} | {error,Reason::term()}.
%%%% ---
%%fsm(Event, State) ->
%%    fsm(Event, get_data(State), State).
%%
%%%% ---
%%%% @private
%%%% fsm_state = starting
%%fsm('init', #data{fsm_state=?S_STARTING}, State) -> start_media(State);
%%fsm({'ticket_direction_error',Err}, _Data, State) -> abort({error,{'ticket_direction_error',Err}},State);
%%fsm('start_media', #data{fsm_state=?S_STARTING}, State) -> start_media(State);
%%fsm('start_media_with_timer', #data{fsm_state=?S_STARTING}, State) -> start_media(State);
%%fsm({'ticket_started_ok',MGID}, #data{fsm_state=?S_STARTING}, State) -> on_started_media(MGID, State);
%%fsm({'ticket_started_error',Err}, #data{fsm_state=?S_STARTING}, State) -> abort({error,{'ticket_started_error',Err}}, State);
%%fsm('ticket_started_timeout', #data{fsm_state=?S_STARTING}, State) -> abort({error,'ticket_started_timeout'}, State);
%%%% fsm_state = performing
%%fsm('start_media_state_timer', #data{fsm_state=?S_PERFORMING}, State) -> State;
%%fsm('collect_timer', #data{fsm_state=?S_PERFORMING}, State) -> start_answer_detector(State);
%%%% fsm_state = stopping
%%fsm('ticket_stopped_ok', #data{fsm_state=?S_STOPPING}, State) -> on_stopped_media(State);
%%fsm({'ticket_stopped_error',Err}, #data{fsm_state=?S_STOPPING}, State) -> abort({error,{'ticket_stopped_error',Err}}, State);
%%fsm('ticket_stopped_timeout', #data{fsm_state=?S_STOPPING}, State) -> abort({error,'ticket_stopped_timeout'}, State);
%%%% all states
%%fsm({'answer_detector_down',Reason}, #data{}, State) -> switch(?S_STOPPING,stop_media(put_result(Reason,State)));
%%fsm({'answer_detector_result',Result}, #data{}, State) -> switch(?S_STOPPING,stop_media(put_result(Result,State)));
%%%% unknown events
%%fsm(_Event, _Data, State) ->
%%    {ok, State}.
%%
%%%% ---
%%%% @private
%%%% FSM actions
%%%% ---
%%%%
%%on_started_media(MGID, State) -> do_on_started_media(MGID,c(State)).
%%%%
%%start_media(State) -> do_start_media(c(State)).
%%%%
%%stop_media(State) -> do_stop_media(c(State)).
%%%%
%%setup_topology(State) -> do_setup_topology(c(State)).
%%%%
%%perform(State) -> do_perform(c(State)).
%%%%
%%abort(Error, State) -> do_abort(Error,c(State)).
%%%%
%%start_answer_detector(State) -> do_start_answer_detector(c(State)).
%%%%
%%put_result(Content, State) -> do_put_result(Content, c(State)).
%%%%
%%finalize_file(State) -> do_finalize_file(c(State)).
%%%%
%%on_stopped_media(State) -> do_on_stopped_media(c(State)).
%%%%
%%switch(NewState, {ok,State}) -> switch(NewState, c(State));
%%switch(NewState, State) when is_atom(NewState) ->
%%    Data = get_data(State),
%%    {ok, set_data(Data#data{fsm_state=NewState}, State)}.
%%%%
%%store({Key,Value}, {ok,State}) -> store({Key,Value}, c(State));
%%store({Key,Value}, State) when is_atom(Key) ->
%%    Data = get_data(State),
%%    Data1 = case Key of
%%                'mgid' -> Data#data{mgid=Value}
%%            end,
%%    {ok, set_data(Data1, State)}.
%%%%
%%start_collect_timer({ok,State}) -> start_collect_timer(c(State));
%%start_collect_timer(State) ->
%%    #data{collect_sec=CollectSec,ref=CmdRef} = Data = get_data(State),
%%    TimerRef = ?SCR_COMP:event_after(CollectSec, {'collect_timer',CmdRef,?S_PERFORMING}),
%%    {ok, set_data(Data#data{collect_timer_ref=TimerRef}, State)}.
%%%%
%%stop_ticket_timer({ok,State}) -> stop_ticket_timer(c(State));
%%stop_ticket_timer(State) ->
%%    #data{ticket_timer_ref=TimerRef} = Data = get_data(State),
%%    ?EU:cancel_timer(TimerRef),
%%    {ok, set_data(Data#data{ticket_timer_ref=undefined}, State)}.
%%%%
%%stop_performing_timers({ok,State}) -> stop_performing_timers(c(State));
%%stop_performing_timers(State) ->
%%    #data{collect_timer_ref=TimerRef1}=Data = get_data(State),
%%    ?EU:cancel_timer(TimerRef1),
%%    {ok, set_data(Data#data{collect_timer_ref = undefined}, State)}.
%%
%%%% ---
%%do_on_started_media(MGID,#state_scr{}=State) ->
%%    {ok,State1} = stop_ticket_timer(store({'mgid',MGID}, State)),
%%    case get_mg_file_info(State1) of
%%        false -> do_abort({error,mg_file_info}, State1);
%%        {ok,MGFileMap} ->
%%            #data{}=CS = get_data(State1),
%%            State2 = set_data(CS#data{mgfilemap=MGFileMap}, State1),
%%            setup_topology(perform(State2))
%%    end.
%%
%%%% @private
%%get_mg_file_info(#state_scr{}=State) ->
%%    #data{mgid = MGID,
%%          mg_file = MGFile} = get_data(State),
%%    case ?U:parse_mg_devicename(?U:get_mg_devicename(MGID)) of
%%        undefined -> false;
%%        {Addr,Postfix} ->
%%            RecPath = ?CFG:get_record_path(Addr,Postfix),
%%            SrcPath = filename:join([RecPath|lists:nthtail(1,filename:split(MGFile))]),
%%            case ?CFG:get_mg_node_by_addr_postfix(Addr,Postfix) of
%%                undefined -> false;
%%                {MGSite,MGNode} -> {ok, #{file => MGFile,
%%                                          site => MGSite,
%%                                          node => MGNode,
%%                                          recpath => RecPath,
%%                                          srcpath => SrcPath}}
%%            end end.
%%
%%%% ---
%%do_perform(#state_scr{}=State) ->
%%    switch(?S_PERFORMING,
%%           start_collect_timer(State)).
%%
%%%% ---
%%do_start_media(#state_scr{ownerpid=OwnerPid}=State) ->
%%    #data{ref = CmdRef,
%%          pid = ScriptPid,
%%          recorder_id = RecorderId,
%%          mg_file = File} = CS = get_data(State),
%%    Cmd = cmd_start_media,
%%    OptsMap = #{recorderid => RecorderId,
%%                file => File,
%%                type => "raw",
%%                buffer_duration => 250,
%%                codec_name => "L16/"++?EU:to_list(?SampleRateHz)},
%%    OwnerPid ! {scriptmachine, {ivr_cmd, CmdRef, {ext, ?MODULE, Cmd, ScriptPid, OptsMap}}},
%%    TimerRef = ?SCR_COMP:event_after(?TIMEOUT_TICKET, {'ticket_started_timeout',CmdRef,S=?S_STARTING}),
%%    CS1 = CS#data{fsm_state = S,
%%                  ticket_timer_ref = TimerRef},
%%    {ok, set_data(CS1, State)}.
%%
%%%% @private
%%%% executed in IVR-FSM pid
%%cmd_start_media({_Ref, _FromPid, _Arg}=P, State) ->
%%    do_cmd_start_media(P, State).
%%
%%%% @private
%%%% executed in IVR-FSM pid
%%do_cmd_start_media({Ref, FromPid, Arg}, #state_ivr{}=State1) when is_map(Arg) ->
%%    Opts = Arg,
%%    case attach_recorder(Opts, State1) of
%%        {ok,State2,MGID} ->
%%            ?SCR_COMP:event(FromPid, {'ticket_started_ok',Ref,MGID}),
%%            {next_state, ?IvrActiveState, State2};
%%        {error,_}=Err ->
%%            ?SCR_COMP:event(FromPid, {'ticket_started_error',Ref,Err}),
%%            {next_state, ?IvrActiveState, State1}
%%    end.
%%
%%%% @private
%%%% executed in IVR-FSM pid
%%attach_recorder(Opts, #state_ivr{}=State) ->
%%    case ?IVR_MEDIA:media_attach_recorder(Opts, State) of
%%        {ok,State1} ->
%%            MGID = case ?IVR_MEDIA:get_current_media_link(State1) of
%%                       {ok, #{}=M} -> maps:get(mgid,M,undefined);
%%                       _ -> undefined
%%                   end,
%%            {ok, State1, MGID};
%%        {error,_}=Err -> Err
%%    end.
%%
%%%% ---
%%do_setup_topology(#state_scr{ownerpid=OwnerPid}=State) ->
%%    #data{recorder_id=RecorderId,pid=ScriptPid} = get_data(State),
%%    Cmd = cmd_setup_topology,
%%    OptsMap = #{recorderid => RecorderId},
%%    OwnerPid ! {scriptmachine, {ivr_cmd, make_ref(), {ext, ?MODULE, Cmd, ScriptPid, OptsMap}}},
%%    {ok, State}.
%%
%%%% @private
%%%% executed in IVR-FSM pid
%%cmd_setup_topology({_Ref, _FromPid, Arg}, #state_ivr{a=#side{state=S}}=State) when S==dialog; S==early ->
%%    do_cmd_setup_topology(Arg, State);
%%cmd_setup_topology(_, #state_ivr{}=State) ->
%%    {next_state, ?IvrActiveState, State}.
%%
%%%% @private
%%%% executed in IVR-FSM pid
%%do_cmd_setup_topology(Opts, #state_ivr{acallid=CallId,media=#media_ivr{players=Ps}}=State) ->
%%    RecorderId = maps:get(recorderid,Opts),
%%    Topology0 = [{PlayerId,RecorderId,isolate} || {PlayerId,_} <- Ps],
%%    Topology1 = [{PlayerId,CallId,oneway} || {PlayerId,_} <- Ps],
%%    Topology2 = [{CallId,RecorderId,oneway}],
%%    Topology = Topology0 ++ Topology1 ++ Topology2,
%%    %Topology = [{PlayerId,CallId,isolate} || {PlayerId,_} <- Ps],
%%    case Topology of
%%        [] -> {next_state, ?IvrActiveState, State};
%%        _ ->
%%            case ?IVR_MEDIA:modify_topology(State, Topology) of
%%                {ok,State1} -> {next_state, ?IvrActiveState, State1};
%%                {error,_} -> {next_state, ?IvrActiveState, State}
%%            end end.
%%
%%%% ---
%%do_abort({error,{'ticket_direction_error',Err}}, State) ->
%%    State1 = assign_field(<<"result">>, Err, State),
%%    ?SCR_COMP:next(<<"transferError">>, State1);
%%do_abort(Reason,#state_scr{}=State) ->
%%    {ok,State1} = abort_media(State),
%%    {ok,State2} = abort_internal(State1),
%%    case Reason of
%%        {error,{'ticket_started_error',Err}} -> Err;
%%        {error,'ticket_started_timeout'} -> {error,{internal_error,<<"IVR-FSM response timeout">>}};
%%        {error,{'ticket_stopped_error',Err}} -> Err;
%%        {error,'ticket_stopped_timeout'} -> {error,{internal_error,<<"IVR-FSM response timeout">>}};
%%        {error,'mg_file_info'} ->
%%            State3 = assign_field(<<"result">>, <<"mg_file_info error">>, State2),
%%            ?SCR_COMP:next(<<"transferError">>, State3);
%%        _ ->
%%            ?LOG("~p:do_abort(Reason=~120tp, State)", [?MODULE,Reason]),
%%            State3 = assign_field(<<"result">>, <<"unknown_error">>, State2),
%%            ?SCR_COMP:next(<<"transferError">>, State3)
%%    end.
%%
%%%% @private
%%abort_media(#state_scr{ownerpid=OwnerPid}=State) ->
%%    case get_data(State) of
%%        #data{recorder_id=undefined} -> {ok, State};
%%        #data{recorder_id=RecorderId,ref=CmdRef,pid=ScriptPid}=CS ->
%%            Cmd = cmd_abort_media,
%%            OptsMap = #{recorderid => RecorderId},
%%            OwnerPid ! {scriptmachine, {ivr_cmd, CmdRef, {ext, ?MODULE, Cmd, ScriptPid, OptsMap}}},
%%            {ok, set_data(CS#data{recorder_id=undefined}, State)}
%%    end.
%%
%%%% @private
%%%% executed in IVR-FSM pid
%%cmd_abort_media({_Ref, _FromPid, Arg}, #state_ivr{}=State) ->
%%    Opts = Arg,
%%    State2 = case detach_recorder(Opts, State) of
%%                 {error,_} -> State;
%%                 {ok,State1} -> State1
%%             end,
%%    {next_state, ?IvrActiveState, State2}.
%%
%%%% @private
%%abort_internal(State) ->
%%    {ok,State1} = stop_ticket_timer(stop_performing_timers(State)),
%%    drop_file(terminating, State1).
%%
%%%% ---
%%do_stop_media(#state_scr{ownerpid=OwnerPid}=State) ->
%%    #data{ref = CmdRef,
%%          recorder_id = RecorderId,
%%          pid = ScriptPid} = CS = get_data(State),
%%    Cmd = cmd_stop_media,
%%    OptsMap = #{recorderid => RecorderId},
%%    OwnerPid ! {scriptmachine, {ivr_cmd, CmdRef, {ext, ?MODULE, Cmd, ScriptPid, OptsMap}}},
%%    TimerRef = ?SCR_COMP:event_after(?TIMEOUT_TICKET, {'ticket_stopped_timeout',CmdRef,?S_STOPPING}),
%%    CS1 = CS#data{ticket_timer_ref = TimerRef,
%%                  recorder_id = undefined},
%%    {ok, set_data(CS1, State)}.
%%
%%%% @private
%%%% executed in IVR-FSM pid
%%cmd_stop_media({Ref, FromPid, Arg}, #state_ivr{}=State) ->
%%    Opts = Arg,
%%    StateX = case detach_recorder(Opts, State) of
%%                 {error,_}=Err ->
%%                     ?SCR_COMP:event(FromPid, {'ticket_stopped_error',Ref,Err}),
%%                     State;
%%                 {ok,State1} ->
%%                     ?SCR_COMP:event(FromPid, {'ticket_stopped_ok',Ref,?S_STOPPING}),
%%                     State1
%%             end,
%%    {next_state, ?IvrActiveState, StateX}.
%%
%%%% @private
%%%% executed in IVR-FSM pid
%%detach_recorder(Opts,#state_ivr{}=State) ->
%%    ?IVR_MEDIA:media_detach_recorder(Opts, State).
%%
%%%% ---
%%%% is run on interval_timer
%%do_start_answer_detector(State) ->
%%    #data{pid=ScriptPid,mgfilemap=MgFileMap,ref=Ref}=CS=get_data(State),
%%    FStartAd = fun() ->
%%                       Result = answer_detector_work(MgFileMap,ScriptPid),
%%                       ?SCR_COMP:event(ScriptPid, {'answer_detector_result',Ref,Result})
%%               end,
%%    AdWorkerPid = erlang:spawn(FStartAd),
%%    %
%%    FMonitor = fun(Reason) -> ?SCR_COMP:event(ScriptPid, {answer_detector_down,Ref,AdWorkerPid,{down,Reason}}) end,
%%    ?MONITOR:start_monitor(AdWorkerPid, [FMonitor]),
%%    %
%%    CS1 = CS#data{detect_worker=AdWorkerPid},
%%    {ok, set_data(CS1, State)}.
%%
%%%% ---
%%do_on_stopped_media(#state_scr{}=State) ->
%%    #data{detect_result=DR} = get_data(State),
%%    State1 = stop_ticket_timer(State),
%%    {ok,State2} = switch(?S_FINAL, finalize_file(State1)),
%%    NextTransfer =
%%        case DR of
%%            ?ResultMatchOk -> <<"transferFound">>;
%%            ?ResultMatchErr -> <<"transferNotFound">>;
%%            ?ResultErr -> <<"transferError">>
%%        end,
%%    ?SCR_COMP:next(NextTransfer, State2).
%%
%%%% ---
%%do_finalize_file(#state_scr{}=State) ->
%%    drop_file(normal, State).
%%
%%%% @private
%%drop_file(Mode,#state_scr{}=State) ->
%%    case get_data(State) of
%%        #data{mgfilemap=FileInfo}=CS when FileInfo/=undefined ->
%%            #data{pid=ScriptPid}=CS,
%%            [MGSite,MGNode,MGFilePath] = ?EU:maps_get([site,node,srcpath],FileInfo),
%%            F = fun() -> ?ENVCROSS:call_node({MGSite,MGNode}, {?Rfile, delete, [MGFilePath]}, false) end,
%%            case Mode of
%%                normal -> F();
%%                terminating -> erlang:spawn(fun() -> timer:sleep(5000), F() end)
%%            end,
%%            MGFileName = filename:basename(MGFilePath),
%%            {LocalRecordPath,LocalRecordWavPath} = local_mgfile_paths(MGFileName,ScriptPid),
%%            ?Rfile:delete(LocalRecordPath),
%%            check_save_record(LocalRecordWavPath,CS,State),
%%            {ok, set_data(CS#data{mgfilemap=undefined}, State)};
%%        _R -> {ok, State}
%%    end.
%%
%%%%IsSaveRecord,Result,ResultInfo,LocalRecordWavPath
%%%% @private
%%check_save_record(LocalRecordWavPath,#data{is_save_record=false},_) ->
%%    ?Rfile:delete(LocalRecordWavPath);
%%check_save_record(LocalRecordWavPath,#data{detect_result=?ResultErr},State) ->
%%    #state_scr{domain=Domain}=State,
%%    DestPath = filename:join([?ENVPLAT:rpath(era_files),?AdFolder,Domain,<<"runtime_error">>,filename:basename(LocalRecordWavPath)]),
%%    ?Rfilelib:ensure_dir(DestPath),
%%    ?Rfile:move(LocalRecordWavPath,DestPath);
%%check_save_record(LocalRecordWavPath,CS,State) ->
%%    #data{detect_result_info=ResultInfo}=CS,
%%    #state_scr{domain=Domain}=State,
%%    DestPath = filename:join([?ENVPLAT:rpath(era_files),?AdFolder,Domain,?EU:str("~ts",[ResultInfo]),filename:basename(LocalRecordWavPath)]),
%%    ?Rfilelib:ensure_dir(DestPath),
%%    ?Rfile:move(LocalRecordWavPath,DestPath).
%%
%%%% ---
%%do_put_result(Result, #state_scr{}=State) ->
%%    #data{detect_worker=AdWorkerPid}=Data = get_data(State),
%%    {DetectResult, ResultInfo} =
%%        case Result of
%%            {down, normal} ->
%%                {?ResultErr,<<"no_answer">>};
%%            {down, DownInfo} ->
%%                ?LOG("~p: answer detector worker down: ~120tp", [?MODULE,DownInfo]),
%%                {?ResultErr,<<"answer_detector_down">>};
%%            {?ResultMatchOk,_Result}=Ok -> Ok;
%%            {?ResultMatchErr,_Result}=ErrMatch -> ErrMatch;
%%            {error,ErrInfo} ->
%%                {?ResultErr,ErrInfo}
%%        end,
%%    ?MONITOR:stop_monitor(AdWorkerPid),
%%    State1 = assign_field(<<"result">>, ResultInfo, State),
%%    State2 = set_data(Data#data{detect_worker=undefined,detect_result=DetectResult,detect_result_info=ResultInfo}, State1),
%%    {ok, State2}.
%%
%%%% ====================================================================
%%%% Internal functions
%%%% ====================================================================
%%
%%%% ---
%%%% Answer detector work
%%%% ---
%%answer_detector_work(MgFileMap,ScriptPid) ->
%%    [MgSite,MgNode,MgSrcFilePath] = ?EU:maps_get([site,node,srcpath],MgFileMap),
%%    MgFileName = filename:basename(MgSrcFilePath),
%%    {LocalRecordPath,LocalRecordWavPath} = local_mgfile_paths(MgFileName,ScriptPid),
%%    case ?ENVCOPY:copy_file({MgSite,MgNode},MgSrcFilePath,?EU:to_unicode_list(LocalRecordPath)) of
%%        ok ->
%%            case add_wav_header(LocalRecordPath,LocalRecordWavPath) of
%%                ok ->
%%                    PrivDir = code:priv_dir(?SipApp),
%%                    os:putenv("LD_LIBRARY_PATH",PrivDir++"/rtx_amd"),
%%                    Cmd = <<(?EU:to_binary(PrivDir))/binary,"/rtx_amd/rtx_amd ",(?EU:to_binary(LocalRecordWavPath))/binary>>, % @todo: написать команду и согласовать интерфейс
%%                    case ?EU:cmd_exec(Cmd) of
%%                        {0,StdOutRaw} ->
%%                            StdOut = trim(StdOutRaw),
%%                            ResRaw = binary:split(StdOut,<<" ">>,[global]),
%%                            case ResRaw of
%%                                [<<"human">> =Type|_] -> {?ResultMatchOk,Type};
%%                                [NotHumanType|_] -> {?ResultMatchErr,NotHumanType};
%%                                _ -> {error,<<"answer detector unknown response">>}
%%                            end;
%%                        {ErrCode,ErrInfo} ->
%%                            ErrText = ?EU:strbin("answer detector stopped with err (~p). Reason: ~120tp",[ErrCode,ErrInfo]),
%%                            {error,ErrText}
%%                    end;
%%                {error,ErrInfo} ->
%%                    ErrText = ?EU:strbin("add_wav_header error: ~120tp",[ErrInfo]),
%%                    {error,ErrText}
%%            end;
%%        {error,_ErrInfo}=Err -> Err
%%    end.
%%
%%%% ---
%%add_wav_header(RecordPath,DestPath) ->
%%    {ok,WavBody} = file:read_file(RecordPath),
%%    WavHeader = ?EU:get_wav_header(false,1,16,?SampleRateHz,size(WavBody) div 2),
%%    file:write_file(DestPath,<<WavHeader/binary,WavBody/binary>>).
%%
%%%% ---
%%%% Routines
%%%% ---
%%
%%c(#state_scr{}=State) -> State;
%%c({ok,#state_scr{}=State}) -> c(State).
%%
%%%% ---
%%get_data(#state_scr{active_component=#component{state=Data}}) -> Data.
%%set_data(#data{}=Data, #state_scr{active_component=Comp}=State) -> State#state_scr{active_component=Comp#component{state=Data}}.
%%
%%%% ---
%%local_mgfile_paths(MGFileName,ScriptPid) ->
%%    TempPath = ?EU:to_list(?SCR_FILE:get_script_temp_directory(ScriptPid)),
%%    RecordPath = ?EU:to_unicode_list(filename:join([TempPath,?AdFolder,MGFileName])),
%%    WavRecordPath = ?EU:to_unicode_list(filename:rootname(RecordPath))++".wav",
%%    {RecordPath,WavRecordPath}.
%%
%%%% ---
%%assign_field(Field, Value, #state_scr{active_component=#component{data=Data}}=State) when is_binary(Field) ->
%%    assign(?SCR_COMP:get(Field, Data, <<>>), Value, State).
%%assign(Var, Value, State) ->
%%    case ?SCR_VAR:set_value(Var, Value, State) of
%%        {ok, State1} -> State1;
%%        _R -> State
%%    end.
%%
%%%% --
%%trim(Val) when is_binary(Val) ->
%%    binary:replace(Val,[<<"\n">>,<<"\t">>,<<"\r">>],<<>>,[global]).