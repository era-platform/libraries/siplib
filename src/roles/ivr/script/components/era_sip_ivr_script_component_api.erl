%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 30.09.2020
%%% @doc

-module(era_sip_ivr_script_component_api).
-author('Peter Bukashin <tbotc@yandex.ru>').

%%-compile([export_all,nowarn_export_all]).
%%-export([init/1,
%%         handle_event/2,
%%         terminate/1,
%%         metadata/1]).
%%
%%%% ====================================================================
%%%% Defines
%%%% ====================================================================
%%
%%%-include_lib("era_script/include/r_script.hrl").
%%-include("../../era_script/include/r_script.hrl").
%%
%%-include("r_sip_ivr.hrl").
%%-include("r_sip_nk.hrl").
%%
%%-define(U, r_sip_utils).
%%-define(CFG, r_sip_config).
%%
%%-define(CURRSTATE, 'active').
%%-define(VarPrefix, "apitemp##_").
%%-define(HTTP_PROFILE, 'api_files').
%%-define(HTTP_DEFAULT_TIMEOUT, 60000).
%%
%%-record(api, {
%%    reason :: binary(),
%%    ref, % general command reference
%%    pid, % pid of async process
%%    ivrid, % ivrid of fsm
%%    ivrpid, % pid of ivr fsm
%%    dlgstate_atstart, % dlg state at start (-incoming | -ringing | early | dialog)
%%    key :: binary(), % key of component to check
%%    monref, % monitor reference of async process
%%    manager = undefined :: undefined | tuple(), % connection opts of manager {'websock',Site,Node,ConnPid,ConnId,SessionId} to send messages (not replies)
%%    inside_component = undefined :: undefined | #component{}, % active_component of inside component
%%    inside_transfer_map = undefined :: undefined | map(), % transfer map from inside transfer numbers to called transfer keys
%%    inside_async_terminating = false, % if terminate resulted with async..
%%    http_requests = [], % active http requests as tasks, {RequestId::reference(),Task::map(),Map::map()}
%%    mgid, % link to current mg
%%    %mg_dest = undefined, % {SITE,NODE} of current mg
%%    %mg_temp_dir = undefined, % mg temp directory to clean (:RECORD/Srv-006/2020-10-13/0.10401.337)
%%    timer_cref, % timer message reference
%%    timer_ref % stop timer reference
%%  }).
%%
%%-record(async, {
%%    scriptpid, % pid of script srv
%%    ivrpid, % pid of ivr fsm
%%    ivrid, % ivrid of fsm
%%    ref, % general command reference
%%    opts % initial map of opts
%%}).
%%
%%%% ====================================================================
%%%% Callback functions
%%%% ====================================================================
%%
%%%% returns descriptions of used properties
%%metadata(_ScriptType) ->
%%    [{<<"key">>, #{type => <<"argument">>}},
%%     {<<"params">>, #{type => <<"argument">>}},
%%     {<<"reportTimeoutMs">>, #{type => <<"argument">>,
%%                               default => 3000}},
%%     {<<"varReason">>, #{type => <<"variable">>}},
%%     {<<"transfer">>, #{type => <<"transfer">> }},
%%     {<<"transferTimeout">>, #{type => <<"transfer">>,
%%                               title => <<"timeout">> }},
%%     {<<"transferError">>, #{type => <<"transfer">>,
%%                             title => <<"error">> }} ].
%%
%%%% ---------------------------------------------------------------------
%%
%%init(#state_scr{ownerpid=undefined}=State) ->
%%    Err = {error,{internal_error,<<"Invalid owner">>}},
%%    fin(Err, State);
%%init(#state_scr{ownerpid=OwnerPid, active_component=Comp}=State) ->
%%    % init
%%    CmdRef = make_ref(),
%%    TRef = make_ref(),
%%    OwnerPid ! {scriptmachine, {ivr_cmd, CmdRef, {ext, ?MODULE, async_handler, self(), #{}}}},
%%    TimerRef = ?SCR_COMP:event_after(10000, {'ticket_timeout',CmdRef,TRef}),
%%    Key = ?SCR_ARG:get_string(<<"key">>, Comp#component.data, <<>>, State),
%%    State1 = State#state_scr{active_component=Comp#component{state=#api{ref=CmdRef, key=Key, timer_cref=TRef,timer_ref=TimerRef}}},
%%    {ok, State1}.
%%
%%%% ---------------------------------------
%%handle_event({ticket_timeout,Ref,TRef}, #state_scr{active_component=#component{state=#api{ref=Ref,timer_cref=TRef}}}=State) ->
%%    %?OUT("DEBUG. Ticket timeout"),
%%    State1 = set_reason(<<"ticket_timeout">>,State),
%%    Err = {error,{timeout,<<"Report timeout">>}},
%%    fin(Err, State1);
%%
%%handle_event({ticket,Ref,{error,_}=Err}, #state_scr{active_component=#component{state=#api{ref=Ref, timer_ref=TimerRef}}}=State) ->
%%    %?OUT("DEBUG. Ticket error"),
%%    erlang:cancel_timer(TimerRef),
%%    State1 = set_reason(<<"ticket_error">>,State),
%%    fin(Err, State1);
%%
%%handle_event({ticket,Ref,{ok,Map}}, #state_scr{active_component=#component{state=#api{ref=Ref, timer_ref=TimerRef}=S,data=Data}=Comp}=State) ->
%%    %?OUT("DEBUG. Ticket ok"),
%%    erlang:cancel_timer(TimerRef),
%%    [IvrPid,IvrId,MGID,DlgState] = ?EU:maps_get([pid,ivrid,mgid,dlgstate],Map),
%%    % spawn async
%%    AsyncPid = spawn(fun() -> loop(#async{scriptpid=self(),ivrpid=IvrPid,ivrid=IvrId,ref=Ref,opts=#{}}) end),
%%    % monitor
%%    Flog = fun(StopReason) -> ?SCR_ERROR(State, "Async API-managm process ~500tp terminated: ~500tp", [AsyncPid, StopReason]) end,
%%    Fclear = fun() -> ?SCR_COMP:event(AsyncPid, {'async_crashed', Ref, AsyncPid}) end,
%%    MonRef = ?MONITOR:start_monitor(AsyncPid, "Async API-managm process", [Flog, Fclear]), % for spawned
%%    % report timer
%%    TimeoutMs = erlang:min(30000,erlang:max(0,?SCR_ARG:get_int(<<"reportTimeoutMs">>, Data, 3000, State))),
%%    TRef1 = make_ref(),
%%    TimerRef1 = ?SCR_COMP:event_after(TimeoutMs, {'report_timeout',Ref,TRef1}),
%%    %
%%    State1 = State#state_scr{active_component=Comp#component{state=S#api{pid=AsyncPid,
%%                                                                         ivrpid=IvrPid,
%%                                                                         ivrid=IvrId,
%%                                                                         dlgstate_atstart=DlgState,
%%                                                                         mgid=MGID,
%%                                                                         monref=MonRef,
%%                                                                         timer_cref=TRef1,
%%                                                                         timer_ref=TimerRef1}}},
%%    call_fun_events(api_management_start, State1),
%%    {ok,State1};
%%
%%handle_event({report_timeout,Ref,TRef}, #state_scr{active_component=#component{state=#api{ref=Ref,timer_cref=TRef}}}=State) ->
%%    %?OUT("DEBUG. Report timeout"),
%%    State1 = set_reason(<<"report_timeout">>,State),
%%    fin(timeout, State1);
%%
%%handle_event({async_crashed,Ref,_AsyncPid}, #state_scr{active_component=#component{state=#api{ref=Ref}}}=State) ->
%%    %?OUT("DEBUG. Async crashed"),
%%    State1 = set_reason(<<"async_crashed">>,State),
%%    Err = {error,{timeout,<<"Async API-managm crashed">>}},
%%    fin(Err,State1);
%%
%%%% ---------------------------------------
%%
%%handle_event({ext_event,{apiman,{Key,Map}}=_Message}, #state_scr{active_component=#component{state=#api{key=Key}}}=State) ->
%%    %?OUT("DEBUG. ext_event: ~120tp", [_Message]),
%%    Operation = maps:get(operation,Map,undefined),
%%    got(maps:get(reply_dest,Map,#{})),
%%    case on_ext_event(Operation, Map, State) of
%%        {ok,State1} -> {ok,State1};
%%        {next,Id,State1} ->
%%            State2 = comp_fin(Id,State1),
%%            {ok,State2};
%%        {A,#state_scr{}=State1} ->
%%            State2 = comp_fin(0,State1),
%%            {A,State2};
%%        {A,B,#state_scr{}=State1} ->
%%            State2 = comp_fin(0,State1),
%%            {A,B,State2}
%%    end;
%%
%%handle_event({api_abort,Ref}, #state_scr{active_component=#component{state=#api{ref=Ref}}}=State) ->
%%    fin(ok, State);
%%
%%%% ---------------------------------------
%%
%%%% dtmf from mg (rfc2833, inband), forward to internal
%%handle_event({mg_dtmf,Dtmf}=Event, State) ->
%%    ok = dtmf(Dtmf, State),
%%    handle_event_inside(Event,State);
%%
%%%% dtmf from sip (info), forward to internal
%%handle_event({sip_info,Req}=Event, State) ->
%%    ?U:get_dtmf_from_sipinfo(Req, fun(Dtmf) -> dtmf(Dtmf,State) end, fun() -> ok end),
%%    handle_event_inside(Event,State);
%%
%%%% ---------------------------------------
%%
%%%% on http response/timeout
%%handle_event({'http_response',Ref,ReplyInfo}, #state_scr{active_component=#component{state=#api{ref=Ref}}}=State) ->
%%    apply_http_result(ReplyInfo,State);
%%
%%%% other events forward to internal
%%handle_event(Event, State) ->
%%    handle_event_inside(Event,State).
%%
%%%% ---------------------------------------
%%terminate(#state_scr{active_component=#component{state=#api{ref=Ref,pid=AsyncPid,mgid=MGID}}}=State) ->
%%    %?OUT("DEBUG. terminate"),
%%    AsyncPid ! {abort,Ref},
%%    State1 = set_reason(<<"bye">>,State),
%%    ?IVR_SCRIPT_UTILS:drop_mg_temp_dir(MGID,self()),
%%    call_fun_events(api_management_stop, State1),
%%    {ok, State1}.
%%
%%%% ====================================================================
%%%% Functional iface of SIP IVR dlg (linked to its fsm)
%%%% ====================================================================
%%
%%async_handler({Ref, FromPid, _OptsMap}, #state_ivr{a=#side{state=S},ivrid=IvrId}=StateIvr) when S==dialog; S==early; S==ringing; S==incoming ->
%%    {ok,#{mgid:=MGID}=_MediaInfo} = ?IVR_MEDIA:get_current_media_link(StateIvr),
%%    ?SCR_COMP:event(FromPid, {'ticket',Ref,{ok,#{pid=>self(),
%%                                                 ivrid=>IvrId,
%%                                                 mgid=>MGID,
%%                                                 dlgstate=>S}}}),
%%    {next_state, ?CURRSTATE, StateIvr};
%%async_handler({Ref, FromPid, _OptsMap}, #state_ivr{a=#side{state=S}}=StateIvr) ->
%%    ?SCR_COMP:event(FromPid, {'ticket',Ref,{error,{invalid_operation,?EU:strbin("Call could not be stopped, state mismatch (~s)",[S])}}}),
%%    {next_state, ?CURRSTATE, StateIvr};
%%async_handler({Ref, FromPid, _OptsMap}, StateIvr) ->
%%    ?SCR_COMP:event(FromPid, {'ticket',Ref,{error,{invalid_operation,<<"Call could not be stopped, state mismatch">>}}}),
%%    {next_state, ?CURRSTATE, StateIvr}.
%%
%%%% -------------------------------------------------------------------
%%%% Async process loop
%%%% -------------------------------------------------------------------
%%loop(#async{ref=Ref}) ->
%%    %?OUT("DEBUG. LOOP started"),
%%    receive
%%        {abort,Ref} ->
%%            %?OUT("DEBUG. LOOP aborted"),
%%            ok;
%%        a -> ok
%%    %after 300000 -> ?OUT("DEBUG. LOOP timeout"), ok
%%    end,
%%    ok.
%%
%%
%%%% ====================================================================
%%%% Logic
%%%% ====================================================================
%%
%%%% -------------------------------
%%%% Accepts management
%%%% -------------------------------
%%on_ext_event(accept,Map,#state_scr{active_component=#component{state=#api{manager=undefined,timer_ref=TimerRef}=S}=Comp}=State) ->
%%    %?OUT("DEBUG. accept +"),
%%    erlang:cancel_timer(TimerRef),
%%    Reply = {ok,#{msg => <<"Accepted successfully">>}},
%%    ReplyDest = maps:get(reply_dest,Map,#{}),
%%    Conn = maps:get(conn,Map,undefined),
%%    replied(Reply,ReplyDest),
%%    {ok,State#state_scr{active_component=Comp#component{state=S#api{manager=Conn}}}};
%%on_ext_event(accept,Map,#state_scr{}=State) ->
%%    %?OUT("DEBUG. accept -"),
%%    Reply = {error,<<"Already accepted">>},
%%    ReplyDest = maps:get(reply_dest,Map,#{}),
%%    replied(Reply,ReplyDest),
%%    {ok,State};
%%
%%%% -------------------------------
%%%% Reaccepts management
%%%% -------------------------------
%%on_ext_event(reaccept,Map,#state_scr{active_component=#component{state=#api{manager=undefined}}}=State) ->
%%    %?OUT("DEBUG. reaccept -"),
%%    Reply = {error,<<"Not accepted yet">>},
%%    ReplyDest = maps:get(reply_dest,Map,#{}),
%%    replied(Reply,ReplyDest),
%%    {ok,State};
%%on_ext_event(reaccept,Map,#state_scr{active_component=#component{state=S}=Comp}=State) ->
%%    %?OUT("DEBUG. reaccept +"),
%%    Reply = {ok,#{msg => <<"Reaccepted successfully">>}},
%%    ReplyDest = maps:get(reply_dest,Map,#{}),
%%    Conn = maps:get(conn,Map,undefined),
%%    replied(Reply,ReplyDest),
%%    {ok,State#state_scr{active_component=Comp#component{state=S#api{manager=Conn}}}};
%%
%%%% -------------------------------
%%%% Stops api-management
%%%% -------------------------------
%%on_ext_event(abort,Map,#state_scr{active_component=#component{state=#api{ref=Ref, inside_component=undefined}=S}=Comp}=State) ->
%%    %?OUT("DEBUG. abort +"),
%%    Reply = {ok,#{msg => <<"Aborted successfully">>}},
%%    ReplyDest = maps:get(reply_dest,Map,#{}),
%%    replied(Reply,ReplyDest),
%%    ?SCR_COMP:event(self(),{api_abort,Ref}),
%%    State1 = State#state_scr{active_component=Comp#component{state=S#api{manager=undefined}}},
%%    State2 = set_reason(<<"aborted">>,State1),
%%    {ok,State2};
%%on_ext_event(abort,Map,State) ->
%%    %?OUT("DEBUG. abort -"),
%%    Reply = {error, <<"Abort cancelled, stop component first">>},
%%    ReplyDest = maps:get(reply_dest,Map,#{}),
%%    replied(Reply,ReplyDest),
%%    {ok,State};
%%
%%%% -------------------------------
%%%% Starts new component
%%%% -------------------------------
%%on_ext_event(component_apply,Map,#state_scr{active_component=#component{state=#api{inside_component=undefined}}}=State) ->
%%    %?OUT("DEBUG. component_apply ~120p",[maps:get(data,Map)]),
%%    case catch decorate_component(maps:get(data,Map)) of
%%        CompData when is_list(CompData) ->
%%            %?OUT("DEBUG. decorated"),
%%            case catch ?SCR_COMP:build_component(CompData,State) of
%%                #component{}=InComp0 ->
%%                    %?OUT("DEBUG. build_component: ~120p", [InComp0]),
%%                    {ok,State1} = decorate_component(InComp0,State),
%%                    %?OUT("DEBUG. decorated: ~120p", [((State1#state_scr.active_component)#component.state)#api.inside_component]),
%%                    ReplyDest = maps:get(reply_dest,Map,#{}),
%%                    case init_component_inside(State1) of
%%                        {ok,State2} ->
%%                            %?OUT("DEBUG. inited a"),
%%                            Reply = {ok,#{msg => <<"Inited successfully">>}},
%%                            replied(Reply,ReplyDest),
%%                            {ok,State2};
%%                        {next,Id,State2} ->
%%                            %?OUT("DEBUG. inited b"),
%%                            Reply = {ok,#{msg => <<"Inited successfully">>}},
%%                            replied(Reply,ReplyDest),
%%                            State3 = comp_fin(Id,State2),
%%                            {ok,State3}
%%                    end;
%%                _T ->
%%                    %?OUT("DEBUG. build_component err: ~120p", [_T]),
%%                    Reply = {error,<<"Unknown component type">>},
%%                    ReplyDest = maps:get(reply_dest,Map,#{}),
%%                    replied(Reply,ReplyDest),
%%                    {ok,State}
%%            end;
%%        _T ->
%%            %?OUT("DEBUG. build_component err: ~120p", [_T]),
%%            Reply = {error,<<"Cannot decorate">>},
%%            ReplyDest = maps:get(reply_dest,Map,#{}),
%%            replied(Reply,ReplyDest),
%%            {ok,State}
%%    end;
%%on_ext_event(component_apply,Map,#state_scr{active_component=#component{state=#api{}}}=State) ->
%%    %?OUT("DEBUG. component_apply -"),
%%    Reply = {error, <<"Component is already active">>},
%%    ReplyDest = maps:get(reply_dest,Map,#{}),
%%    replied(Reply,ReplyDest),
%%    {ok,State};
%%
%%%% -------------------------------
%%%% Stops active component
%%%% -------------------------------
%%on_ext_event(component_terminate,Map,#state_scr{active_component=#component{state=#api{inside_component=undefined}}}=State) ->
%%    %?OUT("DEBUG. component_terminate -"),
%%    Reply = {error, <<"Active component not found">>},
%%    ReplyDest = maps:get(reply_dest,Map,#{}),
%%    replied(Reply,ReplyDest),
%%    {ok,State};
%%on_ext_event(component_terminate,Map,#state_scr{active_component=#component{state=#api{inside_async_terminating=true}}}=State) ->
%%    %?OUT("DEBUG. component_terminate -"),
%%    Reply = {error, <<"Active component is already terminating">>},
%%    ReplyDest = maps:get(reply_dest,Map,#{}),
%%    replied(Reply,ReplyDest),
%%    {ok,State};
%%on_ext_event(component_terminate,Map,#state_scr{active_component=#component{state=CS}=Comp}=State) ->
%%    %?OUT("DEBUG. component_terminate +"),
%%    case terminate_inside(State) of
%%        {ok,State1} ->
%%            Reply = {ok,#{msg => <<"Terminated successfully">>}},
%%            ReplyDest = maps:get(reply_dest,Map,#{}),
%%            replied(Reply,ReplyDest),
%%            {ok,State1};
%%        {async,State1} ->
%%            {ok,State1#state_scr{active_component=Comp#component{state=CS#api{inside_async_terminating=true}}}}
%%    end;
%%
%%%% -------------------------------
%%%% Get path
%%%% -------------------------------
%%on_ext_event(internal_get_path,Map,State) ->
%%    %?OUT("DEBUG. internal_get_path +"),
%%    Path = maps:get(key,Map,<<>>),
%%    SrcPath = ?ENVFILE:get_local_path(?SCR_FILE:expand_and_check_path(Path, read, State)),
%%    Reply = {ok,#{path => SrcPath}},
%%    ReplyDest = maps:get(reply_dest,Map,#{}),
%%    replied(Reply,ReplyDest),
%%    {ok,State};
%%
%%%% -------------------------------
%%%% Upload file to temp ivr folder
%%%% -------------------------------
%%on_ext_event(file_upload_temp,Map,State) ->
%%    %?OUT("DEBUG. file_upload_temp +"),
%%    SMPid = self(),
%%    CS = get_data(State),
%%    Url = maps:get('url',Map,<<>>),
%%    FApply = fun({_Ref,{{_Proto,_Code,_Status},Headers,Body}},State0) ->
%%                    % write to local node
%%                    Filename = ?EU:to_unicode_list(fn(Map,Headers,Url)),
%%                    Folder = ?EU:to_list(?SCR_FILE:get_script_temp_directory(SMPid)),
%%                    AbsFilePath = filename:join([Folder,Filename]),
%%                    LFilePath = filename:join([<<":TEMP">>,Filename]),
%%                    ?Rfilelib:ensure_dir(AbsFilePath),
%%                    ?Rfile:write_file(AbsFilePath, Body),
%%                    {ok,LFilePath,State0}
%%            end,
%%    Task = #{pid => self(),
%%             ref => CS#api.ref,
%%             url => Url,
%%             method => <<"GET">>,
%%             headers => lists:map(fun(X) -> ?EU:to_list(X) end, maps:get('headers',Map,[])),
%%             f_apply => FApply},
%%    case start_http(Task) of
%%        {ok,RequestId} ->
%%            CS1 = CS#api{http_requests=[{RequestId,Task,Map} | CS#api.http_requests]},
%%            {ok,set_data(CS1,State)};
%%        {error,R} ->
%%            Reply = {error,R},
%%            ReplyDest = maps:get(reply_dest,Map,#{}),
%%            replied(Reply,ReplyDest),
%%            {ok,State}
%%    end;
%%
%%%% -------------------------------
%%%% Upload file to static folder
%%%% -------------------------------
%%on_ext_event(file_upload_static,Map,State) ->
%%    %?OUT("DEBUG. file_upload_sync +"),
%%    CS = get_data(State),
%%    Url = maps:get('url',Map,<<>>),
%%    FApply = fun({_Ref,{{_Proto,_Code,_Status},Headers,Body}},State0) ->
%%                    % write to local node
%%                    Filename = ?EU:to_binary(fn(Map,Headers,Url)),
%%                    DestPath0 = ?EU:to_binary(maps:get(destpath,Map,<<>>)),
%%                    DestPath = ?EU:to_binary(string:join(string:tokens(string:trim(?EU:to_unicode_list(DestPath0),both,[$/]),"/"),"/")),
%%                    % build paths
%%                    T = <<DestPath/binary,"/",Filename/binary>>,
%%                    LFilePath = case DestPath of
%%                                    <<":SYNC_COMMON/",_/binary>> -> T;
%%                                    <<":SYNC_DOMAIN_DATA/",_/binary>> -> T;
%%                                    <<":SYNC_DOMAIN_COMMON/",_/binary>> -> T;
%%                                    <<":SITESHARE_PUBLIC/",_/binary>> -> T;
%%                                    <<":SITESHARE_DOMAIN_DATA/",_/binary>> -> T;
%%                                    <<":GLOBALSHARE_PUBLIC/",_/binary>> -> T;
%%                                    <<":GLOBALSHARE_DOMAIN_DATA/",_/binary>> -> T;
%%                                    %
%%                                    <<":SYN_MEDIA/",_/binary>> -> T;
%%                                    <<":SYN_DOM/",_/binary>> -> T;
%%                                    <<":SYN_DOM_MEDIA/",_/binary>> -> T;
%%                                    <<":SS_PUB/",_/binary>> -> T;
%%                                    <<":SS_DOM/",_/binary>> -> T;
%%                                    <<":GS_PUB/",_/binary>> -> T;
%%                                    <<":GS_DOM/",_/binary>> -> T;
%%                                    %
%%                                    <<":SYNC_COMMON">> -> T;
%%                                    <<":SYNC_DOMAIN_DATA">> -> T;
%%                                    <<":SYNC_DOMAIN_COMMON">> -> T;
%%                                    <<":SITESHARE_PUBLIC">> -> T;
%%                                    <<":SITESHARE_DOMAIN_DATA">> -> T;
%%                                    <<":GLOBALSHARE_PUBLIC">> -> T;
%%                                    <<":GLOBALSHARE_DOMAIN_DATA">> -> T;
%%                                    %
%%                                    <<":SYN_MEDIA">> -> T;
%%                                    <<":SYN_DOM">> -> T;
%%                                    <<":SYN_DOM_MEDIA">> -> T;
%%                                    <<":SS_PUB">> -> T;
%%                                    <<":SS_DOM">> -> T;
%%                                    <<":GS_PUB">> -> T;
%%                                    <<":GS_DOM">> -> T;
%%                                    %
%%                                    _ -> <<":SYNC_DOMAIN_DATA/",Filename/binary>>
%%                                end,
%%                    RelFilePath = ?SCR_FILE:expand_and_check_path(LFilePath, read, State),
%%                    AbsFilePath = ?ENVFILE:get_local_path(RelFilePath),
%%                    % put on local machine
%%                    ?Rfilelib:ensure_dir(AbsFilePath),
%%                    ?Rfile:write_file(AbsFilePath, Body),
%%                    % put on mg machine to faster sync
%%                    case maps:get('mg',Map,true) of
%%                        true ->
%%                            % write to mg's relative dir
%%                            FileInfo = #{filepath => AbsFilePath,
%%                                         relfilepath => RelFilePath,
%%                                         dest => sync},
%%                            #api{mgid=MGID} = get_data(State0),
%%                            case ?IVR_SCRIPT_UTILS:push_file_to_mg(MGID,FileInfo) of
%%                                false -> {ok,<<"error">>,[{<<"msg">>, <<"Internal MG copy_file error">>}]};
%%                                {ok,_MGFileInfo} -> {ok,LFilePath,State}
%%                            end;
%%                        _ -> {ok,LFilePath,State0}
%%                    end
%%            end,
%%    Task = #{pid => self(),
%%             ref => CS#api.ref,
%%             url => Url,
%%             method => <<"GET">>,
%%             headers => lists:map(fun(X) -> ?EU:to_list(X) end, maps:get('headers',Map,[])),
%%             f_apply => FApply},
%%    case start_http(Task) of
%%        {ok,RequestId} ->
%%            CS1 = CS#api{http_requests=[{RequestId,Task,Map} | CS#api.http_requests]},
%%            {ok,set_data(CS1,State)};
%%        {error,R} ->
%%            Reply = {error,R},
%%            ReplyDest = maps:get(reply_dest,Map,#{}),
%%            replied(Reply,ReplyDest),
%%            {ok,State}
%%    end;
%%
%%%% -------------------------------
%%%% Other
%%%% -------------------------------
%%on_ext_event(_,_,State) ->
%%    {ok,State}.
%%
%%
%%%% -----------------------------------------
%%%% Inside components
%%%% -----------------------------------------
%%
%%%% init by internal component
%%init_component_inside(#state_scr{active_component=Comp}=State) ->
%%    #api{inside_component=#component{module=Module}=InComp}=CS = get_data(State),
%%    F = fun(#state_scr{active_component=InCompX}=StateX) ->
%%                StateX#state_scr{active_component=Comp#component{state=CS#api{inside_component=InCompX}}}
%%        end,
%%    case Module:init(State#state_scr{active_component=InComp}) of
%%        {ok,State1} -> {ok,F(State1)};
%%        {next,Id,State1} ->
%%            {ok,State2} = do_terminate(Module,State1),
%%            {next,Id,F(State2)};
%%        {A,#state_scr{}=State1} ->
%%            {ok,State2} = do_terminate(Module,State1),
%%            {A,F(State2)};
%%        {A,B,#state_scr{}=State1} ->
%%            {ok,State2} = do_terminate(Module,State1),
%%            {A,B,F(State2)}
%%    end.
%%
%%%% handle by internal component
%%handle_event_inside(_, #state_scr{active_component=#component{state=#api{inside_component=undefined}}}=State) -> {ok,State};
%%handle_event_inside(Event, #state_scr{active_component=#component{state=#api{inside_component=#component{module=Module}=InComp}=CS}=Comp}=State) ->
%%    %?OUT("DEBUG. forward ~120p", [Event]),
%%    F = fun(#state_scr{active_component=InCompX}=StateX) ->
%%                StateX#state_scr{active_component=Comp#component{state=CS#api{inside_component=InCompX}}}
%%        end,
%%    case catch Module:handle_event(Event, State#state_scr{active_component=InComp}) of
%%        {'EXIT',_}=Ex ->
%%            ?LOG("DEBUG. ~120p",[Ex]),
%%            {ok,State};
%%        {ok,State1} -> {ok,F(State1)};
%%        {next,Id,State1} ->
%%            case do_terminate(Module,State1) of
%%                {ok,State2} ->
%%                    State3 = comp_fin(Id,F(State2)),
%%                    {ok,State3};
%%                {async,State2} ->
%%                    {ok,F(State2)}
%%            end;
%%        {A,#state_scr{}=State1} ->
%%            {ok,State2} = do_terminate(Module,State1),
%%            State3 = comp_fin(0,F(State2)),
%%            {A,State3};
%%        {A,B,#state_scr{}=State1} ->
%%            {ok,State2} = do_terminate(Module,State1),
%%            State3 = comp_fin(0,F(State2)),
%%            {A,B,State3}
%%    end.
%%
%%%% terminate internal component
%%terminate_inside(#state_scr{active_component=#component{state=#api{inside_component=#component{module=Module}=InComp}=CS}=Comp}=State) ->
%%    F = fun(#state_scr{active_component=InCompX}=StateX) ->
%%                StateX#state_scr{active_component=Comp#component{state=CS#api{inside_component=InCompX}}}
%%        end,
%%    case catch do_terminate(Module,State#state_scr{active_component=InComp}) of
%%        {'EXIT',_}=Ex ->
%%            ?LOG("DEBUG. ~120p",[Ex]),
%%            {ok,State};
%%        {ok,State2} ->
%%            State3 = comp_fin(0,F(State2)),
%%            {ok,State3};
%%        {async,State2} ->
%%            {async,F(State2)}
%%    end.
%%
%%%% when internal component finished
%%comp_fin(IdNext, #state_scr{variables=Vars,active_component=#component{state=CS}=Comp}=State) ->
%%    #api{inside_component=InComp,inside_transfer_map=TrMap,manager=Conn}=CS,
%%    #component{id=Id,type=Type} = InComp,
%%    FVarVal = fun(#variable{value=Val}) when is_integer(Val) -> Val;
%%                 (#variable{id=VarId}) -> ?SCR_ARG:get_string([{<<"argType">>,2},{<<"variable">>,VarId}],<<>>,State)
%%              end,
%%    ResultVars = lists:filtermap(fun(#variable{name= <<?VarPrefix,Key/binary>>}=Var) -> {true,{Key,FVarVal(Var)}};
%%                                    (_) -> false
%%                                 end, Vars),
%%    Opts = [{<<"oId">>, Id},
%%            {<<"oType">>, Type},
%%            {<<"transferKey">>, maps:get(IdNext,TrMap,<<"undefined">>)},
%%            {<<"results">>, ResultVars}],
%%    Opts1 = decorate_result_by_type(Type,Opts,ResultVars),
%%    %?OUT("DEBUG. fin Opts: ~120p", [Opts1]),
%%    send_event(Conn,{<<"ivrapi_component_stopped">>,Opts1},State),
%%    Vars1 = lists:filter(fun(#variable{name = <<?VarPrefix,_/binary>>}) -> false; (_) -> true end, Vars),
%%    State#state_scr{variables=Vars1,
%%                    active_component=Comp#component{state=CS#api{inside_component = undefined,
%%                                                                 inside_transfer_map = undefined,
%%                                                                 inside_async_terminating = false}}}.
%%
%%%% -----------------------------------------
%%%% Decorates
%%%% -----------------------------------------
%%
%%%% -------------------------------
%%%% before internal component built
%%%% -------------------------------
%%decorate_component(Data) when is_map(Data) ->
%%    maps:to_list(maps:merge(#{<<"name">> => <<"API-inner">>,<<"oId">> => 0}, Data));
%%decorate_component(Data) when is_list(Data) ->
%%    lists:ukeymerge(1,lists:usort(Data),[{<<"name">>,<<"API-inner">>},{<<"oId">>,0}]).
%%
%%%% -------------------------------
%%%% before internal component inited
%%%% -------------------------------
%%decorate_component(#component{module=Module,data=Data}=InComp,#state_scr{type=ScrType,variables=Vars,active_component=#component{state=#api{}=CS}=Comp}=State) ->
%%    Metadata = Module:metadata(ScrType),
%%    {Data1,Vars1,TrMap1} = lists:foldl(fun({Key,Opts},{Data0,Vars0,TrMap0}=Acc) ->
%%                                                case maps:get(type,Opts) of
%%                                                    <<"variable">> ->
%%                                                        VId = ?EU:newid(),
%%                                                        Var = #variable{id = VId,
%%                                                                        name = <<?VarPrefix,(?EU:to_binary(Key))/binary>>,
%%                                                                        location = 'local',
%%                                                                        type = 'string',
%%                                                                        value = <<"">>},
%%                                                        {lists:keystore(Key,1,Data0,{Key,VId}),[Var|Vars0],TrMap0};
%%                                                    <<"transfer">> ->
%%                                                        Next = erlang:phash2(Key),
%%                                                        {lists:keystore(Key,1,Data0,{Key,Next}),Vars0,TrMap0#{Next => Key}};
%%                                                    _ -> Acc
%%                                                end end, {Data,Vars,#{}}, Metadata),
%%    Data2 = decorate_component_by_type(?EU:get_by_key(<<"oType">>,Data1),Data1),
%%    InComp1 = InComp#component{data=Data2},
%%    State1 = State#state_scr{variables=Vars1,
%%                             active_component=Comp#component{state=CS#api{inside_component=InComp1,
%%                                                                          inside_transfer_map=TrMap1}}},
%%    {ok,State1}.
%%
%%%% -------------------------------------------------------------
%%%% Decorates component data before init
%%%% -------------------------------------------------------------
%%%% record
%%decorate_component_by_type(208,Data) ->
%%    lists:keystore(<<"pathToVarMode">>, 1, Data, {<<"pathToVarMode">>, 1});
%%decorate_component_by_type(_,Data) -> Data.
%%
%%
%%%% -------------------------------------------------------------
%%%% Decorates component result after terminate
%%%% -------------------------------------------------------------
%%%% record
%%decorate_result_by_type(208,Opts,ResultVars) ->
%%    case ?EU:get_by_key(<<"varPath">>,ResultVars,<<>>) of
%%        <<>> -> Opts;
%%        _VarPath ->
%%            %?OUT("DEBUG. VarPath: ~120p", [_VarPath]),
%%            Opts
%%    end;
%%decorate_result_by_type(_,Opts,_) -> Opts.
%%
%%%% ====================================================================
%%%% Internal functions
%%%% ====================================================================
%%
%%%% finalize
%%fin({error,_}=Err, _State) -> Err;
%%fin(timeout, State) -> ?SCR_COMP:next(<<"transferTimeout">>,State);
%%fin(ok, State) -> ?SCR_COMP:next(<<"transfer">>,State).
%%
%%%% set reason of stop
%%set_reason(Reason, #state_scr{active_component=#component{state=#api{reason=undefined}=S}=Comp}=State) ->
%%    State#state_scr{active_component=Comp#component{state=S#api{reason = Reason}}};
%%set_reason(_, State) -> State.
%%
%%%% -----
%%%% @private
%%get_data(#state_scr{active_component=#component{state=CS}}) -> CS.
%%%% @private
%%set_data(#api{}=CS,#state_scr{active_component=Comp}=State) -> State#state_scr{active_component=Comp#component{state=CS}}.
%%
%%%% generate dtmf event
%%dtmf(_,#state_scr{active_component=#component{state=#api{manager=undefined}}}) -> ok;
%%dtmf(Dtmf,#state_scr{active_component=#component{state=#api{inside_component=undefined,manager=Conn}}}=State) ->
%%    %?OUT("DEBUG. dtmf ~120p", [Dtmf]),
%%    Opts = [{<<"mode">>, <<"idle">>},
%%            {<<"dtmf">>, ?EU:to_binary(Dtmf)}],
%%    send_event(Conn,{<<"ivrapi_dtmf">>,Opts},State),
%%    ok;
%%dtmf(Dtmf,#state_scr{active_component=#component{state=#api{inside_component=InComp,manager=Conn}}}=State) ->
%%    %?OUT("DEBUG. dtmf ~120p", [Dtmf]),
%%    #component{id = Id} = InComp,
%%    Opts = [{<<"mode">>, <<"component">>},
%%            {<<"oId">>, Id},
%%            {<<"dtmf">>, ?EU:to_binary(Dtmf)}],
%%    send_event(Conn,{<<"ivrapi_dtmf">>,Opts},State),
%%    ok.
%%
%%%% internal component terminate decorator
%%do_terminate(Module,State) ->
%%    case ?EU:function_exported(Module,terminate,1) of
%%        true -> Module:terminate(State);
%%        false -> {ok,State}
%%    end.
%%
%%
%%%% --------------------------------------------------------------------
%%%% ivrevents generator
%%%% --------------------------------------------------------------------
%%%% send events to ivr dlg
%%call_fun_events(api_management_start, #state_scr{active_component=#component{state=#api{}=CS}=Comp}=State) ->
%%    MapOpts = get_mapopts(State),
%%    MapOpts1 = MapOpts#{params => ?SCR_ARG:get_string(<<"params">>, Comp#component.data, <<>>, State),
%%                        state => CS#api.dlgstate_atstart},
%%    ?SCRU:make_parent_event(api_management_start, MapOpts1, State);
%%
%%call_fun_events(api_management_stop, #state_scr{active_component=Comp}=State) ->
%%    MapOpts = get_mapopts(State),
%%    MapOpts1 = MapOpts#{reason => (Comp#component.state)#api.reason},
%%    ?SCRU:make_parent_event(api_management_stop, MapOpts1, State);
%%
%%call_fun_events(api_management_info, #state_scr{active_component=#component{state=#api{}=CS}=Comp}=State) ->
%%    MapOpts = get_mapopts(State),
%%    MapOpts1 = MapOpts#{params => ?SCR_ARG:get_string(<<"params">>, Comp#component.data, <<>>, State),
%%                        state => CS#api.dlgstate_atstart},
%%    ?SCRU:make_parent_event(api_management_info, MapOpts1, State).
%%
%%%% @private
%%get_mapopts(#state_scr{script=Script, active_component=Comp}=State) ->
%%    #{componentid => Comp#component.id,
%%      componentname => Comp#component.name,
%%      code => maps:get(code,Script,<<>>),
%%      name => maps:get(name,Script,<<>>),
%%      key => ?SCR_ARG:get_string(<<"key">>, Comp#component.data, <<>>, State)}.
%%
%%
%%%% --------------------------------------------------------------------
%%%% Send reports to manager
%%%% --------------------------------------------------------------------
%%
%%%% send 'got' msg to caller
%%got(ReplyDest) ->
%%    case maps:get(pid,ReplyDest,undefined) of
%%        undefined -> ok;
%%        Pid ->
%%            [Site,Node,Ref] = ?EU:maps_get([site,node,ref],ReplyDest),
%%            ?ENVCROSS:call_node({Site,Node},{?EU,send_to_pid,[Pid,{got,Ref}]},undefined,1000)
%%    end.
%%
%%%% send 'replied' msg to caller
%%replied(Reply,ReplyDest) ->
%%    case maps:get(pid,ReplyDest,undefined) of
%%        undefined -> ok;
%%        Pid ->
%%            [Site,Node,Ref] = ?EU:maps_get([site,node,ref],ReplyDest),
%%            ?ENVCROSS:call_node({Site,Node},{?EU,send_to_pid,[Pid,{replied,Ref,Reply}]},undefined,1000)
%%    end.
%%
%%%% send event into manager's connection over r_ws_notify module
%%send_event(Conn,{EventName,Opts}=_Msg,#state_scr{active_component=#component{state=#api{key=Key,ivrid=IvrId}}}) when is_binary(EventName), is_list(Opts) ->
%%    %?OUT("DEBUG. send_event ~120p", [_Msg]),
%%    Opts1 = lists:merge(Opts,[{<<"key">>,Key},{<<"ivrid">>,IvrId}]),
%%    ?ENVCALL:cast_webserver(Conn,{'token_ivrapi',{EventName,Opts1}}).
%%
%%%% -------------------------------------------------------------
%%%% HTTP file operations
%%%% -------------------------------------------------------------
%%start_http(Task) ->
%%    http_init(),
%%    case catch http_request(Task) of
%%        {ok,RequestId} -> {ok,RequestId};
%%        {error,_}=Err -> Err;
%%        {'EXIT',Err} -> {error,Err}
%%    end.
%%
%%%% @private
%%http_init() ->
%%    inets:start(),
%%    inets:start(httpc, [{profile,?HTTP_PROFILE}]),
%%    Options = [{max_sessions,10000},
%%               {keep_alive_timeout,5000}],
%%    httpc:set_options(Options,?HTTP_PROFILE).
%%
%%%% @private
%%-spec http_request(Task::map()) -> {ok,RequestId::reference()} | {error,Reason::term()}.
%%http_request(Task) ->
%%    [Pid,Ref,Url0] = ?EU:maps_get(['pid','ref','url'],Task),
%%    Url = ?EU:to_list(Url0),
%%    Opts = [{sync,false},
%%            {receiver,fun(ReplyInfo) -> ?SCR_COMP:event(Pid,{'http_response',Ref,ReplyInfo}) end},
%%            {full_result,true}],
%%    Headers = maps:get('headers',Task,[]),
%%    HttpOpts = case maps:get('timeout',Task,?HTTP_DEFAULT_TIMEOUT) of
%%                   T when T >= 1000 -> [{timeout, T}];
%%                   _ -> [{timeout, ?HTTP_DEFAULT_TIMEOUT}]
%%               end,
%%    case maps:get(method,Task,<<"GET">>) of
%%        <<"GET">> ->
%%            httpc:request(get, {Url, Headers}, HttpOpts, Opts, ?HTTP_PROFILE);
%%        <<"POST">> ->
%%            ContentType = maps:get(content_type,Task),
%%            Body = maps:get(body,Task),
%%            httpc:request(get, {Url, Headers, ContentType, Body}, HttpOpts, Opts, ?HTTP_PROFILE)
%%    end.
%%
%%%% ------------
%%apply_http_result({Ref,{error,timeout}}=_ReplyInfo,#state_scr{}=State) ->
%%    %?OUT("DEBUG. apply_http_result timeout"),
%%    CS = get_data(State),
%%    case lists:keytake(Ref,1,CS#api.http_requests) of
%%        false -> {ok,State};
%%        {value,{_,_,Map},Rest} ->
%%            Reply = {error,<<"HTTP response timeout">>},
%%            ReplyDest = maps:get(reply_dest,Map,#{}),
%%            replied(Reply,ReplyDest),
%%            {ok,set_data(CS#api{http_requests=Rest},State)}
%%    end;
%%
%%%%
%%apply_http_result({Ref,{error,R}}=_ReplyInfo,#state_scr{}=State) ->
%%    %?OUT("DEBUG. apply_http_result error: ~120p", [R]),
%%    CS = get_data(State),
%%    case lists:keytake(Ref,1,CS#api.http_requests) of
%%        false -> {ok,State};
%%        {value,{_,_,Map},Rest} ->
%%            Reply = {error,?EU:strbin("HTTP error: ~120tp",[R])},
%%            ReplyDest = maps:get(reply_dest,Map,#{}),
%%            replied(Reply,ReplyDest),
%%            {ok,set_data(CS#api{http_requests=Rest},State)}
%%    end;
%%%%
%%apply_http_result({Ref,{{_Proto,Code,Status},_Headers,_Body}}=ReplyInfo,#state_scr{}=State) ->
%%    %?OUT("DEBUG. apply_http_result: ~120tp", [{Code,Status}]),
%%    CS = get_data(State),
%%    case lists:keytake(Ref,1,CS#api.http_requests) of
%%        false -> {ok,State};
%%        {value,{_,Task,Map},Rest} ->
%%            case Code of
%%                _ when Code>=200, Code<300 ->
%%                    Fapply = maps:get(f_apply,Task),
%%                    {ok,Res,State1} = Fapply(ReplyInfo,State),
%%                    CS1 = get_data(State1),
%%                    Reply = {res,{Res,[]}},
%%                    ReplyDest = maps:get(reply_dest,Map,#{}),
%%                    replied(Reply,ReplyDest),
%%                    {ok,set_data(CS1#api{http_requests=Rest},State1)};
%%                _ ->
%%                    Reply = {error,?EU:strbin("HTTP error: '~120tp ~ts'",[Code,Status])},
%%                    ReplyDest = maps:get(reply_dest,Map,#{}),
%%                    replied(Reply,ReplyDest),
%%                    {ok,set_data(CS#api{http_requests=Rest},State)}
%%            end
%%    end.
%%
%%%% ------------
%%%% extracts filename for new file
%%%% ------------
%%fn(Map,Headers,Url) ->
%%    case fn_params(Map) of
%%        <<>> ->
%%            case fn_headers(Headers) of
%%                <<>> -> fn_url(Url);
%%                FN -> FN
%%            end;
%%        FN -> FN
%%    end.
%%%% @private
%%fn_params(Map) -> maps:get('filename',Map,<<>>).
%%%% @private
%%fn_headers(Headers) ->
%%    case lists:keyfind("content-disposition",1,lists:map(fun({H,V}) -> {string:to_lower(H),V} end, Headers)) of
%%        {_,V} ->
%%            case lists:filtermap(fun(A) -> case string:tokens(string:trim(A),"=") of ["filename",FN] -> {true,FN}; _ -> false end end, string:tokens(V,";")) of
%%                [FN|_] -> ?EU:to_binary(FN);
%%                _ -> <<>>
%%            end;
%%        false -> <<>>
%%    end.
%%%% @private
%%fn_url(Url) -> ?EU:to_binary(filename:basename(Url)).
%%%% @private
%%fn_temp() -> ?EU:luid().
