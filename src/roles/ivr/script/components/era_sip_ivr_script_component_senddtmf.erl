%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2022 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 27.06.2022
%%% @doc
%%% @todo @ivr

-module(era_sip_ivr_script_component_senddtmf).
-author('Peter Bukashin <tbotc@yandex.ru>').

%%-compile([export_all,nowarn_export_all]).
%%-export([init/1,
%%         handle_event/2,
%%         metadata/1]).
%%
%%%% ====================================================================
%%%% Defines
%%%% ====================================================================
%%
%%%-include("../include/r_script.hrl").
%%-include("../../era_script/include/r_script.hrl").
%%
%%-include("../include/r_sip_ivr.hrl").
%%-include("../include/r_sip_nk.hrl").
%%
%%-define(U, r_sip_utils).
%%
%%-define(CURRSTATE, 'active').
%%
%%-define(TIMEOUT_TICKET, 3000).
%%
%%-define(SendRfc2833EvtID, 1240).
%%
%%-define(Duration, 200).
%%-define(SymbolInterval, ?Duration + 300).
%%
%%-record(dtmfstate, {
%%    pid :: pid(),
%%    ref :: reference(),
%%    general_ref :: reference(),
%%    general_timerref :: reference(),
%%    dtmf = <<>> :: binary(),
%%    proto = 1 :: 1 | 2,
%%    ticket_timer_ref :: reference(),
%%    tone_ref :: reference()
%%  }).
%%
%%%% ====================================================================
%%%% Callback functions (script)
%%%% ====================================================================
%%
%%% returns descriptions of used properties
%%metadata(_ScriptType) ->
%%    [{<<"dtmf">>, #{type => <<"argument">>}},
%%     {<<"proto">>, #{type => <<"list">>,
%%                     items => [{1,<<"rfc2833">>},
%%                               {2,<<"sip-info">>}],
%%                     default => 1 }},
%%     {<<"transfer">>, #{type => <<"transfer">> }},
%%     {<<"transferError">>, #{type => <<"transfer">>,
%%                             title => <<"error">> }} ].
%%
%%%% ---------------------------------------------------------------------
%%init(#state_scr{ownerpid=undefined}=State) ->
%%    ?SCR_COMP:next([<<"transferError">>, <<"transfer">>], State);
%%init(#state_scr{active_component=#component{data=CompData}=Comp}=State) ->
%%    % properties
%%    DTMF = ?SCR_ARG:get_string(<<"dtmf">>, CompData, <<>>, State),
%%    CS = #dtmfstate{dtmf = DTMF,
%%                    proto = ?SCR_COMP:get(<<"proto">>, CompData, 1)},
%%    case check_symbols(DTMF) of
%%        false -> ?SCR_COMP:next([<<"transferError">>, <<"transfer">>], State);
%%        true ->
%%            % @debug
%%            % CS = CS_#dtmfstate{maxcount=5,interrupts=[<<"1">>,<<"2">>,<<"3">>]},
%%            % init state
%%            GenRef = make_ref(),
%%            GenTimeout = size(DTMF) * 1000,
%%            GenTimerRef = ?SCR_COMP:event_after(GenTimeout, {'general_timeout',GenRef}),
%%            State1 = State#state_scr{active_component=Comp#component{state=CS#dtmfstate{pid = self(),
%%                                     ref = make_ref(),
%%                                     general_ref=GenRef,
%%                                     general_timerref=GenTimerRef}}},
%%            %?OUT("DTMF: ~120tp", [DTMF]),
%%            send_dtmf(State1)
%%    end.
%%
%%%% ---------------------------------------
%%
%%handle_event({'general_timeout',Ref}, #state_scr{active_component=#component{state=#dtmfstate{general_ref=Ref}}}=State) ->
%%    fin(general_timeout,State);
%%
%%%% ----
%%%% ticket mgc error (s = wait_ticket)
%%handle_event({ticket,CmdRef,{error,_}=Err}, #state_scr{active_component=#component{state=#dtmfstate{ref=CmdRef,ticket_timer_ref=TimerRef}}}=State) ->
%%    %?OUT("Ticket err: ~120tp",[Err]),
%%    erlang:cancel_timer(TimerRef),
%%    fin(Err, State);
%%
%%handle_event({ticket,CmdRef,ok}, #state_scr{active_component=#component{state=#dtmfstate{ref=CmdRef,ticket_timer_ref=TimerRef}=CS}}=State) ->
%%    %?OUT("Ticket ok"),
%%    erlang:cancel_timer(TimerRef),
%%    ToneRef = make_ref(),
%%    ?SCR_COMP:event_after(?SymbolInterval,{'next_tone',ToneRef}),
%%    CS1 = CS#dtmfstate{tone_ref = ToneRef},
%%    {ok,set_data(CS1,State)};
%%
%%%% ----
%%handle_event({'next_tone',Ref}, #state_scr{active_component=#component{state=#dtmfstate{tone_ref=Ref,dtmf=DTMF}}}=State) ->
%%    case DTMF of
%%        <<>> -> ?SCR_COMP:next([<<"transfer">>], State);
%%        _ -> send_dtmf(State)
%%    end;
%%
%%%% ----
%%handle_event({rfc2833_sent, _CtxTermId, _Event}, State) ->
%%    {ok,State};
%%
%%%%% dtmf from mg (rfc2833, inband)
%%%%handle_event({mg_dtmf,Dtmf}, State) ->
%%%%    dtmf(Dtmf, State);
%%%%
%%%%% dtmf from sip (info)
%%%%handle_event({sip_info,Req}, State) ->
%%%%    ?U:get_dtmf_from_sipinfo(Req, fun(Dtmf) -> dtmf(Dtmf,State) end, fun() -> {ok,State} end);
%%
%%%% other
%%handle_event(_Ev, State) -> {ok, State}.
%%
%%%% ---------------------------------------
%%
%%terminate(State) -> {ok, stop_timers(State)}.
%%
%%%% ====================================================================
%%%% Functional iface of SIP IVR dlg (linked to its fsm)
%%%% ====================================================================
%%
%%send_dtmf(#state_scr{ownerpid=OwnerPid}=State) ->
%%    #dtmfstate{ref=CmdRef,
%%               proto=Proto,
%%               dtmf=DTMF}=CS = get_data(State),
%%    <<C:1/binary,Rest/binary>> = DTMF,
%%    case C of
%%        _ when C== <<",">>; C== <<"'">> ->
%%            ToneRef = make_ref(),
%%            ?SCR_COMP:event_after(1000, {'next_tone',ToneRef}),
%%            CS1 = CS#dtmfstate{dtmf = Rest, tone_ref = ToneRef},
%%            {ok, set_data(CS1,State)};
%%        _ ->
%%            Cmd = cmd_send_dtmf,
%%            OptsMap = #{dtmf => C, proto => Proto, pid => self(), ref => CmdRef},
%%            OwnerPid ! {scriptmachine, {ivr_cmd, CmdRef, {ext, ?MODULE, Cmd, self(), OptsMap}}},
%%            TimerRef = ?SCR_COMP:event_after(?TIMEOUT_TICKET, {'ticket_started_timeout',CmdRef}),
%%            CS1 = CS#dtmfstate{ticket_timer_ref = TimerRef, dtmf = Rest},
%%            {ok, set_data(CS1,State)}
%%    end.
%%
%%%% @private
%%%% executed in IVR-FSM pid
%%cmd_send_dtmf({_Ref, _FromPid, _Arg}=P, #state_ivr{a=#side{state=S}}=State) when S==dialog; S==early ->
%%    do_cmd_send_dtmf(P, State);
%%cmd_send_dtmf({Ref, FromPid, _Arg}, #state_ivr{}=State) ->
%%    ?SCR_COMP:event(FromPid, {'ticket',Ref,{error,{invalid_operation,<<"Media could not be modified, state mismatch">>}}}),
%%    {next_state, ?CURRSTATE, State}.
%%
%%%% @private
%%%% executed in IVR-FSM pid
%%do_cmd_send_dtmf({Ref, FromPid, Opts}, #state_ivr{}=State) when is_map(Opts) ->
%%    Res = case maps:get(proto,Opts) of
%%              1 -> % rfc2833
%%                  send_rfc2833(Opts, State);
%%              2 -> % sip-info
%%                  send_sipinfo(Opts, State)
%%          end,
%%    case Res of
%%        {ok,State1} ->
%%            ?SCR_COMP:event(FromPid, {'ticket',Ref,ok}),
%%            {next_state, ?CURRSTATE, State1};
%%        {error,_}=Err ->
%%            ?SCR_COMP:event(FromPid, {'ticket',Ref,Err}),
%%            {next_state, ?CURRSTATE, State}
%%    end.
%%
%%%% @private
%%%% executed in IVR-FSM pid
%%send_rfc2833(Opts, #state_ivr{}=State) ->
%%    case maps:get(dtmf,Opts) of
%%        false -> {ok,State};
%%        DTMF ->
%%            %?OUT("send dtmf rfc2833: ~120tp", [maps:get(dtmf,Opts)]),
%%            Events = {?SendRfc2833EvtID, [{"rfc2833/send", [{"code",?EU:to_list(get_code(DTMF))}, {"duration",?Duration}] }] },
%%            case ?IVR_MEDIA:modify_term_events(Events, State) of
%%                {ok,State1} -> {ok,State1};
%%                {error,_}=Err -> Err
%%            end end.
%%
%%%% @private
%%%% executed in IVR-FSM pid
%%send_sipinfo(Opts, #state_ivr{}=State) ->
%%    FromPid = maps:get(pid,Opts),
%%    Ref = maps:get(ref,Opts),
%%    DTMF = maps:get(dtmf,Opts),
%%    %?OUT("send dtmf sipinfo: ~120tp", [DTMF]),
%%    FunResult = fun({error,_}=Err) ->
%%                        %?OUT("Err: ~120p",[Err]),
%%                        ?SCR_COMP:event(FromPid, {'ticket',Ref,Err});
%%                   ({ok,_Res}) ->
%%                        %?OUT("INFO. result: ~500tp", [_Res]),
%%                        ?SCR_COMP:event(FromPid, {'ticket',Ref,ok})
%%                end,
%%    ContentType = <<"application/dtmf-relay">>,
%%    Body = <<"Signal=",(get_code(DTMF))/bitstring,"\r\nDuration=",(?EU:to_binary(?Duration))/binary>>,
%%    ?ACTIVE:send_sipinfo(ContentType, Body, FunResult, State),
%%    {ok,State}.
%%
%%%% ====================================================================
%%%% Internal functions
%%%% ====================================================================
%%
%%%% -------------------------------------
%%c(#state_scr{}=State) -> State;
%%c({ok,#state_scr{}=State}) -> c(State).
%%
%%%% -------------------------------------
%%get_data(#state_scr{active_component=#component{state=Data}}) -> Data.
%%set_data(#dtmfstate{}=Data,#state_scr{active_component=Comp}=State) -> State#state_scr{active_component=Comp#component{state=Data}}.
%%
%%%% --------------------------
%%%% dtmf
%%%% --------------------------
%%
%%% check dtmf symbols
%%check_symbols(<<>>) -> false;
%%check_symbols(DTMF) ->
%%    L = "012345678*#,'",
%%    lists:all(fun(I) -> lists:member(I,L) end, ?EU:to_list(DTMF)).
%%
%%% normalize dtmf symbols
%%get_code(<<"*">>) -> <<"10">>;
%%get_code(<<"#">>) -> <<"11">>;
%%get_code(X) -> X.
%%
%%%% --------------------------
%%%% finalize
%%%% --------------------------
%%
%%%% finalize work of component
%%fin(Mode, #state_scr{}=State) ->
%%    fin_1(Mode,stop_timers(State)).
%%fin_1(general_timeout, State) -> ?SCR_COMP:next([<<"transferTimeout">>,<<"transfer">>], State);
%%fin_1(symbol_timeout, State) -> ?SCR_COMP:next([<<"transferTimeout">>,<<"transfer">>], State);
%%fin_1(_, State) -> ?SCR_COMP:next(<<"transfer">>,State).
%%
%%%% @private
%%stop_timers(#state_scr{active_component=#component{state=#dtmfstate{general_timerref=TimerRef1,ticket_timer_ref=TimerRef2}=CS}=Comp}=State) ->
%%    ?EU:cancel_timer(TimerRef1),
%%    ?EU:cancel_timer(TimerRef2),
%%    State#state_scr{active_component=Comp#component{state=CS#dtmfstate{general_timerref=undefined, ticket_timer_ref=undefined}}}.
