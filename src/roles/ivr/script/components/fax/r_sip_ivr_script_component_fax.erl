%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 01.06.2016
%%% @doc

-module(r_sip_ivr_script_component_fax).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Defines
%% ====================================================================

%%%-include("../include/r_script.hrl").
%%-include("../../era_script/include/r_script.hrl").
%%
%%-include("../include/r_sip_ivr.hrl").
%%-include("../include/r_sip_nk.hrl").
%%-include("../include/r_sip_mgc.hrl").
%%
%%-define(U, r_sip_utils).
%%-define(CFG, r_sip_config).
%%
%%-define(CURRSTATE, 'active').
%%
%%-define(TIMEOUT, 10000).
%%-define(TIMEOUT_REINVITE, 16500).
%%
%%-define(S_REINVITING1, reinviting1).
%%-define(S_REINVITING2, reinviting2).
%%-define(S_WAIT_TICKET, wait_ticket).
%%-define(S_TRANSMITTING, transmitting).
%%-define(S_RECEIVING, receiving).
%%-define(S_MOVINGFILE, movingfile).
%%-define(S_STOPPING, stopping).
%%
%%-record(fax, {
%%    state :: ?S_REINVITING1 | ?S_WAIT_TICKET | ?S_TRANSMITTING | ?S_RECEIVING | ?S_STOPPING | ?S_REINVITING2,
%%    ref,
%%    timer_ref,
%%    timestart,
%%    allow_t38 = true,
%%    file = <<>>,
%%    timeout = 0,
%%    initial_proto :: auto | t38 | t30,
%%    mode :: r | t,
%%    proto :: t38 | t30,
%%    rsdp = undefined,
%%    lsdp = undefined,
%%    result = undefined,
%%    pid,
%%    mgid
%%  }).
%%
%%%% ====================================================================
%%%% Callback functions
%%%% ====================================================================
%%
%%% returns descriptions of used properties
%%metadata(_ScriptType) ->
%%    [{<<"mode">>, #{type => <<"list">>,
%%                    items => [{1,<<"receive">>},
%%                              {2,<<"transmit">>}] }}, % removed "default => 1" not effective here
%%     {<<"proto">>, #{type => <<"list">>,
%%                     items => [{0,<<"auto">>},
%%                               {1,<<"t30">>},
%%                               {2,<<"t38">>}] }}, % removed "default => 1" not effective here
%%     {<<"timeoutSec">>, #{type => <<"argument">> }},
%%     {<<"file">>, #{type => <<"file">>,
%%                    filter => <<"mode==2">> }},
%%     {<<"pathToVarMode">>, #{type => <<"list">>,
%%                             filter => <<"mode==1">> }},
%%     {<<"nameVar">>, #{type => <<"variable">>,
%%                       filter => <<"mode==1">> }},
%%     {<<"transfer">>, #{type => <<"transfer">> }},
%%     {<<"transferTimeout">>, #{type => <<"transfer">>,
%%                               title => <<"timeout">> }},
%%     {<<"transferError">>, #{type => <<"transfer">>,
%%                             title => <<"error">> }} ].
%%
%%% ---------------------------------------------------------------------
%%init(#state_scr{ownerpid=undefined}=State) ->
%%    ?SCR_COMP:next([<<"transferError">>, <<"transfer">>], State);
%%init(#state_scr{scriptid=IvrId, active_component=#component{data=CompData}=_Comp}=State) ->
%%    % properties
%%    {TS,RecPath} = ?U:get_media_context_rec_opts(),
%%    CS = #fax{mode = mode(?SCR_COMP:get(<<"mode">>, CompData, 0)),
%%              timestart = os:timestamp(),
%%              timeout = ?SCR_ARG:get_int(<<"timeoutSec">>, CompData, 0, State) * 1000,
%%              initial_proto = proto(?SCR_COMP:get(<<"proto">>, CompData, 0)),
%%              pid = self()},
%%    case CS#fax.mode of
%%        't' ->
%%            FilePath = ?SCR_COMP:get_file(<<"file">>, CompData, <<>>, State),
%%            % ?OUT("init fax tx ~500tp", [FilePath]),
%%            case?SCR_FILE:expand_and_check_path(FilePath, read, State) of
%%                {error, _Reason} ->
%%                    ?LOG("~500tp : next transferError -> Reason ~500tp", [?MODULE, _Reason]),
%%                    ?SCR_COMP:next([<<"transferError">>, <<"transfer">>], State);
%%                FilePath1 ->
%%                    {ok,FilePath2,State1} = file_copy_to_mg_temp(FilePath1, State),
%%                    ?OUT("File copied: ~120p -> ~120p", [FilePath1, FilePath2]),
%%                    % ?OUT("init fax tx expanded ~500tp", [CheckedPath]),
%%                    CS1 = CS#fax{file=FilePath2},
%%                    init_fax(CS1, State1)
%%            end;
%%        'r' ->
%%            CS1 = CS#fax{file=filename:join([RecPath, ?EU:str("ivrr_~s_~s.tiff", [?EU:to_list(IvrId), ?EU:to_list(TS)])])},
%%            init_fax(CS1, State)
%%    end.
%%
%%% @private
%%init_fax(#fax{}=CS1, #state_scr{ownerpid=OwnerPid, active_component=#component{}=Comp}=State) ->
%%    % init
%%    CmdRef = make_ref(),
%%    Cmd = cmd_prepare_stream,
%%    OptsMap = #{proto => CS1#fax.initial_proto,
%%                props => CS1},
%%    OwnerPid ! {scriptmachine, {ivr_cmd, CmdRef, {ext, ?MODULE, Cmd, self(), OptsMap}}},
%%    TimerRef1 = ?SCR_COMP:event_after(?TIMEOUT_REINVITE, {'timeout_state',CmdRef,S=?S_REINVITING1}),
%%    {ok, State#state_scr{active_component=Comp#component{state=CS1#fax{state = S,
%%                                                                       ref = CmdRef,
%%                                                                       timer_ref = TimerRef1}}}}.
%%% ---------------------------------------
%%
%%%% --------------------------
%%%% State == REINVITING1
%%%% --------------------------
%%
%%% timeout of reinviting on start
%%handle_event({timeout_state,Ref,S}, #state_scr{active_component=#component{state=#fax{state=S=?S_REINVITING1,ref=Ref}}}=State) ->
%%    Err = {error,{timeout,<<"Ticket timeout">>}},
%%    fin(Err, State);
%%
%%% backup remotesdp at start
%%handle_event({backup_rsdp,CmdRef,RSdp}, #state_scr{active_component=#component{state=#fax{state=?S_REINVITING1,ref=CmdRef}=CS}=Comp}=State) ->
%%    % ?OUT("Backup RSDP"),
%%    State1 = case CS of
%%                 #fax{rsdp=undefined} -> State#state_scr{active_component=Comp#component{state=CS#fax{rsdp=RSdp}}};
%%                 _ -> State
%%             end,
%%    {ok, State1};
%%
%%% backup localsdp at start
%%handle_event({backup_lsdp,CmdRef,LSdp}, #state_scr{active_component=#component{state=#fax{state=?S_REINVITING1,ref=CmdRef}=CS}=Comp}=State) ->
%%    % ?OUT("Backup LSDP"),
%%    State1 = case CS of
%%                 #fax{lsdp=undefined} -> State#state_scr{active_component=Comp#component{state=CS#fax{lsdp=LSdp}}};
%%                 _ -> State
%%             end,
%%    {ok, State1};
%%
%%% ticket reinvite/mgc error (s = reinviting)
%%handle_event({ticket,CmdRef,{error,_}=Err}, #state_scr{active_component=#component{state=#fax{state=?S_REINVITING1,ref=CmdRef,timer_ref=TimerRef}}}=State) ->
%%    % ?OUT("Ticket prepare ~500tp", [Err]),
%%    erlang:cancel_timer(TimerRef),
%%    fin(Err, State);
%%
%%% ticket try next proto (s = reinviting)
%%handle_event({ticket,CmdRef,{next,NxtProto}}, #state_scr{active_component=#component{state=#fax{state=?S_REINVITING1,ref=CmdRef,timer_ref=TimerRef}=CS}=Comp}=State) ->
%%    % ?OUT("Ticket prepare next proto=~500tp", [NxtProto]),
%%    erlang:cancel_timer(TimerRef),
%%    Cmd = cmd_prepare_stream,
%%    OptsMap = #{proto => NxtProto,
%%                props => CS},
%%    #state_scr{ownerpid=OwnerPid}=State,
%%    OwnerPid ! {scriptmachine, {ivr_cmd, CmdRef, {ext, ?MODULE, Cmd, self(), OptsMap}}},
%%    TimerRef1 = ?SCR_COMP:event_after(?TIMEOUT_REINVITE, {'timeout_state',CmdRef,?S_REINVITING1}),
%%    {ok, State#state_scr{active_component=Comp#component{state=CS#fax{timer_ref = TimerRef1}}}};
%%
%%% ticket ok proto setup (s = reinviting)
%%handle_event({ticket,CmdRef,{ok,Proto}}, #state_scr{active_component=#component{state=#fax{state=?S_REINVITING1,ref=CmdRef,timer_ref=TimerRef}=CS}=Comp}=State) ->
%%    % ?OUT("Ticket prepare ok"),
%%    #state_scr{ownerpid=OwnerPid}=State,
%%    erlang:cancel_timer(TimerRef),
%%    CS1 = CS#fax{proto=Proto},
%%    {Cmd,OptsMap} = make_cmd_opts(start, CS1),
%%    OwnerPid ! {scriptmachine, {ivr_cmd, CmdRef, {ext, ?MODULE, Cmd, self(), OptsMap}}},
%%    TimerRef1 = ?SCR_COMP:event_after(?TIMEOUT, {'timeout_state',CmdRef,S=?S_WAIT_TICKET}),
%%    {ok, State#state_scr{active_component=Comp#component{state=CS1#fax{state = S,
%%                                                                       timer_ref = TimerRef1}}}};
%%
%%%% --------------------------
%%%% State == WAIT_TICKET
%%%% --------------------------
%%
%%% timeout of wait_ticket
%%handle_event({timeout_state,Ref,S}, #state_scr{active_component=#component{state=#fax{state=S=?S_WAIT_TICKET,ref=Ref}}}=State) ->
%%    Err = {error,{timeout,<<"Ticket timeout">>}},
%%    fin(Err, State);
%%
%%% ticket mgc error (s = wait_ticket)
%%handle_event({ticket,CmdRef,{error,_}=Err}, #state_scr{active_component=#component{state=#fax{state=?S_WAIT_TICKET,ref=CmdRef,timer_ref=TimerRef}}}=State) ->
%%    % ?OUT("Ticket start ~500tp", [Err]),
%%    erlang:cancel_timer(TimerRef),
%%    fin(Err, State);
%%
%%handle_event({ticket,CmdRef,{ok,started,MGID}}, #state_scr{active_component=#component{state=#fax{state=?S_WAIT_TICKET,ref=CmdRef,timer_ref=TimerRef}=CS}=Comp}=State) ->
%%    % ?OUT("Ticket start ok"),
%%    erlang:cancel_timer(TimerRef),
%%    case CS of
%%        #fax{timeout=T} when T>0 ->
%%            TimerRef1 = ?SCR_COMP:event_after(T, {'timeout_state',CmdRef,S1=?S_TRANSMITTING}),
%%            {ok, State#state_scr{active_component=Comp#component{state=CS#fax{state=S1,
%%                                                                                 timer_ref=TimerRef1,
%%                                                                              mgid=MGID}}}};
%%        _ ->
%%            {ok, State#state_scr{active_component=Comp#component{state=CS#fax{state=?S_TRANSMITTING,
%%                                                                              mgid=MGID}}}}
%%    end;
%%
%%%% --------------------------
%%%% Event == sip_reinvite_incoming
%%%% State == REINVITING1, WAIT_TICKET, TRANSMITTING, RECEIVING
%%%% --------------------------
%%
%%% incoming reinvite
%%handle_event({sip_reinvite_incoming,[_CallId,_Req]}, #state_scr{active_component=#component{state=#fax{state=PS}}}=State)
%%  when PS==?S_REINVITING1 ->
%%    % ?OUT("FAX. reinvite_incoming while preparing"),
%%    {ok, State};
%%
%%handle_event({sip_reinvite_incoming,[_CallId,Req]}, #state_scr{active_component=#component{state=#fax{state=PS,proto=Proto}=CS}=Comp}=State)
%%  when PS==?S_WAIT_TICKET; PS==?S_TRANSMITTING; PS==?S_RECEIVING ->
%%    % ?OUT("FAX. reinvite_incoming proto=~500tp", [Proto]),
%%    #sdp{}=RSdp=?U:extract_sdp(Req),
%%    case check_sendrecv(RSdp) of
%%        false ->
%%            Err = {error,{operation_failed,<<"Stream mode changed. Must be in sendrecv mode">>}},
%%            fin(Err, State);
%%        true ->
%%            case check_sdp(Proto,RSdp) of
%%                false ->
%%                    #state_scr{ownerpid=OwnerPid}=State,
%%                    #fax{ref=CmdRef,timer_ref=TimerRef}=CS,
%%                    erlang:cancel_timer(TimerRef),
%%                    Cmd = cmd_prepare_stream,
%%                    OptsMap = #{proto => CS#fax.initial_proto,
%%                                props => CS},
%%                    OwnerPid ! {scriptmachine, {ivr_cmd, CmdRef, {ext, ?MODULE, Cmd, self(), OptsMap}}},
%%                    TimerRef1 = ?SCR_COMP:event_after(?TIMEOUT_REINVITE, {'timeout_state',CmdRef,S=?S_REINVITING1}),
%%                    {ok, State#state_scr{active_component=Comp#component{state=CS#fax{state = S,
%%                                                                                        timer_ref = TimerRef1}}}};
%%                true ->
%%                    {ok, State}
%%            end end;
%%
%%%% --------------------------
%%%% State == TRANSMITTING, RECEIVING
%%%% --------------------------
%%
%%% timeout of transmit/receive
%%handle_event({timeout_state,CmdRef,S}, #state_scr{active_component=#component{state=#fax{state=S,ref=CmdRef}=CS}=Comp}=State) when S==?S_TRANSMITTING; S==?S_RECEIVING ->
%%    State1 = State#state_scr{active_component=Comp#component{state=CS#fax{result='timeout'}}},
%%    exec_stop_fax('timeout_state', State1);
%%
%%% fax stopped by mg
%%handle_event({fax_stopped,Event}, #state_scr{active_component=#component{state=#fax{state=S,timer_ref=TimerRef}=CS}=Comp}=State) when S==?S_TRANSMITTING; S==?S_RECEIVING ->
%%    % ?OUT("fax stopped ~120p", [Event]),
%%    erlang:cancel_timer(TimerRef),
%%    State1 = State#state_scr{active_component=Comp#component{state=CS#fax{result={'faxresult',Event}}}},
%%    exec_stop_fax('timeout_state', State1);
%%
%%%% --------------------------
%%%% State == STOPPING
%%%% --------------------------
%%
%%% timeout of stopping
%%handle_event({timeout_state,Ref,S}, #state_scr{active_component=#component{state=#fax{state=S=?S_STOPPING,ref=Ref}}}=State) ->
%%    Err = {error, {timeout, <<"Ticket timeout">>}},
%%    fin(Err, State);
%%
%%handle_event({async_timeout_state,Ref,S}, #state_scr{active_component=#component{state=#fax{state=S=?S_STOPPING,ref=Ref}}}=State) ->
%%    case check_move_file(State) of
%%        ok -> fin(stopped, State);
%%        {ok,State1} -> {ok,State1}
%%    end;
%%
%%% ticket ok (s = stopping)
%%handle_event({ticket,CmdRef,{ok,detached}}, #state_scr{ownerpid=OwnerPid,active_component=#component{state=#fax{state=?S_STOPPING, ref=CmdRef, timer_ref=TimerRef}=CS}=Comp}=State) ->
%%    % ?OUT("Ticket detach ok"),
%%    erlang:cancel_timer(TimerRef),
%%    Cmd = cmd_restore_stream,
%%    OwnerPid ! {scriptmachine, {ivr_cmd, CmdRef, {ext, ?MODULE, Cmd, self(), CS}}},
%%    TimerRef1 = ?SCR_COMP:event_after(?TIMEOUT_REINVITE, {'timeout_state',CmdRef,S=?S_REINVITING2}),
%%    {ok, State#state_scr{active_component=Comp#component{state=CS#fax{state = S,
%%                                                                      timer_ref = TimerRef1}}}};
%%
%%%% --------------------------
%%%% State == REINVITING2
%%%% --------------------------
%%
%%% timeout of reinviting on stop (s = reinviting2)
%%handle_event({timeout_state,Ref,S}, #state_scr{active_component=#component{state=#fax{state=S=?S_REINVITING2,ref=Ref}}}=State) ->
%%    Err = {error,{timeout,<<"Ticket timeout">>}},
%%    fin(Err, State);
%%
%%% ticket reinviting on stop error (s = reinviting2)
%%handle_event({ticket,CmdRef,{error,_}=Err}, #state_scr{active_component=#component{state=#fax{state=?S_REINVITING2,ref=CmdRef,timer_ref=TimerRef}}}=State) ->
%%    % ?OUT("Ticket restore ~500tp", [Err]),
%%    erlang:cancel_timer(TimerRef),
%%    fin(Err, State);
%%
%%% ticket reinviting on stop ok (s = reinviting2)
%%handle_event({ticket,CmdRef,ok}, #state_scr{active_component=#component{state=#fax{state=?S_REINVITING2,ref=CmdRef,timer_ref=TimerRef}}}=State) ->
%%    % ?OUT("Ticket restore ok"),
%%    erlang:cancel_timer(TimerRef),
%%    case check_move_file(State) of
%%        ok -> fin(stopped, State);
%%        {ok,State1} -> {ok,State1}
%%    end;
%%
%%%% --------------------------
%%%% State == MOVINGFILE
%%%% --------------------------
%%
%%% timeout of moving file (s = movingfile)
%%handle_event({timeout_state,Ref,S}, #state_scr{active_component=#component{state=#fax{state=S=?S_MOVINGFILE,ref=Ref}}}=State) ->
%%    Err = {error,{timeout,<<"Ticket timeout">>}},
%%    fin(Err, State);
%%
%%% ticket of error on moving file (s = movingfile)
%%handle_event({copy_result,CmdRef,{error,_}=Err}, #state_scr{active_component=#component{state=#fax{state=?S_MOVINGFILE,ref=CmdRef,timer_ref=TimerRef}}}=State) ->
%%    % ?OUT("Ticket movingfile ~500tp", [Err]),
%%    erlang:cancel_timer(TimerRef),
%%    fin(Err, State);
%%
%%% ticket ok on moving file (s = movingfile)
%%handle_event({copy_result,CmdRef,{ok, FilePath}}, #state_scr{active_component=#component{state=#fax{state=?S_MOVINGFILE,ref=CmdRef,timer_ref=TimerRef}}}=State) ->
%%    % ?OUT("Ticket movingfile ok"),
%%    erlang:cancel_timer(TimerRef),
%%    case put_file(FilePath, State) of
%%        {error,_}=Err -> fin(Err,State);
%%        {ok,State1} -> fin(stopped, State1)
%%    end;
%%
%%%% --------------------------
%%%% Event == ticket error
%%%% --------------------------
%%
%%% ticket mgc error (s = stopping, ...)
%%handle_event({ticket,CmdRef,{error,_}=Err}, #state_scr{active_component=#component{state=#fax{state=_PS,ref=CmdRef,timer_ref=TimerRef}}}=State) ->
%%    % ?OUT("Ticket state=~500tp ~500tp", [PS,Err]),
%%    erlang:cancel_timer(TimerRef),
%%    fin(Err, State);
%%
%%%% --------------------------
%%
%%handle_event(_Ev, State) ->
%%    {ok, State}.
%%
%%% ---------------------------------------
%%
%%% terminate component externally
%%terminate(#state_scr{ownerpid=undefined,active_component=#component{state=#fax{state=PS,timer_ref=TimerRef}}}=State) when PS==?S_TRANSMITTING; PS==?S_RECEIVING ->
%%    erlang:cancel_timer(TimerRef),
%%    {ok, State};
%%terminate(#state_scr{active_component=#component{state=#fax{state=PS,timer_ref=TimerRef}}}=State) when PS==?S_TRANSMITTING; PS==?S_RECEIVING ->
%%    erlang:cancel_timer(TimerRef),
%%    {ok,State1} = exec_stop_fax('async_timeout_state', State),
%%    {async, State1};
%%terminate(State) ->
%%    {ok, State}.
%%
%%%% ====================================================================
%%%% Functional iface of SIP IVR dlg (linked to its fsm)
%%%% ====================================================================
%%
%%%% -------------------------------
%%%% reinvite to fax
%%%% -------------------------------
%%
%%cmd_prepare_stream({_Ref, _FromPid, Opts}=P, #state_ivr{a=#side{state=S}}=State) when S==dialog ->
%%    % ?OUT("FAX. prepare stream"),
%%    Proto = maps:get(proto,Opts),
%%    do_cmd_prepare_stream(P, Proto, #state_ivr{a=#side{state=S}}=State);
%%cmd_prepare_stream({Ref, FromPid, _Opts}, #state_ivr{}=State) ->
%%    % ?OUT("FAX. prepare stream err"),
%%    ?SCR_COMP:event(FromPid, {'ticket',Ref,{error,{invalid_operation,<<"Fax could not be started, state mismatch">>}}}),
%%    {next_state, ?CURRSTATE, State}.
%%
%%% @private
%%do_cmd_prepare_stream(P, 'auto', State) ->
%%    do_cmd_prepare_stream(P, 't38', State);
%%do_cmd_prepare_stream({Ref, FromPid, _}=P, Proto, #state_ivr{a=#side{remotesdp=RSdp}}=State) when Proto=='t38'; Proto=='t30' ->
%%    % ?OUT("FAX. do_cmd_prepare stream sendrecv=~500tp, proto=~500tp", [?M_SDP:check_sendrecv(RSdp), Proto]),
%%    case ?IVR_MEDIA:media_check_no_playrec(State) of
%%        false ->
%%            ?SCR_COMP:event(FromPid, {'ticket',Ref,{error,{operation_failed,<<"Context must not contain play/rec terms">>}}}),
%%            {next_state, ?CURRSTATE, State};
%%        true ->
%%            case check_sendrecv(RSdp) of
%%                false ->
%%                    ?OUT("FAX. do_cmd_prepare stream. No sendrecv"),
%%                    ?SCR_COMP:event(FromPid, {'ticket',Ref,{error,{operation_failed,<<"Stream must be in sendrecv mode">>}}}),
%%                    {next_state, ?CURRSTATE, State};
%%                true ->
%%                    case check_sdp(Proto, RSdp) of
%%                        true ->
%%                            ?OUT("FAX. do_cmd_prepare stream. Sdp Ok"),
%%                            ?SCR_COMP:event(FromPid, {'ticket',Ref,{ok,Proto}}),
%%                            {next_state, ?CURRSTATE, State};
%%                        false ->
%%                            ?OUT("FAX. do_cmd_prepare stream. Reinvite ~500tp", [Proto]),
%%                            ?SCR_COMP:event(FromPid, {'backup_rsdp',Ref,RSdp}),
%%                            do_cmd_reinvite_fax(P, Proto, State)
%%                    end end end.
%%
%%% @private
%%check_sendrecv(Sdp) ->
%%    case {?M_SDP:get_stream_mode(<<"audio">>,Sdp), ?M_SDP:get_stream_mode(<<"image">>,Sdp)} of
%%        {<<"sendrecv">>,undefined} -> true;
%%        {undefined,<<"sendrecv">>} -> true;
%%        {<<"sendrecv">>,<<"sendrecv">>} -> true;
%%        _ -> false
%%    end.
%%
%%% @private
%%check_sdp('t38',Sdp) -> ?M_SDP:check_t38(Sdp);
%%check_sdp('t30',Sdp) -> ?M_SDP:check_t30(Sdp);
%%check_sdp('audio',Sdp) -> ?M_SDP:check_audio(Sdp).
%%
%%% @private
%%% make reinvite and prepare response fun
%%do_cmd_reinvite_fax({Ref, FromPid, _}=_P, Proto, State) ->
%%    FunBackupL = fun(LSdp) -> ?SCR_COMP:event(FromPid, {'backup_lsdp',Ref,LSdp}) end,
%%    FunResult = fun({error,{rejected,_Code}}) when Proto == 't38' -> ?SCR_COMP:event(FromPid, {'ticket',Ref,{next,'t30'}});
%%                   ({error,_}=Err) -> ?SCR_COMP:event(FromPid, {'ticket',Ref,Err});
%%                   ({ok,RSdp}) ->
%%                        ?OUT("FAX. reinvite result: ~500tp", [RSdp]),
%%                        case check_sdp(Proto, RSdp) of
%%                            true ->
%%                                ?SCR_COMP:event(FromPid, {'ticket',Ref,{ok,Proto}});
%%                            false when Proto == 't30' ->
%%                                ?SCR_COMP:event(FromPid, {'ticket',Ref,{error,{operation_failed, <<"Fax media could not be set (T.30 require g711, T.38 require image)">>}}});
%%                            false when Proto == 't38' ->
%%                                ?SCR_COMP:event(FromPid, {'ticket',Ref,{next,'t30'}})
%%                        end,
%%                        ok
%%                end,
%%    ?ACTIVE_REINVITE:send_reinvite(Proto, FunBackupL, FunResult, State).
%%
%%%% -------------------------------
%%%% reinvite to audio (video,...) restore
%%%% -------------------------------
%%
%%cmd_restore_stream({_Ref, _FromPid, _Args}=P, #state_ivr{a=#side{state=S}}=State) when S==dialog ->
%%    % ?OUT("FAX. restore_stream"),
%%    do_cmd_restore_stream(P, #state_ivr{a=#side{state=S}}=State);
%%cmd_restore_stream({Ref, FromPid, _Args}, #state_ivr{}=State) ->
%%    % ?OUT("FAX. restore_stream err"),
%%    ?SCR_COMP:event(FromPid, {'ticket',Ref,{error,{invalid_operation,<<"Fax could not be rolled back, state mismatch">>}}}),
%%    {next_state, ?CURRSTATE, State}.
%%
%%% @private
%%do_cmd_restore_stream({Ref, FromPid, #fax{lsdp=LSdp1}}=P, State) ->
%%    % ?OUT("do_cmd_restore_stream"),
%%    case LSdp1 of
%%        undefined ->
%%            ?OUT("FAX. do_cmd_restore_stream. Ok, notchanged"),
%%            ?SCR_COMP:event(FromPid, {'ticket',Ref,ok}),
%%            {next_state, ?CURRSTATE, State};
%%        #sdp{} ->
%%            do_cmd_reinvite_restore(P, LSdp1, State)
%%    end.
%%
%%% @private
%%% make reinvite and prepare response fun
%%do_cmd_reinvite_restore({Ref, FromPid, _}=_P, LSdp, State) ->
%%    FunResult = fun({error,_}=Err) -> ?SCR_COMP:event(FromPid, {'ticket',Ref,Err});
%%                   ({ok,RSdp}) ->
%%                        ?OUT("FAX. reinvite result: ~500tp", [RSdp]),
%%                        case check_sdp('audio', RSdp) of
%%                            true -> ?SCR_COMP:event(FromPid, {'ticket',Ref,ok});
%%                            false -> ?SCR_COMP:event(FromPid, {'ticket',Ref,{error,{operation_failed, <<"Audio stream was not restored">>}}})
%%                        end,
%%                        ok
%%                end,
%%    ?ACTIVE_REINVITE:send_reinvite(LSdp, undefined, FunResult, State).
%%
%%%% -------------------------------
%%%% attach fax
%%%% -------------------------------
%%
%%cmd_attach_fax({_Ref, _FromPid, _Arg}=P, #state_ivr{a=#side{state=S}}=State) when S==dialog; S==early ->
%%    do_cmd_attach_fax(P, #state_ivr{a=#side{state=S}}=State);
%%cmd_attach_fax({Ref, FromPid, _Arg}, #state_ivr{}=State) ->
%%    ?SCR_COMP:event(FromPid, {'ticket',Ref,{error,{invalid_operation,<<"Fax could not be started, state mismatch">>}}}),
%%    {next_state, ?CURRSTATE, State}.
%%
%%% @private
%%do_cmd_attach_fax({Ref, FromPid, Arg}, State) when is_map(Arg) ->
%%    Opts = Arg,
%%    case ?IVR_MEDIA:media_attach_fax(Opts, State) of
%%        {error,_}=Err ->
%%            ?SCR_COMP:event(FromPid, {'ticket',Ref,Err}),
%%            {next_state, ?CURRSTATE, State};
%%        {ok, State1} ->
%%            MGID = case ?IVR_MEDIA:get_current_media_link(State) of
%%                       {ok, #{}=M} -> maps:get(mgid,M,undefined);
%%                       _ -> undefined
%%                   end,
%%            ?SCR_COMP:event(FromPid, {'ticket',Ref,{ok,started,MGID}}),
%%            {next_state, ?CURRSTATE, State1}
%%    end.
%%
%%%% -------------------------------
%%%% detach fax
%%%% -------------------------------
%%
%%cmd_detach_fax({_Ref, _FromPid, _Arg}=P, #state_ivr{a=#side{state=S}}=State) when S==dialog; S==early; S==bye ->
%%    do_cmd_detach_fax(P, #state_ivr{a=#side{state=S}}=State);
%%cmd_detach_fax({Ref, FromPid, _Arg}, #state_ivr{}=State) ->
%%    ?SCR_COMP:event(FromPid, {'ticket',Ref,{error,{invalid_operation,<<"Fax could not be stopped, state mismatch">>}}}),
%%    {next_state, ?CURRSTATE, State}.
%%
%%% @private
%%do_cmd_detach_fax({Ref, FromPid, Arg}, State) when is_map(Arg) ->
%%    Opts = Arg,
%%    case ?IVR_MEDIA:media_detach_fax(Opts, State) of
%%        {error,_}=Err ->
%%            ?SCR_COMP:event(FromPid, {'ticket',Ref,Err}),
%%            {next_state, ?CURRSTATE, State};
%%        {ok, State1} ->
%%            ?SCR_COMP:event(FromPid, {'ticket',Ref,{ok,detached}}),
%%            {next_state, ?CURRSTATE, State1}
%%    end.
%%
%%%% ====================================================================
%%%% Internal functions
%%%% ====================================================================
%%
%%proto(0) -> auto;
%%proto(1) -> t30;
%%proto(2) -> t38;
%%proto(_) -> auto.
%%
%%%-
%%mode(1) -> 'r';
%%mode(2) -> 't';
%%mode(_) -> 'r'.
%%
%%%% ---------------
%%%% fax
%%%% ---------------
%%
%%% receive/transmit
%%make_cmd_opts(start, #fax{file=File,proto=Proto,mode=Mode}=_CS) ->
%%    O = #{mode => Mode,
%%          proto => Proto,
%%          path => File},
%%    {cmd_attach_fax, O}.
%%
%%
%%% exec stop fax command
%%exec_stop_fax(TimeoutState, #state_scr{active_component=#component{state=#fax{state=PS}}}=State) when PS==?S_TRANSMITTING; PS==?S_RECEIVING ->
%%    exec_fax_cmd(?S_STOPPING, cmd_detach_fax, #{}, TimeoutState, State).
%%
%%% @private
%%exec_fax_cmd(S, Method, Arg, TimeoutState, #state_scr{active_component=#component{state=#fax{}=CS}=Comp}=State) ->
%%    #fax{ref=CmdRef,timer_ref=TimerRef}=CS,
%%    erlang:cancel_timer(TimerRef),
%%    #state_scr{ownerpid=OwnerPid}=State,
%%    OwnerPid ! {scriptmachine, {ivr_cmd, CmdRef, {ext, ?MODULE, Method, self(), Arg}}},
%%    Time = case TimeoutState of
%%               async_timeout_state -> 500;
%%               _ -> ?TIMEOUT
%%            end,
%%    TimerRef1 = ?SCR_COMP:event_after(Time, {TimeoutState,CmdRef,S}),
%%    {ok, State#state_scr{active_component=Comp#component{state=CS#fax{state=S,
%%                                                                      timer_ref=TimerRef1}}}}.
%%
%%%% ----------------
%%%% copy received file
%%%% ----------------
%%
%%% @srv
%%check_move_file(#state_scr{selfpid=SMPid,active_component=#component{state=#fax{ref=CmdRef, mgid=MGID}=CS}=Comp}=State) ->
%%    case CS#fax.mode of
%%        't' -> ok;
%%        'r' when MGID==undefined -> ok;
%%        'r' ->
%%            LocalPath = ?EU:to_list(?SCR_FILE:get_script_temp_directory(SMPid)),
%%            % ?OUT("~500tp : do_copy LocalPath ~500tp",[?MODULE, LocalPath]),
%%            spawn_link(fun() -> do_copy_file(MGID, CS, LocalPath) end),
%%            PackTimeout = 60000,
%%            TimerRef1 = ?SCR_COMP:event_after(PackTimeout, {'timeout_state',CmdRef,S1=?S_MOVINGFILE}),
%%            State1 = State#state_scr{active_component=Comp#component{state=CS#fax{state=S1,
%%                                                                                  timer_ref=TimerRef1}}},
%%            {ok, State1}
%%    end.
%%
%%% @spawned
%%do_copy_file(MGID, #fax{ref=CmdRef, pid=Pid}=CS, LocalPath) when is_pid(Pid) ->
%%    R = case copy_rtp(MGID, CS, LocalPath) of
%%            false -> {error, <<"copy fax error">>};
%%            {ok,_RtpPath}=T -> T
%%        end,
%%    ?SCR_COMP:event(Pid, {copy_result,CmdRef,R}).
%%
%%% @spawned copies rtp file from mgc to local (in spawned proc)
%%copy_rtp(MGID, #fax{file=TempFile}=_CS, LocalPath) ->
%%    case ?U:parse_mg_devicename(?U:get_mg_devicename(MGID)) of
%%        undefined -> false;
%%        {Addr,Postfix} ->
%%            case ?CFG:get_mg_node_by_addr_postfix(Addr,Postfix) of
%%                undefined -> false;
%%                {Site,Node} ->
%%                    RecPath = ?CFG:get_record_path(Addr,Postfix),
%%                    SrcPath = filename:join([RecPath|lists:nthtail(1,filename:split(TempFile))]),
%%                    DestPath = filename:join(LocalPath, lists:last(string:tokens(TempFile,"/"))),
%%                    % ?OUT("~500tp : copy_rtp ~500tp  ~500tp", [?MODULE, SrcPath, DestPath]),
%%                    R = ?ENVCOPY:copy_file({Site,Node}, SrcPath, DestPath),
%%                    ?ENVCROSS:call_node({Site,Node}, {?Rfile, delete, [SrcPath]}, false),
%%                    case R of
%%                        {error,_}=Err ->
%%                            ?LOG("Error copying ~500tp", [Err]),
%%                            false;
%%                        ok ->
%%                            case ?Rfilelib:is_file(DestPath) of
%%                                false -> false;
%%                                true -> {ok,DestPath}
%%                            end end end end.
%%
%%% @srv
%%% move packed file to destination (in gen_srv proc)
%%put_file(SrcPath, #state_scr{active_component=#component{data=Data}, stopfuns=SF}=State) ->
%%    {DestPath, ScriptPath} = make_destfile_path(State),
%%    ?Rfilelib:ensure_dir(DestPath),
%%    case ?Rfile:rename(SrcPath, DestPath) of
%%        {error,_}=Err -> Err;
%%        ok ->
%%            PathMode = ?SCR_COMP:get(<<"pathToVarMode">>, Data, 0),
%%            Var = ?SCR_COMP:get(<<"nameVar">>, Data, <<>>),
%%            case PathMode of
%%                0 -> % don't save
%%                    State1 = State;
%%                1 -> % script key path
%%                    Value = ?EU:to_binary(ScriptPath),
%%                    State1 = assign(Var, Value, State);
%%                2 -> % only file name
%%                    Value = ?EU:to_binary(?EU:get_filename(DestPath)),
%%                    State1 = assign(Var, Value, State)
%%            end,
%%            State2 = State1#state_scr{stopfuns=[fun() -> ?Rfile:delete(DestPath) end|SF]},
%%            {ok,State2}
%%    end.
%%
%%% build destination file path
%%make_destfile_path(#state_scr{scriptid=Id,active_component=#component{state=CS}}=State) ->
%%    #fax{timestart=TS}=CS,
%%
%%    % we make path for next component in ":TEMP/ivr_faxes/filename.tiff"
%%    % and make local path for file:remane()
%%    ScriptTempPath = filename:join([?EU:to_list(?SCR_FILE:get_path_type(temp)), "ivr_faxes"]),
%%    LocalFolder = ?ENVFILE:get_local_path(?EU:to_list(?SCR_FILE:expand_and_check_path(?EU:to_binary(ScriptTempPath), write, State))),
%%
%%    LId = ?EU:to_list(Id),
%%    {_,LId1} = lists:split(length(LId) - 6, LId),
%%    FileName = ?EU:str("fax_~s_~500tp.tiff", [LId1,?EU:timestamp(TS)]),
%%    {filename:join(LocalFolder, FileName), filename:join(ScriptTempPath, FileName)}.
%%
%%
%%%% ----------------
%%%% finalize
%%%% ----------------
%%
%%% finalize work of component
%%% mode = error, stopped
%%fin({error, Reason}, State) ->
%%    ?LOG("~500tp : next transferError -> Reason ~500tp", [?MODULE, Reason]),
%%    ?SCR_COMP:next([<<"transferError">>, <<"transfer">>], State);
%%fin(Mode, #state_scr{active_component=#component{state=#fax{state=PS}=CS}=Comp}=State) when PS==?S_TRANSMITTING; PS==?S_RECEIVING ->
%%    exec_stop_fax('timeout_state', State),
%%    fin(Mode, State#state_scr{active_component=Comp#component{state=CS#fax{state=?S_STOPPING}}});
%%fin(Mode, #state_scr{active_component=#component{data=_Data,state=_CS}}=State) ->
%%    fin_1(Mode, State).
%%fin_1(_, #state_scr{active_component=#component{state=#fax{result=Res}}}=State) ->
%%    case Res of
%%        undefined -> ?SCR_COMP:next([<<"transferError">>, <<"transfer">>],State);
%%        'timeout' -> ?SCR_COMP:next([<<"transferTimeout">>, <<"transfer">>], State);
%%        {'faxresult',Event} ->
%%            % ?OUT("fax result ~120p", [Event]),
%%            case maps:get(parvalue,Event,undefined) of
%%                undefined -> ?SCR_COMP:next(<<"transfer">>,State);
%%                T -> case ?EU:to_list(T) of
%%                         "error" -> ?SCR_COMP:next([<<"transferError">>, <<"transfer">>], State);
%%                         "done" -> ?SCR_COMP:next(<<"transfer">>,State)
%%                     end
%%            end end.
%%
%%% assign value to variable
%%assign(Var, Value, State) ->
%%      case ?SCR_VAR:set_value(Var, Value, State) of
%%        {ok, State1} -> State1;
%%        _ -> State
%%    end.
%%
%%%% --------------------------------------
%%file_copy_to_mg_temp(X,State) ->
%%    F_get_mgid = fun(#state_scr{active_component=#component{state=#fax{mgid=MGID}}}) -> MGID;
%%                    (#state_scr{}=State1) -> ?IVR_SCRIPT_UTILS:get_mgid(State1)
%%                 end,
%%    F_store_mgid = fun(_,#state_scr{active_component=#component{state=#fax{}}}=State1) -> State1;
%%                      (MGID,#state_scr{active_component=Comp}=State1) -> State1#state_scr{active_component=Comp#component{state=#fax{mgid=MGID}}}
%%                   end,
%%    ?IVR_SCRIPT_UTILS:file_copy_to_mg_temp(X,{F_store_mgid,F_get_mgid},State).
