%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 19.01.2017
%%% @doc

-module(r_sip_ivr_script_component_fax_send).
-author('Peter Bukashin <tbotc@yandex.ru>').

%-compile([export_all,nowarn_export_all]).
%%-export([metadata/1,
%%         init/1,
%%         handle_event/2,
%%         terminate/1]).
%%
%%%% ====================================================================
%%%% Defines
%%%% ====================================================================
%%
%%%-include("../include/r_script.hrl").
%%-include("../../era_script/include/r_script.hrl").
%%
%%-define(FAX, r_sip_ivr_script_component_fax).
%%
%%%% ====================================================================
%%%% Callback functions
%%%% ====================================================================
%%
%%% returns descriptions of used properties
%%metadata(_ScriptType) ->
%%    [{<<"proto">>, #{type => <<"list">>,
%%                     items => [{0,<<"auto">>},
%%                                {1,<<"t30">>},
%%                               {2,<<"t38">>}] }}, % removed "default => 1" not effective here
%%     {<<"timeoutSec">>, #{type => <<"argument">> }},
%%     {<<"file">>, #{type => <<"file">> }},
%%     {<<"transfer">>, #{type => <<"transfer">> }},
%%     {<<"transferTimeout">>, #{type => <<"transfer">>,
%%                               title => <<"timeout">> }},
%%     {<<"transferError">>, #{type => <<"transfer">>,
%%                             title => <<"error">> }} ].
%%
%%% ---------------------------------------------------------------------
%%
%%init(#state_scr{active_component=#component{data=CompData}=Comp}=State) ->
%%    CompData1 = [{<<"mode">>,2}|CompData],
%%    State1 = State#state_scr{active_component=Comp#component{data=CompData1}},
%%    ?FAX:init(State1).
%%
%%handle_event(Msg, State) ->
%%    ?FAX:handle_event(Msg, State).
%%
%%terminate(State) ->
%%    ?FAX:terminate(State).
%%
%%%% ====================================================================
%%%% Internal functions
%%%% ====================================================================
