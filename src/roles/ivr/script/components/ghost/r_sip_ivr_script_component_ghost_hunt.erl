%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Pavel Abramov, Peter Bukashin <tbotc@yandex.ru>
%%% @date 12.09.2018
%%% @doc RP-893. Component adapter for hunt queue.

-module(r_sip_ivr_script_component_ghost_hunt).
-author(['Pavel Abramov','Peter Bukashin <tbotc@yandex.ru>']).

%%-export([init/1, handle_event/2]).
%%
%%%% ====================================================================
%%%% Define
%%%% ====================================================================
%%
%%-include("../../era_script/include/r_script.hrl").
%%-include("../include/r_sip_ivr.hrl").
%%
%%%% ====================================================================
%%%% Callback functions
%%%% ====================================================================
%%
%%init(#state_scr{domain=Domain, selfpid=PidSCM, ownerpid=PidFSM, active_component=Comp, meta=Meta}=State) ->
%%    case get_params(State) of
%%        {error, _}=Error -> final(Error, <<"Get params">>, State);
%%        {ok, HuntId, O} ->
%%            Funs = Meta#meta.functions,
%%            ElementId = ?EU:newid(),
%%            XInfo = get_xinfo(Funs, State),
%%            CallInfo = ?IVR_UTILS:get_b2b_call_info(maps:get(call_id,XInfo),Domain),
%%            [InviteId,EsgDlg] = ?EU:maps_get_default([{<<"inviteid">>,undefined},{<<"esgdlg">>,<<>>}],CallInfo),
%%            O1 = O#{xinfo => XInfo#{inviteid => InviteId,
%%                                    esgdlg => EsgDlg},
%%                    parent_functions => Funs, % RP-1468
%%                    element_id => ElementId}, % RP-1481
%%            case ?QUEUE:start(O1) of
%%                {ok, QPid} ->
%%                    ?QUEUE:router_registered(QPid, PidFSM),
%%                    ?QUEUE:scriptmachine_start(QPid, PidSCM),
%%                    wait(State#state_scr{active_component=Comp#component{state=#{qpid => QPid,
%%                                                                                 hunt_id => HuntId,
%%                                                                                 element_id => ElementId}}});
%%                Error -> final(Error, <<"Queue not started">>, State)
%%            end
%%    end.
%%
%%%% --------------------------------------
%%%%
%%%% --------------------------------------
%%handle_event({put, QId}, State) ->
%%    ?SCR_TRACE(State, "~500tp -- hunt has been put: ~500tp", [?MODULE, QId]),
%%    final(ok, <<"put">>, update_functions(QId, State));
%%handle_event(_Event, State) ->
%%    ?SCR_TRACE(State, "~500tp -- Unknown event catched: ~500tp", [?MODULE, _Event]),
%%    {ok, State}.
%%
%%
%%%% ====================================================================
%%%% Internal functions
%%%% ====================================================================
%%get_params(#state_scr{domain=D,active_component=#component{data=CompData}}=State) ->
%%    HuntId = ?SCR_ARG:get_string(<<"huntid">>, CompData, <<>>, State),
%%
%%    HB = {?EU:to_bool(?SCR_ARG:get_string(<<"huntblockenabled">>, CompData, <<"false">>, State)),
%%          ?SCR_ARG:get_string(<<"huntblockcode">>, CompData, 0, State)
%%         },
%%    O = #{
%%      domain => D,
%%      huntid => HuntId,
%%      from_user => ?SCR_ARG:get_string(<<"fromuser">>, CompData, <<>>, State),
%%      from_dn => ?SCR_ARG:get_string(<<"displayname">>, CompData, <<>>, State),
%%      remote_script => ?SCR_ARG:get_string(<<"preivrscript">>, CompData, <<>>, State),
%%      maxattemptstrannumber => ?SCR_ARG:get_string(<<"maxattemptstrannumber">>, CompData, <<>>, State),
%%      timeouttrannumber => ?SCR_ARG:get_string(<<"timeouttrannumber">>, CompData, <<>>, State),
%%      resdialtimeout => ?SCR_ARG:get_int(<<"resdialtimeout">>, CompData, 0, State),
%%      huntblock => HB
%%     },
%%    {ok, HuntId, O}.
%%
%%%% --------------------------------------
%%%%
%%%% --------------------------------------
%%final({error, Reason}, ComponentReason, State) when is_binary(ComponentReason) ->
%%    ?SCR_TRACE(State, "~500tp -- ~120p: ~500tp", [?MODULE, ComponentReason, Reason]),
%%    ?SCR_COMP:next(<<"transferError">>, State);
%%final(ok, Step, State) ->
%%    ?SCR_TRACE(State, "~500tp -- ~120p : ok", [?MODULE, Step]),
%%    ?SCR_COMP:next(<<"transfer">>, State).
%%
%%%% --------------------------------------
%%%%
%%%% --------------------------------------
%%wait(State) -> {ok, State}.
%%
%%%% --------------------------------------
%%%% Replace only existing functions in state.
%%%% --------------------------------------
%%update_functions(QId, #state_scr{domain=D, meta=#meta{functions=Funs, monitor=MonMap}=Meta, active_component=#component{state=CS}}=State) ->
%%    [QPid, HuntId] = ?EU:maps_get([qpid,hunt_id],CS),
%%    NewFuns = ?QUEUE:get_expression_funs(QPid, D, HuntId, QId),
%%    F = fun({K, Fun}, Acc) ->
%%                case lists:keysearch(K, 1, NewFuns) of
%%                    false -> lists:append(Acc, [{K, Fun}]);
%%                    {value,{_, NewFun}} -> lists:append(Acc, [{K, NewFun}])
%%                end
%%        end,
%%    UpFuns = lists:foldl(F, [], Funs),
%%    UpMonitor = maps:put(trace_events, ?QUEUE:get_trace_events(QPid), MonMap),
%%    State#state_scr{meta=Meta#meta{functions=UpFuns,
%%                                   monitor=UpMonitor}}.
%%
%%%% --------------------------------------
%%%% info for statistics
%%%% --------------------------------------
%%get_xinfo(Funs, State) ->
%%    get_xinfo(Funs, [<<"remotenum()">>, <<"callid()">>], State).
%%get_xinfo(Funs, Keys, State) ->
%%    F = fun(_, {error, _}=Error) -> Error;
%%           (K, M) ->
%%                {R,_} = ?ScriptExpressions:apply(K, Funs, State),
%%                maps:put(key_xinfo(K), R, M)
%%        end,
%%    lists:foldl(F, #{}, Keys).
%%%% @private
%%key_xinfo(<<"remotenum()">>) -> number;
%%key_xinfo(<<"callid()">>) -> call_id.
