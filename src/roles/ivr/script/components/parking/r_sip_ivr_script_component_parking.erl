%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Pavel Abramov, Peter Bukashin <tbotc@yandex.ru>
%%% @date 13.01.2017
%%% @doc
%%% @todo @ivr

-module(r_sip_ivr_script_component_parking).
-author(['Pavel Abramov','Peter Bukashin <tbotc@yandex.ru>']).

%%-export([init/1,
%%         handle_event/2,
%%         terminate/1,
%%         metadata/1]).
%%
%%%% ====================================================================
%%%% Defines
%%%% ====================================================================
%%
%%-include("r_sip_ivr_script_component_parking.hrl").
%%
%%%% ====================================================================
%%%% Callback functions (script)
%%%% ====================================================================
%%
%%init(#state_scr{ownerpid=undefined}=State) ->
%%    ?SCR_COMP:next([<<"transferError">>, <<"transfer">>], State);
%%init(#state_scr{active_component=#component{data=CompData}}=State) ->
%%    Action = case ?SCR_ARG:get_value(<<"action">>, CompData, 0, State) of 0 -> push; 1 -> pop end,
%%    start_component(Action, State).
%%% ---------------------------------------
%%
%%handle_event(Event, #state_scr{}=State) ->
%%    event(Event,State);
%%
%%handle_event(_Ev, State) ->
%%    {ok, State}.
%%
%%% ---------------------------------------
%%
%%terminate(#state_scr{active_component=#component{state=#parking_state{owner=undefined}}}=State) ->
%%     {ok, State};
%%terminate(#state_scr{active_component=#component{state=#parking_state{owner=Pid}}}=State) ->
%%    Pid ! {stop, self()},
%%     {ok, State}.
%%
%%% ---------------------------------------
%%% returns descriptions of used properties
%%metadata(_ScriptType) ->
%%    [{<<"action">>, #{type => <<"list">>,
%%                     items => [{0,<<"push">>},
%%                            {1, <<"pop">>}] }},
%%    {<<"parkingCode">>, #{type => <<"argument">> }},
%%    {<<"errorCodeVar">>, #{type => <<"variable">> }},
%%    {<<"transfer">>, #{type => <<"transfer">>}},
%%    {<<"transferBusy">>, #{type => <<"transfer">>,
%%                         filter => <<"action == 0">>}},
%%    {<<"transferNotFound">>, #{type => <<"transfer">>,
%%                          filter => <<"action == 1">>}},
%%    {<<"transferError">>, #{type => <<"transfer">>,
%%                            title => <<"error">> }}].
%%
%%%% ====================================================================
%%%% Functional iface of SIP IVR dlg (linked to its fsm)
%%%% ====================================================================
%%
%%%% ====================================================================
%%%% Internal functions
%%%% ====================================================================
%%
%%% @private
%%-spec start_component(Action, State) -> {ok, State} | {next, Id, State} when
%%    Action :: atom(),
%%    State :: #state_scr{},
%%    Id :: non_neg_integer().
%%start_component(push, #state_scr{}=State) ->
%%    ?PARKING_PUSH:start_component(State);
%%start_component(pop, #state_scr{}=State) ->
%%    ?PARKING_POP:start_component(State).
%%
%%% ---------------------------------------------------------------------
%%% @private
%%-spec event(Event, State) -> {ok, State} | {next, Id, State} when
%%    Event :: tuple(),
%%    State :: #state_scr{},
%%    Id :: non_neg_integer().
%%event(Event, #state_scr{active_component=#component{state=#parking_state{action=push}}}=State) ->
%%    ?PARKING_PUSH:event(Event, State);
%%event(Event, #state_scr{active_component=#component{state=#parking_state{action=pop}}}=State) ->
%%    ?PARKING_POP:event(Event, State);
%%event(_Event, State) ->
%%    {ok, State}.
%%
%%
