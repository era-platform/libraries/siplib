
%% ====================================================================
%% Defines
%% ====================================================================

%-include("../refer/r_sip_ivr_script_refer.hrl").
-include("r_sip_log.hrl").

-define(TIMEOUTs, 10).
-define(TIMEOUTms, 10 * 1000).

-define(CFG, r_sip_config).

-define(PARKING_PUSH, r_sip_ivr_script_component_parking_push).
-define(PARKING_POP, r_sip_ivr_script_component_parking_pop).
-define(PARKING_UTILS, r_sip_ivr_script_component_parking_utils).

-define(REFER_REPLACE, r_sip_ivr_script_component_refer_replace).

-define(PARAM_CODE, parking_code).
-define(PARAM_DOM, domain).
-define(PARAM_PID, from_pid).
-define(PARAM_REF, ref).
-define(PARAM_TIM, time_start).
-define(PARAM_SITE, site).
-define(PARAM_SERV, server_id).
-define(PARAM_IVR, ivr_id).

-define(PUSH_STATE_II, push_ivr_id_wait).
-define(PUSH_STATE_RR, push_reserver_result_wait).
-define(POP_STATE_FR, pop_find_parking_result_wait).
-define(POP_STATE_FI, pop_find_ivr_result_wait).
-define(POP_STATE_RE, pop_refer_result_wait).


-define(MONITOR, r_env_monitor_srv).
-define(ENVCALL, r_env_call).
-define(ENVCROSS, r_env_cross).
-define(U, r_sip_utils).

%% ====================================================================
%% Records
%% ====================================================================

-record(parking_state, {
    domain = <<"">> : binary(),
    state = undefined :: undefined | push_ivr_id_wait | push_reserver_result_wait | pop_find_parking_result_wait | pop_find_ivr_result_wait | pop_refer_result_wait,
    ref = undefined :: undefined | reference(),
    timer_ref = undefined :: undefined | reference(),
    action = push :: push | pop,
    parkingCode = undefined :: undefined | binary(),
    ivr_id = undefined :: undefined | binary(),
    server_id = undefined :: undefined | non_neg_integer(),
    site = <<"">> :: binary(),
    owner = undefined :: undefined | pid(),
    refer = undefined :: undefined | #refer{}
  }).