%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Pavel Abramov, Peter Bukashin <tbotc@yandex.ru>
%%% @date 13.01.2017
%%% @doc
%%% @todo @ivr

-module(r_sip_ivr_script_component_parking_pop).
-author(['Pavel Abramov','Peter Bukashin <tbotc@yandex.ru>']).

%%-export([start_component/1, find_parking/1, find_ivr/1, event/2]).
%%-export([pop_find_parking_result_wait/2, pop_find_ivr_result_wait/2, pop_refer_result_wait/2]).
%%
%%%% ====================================================================
%%%% Defines
%%%% ====================================================================
%%
%%-include("r_sip_ivr_script_component_parking.hrl").
%%
%%-define(TICKET_KEY_FP, <<"find_parking_result">>).
%%-define(TICKET_KEY_FI, <<"find_ivr_result">>).
%%
%%-define(ROLE_IVR, ivr).
%%-define(ENVCFG, r_env_config).
%%
%%%% ====================================================================
%%%% Public functions
%%%% ====================================================================
%%
%%-spec start_component(State) -> {ok, State} | {next, Id:: non_neg_integer(), State} when State :: #state_scr{}.
%%
%%start_component(#state_scr{active_component=Comp}=State) ->
%%    PState1 = parse_param(State),
%%    Number = PState1#parking_state.parkingCode,
%%    Domain = PState1#parking_state.domain,
%%    Param = #{?PARAM_CODE => Number, ?PARAM_DOM => Domain},
%%    PState2 = ?PARKING_UTILS:start_new_process(?MODULE, find_parking, Param, PState1#parking_state{state=?POP_STATE_FR}),
%%    {ok, State#state_scr{active_component=Comp#component{state=PState2}}}.
%%
%%% ---------------------------------------------------------------------
%%
%%event(Event, #state_scr{active_component=#component{state=#parking_state{state=S}}}=State) ->
%%    ?MODULE:S(Event, State).
%%
%%%% ====================================================================
%%%% Work process
%%%% ====================================================================
%%
%%-spec find_parking(Param:: map()) -> ok.
%%
%%find_parking(Param) when is_map(Param) ->
%%    Number = maps:get(?PARAM_CODE, Param, <<"">>),
%%    Domain = maps:get(?PARAM_DOM, Param, <<"">>),
%%    FromPid = maps:get(?PARAM_PID, Param, indefined),
%%    Ref = maps:get(?PARAM_REF, Param, indefined),
%%    StartTime = maps:get(?PARAM_TIM, Param, indefined),
%%    %
%%    Param2 = case maps:get(monitor, Param, indefined) of
%%                 indefined ->
%%                     MonRef = erlang:monitor(process, FromPid),
%%                     maps:put(monitor, MonRef, Param);
%%                 _ -> Param
%%             end,
%%    case {Number, Domain} of
%%        {<<"">>, _} -> ok;
%%        {_, <<"">>} -> ok;
%%        _ ->
%%            case {FromPid, Ref, StartTime} of
%%                {indefined, _, _} -> ok;
%%                {_, indefined, _} -> ok;
%%                {_, _, indefined} -> ok;
%%                _ ->
%%                    LocalTime = calendar:local_time(),
%%                    LocalTimeSecond = calendar:datetime_to_gregorian_seconds(LocalTime),
%%                    case LocalTimeSecond - StartTime of
%%                        Time when Time >= ?TIMEOUTs ->
%%                            send_ticket_find_parking_result(FromPid,Ref,{error, 201}),
%%                            ok;
%%                        _ ->
%%                            case ?PARKING_UTILS:parking_get_payload(Number, Domain) of
%%                                {error, exception} ->
%%                                    receive
%%                                        {'DOWN', _, process, FromPid, _} -> ok;
%%                                        {'EXIT', FromPid, _} -> ok;
%%                                        {stop, FromPid} -> ok
%%                                    after 1000 -> find_parking(Param2)
%%                                    end;
%%                                timeout ->
%%                                    receive
%%                                        {'DOWN', _, process, FromPid, _} -> ok;
%%                                        {'EXIT', FromPid, _} -> ok;
%%                                        {stop, FromPid} -> ok
%%                                    after 500 -> find_parking(Param2)
%%                                    end;
%%                                {ok, Payload} ->
%%                                    send_ticket_find_parking_result(FromPid,Ref,Payload),
%%                                    ok;
%%                                _ ->
%%                                    send_ticket_find_parking_result(FromPid,Ref,{error, 202}),
%%                                    ok
%%                            end
%%                    end
%%            end
%%    end.
%%
%%% ---------------------------------------------------------------------
%%
%%-spec find_ivr(Param:: map()) -> ok.
%%
%%find_ivr(Param) when is_map(Param) ->
%%    Site = maps:get(?PARAM_SITE, Param, <<"">>),
%%    ServerId = maps:get(?PARAM_SERV, Param, -1),
%%    IvrId = maps:get(?PARAM_IVR, Param, <<"">>),
%%    FromPid = maps:get(?PARAM_PID, Param, indefined),
%%    Ref = maps:get(?PARAM_REF, Param, indefined),
%%    StartTime = maps:get(?PARAM_TIM, Param, indefined),
%%
%%    Param2 = case maps:get(monitor, Param, indefined) of
%%            indefined -> MonRef = erlang:monitor(process, FromPid),
%%                         maps:put(monitor, MonRef, Param);
%%            _ -> Param
%%        end,
%%
%%    case {Site, ServerId, IvrId} of
%%        {<<"">>, _, _} -> ok;
%%        {_, -1, _} -> ok;
%%        {_, _, <<"">>} -> ok;
%%        _ ->
%%            case {FromPid, Ref, StartTime} of
%%                {indefined, _, _} -> ok;
%%                {_, indefined, _} -> ok;
%%                {_, _, indefined} -> ok;
%%                _ ->
%%                    LocalTime = calendar:local_time(),
%%                    LocalTimeSecond = calendar:datetime_to_gregorian_seconds(LocalTime),
%%                    case LocalTimeSecond - StartTime of
%%                        Time when Time >= ?TIMEOUTs ->
%%                            send_ticket_find_ivr_result(FromPid,Ref,{error, 201}),
%%                            ok;
%%                        _ ->
%%                            case get_call_info_ivr(Site, ServerId, IvrId) of
%%                                false ->
%%                                    receive
%%                                        {'DOWN', _, process, FromPid, _} -> ok;
%%                                        {'EXIT', FromPid, _} -> ok;
%%                                        {stop, FromPid} -> ok
%%                                    after 1000 -> find_ivr(Param2)
%%                                    end;
%%                                {ok, Info} ->
%%                                    send_ticket_find_ivr_result(FromPid,Ref,Info),
%%                                    ok
%%                            end
%%                    end
%%            end
%%    end.
%%
%%%% ====================================================================
%%%% Internal functions
%%%% ====================================================================
%%
%%% @private
%%send_ticket_find_parking_result(FromPid, Ref, Result) ->
%%    ?SCR_COMP:event(FromPid, {'ticket',Ref, {?TICKET_KEY_FP, Result}}).
%%
%%% @private
%%send_ticket_find_ivr_result(FromPid, Ref, Result) ->
%%    ?SCR_COMP:event(FromPid, {'ticket',Ref, {?TICKET_KEY_FI, Result}}).
%%
%%% ---------------------------------------------------------------------
%%% @private
%%-spec get_call_info_ivr(Site, ServerId, IvrId) -> {ok, Info} | false when
%%    Site :: binary(), ServerId :: non_neg_integer(), IvrId :: binary(),
%%    Info :: {ToUri, FromUri, CallId, ToTag, FromTag},
%%    ToUri :: binary(), FromUri :: binary(),    CallId :: binary(), ToTag :: binary(), FromTag :: binary().
%%
%%get_call_info_ivr(Site, ServerId, IvrId) ->
%%    Nodes = ?ENVCFG:get_nodes_containing_role_site(?ROLE_IVR, Site),
%%    %{Site::binary,Node::atom,Index::int,IpAddr::binary}
%%    case lists:filter(fun({S,_N,I,_A}) -> case {S,I} of {Site, ServerId} -> true; _ -> false end end, Nodes) of
%%        [] -> false;
%%        [{_,Node,_,_}|_] ->
%%            case ?ENVCROSS:call_node({Site, Node}, {?IVR_SRV, get_call_info_ivr, [IvrId]}, false) of
%%                false -> false;
%%                Info -> {ok, Info}
%%            end
%%    end.
%%
%%% ---------------------------------------------------------------------
%%% @private
%%-spec parse_param(SrcState::#state_scr{}) -> ParkingState:: #parking_state{}.
%%
%%parse_param(#state_scr{domain=Domain,active_component=#component{data=CompData}}=State) ->
%%    ParkingCode = ?SCR_ARG:get_string(<<"parkingCode">>, CompData, <<"">>, State),
%%    Site = ?CFG:get_current_site(),
%%    #parking_state{parkingCode=ParkingCode, site=Site, domain=Domain, action=pop}.
%%
%%%% ====================================================================
%%%% STATES
%%%% ====================================================================
%%
%%pop_find_parking_result_wait({timeout,Ref},
%%    #state_scr{active_component=#component{state=#parking_state{ref=Ref}}}=State) ->
%%    ?SCR_COMP:next([<<"transferError">>,<<"transfer">>],State);
%%
%%pop_find_parking_result_wait({ticket,Ref,{?TICKET_KEY_FP,{error, 202}}},
%%    #state_scr{active_component=#component{state=#parking_state{ref=Ref, timer_ref=TimerRef}}}=State) ->
%%    erlang:cancel_timer(TimerRef),
%%    ?SCR_COMP:next([<<"transferNotFound">>,<<"transfer">>],?PARKING_UTILS:write_error(202, State));
%%
%%pop_find_parking_result_wait({ticket,Ref,{?TICKET_KEY_FP,{error, Code}}},
%%    #state_scr{active_component=#component{state=#parking_state{ref=Ref, timer_ref=TimerRef}}}=State) ->
%%    erlang:cancel_timer(TimerRef),
%%    ?SCR_COMP:next([<<"transferError">>,<<"transfer">>],?PARKING_UTILS:write_error(Code, State));
%%
%%pop_find_parking_result_wait({ticket,Ref,{?TICKET_KEY_FP,{Site,ServerId,IvrId}}},
%%    #state_scr{active_component=#component{state=#parking_state{ref=Ref, timer_ref=TimerRef}=PState}=Comp}=State) ->
%%    erlang:cancel_timer(TimerRef),
%%    Param = #{?PARAM_SITE => Site, ?PARAM_SERV => ServerId,    ?PARAM_IVR => IvrId},
%%    PState2 = ?PARKING_UTILS:start_new_process(?MODULE, find_ivr, Param, PState#parking_state{state=?POP_STATE_FI}),
%%    {ok, State#state_scr{active_component=Comp#component{state=PState2}}}.
%%
%%% ---------------------------------------------------------------------
%%
%%pop_find_ivr_result_wait({timeout,Ref},
%%    #state_scr{active_component=#component{state=#parking_state{ref=Ref}}}=State) ->
%%    ?SCR_COMP:next([<<"transferError">>,<<"transfer">>],State);
%%
%%pop_find_ivr_result_wait({ticket,Ref,{?TICKET_KEY_FI,{error, 202}}},
%%    #state_scr{active_component=#component{state=#parking_state{ref=Ref, timer_ref=TimerRef}}}=State) ->
%%    erlang:cancel_timer(TimerRef),
%%    ?SCR_COMP:next([<<"transferNotFound">>,<<"transfer">>],?PARKING_UTILS:write_error(202, State));
%%
%%pop_find_ivr_result_wait({ticket,Ref,{?TICKET_KEY_FI,{error, Code}}},
%%    #state_scr{active_component=#component{state=#parking_state{ref=Ref, timer_ref=TimerRef}}}=State) ->
%%    erlang:cancel_timer(TimerRef),
%%    ?SCR_COMP:next([<<"transferError">>,<<"transfer">>],?PARKING_UTILS:write_error(Code, State));
%%
%%pop_find_ivr_result_wait({ticket,Ref,{?TICKET_KEY_FI,{ToUri, _FromUri, CallId, ToTag, FromTag}}},
%%    #state_scr{active_component=#component{state=#parking_state{ref=Ref, timer_ref=TimerRef}=PState,data=Data}=Comp}=State) ->
%%    erlang:cancel_timer(TimerRef),
%%    UnUri = ?U:unparse_uri(ToUri),
%%    UdData = lists:append([{<<"referToUri">>, [{<<"value">>,UnUri},{<<"varType">>,2},{<<"argType">>,1}]},
%%                           {<<"replacesCallId">>, [{<<"value">>,CallId},{<<"varType">>,2},{<<"argType">>,1}]},
%%                           {<<"replacesToTag">>, [{<<"value">>,ToTag},{<<"varType">>,2},{<<"argType">>,1}]},
%%                           {<<"replacesFromTag">>, [{<<"value">>,FromTag},{<<"varType">>,2},{<<"argType">>,1}]},
%%                           {<<"reinviteMode">>, 1}], Data),
%%    case ?REFER_REPLACE:init(State#state_scr{active_component=Comp#component{data=UdData}}) of
%%        {error, _} ->
%%            ?SCR_COMP:next([<<"transferError">>,<<"transfer">>],?PARKING_UTILS:write_error(203, State));
%%        {ok,#state_scr{active_component=#component{state=Refer}}} ->
%%            {ok, State#state_scr{active_component=Comp#component{state=PState#parking_state{refer=Refer,state=?POP_STATE_RE}}}}
%%    end.
%%
%%% ---------------------------------------------------------------------
%%
%%pop_refer_result_wait(Event, #state_scr{active_component=#component{state=#parking_state{refer=Refer}=PState}=Comp}=State) ->
%%    case ?REFER:handle_event(Event, State#state_scr{active_component=Comp#component{state=Refer}}) of
%%        {next, Id, UpdateState} ->
%%            {next, Id, UpdateState#state_scr{active_component=Comp#component{state=PState#parking_state{refer=undefined}}}};
%%        {error,_Error} ->
%%            UndefState = State#state_scr{active_component=Comp#component{state=PState#parking_state{refer=undefined}}},
%%            ?SCR_COMP:next([<<"transferError">>,<<"transfer">>],?PARKING_UTILS:write_error(202, UndefState));
%%        {ok, #state_scr{active_component=#component{state=UpRefer}}=UpdateState} ->
%%            {ok, UpdateState#state_scr{active_component=Comp#component{state=PState#parking_state{refer=UpRefer}}}}
%%    end.
%%
