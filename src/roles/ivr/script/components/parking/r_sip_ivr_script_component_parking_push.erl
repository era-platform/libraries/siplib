%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Pavel Abramov, Peter Bukashin <tbotc@yandex.ru>
%%% @date 13.01.2017
%%% @doc
%%% 1) Collect info for save in reserver
%%% 2) Send request to reserver
%%% @todo @ivr

-module(r_sip_ivr_script_component_parking_push).
-author(['Pavel Abramov','Peter Bukashin <tbotc@yandex.ru>']).

%%-export([start_component/1, event/2, reserve/1]).
%%
%%-export([fill_ivr_id/2, push_ivr_id_wait/2, push_reserver_result_wait/2]).
%%
%%%% ====================================================================
%%%% Defines
%%%% ====================================================================
%%
%%-include("r_sip_ivr_script_component_parking.hrl").
%%
%%-define(TICKET_KEY_FII, <<"fill_ivr_id">>).
%%-define(TICKET_KEY_RR, <<"reserver_result">>).
%%
%%%% ====================================================================
%%%% Public functions
%%%% ====================================================================
%%
%%-spec start_component(State) -> {ok, State} | {next, Id::non_neg_integer(), State} when State :: #state_scr{}.
%%
%%start_component(#state_scr{ownerpid=OwnerPid,active_component=Comp}=State) ->
%%    CmdRef = make_ref(),
%%    TimerRef = ?SCR_COMP:event_after(?TIMEOUTms, {'timeout',CmdRef}),
%%    PState1 = parse_param(State),
%%    PState2 = PState1#parking_state{ref=CmdRef, timer_ref=TimerRef, action=push, state=?PUSH_STATE_II},
%%    OwnerPid ! {scriptmachine, {ivr_cmd, CmdRef, {ext, ?MODULE, fill_ivr_id, self(), []}}},
%%    {ok, State#state_scr{active_component=Comp#component{state=PState2}}}.
%%
%%% ---------------------------------------------------------------------
%%
%%event(Event, #state_scr{active_component=#component{state=#parking_state{state=S}}}=State) ->
%%    ?MODULE:S(Event, State).
%%
%%%% ====================================================================
%%%% Functional iface of SIP IVR dlg (linked to its fsm)
%%%% ====================================================================
%%
%%fill_ivr_id({Ref, FromPid, []}, #state_ivr{ivrid=Id}=IvrState) ->
%%    send_ticket_fill_ivr_id_result(FromPid,Ref,Id),
%%    {next_state, active, IvrState}.
%%
%%
%%%% ====================================================================
%%%% Work process
%%%% ====================================================================
%%
%%-spec reserve(Param::map()) -> ok.
%%
%%reserve(Param) when is_map(Param) ->
%%    Id = maps:get(?PARAM_IVR, Param, <<"">>),
%%    Domain = maps:get(?PARAM_DOM, Param, <<"">>),
%%    ParkingCode = maps:get(?PARAM_CODE, Param, <<"">>),
%%    ServerId = maps:get(?PARAM_SERV, Param, -1),
%%     Site = maps:get(?PARAM_SITE, Param, <<"">>),
%%    FromPid = maps:get(?PARAM_PID, Param, indefined),
%%    Ref = maps:get(?PARAM_REF, Param, indefined),
%%    StartTime = maps:get(?PARAM_TIM, Param, indefined),
%%    %
%%    Param2 = case maps:get(monitor, Param, indefined) of
%%            indefined ->
%%                MonRef = erlang:monitor(process, FromPid),
%%                maps:put(monitor, MonRef, Param);
%%            _ -> Param
%%        end,
%%    %
%%    case {Id, Domain, ParkingCode, ServerId, Site} of
%%        {<<"">>, _, _, _, _} -> ok;
%%        {_, <<"">>, _, _, _} -> ok;
%%        {_, _, <<"">>, _, _} -> ok;
%%        {_, _, _,     -1, _} -> ok;
%%        {_, _, _, _, <<"">>} -> ok;
%%        _ ->
%%            case {FromPid, Ref, StartTime} of
%%                {indefined, _, _} -> ok;
%%                {_, indefined, _} -> ok;
%%                {_, _, indefined} -> ok;
%%                _ ->
%%                    LocalTime = calendar:local_time(),
%%                    LocalTimeSecond = calendar:datetime_to_gregorian_seconds(LocalTime),
%%                    case LocalTimeSecond - StartTime of
%%                        Time when Time >= ?TIMEOUTs ->
%%                            send_ticket_reserver_result(FromPid,Ref,{error, 102}),
%%                            ok;
%%                        _ ->
%%                            case ?PARKING_UTILS:parking_reserve(ParkingCode, Id, ServerId, Site, Domain) of
%%                                {error, exception} ->
%%                                    receive
%%                                        {'DOWN', _, process, FromPid, _} -> ok;
%%                                        {'EXIT', FromPid, _} -> ok;
%%                                        {stop, FromPid} -> ok
%%                                    after 1000 -> reserve(Param2)
%%                                    end;
%%                                timeout ->
%%                                    receive
%%                                        {'DOWN', _, process, FromPid, _} -> ok;
%%                                        {'EXIT', FromPid, _} -> ok;
%%                                        {stop, FromPid} -> ok
%%                                    after 500 -> reserve(Param2)
%%                                    end;
%%                                Result ->
%%                                    send_ticket_reserver_result(FromPid,Ref,Result),
%%                                    ok
%%                            end end end end.
%%
%%%% ====================================================================
%%%% Internal functions
%%%% ====================================================================
%%
%%% @private
%%-spec parse_param(SrcState:: #state_scr{}) -> ParkingState:: #parking_state{}.
%%
%%parse_param(#state_scr{domain=Domain,active_component=#component{data=CompData}}=State) ->
%%    ParkingCode = ?SCR_ARG:get_string(<<"parkingCode">>, CompData, <<"">>, State),
%%    Site = ?CFG:get_current_site(),
%%    ServerId = ?CFG:get_node_index(node()),
%%    #parking_state{parkingCode=ParkingCode, site=Site, server_id=ServerId, domain=Domain}.
%%
%%%% --------------------------------------------------------------------
%%% @private
%%send_ticket_fill_ivr_id_result(FromPid, Ref, Result) ->
%%    ?SCR_COMP:event(FromPid, {'ticket',Ref, {?TICKET_KEY_FII, Result}}).
%%% @private
%%send_ticket_reserver_result(FromPid, Ref, Result) ->
%%    ?SCR_COMP:event(FromPid, {'ticket',Ref, {?TICKET_KEY_RR, Result}}).
%%
%%%% ====================================================================
%%%% STATES
%%%% ====================================================================
%%
%%push_ivr_id_wait({timeout,Ref}, #state_scr{active_component=#component{state=#parking_state{ref=Ref}}}=State) ->
%%    ?SCR_COMP:next([<<"transferError">>, <<"transfer">>],State);
%%
%%push_ivr_id_wait({ticket,Ref,{?TICKET_KEY_FII, IvrId}},
%%    #state_scr{active_component=#component{state=#parking_state{ref=Ref,timer_ref=TimerRef}=PState}=Comp}=State) ->
%%    erlang:cancel_timer(TimerRef),
%%    Domain = PState#parking_state.domain,
%%    Site = PState#parking_state.site,
%%    PCode = PState#parking_state.parkingCode,
%%    Sid = PState#parking_state.server_id,
%%    Param = #{?PARAM_IVR => IvrId, ?PARAM_DOM => Domain, ?PARAM_SITE => Site, ?PARAM_CODE => PCode, ?PARAM_SERV => Sid},
%%    PState2 = ?PARKING_UTILS:start_new_process(?MODULE, reserve, Param, PState#parking_state{ivr_id=IvrId,state=?PUSH_STATE_RR}),
%%    {ok, State#state_scr{active_component=Comp#component{state=PState2}}}.
%%
%%%% --------------------------------------------------------------------
%%
%%push_reserver_result_wait({timeout,Ref}, #state_scr{active_component=#component{state=#parking_state{ref=Ref}}}=State) ->
%%    ?SCR_COMP:next([<<"transferError">>, <<"transfer">>],State);
%%
%%push_reserver_result_wait({ticket,Ref,{?TICKET_KEY_RR, true}},
%%    #state_scr{timersec_funs_acc=Funs,active_component=#component{state=#parking_state{ref=Ref,timer_ref=TimerRef}=PState}}=State) ->
%%    erlang:cancel_timer(TimerRef),
%%    Pid = self(),
%%    Number = PState#parking_state.parkingCode,
%%    IvrId = PState#parking_state.ivr_id,
%%    ServerId = PState#parking_state.server_id,
%%    Site = PState#parking_state.site,
%%    Domain = PState#parking_state.domain,
%%    ?MONITOR:append_fun(Pid, parking_clear, fun() -> ?PARKING_UTILS:parking_clear(Number, IvrId, Domain) end),
%%    Funs2 = lists:append([{parking_prolong,{fun(Acc) -> ?PARKING_UTILS:parking_prolong(Number, IvrId, ServerId, Site, Domain, Acc) end, []}}], Funs),
%%    UpdateState2 = State#state_scr{timersec_funs_acc=Funs2},
%%    ?SCR_COMP:next(<<"transfer">>,UpdateState2);
%%
%%push_reserver_result_wait({ticket,Ref,{?TICKET_KEY_RR, false}},
%%    #state_scr{active_component=#component{state=#parking_state{ref=Ref,timer_ref=TimerRef}}}=State) ->
%%    erlang:cancel_timer(TimerRef),
%%    ?SCR_COMP:next([<<"transferBusy">>, <<"transfer">>],?PARKING_UTILS:write_error(101, State));
%%
%%push_reserver_result_wait({ticket,Ref,{?TICKET_KEY_RR, _}},
%%    #state_scr{active_component=#component{state=#parking_state{ref=Ref,timer_ref=TimerRef}}}=State) ->
%%    erlang:cancel_timer(TimerRef),
%%    ?SCR_COMP:next([<<"transferError">>, <<"transfer">>],?PARKING_UTILS:write_error(102, State)).
