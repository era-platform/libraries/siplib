%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Pavel Abramov, Peter Bukashin <tbotc@yandex.ru>
%%% @date 13.01.2017
%%% @doc
%%% 1) Clear info from reserver
%%% 2) Prolong request to reserver
%%% 3) Send request to reserver
%%% @todo @ivr

-module(r_sip_ivr_script_component_parking_utils).
-author(['Pavel Abramov','Peter Bukashin <tbotc@yandex.ru>']).

%%-export([parking_clear/3,
%%         parking_prolong/6,
%%         parking_reserve/5,
%%         parking_get_payload/2,
%%         start_new_process/4,
%%         write_error/2]).
%%
%%%% ====================================================================
%%%% Defines
%%%% ====================================================================
%%
%%-include("r_sip_ivr_script_component_parking.hrl").
%%
%%%% ====================================================================
%%%% Public functions
%%%% ====================================================================
%%
%%-spec parking_clear(Number::binary(), IvrId::binary(), Domain::binary()) -> ok.
%%
%%parking_clear(Number, IvrId, Domain) ->
%%    Subject = {parking, Number},
%%    RsvOwner = IvrId,
%%    TTL = 0,
%%    Payload = [],
%%    Default = timeout,
%%    FunRequest = {reserve,[Subject, RsvOwner, TTL, Payload]},
%%    try
%%        ?ENVCALL:call_reserver_local(Domain, FunRequest, Default)
%%    of
%%        _Result -> ok
%%    catch
%%        Error:Err -> ?OUT("~500tp -- parking_clear: catch ~500tp", [?MODULE, {Error,Err}]),ok
%%    end.
%%
%%
%%% ---------------------------------------------------------------------
%%-spec parking_prolong(Number::binary(), IvrId::binary(), ServerId::node(), Site::binary(), Domain::binary(), Acc::non_neg_integer()) ->
%%          UpdateAcc:: non_neg_integer().
%%
%%parking_prolong(_Number, _IvrId, _ServerId, _Site, _Domain, []) ->
%%    LocalTime = calendar:local_time(),
%%    calendar:datetime_to_gregorian_seconds(LocalTime);
%%parking_prolong(Number, IvrId, ServerId, Site, Domain, LastProlong) ->
%%    LocalTime = calendar:local_time(),
%%    LocalTimeSecond = calendar:datetime_to_gregorian_seconds(LocalTime),
%%    case LocalTimeSecond - LastProlong of
%%        Time when Time >= 120 ->
%%            parking_reserve(Number, IvrId, ServerId, Site, Domain),
%%            LocalTimeSecond;
%%        _ -> LastProlong
%%    end.
%%
%%% ---------------------------------------------------------------------
%%-spec parking_reserve(Number::binary(), IvrId::binary(), ServerId::node(), Site::binary(), Domain::binary()) ->
%%          true | false | {error, exception} | timeout.
%%
%%parking_reserve(Number, IvrId, ServerId, Site, Domain) ->
%%    Subject = {parking, Number},
%%    RsvOwner = IvrId,
%%    TTL = 180,
%%    Payload = {Site, ServerId, IvrId},
%%    Default = timeout,
%%    FunRequest = {reserve,[Subject, RsvOwner, TTL, Payload]},
%%    try
%%        ?ENVCALL:call_reserver_local(Domain, FunRequest, Default)
%%    of
%%        Result -> Result
%%    catch
%%        Error:Err ->
%%            ?OUT("~500tp -- parking_reserve: catch ~500tp", [?MODULE, {Error,Err}]),
%%            {error, exception}
%%    end.
%%
%%% ---------------------------------------------------------------------
%%-spec parking_get_payload(Number::binary(), Domain::binary()) ->
%%          false | {error, exception} | timeout | {ok, Payload::{Site::binary(), ServerId::node(), IvrId::binary()}}.
%%
%%parking_get_payload(Number, Domain) ->
%%    Subject = {parking, Number},
%%    Default = timeout,
%%    FunRequest = {get_payload,[Subject]},
%%    try
%%        ?ENVCALL:call_reserver_local(Domain, FunRequest, Default)
%%    of
%%        [Payload|_] -> {ok, Payload};
%%        Result -> Result
%%    catch
%%        Error:Err ->
%%            ?OUT("~500tp -- parking_reserve: catch ~500tp", [?MODULE, {Error,Err}]),
%%            {error, exception}
%%    end.
%%
%%
%%% ---------------------------------------------------------------------
%%-spec start_new_process(Module::atom(), Method::atom(), Param::map(), ParkingState::#parking_state{}) -> ParkingState::#parking_state{}.
%%
%%start_new_process(Module, Method, Param, #parking_state{}=ParkingState) ->
%%    CmdRef = make_ref(),
%%    TimerRef = ?SCR_COMP:event_after(?TIMEOUTms, {'timeout',CmdRef}),
%%    ParkingState1 = ParkingState#parking_state{ref=CmdRef, timer_ref=TimerRef},
%%    %
%%    LocalTime = calendar:local_time(),
%%    LocalTimeSecond = calendar:datetime_to_gregorian_seconds(LocalTime),
%%    %
%%    Param1 = maps:put(?PARAM_PID, self(), Param),
%%    Param2 = maps:put(?PARAM_REF, CmdRef, Param1),
%%    Param3 = maps:put(?PARAM_TIM, LocalTimeSecond, Param2),
%%    %
%%    Owner = erlang:spawn(Module, Method, [Param3]),
%%    ParkingState1#parking_state{owner=Owner}.
%%
%%% ---------------------------------------------------------------------
%%-spec write_error(ErrorCode::non_neg_integer(), State::#state_scr{}) -> State:: #state_scr{}.
%%
%% write_error(ErrorCode, #state_scr{}=State) ->
%%    case ?SCR_VAR:set_value(<<"errorCodeVar">>, ErrorCode, State) of
%%        error -> State;
%%        {ok, UpState} -> UpState
%%    end.
%%
%%%% ====================================================================
%%%% Internal functions
%%%% ====================================================================
