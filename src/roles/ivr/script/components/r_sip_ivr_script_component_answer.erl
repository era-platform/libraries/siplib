%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 14.04.2016
%%% @doc
%%% @todo @ivr

-module(r_sip_ivr_script_component_answer).
-author('Peter Bukashin <tbotc@yandex.ru>').

%%-compile([export_all,nowarn_export_all]).
%%-export([init/1,
%%         handle_event/2,
%%         metadata/1]).
%%
%%%% ====================================================================
%%%% Defines
%%%% ====================================================================
%%
%%%-include("../include/r_script.hrl").
%%-include("../../era_script/include/r_script.hrl").
%%
%%-include("../include/r_sip_ivr.hrl").
%%-include("../include/r_sip_nk.hrl").
%%-include("../include/r_sip_headers.hrl").
%%
%%-define(U, r_sip_utils).
%%
%%-define(CURRSTATE, 'active').
%%
%%-define(TIMEOUT, 10000).
%%
%%-record(answer, {
%%    ref,
%%    timer_ref,
%%    pause_after_ms
%%  }).
%%
%%%% ====================================================================
%%%% Callback functions (script)
%%%% ====================================================================
%%
%%% returns descriptions of used properties
%%metadata(_ScriptType) ->
%%    [{<<"sipCode">>, #{type => <<"argument">> }},
%%     {<<"phrase">>, #{type => <<"argument">> }},
%%     {<<"reason">>, #{type => <<"argument">> }},
%%     {<<"remotePartyDisplay">>, #{type => <<"argument">> }},
%%     {<<"redirectTo">>, #{type => <<"argument">> }},
%%     {<<"pauseAfterMs">>, #{type => <<"argument">>}},
%%     {<<"transfer">>, #{type => <<"transfer">> }} ].
%%
%%% ---------------------------------------------------------------------
%%
%%init(#state_scr{ownerpid=undefined}=State) ->
%%    ?SCR_COMP:next([<<"transferError">>, <<"transfer">>], State);
%%init(#state_scr{ownerpid=OwnerPid, active_component=#component{data=CompData}=Comp}=State) ->
%%    % properties
%%    % Mode = ?EU:to_int(?SCR_ARG:get_int(<<"mode">>, CompData, 0, State)),
%%    SipCode = ?SCR_ARG:get_int(<<"sipCode">>, CompData, 200, State),
%%    Phrase = ?SCR_ARG:get_string(<<"phrase">>, CompData, <<>>, State),
%%    Reason = ?SCR_ARG:get_string(<<"reason">>, CompData, <<>>, State),
%%    RedirectTo = ?SCR_ARG:get_string(<<"redirectTo">>, CompData, <<>>, State),
%%    RemotePartyDN = ?SCR_ARG:get_string(<<"remotePartyDisplay">>, CompData, <<>>, State),
%%    PauseAfterMs = ?SCR_ARG:get_int(<<"pauseAfterMs">>, CompData, 0, State),
%%    % init
%%    CmdRef = make_ref(),
%%    OwnerPid ! {scriptmachine, {ivr_cmd, CmdRef, {ext, ?MODULE, send_answer, self(), #{sipcode => SipCode,
%%                                                                                       phrase => Phrase,
%%                                                                                       reason => Reason,
%%                                                                                       redirectto => RedirectTo,
%%                                                                                       remotepartydisplay => RemotePartyDN}}}},
%%    TimerRef = ?SCR_COMP:event_after(?TIMEOUT, {'timeout',CmdRef}),
%%    CState = #answer{pause_after_ms=PauseAfterMs,
%%                     ref=CmdRef,
%%                     timer_ref=TimerRef},
%%    {ok, State#state_scr{active_component=Comp#component{state=CState}}}.
%%
%%% ---------------------------------------
%%
%%handle_event({timeout,Ref}, #state_scr{active_component=#component{state=#answer{ref=Ref}}}=State) ->
%%    Err = {error,{timeout,<<"Ticket timeout">>}},
%%    fin(Err, State);
%%
%%handle_event({ticket,Ref,{error,_}=Err}, #state_scr{active_component=#component{state=#answer{ref=Ref, timer_ref=TimerRef}}}=State) ->
%%    erlang:cancel_timer(TimerRef),
%%    fin(Err, State);
%%
%%handle_event({ticket,Ref,{ok,filtered}}, #state_scr{active_component=#component{state=#answer{ref=Ref, timer_ref=TimerRef}}}=State) ->
%%    erlang:cancel_timer(TimerRef),
%%    fin(ok, State);
%%
%%handle_event({ticket,Ref,{ok,answered}}, #state_scr{active_component=#component{state=#answer{ref=Ref}=CState}=Comp}=State) ->
%%    #answer{timer_ref=TimerRef, pause_after_ms=PauseAfterMs}=CState,
%%    erlang:cancel_timer(TimerRef),
%%    case PauseAfterMs > 0 of
%%        true ->
%%            TimerRef1 = ?SCR_COMP:event_after(PauseAfterMs, {'pause_timeout',Ref}),
%%            {ok, State#state_scr{active_component=Comp#component{state=CState#answer{timer_ref=TimerRef1}}}};
%%        _ ->
%%            fin(ok, State)
%%    end;
%%
%%handle_event({pause_timeout,Ref}, #state_scr{active_component=#component{state=#answer{ref=Ref}}}=State) ->
%%    fin(ok, State);
%%
%%handle_event(_Ev, State) ->
%%    {ok, State}.
%%
%%% ---------------------------------------
%%
%%%% terminate(State) ->
%%%%     {ok, State}.
%%
%%%% ====================================================================
%%%% Functional iface of SIP IVR dlg (linked to its fsm)
%%%% ====================================================================
%%
%%send_answer({Ref, FromPid, OptsMap}=P, #state_ivr{a=#side{dir='in',state=S}=_Side}=State) when S=='incoming'; S=='ringing'; S=='early' ->
%%    case make_answer(OptsMap, State) of
%%        {_,undefined} -> % invalid response code
%%            send_ticket(FromPid, Ref, {error,{invalid_operation,<<"Invalid response code">>}}),
%%            {next_state, ?CURRSTATE, State};
%%        {_,{Code,_Opts}}=Answer % sdp answers: 183, 2xx
%%          when Code==183 orelse Code>=200 andalso Code=<299 ->
%%            do_answer_sdp(P, Answer, State);
%%        Answer -> % no sdp answers: 180, 182, 3xx-6xx
%%            do_answer_nosdp(P, Answer, State)
%%    end;
%%send_answer({Ref, FromPid, OptsMap}, #state_ivr{a=#side{dir='in',state=S}=_Side}=State) when S=='dialog' ->
%%    case make_answer(OptsMap, State) of
%%        {_,{Code,_Opts}} when Code>=200, Code=<299, S=='dialog' ->
%%            send_ticket(FromPid, Ref, {ok,filtered});
%%        {_,{Code,_Opts}} when Code==183 andalso S=='early' ->
%%            send_ticket(FromPid, Ref, {ok,filtered});
%%        _ ->
%%            send_ticket(FromPid, Ref, {error,{invalid_operation,<<"Call is already answered">>}})
%%    end,
%%    {next_state, ?CURRSTATE, State};
%%send_answer({Ref, FromPid, _OptsMap}, #state_ivr{a=#side{dir='out'}}=State) ->
%%    % #277
%%    % send_ticket(FromPid, Ref, {error,{invalid_operation,<<"Outgoing call could not be answered">>}}),
%%    send_ticket(FromPid, Ref, {ok,filtered}),
%%    {next_state, ?CURRSTATE, State};
%%send_answer({Ref, FromPid, _OptsMap}, State) ->
%%    send_ticket(FromPid, Ref, {error,{invalid_operation,<<"Call is already answered">>}}),
%%    {next_state, ?CURRSTATE, State}.
%%
%%%% ====================================================================
%%%% Internal functions
%%%% ====================================================================
%%
%%%% ---------------------------------
%%%% answer
%%%% ---------------------------------
%%
%%% @private
%%% define sipcode and opts
%%make_answer(OptsMap, _State) ->
%%    SipCode = maps:get(sipcode, OptsMap, undefined),
%%    Opts = [],
%%    Opts1 = case maps:get(phrase,OptsMap,<<>>) of <<>> -> Opts; RPhrase -> [{reason_phrase, ?EU:to_binary(RPhrase)}|Opts] end,
%%    Opts2 = case maps:get(reason,OptsMap,<<>>) of <<>> -> Opts1; Reason -> [{reason, {sip, SipCode, Reason}}|Opts1] end,
%%    Opts3 = case  maps:get(remotepartydisplay,OptsMap,<<>>) of <<>> -> Opts2; RemoteParty -> [{remotepartydisplay, RemoteParty}|Opts2] end,
%%    OptsX = case maps:get(redirectto,OptsMap,<<>>) of <<>> -> Opts3; RedirTo -> [{redirectto, RedirTo}|Opts3] end,
%%    case SipCode of
%%        180 -> S=ringing, R={180, OptsX};
%%        182 -> S=ringing, R={182, OptsX};
%%        183 -> S=early, R={183, OptsX};
%%        200 -> S=dialog, R={200, OptsX};
%%        N when N>=200, N=<299 -> S=dialog, R={200, OptsX}; % ! for refer notifies should be 200
%%        %undefined -> S=dialog, R={180, []};
%%        %undefined -> S=dialog, R={182, []};
%%        %undefined -> S=dialog, R={200, [{remotepartydisplay, <<"IVR_DISPLAY">>}]};
%%        %undefined -> S=declined, R={301, [{contact, <<"012">>}]};
%%        %undefined -> S=declined, R={400, [{reason, {sip, 400, <<"IVR. Test reason">>}}]};
%%        Code when is_integer(Code) andalso Code>=300 andalso Code=<399 -> S=moved, R={Code, OptsX};
%%        Code when is_integer(Code) andalso Code>=400 andalso Code=<699 -> S=declined, R={Code, OptsX};
%%        undefined -> S=dialog, R={200, []};
%%        _ -> S=undefined, R=undefined
%%    end,
%%    {S,R}.
%%
%%% @private and @public
%%% answer no sdp
%%do_answer_nosdp({Ref, FromPid, _OptsMap}, {SideState,SipReply}, #state_ivr{a=Side}=State) ->
%%    #side{rhandle=RHandle, responses=Resp}=Side,
%%    {Code,Opts}=SipReply,
%%    {ok,SipOpts,_Meta} = make_answer_opts(Code,Opts,State),
%%    State1 = ?IVR_UTILS:send_response(RHandle, {Code,SipOpts}, State),
%%    State2 = State1#state_ivr{a=Side#side{state=SideState, responses=[SipReply|Resp]}},
%%    send_ticket(FromPid, Ref, {ok,answered}),
%%    {next_state, ?CURRSTATE, State2}.
%%
%%% @private
%%% answer with sdp
%%do_answer_sdp({Ref, FromPid, _OptsMap}, {SideState,SipReply}, #state_ivr{a=Side}=State) ->
%%    #side{responses=Resp}=Side,
%%    case ?IVR_MEDIA:media_prepare_start(Side, State) of
%%        {error,Reply}=Err ->
%%            send_ticket(FromPid,Ref,Err),
%%            ?IVR_UTILS:error_mgc_final("Error preparing media", State, Side, Reply);
%%        {ok, State1, LSdp} ->
%%            {Code,Opts}=SipReply,
%%            {ok,SipOpts,Meta} = make_answer_opts(Code,Opts,State1),
%%            State2 = ?ACTIVE_UTILS:send_sdp_to_inviter(Code, LSdp, Side, SipOpts, State1),
%%            State3 = State2#state_ivr{a=Side#side{state=SideState,
%%                                                  responses=[SipReply|Resp],
%%                                                  answertime=case Code>200 of true -> os:timestamp(); false -> undefined end},
%%                                      meta=Meta},
%%            send_ticket(FromPid,Ref,{ok,answered}),
%%            {next_state, ?CURRSTATE, State3}
%%    end.
%%
%%% @public
%%% re answer with sdp when replacing
%%do_answer_sdp_replace(ACallId, {SideState,SipReply}, #state_ivr{a=Side}=State) ->
%%    #side{responses=Resp}=Side,
%%    case ?IVR_MEDIA:media_replace(ACallId, Side, State) of
%%        {error,Reply} ->
%%            ?IVR_UTILS:error_mgc_final("Error preparing media on replace", State, Side, Reply);
%%        {ok, State1, LSdp} ->
%%            {Code,Opts}=SipReply,
%%            {ok,SipOpts,_Meta} = make_answer_opts(Code,Opts,State1),
%%            State2 = ?ACTIVE_UTILS:send_sdp_to_inviter(Code, LSdp, Side, SipOpts, State1),
%%            State3 = State2#state_ivr{a=Side#side{state=SideState,
%%                                                  responses=[SipReply|Resp],
%%                                                  answertime=case Code>200 of true -> os:timestamp(); false -> undefined end}},
%%            {next_state, ?CURRSTATE, State3}
%%    end.
%%
%%% @private
%%% 1xx answer opts (remotepartydisplay)
%%make_answer_opts(Code,ROpts,State) when Code>100, Code=<199 ->
%%    % @remoteparty
%%    {RemoteParty,ROpts1} = apply_remoteparty(ROpts,State),
%%    % opts & meta
%%    AOpts = ?U:remotepartyid_opts(RemoteParty,'1xx'),
%%    Meta = [{remotepartyid, RemoteParty}],
%%    make_answer_opts_1(Code, ROpts1, State, Meta, AOpts);
%%% 2xx answer opts (remotepartydisplay, contact)
%%make_answer_opts(Code,ROpts,State) when Code>=200, Code=<299 ->
%%    % @remoteparty
%%    {RemoteParty,ROpts1} = apply_remoteparty(ROpts,State),
%%    % @contact
%%    LContact = ?U:build_local_contact(#uri{scheme=sip, user=RemoteParty#uri.user}), % @localcontact (cfg=default! | Top via's domain!)
%%    % opts & meta
%%    AOpts = ?U:remotepartyid_opts(RemoteParty,'2xx') ++ [{contact, LContact}],
%%    Meta = [{remotepartyid, RemoteParty},
%%            {contact, LContact}],
%%    make_answer_opts_1(Code, ROpts1, State, Meta, AOpts);
%%% 3xx answer opts (contact)
%%make_answer_opts(Code,ROpts,State) when Code >=300, Code=<399 ->
%%    case lists:keytake(redirectto,1,ROpts) of
%%        {value, {_,RedirTo}, ROpts1} when RedirTo /= <<>> -> ok;
%%        _ ->
%%            RedirTo = <<"undefined">>,
%%            ROpts1 = ROpts
%%    end,
%%    % contact
%%    LContact = #uri{scheme=sip, user=RedirTo, domain=maps:get(domain,State#state_ivr.map)}, % @localcontact! (cluster internal domain)
%%    % opts & meta
%%    AOpts = [{contact, LContact}],
%%    Meta = [{contact, LContact}],
%%    make_answer_opts_1(Code, ROpts1, State, Meta, AOpts);
%%% other answer opts
%%make_answer_opts(Code,ROpts,State) -> make_answer_opts_1(Code, ROpts, State, [], []).
%%
%%% @private
%%apply_remoteparty(ROpts,State) ->
%%    % @todo @remoteparty remote-party-id option
%%    case lists:keytake(remotepartydisplay,1,ROpts) of
%%        {value,{_,R},ROpts1} when R /= <<>> -> DN = R;
%%        _ -> DN = ?U:make_ivr_display(), ROpts1 = ROpts
%%    end,
%%    U = ?U:make_ivr_user(),
%%    Domain = maps:get(domain,State#state_ivr.map),
%%    RemoteParty = #uri{scheme=sip, disp=?U:quote_display(?U:unquote_display(DN)), user=U, domain=Domain},
%%    {RemoteParty,ROpts1}.
%%
%%
%%% common answer opts (reason, custom headers (resend in b2bua), ...)
%%make_answer_opts_1(_Code,ROpts,_State,Meta,AOpts) ->
%%    AOpts1 = case lists:keyfind('reason_phrase',1,ROpts) of
%%                 {_,RP} when RP /= <<>> -> [{reason_phrase,RP}|AOpts];
%%                 _ -> AOpts
%%             end,
%%    AOpts2 = case lists:keyfind('reason',1,ROpts) of
%%                 {_,R} when R /= <<>> -> [{reason,R}|AOpts1];
%%                 _ -> AOpts1
%%             end,
%%    {ok,AOpts2,Meta}.
%%
%%%% ------------------------------
%%%% finalize
%%%% ------------------------------
%%fin({error,_}=Err, _State) -> Err;
%%fin(ok, State) -> ?SCR_COMP:next(<<"transfer">>,State).
%%
%%%% ------------------------------
%%%% send ticket to script machine
%%%%
%%send_ticket(Pid, Ref, Res) ->
%%    case is_pid(Pid) of
%%        true -> ?SCR_COMP:event(Pid, {'ticket',Ref,Res});
%%        false -> ok
%%    end.
