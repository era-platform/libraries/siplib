%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 10.07.2019
%%% @doc RP-1470 TODO
%%%      IVR-component ASR (Automatic Speech Recognition).
%%%      Implements integration to MRCP Server to listen and recognize speech to text, powered by MRCP-Server.

-module(r_sip_ivr_script_component_asr).
-author('Peter Bukashin <tbotc@yandex.ru>').

%%-compile([export_all,nowarn_export_all]).
%%-export([init/1,
%%         handle_event/2,
%%         metadata/1]).
%%
%%%% ====================================================================
%%%% Define
%%%% ====================================================================
%%
%%%-include("../include/r_script.hrl").
%%-include("../../era_script/include/r_script.hrl").
%%
%%-include("../include/r_sip_ivr.hrl").
%%-include("../include/r_sip_nk.hrl").
%%
%%-define(U, r_sip_utils).
%%
%%-record(data, {
%%    state :: wait_ticket | performing | terminating,
%%    ref,
%%    timer_ref
%%}).
%%
%%-define(TIMEOUT, 10000).
%%
%%-define(S_WAIT_TICKET, wait_ticket).
%%-define(S_PLAYING, performing).
%%-define(S_TERMINATING, terminating).
%%
%%%% ====================================================================
%%%% Public functions
%%%% ====================================================================
%%
%%% returns descriptions of used properties
%%metadata(_ScriptType) ->
%%    [].
%%
%%% ---------------------------------------------------------------------
%%init(#state_scr{ownerpid=undefined}=State) ->
%%    ?SCR_COMP:next([<<"transferError">>, <<"transfer">>], State);
%%init(#state_scr{ownerpid=OwnerPid, active_component=#component{data=_CompData}=Comp}=State) ->
%%    % properties
%%    CS = #data{},
%%    % init
%%    CmdRef = make_ref(),
%%    {Cmd,OptsMap} = {start,#{}},
%%    OwnerPid ! {scriptmachine, {ivr_cmd, CmdRef, {ext, ?MODULE, Cmd, self(), OptsMap}}},
%%    TimerRef1 = ?SCR_COMP:event_after(?TIMEOUT, {'timeout_state',CmdRef,S=?S_WAIT_TICKET}),
%%    {ok, State#state_scr{active_component=Comp#component{state=CS#data{state = S,
%%                                                                       ref = CmdRef,
%%                                                                       timer_ref = TimerRef1}}}}.
%%
%%% ---------------------------------------
%%
%%% timeout
%%handle_event({timeout,Ref}, #state_scr{active_component=#component{state=#data{ref=Ref}}}=State) ->
%%    {ok, State}.
%%
%%%% ====================================================================
%%%% Internal functions
%%%% ====================================================================