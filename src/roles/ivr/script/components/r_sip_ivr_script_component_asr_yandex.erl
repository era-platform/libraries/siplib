%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 15.07.2019
%%% @doc RP-1466
%%%      IVR-component ASR (Automatic Speech Recognition).
%%%      Implements integration to Yandex-Cloud Speech Kit over HTTP.
%%%      DevDoc https://oktell.atlassian.net/wiki/spaces/KD/pages/1339424811/MRCP.+ASR+TTS
%%%      Algorithm:
%%%        - Init:
%%%          - Modifies RTP term to detect VAD
%%%          - Starts internal recording to raw L16 file buffered by 1000 ms. Waits reply ticket.
%%%          - Creates and starts internal ghost component PLAYFILE
%%%          - Setup topology to avoid recording of player.
%%%        - Preplay:
%%%          - Translates events of PLAYFILE to internal ghost
%%%          - Waits for VAD or DTMF to stop PLAYFILE (and remove Player termination from context)
%%%          - Setup minus 3 seconds from now() to avoid sending silence from record to YSK.
%%%        - Initialization:
%%%          - Check recordfile, if data is present then connects to Yandex-Cloud Speech Kit, sends part of recorded content as first chunk
%%%          - Enables timers (ticket, general)
%%%        - Process:
%%%          - Every 1 second takes next written raw audio file data and send as chunk portion into Yandex connection
%%%          - Detects DTMF to interrupt process.
%%%          - Detects VAD up/down and starts/enables silence timer.
%%%          - Handles silence timer to interrupt process.
%%%          - Handles general timer to interrupt process.
%%%        - Final
%%%          1. Removes 'ivrr' term
%%%          2. Modifies RTP term to stop VAD.
%%%          3. Takes last part of recorded file and finalizes request to Yandex Cloud by sending last chunk.
%%%          4. Waits response from Yandex Cloud Speech Kit
%%%          5. Parses response and choose best variant
%%%      FSM:
%%%        States: starting, preplaying, performing, stopping, recognizing
%%%        Events: init, ticket_started_ok, ticket_started_error, ticket_started_timeout, dtmf, vad_changed,
%%%                [any_event], [preplay_stopped],
%%%                chunk_timer, silence_timeout, general_timeout, ticket_stopped_ok, ticket_stopped_error, ticket_stopped_timeout,
%%%                http_response, http_timeout
%%%        Operations: start_media (enable_vad, start_recorder),
%%%                    on_started_media (get_mg_file_info), stop_ticket_timer
%%%                    start_preplay (prepare_player_component, build_player_data, init_player), stop_preplay, apply_player_result,
%%%                    setup_topology,
%%%                    perform (start_general_timer), start_chunk_timer,
%%%                    apply_dtmf, apply_vad_change (start_silence_timer, stop_silence_timer),
%%%                    maybe_start_http (start_http, http_init, http_request, fun_chunk),
%%%                    stop_performing_timers, stop_media (disable_vad, detach_recorder),
%%%                    on_stopped_media (check_apply_stored_http_response, finalize_chunking), finalize_record_file (drop_file, move_file), parse_response
%%%                    abort (abort_media, abort_internal, close_connection), switch, store

-module(r_sip_ivr_script_component_asr_yandex).
-author('Peter Bukashin <tbotc@yandex.ru>').

%%-compile([export_all,nowarn_export_all]).
%%-export([init/1,
%%         handle_event/2,
%%         terminate/1,
%%         metadata/1]).
%%
%%%% ====================================================================
%%%% Define
%%%% ====================================================================
%%
%%%-include("../include/r_script.hrl").
%%-include("../../era_script/include/r_script.hrl").
%%
%%-include("../include/r_sip_ivr.hrl").
%%-include("../include/r_sip_nk.hrl").
%%-include("../include/r_sip_mgc.hrl").
%%
%%-define(U, r_sip_utils).
%%-define(CFG, r_sip_config).
%%
%%-record(data, {
%%               state :: starting | performing | stopping | recognizing,
%%               %
%%               yandex_uri :: binary(),
%%               yandex_api_key :: binary(),
%%               yandex_folder_id :: binary(),
%%               recorder_id :: binary() | undefined,
%%               temp_file = <<>> :: binary(),
%%               %
%%               preplay_file = <<>> :: binary(),
%%               save_record :: boolean(),
%%               %
%%               topic :: binary(),
%%               lang :: binary(),
%%               profanity_filter :: boolean(),
%%               %
%%               record_timeout = 0 :: integer(),
%%               check_dtmf :: boolean(),
%%               buffer :: binary(),
%%               max_count :: integer(),
%%               interrupts :: [binary()],
%%               clear_buffer :: boolean(),
%%               clear_interrupt :: boolean(),
%%               abort_on_silence :: boolean(),
%%               silence_timeout :: integer(),
%%               vad_threshold :: integer(),
%%               response_timeout :: integer(),
%%               %
%%               mgid :: term(),
%%               mgfileinfo :: map(),
%%               chunk_pid :: pid(),
%%               stored_http_response :: tuple(),
%%               http_request_id :: reference(),
%%               vad_state = 'down' :: up | down,
%%               player_component :: term(), % play component metadata
%%               record_offset = 0 :: integer(),
%%               %
%%               pid :: pid(),
%%               ref :: reference(),
%%               ticket_timer_ref :: reference(),
%%               general_timer_ref :: reference(),
%%               chunk_timer_ref :: reference(),
%%               silence_timer_ref :: reference(),
%%               http_timer_ref :: reference()
%%              }).
%%
%%-define(TIMEOUT_TICKET, 10000).
%%-define(TIMEOUT_PERFORMING, 5000).
%%
%%-define(CURRSTATE, 'active').
%%
%%-define(S_STARTING, starting).
%%-define(S_PREPLAYING, preplaying).
%%-define(S_PERFORMING, performing).
%%-define(S_STOPPING, stopping).
%%-define(S_RECOGNIZING, recognizing).
%%
%%-define(VadEvtID, 1239).
%%-define(RtxVadDur, 30).
%%-define(RtxSilenceDur, 500).
%%
%%-define(PROFILE, 'asr_yandex_short').
%%-define(URI, "https://stt.api.cloud.yandex.net/speech/v1/stt:recognize").
%%
%%-define(PlayComponent, r_sip_ivr_script_component_play).
%%
%%-define(SampleRateHz, 8000).
%%
%%%% ====================================================================
%%%% Public functions
%%%% ====================================================================
%%
%%% ---------------------------------------------------------------------
%%%% returns descriptions of used properties
%%% ---------------------------------------------------------------------
%%metadata(_ScriptType) ->
%%    [{<<"topic">>, #{type => <<"list">>,
%%                     items => [{0,<<"general">>},
%%                               {1,<<"maps">>},
%%                               {2,<<"dates">>},
%%                               {3,<<"names">>},
%%                               {4,<<"numbers">>},
%%                               {100,<<"custom">>}] }},
%%     {<<"topicCustom">>, #{type => <<"argument">>,
%%                           filter => <<"topic==100">> }},
%%     {<<"lang">>, #{type => <<"list">>,
%%                    items => [{0,<<"ru-RU">>},
%%                              {1,<<"en-US">>}]}},
%%     {<<"profanityFilter">>, #{type => <<"list">>,
%%                               items => [{0,<<"false">>},
%%                                         {1,<<"true">>}] }},
%%     %{<<"groups">>, #{type => <<"argument">>}},
%%     {<<"recordTimeoutSec">>, #{type => <<"argument">>}},
%%     {<<"checkDTMF">>, #{type => <<"list">>,
%%                         items => [{0,<<"no">>},
%%                                   {1,<<"yes">>}] }},
%%     {<<"dtmfBuffer">>, #{type => <<"variable">>,
%%                          filter => <<"checkDTMF==1">> }},
%%     {<<"clearDtmfBuffer">>, #{type => <<"list">>,
%%                               items => [{0,<<"no">>},
%%                                         {1,<<"yes">>}],
%%                               filter => <<"(checkDTMF==1) && (dtmfBuffer!=null)">> }},
%%     {<<"maxSymbolCount">>, #{type => <<"argument">>,
%%                              filter => <<"checkDTMF==1">> }},
%%     {<<"interruptSymbols">>, #{type => <<"string">>,
%%                                filter => <<"checkDTMF==1">> }},
%%     {<<"abortOnSilence">>, #{type => <<"list">>,
%%                              items => [{0,<<"no">>},
%%                                        {1,<<"yes">>}] }},
%%     {<<"silenceTimeoutSec">>, #{type => <<"argument">>,
%%                                 filter => <<"abortOnSilence==1">> }},
%%     {<<"vadThreshold">>, #{type => <<"argument">>,
%%                            filter => <<"abortOnSilence==1">> }},
%%     {<<"responseTimeoutSec">>, #{type => <<"argument">>}},
%%     {<<"varText">>, #{type => <<"variable">>}},
%%     %{<<"varGroup">>, #{type => <<"variable">>}},
%%     {<<"varHttpCode">>, #{type => <<"variable">>}},
%%     {<<"varHttpBody">>, #{type => <<"variable">>}},
%%     {<<"saveRec">>, #{type => <<"list">>,
%%                       items => [{0,<<"no">>},
%%                                 {1,<<"yes">>}] }},
%%     {<<"varRecordPath">>, #{type => <<"variable">>,
%%                             filter => <<"saveRec==1">> }},
%%     {<<"prePlayFile">>, #{type => <<"file">>}},
%%     %{<<"finSignal">>, #{type => <<"file">>}},
%%     {<<"transfer">>, #{type => <<"transfer">> }},
%%     {<<"transferTimeout">>, #{type => <<"transfer">>,
%%                               title => <<"timeout">> }},
%%     {<<"transferError">>, #{type => <<"transfer">>,
%%                             title => <<"error">> }} ].
%%
%%% ---------------------------------------------------------------------
%%init(#state_scr{ownerpid=undefined}=State) ->
%%    ?SCR_COMP:next(<<"transferError">>, State);
%%init(#state_scr{active_component=Comp}=State) ->
%%    CS = prepare_param(State),
%%    State1 = State#state_scr{active_component=Comp#component{state=CS#data{state = ?S_STARTING,
%%                                                                           pid = self(),
%%                                                                           ref = make_ref()}}},
%%    fsm(init,State1).
%%
%%%% @private
%%prepare_param(#state_scr{scriptid=IvrId, domain=Domain, active_component=#component{data=CompData}=_Comp}=State) ->
%%    {TS,RecPath} = ?U:get_media_context_rec_opts(),
%%    {ok,{Uri,ApiKey,FolderId}} = get_yandex_keys(Domain),
%%    #data{yandex_uri = Uri,
%%          yandex_api_key = ApiKey,
%%          yandex_folder_id = FolderId,
%%          recorder_id = ?EU:to_binary(1100000000 + ?EU:random(100000000)),
%%          temp_file = filename:join([RecPath, ?EU:str("ivrr_~s_~s.raw", [?EU:to_list(IvrId), ?EU:to_list(TS)])]),
%%          %
%%          topic = case ?SCR_COMP:get(<<"topic">>, CompData, 1) of
%%                      0 -> <<"general">>;
%%                      1 -> <<"maps">>;
%%                      2 -> <<"dates">>;
%%                      3 -> <<"names">>;
%%                      4 -> <<"numbers">>;
%%                      100 -> ?SCR_ARG:get_string(<<"topicCustom">>, CompData, <<>>, State)
%%                  end,
%%          lang = case ?SCR_ARG:get_int(<<"lang">>, CompData, 1, State) of
%%                     0 -> <<"ru-RU">>;
%%                     1 -> <<"en-US">>
%%                 end,
%%          profanity_filter = ?EU:to_bool(?SCR_COMP:get(<<"profanityFilter">>, CompData, 0)),
%%          %
%%          record_timeout = max(0, min(60, ?SCR_ARG:get_int(<<"recordTimeoutSec">>, CompData, 0, State))) * 1000,
%%          check_dtmf = ?EU:to_bool(?SCR_COMP:get(<<"checkDTMF">>, CompData, 1)),
%%          max_count = ?SCR_ARG:get_int(<<"maxSymbolCount">>, CompData, 0, State),
%%          interrupts = ?IVR_SCRIPT_UTILS:parse_interrupts(<<"interruptSymbols">>, CompData),
%%          clear_buffer = ?EU:to_bool(?SCR_COMP:get(<<"clearDtmfBuffer">>, CompData, 1)),
%%          clear_interrupt = ?EU:to_bool(?SCR_COMP:get(<<"clearInterrupt">>, CompData, 0)),
%%          abort_on_silence = ?EU:to_bool(?SCR_COMP:get(<<"abortOnSilence">>, CompData, 1)),
%%          silence_timeout = max(1, ?SCR_ARG:get_int(<<"silenceTimeoutSec">>, CompData, 1, State)) * 1000 - ?RtxSilenceDur,
%%          vad_threshold = max(20, min(60, ?SCR_ARG:get_int(<<"vadThreshold">>, CompData, 30, State))),
%%          response_timeout = case max(0, min(60, ?SCR_ARG:get_int(<<"responseTimeoutSec">>, CompData, 5, State))) * 1000 of
%%                                 0 -> 60000;
%%                                 T -> T
%%                             end,
%%          %
%%          preplay_file = ?SCR_COMP:get_file(<<"prePlayFile">>, CompData, <<>>, State),
%%          save_record = ?EU:to_bool(?SCR_COMP:get(<<"saveRec">>, CompData, 0))
%%         }.
%%
%%%% @private
%%get_yandex_keys(Domain) ->
%%    case ?ENVDC:get_object_sticky(Domain,settings,[{keys,[<<"yandex_cloud">>]},{fields,[value]}],auto) of
%%        {ok,[Item],_HC} ->
%%            Value = maps:get(value,Item),
%%            case maps:get(<<"speech">>,Value,undefined) of
%%                undefined -> {error,{'yandex_api_key',not_found}};
%%                Speech ->
%%                    case maps:get(<<"apiKey">>,Speech,<<>>) of
%%                        <<>> -> {error,{'yandex_api_key',not_found}};
%%                        ApiKey ->
%%                            Uri = case maps:get(<<"uri_asr_short">>,Speech,?URI) of <<>> -> ?URI; V -> ?EU:to_list(V) end,
%%                            {ok, {Uri, ApiKey, maps:get(<<"folderId">>,Value,<<>>)}}
%%                    end
%%            end;
%%        _ -> {error,{'yandex_api_key',not_found}}
%%    end.
%%
%%%% ---------------------------------------------------------------------
%%handle_event(Event, State) ->
%%    handle_event_1(Event, State).
%%
%%%% ---------------------------------
%%%% vad from mg
%%handle_event_1({vad_threshold, CtxTermId, Event}, State) ->
%%    fsm({vad_threshold, CtxTermId, Event}, State);
%%
%%%% ---------------------------------
%%%% dtmf from mg (rfc2833, in-band)
%%handle_event_1({mg_dtmf,Dtmf}, State) ->
%%    fsm({dtmf,Dtmf}, State);
%%
%%%% ---------------------------------------------------------------------
%%%% any events during PREPLAYING are transmitting into decorated PLAY(204) component
%%handle_event_1(Event, #state_scr{active_component=#component{state=#data{state=?S_PREPLAYING,player_component=PlayerComp}=CS}=Comp}=State) ->
%%    F = fun(#state_scr{active_component=PlayerCompX}=StateX) ->
%%                StateX#state_scr{active_component=Comp#component{state=CS#data{player_component=PlayerCompX}}}
%%        end,
%%    case ?PlayComponent:handle_event(Event, State#state_scr{active_component=PlayerComp}) of
%%        {ok,State1} -> {ok,F(State1)};
%%        {next,_Id,State1} ->
%%            {ok,State2} = ?PlayComponent:terminate(State1),
%%            apply_player_result(F(State2))
%%    end;
%%
%%%% ---------------------------------
%%%%
%%handle_event_1({'ticket_started_ok',Ref,MGID}, #state_scr{active_component=#component{state=#data{state=?S_STARTING,ref=Ref}}}=State) ->
%%    fsm({'ticket_started_ok',MGID},State);
%%%%
%%handle_event_1({'ticket_started_error',Ref,Err}, #state_scr{active_component=#component{state=#data{state=?S_STARTING,ref=Ref}}}=State) ->
%%    fsm({'ticket_started_error',Err},State);
%%%%
%%handle_event_1({'ticket_started_timeout',Ref,S}, #state_scr{active_component=#component{state=#data{state=S=?S_STARTING,ref=Ref}}}=State) ->
%%    fsm('ticket_started_timeout',State);
%%
%%%% ---------------------------------
%%%%
%%handle_event_1({'chunk_pid_reg',Ref,Pid}, #state_scr{active_component=#component{state=#data{ref=Ref}}}=State) ->
%%    fsm({'chunk_pid_reg',Pid},State);
%%
%%%% ---------------------------------
%%%%
%%handle_event_1({'chunk_timer',Ref,S}, #state_scr{active_component=#component{state=#data{state=S=?S_PERFORMING,ref=Ref}}}=State) ->
%%    fsm('chunk_timer',State);
%%%%
%%handle_event_1({'silence_timeout',Ref,S}, #state_scr{active_component=#component{state=#data{state=S=?S_PERFORMING,ref=Ref}}}=State) ->
%%    fsm('silence_timeout',State);
%%%%
%%handle_event_1({'general_timeout',Ref,S}, #state_scr{active_component=#component{state=#data{state=S=?S_PERFORMING,ref=Ref}}}=State) ->
%%    fsm('general_timeout',State);
%%
%%%% dtmf from sip (info)
%%handle_event_1({sip_info,Req}, State) ->
%%    ?U:get_dtmf_from_sipinfo(Req, fun(Dtmf) -> fsm({dtmf,Dtmf},State) end, fun() -> {ok,State} end);
%%
%%%% ---------------------------------
%%%%
%%handle_event_1({'ticket_stopped_ok',Ref,S}, #state_scr{active_component=#component{state=#data{state=S=?S_STOPPING,ref=Ref}}}=State) ->
%%    fsm('ticket_stopped_ok',State);
%%%%
%%handle_event_1({'ticket_stopped_error',Ref,Err}, #state_scr{active_component=#component{state=#data{state=?S_STOPPING,ref=Ref}}}=State) ->
%%    fsm({'ticket_stopped_error',Err},State);
%%%%
%%handle_event_1({'ticket_stopped_timeout',Ref,S}, #state_scr{active_component=#component{state=#data{state=S=?S_STOPPING,ref=Ref}}}=State) ->
%%    fsm('ticket_stopped_timeout',State);
%%
%%%% ---------------------------------
%%%%
%%handle_event_1({'http_response',Ref,ReplyInfo}, #state_scr{active_component=#component{state=#data{ref=Ref}}}=State) ->
%%    fsm({'http_response',ReplyInfo},State);
%%%%
%%handle_event_1({'http_timeout',Ref,S}, #state_scr{active_component=#component{state=#data{state=S=?S_RECOGNIZING,ref=Ref}}}=State) ->
%%    fsm('http_timeout',State);
%%
%%%% ---------------------------------
%%%% other
%%handle_event_1(_Event, State) ->
%%    {ok, State}.
%%
%%%% ---------------------------------------------------------------------
%%terminate(State) ->
%%    abort_internal(State).
%%
%%%% ====================================================================
%%%% Internal FSM implementation
%%%% ====================================================================
%%
%%%% --------------------------------------------------------------------
%%-spec fsm(Event::term(), State::#state_scr{}) -> {ok,State::#state_scr{}} | {error,Reason::term()}.
%%%% --------------------------------------------------------------------
%%fsm(Event,State) ->
%%    fsm(Event,get_data(State),State).
%%
%%%% --------------------------------------------------------------------
%%%% @private
%%%% state = starting
%%fsm('init', #data{state=?S_STARTING}, State) -> start_media(State);
%%fsm({'ticket_started_ok',MGID}, #data{state=?S_STARTING}, State) -> on_started_media(MGID,State);
%%fsm({'ticket_started_error',Err}, #data{state=?S_STARTING}, State) -> abort({error,{'ticket_started_error',Err}},State);
%%fsm('ticket_started_timeout', #data{state=?S_STARTING}, State) -> abort({error,'ticket_started_timeout'},State);
%%fsm({'vad_threshold', _CtxTermId, _Event}=VAD, #data{state=?S_STARTING}, State) -> apply_vad(VAD, State);
%%fsm({'dtmf',DTMF}, #data{state=?S_STARTING}, State) -> apply_dtmf(DTMF, State);
%%%% state = playing
%%fsm({'vad_threshold', _CtxTermId, _Event}=VAD, #data{state=?S_PREPLAYING}, State) -> apply_vad(VAD, perform(stop_preplay(State)));
%%fsm({'dtmf',DTMF}, #data{state=?S_PREPLAYING}, State) -> apply_dtmf(DTMF, perform(stop_preplay(State)));
%%%% state = performing
%%fsm({'chunk_pid_reg',Pid}, #data{}, State) -> store({'chunk_pid',Pid},State);
%%fsm('chunk_timer', #data{state=?S_PERFORMING}, State) -> maybe_start_http(State);
%%fsm({'vad_threshold', _CtxTermId, _Event}=VAD, #data{state=?S_PERFORMING}, State) -> apply_vad(VAD, State);
%%fsm({'dtmf',DTMF}, #data{state=?S_PERFORMING}, State) -> apply_dtmf(DTMF, State);
%%fsm('silence_timeout', #data{state=?S_PERFORMING}, State) -> switch(?S_STOPPING,stop_media(stop_performing_timers(State)));
%%fsm('general_timeout', #data{state=?S_PERFORMING}, State) -> switch(?S_STOPPING,stop_media(stop_performing_timers(State)));
%%fsm({'http_response',ReplyInfo}, #data{state=?S_PERFORMING}, State) -> abort({error,{'http_response',ReplyInfo}},State);
%%%% state = stopping
%%fsm('ticket_stopped_ok', #data{state=?S_STOPPING}, State) -> on_stopped_media(State);
%%fsm({'ticket_stopped_error',Err}, #data{state=?S_STOPPING}, State) -> abort({error,{'ticket_stopped_error',Err}},State);
%%fsm('ticket_stopped_timeout', #data{state=?S_STOPPING}, State) -> abort({error,'ticket_stopped_timeout'},State);
%%fsm({'http_response',ReplyInfo}, #data{state=?S_STOPPING}, State) -> store({'stored_http_response',ReplyInfo},State);
%%fsm({'dtmf',_DTMF}, #data{state=?S_STOPPING}, State) -> {keep,State};
%%%% state = recognizing
%%fsm({'http_response',ReplyInfo}, #data{state=?S_RECOGNIZING}, State) -> parse_response(ReplyInfo,stop_http_timer(State));
%%fsm('http_timeout', #data{state=?S_RECOGNIZING}, State) -> abort({error,http_timeout},State);
%%fsm({'dtmf',_DTMF}, #data{state=?S_RECOGNIZING}, State) -> {keep,State};
%%%% unknown events
%%fsm(_Event, _Data, State) -> {ok,State}.
%%
%%%% --------------------------------------------------------------------
%%%% @private
%%%% FSM actions
%%%% --------------------------------------------------------------------
%%%%
%%on_started_media(MGID,State) -> do_on_started_media(MGID,c(State)).
%%%%
%%start_media(State) -> do_start_media(c(State)).
%%%%
%%stop_media(State) -> do_stop_media(c(State)).
%%%%
%%start_preplay(State) -> do_start_preplay(c(State)).
%%%%
%%stop_preplay(State) -> do_stop_preplay(c(State)).
%%%%
%%setup_topology(State) -> do_setup_topology(c(State)).
%%%%
%%perform(State) -> do_perform(c(State)).
%%%%
%%abort(Error,State) -> do_abort(Error,c(State)).
%%%%
%%apply_vad(Vad,State) -> do_apply_vad(Vad,c(State)).
%%%%
%%apply_dtmf(DTMF,State) -> do_apply_dtmf(DTMF,c(State)).
%%%%
%%maybe_start_http(State) -> do_maybe_start_http(c(State)).
%%%%
%%parse_response(Content,State) -> do_parse_response(Content,c(State)).
%%%%
%%finalize_file(State) -> do_finalize_file(c(State)).
%%%%
%%on_stopped_media(State) -> do_on_stopped_media(c(State)).
%%%%
%%switch(NewState, {ok,State}) -> switch(NewState, c(State));
%%switch(NewState, State) when is_atom(NewState) ->
%%    Data=get_data(State),
%%    {ok,set_data(Data#data{state=NewState},State)}.
%%%%
%%store({Key,Value}, {ok,State}) -> store({Key,Value}, c(State));
%%store({Key,Value}, State) when is_atom(Key) ->
%%    Data=get_data(State),
%%    Data1 = case Key of
%%                'chunk_pid' -> Data#data{chunk_pid=Value};
%%                'mgid' -> Data#data{mgid=Value};
%%                'stored_http_response' -> Data#data{stored_http_response=Value}
%%            end,
%%    {ok,set_data(Data1,State)}.
%%%%
%%start_general_timer({ok,State}) -> start_general_timer(c(State));
%%start_general_timer(State) ->
%%    #data{ref=CmdRef,record_timeout=Timeout}=Data = get_data(State),
%%    case Timeout of
%%        0 -> {ok,State};
%%        Timeout ->
%%            TimerRef = ?SCR_COMP:event_after(Timeout, {'general_timeout',CmdRef,?S_PERFORMING}),
%%            {ok,set_data(Data#data{general_timer_ref=TimerRef}, State)}
%%    end.
%%%%
%%start_chunk_timer({ok,State}) -> start_chunk_timer(c(State));
%%start_chunk_timer(State) ->
%%    #data{ref=CmdRef}=Data = get_data(State),
%%    Timeout = 200,
%%    TimerRef = ?SCR_COMP:event_after(Timeout, {'chunk_timer',CmdRef,?S_PERFORMING}),
%%    {ok,set_data(Data#data{chunk_timer_ref=TimerRef}, State)}.
%%%%
%%stop_ticket_timer({ok,State}) -> stop_ticket_timer(c(State));
%%stop_ticket_timer(State) ->
%%    #data{ticket_timer_ref=TimerRef}=Data = get_data(State),
%%    ?EU:cancel_timer(TimerRef),
%%    {ok,set_data(Data#data{ticket_timer_ref=undefined}, State)}.
%%%%
%%stop_performing_timers({ok,State}) -> stop_performing_timers(c(State));
%%stop_performing_timers(State) ->
%%    #data{chunk_timer_ref=TimerRef1,
%%          general_timer_ref=TimerRef2,
%%          silence_timer_ref=TimerRef3}=Data = get_data(State),
%%    ?EU:cancel_timer(TimerRef1),
%%    ?EU:cancel_timer(TimerRef2),
%%    ?EU:cancel_timer(TimerRef3),
%%    {ok,set_data(Data#data{chunk_timer_ref=undefined,
%%                           general_timer_ref=undefined,
%%                           silence_timer_ref=undefined}, State)}.
%%%%
%%stop_http_timer({ok,State}) -> stop_http_timer(c(State));
%%stop_http_timer(State) ->
%%    #data{http_timer_ref=TimerRef}=Data = get_data(State),
%%    ?EU:cancel_timer(TimerRef),
%%    {ok,set_data(Data#data{http_timer_ref=undefined}, State)}.
%%
%%%% --------------------------------------------------------------------
%%do_on_started_media(MGID,#state_scr{}=State) ->
%%    {ok,State1} = stop_ticket_timer(store({'mgid',MGID},State)),
%%    case get_mg_file_info(State1) of
%%        false -> do_abort({error,mg_file_info},State1);
%%        {ok,MGFileInfo} ->
%%            #data{preplay_file=PlayFile}=CS = get_data(State1),
%%            State2 = set_data(CS#data{mgfileinfo=MGFileInfo},State1),
%%            case PlayFile of
%%                <<>> -> setup_topology(perform(State2));
%%                _ -> setup_topology(start_preplay(State2)) % async, but consistent in ivr-fsm pid
%%            end end.
%%
%%%% @private
%%get_mg_file_info(#state_scr{}=State) ->
%%    #data{mgid=MGID,
%%          temp_file=MGFile} = get_data(State),
%%    case ?U:parse_mg_devicename(?U:get_mg_devicename(MGID)) of
%%        undefined -> false;
%%        {Addr,Postfix} ->
%%            RecPath = ?CFG:get_record_path(Addr,Postfix),
%%            SrcPath = filename:join([RecPath|lists:nthtail(1,filename:split(MGFile))]),
%%            case ?CFG:get_mg_node_by_addr_postfix(Addr,Postfix) of
%%                undefined -> false;
%%                {MGSite,MGNode} -> {ok,#{file => MGFile,
%%                                         site => MGSite,
%%                                         node => MGNode,
%%                                         recpath => RecPath,
%%                                         srcpath => SrcPath}}
%%            end end.
%%
%%%% --------------------------------------------------------------------
%%do_start_preplay(#state_scr{}=State) ->
%%    {ok,State1} = switch(?S_PREPLAYING,State),
%%    {ok,State2} = prepare_player_component(State1),
%%    init_player(State2).
%%
%%%% @private
%%prepare_player_component(#state_scr{}=State) ->
%%    #data{}=CS = get_data(State),
%%    Id = PlayerId = 1100000000 + ?EU:random(100000000),
%%    Type = 204,
%%    Name = <<"ASR Yandex Player">>,
%%    PlayerComp = #component{id = Id,
%%                            type = Type,
%%                            name = Name,
%%                            module = ?PlayComponent,
%%                            data = build_player_data({Id,Type,Name,PlayerId},State),
%%                            state = undefined},
%%    {ok,set_data(CS#data{player_component = PlayerComp},State)}.
%%
%%%% @private
%%build_player_data({Id,Type,Name,PlayerId}, #state_scr{active_component=#component{data=CData}}=_State) ->
%%    [{<<"oId">>,Id},
%%     {<<"oType">>,Type},
%%     {<<"name">>,Name},
%%     {<<"playerId">>,PlayerId},
%%     {<<"mode">>,0},
%%     {<<"file">>,?EU:get_by_key(<<"prePlayFile">>,CData)},
%%     {<<"transfer">>,-1},
%%     {<<"transferError">>,-1}].
%%
%%%% @private
%%init_player(#state_scr{active_component=Comp}=State) ->
%%    #data{player_component=PlayerComp}=CS = get_data(State),
%%    F = fun(#state_scr{active_component=PlayerCompX}=StateX) ->
%%                StateX#state_scr{active_component=Comp#component{state=CS#data{player_component=PlayerCompX}}}
%%        end,
%%    case ?PlayComponent:init(State#state_scr{active_component=PlayerComp}) of
%%        {ok,State1} -> {ok,F(State1)};
%%        {next,_Id,State1} ->
%%            {ok,State2} = ?PlayComponent:terminate(State1),
%%            apply_player_result(F(State2))
%%    end.
%%
%%%% --------------------------------------------------------------------
%%do_stop_preplay(#state_scr{active_component=Comp}=State) ->
%%    #data{player_component=PlayerComp}=CS = get_data(State),
%%    F = fun(#state_scr{active_component=PlayerCompX}=StateX) ->
%%                StateX#state_scr{active_component=Comp#component{state=CS#data{player_component=PlayerCompX}}}
%%        end,
%%    case ?PlayComponent:handle_event('stop_play',State#state_scr{active_component=PlayerComp}) of
%%        {ok,State1} -> {ok,record_offset(F(State1))};
%%        {next,_Id,State1} ->
%%            {ok,State2} = ?PlayComponent:terminate(State1),
%%            {ok,record_offset(F(State2))}
%%    end.
%%
%%%% @private
%%record_offset(State) ->
%%    #data{mgfileinfo=MGFileInfo}=CS = get_data(State),
%%    BufferSize = 3*16000,
%%    FromByte = case read_file_endpart(MGFileInfo,0) of
%%                   {ok,_D} when size(_D)>BufferSize -> size(_D)-BufferSize;
%%                   _ -> 0
%%               end,
%%    set_data(CS#data{record_offset=FromByte},State).
%%
%%%% --------------------------------------------------------------------
%%apply_player_result(#state_scr{}=State) ->
%%    do_perform(record_offset(State)).
%%
%%%% --------------------------------------------------------------------
%%do_perform(#state_scr{}=State) ->
%%    switch(?S_PERFORMING,
%%             start_chunk_timer(
%%               start_general_timer(State))).
%%
%%%% --------------------------------------------------------------------
%%do_start_media(#state_scr{ownerpid=OwnerPid}=State) ->
%%    #data{ref=CmdRef,
%%          recorder_id=RecorderId,
%%          temp_file=File,
%%          abort_on_silence=SilenceVAD,
%%          vad_threshold=VADThreshold,
%%          preplay_file = PrePlayFile}=CS = get_data(State),
%%    Cmd = cmd_start_media,
%%    OptsMap = #{recorderid => RecorderId,
%%                file => File,
%%                type => "raw",
%%                buffer_duration => 250,
%%                codec_name => "L16/8000",
%%                vad => SilenceVAD orelse PrePlayFile /= <<>>,
%%                vad_threshold => VADThreshold},
%%    OwnerPid ! {scriptmachine, {ivr_cmd, CmdRef, {ext, ?MODULE, Cmd, self(), OptsMap}}},
%%    TimerRef = ?SCR_COMP:event_after(?TIMEOUT_TICKET, {'ticket_started_timeout',CmdRef,S=?S_STARTING}),
%%    CS1 = CS#data{state = S,
%%                  ticket_timer_ref = TimerRef,
%%                  buffer = <<>>},
%%    {ok, set_data(CS1,State)}.
%%
%%%% @private
%%%% executed in IVR-FSM pid
%%cmd_start_media({_Ref, _FromPid, _Arg}=P, #state_ivr{a=#side{state=S}}=State) when S==dialog; S==early ->
%%    do_cmd_start_media(P, State);
%%cmd_start_media({Ref, FromPid, _Arg}, #state_ivr{}=State) ->
%%    ?SCR_COMP:event(FromPid, {'ticket_started_error',Ref,{error,{invalid_operation,<<"Media could not be started, state mismatch">>}}}),
%%    {next_state, ?CURRSTATE, State}.
%%
%%%% @private
%%%% executed in IVR-FSM pid
%%do_cmd_start_media({Ref, FromPid, Arg}, #state_ivr{}=State) when is_map(Arg) ->
%%    Opts = Arg,
%%    case enable_vad(Opts, State) of
%%        {ok,State1} ->
%%            case attach_recorder(Opts, State1) of
%%                {ok,State2,MGID} ->
%%                    ?SCR_COMP:event(FromPid, {'ticket_started_ok',Ref,MGID}),
%%                    {next_state, ?CURRSTATE, State2};
%%                {error,_}=Err ->
%%                    ?SCR_COMP:event(FromPid, {'ticket_started_error',Ref,Err}),
%%                    {next_state, ?CURRSTATE, State1}
%%            end;
%%        {error,_}=Err ->
%%            ?SCR_COMP:event(FromPid, {'ticket_started_error',Ref,Err}),
%%            {next_state, ?CURRSTATE, State}
%%    end.
%%
%%%% @private
%%%% executed in IVR-FSM pid
%%enable_vad(Opts, #state_ivr{}=State) ->
%%    case maps:get(vad,Opts) of
%%        false -> {ok,State};
%%        _ ->
%%            VADThreshold = maps:get(vad_threshold,Opts),
%%            Events = {?VadEvtID, [{"vdp/vad", [{"vthres",VADThreshold},{"vad_min_duration",?RtxVadDur},{"silence_min_duration",?RtxSilenceDur}] }] },
%%            case ?IVR_MEDIA:modify_term_events(Events, State) of
%%                {ok,State1} -> {ok,State1};
%%                {error,_}=Err -> Err
%%            end end.
%%
%%%% @private
%%%% executed in IVR-FSM pid
%%attach_recorder(Opts, #state_ivr{}=State) ->
%%    case ?IVR_MEDIA:media_attach_recorder(Opts, State) of
%%        {ok,State1} ->
%%            MGID = case ?IVR_MEDIA:get_current_media_link(State1) of
%%                       {ok, #{}=M} -> maps:get(mgid,M,undefined);
%%                       _ -> undefined
%%                   end,
%%            {ok,State1,MGID};
%%        {error,_}=Err -> Err
%%    end.
%%
%%%% --------------------------------------------------------------------
%%do_setup_topology(#state_scr{ownerpid=OwnerPid}=State) ->
%%    #data{recorder_id=RecorderId} = get_data(State),
%%    Cmd = cmd_setup_topology,
%%    OptsMap = #{recorderid => RecorderId},
%%    OwnerPid ! {scriptmachine, {ivr_cmd, make_ref(), {ext, ?MODULE, Cmd, self(), OptsMap}}},
%%    {ok,State}.
%%
%%%% @private
%%%% executed in IVR-FSM pid
%%cmd_setup_topology({_Ref, _FromPid, Arg}, #state_ivr{a=#side{state=S}}=State) when S==dialog; S==early ->
%%    do_cmd_setup_topology(Arg, State);
%%cmd_setup_topology(_, #state_ivr{}=State) ->
%%    {next_state, ?CURRSTATE, State}.
%%
%%%% @private
%%%% executed in IVR-FSM pid
%%do_cmd_setup_topology(Opts, #state_ivr{acallid=CallId,media=#media_ivr{players=Ps}}=State) ->
%%    RecorderId = maps:get(recorderid,Opts),
%%    Topology0 = [{PlayerId,RecorderId,isolate} || {PlayerId,_} <- Ps],
%%    Topology1 = [{PlayerId,CallId,oneway} || {PlayerId,_} <- Ps],
%%    Topology2 = [{CallId,RecorderId,oneway}],
%%    Topology = Topology0 ++ Topology1 ++ Topology2,
%%    %Topology = [{PlayerId,CallId,isolate} || {PlayerId,_} <- Ps],
%%    case Topology of
%%        [] -> {next_state, ?CURRSTATE, State};
%%        _ ->
%%            case ?IVR_MEDIA:modify_topology(State, Topology) of
%%                {ok,State1} -> {next_state, ?CURRSTATE, State1};
%%                {error,_} -> {next_state, ?CURRSTATE, State}
%%            end end.
%%
%%%% --------------------------------------------------------------------
%%do_abort(Reason,#state_scr{}=State) ->
%%    {ok,State1} = abort_media(State),
%%    {ok,State2} = abort_internal(assign_dtmf_buffer(State1)),
%%    case Reason of
%%        {error,{'yandex_api_key',not_found}}=Err -> Err;
%%        {error,{'http_request_error',_Err}} -> ?SCR_COMP:next(<<"transferError">>,State2);
%%        {error,'http_timeout'} -> ?SCR_COMP:next(<<"transferTimeout">>,State2);
%%        {error,{'http_response',_ReplyInfo}} -> ?SCR_COMP:next(<<"transferError">>,State2);
%%        {error,{'ticket_started_error',Err}} -> Err;
%%        {error,'ticket_started_timeout'} -> {error,{internal_error,<<"IVR-FSM response timeout">>}};
%%        {error,{'ticket_stopped_error',Err}} -> Err;
%%        {error,'ticket_stopped_timeout'} -> {error,{internal_error,<<"IVR-FSM response timeout">>}};
%%        {error,'mg_file_info'} -> ?SCR_COMP:next(<<"transferError">>,State2);
%%        {error,'mg_file_size'} -> ?SCR_COMP:next(<<"transferError">>,State2);
%%        {error,'fsm_error'} -> ?SCR_COMP:next(<<"transferError">>,State2);
%%        _ -> ?SCR_COMP:next(<<"transferError">>,State2)
%%    end.
%%
%%%% @private
%%abort_media(#state_scr{ownerpid=OwnerPid}=State) ->
%%    case get_data(State) of
%%        #data{recorder_id=undefined} -> {ok,State};
%%        #data{recorder_id=RecorderId,abort_on_silence=VAD,ref=CmdRef}=CS ->
%%            Cmd = cmd_abort_media,
%%            OptsMap = #{recorderid => RecorderId,
%%                        vad => VAD},
%%            OwnerPid ! {scriptmachine, {ivr_cmd, CmdRef, {ext, ?MODULE, Cmd, self(), OptsMap}}},
%%            {ok,set_data(CS#data{recorder_id=undefined},State)}
%%    end.
%%
%%%% @private
%%%% executed in IVR-FSM pid
%%cmd_abort_media({_Ref, _FromPid, Arg}, #state_ivr{}=State) ->
%%    Opts = Arg,
%%    State2 = case detach_recorder(Opts,State) of
%%                 {error,_} -> State;
%%                 {ok,State1} -> State1
%%             end,
%%    State4 = case disable_vad(Opts,State2) of
%%                 {error,_} -> State2;
%%                 {ok,State3} -> State3
%%             end,
%%    {next_state, ?CURRSTATE, State4}.
%%
%%%% @private
%%abort_internal(State) ->
%%    {ok,State1} = stop_http_timer(stop_ticket_timer(stop_performing_timers(State))),
%%    {ok,State2} = close_connection(State1),
%%    drop_file(terminating,State2).
%%
%%%% @private
%%close_connection(State) ->
%%    #data{http_request_id=RequestId} = get_data(State),
%%    case RequestId of
%%        undefined -> {ok,State};
%%        _ ->
%%            httpc:cancel_request(RequestId, ?PROFILE),
%%            {ok,State}
%%    end.
%%
%%%% --------------------------------------------------------------------
%%do_stop_media(#state_scr{ownerpid=OwnerPid}=State) ->
%%    #data{ref=CmdRef,
%%          recorder_id=RecorderId,
%%          abort_on_silence=SilenceVAD,
%%          preplay_file=PrePlayFile}=CS=get_data(State),
%%    Cmd = cmd_stop_media,
%%    OptsMap = #{recorderid => RecorderId,
%%                vad => SilenceVAD orelse PrePlayFile /= <<>>},
%%    OwnerPid ! {scriptmachine, {ivr_cmd, CmdRef, {ext, ?MODULE, Cmd, self(), OptsMap}}},
%%    TimerRef = ?SCR_COMP:event_after(?TIMEOUT_TICKET, {'ticket_stopped_timeout',CmdRef,?S_STOPPING}),
%%    CS1 = CS#data{ticket_timer_ref=TimerRef,
%%                  recorder_id=undefined},
%%    {ok, set_data(CS1,State)}.
%%
%%%% @private
%%%% executed in IVR-FSM pid
%%cmd_stop_media({Ref, FromPid, Arg}, #state_ivr{}=State) ->
%%    Opts = Arg,
%%    StateX = case detach_recorder(Opts,State) of
%%                 {error,_}=Err -> ?SCR_COMP:event(FromPid, {'ticket_stopped_error',Ref,Err}), State;
%%                 {ok,State1} ->
%%                     case disable_vad(Opts,State1) of
%%                         {error,_}=Err -> ?SCR_COMP:event(FromPid, {'ticket_stopped_error',Ref,Err}), State1;
%%                         {ok,State2} -> ?SCR_COMP:event(FromPid, {'ticket_stopped_ok',Ref,?S_STOPPING}), State2
%%                     end end,
%%    {next_state, ?CURRSTATE, StateX}.
%%
%%%% @private
%%%% executed in IVR-FSM pid
%%disable_vad(Opts, #state_ivr{}=State) ->
%%    case maps:get(vad,Opts) of
%%        false -> {ok,State};
%%        _ ->
%%            Events = {?VadEvtID, [{"vdp/empty", [] }] }, % {0, []},
%%            case ?IVR_MEDIA:modify_term_events(Events, State) of
%%                {ok,State1} -> {ok,State1};
%%                {error,_}=Err -> Err
%%            end end.
%%
%%%% @private
%%%% executed in IVR-FSM pid
%%detach_recorder(Opts,#state_ivr{}=State) ->
%%    ?IVR_MEDIA:media_detach_recorder(Opts, State).
%%
%%%% --------------------------------------------------------------------
%%do_apply_vad({'vad_threshold', _CtxTermId, Event}, #state_scr{}=State) ->
%%    case maps:get(parname,Event,undefined) of
%%        "state" ->
%%            #data{vad_state=VAD,
%%                  abort_on_silence=SilenceVAD,
%%                  silence_timer_ref=TimerRef,
%%                  silence_timeout=Timeout,
%%                  ref=Ref}=CS = get_data(State),
%%            case maps:get(parvalue,Event) of
%%                "up" when VAD/='up' ->
%%                    ?EU:cancel_timer(TimerRef),
%%                    {ok, set_data(CS#data{silence_timer_ref=undefined,
%%                                          vad_state=up}, State)};
%%                "down" when VAD/='down' andalso SilenceVAD ->
%%                    {ok, set_data(CS#data{silence_timer_ref=?SCR_COMP:event_after(Timeout,self(),{'silence_timeout',Ref,?S_PERFORMING}),
%%                                          vad_state=down}, State)};
%%                _ -> {ok,State}
%%            end;
%%        _ -> {ok, State}
%%    end.
%%
%%%% --------------------------------------------------------------------
%%do_apply_dtmf(Dtmf, State) ->
%%    case get_data(State) of
%%        #data{check_dtmf=true, buffer=Buffer}=CS ->
%%            Dtmf1 = ?U:normalize_dtmf(Dtmf),
%%            CS1 = CS#data{buffer= <<Buffer/bitstring,Dtmf1/bitstring>>},
%%            check_interrupt(set_data(CS1, State));
%%        _ -> {ok,State}
%%    end.
%%
%%%% @private
%%%% check if complete by interrupt
%%check_interrupt(#state_scr{}=State) ->
%%    #data{buffer=Buffer,
%%          interrupts=Interrupts}=CS = get_data(State),
%%    Length = size(Buffer),
%%    F = fun(Interrupt, false) ->
%%                case binary:matches(Buffer, Interrupt) of
%%                    [] -> false;
%%                    M ->
%%                        {Pos,Len} = lists:last(M),
%%                        case Pos+Len of
%%                            Length ->
%%                                case CS#data.clear_interrupt of
%%                                    false -> {true,Buffer};
%%                                    true ->
%%                                        % clear interrupt symbols
%%                                        Bits = Pos*8,
%%                                        <<Clear:Bits/bitstring,_/bitstring>> = Buffer,
%%                                        {true,Clear}
%%                                end;
%%                            _ -> false
%%                        end
%%                end;
%%           (_,Acc) -> Acc
%%        end,
%%    case lists:foldl(F, false, Interrupts) of
%%        {true,Res} -> stop_by_dtmf(interrupt, set_data(CS#data{buffer=Res},State));
%%        false -> check_length(State)
%%    end.
%%
%%%% @private
%%%% check if complete by max length
%%check_length(#state_scr{}=State) ->
%%    #data{buffer=Buffer,
%%          max_count=MaxSymbolCount} = get_data(State),
%%    Length = size(Buffer),
%%    case MaxSymbolCount>0 andalso Length>=MaxSymbolCount of
%%        true -> stop_by_dtmf(length,State);
%%        false -> {ok,State}
%%    end.
%%
%%%% @private
%%%% stop record by dtmf
%%stop_by_dtmf(_Mode, #state_scr{}=State) ->
%%    switch(?S_STOPPING,stop_media(State)).
%%
%%%% --------------------------------------------------------------------
%%do_maybe_start_http(#state_scr{}=State) ->
%%    #data{state=S,
%%          http_request_id=RequestId,
%%          mgfileinfo=MGFileInfo} = get_data(State),
%%    case RequestId of
%%        undefined ->
%%            case read_file_endpart(MGFileInfo,0) of
%%                {ok,D} when size(D)>0 -> start_http(State);
%%                _ when S==?S_PERFORMING -> start_chunk_timer(State);
%%                _ -> do_abort({error,'mg_file_size'},State)
%%            end;
%%        _ -> {ok,State}
%%    end.
%%
%%%% @private
%%start_http(#state_scr{}=State) ->
%%    http_init(),
%%    case catch http_request(State) of
%%        {ok,RequestId} ->
%%            #data{}=CS = get_data(State),
%%            {ok,set_data(CS#data{http_request_id=RequestId},State)};
%%        {error,_}=Err -> do_abort({error,{'http_request_error',Err}},State);
%%        {'EXIT',Err} -> do_abort({error,{'http_request_error',Err}},State)
%%    end.
%%
%%%% @private
%%http_init() ->
%%    inets:start(),
%%    inets:start(httpc, [{profile,?PROFILE}]),
%%    Options = [{max_sessions,10000},
%%               {keep_alive_timeout,5000}],
%%    httpc:set_options(Options,?PROFILE).
%%
%%%% @private
%%-spec http_request(#state_scr{}) -> {ok,RequestId::reference()} | {error,Reason::term()}.
%%http_request(#state_scr{}=State) ->
%%    #data{state=S,
%%          pid=Pid,
%%          ref=Ref,
%%          yandex_uri=Uri,
%%          yandex_api_key=ApiKey,
%%          yandex_folder_id=FolderId,
%%          mgfileinfo=FileInfo,
%%          record_offset=FromByte,
%%          response_timeout=ResponseTimeout,
%%          lang=Lang,
%%          topic=Topic,
%%          profanity_filter=ProfanityFilter} = get_data(State),
%%    Params0 = case FolderId of
%%                  <<>> -> [];
%%                  _ -> ?EU:str("&folderId=~s",[FolderId])
%%              end,
%%    URI = ?EU:str("~s?lang=~s&topic=~s&profanityFilter=~s&format=lpcm&sampleRateHertz=~p~s", [Uri,Lang,Topic,ProfanityFilter,?SampleRateHz,Params0]),
%%    ContentType = ?EU:str("audio/x-pcm;bit=16;rate=~p",[?SampleRateHz]),
%%    Headers = [{"Host",re:replace(Uri,"^[^/]+//([^/]+)/.*", "\\1", [{return,list}])},
%%               {"Authorization",?EU:str("Api-Key ~s", [ApiKey])},
%%               {"Transfer-Encoding","chunked"},
%%               {"Content-Type",ContentType},
%%               {"Connection","close"}],
%%    HttpOpts = [{timeout, ResponseTimeout+20000}], % real timeout is provided by general_timer
%%    Opts = [{sync,false},
%%            {headers_as_is,true},
%%            {receiver,fun(ReplyInfo) -> ?SCR_COMP:event(Pid,{'http_response',Ref,ReplyInfo}) end},
%%            {full_result,true}],
%%    case S of
%%        ?S_PERFORMING ->
%%            Acc = {{'initial',FromByte},FileInfo,Pid,Ref},
%%            Body = {chunkify, fun fun_chunk/1, Acc},
%%            httpc:request(post, {URI, Headers, ContentType, Body}, HttpOpts, Opts, ?PROFILE);
%%        _ ->
%%            case read_file_endpart(FileInfo,FromByte) of
%%                {ok,IOData} when size(IOData)>0 -> httpc:request(post, {URI, Headers, ContentType, IOData}, HttpOpts, Opts, ?PROFILE);
%%                _ -> {error,'mg_file_info'}
%%            end end.
%%
%%%% @private
%%fun_chunk({{'initial',FromByte},FileInfo,Pid,Ref}=_Acc) ->
%%    erlang:monitor(process,Pid),
%%    ?SCR_COMP:event(Pid,{'chunk_pid_reg',Ref,self()}),
%%    self() ! {next,Ref},
%%    fun_chunk_next({FromByte,FileInfo,Pid,Ref});
%%fun_chunk({_From,_FileInfo,_Pid,Ref}=Acc) ->
%%    receive
%%        {next,R} when R==Ref -> fun_chunk_next(Acc);
%%        {eof,R} when R==Ref -> eof;
%%        {'DOWN',_,process,_,_} -> eof
%%    after 500 -> fun_chunk_next(Acc)
%%    end.
%%fun_chunk_next({From,FileInfo,Pid,Ref}=Acc) ->
%%    case read_file_endpart(FileInfo,From) of
%%        {ok,IOData} when size(IOData)>0 -> {ok, IOData, {From+size(IOData),FileInfo,Pid,Ref}};
%%        false -> eof;
%%        _ -> fun_chunk(Acc)
%%    end.
%%
%%%% @private
%%read_file_endpart(FileInfo,FromByte) -> read_file_endpart(FileInfo,FromByte,1).
%%read_file_endpart(FileInfo,FromByte,Attempts) ->
%%    [MGSite,MGNode,MGFilePath] = ?EU:maps_get([site,node,srcpath],FileInfo),
%%    case ?ENVCROSS:call_node({MGSite,MGNode}, {?EU, file_read_endpart, [MGFilePath,FromByte]}, false) of
%%        {ok,<<>>} when Attempts>0 -> read_file_endpart(FileInfo,FromByte,Attempts-1);
%%        T -> T
%%    end.
%%
%%%% --------------------------------------------------------------------
%%do_on_stopped_media(#state_scr{}=State) ->
%%    case maybe_start_http(stop_ticket_timer(State)) of
%%        {ok,State1} ->
%%            {ok,State2} = switch(?S_RECOGNIZING,finalize_file(finalize_chunking(State1))),
%%            check_apply_stored_http_response(State2);
%%        {next,_,State1}=Next ->
%%            % fallback when interrupted by dtmf, but recording still absent - should return by transfer and save dtmf (it was done in abort/2)
%%            #data{buffer=Buffer} = get_data(State1),
%%            case Buffer of
%%                <<>> -> Next;
%%                _ -> ?SCR_COMP:next(<<"transfer">>,State1)
%%            end end.
%%%%
%%finalize_chunking(#state_scr{}=State) ->
%%    case get_data(State) of
%%        #data{ref=Ref,chunk_pid=Pid} when is_pid(Pid) -> Pid ! {eof,Ref};
%%        _ -> ok
%%    end,
%%    {ok,State}.
%%%%
%%check_apply_stored_http_response(#state_scr{}=State) ->
%%    #data{stored_http_response=ReplyInfo,response_timeout=Timeout,ref=CmdRef}=Data = get_data(State),
%%    case ReplyInfo of
%%        undefined when Timeout>0 ->
%%            TimerRef = ?SCR_COMP:event_after(Timeout, {'http_timeout',CmdRef,?S_RECOGNIZING}),
%%            {ok,set_data(Data#data{http_timer_ref=TimerRef}, State)};
%%        undefined -> {ok,State};
%%        _ -> parse_response(ReplyInfo,State)
%%    end.
%%
%%%% --------------------------------------------------------------------
%%do_finalize_file(#state_scr{}=State) ->
%%    case get_data(State) of
%%        #data{save_record=false} -> drop_file(normal,State);
%%        #data{} -> move_file(State)
%%    end.
%%
%%%% @private
%%drop_file(Mode,#state_scr{}=State) ->
%%    case get_data(State) of
%%        #data{mgfileinfo=FileInfo}=CS when FileInfo/=undefined ->
%%            [MGSite,MGNode,MGFilePath] = ?EU:maps_get([site,node,srcpath],FileInfo),
%%            F = fun() -> ?ENVCROSS:call_node({MGSite,MGNode}, {?Rfile, delete, [MGFilePath]}, false) end,
%%            case Mode of
%%                normal -> F();
%%                terminating -> spawn(fun() -> timer:sleep(5000), F end)
%%            end,
%%            {ok,set_data(CS#data{mgfileinfo=undefined},State)};
%%        _R -> {ok,State}
%%    end.
%%
%%%% @private
%%move_file(#state_scr{}=State) ->
%%    case get_data(State) of
%%        #data{pid=Pid,mgfileinfo=FileInfo}=CS when FileInfo/=undefined ->
%%            [MGSite,MGNode,MGFilePath] = ?EU:maps_get([site,node,srcpath],FileInfo),
%%            TempPath = ?EU:to_list(?SCR_FILE:get_script_temp_directory(Pid)),
%%            DestPath = filename:join([TempPath, lists:last(string:tokens(MGFilePath,"/"))]),
%%            ?Rfilelib:ensure_dir(DestPath),
%%            State1 = case ?ENVCOPY:copy_file({MGSite,MGNode}, MGFilePath, DestPath) of
%%                         {error,_}=Err ->
%%                             ?LOG("ASR_yandex. Error copying ~500tp", [Err]),
%%                             State;
%%                         ok ->
%%                             case ?Rfilelib:is_file(DestPath) of
%%                                 false -> false;
%%                                 true ->
%%                                     {ok,WavBody} = file:read_file(DestPath),
%%                                     WavHeader = ?EU:get_wav_header(false,1,16,?SampleRateHz,size(WavBody) div 2),
%%                                     file:delete(DestPath),
%%                                     DestPath1 = filename:join(filename:dirname(DestPath),
%%                                                               string:join(lists:reverse(["wav"|lists:nthtail(1,lists:reverse(string:tokens(filename:basename(DestPath),".")))]),".")),
%%                                     file:write_file(DestPath1,<<WavHeader/binary,WavBody/binary>>),
%%                                     assign_field(<<"varRecordPath">>, binary:replace(?EU:to_binary(DestPath1),?EU:to_binary(TempPath),<<":TEMP">>), State)
%%                             end end,
%%            ?ENVCROSS:call_node({MGSite,MGNode}, {?Rfile, delete, [MGFilePath]}, false),
%%            {ok,set_data(CS#data{mgfileinfo=undefined},State1)};
%%        _ -> {ok,State}
%%    end.
%%
%%%% --------------------------------------------------------------------
%%do_parse_response({_Ref,{error,timeout}}=_ReplyInfo,#state_scr{}=State) ->
%%    State1 = assign_field(<<"varHttpCode">>, 408, State),
%%    ?SCR_COMP:next(<<"transferTimeout">>, State1);
%%%%
%%do_parse_response({_Ref,{error,_}}=_ReplyInfo,#state_scr{}=State) ->
%%    State1 = assign_field(<<"varHttpCode">>, 500, State),
%%    ?SCR_COMP:next(<<"transferError">>, State1);
%%%%
%%do_parse_response({_Ref,{{_Proto,Code,_Status},_Headers,Body}}=_ReplyInfo, #state_scr{}=State) ->
%%    State1 = assign_field(<<"varHttpCode">>, Code, State),
%%    State2 = assign_field(<<"varHttpBody">>, ?EU:to_binary(Body), State1),
%%    State3 = assign_dtmf_buffer(State2),
%%    case Code of
%%        _ when Code>=200, Code<300 ->
%%            try Map = jsx:decode(Body,[return_maps]),
%%                Result = maps:get(<<"result">>,Map),
%%                State4 = assign_field(<<"varText">>, ?EU:to_binary(Result), State3),
%%                ?SCR_COMP:next(<<"transfer">>, State4)
%%            catch _:_ -> ?SCR_COMP:next(<<"transferError">>, State3) end;
%%        _ -> ?SCR_COMP:next(<<"transferError">>, State3)
%%    end.
%%
%%%% ====================================================================
%%%% Internal functions
%%%% ====================================================================
%%
%%%% -------------------------------------
%%c(#state_scr{}=State) -> State;
%%c({ok,#state_scr{}=State}) -> c(State).
%%
%%%% -------------------------------------
%%get_data(#state_scr{active_component=#component{state=Data}}) -> Data.
%%set_data(#data{}=Data,#state_scr{active_component=Comp}=State) -> State#state_scr{active_component=Comp#component{state=Data}}.
%%
%%%% -------------------------------------
%%assign_dtmf_buffer(#state_scr{active_component=#component{data=CData}}=State) ->
%%    #data{buffer=Buffer} = get_data(State),
%%    Var = ?SCR_COMP:get(<<"dtmfBuffer">>, CData, <<>>),
%%    Prev = prev_dtmf_buffer(Var, State),
%%    assign(Var, <<Prev/bitstring, Buffer/bitstring>>, State).
%%
%%%% @private
%%prev_dtmf_buffer(Var, #state_scr{}=State) ->
%%    #data{}=CS = get_data(State),
%%    case CS#data.clear_buffer orelse ?SCR_VAR:get_value(Var, undefined, State) of
%%        true -> <<>>;
%%        undefined -> <<>>;
%%        V when is_binary(V) -> V;
%%        V when is_integer(V) -> ?EU:to_binary(V);
%%        {S,_Enc} when is_binary(S) -> S;
%%        _ -> <<>>
%%    end.
%%
%%%% -------------------------------------
%%%% assign value to variable
%%%% -------------------------------------
%%assign_field(Field, Value, #state_scr{active_component=#component{data=Data}}=State) when is_binary(Field) ->
%%    assign(?SCR_COMP:get(Field, Data, <<>>), Value, State).
%%assign(Var, Value, State) ->
%%    case ?SCR_VAR:set_value(Var, Value, State) of
%%        {ok, State1} -> State1;
%%        _ -> State
%%    end.