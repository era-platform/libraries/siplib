%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 14.04.2016
%%% @doc
%%% @todo @ivr

-module(r_sip_ivr_script_component_bye).
-author('Peter Bukashin <tbotc@yandex.ru>').

%%-compile([export_all,nowarn_export_all]).
%%-export([init/1,
%%         handle_event/2,
%%         metadata/1]).
%%
%%%% ====================================================================
%%%% Defines
%%%% ====================================================================
%%
%%%-include("../include/r_script.hrl").
%%-include("../../era_script/include/r_script.hrl").
%%
%%-include("../include/r_sip_ivr.hrl").
%%-include("../include/r_sip_nk.hrl").
%%
%%-define(U, r_sip_utils).
%%
%%-define(CURRSTATE, 'active').
%%
%%-record(bye, {
%%    ref,
%%    timer_ref
%%  }).
%%
%%%% ====================================================================
%%%% Callback functions
%%%% ====================================================================
%%
%%%% returns descriptions of used properties
%%metadata(_ScriptType) -> [].
%%
%%%% ---------------------------------------------------------------------
%%init(#state_scr{ownerpid=undefined}=State) ->
%%    Err = {error,{internal_error,<<"Invalid owner">>}},
%%    fin(Err, State);
%%init(#state_scr{ownerpid=OwnerPid, active_component=Comp}=State) ->
%%    % init
%%    CmdRef = make_ref(),
%%    OwnerPid ! {scriptmachine, {ivr_cmd, CmdRef, {ext, ?MODULE, send_bye, self(), #{}}}},
%%    TimerRef = ?SCR_COMP:event_after(10000, {'timeout',CmdRef}),
%%    {ok, State#state_scr{active_component=Comp#component{state=#bye{ref=CmdRef, timer_ref=TimerRef}}}}.
%%
%%%% ---------------------------------------
%%handle_event({timeout,Ref}, #state_scr{active_component=#component{state=#bye{ref=Ref}}}=State) ->
%%    Err = {error,{timeout,<<"Ticket timeout">>}},
%%    fin(Err, State);
%%
%%handle_event({ticket,Ref,{error,_}=Err}, #state_scr{active_component=#component{state=#bye{ref=Ref, timer_ref=TimerRef}}}=State) ->
%%    erlang:cancel_timer(TimerRef),
%%    fin(Err, State);
%%
%%handle_event({ticket,Ref,ok}, #state_scr{active_component=#component{state=#bye{ref=Ref, timer_ref=TimerRef}}}=State) ->
%%    erlang:cancel_timer(TimerRef),
%%    fin(ok,State);
%%
%%handle_event(_Ev, State) ->
%%    {ok, State}.
%%
%%%% ====================================================================
%%%% Functional iface of SIP IVR dlg (linked to its fsm)
%%%% ====================================================================
%%
%%send_bye({Ref, FromPid, _OptsMap}, #state_ivr{a=#side{state=S}=Side}=State) when S==dialog ->
%%    {ok,State1} = ?ACTIVE_UTILS:do_stop_bye(State),
%%    State2 = State1#state_ivr{a=Side#side{state=bye,
%%                                          finaltime=os:timestamp()}},
%%    ?SCR_COMP:event(FromPid, {'ticket',Ref,ok}),
%%    ?IVR_UTILS:return_stopping(?IVR_UTILS:do_finalize_ivr(State2));
%%send_bye(P, #state_ivr{a=#side{state=S}}=State) when S==incoming; S==ringing; S==early ->
%%    SipCode = 486,
%%    NewSideState = declined,
%%    R = {SipCode, [{reason, {sip, SipCode, <<"BYE fallback">>}}]},
%%    do_answer_nosdp(P,{NewSideState,R},State);
%%send_bye({Ref, FromPid, _OptsMap}, State) ->
%%    ?SCR_COMP:event(FromPid, {'ticket',Ref,{error,{invalid_operation,<<"Call could not be stopped, state mismatch">>}}}),
%%    {next_state, ?CURRSTATE, State}.
%%
%%%% --------------------
%%%% fallback answer 4xx
%%%% --------------------
%%%% @private
%%%% answer no sdp
%%do_answer_nosdp({Ref, FromPid, _OptsMap}, {SideState,SipReply}, #state_ivr{a=Side}=State) ->
%%    #side{rhandle=RHandle, responses=Resp}=Side,
%%    {Code,Opts}=SipReply,
%%    {ok,SipOpts,_Meta} = make_answer_opts(Code,Opts,State),
%%    State1 = ?IVR_UTILS:send_response(RHandle, {Code,SipOpts}, State),
%%    State2 = State1#state_ivr{a=Side#side{state=SideState, responses=[SipReply|Resp]}},
%%    ?SCR_COMP:event(FromPid, {'ticket',Ref,ok}),
%%    {next_state, ?CURRSTATE, State2}.
%%
%%%% @private
%%%% common answer opts (reason, custom headers (resend in b2bua), ...)
%%make_answer_opts(_Code,ROpts,_State) ->
%%    Meta = [],
%%    AOpts = [],
%%    AOpts1 = case lists:keyfind('reason_phrase',1,ROpts) of
%%                 {_,RP} when RP /= <<>> -> [{reason_phrase,RP}|AOpts];
%%                 _ -> AOpts
%%             end,
%%    AOpts2 = case lists:keyfind('reason',1,ROpts) of
%%                 {_,R} when R /= <<>> -> [{reason,R}|AOpts1];
%%                 _ -> AOpts1
%%             end,
%%    {ok,AOpts2,Meta}.
%%
%%%% ====================================================================
%%%% Internal functions
%%%% ====================================================================
%%
%%%% finalize
%%fin({error,_}=Err, _State) -> Err;
%%fin(ok, State) -> {stop, State}. % stop main script body, normal type
