%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 20.04.2016
%%% @doc
%%% @todo @ivr

-module(r_sip_ivr_script_component_dtmf).
-author('Peter Bukashin <tbotc@yandex.ru>').

%%-compile([export_all,nowarn_export_all]).
%%-export([init/1,
%%         handle_event/2,
%%         metadata/1]).
%%
%%%% ====================================================================
%%%% Defines
%%%% ====================================================================
%%
%%%-include("../include/r_script.hrl").
%%-include("../../era_script/include/r_script.hrl").
%%
%%-include("../include/r_sip_ivr.hrl").
%%-include("../include/r_sip_nk.hrl").
%%
%%-define(U, r_sip_utils).
%%
%%-define(CURRSTATE, 'active').
%%
%%-record(dtmfstate, {
%%    general_ref,
%%    general_timerref,
%%    symbol_ref,
%%    symbol_timerref,
%%    buffer = <<>>,
%%    %
%%    maxcount = 0,
%%    interrupts = [],
%%    clear_interrupt = true,
%%    clear_buffer = true,
%%    symbol_timeout = 0
%%  }).
%%
%%%% ====================================================================
%%%% Callback functions (script)
%%%% ====================================================================
%%
%%% returns descriptions of used properties
%%metadata(_ScriptType) ->
%%    [{<<"timeoutSec">>, #{type => <<"argument">>,
%%                          default => 20 }},
%%     {<<"symbolTimeoutSec">>, #{type => <<"argument">>,
%%                                default => 0 }},
%%     {<<"dtmfBuffer">>, #{type => <<"variable">>}},
%%     {<<"clearDtmfBuffer">>, #{type => <<"list">>,
%%                               items => [{0,<<"no">>},
%%                                           {1,<<"yes">>}],
%%                               default => 1,
%%                               filter => <<"(dtmfBuffer!=null)">> }},
%%     {<<"maxSymbolCount">>, #{type => <<"argument">> }},
%%     {<<"interruptSymbols">>, #{type => <<"string">>,
%%                                default => null }},
%%     {<<"clearInterrupt">>, #{type => <<"list">>,
%%                              items => [{0,<<"no">>},
%%                                          {1,<<"yes">>}],
%%                              filter => <<"(interruptSymbols!=null) && (interruptSymbols!=\"\")">>,
%%                              default => 1 }},
%%     {<<"transfer">>, #{type => <<"transfer">> }},
%%     {<<"transferTimeout">>, #{type => <<"transfer">>,
%%                               title => <<"timeout">> }} ].
%%
%%% ---------------------------------------------------------------------
%%init(#state_scr{ownerpid=undefined}=State) ->
%%    ?SCR_COMP:next([<<"transferError">>, <<"transfer">>], State);
%%init(#state_scr{active_component=#component{data=CompData}=Comp}=State) ->
%%    % properties
%%    Timeout = ?SCR_ARG:get_int(<<"timeoutSec">>, CompData, 20, State) * 1000,
%%    CS = #dtmfstate{maxcount = ?SCR_ARG:get_int(<<"maxSymbolCount">>, CompData, 0, State),
%%                    interrupts = ?IVR_SCRIPT_UTILS:parse_interrupts(<<"interruptSymbols">>, CompData),
%%                    clear_buffer = ?EU:to_bool(?SCR_COMP:get(<<"clearDtmfBuffer">>, CompData, 1)),
%%                    clear_interrupt = ?EU:to_bool(?SCR_COMP:get(<<"clearInterrupt">>, CompData, 1)),
%%                    symbol_timeout = max(0, min(60, ?SCR_ARG:get_int(<<"symbolTimeoutSec">>, CompData, 0, State))) * 1000},
%%    % @debug
%%    % CS = CS_#dtmfstate{maxcount=5,interrupts=[<<"1">>,<<"2">>,<<"3">>]},
%%    % init state
%%    GenRef = make_ref(),
%%    GenTimerRef = ?SCR_COMP:event_after(Timeout, {'general_timeout',GenRef}),
%%    {ok, State#state_scr{active_component=Comp#component{state=CS#dtmfstate{general_ref=GenRef,
%%                                                                            general_timerref=GenTimerRef,
%%                                                                            buffer= <<>>}}}}.
%%
%%% ---------------------------------------
%%
%%handle_event({'general_timeout',Ref}, #state_scr{active_component=#component{state=#dtmfstate{general_ref=Ref}}}=State) ->
%%    fin(general_timeout,State);
%%
%%%
%%handle_event({'symbol_timeout',Ref}, #state_scr{active_component=#component{state=#dtmfstate{symbol_ref=Ref}}}=State) ->
%%    fin(symbol_timeout,State);
%%
%%% dtmf from mg (rfc2833, inband)
%%handle_event({mg_dtmf,Dtmf}, State) ->
%%    dtmf(Dtmf, State);
%%
%%% dtmf from sip (info)
%%handle_event({sip_info,Req}, State) ->
%%    ?U:get_dtmf_from_sipinfo(Req, fun(Dtmf) -> dtmf(Dtmf,State) end, fun() -> {ok,State} end);
%%
%%% other
%%handle_event(_Ev, State) -> {ok, State}.
%%
%%% ---------------------------------------
%%
%%terminate(State) -> {ok, stop_timers(State)}.
%%
%%%% ====================================================================
%%%% Functional iface of SIP IVR dlg (linked to its fsm)
%%%% ====================================================================
%%
%%%% ====================================================================
%%%% Internal functions
%%%% ====================================================================
%%
%%%% --------------------------
%%%% dtmf
%%%% --------------------------
%%
%%% normalize dtmf symbols
%%normalize(X) -> ?U:normalize_dtmf(X).
%%
%%% dtmf received
%%dtmf(Dtmf, #state_scr{active_component=#component{state=CS}=Comp}=State) ->
%%    %?OUT("Dtmf: ~500tp", [Dtmf]),
%%    Dtmf1 = normalize(Dtmf),
%%    #dtmfstate{buffer=Buffer}=CS,
%%    CS1 = case CS of
%%              #dtmfstate{symbol_timeout=T} when T>0 ->
%%                   SymbolRef = make_ref(),
%%                   SymbolTimerRef = ?SCR_COMP:event_after(T, {'symbol_timeout',SymbolRef}),
%%                   CS#dtmfstate{symbol_timerref=SymbolTimerRef,
%%                                symbol_ref=SymbolRef};
%%              _ -> CS
%%          end,
%%    State1 = State#state_scr{active_component=Comp#component{state=CS1#dtmfstate{buffer= <<Buffer/bitstring,Dtmf1/bitstring>>}}},
%%    check_fin(State1).
%%
%%% check if complete
%%check_fin(State) ->
%%    check_interrupt(State).
%%
%%% check if complete by interrupt
%%check_interrupt(#state_scr{active_component=#component{state=CS}=Comp}=State) ->
%%    #dtmfstate{buffer=Buffer,
%%               interrupts=Interrupts}=CS,
%%    Length = size(Buffer),
%%    F = fun(Interrupt, false) ->
%%                case binary:matches(Buffer, Interrupt) of
%%                    [] -> false;
%%                    M ->
%%                        {Pos,Len} = lists:last(M),
%%                        case Pos+Len of
%%                            Length ->
%%                                case CS#dtmfstate.clear_interrupt of
%%                                    false -> {true,Buffer};
%%                                    true ->
%%                                        % clear interrupt symbols
%%                                        Bits = Pos*8,
%%                                        <<Clear:Bits/bitstring,_/bitstring>> = Buffer,
%%                                        {true,Clear}
%%                                end;
%%                            _ -> false
%%                        end
%%                end;
%%            (_,Acc) -> Acc
%%         end,
%%    case lists:foldl(F, false, Interrupts) of
%%        {true,Res} -> fin(interrupt, State#state_scr{active_component=Comp#component{state=CS#dtmfstate{buffer=Res}}});
%%        false -> check_length(State)
%%    end.
%%
%%% check if complete by max length
%%check_length(#state_scr{active_component=#component{state=CS}}=State) ->
%%    #dtmfstate{buffer=Buffer,
%%               maxcount=MaxSymbolCount}=CS,
%%    Length = size(Buffer),
%%    case MaxSymbolCount>0 andalso Length>=MaxSymbolCount of
%%        true -> fin(length,State);
%%        false -> {ok,State}
%%    end.
%%
%%%% --------------------------
%%%% finalize
%%%% --------------------------
%%
%%% finalize work of component
%%fin(Mode, #state_scr{}=State) ->
%%    fin_1(Mode,stop_timers(State)).
%%fin_1(Mode, #state_scr{active_component=#component{data=Data,state=CS}}=State) ->
%%    #dtmfstate{buffer=Buffer}=CS,
%%    Var = ?SCR_COMP:get(<<"dtmfBuffer">>, Data, <<>>),
%%    Prev = prev_dtmf_buffer(Var, State),
%%    State1 = assign(Var, <<Prev/bitstring, Buffer/bitstring>>, State),
%%    fin_2(Mode, State1).
%%fin_2(general_timeout, State) -> ?SCR_COMP:next([<<"transferTimeout">>,<<"transfer">>], State);
%%fin_2(symbol_timeout, State) -> ?SCR_COMP:next([<<"transferTimeout">>,<<"transfer">>], State);
%%fin_2(_, State) -> ?SCR_COMP:next(<<"transfer">>,State).
%%
%%%
%%stop_timers(#state_scr{active_component=#component{state=#dtmfstate{general_timerref=TimerRef1, symbol_timerref=TimerRef2}=CS}=Comp}=State) ->
%%    ?EU:cancel_timer(TimerRef1),
%%    ?EU:cancel_timer(TimerRef2),
%%    State#state_scr{active_component=Comp#component{state=CS#dtmfstate{general_timerref=undefined,
%%                                                                       symbol_timerref=undefined}}}.
%%
%%% return previous value of variable
%%prev_dtmf_buffer(Var, #state_scr{active_component=#component{state=CS}}=State) ->
%%    case CS#dtmfstate.clear_buffer orelse ?SCR_VAR:get_value(Var, undefined, State) of
%%        true -> <<>>;
%%        undefined -> <<>>;
%%        V when is_binary(V) -> V;
%%        V when is_integer(V) -> ?EU:to_binary(V);
%%        {S,_Enc} when is_binary(S) -> S;
%%        _ -> <<>>
%%    end.
%%
%%% assign value to variable
%%assign(Var, Value, State) ->
%%      case ?SCR_VAR:set_value(Var, Value, State) of
%%        {ok, State1} -> State1;
%%        _ -> State
%%    end.
