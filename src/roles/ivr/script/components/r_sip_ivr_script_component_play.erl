%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 18.04.2016
%%% @doc
%%% @todo @ivr

-module(r_sip_ivr_script_component_play).
-author('Peter Bukashin <tbotc@yandex.ru>').

%%-compile([export_all,nowarn_export_all]).
%%-export([init/1,
%%         handle_event/2,
%%         metadata/1]).
%%
%%%% ====================================================================
%%%% Defines
%%%% ====================================================================
%%
%%%-include("../include/r_script.hrl").
%%-include("../../era_script/include/r_script.hrl").
%%
%%-include("../include/r_sip_ivr.hrl").
%%-include("../include/r_sip_nk.hrl").
%%
%%-define(U, r_sip_utils).
%%-define(CFG, r_sip_config).
%%
%%-define(CURRSTATE, 'active').
%%
%%-define(TIMEOUT, 10000).
%%
%%-record(play, {
%%               state :: wait_ticket | playing | pausing | unpausing | prestopping | stopping | extra | gendtmf,
%%               ref,
%%               timer_ref,
%%               player_id,
%%               mode = 0,
%%               volume_divider = 1,
%%               file = <<>>,
%%               files = [],
%%               folder = <<>>,
%%               randomize = true,
%%               loop = false,
%%               startat = 0,
%%               stopat = 0,
%%               playtime = 0,
%%               extratime = 0,
%%               maxcount = 0,
%%               interrupts = [],
%%               buffer = <<>>,
%%               clear_buffer = true,
%%               clear_interrupt = true,
%%               gen_dtmf = <<>>,
%%               mgid
%%              }).
%%
%%-define(S_WAIT_TICKET, wait_ticket).
%%-define(S_PLAYING, playing).
%%-define(S_PAUSING, pausing).
%%-define(S_UNPAUSING, unpausing).
%%-define(S_PRESTOPPING, prestopping).
%%-define(S_STOPPING, stopping).
%%-define(S_EXTRA, extra).
%%-define(S_GENDTMF, gendtmf).
%%
%%-define(M_ASYNC_START_PLAY, async_start_play).
%%-define(M_ASYNC_STOP_PLAY, async_stop_play).
%%-define(M_ASYNC_MODIFY_VOLUME, async_modify_volume).
%%-define(M_SYNC_PLAY_FILE, sync_play_file).
%%-define(M_SYNC_PLAY_FILES, sync_play_files).
%%-define(M_SYNC_PLAY_FILE_LIMIT, sync_play_file_limit).
%%-define(M_SYNC_PLAY_FILE_EXTRA, sync_play_file_extra).
%%-define(M_GEN_DTMF, gen_dtmf).
%%
%%%% ====================================================================
%%%% Callback functions
%%%% ====================================================================
%%
%%%% -----------------------------------------------------
%%%% returns descriptions of used properties
%%%% -----------------------------------------------------
%%metadata(_ScriptType) ->
%%    [{<<"playerId">>, #{type => <<"argument">> }},
%%     {<<"mode">>, #{type => <<"list">>,
%%                    items => [{0,<<"syncPlayFile">>},
%%                              {1,<<"syncPlayFileLimit">>},
%%                              {2,<<"syncPlayFileExtra">>},
%%                              {7,<<"syncPlayFiles">>},
%%                              {3,<<"asyncPlayStart">>},
%%                              {4,<<"asyncPlayStop">>},
%%                              {6,<<"asyncModifyVolume">>}] }}, % removed "default => 0" not effective here
%%     {<<"playTimeSec">>, #{type => <<"argument">>,
%%                           filter => <<"mode==1">> }},
%%     {<<"extraTimeSec">>, #{type => <<"argument">>,
%%                            filter => <<"mode==2">> }},
%%     {<<"folder">>, #{type => <<"argument">>,
%%                      filter => <<"mode==3">> }},
%%     {<<"randomize">>, #{type => <<"list">>,
%%                         items => [{0,<<"no">>},
%%                                   {1,<<"yes">>}],
%%                         filter => <<"(mode==3) && (folder!=null) && (folder!=\"\")">> }}, % removed "default => 1" not effective here
%%     {<<"file">>, #{type => <<"file">>,
%%                    filter => <<"mode in (0,1,2,3)">> }},
%%     {<<"filesArg">>, #{type => <<"argument">>,
%%                        filter => <<"mode==7">> }},
%%     {<<"loop">>, #{type => <<"list">>,
%%                    items => [{0,<<"no">>},
%%                              {1,<<"yes">>}],
%%                    filter => <<"mode==3">> }}, % removed "default => 0" not effective here
%%     {<<"startAt">>, #{type => <<"argument">>,
%%                       filter => <<"(mode in (0,1,2)) && (file!=null)">> }},
%%     {<<"stopAt">>, #{type => <<"argument">>,
%%                      filter => <<"(mode in (0,1,2)) && (file!=null)">> }},
%%     {<<"volumeDiv">>, #{type => <<"argument">>,
%%                         filter => <<"mode in (0,1,2,3,6,7)">> }},
%%     {<<"dtmfBuffer">>, #{type => <<"variable">>,
%%                          filter => <<"mode in (0,1,2,7)">> }},
%%     {<<"clearDtmfBuffer">>, #{type => <<"list">>,
%%                               items => [{0,<<"no">>},
%%                                         {1,<<"yes">>}],
%%                               filter => <<"(mode in (0,1,2,7)) && (dtmfBuffer!=null)">> }}, % removed "default => 1" not effective here
%%     {<<"maxSymbolCount">>, #{type => <<"argument">>,
%%                              filter => <<"mode in (0,1,2,7)">> }},
%%     {<<"interruptSymbols">>, #{type => <<"string">>,
%%                                filter => <<"mode in (0,1,2,7)">> }}, % removed "default => null" not effective here
%%     {<<"clearInterrupt">>, #{type => <<"list">>,
%%                              items => [{0,<<"no">>},
%%                                        {1,<<"yes">>}],
%%                              filter => <<"(mode in (0,1,2,7)) && (interruptSymbols!=null) && (interruptSymbols!=\"\")">> }}, % removed "default => 1" not effective here
%%     {<<"transfer">>, #{type => <<"transfer">> }},
%%     {<<"transferError">>, #{type => <<"transfer">>,
%%                             title => <<"error">> }} ].
%%
%%%% -----------------------------------------------------
%%
%%init(#state_scr{ownerpid=undefined}=State) ->
%%    ?SCR_COMP:next([<<"transferError">>, <<"transfer">>], State);
%%init(State) ->
%%    %checking file or folder path
%%    case expand_and_check_paths(State) of
%%        {error, Reason} ->
%%            ?SCR_ERROR(State, "~500tp : next transferError -> Reason ~500tp", [?MODULE, Reason]),
%%            ?SCR_COMP:next([<<"transferError">>, <<"transfer">>], State);
%%        {file, {error,Reason}} ->
%%            ?SCR_ERROR(State, "~500tp : next transferError -> Reason ~500tp", [?MODULE, Reason]),
%%            ?SCR_COMP:next([<<"transferError">>, <<"transfer">>], State);
%%        {folder, {error,Reason}} ->
%%            ?SCR_ERROR(State, "~500tp : next transferError -> Reason ~500tp", [?MODULE, Reason]),
%%            ?SCR_COMP:next([<<"transferError">>, <<"transfer">>], State);
%%        {file, FilePath} ->
%%            ?SCR_TRACE(State, "~500tp : init player {file, ~500tp}", [?MODULE, FilePath]),
%%            {ok,FilePath1,State1} = file_copy_to_mg_temp(FilePath, State),
%%            case FilePath1 of
%%                false ->
%%                    ?SCR_ERROR(State, "~500tp : next transferError -> Reason cannot copy to MG", [?MODULE]),
%%                    ?SCR_COMP:next([<<"transferError">>, <<"transfer">>], State);
%%                _ ->
%%                    init_player({FilePath1, [], <<>>}, State1)
%%            end;
%%        {files, FilePathList} ->
%%            ?SCR_TRACE(State, "~500tp : init player {files, ~500tp}", [?MODULE, FilePathList]),
%%            {ok,FilePathList1,State1} = file_copy_to_mg_temp(FilePathList, State),
%%            case lists:filter(fun(false) -> false; (_) -> true end, FilePathList1) of
%%                [] ->
%%                    ?SCR_ERROR(State, "~500tp : next transferError -> Reason cannot copy to MG", [?MODULE]),
%%                    ?SCR_COMP:next([<<"transferError">>, <<"transfer">>], State);
%%                FilePathList2 ->
%%                    init_player({<<>>, FilePathList2, <<>>}, State1)
%%            end;
%%        {folder, FolderPath} ->
%%            ?SCR_TRACE(State, "~500tp : init player {folder, ~500tp}", [?MODULE, FolderPath]),
%%            init_player({<<>>, [], FolderPath}, State);
%%        ok ->
%%            ?SCR_TRACE(State, "~500tp : init player operation", [?MODULE]),
%%            init_player({<<>>, [], <<>>}, State)
%%    end.
%%
%%%% @private
%%init_player({FilePath, FilePathList, FolderPath},
%%            #state_scr{ownerpid=OwnerPid, active_component=#component{data=CompData}=Comp}=State) ->
%%    % properties
%%    Mode = mode(?SCR_COMP:get(<<"mode">>, CompData, 0)),
%%    CS = #play{player_id = ?SCR_ARG:get_string(<<"playerId">>, CompData, <<"default">>, State),
%%               mode = Mode,
%%               volume_divider = max(1, min(100, ?SCR_ARG:get_int(<<"volumeDiv">>, CompData, 1, State))),
%%               loop = ?EU:to_bool(?SCR_COMP:get(<<"loop">>, CompData, 0)),
%%               file = FilePath,
%%               files = FilePathList,
%%               startat = ?SCR_ARG:get_int(<<"startAt">>, CompData, 0, State),
%%               stopat = ?SCR_ARG:get_int(<<"stopAt">>, CompData, 0, State),
%%               folder = FolderPath,
%%               randomize = ?EU:to_bool(?SCR_COMP:get(<<"randomize">>, CompData, 1)),
%%               playtime = case Mode of ?M_SYNC_PLAY_FILE_LIMIT -> ?SCR_ARG:get_int(<<"playTimeSec">>, CompData, 0, State) * 1000; _ -> 0 end,
%%               extratime = case Mode of ?M_SYNC_PLAY_FILE_EXTRA -> ?SCR_ARG:get_int(<<"extraTimeSec">>, CompData, 0, State) * 1000; _ -> 0 end,
%%               maxcount = ?SCR_ARG:get_int(<<"maxSymbolCount">>, CompData, 0, State),
%%               interrupts = ?IVR_SCRIPT_UTILS:parse_interrupts(<<"interruptSymbols">>, CompData),
%%               clear_buffer = ?EU:to_bool(?SCR_COMP:get(<<"clearDtmfBuffer">>, CompData, 1)),
%%               clear_interrupt = ?EU:to_bool(?SCR_COMP:get(<<"clearInterrupt">>, CompData, 0)),
%%               gen_dtmf = ?SCR_ARG:get_string(<<"dtmf">>, CompData, <<>>, State)},
%%    % @debug
%%    %CS = CS_#play{mode = mode(1), playtime = 10000, file= <<"files/wait/hold.wav">>, extratime=2000},
%%    %CS = CS_#play{mode = mode(2), playtime = 10000, file= <<"files/wait/snd_city.wav">>, extratime= 2000},
%%    %CS = CS_#play{mode = mode(3), playtime = 10000, folder= <<"files/wait">>},
%%    % init
%%    CmdRef = make_ref(),
%%    {Cmd,OptsMap} = make_cmd_opts(CS),
%%    OwnerPid ! {scriptmachine, {ivr_cmd, CmdRef, {ext, ?MODULE, Cmd, self(), OptsMap}}},
%%    TimerRef1 = ?SCR_COMP:event_after(?TIMEOUT, {'timeout_state',CmdRef,S=?S_WAIT_TICKET}),
%%    {ok, State#state_scr{active_component=Comp#component{state=CS#play{state = S,
%%                                                                       ref = CmdRef,
%%                                                                       timer_ref = TimerRef1,
%%                                                                       buffer = <<>>}}}}.
%%
%%%% -----------------------------------------------------
%%
%%%% timeout of wait_ticket
%%handle_event({timeout_state,Ref,S}, #state_scr{active_component=#component{state=#play{state=S=?S_WAIT_TICKET,ref=Ref}}}=State) ->
%%    %?OUT("Ticket1 timeout", []),
%%    Err = {error,{timeout,<<"Ticket timeout">>}},
%%    fin(Err, State);
%%
%%%% ticket mgc error (s = wait_ticket)
%%handle_event({ticket,CmdRef,{error,_}=Err}, #state_scr{active_component=#component{state=#play{state=?S_WAIT_TICKET,ref=CmdRef,timer_ref=TimerRef}}}=State) ->
%%    %?OUT("Ticket1 ~500tp", [Err]),
%%    erlang:cancel_timer(TimerRef),
%%    fin(Err, State);
%%
%%%% ticket ok (s = wait_ticket)
%%handle_event({ticket,CmdRef,{ok,gendtmf,T}}, #state_scr{active_component=#component{state=#play{state=?S_WAIT_TICKET,ref=CmdRef,timer_ref=TimerRef}=CS}=Comp}=State) ->
%%    erlang:cancel_timer(TimerRef),
%%    TimerRef1 = ?SCR_COMP:event_after(T, {'timeout_state',CmdRef,S1=?S_GENDTMF}),
%%    {ok, State#state_scr{active_component=Comp#component{state=CS#play{state=S1,
%%                                                                       timer_ref=TimerRef1}}}};
%%
%%handle_event({ticket,CmdRef,{ok,started}}, #state_scr{active_component=#component{state=#play{state=?S_WAIT_TICKET,ref=CmdRef,timer_ref=TimerRef}=CS}=Comp}=State) ->
%%    %?OUT("Ticket1 ok"),
%%    erlang:cancel_timer(TimerRef),
%%    case CS of
%%        #play{mode=?M_ASYNC_START_PLAY} -> fin(async, State);
%%        #play{mode=?M_ASYNC_STOP_PLAY} -> fin(async, State);
%%        #play{mode=?M_ASYNC_MODIFY_VOLUME} -> fin(async, State);
%%        #play{mode=?M_SYNC_PLAY_FILE_LIMIT, playtime=T} when T>0 ->
%%            TimerRef1 = ?SCR_COMP:event_after(T, {'timeout_state',CmdRef,S1=?S_PLAYING}),
%%            {ok, State#state_scr{active_component=Comp#component{state=CS#play{state=S1,
%%                                                                               timer_ref=TimerRef1}}}};
%%        _ ->
%%            {ok, State#state_scr{active_component=Comp#component{state=CS#play{state=?S_PLAYING}}}}
%%    end;
%%
%%%% ------------------------------------
%%
%%%% dtmf from mg (rfc2833, inband)
%%handle_event({mg_dtmf,Dtmf}, State) ->
%%    dtmf(Dtmf, State);
%%
%%%% dtmf from sip (info)
%%handle_event({sip_info,Req}, State) ->
%%    ?U:get_dtmf_from_sipinfo(Req, fun(Dtmf) -> dtmf(Dtmf,State) end, fun() -> {ok,State} end);
%%
%%%% ------------------------------------
%%
%%%% timeout of playing
%%handle_event({timeout_state,CmdRef,S}, #state_scr{active_component=#component{state=#play{state=S=?S_PLAYING,ref=CmdRef}}}=State) ->
%%    exec_stop_play(State);
%%
%%%% external stop of playing
%%handle_event('stop_play', #state_scr{active_component=#component{state=#play{state=?S_PLAYING}}}=State) ->
%%    exec_stop_play(State);
%%
%%%% file play stopped by mg
%%handle_event({play_stopped,PlayerId,_Event}, #state_scr{active_component=#component{state=#play{state=?S_PLAYING,player_id=PlayerId,timer_ref=TimerRef}}}=State) ->
%%    erlang:cancel_timer(TimerRef),
%%    exec_stop_play(State);
%%
%%%% ------------------------------------
%%
%%%% timeout of pausing
%%handle_event({timeout_state,Ref,S}, #state_scr{active_component=#component{state=#play{state=S=?S_PAUSING,ref=Ref}}}=State) ->
%%    Err = {error, {timeout, <<"Ticket timeout">>}},
%%    fin(Err, State);
%%
%%%% ticket ok (s = pausing)
%%handle_event({ticket,CmdRef,{ok,paused}}, #state_scr{active_component=#component{state=#play{state=?S_PAUSING, ref=CmdRef, timer_ref=TimerRef}}}=State) ->
%%    %?OUT("Ticket ok paused"),
%%    erlang:cancel_timer(TimerRef),
%%    {ok, State};
%%
%%%% ------------------------------------
%%
%%%% timeout of unpausing
%%handle_event({timeout_state,Ref,S}, #state_scr{active_component=#component{state=#play{state=S=?S_UNPAUSING,ref=Ref}}}=State) ->
%%    Err = {error, {timeout, <<"Ticket timeout">>}},
%%    fin(Err, State);
%%
%%%% ticket ok (s = unpausing)
%%handle_event({ticket,CmdRef,{ok,unpaused}}, #state_scr{active_component=#component{state=#play{state=?S_UNPAUSING, ref=CmdRef, timer_ref=TimerRef}=CS}=Comp}=State) ->
%%    %?OUT("Ticket ok unpaused"),
%%    erlang:cancel_timer(TimerRef),
%%    {ok, State#state_scr{active_component=Comp#component{state=CS#play{state=?S_PLAYING}}}};
%%
%%%% ------------------------------------
%%
%%%% timeout of prestopping
%%handle_event({timeout_state,Ref,S}, #state_scr{active_component=#component{state=#play{state=S=?S_PRESTOPPING,ref=Ref}}}=State) ->
%%    Err = {error, {timeout, <<"Ticket timeout">>}},
%%    fin(Err, State);
%%
%%%% ticket ok (s = prestopping)
%%handle_event({ticket,CmdRef,{ok,prestopped}}, #state_scr{active_component=#component{state=#play{state=?S_PRESTOPPING, ref=CmdRef, timer_ref=TimerRef}}}=State) ->
%%    ?OUT("Ticket ok prestopped"),
%%    erlang:cancel_timer(TimerRef),
%%    exec_stop_play(State);
%%
%%%% ------------------------------------
%%
%%%% timeout of stopping
%%handle_event({timeout_state,Ref,S}, #state_scr{active_component=#component{state=#play{state=S=?S_STOPPING,ref=Ref}}}=State) ->
%%    Err = {error, {timeout, <<"Ticket timeout">>}},
%%    fin(Err, State);
%%
%%%% ticket ok (s = stopping)
%%handle_event({ticket,CmdRef,{ok,stopped}}, #state_scr{active_component=#component{state=#play{state=S, ref=CmdRef, timer_ref=TimerRef}=CS}=Comp}=State)
%%  when S==?S_STOPPING; S==?S_WAIT_TICKET ->
%%    %?OUT("Ticket ok stopped"),
%%    erlang:cancel_timer(TimerRef),
%%    case CS of
%%        #play{extratime=T} when T>0 ->
%%            TimerRef1 = ?SCR_COMP:event_after(T, {'timeout_state',CmdRef,S1=?S_EXTRA}),
%%            {ok, State#state_scr{active_component=Comp#component{state=CS#play{state=S1,
%%                                                                               timer_ref=TimerRef1}}}};
%%        _ ->
%%            fin(stopped, State)
%%    end;
%%
%%%% ------------------------------------
%%
%%%% ticket mgc error (s = pausing, prestopping, stopping)
%%handle_event({ticket,CmdRef,{error,_}=Err}, #state_scr{active_component=#component{state=#play{ref=CmdRef,timer_ref=TimerRef}}}=State) ->
%%    %?OUT("Ticket error"),
%%    erlang:cancel_timer(TimerRef),
%%    fin(Err, State);
%%
%%%% ------------------------------------
%%
%%%% timeout of extra
%%handle_event({timeout_state,Ref,S}, #state_scr{active_component=#component{state=#play{state=S=?S_EXTRA,ref=Ref}}}=State) ->
%%    fin(extra, State);
%%
%%%% timeout of gendtmf
%%handle_event({timeout_state,CmdRef,S}, #state_scr{active_component=#component{state=#play{state=S=?S_GENDTMF,ref=CmdRef}}}=State) ->
%%    fin(gendtmf, State);
%%
%%%% ------------------------------------
%%
%%handle_event(_Ev, State) ->
%%    {ok, State}.
%%
%%%% ------------------------------------
%%
%%%% terminate component externally
%%terminate(#state_scr{active_component=#component{state=#play{state=PS,timer_ref=TimerRef}}}=State) when PS==?S_PLAYING; PS==?S_PAUSING; PS==?S_PRESTOPPING ->
%%    erlang:cancel_timer(TimerRef),
%%    {ok,State1} = exec_stop_play(State),
%%    #state_scr{active_component=#component{state=#play{timer_ref=TimerRef1}}}=State1,
%%    erlang:cancel_timer(TimerRef1),
%%    {ok,State1};
%%
%%terminate(State) ->
%%    {ok, State}.
%%
%%%% ====================================================================
%%%% Functional iface of SIP IVR dlg (linked to its fsm)
%%%% ====================================================================
%%
%%%% -------------------------------
%%%% start player
%%%% -------------------------------
%%
%%cmd_attach_player({_Ref, _FromPid, _Arg}=P, #state_ivr{a=#side{state=S}}=State) when S==dialog; S==early ->
%%    do_cmd_attach_player(P, State);
%%cmd_attach_player({Ref, FromPid, _Arg}, #state_ivr{}=State) ->
%%    ?SCR_COMP:event(FromPid, {'ticket',Ref,{error,{invalid_operation,<<"Play could not be started, state mismatch">>}}}),
%%    {next_state, ?CURRSTATE, State}.
%%
%%% @private
%%do_cmd_attach_player({Ref, FromPid, Arg}, State) when is_map(Arg) ->
%%    Opts = Arg,
%%    case ?IVR_MEDIA:media_attach_player(Opts, State) of
%%        {error,_}=Err ->
%%            ?SCR_COMP:event(FromPid, {'ticket',Ref,Err}),
%%            {next_state, ?CURRSTATE, State};
%%        {ok, State1} ->
%%            ?SCR_COMP:event(FromPid, {'ticket',Ref,{ok,started}}),
%%            {next_state, ?CURRSTATE, State1}
%%    end.
%%
%%%% -------------------------------
%%%% modify player (pause/unpause, prestop)
%%%% -------------------------------
%%
%%cmd_modify_player({_Ref, _FromPid, _Arg}=P, #state_ivr{a=#side{state=S}}=State) when S==dialog; S==early; S==bye ->
%%    do_cmd_modify_player(P, #state_ivr{a=#side{state=S}}=State);
%%cmd_modify_player({Ref, FromPid, _Arg}, #state_ivr{}=State) ->
%%    ?SCR_COMP:event(FromPid, {'ticket',Ref,{error,{invalid_operation,<<"Player could not be modified, state mismatch">>}}}),
%%    {next_state, ?CURRSTATE, State}.
%%
%%% @private
%%do_cmd_modify_player({Ref, FromPid, Arg}, State) when is_map(Arg) ->
%%    Opts = maps:without(['report'], Arg),
%%    case ?IVR_MEDIA:media_modify_player(Opts, State) of
%%        {error,_}=Err ->
%%            ?SCR_COMP:event(FromPid, {'ticket',Ref,Err}),
%%            {next_state, ?CURRSTATE, State};
%%        {ok, State1} ->
%%            ?SCR_COMP:event(FromPid, {'ticket',Ref,{ok,maps:get('report',Arg,'started')}}),
%%            {next_state, ?CURRSTATE, State1}
%%    end.
%%
%%%% -------------------------------
%%%% detach player
%%%% -------------------------------
%%
%%cmd_detach_player({_Ref, _FromPid, _Arg}=P, #state_ivr{a=#side{state=S}}=State) when S==dialog; S==early; S==bye ->
%%    do_cmd_detach_player(P, #state_ivr{a=#side{state=S}}=State);
%%cmd_detach_player({Ref, FromPid, _Arg}, #state_ivr{}=State) ->
%%    ?SCR_COMP:event(FromPid, {'ticket',Ref,{error,{invalid_operation,<<"Play could not be stopped, state mismatch">>}}}),
%%    {next_state, ?CURRSTATE, State}.
%%
%%% @private
%%do_cmd_detach_player({Ref, FromPid, Arg}, State) when is_map(Arg) ->
%%    Opts = Arg,
%%    case ?IVR_MEDIA:media_detach_player(Opts, State) of
%%        {error,_}=Err ->
%%            ?SCR_COMP:event(FromPid, {'ticket',Ref,Err}),
%%            {next_state, ?CURRSTATE, State};
%%        {ok, State1} ->
%%            ?SCR_COMP:event(FromPid, {'ticket',Ref,{ok,stopped}}),
%%            {next_state, ?CURRSTATE, State1}
%%    end.
%%
%%%% -------------------------------
%%%% gen_dtmf
%%%% -------------------------------
%%
%%generate_dtmf({_Ref, _FromPid, _Arg}=P, #state_ivr{a=#side{state=S}}=State) when S==dialog; S==early ->
%%    do_generate_dtmf(P, #state_ivr{a=#side{state=S}}=State);
%%generate_dtmf({Ref, FromPid, _Arg}, State) ->
%%    ?SCR_COMP:event(FromPid, {'ticket',Ref,{error,{invalid_operation,<<"DTMF could not be sent, state mismatch">>}}}),
%%    {next_state, ?CURRSTATE, State}.
%%
%%% @private
%%do_generate_dtmf({Ref, FromPid, Arg}, State) when is_map(Arg) ->
%%    % @todo
%%    %%     Dtmf = maps:get(dtmf,Arg,<<>>),
%%    %%     case ?IVR_MEDIA:media_generate_dtmf(Dtmf, State) of
%%    %%         {error,_}=Err ->
%%    %%             ?SCR_COMP:event(FromPid, {'ticket',Ref,Err}),
%%    %%             {next_state, ?CURRSTATE, State};
%%    %%         {ok, State1} ->
%%    %%             Length = size(Dtmf),
%%    %%             T = Length * (300 + 200),
%%    %%             ?SCR_COMP:event(FromPid, {'ticket',Ref,{ok,gendtmf,T}}),
%%    %%             {next_state, ?CURRSTATE, State1}
%%    %%     end,
%%    ?SCR_COMP:event(FromPid, {'ticket',Ref,{error,{not_implemented,<<"Generate DTMF command not implemented">>}}}),
%%    {next_state, ?CURRSTATE, State}.
%%
%%%% ====================================================================
%%%% Internal functions
%%%% ====================================================================
%%
%%%% -------------------------------------
%%%% play
%%%% -------------------------------------
%%
%%%% ---------------
%%%% mode mapping
%%%% ---------------
%%mode(0) -> ?M_SYNC_PLAY_FILE;
%%mode(1) -> ?M_SYNC_PLAY_FILE_LIMIT;
%%mode(2) -> ?M_SYNC_PLAY_FILE_EXTRA;
%%mode(3) -> ?M_ASYNC_START_PLAY;
%%mode(4) -> ?M_ASYNC_STOP_PLAY;
%%mode(5) -> ?M_GEN_DTMF;
%%mode(6) -> ?M_ASYNC_MODIFY_VOLUME;
%%mode(7) -> ?M_SYNC_PLAY_FILES;
%%mode(Mode) when is_atom(Mode) -> Mode.
%%
%%%% -------------------------------------
%%%% prepare opts for rtx
%%%% -------------------------------------
%%make_cmd_opts(#play{mode=?M_GEN_DTMF, gen_dtmf=Dtmf}) ->
%%    O = #{dtmf => Dtmf},
%%    {generate_dtmf, O};
%%make_cmd_opts(#play{player_id=PlayerId}=CS) ->
%%    O = #{playerid => PlayerId},
%%    make_cmd_opts_1(CS, O).
%%
%%%% @private
%%make_cmd_opts_1(#play{mode=?M_ASYNC_STOP_PLAY}=_CS, O) ->
%%    {cmd_detach_player, O};
%%make_cmd_opts_1(#play{mode=?M_ASYNC_MODIFY_VOLUME, volume_divider=Volume}=_CS, O) ->
%%    O1 = O#{mode => "play",
%%            volume => Volume},
%%    {cmd_modify_player, O1};
%%make_cmd_opts_1(#play{mode=?M_ASYNC_START_PLAY, volume_divider=Volume, loop=Loop}=CS, O) ->
%%    O1 = O#{mode => "play"},
%%    O2 = case Volume>1 of true -> O1#{volume => Volume}; false -> O1 end,
%%    O3 = case is_boolean(Loop) of true when Loop -> O2#{loop => Loop}; _ -> O2 end,
%%    make_cmd_opts_2(CS, O3);
%%make_cmd_opts_1(#play{volume_divider=Volume}=CS, O) ->
%%    O1 = case Volume>1 of true -> O#{volume => Volume}; false -> O end,
%%    make_cmd_opts_2(CS, O1).
%%
%%%% @private
%%make_cmd_opts_2(#play{mode=?M_ASYNC_START_PLAY, folder=Folder, randomize=Rand}=_CS, O) when Folder/= <<>> ->
%%    O1 = O#{dir => Folder},
%%    O2 = case is_boolean(Rand) of true when Rand -> O1#{random => Rand}; _ -> O1 end,
%%    {cmd_attach_player, O2};
%%make_cmd_opts_2(#play{mode=?M_SYNC_PLAY_FILES, files=Files}=_CS, O) when is_list(Files) ->
%%    O1 = O#{files => Files},
%%    {cmd_attach_player, O1};
%%make_cmd_opts_2(#play{mode=M, file=File}=CS, O)
%%  when M==?M_ASYNC_START_PLAY;
%%       M==?M_SYNC_PLAY_FILE;
%%       M==?M_SYNC_PLAY_FILE_LIMIT;
%%       M==?M_SYNC_PLAY_FILE_EXTRA ->
%%    #play{startat=StartAt, stopat=StopAt}=CS,
%%    O1 = O#{file => File},
%%    O2 = case StartAt>0 of true -> O1#{startat => StartAt}; false -> O1 end,
%%    O3 = case StopAt>0 of true -> O2#{stopat => StopAt}; false -> O2 end,
%%    {cmd_attach_player, O3}.
%%
%%%%%% exec pause play command
%%%%exec_pause_play(#state_scr{active_component=#component{state=#play{state=?S_PLAYING}}}=State) ->
%%%%    exec_modify_player(?S_PAUSING, "pause", paused, State).
%%%%
%%%%%% exec unpause play command
%%%%exec_unpause_play(#state_scr{active_component=#component{state=#play{state=?S_PAUSING}}}=State) ->
%%%%    exec_modify_player(?S_UNPAUSING, "unpaused", unpaused, State).
%%%%
%%%%%% %% exec prestop play command
%%%%%% exec_prestop_play(#state_scr{active_component=#component{state=#play{state=PS}}}=State) when PS==?S_PLAYING; PS==?S_PAUSING ->
%%%%%%     exec_modify_player(?S_PRESTOPPING, "stop", prestopped, State).
%%%%
%%%%%% @private
%%%%exec_modify_player(S, Mode, Report, #state_scr{active_component=#component{state=#play{player_id=PlayerId}}}=State) ->
%%%%    Arg = #{playerid => PlayerId,
%%%%            mode => Mode,
%%%%            report => Report},
%%%%    exec_player_cmd(S, cmd_modify_player, Arg, State).
%%
%%%% exec stop play command
%%exec_stop_play(#state_scr{active_component=#component{state=#play{state=PS, player_id=PlayerId}}}=State) when PS==?S_PLAYING; PS==?S_PAUSING; PS==?S_PRESTOPPING ->
%%    exec_player_cmd(?S_STOPPING, cmd_detach_player, #{playerid => PlayerId}, State).
%%
%%%% @private
%%exec_player_cmd(S, Method, Arg, #state_scr{active_component=#component{state=#play{}=CS}=Comp}=State) ->
%%    #play{ref=CmdRef,timer_ref=TimerRef,player_id=PlayerId}=CS,
%%    erlang:cancel_timer(TimerRef),
%%    #state_scr{ownerpid=OwnerPid}=State,
%%    OwnerPid ! {scriptmachine, {ivr_cmd, CmdRef, {ext, ?MODULE, Method, self(), Arg}}},
%%    TimerRef1 = ?SCR_COMP:event_after(?TIMEOUT, {'timeout_state',CmdRef,S}),
%%    {ok, State#state_scr{active_component=Comp#component{state=CS#play{state=S,
%%                                                                       timer_ref=TimerRef1,
%%                                                                       player_id=PlayerId}}}}.
%%
%%%% -------------------------------------
%%%% dtmf interrupt
%%%% -------------------------------------
%%
%%%% -----------
%%%% normalize dtmf symbols
%%%% -----------
%%normalize(X) -> ?U:normalize_dtmf(X).
%%
%%%% -----------
%%%% dtmf received
%%%% -----------
%%dtmf(Dtmf, #state_scr{active_component=#component{state=CS}=Comp}=State) ->
%%    Dtmf1 = normalize(Dtmf),
%%    #play{buffer=Buffer}=CS,
%%    State1 = State#state_scr{active_component=Comp#component{state=CS#play{buffer= <<Buffer/bitstring,Dtmf1/bitstring>>}}},
%%    check_fin(State1).
%%
%%%% -----------
%%%% @private
%%%% check if complete
%%%% -----------
%%check_fin(State) ->
%%    check_interrupt(State).
%%
%%%% -----------
%%%% @private
%%%% check if complete by interrupt
%%%% -----------
%%check_interrupt(#state_scr{active_component=#component{state=CS}=Comp}=State) ->
%%    #play{buffer=Buffer,
%%          interrupts=Interrupts}=CS,
%%    Length = size(Buffer),
%%    F = fun(Interrupt, false) ->
%%                case binary:matches(Buffer, Interrupt) of
%%                    [] -> false;
%%                    M ->
%%                        {Pos,Len} = lists:last(M),
%%                        case Pos+Len of
%%                            Length ->
%%                                case CS#play.clear_interrupt of
%%                                    false -> {true,Buffer};
%%                                    true ->
%%                                        % clear interrupt symbols
%%                                        Bits = Pos*8,
%%                                        <<Clear:Bits/bitstring,_/bitstring>> = Buffer,
%%                                        {true,Clear}
%%                                end;
%%                            _ -> false
%%                        end
%%                end;
%%           (_,Acc) -> Acc
%%        end,
%%    case lists:foldl(F, false, Interrupts) of
%%        {true,Res} -> fin(interrupt, State#state_scr{active_component=Comp#component{state=CS#play{buffer=Res}}});
%%        false -> check_length(State)
%%    end.
%%
%%%% -----------
%%%% @private
%%%% check if complete by max length
%%%% -----------
%%check_length(#state_scr{active_component=#component{state=CS}}=State) ->
%%    #play{buffer=Buffer,
%%          maxcount=MaxSymbolCount}=CS,
%%    Length = size(Buffer),
%%    case MaxSymbolCount>0 andalso Length>=MaxSymbolCount of
%%        true -> fin(length,State);
%%        false -> {ok,State}
%%    end.
%%
%%%% -------------------------------------
%%%% finalize work of component
%%%% mode = error, async, interrupt, length, stopped, extra, gendtmf
%%%% -------------------------------------
%%fin({error,_}=Err, State) ->
%%    ?SCR_ERROR(State, "~500tp : fin : error : ~500tp", [?MODULE, Err]),
%%    Err;
%%fin(Mode, #state_scr{active_component=#component{state=#play{state=?S_PLAYING}=CS}=Comp}=State) ->
%%    exec_stop_play(State),
%%    fin(Mode, State#state_scr{active_component=Comp#component{state=CS#play{state=?S_STOPPING}}});
%%fin(Mode, #state_scr{active_component=#component{data=Data,state=CS}}=State) ->
%%    #play{buffer=Buffer}=CS,
%%    Var = ?SCR_COMP:get(<<"dtmfBuffer">>, Data, <<>>),
%%    Prev = prev_dtmf_buffer(Var, State),
%%    State1 = assign(Var, <<Prev/bitstring, Buffer/bitstring>>, State),
%%    fin_1(Mode, State1).
%%fin_1(_, State) -> ?SCR_COMP:next(<<"transfer">>,State).
%%
%%%% -------------------------------------
%%%% return previous value of variable
%%%% -------------------------------------
%%prev_dtmf_buffer(Var, #state_scr{active_component=#component{state=CS}}=State) ->
%%    case CS#play.clear_buffer orelse ?SCR_VAR:get_value(Var, undefined, State) of
%%        true -> <<>>;
%%        undefined -> <<>>;
%%        V when is_binary(V) -> V;
%%        V when is_integer(V) -> ?EU:to_binary(V);
%%        {S,_Enc} when is_binary(S) -> S;
%%        _ -> <<>>
%%    end.
%%
%%%% -------------------------------------
%%%% assign value to variable
%%%% -------------------------------------
%%assign(Var, Value, State) ->
%%    case ?SCR_VAR:set_value(Var, Value, State) of
%%        {ok, State1} -> State1;
%%        _ -> State
%%    end.
%%
%%%% -------------------------------------
%%%% checking incoming file or folder path
%%%% -------------------------------------
%%-spec expand_and_check_paths(State::state_scr()) ->
%%                                    ok |
%%                                    {file, FilePath::binary()} |
%%                                    {files, FilesPaths::[binary()]} |
%%                                    {folder, FolderPath::binary()} |
%%                                    {error, Reason::binary()}.
%%%% -------------------------------------
%%expand_and_check_paths(#state_scr{active_component=#component{data=CompData}}=State) ->
%%    case ?EU:get_by_key('direct_file', CompData, <<>>) of
%%        <<>> -> expand_and_check_paths_1(State);
%%        FilePath when is_binary(FilePath) -> {file,FilePath}
%%    end.
%%
%%%% @private
%%expand_and_check_paths_1(#state_scr{active_component=#component{data=CompData}}=State) ->
%%    FilePath = ?SCR_COMP:get_file(<<"file">>, CompData, <<>>, State),
%%    FolderPath = ?SCR_ARG:get_string(<<"folder">>, CompData, <<>>, State),
%%    FilesList = try ?SCR_ARG:get_string(<<"filesArg">>, CompData, <<>>, State)
%%                catch X:Y:StackTrace -> ?OUT("X=~tp, Y=~tp, Trace=~120tp",[X,Y,StackTrace])
%%                end,
%%    Mode = mode(?SCR_COMP:get(<<"mode">>, CompData, 0)),
%%    case Mode of
%%        ?M_SYNC_PLAY_FILES when FilesList == <<>> -> {error, <<"badargs">>};
%%        ?M_SYNC_PLAY_FILES ->
%%            try
%%                Files1 = jsx:decode(FilesList),
%%                Files2 = lists:map(fun(Path) -> ?SCR_FILE:expand_and_check_path(Path, read, State) end, Files1),
%%                {files, Files2}
%%            catch _:_ -> {error, <<"badargs">>}
%%            end;
%%        _ when Mode==?M_ASYNC_START_PLAY
%%               orelse Mode==?M_SYNC_PLAY_FILE
%%               orelse Mode==?M_SYNC_PLAY_FILE_EXTRA
%%               orelse Mode==?M_SYNC_PLAY_FILE_LIMIT ->
%%            case {FilePath, FolderPath} of
%%                {<<>>, <<>>} -> {error, <<"badargs">>};
%%                {_, <<>>} -> {file, ?SCR_FILE:expand_and_check_path(FilePath, read, State)};
%%                {<<>>,_} -> {folder, ?SCR_FILE:expand_and_check_path(FolderPath, read, State)};
%%                {_, _} -> {error, <<"badargs">>}
%%            end;
%%        _ -> ok
%%    end.
%%
%%%% --------------------------------------
%%file_copy_to_mg_temp(X,State) ->
%%    F_get_mgid = fun(#state_scr{active_component=#component{state=#play{mgid=MGID}}}) -> MGID;
%%                    (#state_scr{}=State1) -> ?IVR_SCRIPT_UTILS:get_mgid(State1)
%%                 end,
%%    F_store_mgid = fun(_,#state_scr{active_component=#component{state=#play{}}}=State1) -> State1;
%%                      (MGID,#state_scr{active_component=Comp}=State1) -> State1#state_scr{active_component=Comp#component{state=#play{mgid=MGID}}}
%%                   end,
%%    ?IVR_SCRIPT_UTILS:file_copy_to_mg_temp(X,{F_store_mgid,F_get_mgid},State).
