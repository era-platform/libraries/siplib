%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Pavel Abramov, Peter Bukashin <tbotc@yandex.ru>
%%% @date 22.05.2018
%%% @issue RP-600
%%% @doc Play of numerals component.
%%% @todo @ivr

-module(r_sip_ivr_script_component_playnum).
-author(['Pavel Abramov','Peter Bukashin <tbotc@yandex.ru>']).

%%-export([init/1,
%%         handle_event/2,
%%         terminate/1,
%%         metadata/1]).
%%
%%%% ====================================================================
%%%% Defines
%%%% ====================================================================
%%
%%-include("../../../../../../era_script/include/r_script.hrl").
%%
%%-define(PLAY, r_sip_ivr_script_component_play).
%%-define(TIMEOUT, 120).
%%-define(COMMON, "common").
%%-define(FOLDER, "standardexpressions").
%%
%%-record(playnum,
%%        {
%%         component_play = undefined,
%%         voice = <<>>
%%        }).
%%
%%%% ====================================================================
%%%% Callback functions
%%%% ====================================================================
%%
%%metadata(_ScriptType) ->
%%    [{<<"playerId">>, #{type => <<"argument">> }},
%%     {<<"number">>, #{type => <<"argument">>}},
%%     {<<"lang">>, #{type => <<"list">>,
%%                    items => [{0,<<"ru">>},
%%                              {1,<<"en">>}] }}, % removed "default => 0" not effective here
%%     {<<"mode">>, #{type => <<"list">>,
%%                    items => [{0,<<"byformat">>},
%%                              {1,<<"quantity">>},
%%                              {2,<<"ordinal">>},
%%                              {3,<<"phone">>},
%%                              {4,<<"date">>},
%%                              {5,<<"time">>},
%%                              {6,<<"datetime">>},
%%                              {7,<<"weekday">>},
%%                              {8,<<"timewo0b">>},
%%                              {9,<<"timewo0e">>},
%%                              {10,<<"timesec">>},
%%                              {15,<<"timeminutes">>},
%%                              {11,<<"moneyusd">>},
%%                              {12,<<"moneyeur">>},
%%                              {13,<<"moneyrub">>},
%%                              {14,<<"percent">>}] }}, % removed "default => 1" not effective here, changed 0 to 1 in r_script_metadata_translator_cprops_spec.erl
%%     {<<"format">>, #{type => <<"list">>,
%%                    items => [{0,<<"sym1">>},
%%                              {1,<<"sym2">>},
%%                              {2,<<"sym3">>},
%%                              {3,<<"number">>},
%%                              {4,<<"letters">>},
%%                              {5,<<"words">>}],
%%                    filter => <<"mode==0">> }}, % removed "default => 0" not effective here
%%     {<<"gender">>, #{type => <<"list">>,
%%                    items => [{0,<<"masculine">>},
%%                              {1,<<"feminine">>},
%%                              {2,<<"neuter">>}],
%%                    filter => <<"mode in (0,1,2)">> }}, % removed "default => 0" not effective here
%%     {<<"downTone">>, #{type => <<"list">>,
%%                    items => [{0,<<"no">>},
%%                              {1,<<"yes">>}] }}, % removed "default => 0" not effective here
%%     {<<"voice">>, #{type => <<"clist">>,
%%                          key => <<"txtnumvoices">>}},
%%     {<<"interruptSymbols">>, #{type => <<"string">>}}, % removed "default => null" not effective here
%%     {<<"transfer">>, #{type => <<"transfer">> }},
%%     {<<"transferError">>, #{type => <<"transfer">>,
%%                             title => <<"error">> }}].
%%
%%%% --------------------------------------------------------------------
%%%%
%%%% --------------------------------------------------------------------
%%
%%init(#state_scr{active_component=#component{data=Data}=Comp}=State) ->
%%    PlayerId = ?SCR_ARG:get_string(<<"playerId">>, Data, <<"default">>, State),
%%    Number = ?SCR_ARG:get_string(<<"number">>, Data, <<>>, State),
%%    Voice = ?SCR_COMP:get(<<"voice">>, Data, <<>>),
%%    InterruptSymbols = ?SCR_COMP:get(<<"interruptSymbols">>, Data, <<>>),
%%    ?SCR_TRACE(State, "Number: ~500tp, Voice: ~500tp.",[Number, Voice]),
%%    case (Number == <<>>) or (Voice == <<>>) of
%%        true -> ?SCR_COMP:next(<<"transferError">>,State);
%%        false ->
%%            Opts = parse_opts(Data),
%%            case ?ENVTXTNUM:build(Number,Opts) of
%%                [] -> ?SCR_COMP:next(<<"transferError">>,State);
%%                Result ->
%%                    Str = string:join(lists:map(fun(X) -> ?EU:to_unicode_list(X) end, Result), " "),
%%                    ?SCR_TRACE(State, "Text: ~500tp",[Str]),
%%                    case check_files(Result, Voice, State) of
%%                        {ok, Files} ->
%%                            case create_component_play(205, PlayerId, InterruptSymbols, Files, State#state_scr{active_component=Comp#component{state=#playnum{voice=Voice}}}) of
%%                                {error, _} ->
%%                                    ?SCR_TRACE(State, "Component play init error."),
%%                                    ?SCR_COMP:next(<<"transferError">>,State);
%%                                Return -> Return
%%                            end;
%%                        {error, _} ->
%%                            ?SCR_TRACE(State, "Error: Files not found."),
%%                            ?SCR_COMP:next(<<"transferError">>,State)
%%                    end
%%            end
%%    end.
%%
%%
%%%% --------------------------------------------------------------------
%%%%
%%%% --------------------------------------------------------------------
%%handle_event(Event, #state_scr{active_component=#component{state=#playnum{component_play=CompPlay}=PN}=Comp}=State) ->
%%    case ?PLAY:handle_event(Event, State#state_scr{active_component=CompPlay}) of
%%        {ok,  #state_scr{active_component=UpCompPlay}} -> {ok, State#state_scr{active_component=Comp#component{state=PN#playnum{component_play=UpCompPlay}}}};
%%        {next, Id, _} ->
%%            case (Id < 0) of
%%                true ->
%%                    ?SCR_TRACE(State, "Component play event error."),
%%                    ?SCR_COMP:next(<<"transferError">>, State#state_scr{active_component=Comp#component{state=PN#playnum{component_play=undefined}}});
%%                false -> ?SCR_COMP:next(<<"transfer">>, State#state_scr{active_component=Comp#component{state=PN#playnum{component_play=undefined}}})
%%            end
%%    end;
%%
%%handle_event(_Event, State) ->
%%    {ok,State}.
%%
%%%% --------------------------------------------------------------------
%%%%
%%%% --------------------------------------------------------------------
%%terminate(#state_scr{active_component=#component{state=#playnum{component_play=undefined}}}=State) ->
%%    {ok, State};
%%terminate(#state_scr{active_component=#component{state=#playnum{component_play=CompPlay}}}=State) ->
%%    ?PLAY:terminate(State#state_scr{active_component=CompPlay}),
%%    {ok, State};
%%terminate(State) ->
%%    %?LOG("~s:terminate(CompState = ~tp)", [?MODULE, CompState]), % saw CompState=undefined; didn't have time to find out where it comes from, though.
%%     {ok, State}.
%%
%%
%%%% ====================================================================
%%%% Internal functions
%%%% ====================================================================
%%parse_opts(Data) ->
%%    Lang = case ?SCR_COMP:get(<<"lang">>, Data, 0) of 0 -> ru; 1 -> en end,
%%    Mode = case ?SCR_COMP:get(<<"mode">>, Data, 1) of
%%               0 -> byformat;
%%               1 -> quantity;
%%               2 -> ordinal;
%%               3 -> phone;
%%               4 -> date;
%%               5 -> time;
%%               6 -> datetime;
%%               7 -> weekday;
%%               8 -> timewo0b;
%%               9 -> timewo0e;
%%               10 -> timesec;
%%               11 -> moneyusd;
%%               12 -> moneyeur;
%%               13 -> moneyrub;
%%               14 -> percent;
%%               15 -> timeminutes
%%           end,
%%    Format = case ?SCR_COMP:get(<<"format">>, Data, 0) of
%%                 0 -> sym1;
%%                 1 -> sym2;
%%                 2 -> sym3;
%%                 3 -> number;
%%                 4 -> letters;
%%                 5 -> words
%%             end,
%%    Gender = case ?SCR_COMP:get(<<"gender">>, Data, 0) of 0 -> masculine; 1 -> feminine; 2 -> neuter end,
%%    DownTone = case ?SCR_COMP:get(<<"downTone">>, Data, 0) of 0 -> false; 1 -> true end,
%%    #{lang => Lang, mode => Mode, format => Format, gender => Gender, downTone => DownTone}.
%%
%%%% --------------------------------------
%%%%
%%%% --------------------------------------
%%check_files(Result, Author, State) ->
%%    SyncPath = ?ENVFILE:get_folder_type_path(?ENVFILE:get_path_type_atom(":SYNC")),
%%    Path = filename:join([SyncPath, ?COMMON, ?FOLDER, Author]),
%%    F = fun(Filename) -> check_file(Filename, Path, Author, State) end,
%%    case lists:filtermap(F, Result) of
%%        [] -> {error, <<"Files not found.">>};
%%        Files -> {ok, Files}
%%    end.
%%check_file(Filename, Path, Author, State) ->
%%    FilePath = filename:join([Path,Filename]),
%%    FilePathExt = <<FilePath/binary, ".wav">>,
%%    File = <<":SYNC_COMMON/", ?FOLDER, $/, Author/binary, $/, Filename/binary, ".wav">>,
%%    case ?ENVFILE:file_exists(FilePathExt) of
%%        {ok,true} -> {true, File};
%%        _ -> case binary:last(Filename) of
%%                     $_ -> check_file(binary:part(Filename, 0, byte_size(Filename) - 1), Path, Author, State);
%%                     _ ->
%%                         ?SCR_TRACE(State, "Error: not found ~500tp",[File]),
%%                         false
%%                 end
%%    end.
%%
%%%% --------------------------------------
%%%%
%%%% --------------------------------------
%%create_component_play(Id, PlayerId, InterruptSymbols, Files, #state_scr{active_component=#component{state=PN}=Comp}=State) ->
%%    CompData = [{<<"oId">>,Id},{<<"oType">>,204},{<<"name">>,<<"play from component playnum">>},
%%                {<<"playerId">>, PlayerId},
%%                {<<"mode">>, 7},
%%                {<<"filesArg">>, [{<<"argType">>,32},{<<"template">>,jsx:encode(Files)}]},
%%                {<<"interruptSymbols">>, InterruptSymbols},
%%                {<<"transfer">>,Id},
%%                {<<"transferError">>,-Id}],
%%    CompPlay = ?SCR_COMP:build_component(CompData,State),
%%    case ?PLAY:init(State#state_scr{active_component=CompPlay}) of
%%        {ok, #state_scr{active_component=UpCompPlay}} ->
%%            {ok, State#state_scr{active_component=Comp#component{state=PN#playnum{component_play=UpCompPlay}}}};
%%        _ -> {error, <<"Error init component play.">>}
%%    end.
%%
