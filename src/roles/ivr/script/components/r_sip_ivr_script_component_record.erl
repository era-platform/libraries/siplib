%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 23.05.2016
%%% @doc
%%% @todo @ivr

-module(r_sip_ivr_script_component_record).
-author('Peter Bukashin <tbotc@yandex.ru>').

%%-compile([export_all,nowarn_export_all]).
%%-export([init/1,
%%         handle_event/2,
%%         metadata/1]).

%% ====================================================================
%% Defines
%% ====================================================================

%-include("../include/r_script.hrl").
%%-include("../../era_script/include/r_script.hrl").
%%
%%-include("../include/r_sip_ivr.hrl").
%%-include("../include/r_sip_nk.hrl").
%%
%%-define(U, r_sip_utils).
%%-define(CFG, r_sip_config).
%%
%%-define(CURRSTATE, 'active').
%%
%%-define(TIMEOUT, 10000).
%%
%%-record(rec, {
%%              state :: wait_ticket | recording | stopping,
%%              pid,
%%              ref,
%%              timestart,
%%              timer_ref,
%%              recorder_id,
%%              mode = 0,
%%              tempfile = <<>>,
%%              codec = 0,
%%              rectime = 0,
%%              maxcount = 0,
%%              interrupts = [],
%%              buffer = <<>>,
%%              clear_buffer = true,
%%              clear_interrupt = true,
%%              abort_on_silence :: boolean(),
%%              silence_timeout :: integer(),
%%              vad_threshold :: integer(),
%%              vad_state :: up | down,
%%              silence_timer_ref :: reference(),
%%              stopmode = undefined,
%%              media_gate_id = undefined
%%             }).
%%
%%-define(S_WAIT_TICKET, wait_ticket).
%%-define(S_RECORDING, recording).
%%-define(S_STOPPING, stopping).
%%-define(S_PACKING, packing).
%%
%%-define(VadEvtID, 1239).
%%-define(RtxVadDur, 30).
%%-define(RtxSilenceDur, 500).
%%
%%-define(M_SYNC_REC_FILE, sync_rec_file).
%%
%%%% ====================================================================
%%%% Callback functions
%%%% ====================================================================
%%
%%% returns descriptions of used properties
%%metadata(_ScriptType) ->
%%    [%{<<"recorderId">>, #{type => <<"argument">> }},
%%     {<<"mode">>, #{type => <<"list">>,
%%                    items => [{0,<<"syncRecFile">>}] }}, % removed "default => 0" not effective here
%%     {<<"pathToVarMode">>, #{type => <<"list">>,
%%                             items => [{0,<<"no">>},
%%                                       {1,<<"yes">>}] }}, % removed "default => 1" not effective here
%%     {<<"nameVar">>, #{type => <<"variable">>}},
%%     {<<"codec">>,  #{type => <<"list">>,
%%                      items => [{0,<<"pcm">>},
%%                                {1,<<"pcma">>},
%%                                {2,<<"pcmu">>},
%%                                {3,<<"gsm">>},
%%                                {4,<<"mp3_8">>},
%%                                {5,<<"mp3_16">>},
%%                                {6,<<"mp3_24">>}] }}, % removed "default => 1" not effective here
%%     {<<"recTimeSec">>, #{type => <<"argument">>,
%%                          filter => <<"mode==0">> }},
%%     {<<"dtmfBuffer">>, #{type => <<"variable">>,
%%                          filter => <<"mode == 0">> }},
%%     {<<"clearDtmfBuffer">>, #{type => <<"list">>,
%%                               items => [{0,<<"no">>},
%%                                         {1,<<"yes">>}],
%%                               filter => <<"(mode == 0) && (dtmfBuffer!=null)">> }}, % removed "default => 1" not effective here
%%     {<<"maxSymbolCount">>, #{type => <<"argument">> }},
%%     {<<"interruptSymbols">>, #{type => <<"string">>,
%%                                filter => <<"mode == 0">> }}, % removed "default => <<>>" not effective here
%%     {<<"clearInterrupt">>, #{type => <<"list">>,
%%                              items => [{0,<<"no">>},
%%                                        {1,<<"yes">>}],
%%                              filter => <<"(mode == 0) && (interruptSymbols!=null) && (interruptSymbols!=\"\")">> }}, % removed "default => 1" not effective here
%%     {<<"abortOnSilence">>, #{type => <<"list">>,
%%                              items => [{0,<<"no">>},
%%                                        {1,<<"yes">>}] }},
%%     {<<"silenceTimeoutSec">>, #{type => <<"argument">>,
%%                                 filter => <<"abortOnSilence==1">> }},
%%     {<<"vadThreshold">>, #{type => <<"argument">>,
%%                            filter => <<"abortOnSilence==1">> }},
%%     {<<"transfer">>, #{type => <<"transfer">> }},
%%     {<<"transferTimeout">>, #{type => <<"transfer">>,
%%                               title => <<"timeout">> }} ].
%%
%%% ---------------------------------------------------------------------
%%init(#state_scr{ownerpid=undefined}=State) ->
%%    ?SCR_COMP:next([<<"transferError">>, <<"transfer">>], State);
%%init(#state_scr{scriptid=IvrId, ownerpid=OwnerPid, active_component=#component{data=CompData}=Comp}=State) ->
%%    % properties
%%    {TS,RecPath} = ?U:get_media_context_rec_opts(),
%%    CS = #rec{recorder_id = ?SCR_ARG:get_string(<<"recorderId">>, CompData, default, State),
%%              timestart = os:timestamp(),
%%              mode = mode(?SCR_COMP:get(<<"mode">>, CompData, 0)),
%%              %tempfile = ?EU:str("~s/ivrr_~s_~s.rtp", [RecPath, ?EU:to_list(IvrId), ?EU:to_list(TS)]),
%%              %tempfile = filename:join([":RECORD", RecPath, ?EU:str("ivrr_~s_~s.rtp", [?EU:to_list(IvrId), ?EU:to_list(TS)])]),
%%              tempfile = filename:join([RecPath, ?EU:str("ivrr_~s_~s.rtp", [?EU:to_list(IvrId), ?EU:to_list(TS)])]),
%%              codec = ?SCR_COMP:get(<<"codec">>, CompData, 1),
%%              rectime = ?SCR_ARG:get_int(<<"recTimeSec">>, CompData, 0, State) * 1000,
%%              maxcount = ?SCR_ARG:get_int(<<"maxSymbolCount">>, CompData, 0, State),
%%              interrupts = ?IVR_SCRIPT_UTILS:parse_interrupts(<<"interruptSymbols">>, CompData),
%%              clear_buffer = ?EU:to_bool(?SCR_COMP:get(<<"clearDtmfBuffer">>, CompData, 1)),
%%              clear_interrupt = ?EU:to_bool(?SCR_COMP:get(<<"clearInterrupt">>, CompData, 0)),
%%              abort_on_silence = ?EU:to_bool(?SCR_COMP:get(<<"abortOnSilence">>, CompData, 0)),
%%              silence_timeout = max(1, ?SCR_ARG:get_int(<<"silenceTimeoutSec">>, CompData, 2, State)) * 1000 - ?RtxSilenceDur,
%%              vad_threshold = max(20, min(60, ?SCR_ARG:get_int(<<"vadThreshold">>, CompData, 30, State))),
%%              vad_state=down},
%%    % @debug
%%    %CS = CS_#rec{mode = mode(0), rectime=10000, interrupts = [<<"*">>]},
%%    % init
%%    CmdRef = make_ref(),
%%    {Cmd,OptsMap} = make_cmd_opts(CS),
%%    OwnerPid ! {scriptmachine, {ivr_cmd, CmdRef, {ext, ?MODULE, Cmd, self(), OptsMap}}},
%%    TimerRef1 = ?SCR_COMP:event_after(?TIMEOUT, {'timeout_state',CmdRef,S=?S_WAIT_TICKET}),
%%    {ok, State#state_scr{active_component=Comp#component{state=CS#rec{state = S,
%%                                                                      ref = CmdRef,
%%                                                                      pid = self(),
%%                                                                      timer_ref = TimerRef1,
%%                                                                      buffer = <<>>}}}}.
%%
%%% ---------------------------------------
%%%% start (attach recorder term)
%%
%%% timeout of wait_ticket
%%handle_event({timeout_state,Ref,S}, #state_scr{active_component=#component{state=#rec{state=S=?S_WAIT_TICKET,ref=Ref}}}=State) ->
%%    Err = {error,{timeout,<<"Ticket timeout">>}},
%%    fin(Err, State);
%%
%%% ticket mgc error (s = wait_ticket)
%%handle_event({ticket,CmdRef,{error,_}=Err}, #state_scr{active_component=#component{state=#rec{state=?S_WAIT_TICKET,ref=CmdRef,timer_ref=TimerRef}}}=State) ->
%%    %?OUT("Ticket ~500tp", [Err]),
%%    erlang:cancel_timer(TimerRef),
%%    fin(Err, State);
%%
%%handle_event({ticket,CmdRef,{ok,started,MGID}}, #state_scr{active_component=#component{state=#rec{state=?S_WAIT_TICKET,ref=CmdRef,timer_ref=TimerRef}=CS}=Comp}=State) ->
%%    %?OUT("Ticket ok"),
%%    erlang:cancel_timer(TimerRef),
%%    T = case CS#rec.rectime of
%%            N when N>0 -> N;
%%            _ -> 3600000
%%        end,
%%    case CS of
%%        #rec{mode=sync_rec_file} ->
%%            TimerRef1 = ?SCR_COMP:event_after(T, {'timeout_state',CmdRef,S1=?S_RECORDING}),
%%            {ok, State#state_scr{active_component=Comp#component{state=CS#rec{state=S1,
%%                                                                              timer_ref=TimerRef1,
%%                                                                              media_gate_id=MGID}}}}
%%    end;
%%
%%%% --------------------------
%%%% dtmf events
%%
%%% dtmf from mg (rfc2833, inband)
%%handle_event({mg_dtmf,Dtmf}, State) ->
%%    dtmf(Dtmf, State);
%%
%%% dtmf from sip (info)
%%handle_event({sip_info,Req}, State) ->
%%    ?U:get_dtmf_from_sipinfo(Req, fun(Dtmf) -> dtmf(Dtmf,State) end, fun() -> {ok,State} end);
%%
%%%% ---------------------------------
%%%% vad from mg
%%handle_event({vad_threshold,CtxTermId,Event}, State) ->
%%    apply_vad({vad_threshold,CtxTermId,Event}, State);
%%
%%%% vad silence timeout
%%handle_event({silence_timeout,CmdRef,S}, #state_scr{active_component=#component{state=#rec{state=S=?S_RECORDING,ref=CmdRef}=_CS}}=State) ->
%%    stop_by_vad(State);
%%
%%%% --------------------------
%%%% rec timeout
%%
%%% timeout of recording
%%handle_event({timeout_state,CmdRef,S}, #state_scr{active_component=#component{state=#rec{state=S=?S_RECORDING,ref=CmdRef}=CS}=Comp}=State) ->
%%    State1 = State#state_scr{active_component=Comp#component{state=CS#rec{stopmode=timeout}}},
%%    exec_stop_record('timeout_state', State1);
%%
%%%% --------------------------
%%%% stop (detach record term)
%%
%%% timeout of stopping
%%handle_event({timeout_state,Ref,S}, #state_scr{active_component=#component{state=#rec{state=S=?S_STOPPING,ref=Ref}}}=State) ->
%%    Err = {error, {timeout, <<"Ticket timeout">>}},
%%    fin(Err, State);
%%
%%handle_event({async_timeout_state,Ref,S}, #state_scr{active_component=#component{state=#rec{state=S=?S_STOPPING,ref=Ref}}}=State) ->
%%    check_end_record(State);
%%
%%% ticket mgc error (s = stopping)
%%handle_event({ticket,CmdRef,{error,_}=Err}, #state_scr{active_component=#component{state=#rec{ref=CmdRef,timer_ref=TimerRef}}}=State) ->
%%    %?OUT("Ticket error"),
%%    erlang:cancel_timer(TimerRef),
%%    fin(Err, State);
%%
%%% ticket ok (s = stopping)
%%handle_event({ticket,CmdRef,{ok,stopped}}, #state_scr{active_component=#component{state=#rec{ref=CmdRef, timer_ref=TimerRef}}}=State) ->
%%    %?OUT("Ticket ok stopped"),
%%    erlang:cancel_timer(TimerRef),
%%    new_process_mix_file(State);
%%
%%%% --------------------------
%%%% mixer (download rtp, mix, upload wav/mp3)
%%
%%% timeout of packing
%%handle_event({timeout_state,Ref,S}, #state_scr{active_component=#component{state=#rec{state=S=?S_PACKING,ref=Ref}}}=State) ->
%%    Err = {error, {timeout, <<"Packing timeout">>}},
%%    fin(Err, State);
%%
%%% mix result ok
%%handle_event({mix_result,CmdRef,{ok,FilePath}}, #state_scr{active_component=#component{state=#rec{ref=CmdRef, timer_ref=TimerRef, stopmode=StopMode}}}=State) ->
%%    %?OUT("mix_result ok"),
%%    erlang:cancel_timer(TimerRef),
%%    case put_file(FilePath, State) of
%%        {error,_}=Err -> fin(Err,State);
%%        {ok,State1} -> fin(StopMode, State1)
%%    end;
%%
%%% mix result error
%%handle_event({mix_result,CmdRef,{error,ErrReason}=Err}, #state_scr{active_component=#component{state=#rec{ref=CmdRef, timer_ref=TimerRef}}}=State) ->
%%    ?OUT("mix_result err ~500tp", [ErrReason]),
%%    erlang:cancel_timer(TimerRef),
%%    fin(Err, State);
%%
%%%% --------------------------
%%%% other
%%
%%handle_event(_Ev, State) ->
%%    {ok, State}.
%%
%%% ---------------------------------------
%%
%%% terminate component externally
%%terminate(#state_scr{ownerpid=undefined,active_component=#component{state=#rec{state=?S_RECORDING,timer_ref=TimerRef}}}=State) ->
%%    erlang:cancel_timer(TimerRef),
%%    {ok, State};
%%terminate(#state_scr{active_component=#component{state=#rec{state=?S_RECORDING,timer_ref=TimerRef}}}=State) ->
%%    erlang:cancel_timer(TimerRef),
%%    {ok,State1} = exec_stop_record('async_timeout_state',State),
%%    {async, State1};
%%terminate(State) ->
%%    {ok, State}.
%%
%%%% ====================================================================
%%%% Functional iface of SIP IVR dlg (linked to its fsm)
%%%% ====================================================================
%%
%%%% -------------------------------
%%%% start recorder
%%%% -------------------------------
%%
%%cmd_start_record({_Ref, _FromPid, _Arg}=P, #state_ivr{a=#side{state=S}}=State) when S==dialog; S==early ->
%%    do_cmd_start_record(P, #state_ivr{a=#side{state=S}}=State);
%%cmd_start_record({Ref, FromPid, _Arg}, #state_ivr{}=State) ->
%%    ?SCR_COMP:event(FromPid, {'ticket',Ref,{error,{invalid_operation,<<"Record could not be started, state mismatch">>}}}),
%%    {next_state, ?CURRSTATE, State}.
%%
%%% @private
%%do_cmd_start_record({Ref, FromPid, Arg}, State) when is_map(Arg) ->
%%    Opts = Arg,
%%    case enable_vad(Opts, State) of
%%        {ok,State1} ->
%%            case ?IVR_MEDIA:media_attach_recorder(Opts, State1) of
%%                {error,_}=Err ->
%%                    ?SCR_COMP:event(FromPid, {'ticket',Ref,Err}),
%%                    {next_state, ?CURRSTATE, State1};
%%                {ok, State2} ->
%%                    MGID = case ?IVR_MEDIA:get_current_media_link(State2) of
%%                               {ok, #{}=M} -> maps:get(mgid,M,undefined);
%%                               _ -> undefined
%%                           end,
%%                    ?SCR_COMP:event(FromPid, {'ticket',Ref,{ok,started,MGID}}),
%%                    {next_state, ?CURRSTATE, State2}
%%            end;
%%        {error,_}=Err ->
%%            ?SCR_COMP:event(FromPid, {'ticket',Ref,Err}),
%%            {next_state, ?CURRSTATE, State}
%%    end.
%%
%%%% @private
%%%% executed in IVR-FSM pid
%%enable_vad(Opts, #state_ivr{}=State) ->
%%    case maps:get(vad,Opts) of
%%        false -> {ok,State};
%%        _ ->
%%            VADThreshold = maps:get(vad_threshold,Opts),
%%            Events = {?VadEvtID, [{"vdp/vad", [{"vthres",VADThreshold},{"vad_min_duration",?RtxVadDur},{"silence_min_duration",?RtxSilenceDur}] }] },
%%            case ?IVR_MEDIA:modify_term_events(Events, State) of
%%                {ok,State1} -> {ok,State1};
%%                {error,_}=Err -> Err
%%            end end.
%%
%%%% -------------------------------
%%%% stop recorder
%%%% -------------------------------
%%
%%cmd_stop_record({_Ref, _FromPid, _Arg}=P, #state_ivr{a=#side{state=S}}=State) when S==dialog; S==early; S==bye ->
%%    do_cmd_stop_record(P, #state_ivr{a=#side{state=S}}=State);
%%cmd_stop_record({Ref, FromPid, _Arg}, #state_ivr{}=State) ->
%%    ?SCR_COMP:event(FromPid, {'ticket',Ref,{error,{invalid_operation,<<"Record could not be stopped, state mismatch">>}}}),
%%    {next_state, ?CURRSTATE, State}.
%%
%%% @private
%%do_cmd_stop_record({Ref, FromPid, Arg}, State) when is_map(Arg) ->
%%    Opts = Arg,
%%    case ?IVR_MEDIA:media_detach_recorder(Opts, State) of
%%        {error,_}=Err ->
%%            ?SCR_COMP:event(FromPid, {'ticket',Ref,Err}),
%%            {next_state, ?CURRSTATE, State};
%%        {ok, State1} ->
%%            State3 = case disable_vad(Opts,State1) of
%%                         {error,_}=Err -> ?SCR_COMP:event(FromPid, {'ticket',Ref,Err}), State1;
%%                         {ok,State2} -> ?SCR_COMP:event(FromPid, {'ticket',Ref,{ok,stopped}}), State2
%%                     end,
%%            {next_state, ?CURRSTATE, State3}
%%    end.
%%
%%%% @private
%%%% executed in IVR-FSM pid
%%disable_vad(Opts, #state_ivr{}=State) ->
%%    case maps:get(vad,Opts) of
%%        false -> {ok,State};
%%        _ ->
%%            Events = {?VadEvtID, [{"vdp/empty", [] }] }, % {0, []},
%%            case ?IVR_MEDIA:modify_term_events(Events, State) of
%%                {ok,State1} -> {ok,State1};
%%                {error,_}=Err -> Err
%%            end end.
%%
%%%% ====================================================================
%%%% Internal functions
%%%% ====================================================================
%%
%%%% ---------------
%%%% record
%%%% ---------------
%%
%%% @private
%%mode(0) -> ?M_SYNC_REC_FILE;
%%mode(Mode) when is_atom(Mode) -> Mode.
%%
%%% @private
%%make_cmd_opts(#rec{recorder_id=RecorderId}=CS) ->
%%    O = #{recorderid => RecorderId},
%%          make_cmd_opts_1(CS, O).
%%
%%%
%%make_cmd_opts_1(#rec{mode=M}=CS, O)
%%  when M==sync_rec_file ->
%%    #rec{abort_on_silence=SilenceVAD,
%%         vad_threshold=VADThreshold,
%%         tempfile=File}=CS,
%%    O1 = O#{file => File,
%%            vad => SilenceVAD,
%%            vad_threshold => VADThreshold},
%%    {cmd_start_record, O1}.
%%
%%% exec stop record command
%%exec_stop_record(TimeoutState, #state_scr{active_component=#component{state=#rec{state=?S_RECORDING}=CS}=Comp}=State) ->
%%    #rec{ref=CmdRef,timer_ref=TimerRef,recorder_id=RecorderId}=CS,
%%    erlang:cancel_timer(TimerRef),
%%    #state_scr{ownerpid=OwnerPid}=State,
%%    OwnerPid ! {scriptmachine, {ivr_cmd, CmdRef, {ext, ?MODULE, cmd_stop_record, self(), #{recorderid => RecorderId,
%%                                                                                           vad => CS#rec.abort_on_silence}}}},
%%    Time = case TimeoutState of
%%               async_timeout_state -> 500;
%%               _ -> ?TIMEOUT
%%           end,
%%    TimerRef1 = ?SCR_COMP:event_after(Time, {TimeoutState,CmdRef,S1=?S_STOPPING}),
%%    {ok, State#state_scr{active_component=Comp#component{state=CS#rec{state=S1,
%%                                                                      timer_ref=TimerRef1,
%%                                                                      recorder_id=RecorderId}}}};
%%exec_stop_record(_TimeoutState, State) ->
%%    {ok, State}.
%%
%%%% ---------------
%%%% dtmf interrupt
%%%% ---------------
%%
%%% normalize dtmf symbols
%%normalize(X) -> ?U:normalize_dtmf(X).
%%
%%% dtmf received
%%dtmf(Dtmf, #state_scr{active_component=#component{state=CS}=Comp}=State) ->
%%    Dtmf1 = normalize(Dtmf),
%%    #rec{buffer=Buffer}=CS,
%%    State1 = State#state_scr{active_component=Comp#component{state=CS#rec{buffer= <<Buffer/bitstring,Dtmf1/bitstring>>}}},
%%    check_interrupt(State1).
%%
%%% check if complete by interrupt
%%check_interrupt(#state_scr{active_component=#component{state=CS}=Comp}=State) ->
%%    #rec{buffer=Buffer,
%%         interrupts=Interrupts}=CS,
%%    Length = size(Buffer),
%%    F = fun(Interrupt, false) ->
%%                case binary:matches(Buffer, Interrupt) of
%%                    [] -> false;
%%                    M ->
%%                        {Pos,Len} = lists:last(M),
%%                        case Pos+Len of
%%                            Length ->
%%                                case CS#rec.clear_interrupt of
%%                                    false -> {true,Buffer};
%%                                    true ->
%%                                        % clear interrupt symbols
%%                                        Bits = Pos*8,
%%                                        <<Clear:Bits/bitstring,_/bitstring>> = Buffer,
%%                                        {true,Clear}
%%                                end;
%%                            _ -> false
%%                        end
%%                end;
%%           (_,Acc) -> Acc
%%        end,
%%    case lists:foldl(F, false, Interrupts) of
%%        {true,Res} -> stop_by_dtmf(interrupt, State#state_scr{active_component=Comp#component{state=CS#rec{buffer=Res}}});
%%        false -> check_length(State)
%%    end.
%%
%%% check if complete by max length
%%check_length(#state_scr{active_component=#component{state=CS}}=State) ->
%%    #rec{buffer=Buffer,
%%         maxcount=MaxSymbolCount}=CS,
%%    Length = size(Buffer),
%%    case MaxSymbolCount>0 andalso Length>=MaxSymbolCount of
%%        true -> stop_by_dtmf(length,State);
%%        false -> {ok,State}
%%    end.
%%
%%% stop record by dtmf
%%stop_by_dtmf(Mode, #state_scr{active_component=#component{state=CS}=Comp}=State) ->
%%    State1 = State#state_scr{active_component=Comp#component{state=CS#rec{stopmode=Mode}}},
%%    exec_stop_record('timeout_state',State1).
%%
%%%% ----------------
%%%% vad
%%%% ----------------
%%apply_vad({'vad_threshold', _CtxTermId, Event}, #state_scr{}=State) ->
%%    case maps:get(parname,Event,undefined) of
%%        "state" ->
%%            #rec{vad_state=VAD,
%%                 abort_on_silence=SilenceVAD,
%%                 silence_timer_ref=TimerRef,
%%                 silence_timeout=Timeout,
%%                 ref=Ref}=CS = get_data(State),
%%            case maps:get(parvalue,Event) of
%%                "up" when VAD/='up' ->
%%                    %?OUT("VAD UP ~120tp", [_CtxTermId, Event]),
%%                    ?EU:cancel_timer(TimerRef),
%%                    {ok, set_data(CS#rec{silence_timer_ref=undefined,
%%                                         vad_state=up}, State)};
%%                "down" when VAD/='down' andalso SilenceVAD ->
%%                    %?OUT("VAD DOWN ~120tp", [_CtxTermId, Event]),
%%                    {ok, set_data(CS#rec{silence_timer_ref=?SCR_COMP:event_after(Timeout,self(),{'silence_timeout',Ref,?S_RECORDING}),
%%                                         vad_state=down}, State)};
%%                _ -> {ok,State}
%%            end;
%%        _ ->
%%            %?OUT("VAD MISMATCH: ~120tp, ~120tp", [_CtxTermId, Event]),
%%            {ok, State}
%%    end.
%%
%%%% @private
%%get_data(#state_scr{active_component=#component{state=CS}}) -> CS.
%%
%%%% @private
%%set_data(CS, #state_scr{active_component=Comp}=State) -> State#state_scr{active_component=Comp#component{state=CS}}.
%%
%%%% stop record by vad
%%stop_by_vad(#state_scr{active_component=#component{state=CS}=Comp}=State) ->
%%    State1 = State#state_scr{active_component=Comp#component{state=CS#rec{stopmode=vad}}},
%%    exec_stop_record('timeout_state',State1).
%%
%%%% ----------------
%%%% mixer (spawned)
%%%% ----------------
%%
%%% @spawned
%%do_mix_file(MGID, MG_TempFile, LocalPath, CS) ->
%%    R = case copy_rtp(MGID, MG_TempFile, LocalPath) of
%%            false -> {error, <<"copy rtp error">>};
%%            {ok,RtpPath} ->
%%                mix_rtp(RtpPath, CS)
%%        end,
%%    #rec{ref=CmdRef, pid=Pid}=CS,
%%    ?SCR_COMP:event(Pid, {mix_result,CmdRef,R}).
%%
%%%% get_cwd() ->
%%%%     case ?Rfile:get_cwd() of
%%%%         {ok, Path} -> Path;
%%%%         _ -> []
%%%%     end.
%%
%%% @spawned copies rtp file from mgc to local (in spawned proc)
%%copy_rtp(MGID, MG_TempFile, LocalPath) ->
%%    case ?U:parse_mg_devicename(?U:get_mg_devicename(MGID)) of
%%        undefined -> false;
%%        {Addr,Postfix} ->
%%            case ?CFG:get_mg_node_by_addr_postfix(Addr,Postfix) of
%%                undefined -> false;
%%                {Site,Node} ->
%%                    %?OUT("~500tp : Addr: ~120tp, Postfix: ~120tp, Site: ~120tp, Node: ~120tp", [?MODULE, Addr,Postfix,Site,Node]),
%%                    RecPath = ?CFG:get_record_path(Addr,Postfix),
%%                    SrcPath = filename:join([RecPath|lists:nthtail(1,filename:split(MG_TempFile))]),
%%                    DestPath = filename:join([LocalPath, lists:last(string:tokens(MG_TempFile,"/"))]),
%%                    %?OUT("~500tp : copy_rtp ~500tp  ~500tp", [?MODULE, DestPath, SrcPath]),
%%                    R = ?ENVCOPY:copy_file({Site,Node}, SrcPath, DestPath),
%%                    ?ENVCROSS:call_node({Site,Node}, {?Rfile, delete, [SrcPath]}, false),
%%                    case R of
%%                        {error,_}=Err ->
%%                            ?LOG("Error copying ~500tp", [Err]),
%%                            false;
%%                        ok ->
%%                            case ?Rfilelib:is_file(DestPath) of
%%                                false -> false;
%%                                true -> {ok,DestPath}
%%                            end end end end.
%%
%%% mix locally (in spawned proc)
%%mix_rtp(FPath, #rec{codec=Codec}) ->
%%    Tokens = filename:split(FPath),
%%    Name = lists:foldl(fun(_,Acc) -> lists:droplast(Acc) end, lists:last(Tokens), lists:seq(1,4)),
%%    DestPath = filename:join(lists:droplast(Tokens) ++ [Name ++ extension(Codec)]),
%%    %?OUT("~500tp : mix_rtp ~500tp ~500tp", [?MODULE, FPath, DestPath]),
%%    mix_rtp_1(FPath, DestPath, Codec).
%%
%%
%%% @private
%%mix_rtp_1(SrcPath, DestPath, Codec) ->
%%    % @todo pack file to codec by local mixer (for ceceron)
%%    %?OUT("mix_rtp_1 Dest:~500tp  Src:~500tp", [?Rfilelib:ensure_dir(DestPath), ?Rfilelib:ensure_dir(SrcPath)]),
%%    Opts = [{source, [SrcPath]},
%%            {mixed, DestPath},
%%            {targetformat, codec(Codec)}],
%%    case mix_files([Opts]) of
%%        [{ok, DestPath}=Ok] ->
%%            ?Rfile:delete(SrcPath),
%%            Ok;
%%        [{error,_}=Err] ->
%%            Err
%%    end.
%%
%%% @private
%%mix_files([Opts]) ->
%%    %%  % @debug
%%    %%     SrcPath = ?EU:get_by_key(source,Opts,""),
%%    %%     DestPath = ?EU:get_by_key(mixed,Opts,""),
%%    %%     file:copy(SrcPath, DestPath),
%%    %%     [{ok, DestPath}].
%%    ?APP_MIXER:mix_files([Opts]).
%%
%%% @private
%%codec(0) -> "L16 8000 1"; % pcm mono
%%codec(1) -> "PCMA 8000 1"; % g711a mono
%%codec(2) -> "PCMU 8000 1"; % g711u mono
%%codec(3) -> "GSM 8000 1"; % gsm610 mono
%%codec(4) -> "MP3 8000 1 8000"; % mp3 8kHz 8kbit mono
%%codec(5) -> "MP3 8000 1 16000"; % mp3 8kHz 16kbin mono
%%codec(6) -> "MP3 8000 1 24000". % mp3 8kHz 24kbin mono
%%
%%% @private
%%extension(CodecType) when CodecType>=4, CodecType=<6 -> ".mp3";
%%extension(_)  -> ".wav".
%%
%%% move packed file to destination (in gen_srv proc)
%%put_file(SrcPath, #state_scr{active_component=#component{data=Data}, stopfuns=SF}=State) ->
%%    {DestPath, ScrPath} = make_destfile_path(State),
%%    %?OUT("~500tp : put_file ~500tp  ~500tp", [?MODULE, DestPath, ScrPath]),
%%    ?Rfilelib:ensure_dir(DestPath),
%%    case ?Rfile:rename(SrcPath, DestPath) of
%%        {error,_R}=Err ->
%%            %?OUT("~500tp : error ~500tp", [?MODULE, _R]),
%%            Err;
%%        ok ->
%%            PathMode = ?SCR_COMP:get(<<"pathToVarMode">>, Data, 0),
%%            Var = ?SCR_COMP:get(<<"nameVar">>, Data, <<>>),
%%            case PathMode of
%%                0 -> % don't save
%%                    State1 = State;
%%                1 -> % script keyed path
%%                    Value = ?EU:to_binary(ScrPath),
%%                    State1 = assign(Var, Value, State);
%%                2 -> % only file name
%%                    Value = ?EU:to_binary(?EU:get_filename(DestPath)),
%%                    State1 = assign(Var, Value, State)
%%            end,
%%            State2 = State1#state_scr{stopfuns=[fun() -> ?Rfile:delete(DestPath) end|SF]},
%%            %?OUT("~500tp : ok", [?MODULE]),
%%            {ok,State2}
%%    end.
%%
%%% build destination file path
%%make_destfile_path(#state_scr{scriptid=Id,active_component=#component{state=CS}}=State) ->
%%    #rec{timestart=TS, codec=Codec}=CS,
%%    % we make path for next component in ":TEMP/filename.ext"
%%    % and make local path for file:rename()
%%    IvrTempFolder = filename:join([?EU:to_list(?SCR_FILE:get_path_type(temp)), "ivr_records"]),
%%    LocalFolder = ?ENVFILE:get_local_path(?EU:to_list(?SCR_FILE:expand_and_check_path(?EU:to_binary(IvrTempFolder), write, State))),
%%    LId = ?EU:to_list(Id),
%%    {_,LId1} = lists:split(length(LId) - 6, LId),
%%    FileName = ?EU:str("ivr_~s_~500tp~s", [LId1,?EU:timestamp(TS),extension(Codec)]),
%%    {filename:join(LocalFolder, FileName), filename:join(IvrTempFolder, FileName)}.
%%
%%%% ----------------
%%%% finalize
%%%% ----------------
%%
%%% finalize work of component
%%% mode = error, async, interrupt, length, done
%%fin({error,_}=Err, _State) -> Err;
%%fin(Mode, #state_scr{active_component=#component{state=#rec{state=?S_RECORDING}=CS}=Comp}=State) ->
%%    {ok,State1} = exec_stop_record('timeout_state', State),
%%    fin(Mode, State1#state_scr{active_component=Comp#component{state=CS#rec{state=?S_STOPPING}}});
%%fin(Mode, #state_scr{active_component=#component{data=Data,state=CS}}=State) ->
%%    #rec{buffer=Buffer}=CS,
%%    Var = ?SCR_COMP:get(<<"dtmfBuffer">>, Data, <<>>),
%%    Prev = prev_dtmf_buffer(Var, State),
%%    State1 = assign(Var, <<Prev/bitstring, Buffer/bitstring>>, State),
%%    fin_1(Mode, State1).
%%fin_1(_, State) -> ?SCR_COMP:next(<<"transfer">>,State).
%%
%%% return previous value of variable
%%prev_dtmf_buffer(Var, #state_scr{active_component=#component{state=CS}}=State) ->
%%    case CS#rec.clear_buffer orelse ?SCR_VAR:get_value(Var, undefined, State) of
%%        true -> <<>>;
%%        undefined -> <<>>;
%%        V when is_binary(V) -> V;
%%        V when is_integer(V) -> ?EU:to_binary(V);
%%        {S,_Enc} when is_binary(S) -> S;
%%        _ -> <<>>
%%    end.
%%
%%% assign value to variable
%%assign(Var, Value, State) ->
%%    case ?SCR_VAR:set_value(Var, Value, State) of
%%        {ok, State1} -> State1;
%%        _ -> State
%%    end.
%%
%%% ---------------------------------------
%%% @private 25.01.2017 -- @abramov #61
%%% check record file end in async terminated
%%-spec check_end_record(State) -> {ok, State} when
%%      State :: #state_scr{}.
%%check_end_record(#state_scr{}=State) ->
%%    % TODO not implemented
%%    new_process_mix_file(State).
%%
%%% ---------------------------------------
%%% @private
%%-spec new_process_mix_file(State) -> {ok, State} when
%%      State :: #state_scr{}.
%%new_process_mix_file(#state_scr{selfpid=SMPid,active_component=#component{state=#rec{
%%                                                                                   ref=CmdRef, mode=sync_rec_file, tempfile=MG_TempFile, media_gate_id=MGID}=CS}=Comp}=State) ->
%%    LocalPath = ?EU:to_list(?SCR_FILE:get_script_temp_directory(SMPid)),
%%    spawn_link(fun() -> do_mix_file(MGID, MG_TempFile, LocalPath, CS) end),
%%    PackTimeout = 60000,
%%    TimerRef1 = ?SCR_COMP:event_after(PackTimeout, {'timeout_state',CmdRef,S1=?S_PACKING}),
%%    State1 = State#state_scr{active_component=Comp#component{state=CS#rec{state=S1,timer_ref=TimerRef1}}},
%%    {ok, State1};
%%new_process_mix_file(State) -> {ok, State}.
