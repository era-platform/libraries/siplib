%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 10.07.2019
%%% @doc RP-1470 TODO
%%%      IVR-component TTS (Text To Speech).
%%%      Implements integration to MRCP Server to play of sythesized speech, powered by MRCP-Server.

-module(r_sip_ivr_script_component_tts).
-author('Peter Bukashin <tbotc@yandex.ru>').

%%-compile([export_all,nowarn_export_all]).
%%-export([init/1,
%%         handle_event/2,
%%         metadata/1]).
%%
%%%% ====================================================================
%%%% Define
%%%% ====================================================================
%%
%%%-include("../include/r_script.hrl").
%%-include("../../era_script/include/r_script.hrl").
%%
%%-include("../include/r_sip_ivr.hrl").
%%-include("../include/r_sip_nk.hrl").
%%
%%-define(U, r_sip_utils).
%%
%%-record(data, {
%%    state :: wait_ticket | performing | terminating,
%%    buffer :: binary(),
%%    max_count :: integer(),
%%    interrupts :: [binary()],
%%    clear_buffer :: boolean(),
%%    clear_interrupt :: boolean(),
%%    ref :: reference(),
%%    timer_ref :: reference()
%%}).
%%
%%-define(TIMEOUT, 10000).
%%-define(TIMEOUT_PERFORMING, 5000).
%%
%%-define(CURRSTATE, 'active').
%%
%%-define(S_WAIT_TICKET, wait_ticket).
%%-define(S_PERFORMING, performing).
%%-define(S_TERMINATING, terminating).
%%
%%%% ====================================================================
%%%% Public functions
%%%% ====================================================================
%%
%%% returns descriptions of used properties
%%metadata(_ScriptType) ->
%%    [{<<"phrase">>, #{type => <<"argument">> }},
%%     {<<"dtmfBuffer">>, #{type => <<"variable">> }},
%%     {<<"clearDtmfBuffer">>, #{type => <<"list">>,
%%                               items => [{0,<<"no">>},
%%                                         {1,<<"yes">>}],
%%                               filter => <<"dtmfBuffer!=null">> }},
%%     {<<"maxSymbolCount">>, #{type => <<"argument">> }},
%%     {<<"interruptSymbols">>, #{type => <<"string">> }},
%%     {<<"clearInterrupt">>, #{type => <<"list">>,
%%                              items => [{0,<<"no">>},
%%                                        {1,<<"yes">>}]} },
%%     {<<"transfer">>, #{type => <<"transfer">> }},
%%     {<<"transferError">>, #{type => <<"transfer">>,
%%                             title => <<"error">> }} ].
%%
%%% ---------------------------------------------------------------------
%%init(#state_scr{ownerpid=undefined}=State) ->
%%    ?SCR_COMP:next([<<"transferError">>, <<"transfer">>], State);
%%init(#state_scr{ownerpid=OwnerPid, active_component=#component{data=CompData}=Comp}=State) ->
%%    % properties
%%    CS = #data{max_count = ?SCR_ARG:get_int(<<"maxSymbolCount">>, CompData, 0, State),
%%               interrupts = ?IVR_SCRIPT_UTILS:parse_interrupts(<<"interruptSymbols">>, CompData),
%%               clear_buffer = ?EU:to_bool(?SCR_COMP:get(<<"clearDtmfBuffer">>, CompData, 1)),
%%               clear_interrupt = ?EU:to_bool(?SCR_COMP:get(<<"clearInterrupt">>, CompData, 0))},
%%    % init
%%    CmdRef = make_ref(),
%%    {Cmd,OptsMap} = {cmd_start,#{}},
%%    OwnerPid ! {scriptmachine, {ivr_cmd, CmdRef, {ext, ?MODULE, Cmd, self(), OptsMap}}},
%%    TimerRef1 = ?SCR_COMP:event_after(?TIMEOUT, {'timeout_state',CmdRef,S=?S_WAIT_TICKET}),
%%    {ok, State#state_scr{active_component=Comp#component{state=CS#data{state = S,
%%                                                                       ref = CmdRef,
%%                                                                       timer_ref = TimerRef1,
%%                                                                       buffer = <<>>}}}}.
%%
%%% ---------------------------------------
%%
%%%% timeout of wait_ticket
%%handle_event({timeout_state,Ref,S}, #state_scr{active_component=#component{state=#data{state=S=?S_WAIT_TICKET,ref=Ref}}}=State) ->
%%    ?OUT("Ticket1 timeout", []),
%%    Err = {error,{timeout,<<"Ticket timeout">>}},
%%    fin(Err, State);
%%
%%%% ticket error (s = wait_ticket)
%%handle_event({ticket,CmdRef,{error,_}=Err}, #state_scr{active_component=#component{state=#data{state=?S_WAIT_TICKET,ref=CmdRef,timer_ref=TimerRef}}}=State) ->
%%    ?OUT("Ticket1 ~500tp", [Err]),
%%    ?EU:cancel_timer(TimerRef),
%%    fin(Err, State);
%%
%%%% ticket ok
%%handle_event({ticket,CmdRef,{ok,started}}, #state_scr{active_component=#component{state=#data{state=?S_WAIT_TICKET,ref=CmdRef,timer_ref=TimerRef}=CS}=Comp}=State) ->
%%    ?OUT("Ticket1 ok"),
%%    ?EU:cancel_timer(TimerRef),
%%    TimerRef1 = ?SCR_COMP:event_after(?TIMEOUT_PERFORMING, {'timeout_state',CmdRef,S=?S_PERFORMING}),
%%    {ok, State#state_scr{active_component=Comp#component{state=CS#data{state=S,
%%                                                                       timer_ref=TimerRef1}}}};
%%
%%%% -------------------------
%%
%%%% timeout of performing
%%handle_event({timeout_state,Ref,S}, #state_scr{active_component=#component{state=#data{state=S=?S_PERFORMING,ref=Ref}}}=State) ->
%%    ?OUT("Perform timeout", []),
%%    Err = {error,{timeout,<<"Perform timeout">>}},
%%    fin(Err, State);
%%
%%%% ticket complete perform
%%handle_event({ticket,CmdRef,{complete,Result}}, #state_scr{active_component=#component{state=#data{state=?S_PERFORMING,ref=CmdRef,timer_ref=TimerRef}=_CS}=_Comp}=State) ->
%%    ?OUT("Perform ok"),
%%    ?EU:cancel_timer(TimerRef),
%%    fin({complete,Result}, State);
%%
%%%% ticket error perform
%%handle_event({ticket,CmdRef,{error,_}=Err}, #state_scr{active_component=#component{state=#data{state=?S_PERFORMING,ref=CmdRef,timer_ref=TimerRef}=_CS}=_Comp}=State) ->
%%    ?OUT("Perform error 2"),
%%    ?EU:cancel_timer(TimerRef),
%%    fin(Err, State);
%%
%%%% --------------------------
%%
%%% timeout
%%handle_event({timeout,Ref}, #state_scr{active_component=#component{state=#data{ref=Ref}}}=State) ->
%%    ?OUT("tts timeout"),
%%    {ok, State};
%%
%%% timeout
%%handle_event(Event, State) ->
%%    ?OUT("tts event: ~120p", [Event]),
%%    {ok, State}.
%%
%%%% ====================================================================
%%%% Internal functions
%%%% ====================================================================
%%
%%cmd_start({_Ref, _FromPid, _Arg}=P, #state_ivr{a=#side{state=S}}=State) when S==dialog; S==early ->
%%    do_cmd_start(P, #state_ivr{a=#side{state=S}}=State);
%%cmd_start({Ref, FromPid, _Arg}, #state_ivr{}=State) ->
%%    ?SCR_COMP:event(FromPid, {'ticket',Ref,{error,{invalid_operation,<<"Operation could not be started, state mismatch">>}}}),
%%    {next_state, ?CURRSTATE, State}.
%%
%%% @private
%%do_cmd_start({Ref, FromPid, Opts}, State) when is_map(Opts) ->
%%    ?OUT("TTS START"),
%%    F_handler = fun(EventType, EventContent, StateName, StateData) -> ?MRCP_ADPT:handle_event(EventType, EventContent, StateName, StateData) end,
%%    State1 = State#state_ivr{ext_handler_fun=F_handler},
%%    Opts1 = Opts#{fun_started => fun() -> ?SCR_COMP:event(FromPid, {'ticket',Ref,{ok,started}}) end,
%%                  fun_error => fun({error,_}=Err) -> ?SCR_COMP:event(FromPid, {'ticket',Ref,Err}) end,
%%                  fun_complete => fun(Result) -> ?SCR_COMP:event(FromPid, {'ticket',Ref,{complete,Result}}) end,
%%                  resource => <<"speechsynth">>,
%%                  mode => recvonly},
%%    self() ! {extension, {mrcp_start, Opts1}},
%%    {next_state, ?CURRSTATE, State1}.
%%
%%%% -------------------------------------
%%%% finalize work of component
%%%% mode = error, async, interrupt, length, stopped, extra, gendtmf
%%%% -------------------------------------
%%fin({error,_}=Err, #state_scr{ownerpid=OwnerPid}=State) ->
%%    ?OUT("TTS fin: ~120p", [Err]),
%%    ?SCR_ERROR(State, "~500tp : fin : error : ~500tp", [?MODULE, Err]),
%%    OwnerPid ! {mrcp, stop},
%%    %Err;
%%    ?SCR_COMP:next([<<"transferError">>,<<"transfer">>],State);
%%%%fin(Mode, #state_scr{active_component=#component{state=#data{state=?S_PERFORMING}=CS}=Comp}=State) ->
%%%%    exec_terminate(State),
%%%%    fin(Mode, State#state_scr{active_component=Comp#component{state=CS#data{state=?S_TERMINATING}}});
%%fin({complete,_Result}, #state_scr{active_component=#component{data=Data,state=CS}}=State) ->
%%    ?OUT("TTS fin ~120p", [{complete,_Result}]),
%%    #data{buffer=Buffer}=CS,
%%    Var = ?SCR_COMP:get(<<"dtmfBuffer">>, Data, <<>>),
%%    Prev = prev(Var, State),
%%    State1 = assign(Var, <<Prev/bitstring, Buffer/bitstring>>, State),
%%    ?SCR_COMP:next(<<"transfer">>,State1).
%%%% @private
%%fin_1(State) -> ?SCR_COMP:next(<<"transfer">>,State).
%%
%%%% -------------------------------------
%%%% @private
%%%% return previous value of variable
%%%% -------------------------------------
%%prev(Var, #state_scr{active_component=#component{state=CS}}=State) ->
%%    case CS#data.clear_buffer orelse ?SCR_VAR:get_value(Var, undefined, State) of
%%        true -> <<>>;
%%        undefined -> <<>>;
%%        V when is_binary(V) -> V;
%%        V when is_integer(V) -> ?EU:to_binary(V);
%%        {S,_Enc} when is_binary(S) -> S;
%%        _ -> <<>>
%%    end.
%%
%%%% -------------------------------------
%%%% @private
%%%% assign value to variable
%%%% -------------------------------------
%%assign_field(Field, Value, #state_scr{active_component=#component{data=Data}}=State) when is_binary(Field) ->
%%    assign(?SCR_COMP:get(Field, Data, <<>>), Value, State).
%%assign(Var, Value, State) ->
%%    case ?SCR_VAR:set_value(Var, Value, State) of
%%        {ok, State1} -> State1;
%%        _ -> State
%%    end.