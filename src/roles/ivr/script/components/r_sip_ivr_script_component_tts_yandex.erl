%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 17.07.2019
%%% @doc RP-1466
%%%      IVR-component TTS (Text To Speech).
%%%      Implements integration to Yandex-Cloud Speech Kit over HTTP.
%%%      Description:
%%%        Implemented 2 modes: "save to file" and "play". NB: play mode also save to file too
%%%        File is saved to temporary script folder
%%%        Playing implemented by internal decorated PLAY(204) component. It plays audio file directly stored onto MG's server in :RECORD folder.
%%%        File is automatically removed from MG's :RECORD folder after component final.
%%%      Algorithm:
%%%          1. Sends HTTP to Yandex Cloud SpeechKit
%%%          2. Makes audio-file in script's temporary folder
%%%          3. If 'play' mode is activated then
%%%             a. Copies file into MG's server over RPC into :RECORD folder
%%%             b. Makes metadata of internal decorated PLAY(204) component.
%%%             c. Initiates internal decorated PLAY(204) component.
%%%             d. Posts all events into decorated PLAY component until it returns with stop.
%%%             e. Deletes file from MG's server.
%%%      FSM:
%%%        States: synthesizing, playing.
%%%        Events: init, http_timeout, http_response. Others are directly posted into decorated PLAY(204) component.
%%%        Operations: start_http, apply_http_result (write_file, start_play), apply_player_result, abort, drop_file.

-module(r_sip_ivr_script_component_tts_yandex).
-author('Peter Bukashin <tbotc@yandex.ru>').

%%-compile([export_all,nowarn_export_all]).
%%-export([init/1,
%%         handle_event/2,
%%         terminate/1,
%%         metadata/1]).
%%
%%%% ====================================================================
%%%% Define
%%%% ====================================================================
%%
%%%-include("../include/r_script.hrl").
%%-include("../../era_script/include/r_script.hrl").
%%
%%-include("../include/r_sip_ivr.hrl").
%%-include("../include/r_sip_nk.hrl").
%%
%%-define(U, r_sip_utils).
%%-define(CFG, r_sip_config).
%%
%%-record(data, {
%%               state :: synthesizing | playing,
%%               %
%%               yandex_uri :: binary(),
%%               yandex_api_key :: binary(),
%%               yandex_folder_id :: binary(),
%%               filename :: binary(),
%%               filepath :: binary(),
%%               %
%%               mode :: file | play,
%%               text :: binary(),
%%               lang :: binary(),
%%               voice :: binary(),
%%               emotion :: binary(),
%%               speed :: float(),
%%               %
%%               check_dtmf :: boolean(),
%%               buffer :: binary(),
%%               max_count :: integer(),
%%               interrupts :: [binary()],
%%               clear_buffer :: boolean(),
%%               clear_interrupt :: boolean(),
%%               response_timeout :: integer(),
%%               %
%%               mgid :: term(),
%%               mgfileinfo :: map(),
%%               http_request_id :: reference(),
%%               player_component :: term(), % play component metadata
%%               %
%%               pid :: pid(),
%%               ref :: reference(),
%%               ticket_timer_ref :: reference(),
%%               http_timer_ref :: reference()
%%              }).
%%
%%-define(TIMEOUT_TICKET, 10000).
%%
%%-define(CURRSTATE, 'active').
%%
%%-define(S_SYNTHESIZING, synthesizing).
%%-define(S_PLAYING, playing).
%%
%%-define(PROFILE, 'tts_yandex').
%%-define(URI, "https://tts.api.cloud.yandex.net/speech/v1/tts:synthesize").
%%
%%-define(DEFAULT_TIMEOUT, 60000).
%%
%%-define(SampleRateHz, 8000).
%%
%%-define(PlayComponent, r_sip_ivr_script_component_play).
%%
%%%% ====================================================================
%%%% Public functions
%%%% ====================================================================
%%
%%% ---------------------------------------------------------------------
%%%% returns descriptions of used properties
%%% ---------------------------------------------------------------------
%%metadata(_ScriptType) ->
%%    [{<<"mode">>, #{type => <<"list">>,
%%                    items => [{0,<<"play">>},
%%                              {1,<<"file">>}] }},
%%     {<<"text">>, #{type => <<"argument">> }},
%%     {<<"lang">>, #{type => <<"list">>,
%%                    items => [{0,<<"ru-RU">>},
%%                              {1,<<"en-US">>}]}},
%%     {<<"voice">>, #{type => <<"list">>,
%%                     items => [{0,<<"alyss">>},
%%                               {1,<<"jane">>},
%%                               {2,<<"oksana">>},
%%                               {3,<<"omazh">>},
%%                               {4,<<"zahar">>},
%%                               {5,<<"ermil">>},
%%                               {6,<<"alena">>},
%%                               {7,<<"filipp">>},
%%                               {100,<<"other">>}]}},
%%     {<<"voiceOther">>, #{type => <<"argument">>,
%%                          filter => <<"voice==100">> }},
%%     {<<"emotion">>, #{type => <<"list">>,
%%                       items => [{0,<<"good">>},
%%                                 {1,<<"evil">>},
%%                                 {2,<<"neutral">>}] }},
%%     {<<"speed">>, #{type => <<"argument">>}},
%%     %
%%     {<<"checkDTMF">>, #{type => <<"list">>,
%%                         items => [{0,<<"no">>},
%%                                   {1,<<"yes">>}],
%%                         filter => <<"mode==0">> }},
%%     {<<"dtmfBuffer">>, #{type => <<"variable">>,
%%                          filter => <<"(mode==0) && (checkDTMF==1)">> }},
%%     {<<"clearDtmfBuffer">>, #{type => <<"list">>,
%%                               items => [{0,<<"no">>},
%%                                         {1,<<"yes">>}],
%%                               filter => <<"(mode==0) && (checkDTMF==1) && (dtmfBuffer!=null)">> }},
%%     {<<"maxSymbolCount">>, #{type => <<"argument">>,
%%                              filter => <<"(mode==0) && (checkDTMF==1)">> }},
%%     {<<"interruptSymbols">>, #{type => <<"string">>,
%%                                filter => <<"(mode==0) && (checkDTMF==1)">> }},
%%     {<<"responseTimeoutSec">>, #{type => <<"argument">>}},
%%     %
%%     {<<"varHttpCode">>, #{type => <<"variable">>}},
%%     {<<"varFile">>, #{type => <<"variable">>}},
%%     %
%%     {<<"transfer">>, #{type => <<"transfer">> }},
%%     {<<"transferTimeout">>, #{type => <<"transfer">>,
%%                               title => <<"timeout">> }},
%%     {<<"transferError">>, #{type => <<"transfer">>,
%%                             title => <<"error">> }} ].
%%
%%% ---------------------------------------------------------------------
%%init(#state_scr{ownerpid=undefined}=State) ->
%%    ?SCR_COMP:next(<<"transferError">>, State);
%%init(#state_scr{active_component=Comp}=State) ->
%%    CS = prepare_param(State),
%%    State1 = State#state_scr{active_component=Comp#component{state=CS#data{state = ?S_SYNTHESIZING,
%%                                                                           pid = self(),
%%                                                                           ref = make_ref()}}},
%%    start_http(State1).
%%
%%%% @private
%%prepare_param(#state_scr{domain=Domain,active_component=#component{data=CompData}=_Comp}=State) ->
%%    {ok,{Uri,ApiKey,FolderId}} = get_yandex_keys(Domain),
%%    #data{yandex_uri = Uri,
%%          yandex_api_key = ApiKey,
%%          yandex_folder_id = FolderId,
%%          mode = case ?SCR_COMP:get(<<"mode">>, CompData, 0) of
%%                     0 -> 'play';
%%                     1 -> 'file'
%%                 end,
%%          text = ?SCR_ARG:get_string(<<"text">>, CompData, <<"">>, State),
%%          lang = case ?SCR_ARG:get_int(<<"lang">>, CompData, 0, State) of
%%                     0 -> <<"ru-RU">>;
%%                     1 -> <<"en-US">>
%%                 end,
%%          voice = case ?SCR_COMP:get(<<"voice">>, CompData, 0) of
%%                      0 -> <<"alyss">>;
%%                      1 -> <<"jane">>;
%%                      2 -> <<"oksana">>;
%%                      3 -> <<"omazh">>;
%%                      4 -> <<"zahar">>;
%%                      5 -> <<"ermil">>;
%%                      6 -> <<"alena">>;
%%                      7 -> <<"filipp">>;
%%                      100 -> ?SCR_ARG:get_string(<<"voiceOther">>, CompData, <<>>, State)
%%                  end,
%%          emotion = case ?SCR_COMP:get(<<"emotion">>, CompData, 0) of
%%                        0 -> <<"good">>;
%%                        1 -> <<"evil">>;
%%                        2 -> <<"neutral">>
%%                    end,
%%          speed = min(3.0,max(0.1,?SCR_ARG:get_float(<<"speed">>, CompData, 1.0, State))),
%%          %
%%          check_dtmf = ?EU:to_bool(?SCR_COMP:get(<<"checkDTMF">>, CompData, 1)),
%%          max_count = ?SCR_ARG:get_int(<<"maxSymbolCount">>, CompData, 0, State),
%%          interrupts = ?IVR_SCRIPT_UTILS:parse_interrupts(<<"interruptSymbols">>, CompData),
%%          clear_buffer = ?EU:to_bool(?SCR_COMP:get(<<"clearDtmfBuffer">>, CompData, 1)),
%%          clear_interrupt = ?EU:to_bool(?SCR_COMP:get(<<"clearInterrupt">>, CompData, 0)),
%%          response_timeout = max(0, min(60, ?SCR_ARG:get_int(<<"responseTimeoutSec">>, CompData, 10, State))) * 1000 }.
%%
%%%% @private
%%get_yandex_keys(Domain) ->
%%    case ?ENVDC:get_object_sticky(Domain,settings,[{keys,[<<"yandex_cloud">>]},{fields,[value]}],auto) of
%%        {ok,[Item],_HC} ->
%%            Value = maps:get(value,Item),
%%            case maps:get(<<"speech">>,Value,undefined) of
%%                undefined -> {error,{'yandex_api_key',not_found}};
%%                Speech ->
%%                    case maps:get(<<"apiKey">>,Speech,<<>>) of
%%                        <<>> -> {error,{'yandex_api_key',not_found}};
%%                        ApiKey ->
%%                            Uri = case maps:get(<<"uri_tts">>,Speech,?URI) of <<>> -> ?URI; V -> ?EU:to_list(V) end,
%%                            {ok, {Uri, ApiKey, maps:get(<<"folderId">>,Value,<<>>)}}
%%                    end
%%            end;
%%        _ -> {error,{'yandex_api_key',not_found}}
%%    end.
%%
%%%% ---------------------------------------------------------------------
%%handle_event(Event, State) ->
%%    handle_event_1(Event, State).
%%
%%%% ---------------------------------
%%%% http response
%%handle_event_1({'http_response',Ref,ReplyInfo}, #state_scr{active_component=#component{state=#data{state=?S_SYNTHESIZING,ref=Ref}}}=State) ->
%%    apply_http_result(ReplyInfo,State);
%%%% http timeout
%%handle_event_1({'http_timeout',Ref,S}, #state_scr{active_component=#component{state=#data{state=S=?S_SYNTHESIZING,ref=Ref}}}=State) ->
%%    abort({error,'http_timeout'}, State);
%%
%%%% ---------------------------------
%%%% any events during PLAYING are transmitting into decorated PLAY(204) component
%%handle_event_1(Event, #state_scr{active_component=#component{state=#data{state=?S_PLAYING,player_component=PlayerComp}=CS}=Comp}=State) ->
%%    F = fun(#state_scr{active_component=PlayerCompX}=StateX) ->
%%                StateX#state_scr{active_component=Comp#component{state=CS#data{player_component=PlayerCompX}}}
%%        end,
%%    case ?PlayComponent:handle_event(Event, State#state_scr{active_component=PlayerComp}) of
%%        {ok,State1} -> {ok,F(State1)};
%%        {next,Id,State1} ->
%%            {ok,State2} = ?PlayComponent:terminate(State1),
%%            {ok,State3} = apply_player_result(F(State2)),
%%            {next,Id,State3}
%%    end;
%%
%%%% ---------------------------------
%%%% other
%%handle_event_1(_Event, State) ->
%%    {ok, State}.
%%
%%%% ---------------------------------------------------------------------
%%terminate(State) ->
%%    abort_internal(terminating,State).
%%
%%%% ====================================================================
%%%% FSM actions
%%%% ====================================================================
%%
%%%% --------------------------------------------------------------------
%%start_http(#state_scr{}=State) ->
%%    http_init(),
%%    case catch http_request(State) of
%%        {ok,RequestId} ->
%%            #data{}=CS = get_data(State),
%%            {ok,set_data(CS#data{http_request_id=RequestId},State)};
%%        {error,_}=Err -> abort({error,{'http_request_error',Err}},State);
%%        {'EXIT',Err} -> abort({error,{'http_request_error',Err}},State)
%%    end.
%%
%%%% @private
%%http_init() ->
%%    inets:start(),
%%    inets:start(httpc, [{profile,?PROFILE}]),
%%    Options = [{max_sessions,10000},
%%               {keep_alive_timeout,5000}],
%%    httpc:set_options(Options,?PROFILE).
%%
%%%% @private
%%-spec http_request(#state_scr{}) -> {ok,RequestId::reference()} | {error,Reason::term()}.
%%http_request(#state_scr{}=State) ->
%%    #data{pid=Pid,
%%          ref=Ref,
%%          yandex_uri=Uri,
%%          yandex_api_key=ApiKey,
%%          yandex_folder_id=FolderId,
%%          response_timeout=ResponseTimeout}=CS = get_data(State),
%%    Params0 = case FolderId of
%%                  <<>> -> [];
%%                  _ -> [{"folderId",FolderId}]
%%              end,
%%    Params = [{"text",CS#data.text},
%%              {"lang",CS#data.lang},
%%              {"voice",CS#data.voice},
%%              {"emotion",CS#data.emotion},
%%              {"speed",CS#data.speed},
%%              {"format","lpcm"},
%%              {"sampleRateHertz",?SampleRateHz}|Params0],
%%    ContentType = "application/x-www-form-urlencoded",
%%    <<_:1/binary,Body/binary>> = lists:foldl(fun({K,V},Acc) ->
%%                                                     V1 = case is_binary(V) of true -> ?EU:urlencode(V); _ -> ?EU:to_binary(V) end,
%%                                                     <<Acc/binary,"&",(?EU:strbin("~s=~s",[K,V1]))/binary>>
%%                                             end, <<>>, Params),
%%    Headers = [{"Authorization",?EU:str("Api-Key ~s", [ApiKey])}],
%%    HttpOpts = case ResponseTimeout of
%%                   T when T >= 1000 -> [{timeout, T}];
%%                   _ -> [{timeout, ?DEFAULT_TIMEOUT}]
%%               end,
%%    Opts = [{sync,false},
%%            {receiver,fun(ReplyInfo) -> ?SCR_COMP:event(Pid,{'http_response',Ref,ReplyInfo}) end},
%%            {full_result,true}],
%%    httpc:request(post, {Uri, Headers, ContentType, Body}, HttpOpts, Opts, ?PROFILE).
%%
%%%% --------------------------------------------------------------------
%%apply_http_result({_Ref,{error,timeout}}=_ReplyInfo,#state_scr{}=State) ->
%%    State1 = assign_field(<<"varHttpCode">>, 408, State),
%%    ?SCR_COMP:next(<<"transferTimeout">>, State1);
%%%%
%%apply_http_result({_Ref,{error,_}}=_ReplyInfo,#state_scr{}=State) ->
%%    State1 = assign_field(<<"varHttpCode">>, 500, State), % emulate
%%    ?SCR_COMP:next(<<"transferError">>, State1);
%%%%
%%apply_http_result({_Ref,{{_Proto,Code,_Status},_Headers,Body}}=_ReplyInfo,#state_scr{}=State) ->
%%    State1 = assign_field(<<"varHttpCode">>, Code, State),
%%    case Code of
%%        _ when Code>=200, Code<300 ->
%%            #data{mode=Mode} = get_data(State),
%%            case catch write_file(Body,State1) of
%%                {error,_}=Err -> abort({error,{'write_file',Err}},State1);
%%                {ok,State2} when Mode==file -> ?SCR_COMP:next(<<"transfer">>, State2);
%%                {ok,State2} when Mode==play -> start_play(State2)
%%            end;
%%        _ -> ?SCR_COMP:next(<<"transferError">>, State1)
%%    end.
%%
%%%% --------------------------------------------------------------------
%%%% @private
%%%% write file in script's temp folder, assigns path to variable
%%write_file(Body,#state_scr{scriptid=IvrId}=State) when is_binary(Body) ->
%%    #data{pid=SMPid}=CS = get_data(State),
%%    Filename = ?EU:str("ivrr_~s_~s.wav", [?EU:to_list(IvrId), ?EU:to_list(?EU:timestamp())]),
%%    Folder = ?EU:to_list(?SCR_FILE:get_script_temp_directory(SMPid)),
%%    FilePath = filename:join([Folder,Filename]),
%%    LFilePath = filename:join([<<":TEMP">>,Filename]),
%%    State1 = assign_field(<<"varFile">>, LFilePath, State),
%%    WavHeader = ?EU:get_wav_header(false,1,16,?SampleRateHz,size(Body) div 2),
%%    ?Rfilelib:ensure_dir(FilePath),
%%    case ?Rfile:write_file(FilePath,<<WavHeader/binary,Body/binary>>) of
%%        {error,_}=Err -> Err;
%%        ok -> {ok,set_data(CS#data{filename=Filename,
%%                                   filepath=FilePath},State1)}
%%    end.
%%
%%%% --------------------------------------------------------------------
%%%% @private
%%start_play(#state_scr{}=State) ->
%%    #data{}=CS = get_data(State),
%%    {ok,MGID} = get_mgid(State),
%%    {ok,State1} = push_file(MGID,set_data(CS#data{mgid=MGID,state=?S_PLAYING},State)),
%%    {ok,State2} = prepare_player_component(State1),
%%    init_player(State2).
%%
%%%% @private
%%get_mgid(#state_scr{ownerpid=OwnerPid}=_State) ->
%%    CmdRef = make_ref(),
%%    F = fun({Ref,FromPid,_},#state_ivr{}=StateIvr) ->
%%                {ok,#{mgid:=MGID}=_MediaInfo} = ?IVR_MEDIA:get_current_media_link(StateIvr),
%%                FromPid ! {ok,Ref,MGID},
%%                {next_state, ?CURRSTATE, StateIvr}
%%        end,
%%    OwnerPid ! {scriptmachine, {ivr_cmd, CmdRef, {extf, F, self(), #{}}}},
%%    receive {ok,Ref,MGID} when Ref == CmdRef -> {ok,MGID}
%%    after 5000 -> false
%%    end.
%%
%%%% @private
%%push_file(MGID, #state_scr{}=State) ->
%%    case get_mg_fileinfo(MGID,State) of
%%        false -> false;
%%        {ok,MGFileInfo} ->
%%            #data{filepath=SrcPath}=CS = get_data(State),
%%            [MGFilePath,MGSite,MGNode] = ?EU:maps_get([filepath,site,node],MGFileInfo),
%%            CurrentDest = {?CFG:get_current_site(),node()},
%%            ok = ?ENVCROSS:call_node({MGSite,MGNode}, {?Rfilelib, ensure_dir, [MGFilePath]}, false),
%%            ok = ?ENVCROSS:call_node({MGSite,MGNode}, {?ENVCOPY, copy_file, [CurrentDest,SrcPath,MGFilePath]}, false),
%%            {ok,set_data(CS#data{mgfileinfo=MGFileInfo}, State)}
%%    end.
%%
%%%% @private
%%get_mg_fileinfo(MGID,#state_scr{}=State) ->
%%    case ?U:parse_mg_devicename(?U:get_mg_devicename(MGID)) of
%%        undefined -> false;
%%        {Addr,Postfix} ->
%%            case ?CFG:get_mg_node_by_addr_postfix(Addr,Postfix) of
%%                undefined -> false;
%%                {MGSite,MGNode} ->
%%                    #data{filename=Filename} = get_data(State),
%%                    MGRecFolder = filename:join([?CFG:get_record_path(Addr,Postfix)]), % "E:/!!DEL!!/erlang/site1/records"
%%                    {_TS,RecPath} = ?U:get_media_context_rec_opts(), % ":RECORD/Srv-006/2019-07-17"
%%                    FP1 = filename:join([RecPath, Filename]),
%%                    MGFilePath = filename:join([MGRecFolder|lists:nthtail(1,filename:split(FP1))]),
%%                    {ok, #{filename => Filename,
%%                           filepath => MGFilePath,
%%                           filepath_by_rec => binary:replace(?EU:to_binary(MGFilePath),?EU:to_binary(MGRecFolder),<<":RECORD">>),
%%                           %recfolder => MGRecFolder,
%%                           %recpath => RecPath,
%%                           site => MGSite,
%%                           node => MGNode}}
%%            end end.
%%
%%%% @private
%%prepare_player_component(#state_scr{}=State) ->
%%    #data{}=CS = get_data(State),
%%    Id = ?EU:random(1000000000,9999999999),
%%    Type = 204,
%%    Name = <<"TTS Yandex Player">>,
%%    PlayerComp = #component{id = Id,
%%                            type = Type,
%%                            name = Name,
%%                            module = ?PlayComponent,
%%                            data = build_player_data({Id,Type,Name},State),
%%                            state = undefined},
%%    {ok,set_data(CS#data{player_component = PlayerComp},State)}.
%%
%%%% @private
%%build_player_data({Id,Type,Name}, #state_scr{active_component=#component{data=CData}}=State) ->
%%    #data{check_dtmf=CheckDTMF,
%%          mgfileinfo=MGFileInfo} = get_data(State),
%%    DtmfKeys = case CheckDTMF of
%%                    false -> [];
%%                    true -> [<<"dtmfBuffer">>,
%%                             <<"clearDtmfBuffer">>,
%%                             <<"maxSymbolCount">>,
%%                             <<"interruptSymbols">>,
%%                             <<"clearInterrupt">>]
%%                end,
%%    TransferKeys = [<<"transfer">>, <<"transferError">>],
%%    CopiedKeys = DtmfKeys ++ TransferKeys,
%%    CopiedProps = lists:filter(fun({K,_V}) -> lists:member(K,CopiedKeys) end, CData),
%%    [{<<"oId">>,Id},
%%     {<<"oType">>,Type},
%%     {<<"name">>,Name},
%%     {<<"playerId">>,?EU:random(100000000,1000000000)},
%%     {<<"mode">>,0},
%%     {'direct_file',?EU:to_binary(maps:get(filepath_by_rec,MGFileInfo))}
%%     | CopiedProps].
%%
%%%% @private
%%init_player(#state_scr{active_component=Comp}=State) ->
%%    #data{player_component=PlayerComp}=CS = get_data(State),
%%    F = fun(#state_scr{active_component=PlayerCompX}=StateX) ->
%%                StateX#state_scr{active_component=Comp#component{state=CS#data{player_component=PlayerCompX}}}
%%        end,
%%    case ?PlayComponent:init(State#state_scr{active_component=PlayerComp}) of
%%        {ok,State1} -> {ok,F(State1)};
%%        {next,Id,State1} ->
%%            {ok,State2} = ?PlayComponent:terminate(State1),
%%            {ok,State3} = apply_player_result(F(State2)),
%%            {next,Id,State3}
%%    end.
%%
%%%% --------------------------------------------------------------------
%%apply_player_result(State) ->
%%    drop_file(normal,State).
%%
%%%% --------------------------------------------------------------------
%%abort(Reason,#state_scr{}=State) ->
%%    {ok,State1} = abort_internal(normal,State),
%%    case Reason of
%%        {error,{'yandex_api_key',not_found}}=Err -> Err;
%%        {error,{'http_request_error',_Err}} -> ?SCR_COMP:next(<<"transferError">>, State1);
%%        {error,'http_timeout'} -> ?SCR_COMP:next(<<"transferTimeout">>, State1);
%%        {error,{'write_file',_Err}} -> ?SCR_COMP:next(<<"transferError">>, State1);
%%        _ -> ?SCR_COMP:next(<<"transferError">>, State1)
%%    end.
%%
%%%% @private
%%abort_internal(Mode,#state_scr{}=State) ->
%%    drop_file(Mode,State).
%%
%%%% --------------------------------------------------------------------
%%drop_file(Mode, #state_scr{}=State) ->
%%    case get_data(State) of
%%        #data{mgfileinfo=MGFileInfo}=CS when is_map(MGFileInfo) ->
%%            [MGSite,MGNode,MGFilePath] = ?EU:maps_get([site,node,filepath],MGFileInfo),
%%            F = fun() -> ?ENVCROSS:call_node({MGSite,MGNode}, {?Rfile, delete, [MGFilePath]}, false) end,
%%            case Mode of
%%                normal -> F();
%%                terminating -> spawn(fun() -> timer:sleep(5000), F() end)
%%            end,
%%            {ok,set_data(CS#data{mgfileinfo=undefined},State)};
%%        _ -> {ok,State}
%%    end.
%%
%%%% ====================================================================
%%%% Internal functions
%%%% ====================================================================
%%
%%%% -----
%%%% @private
%%c(#state_scr{}=State) -> State;
%%c({ok,#state_scr{}=State}) -> c(State).
%%
%%%% -----
%%%% @private
%%get_data(#state_scr{active_component=#component{state=Data}}) -> Data.
%%%% @private
%%set_data(#data{}=Data,#state_scr{active_component=Comp}=State) -> State#state_scr{active_component=Comp#component{state=Data}}.
%%
%%%% -------------------------------------
%%%% @private
%%%% assign value to variable
%%%% -------------------------------------
%%assign_field(Field, Value, #state_scr{active_component=#component{data=Data}}=State) when is_binary(Field) ->
%%    assign(?SCR_COMP:get(Field, Data, <<>>), Value, State).
%%assign(Var, Value, State) ->
%%    case ?SCR_VAR:set_value(Var, Value, State) of
%%        {ok, State1} -> State1;
%%        _ -> State
%%    end.