%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Pavel Abramov, Peter Bukashin <tbotc@yandex.ru>
%%% @date 17.11.2016
%%% @doc
%%% @todo @ivr

-module(r_sip_ivr_script_component_refer).
-author(['Pavel Abramov','Peter Bukashin <tbotc@yandex.ru>']).

%%%-compile([export_all,nowarn_export_all]).
%%-export([init/1,
%%         handle_event/2,
%%         metadata/1]).
%%
%%%% ====================================================================
%%%% Defines
%%%% ====================================================================
%%
%%-include("r_sip_ivr_script_refer.hrl").
%%
%%%% ====================================================================
%%%% Callback functions (script)
%%%% ====================================================================
%%
%%%% ----------------------------
%%init(#state_scr{ownerpid=undefined}=_State) -> {error,{internal_error,<<"Invalid owner">>}};
%%init(#state_scr{active_component=#component{data=CompData}=Comp}=State) ->
%%    ReferTo = ?SCR_ARG:get_string(<<"number">>, CompData, <<>>, State),
%%    ReinviteMode = ?REFER:reinvite_stream_mode(?SCR_COMP:get(<<"reinviteMode">>, CompData, 0)),
%%    CS = case ReferTo of
%%            <<>> -> {error,{invalid_params,<<"Number could not be empty">>}};
%%            _ -> #refer{referto = fun(#state_ivr{map=Map}=_State) -> #uri{scheme=sip, user=ReferTo, domain=maps:get(domain,Map)} end,
%%                        reinvite_mode = ReinviteMode}
%%        end,
%%    ?REFER:init(State#state_scr{active_component=Comp#component{state=CS}}).
%%
%%%% ----------------------------
%%handle_event(Msg, #state_scr{}=State) ->
%%    ?REFER:handle_event(Msg, State).
%%
%%%% ----------------------------
%%%% returns descriptions of used properties
%%%% ----------------------------
%%metadata(_ScriptType) ->
%%    [{<<"number">>, #{type => <<"argument">>}},
%%     {<<"reinviteMode">>, #{type => <<"list">>,
%%                            items => [{0,<<"not_used">>},
%%                                      {1,<<"auto">>},
%%                                      {2,<<"sendonly">>},
%%                                      {3,<<"inactive">>}] }},
%%     {<<"resultCode">>, #{type => <<"variable">>}},
%%     {<<"resultDescription">>, #{type => <<"variable">>}},
%%     {<<"resultSipCode">>, #{type => <<"variable">>}},
%%     {<<"resultSipReason">>, #{type => <<"variable">>}},
%%     {<<"transferSubTerminated">>, #{type => <<"transfer">>}},
%%     {<<"transferRejected">>, #{type => <<"transfer">>}},
%%     {<<"transferTimeout">>, #{type => <<"transfer">>}}].
%%
%%%% ====================================================================
%%%% Internal functions
%%%% ====================================================================
