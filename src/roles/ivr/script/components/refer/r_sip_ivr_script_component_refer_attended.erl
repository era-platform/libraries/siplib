%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Evgeniy Grebenyuk <llceceron@gmail.com>, Peter Bukashin <tbotc@yandex.ru>
%%% @date 25.10.2016
%%% @doc
%%% @todo
%%% @changed Abramov Pavel 21.11.2016

-module(r_sip_ivr_script_component_refer_attended).
-author(['Evgeniy Grebenyuk <llceceron@gmail.com>', 'Peter Bukashin <tbotc@yandex.ru>']).

%% ====================================================================
%% Exports
%% ====================================================================

%%-export([init/1,
%%         handle_event/2,
%%         metadata/1]).
%%
%%-export([svc_dialer_owner/2,
%%         monitor_dialer/4]).
%%
%%%% ====================================================================
%%%% Defines
%%%% ====================================================================
%%
%%-include("r_sip_ivr_script_refer.hrl").
%%-include("r_sip.hrl").
%%
%%%% component defines
%%-record(dialing, {
%%    state :: start_dialing | dialing | in_call,
%%    ivr_fsm_pid,
%%    ref,
%%    timer_ref,
%%    timer_total_ref,
%%    opts :: #{},
%%    dialing_owner_pids=[],
%%    dialer_pids=[],
%%    dialingto
%%  }).
%%
%%-define(ToutStartDialing, 10000).
%%
%%-define(LStateStartDialing, 'start_dialing').
%%-define(LStateDialing, 'dialing').
%%-define(LStateInCall, 'in_call').
%%
%%%% ====================================================================
%%%% Callback functions (script)
%%%% ====================================================================
%%
%%%% ----------------------------
%%init(#state_scr{ownerpid=undefined}=_State) -> {error,{internal_error,<<"Invalid owner">>}};
%%init(#state_scr{ownerpid=OwnerPid, meta=#meta{scripts=Scripts},map=Map,active_component=#component{data=CompData}=Comp}=State) ->
%%    ReferTo = ?SCR_ARG:get_string(<<"number">>, CompData, <<>>, State),
%%    case ReferTo of
%%        <<>> ->
%%            State1 = ?REFER:assign(?VarResCode, {<<"001">>,<<"utf8">>}, State),
%%            State2 = ?REFER:assign(?VarResDescr, {<<"Number could not be empty">>,<<"utf8">>}, State1),
%%            fin_dial(transfer_error, State2);
%%        _ ->
%%            CmdRef = make_ref(),
%%            Domain = maps:get(domain,Map,undefined),
%%            Headers0 = <<"X-Era-Ghost: ivr-refer">>,
%%            Headers = case ?SCR_ARG:get_string(<<"headers">>, CompData, undefined, State) of
%%                          undefined -> Headers0;
%%                          <<>> -> Headers0;
%%                          H -> <<Headers0/binary,"\r\n",H/binary>>
%%                      end,
%%            Opts = #{scrmachinepid => self(),
%%                     basenumber => ReferTo,
%%                     domain => Domain,
%%                     callerid => ?SCR_ARG:get_string(<<"callerId">>, CompData, <<>>, State),
%%                     callername => ?SCR_ARG:get_string(<<"callerName">>, CompData, <<>>, State),
%%                     headers => Headers,
%%                     ivrscrparams => ?SCR_ARG:get_string(<<"ivrScrParams">>, CompData, undefined, State),
%%                     dialingtout => DialingTout=?SCR_ARG:get_int(<<"dialingTimeout">>, CompData, 30, State)*1000,
%%                     ivrtout => ?SCR_ARG:get_int(<<"ivrTimeout">>, CompData, 1800, State)*1000,
%%                     setscrmode => SetScrMode=?SCR_COMP:get(<<"setScrMode">>, CompData, 3),
%%                     ref => CmdRef},
%%            Fstartdialing = fun(OptsX) ->
%%                                case start_ivr_dialing(ReferTo,OptsX) of
%%                                    {ok,CompState} ->
%%                                        TimerRef1 = ?SCR_COMP:event_after(?ToutStartDialing, {'timeout_dialing_start',CmdRef,?LStateStartDialing}),
%%                                        TimerRefTotal = ?SCR_COMP:event_after(DialingTout, {'timeout_dialing_total',CmdRef}),
%%                                        {ok, State#state_scr{active_component=Comp#component{state=CompState#dialing{state=?LStateStartDialing,ivr_fsm_pid=OwnerPid,timer_ref=TimerRef1,timer_total_ref=TimerRefTotal,ref=CmdRef}}}};
%%                                    {error,Err} ->
%%                                        State1 = ?REFER:assign(?VarResCode, {<<"001">>,<<"utf8">>}, State),
%%                                        State2 = ?REFER:assign(?VarResDescr, {Err,<<"utf8">>}, State1),
%%                                        fin_dial(transfer_error, State2)
%%                                end end,
%%            case SetScrMode of
%%                _ when SetScrMode == 0; SetScrMode == 2 -> % by code or select in list
%%                    IvrScrCode = case SetScrMode of
%%                                     0 -> ?SCR_ARG:get_string(<<"ivrScrCode">>, CompData, <<>>, State);
%%                                     2 -> ?SCR_COMP:get(<<"ivrScrCodeSel">>, CompData, <<>>)
%%                                 end,
%%                    case Scripts(IvrScrCode) of
%%                        X when X=:=undefined;X=:=not_found ->
%%                            Err = ?EU:to_binary("Script "++?EU:to_list(X)),
%%                            State1 = ?REFER:assign(?VarResCode, {<<"001">>,<<"utf8">>}, State),
%%                            State2 = ?REFER:assign(?VarResDescr, {Err,<<"utf8">>}, State1),
%%                            fin_dial(transfer_error, State2);
%%                        IvrScr ->
%%                            Opts1 = Opts#{ivrscrcode => IvrScrCode, ivrscr => IvrScr},
%%                            Fstartdialing(Opts1)
%%                    end;
%%                1 -> % by body
%%                    IvrScr = ?SCR_ARG:get_string(<<"ivrScr">>, CompData, <<>>, State),
%%                    Opts1 = Opts#{ivrscrcode => undefined, ivrscr => IvrScr},
%%                    Fstartdialing(Opts1);
%%                3 -> % none
%%                    Opts1 = Opts#{ivrscrcode => undefined, ivrscr => <<>>},
%%                    Fstartdialing(Opts1);
%%                _ ->
%%                    State1 = ?REFER:assign(?VarResCode, {<<"001">>,<<"utf8">>}, State),
%%                    State2 = ?REFER:assign(?VarResDescr, {<<"Script mode could not be empty">>,<<"utf8">>}, State1),
%%                    fin_dial(transfer_error, State2)
%%            end
%%    end.
%%
%%%% -------------------------------
%%
%%%% ------------
%%%% Events start dialer
%%%% ------------
%%handle_event({'success_start_dialing',CmdRef,{DialerOwnerPid,Number,DialerPid,IvrNode}=_Data},
%%             #state_scr{active_component=#component{state=#dialing{state=LocalState,dialing_owner_pids=DialerOwnerPidList,dialer_pids=DialerPids,ref=CmdRef,timer_ref=TimerRef}=Dial}=Comp}=State)
%%  when LocalState=:=?LStateStartDialing; LocalState=:=?LStateDialing ->
%%    ScriptMachinePid = self(),
%%    case TimerRef of A when is_reference(A) -> erlang:cancel_timer(TimerRef); _ -> ok end,
%%    NewDialerOwnerPidList = lists:keydelete(DialerOwnerPid, 1, DialerOwnerPidList),
%%    % Monitor each Dialer
%%    Flog = fun(StopReason) -> ?OUT("Attended Refer. Dialer process ~500tp terminated: ~500tp", [DialerPid, StopReason]) end,
%%    Fclear = fun() -> ?SCR_COMP:event(ScriptMachinePid,{'dialer_dead',CmdRef,DialerPid}) end,
%%    DialerMonitorPid = erlang:spawn(?MODULE,monitor_dialer,[ScriptMachinePid,DialerPid,Flog,Fclear]),
%%    %
%%    NewDialerPidList = [{DialerPid,Number,IvrNode,DialerMonitorPid}|DialerPids],
%%    State1 = State#state_scr{active_component=Comp#component{state=Dial#dialing{state=?LStateDialing,dialing_owner_pids=NewDialerOwnerPidList,dialer_pids=NewDialerPidList,timer_ref=undefined}}},
%%    {ok,State1};
%%
%%%%
%%handle_event({'error_start_dialing',CmdRef,{DialerOwnerPid,Number,Err}=_Data},
%%             #state_scr{active_component=#component{state=#dialing{state=LocalState,dialing_owner_pids=DialerOwnerPidList,ref=CmdRef}=Dial}=Comp}=State)
%%  when LocalState=:=?LStateStartDialing; LocalState=:=?LStateDialing ->
%%    d1(Dial, "error start dialing ToNumber=~500tp, Caught error=~500tp", [Number, Err]),
%%    case lists:keydelete(DialerOwnerPid, 1, DialerOwnerPidList) of
%%        [] ->
%%            State1 = ?REFER:assign(?VarResCode, {<<"002">>,<<"utf8">>}, State),
%%            State2 = ?REFER:assign(?VarResDescr, {<<"Error start dialing">>,<<"utf8">>}, State1),
%%            fin_dial(transfer_error, State2);
%%        NewDialerOwnerPidList ->
%%            State1 = State#state_scr{active_component=Comp#component{state=Dial#dialing{dialing_owner_pids=NewDialerOwnerPidList}}},
%%            {ok,State1}
%%    end;
%%
%%%% 1xx
%%handle_event({'dialer_response_1xx',CmdRef,_Msg},
%%             #state_scr{active_component=#component{state=#dialing{state=?LStateDialing,dialer_pids=_DialerPidList,ref=CmdRef}=_Dial}=_Comp}=State) ->
%%    {ok,State};
%%
%%%% 2xx
%%handle_event({'dialer_response_2xx',CmdRef,{FromDialerPid,#{}=_Data}=_Msg},
%%             #state_scr{active_component=#component{state=#dialing{state=?LStateDialing,opts=Opts,dialer_pids=DialerPidList,timer_total_ref=TimerRefTotal,ref=CmdRef}=Dial}=Comp}=State) ->
%%    case lists:keyfind(FromDialerPid, 1, DialerPidList) of
%%        {FromDialerPid,ResNum,ResIvrNode,FromDialerMonitorPid} ->
%%            erlang:cancel_timer(TimerRefTotal),
%%%%             [?MONITOR:stop_monitor(DialerPid) || {DialerPid,_Num,_IvrNode} <- DialerPidList, DialerPid=/=FromDialerPid],
%%            [stop_monitor_dialer(DialerMonitorPid,DialerPid,self()) || {DialerPid,_Num,_IvrNode,DialerMonitorPid} <- DialerPidList, DialerPid=/=FromDialerPid],
%%            [stop_dialer(DialerPid,IvrNode) || {DialerPid,_Num,IvrNode,_} <- DialerPidList, DialerPid=/=FromDialerPid],
%%            NewDialerPids = [{FromDialerPid,ResNum,ResIvrNode,FromDialerMonitorPid}],
%%            % IVR timer
%%            IvrTout = maps:get(ivrtout,Opts),
%%            TimerRef1 = ?SCR_COMP:event_after(IvrTout, {'timeout_ivr',CmdRef,?LStateInCall}),
%%            %
%%            State1 = State#state_scr{active_component=Comp#component{state=Dial#dialing{state=?LStateInCall,dialing_owner_pids=undefined,dialer_pids=NewDialerPids,dialingto=ResNum, timer_ref=TimerRef1}}};
%%        false ->
%%            State1 = State
%%    end,
%%    {ok,State1};
%%
%%%% 4xx-6xx
%%handle_event({'dialer_response_failed',CmdRef,{FromDialerPid,#{}=Data}=_Msg},
%%             #state_scr{active_component=#component{state=#dialing{state=?LStateDialing,dialer_pids=DialerPidList,ref=CmdRef}=Dial}=Comp}=State) ->
%%    case lists:keyfind(FromDialerPid, 1, DialerPidList) of
%%        {FromDialerPid,Number,IvrNode,FromDialerMonitorPid} ->
%%            stop_monitor_dialer(FromDialerPid,FromDialerMonitorPid,self()),
%%            ErrSipCode = maps:get(sipcode,Data),
%%            d1(Dial, "dialer response failed ToNumber=~500tp, on Node=~500tp, Caught error=~500tp", [Number, IvrNode, ErrSipCode]),
%%            case lists:keydelete(FromDialerPid, 1, DialerPidList) of
%%                [] ->
%%                    ErrSipReason = maps:get(sipdescr,Data),
%%                    ErrSipHeaders = maps:get(sipheaders,Data),
%%                    VarList = [{?VarResCode, <<"003">>},
%%                               {?VarResDescr, <<"Dialer response failed">>},
%%                               {?VarResSipCode, ?EU:to_binary(ErrSipCode)},
%%                               {?VarResSipReas,ErrSipReason},
%%                               {?VarResSipHeaders, ErrSipHeaders}],
%%                    State1 = ?REFER:multi_assign(VarList,State),
%%                    fin_dial(transfer_failed, State1);
%%                NewDialerPidList ->
%%                    State1 = State#state_scr{active_component=Comp#component{state=Dial#dialing{dialer_pids=NewDialerPidList}}},
%%                    {ok,State1}
%%            end;
%%        false ->
%%            {ok,State}
%%    end;
%%
%%%% IVR stopped start Refer
%%handle_event({dialer_ivr_stopped,CmdRef,{FromDialerPid,#{}=Data}=_Msg},
%%             #state_scr{active_component=#component{data=CompData,state=#dialing{state=?LStateInCall,dialer_pids=DialerPidList,ref=CmdRef}=_D}=Comp}=State) ->
%%    ACallId = maps:get(callid,Data),
%%    RemoteTag = maps:get(remotetag,Data),
%%    LocalTag = maps:get(localtag,Data),
%%    [{FromDialerPid,ResNum,_ResIvrNode, FromDialerMonitorPid}] = DialerPidList,
%%    ReferTo = ResNum,
%%    % init
%%    ReinviteMode = ?REFER:reinvite_stream_mode(?SCR_COMP:get(<<"reinviteMode">>, CompData, 0)),
%%    CS = #refer{referto = fun(#state_ivr{map=Map}=_State) -> ?IVR_SCRIPT_UTILS:build_referto_replaces(#uri{scheme=sip, user=ReferTo, domain=maps:get(domain,Map)}, {ACallId, LocalTag, RemoteTag}) end,
%%                reinvite_mode = ReinviteMode},
%%    ?REFER:init(State#state_scr{active_component=Comp#component{state=CS#refer{dialerpid=FromDialerPid,dialermonitorpid=FromDialerMonitorPid}}});
%%
%%%% ------------
%%%% Timeout Dialing events
%%%% ------------
%%
%%handle_event({'timeout_dialing_start',CmdRef,?LStateStartDialing}, #state_scr{active_component=#component{state=#dialing{state=?LStateStartDialing,ref=CmdRef}=_D}=_C}=State) ->
%%    State1 = ?REFER:assign(?VarResCode, {<<"003">>,<<"utf8">>}, State),
%%    State2 = ?REFER:assign(?VarResDescr, {<<"Dialer response failed">>,<<"utf8">>}, State1),
%%    fin_dial(transfer_failed, State2);
%%
%%%% Total Component tout
%%handle_event({'timeout_dialing_total',CmdRef}, #state_scr{active_component=#component{state=#dialing{ref=CmdRef}=_D}=_C}=State) ->
%%    State1 = ?REFER:assign(?VarResCode, {<<"009">>,<<"utf8">>}, State),
%%    State2 = ?REFER:assign(?VarResDescr, {<<"Dialing timeout">>,<<"utf8">>}, State1),
%%    State3 = stop_dialers(State2),
%%    fin_dial(transfer_timeout, State3);
%%
%%handle_event({'timeout_ivr',CmdRef,?LStateInCall}, #state_scr{active_component=#component{state=#dialing{state=?LStateInCall,ref=CmdRef}=_D}=_C}=State) ->
%%    State1 = ?REFER:assign(?VarResCode, {<<"005">>,<<"utf8">>}, State),
%%    State2 = ?REFER:assign(?VarResDescr, {<<"Dialing IVR timeout">>,<<"utf8">>}, State1),
%%    State3 = stop_dialers(State2),
%%    fin_dial(transfer_timeout, State3);
%%
%%%% ------------
%%%% Dialer dead events
%%%% ------------
%%
%%handle_event({'dialer_dead',CmdRef,FromDialerPid}, #state_scr{active_component=#component{state=#dialing{dialer_pids=DialerPidList,dialing_owner_pids=DialerOwnerPidList,ref=CmdRef}=Dial}=Comp}=State) ->
%%    case {lists:keydelete(FromDialerPid, 1, DialerPidList),DialerOwnerPidList} of
%%        {[],X}
%%          when X==[]; X==undefined ->
%%            State1 = ?REFER:assign(?VarResCode, {<<"010">>,<<"utf8">>}, State),
%%            State2 = ?REFER:assign(?VarResDescr, {<<"All Dialer dead">>,<<"utf8">>}, State1),
%%            fin_dial(transfer_failed, State2);
%%        {NewDialerPidList,_} ->
%%            State1 = State#state_scr{active_component=Comp#component{state=Dial#dialing{dialer_pids=NewDialerPidList}}},
%%            {ok,State1}
%%    end;
%%
%%handle_event({'dialer_dead',CmdRef,FromDialerPid}, #state_scr{active_component=#component{state=#refer{state=S,ref=CmdRef,dialermonitorpid=FromDialerPid}=_Refer}=_Comp}=State)
%%  when S=/=?S_ACCEPTED ->
%%    State1 = ?REFER:assign(?VarResCode, {<<"010">>,<<"utf8">>}, State),
%%    State2 = ?REFER:assign(?VarResDescr, {<<"All Dialer dead">>,<<"utf8">>}, State1),
%%    fin_dial(transfer_failed, State2);
%%
%%%% ------------
%%%% Refer events
%%%% ------------
%%
%%%% ticket send REFER ok (s = wait_bye)
%%handle_event({ticket,CmdRef,{ok,_M}}, #state_scr{active_component=#component{state=#refer{ref=CmdRef,dialerpid=DialerPid,dialermonitorpid=DialerMonitorPid}}}=State) ->
%%    stop_monitor_dialer(DialerMonitorPid,DialerPid,self()),
%%    ?REFER:handle_event({ticket,CmdRef,{ok,_M}},State);
%%
%%%% -----
%%handle_event(Msg, State) ->
%%    ?REFER:handle_event(Msg, State).
%%
%%%% ---------------------------------------
%%%% returns descriptions of used properties
%%%% ---------------------------------------
%%metadata(_ScriptType) ->
%%    [{<<"number">>, #{type => <<"argument">>}},
%%     {<<"callerId">>, #{type => <<"argument">>}},
%%     {<<"callerName">>, #{type => <<"argument">>}},
%%     {<<"headers">>, #{type => <<"argument">>}},
%%     {<<"dialingTimeout">>, #{type => <<"argument">>}},
%%     {<<"setScrMode">>, #{type => <<"list">>,
%%                          items => [{3,<<"none">>},
%%                                    {2,<<"code_sel">>},
%%                                    {0,<<"code">>},
%%                                    {1,<<"body">>}] }},
%%     {<<"ivrScrCode">>, #{type => <<"argument">>,
%%                          filter => <<"setScrMode == 0">>}},
%%     {<<"ivrScr">>, #{type => <<"argument">>,
%%                      filter => <<"setScrMode == 1">>}},
%%     {<<"ivrScrCodeSel">>, #{type => <<"clist">>,
%%                             key => <<"ivrscriptproj">>,
%%                             filter => <<"setScrMode == 2">>}},
%%     {<<"ivrScrParams">>, #{type => <<"argument">>,
%%                            filter => <<"setScrMode in (0,1,2)">>}},
%%     {<<"ivrTimeout">>, #{type => <<"argument">>,
%%                          filter => <<"setScrMode in (0,1,2)">>}},
%%     {<<"reinviteMode">>, #{type => <<"list">>,
%%                            items => [{0,<<"not_used">>},
%%                                      {1,<<"auto">>},
%%                                      {2,<<"sendonly">>},
%%                                      {3,<<"inactive">>}] }},
%%     {<<"resultCode">>, #{type => <<"variable">>}},
%%     {<<"resultDescription">>, #{type => <<"variable">>}},
%%     {<<"resultSipCode">>, #{type => <<"variable">>}},
%%     {<<"resultSipReason">>, #{type => <<"variable">>}},
%%     {<<"resultSipHeaders">>, #{type => <<"variable">>}},
%%     {<<"transferSubTerminated">>, #{type => <<"transfer">>}},
%%     {<<"transferRejected">>, #{type => <<"transfer">>}},
%%     {<<"transferTimeout">>, #{type => <<"transfer">>}},
%%     {<<"transferFailed">>, #{type => <<"transfer">>}},
%%     {<<"transferError">>, #{type => <<"transfer">>}}].
%%
%%%% ====================================================================
%%%% Callback functions (IVR Dialing)
%%%% ====================================================================
%%svc_dialer_owner(Number,Opts) ->
%%    [OwnerPid,Domain,CallerId,CallerName,IvrScrCode,IvrScr,IvrScrParams,Ref] =
%%        ?EU:maps_get([scrmachinepid,domain,callerid,callername,ivrscrcode,ivrscr,ivrscrparams,ref],Opts),
%%    FunResult = fun(EventType,Msg) ->
%%                        case EventType of
%%                            call_inited -> ok;
%%                            pre_res -> ?SCR_COMP:event(OwnerPid,{'dialer_response_1xx',Ref,Msg});
%%                            success_res -> ?SCR_COMP:event(OwnerPid,{'dialer_response_2xx',Ref,Msg});
%%                            unsuccess_res -> ?SCR_COMP:event(OwnerPid,{'dialer_response_failed',Ref,Msg});
%%                            refer_notify -> ok;
%%                            ivr_started -> ok;
%%                            ivr_msg -> ok;
%%                            ivr_stopped -> ?SCR_COMP:event(OwnerPid,{'dialer_ivr_stopped',Ref,Msg})
%%                        end
%%                end,
%%    % RP-759
%%    Args0 = case IvrScrParams of
%%                undefined -> [];
%%                VV1 when is_binary(VV1) ->
%%                    case catch jsx:decode(VV1) of
%%                        {'EXIT',_} -> [];
%%                        Res -> [{initial_var_values,Res}]
%%                    end end,
%%    Args = [{script,IvrScr},
%%            {script_code,IvrScrCode},
%%            {domain,Domain},
%%            {number,Number},
%%            {fun_res,FunResult},
%%            {callername,CallerName},
%%            {callerid,CallerId},
%%            {ownerpid,OwnerPid},
%%            {ownermode,3},
%%            {headers,make_headers(Domain,OwnerPid,Opts)},
%%            {startparam1,IvrScrParams} % RP-759
%%            | Args0],
%%    Param = #{mode => <<"1">>, args => Args},
%%    IvrNodeList = ?CFG:get_site_ivr(),
%%    IvrSrvNum = erlang:phash2(OwnerPid) rem erlang:length(IvrNodeList)+1,
%%    {IvrNode,_} = lists:nth(IvrSrvNum, IvrNodeList),
%%    CallRes = ?ENVCROSS:call_node(IvrNode, {?APP,ivr_cb_start_by_mode,[Param]}, undefined,3000),
%%    case CallRes of
%%        {ok,DialerPid} ->
%%            Data = {self(),Number,DialerPid,IvrNode},
%%            Msg = 'success_start_dialing';
%%        Err ->
%%            %% @@todo if error try to another IVR
%%            Data = {self(),Number,Err},
%%            Msg = 'error_start_dialing'
%%    end,
%%    ?SCR_COMP:event(OwnerPid, {Msg, Ref, Data}).
%%
%%%% @private
%%make_headers(Domain,OwnerPid,Opts) ->
%%    BindingsNested = nest_bindings_headers(Domain,OwnerPid),
%%    case maps:get(headers,Opts) of
%%        undefined when BindingsNested==[] -> [];
%%        undefined -> [{?BindingsHeader,?EU:join_binary(BindingsNested,<<";">>)}];
%%        H ->
%%            % RP-1297
%%            HLines = binary:split(H, [<<"\n">>,<<"\r\n">>], [global,trim_all]),
%%            HLines1 = lists:map(fun(HLine) -> binary:split(HLine, <<":">>) end, HLines),
%%            Ftrim = fun(X) -> ?EU:to_binary(string:trim(?EU:to_unicode_list(X))) end,
%%            {HLines2, HasBind} = lists:foldr(fun([HN,HV],{AccL,AccB}) ->
%%                                                    case string:lowercase(HN) of
%%                                                        ?BindingsHeaderLow when BindingsNested/=[] ->
%%                                                            {[{Ftrim(HN),?EU:join_binary([Ftrim(HV)|BindingsNested],<<";">>)} | AccL], AccB};
%%                                                        _ -> {[{Ftrim(HN),Ftrim(HV)} | AccL], AccB}
%%                                                    end end, {[],false}, HLines1),
%%            case HasBind of
%%                false when BindingsNested/=[] -> [{?BindingsHeader,?EU:join_binary(BindingsNested,<<";">>)} | HLines2];
%%                _ -> HLines2
%%            end
%%    end.
%%
%%%% @private
%%nest_bindings_headers(Domain,ScriptPid) when is_pid(ScriptPid) ->
%%    case sys:get_state(ScriptPid) of
%%        #state_scr{ownerpid=IvrPid} when is_pid(IvrPid) ->
%%            case sys:get_state(IvrPid) of
%%                {_, #state_ivr{acallid=ACallId}} ->
%%                    get_b2b_bindings(Domain,ACallId);
%%                _ -> []
%%            end;
%%        _ -> []
%%    end.
%%
%%%% @private
%%get_b2b_bindings(Domain,ACallId) ->
%%    case ?ENVCALL:call_callstore_local(Domain,{search_list,[fcallid,ACallId]},false) of
%%        {ok,[DialogId|_]} ->
%%            SrvIdx = ?EU:parse_srvidx(DialogId),
%%            case ?CFG:get_sipserver_by_index(SrvIdx) of
%%                undefined -> [];
%%                {_Site, _Node}=Dest ->
%%                    Request = {?APP,b2b_dialog_bindings_get,[DialogId]},
%%                    case ?ENVCROSS:call_node(Dest, Request, undefined, 5000) of
%%                        BindingList when is_list(BindingList) -> BindingList;
%%                        _ -> []
%%                    end
%%            end;
%%        false -> []
%%    end.
%%
%%%% -------------------------------
%%%% Monitor Dialer Process
%%%% -------------------------------
%%monitor_dialer(OwnerPid,DialerPid,FunLog,FunClear) ->
%%    RefOwner = erlang:monitor(process, OwnerPid),
%%    RefDialer = erlang:monitor(process, DialerPid),
%%    loop_down(OwnerPid,RefOwner,DialerPid,RefDialer,FunLog,FunClear).
%%
%%loop_down(OwnerPid,RefOwner,DialerPid,RefDialer,FunLog,FunClear) ->
%%    receive
%%        {'stop_monitor',DialerPid,OwnerPid} -> ok;
%%        {'DOWN', RefOwner, process, OwnerPid, _Result} -> ok;
%%        {'DOWN', RefDialer, process, DialerPid, Result} ->
%%            FunLog(Result),FunClear()
%%    end.
%%
%%stop_monitor_dialer(DialerMonitorPid,DialerPid,OwnerPid) ->
%%    DialerMonitorPid ! {'stop_monitor',DialerPid,OwnerPid}.
%%
%%%% ====================================================================
%%%% Support functions
%%%% ====================================================================
%%
%%%% -------------------------------
%%%% Component dialing functions
%%%% -------------------------------
%%start_ivr_dialing(ReferTo,Opts) ->
%%    case get_call_strategy(ReferTo) of
%%        {error,_}=Err ->
%%            Err;
%%        {number,NumberList} ->
%%            FunStart = fun(Number) -> erlang:spawn(?MODULE, svc_dialer_owner, [Number,Opts]) end,
%%            DialerOwnerPidList = [{FunStart(X),X} || X <- NumberList],
%%            CompState = #dialing{opts=Opts,dialing_owner_pids=DialerOwnerPidList},
%%            {ok,CompState}
%%    end.
%%
%%%% @private
%%get_call_strategy(ReferTo) ->
%%    try jsx:decode(ReferTo) of
%%        X when is_list(X); is_tuple(X) ->
%%            {error, <<"Number could not be json">>};
%%        _ ->
%%            NumberList = binary:split(ReferTo,<<",">>,[global,trim_all]),
%%            {number,NumberList}
%%    catch
%%        _:_ ->
%%            NumberList = binary:split(ReferTo,<<",">>,[global,trim_all]),
%%            {number,NumberList}
%%    end.
%%
%%% @private
%%stop_dialer(DialerPid,IvrNode) ->
%%    Reason = <<"stop_from_attended_refer">>,
%%    erlang:spawn(?ENVCROSS,call_node,[IvrNode, {?IVR_SRV,stop,[DialerPid,Reason]}, undefined,10000]).
%%
%%% @private
%%stop_dialers(#state_scr{active_component=#component{state=#dialing{dialer_pids=DialerPids}=St}=Comp}=State) ->
%%    lists:foreach(fun({DialerPid,_Num,IvrNode,DialerMonitorPid}) ->
%%                          stop_monitor_dialer(DialerMonitorPid,DialerPid,self()),
%%                          stop_dialer(DialerPid,IvrNode)
%%                  end, DialerPids),
%%    State#state_scr{active_component=Comp#component{state=St#dialing{dialer_pids=[]}}}.
%%
%%%% ====================================================================
%%%% Internal functions
%%%% ====================================================================
%%
%%%% -------------------------------
%%%% finalize dialer
%%%% -------------------------------
%%fin_dial(transfer_failed, State) -> ?SCR_COMP:next([<<"transferFailed">>],State);
%%fin_dial(transfer_timeout, State) -> ?SCR_COMP:next([<<"transferTimeout">>],State);
%%fin_dial(transfer_error, State) -> ?SCR_COMP:next([<<"transferError">>],State).
%%
%%%% @private
%%d1(DialingState, Fmt, Args) ->
%%    #dialing{state=LState}=DialingState,
%%    ?LOGSIP("IVR fsm LocalState:~500tp '~500tp':" ++ Fmt, [LState,?CURRSTATE] ++ Args).
