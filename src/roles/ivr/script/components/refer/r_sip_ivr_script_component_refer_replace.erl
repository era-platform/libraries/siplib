%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Pavel Abramov, Peter Bukashin <tbotc@yandex.ru>
%%% @date 17.11.2016
%%% @doc
%%% @todo @ivr

-module(r_sip_ivr_script_component_refer_replace).
-author(['Pavel Abramov','Peter Bukashin <tbotc@yandex.ru>']).

%%-export([init/1,
%%         handle_event/2,
%%         metadata/1]).
%%
%%%% ====================================================================
%%%% Defines
%%%% ====================================================================
%%
%%-include("r_sip_ivr_script_refer.hrl").
%%-include("r_sip.hrl").
%%
%%%% ====================================================================
%%%% Callback functions (script)
%%%% ====================================================================
%%
%%%% ----------------------------
%%init(#state_scr{ownerpid=undefined}=_State) -> {error,{internal_error,<<"Invalid owner">>}};
%%init(#state_scr{active_component=#component{data=CompData}=Comp}=State) ->
%%    CS = get_refer_to(State),
%%    ReinviteMode = ?REFER:reinvite_stream_mode(?SCR_COMP:get(<<"reinviteMode">>, CompData, 0)),
%%    CS1 = CS#refer{reinvite_mode = ReinviteMode},
%%    ?REFER:init(State#state_scr{active_component=Comp#component{state=CS1}}).
%%
%%%% ----------------------------
%%handle_event(Msg, #state_scr{}=State) ->
%%    ?REFER:handle_event(Msg, State).
%%
%%%% ----------------------------
%%%% returns descriptions of used properties
%%%% ----------------------------
%%metadata(_ScriptType) ->
%%    [{<<"referToUri">>, #{type => <<"argument">>}},
%%     {<<"replacesCallId">>, #{type => <<"argument">>}},
%%     {<<"replacesToTag">>, #{type => <<"argument">>}},
%%     {<<"replacesFromTag">>, #{type => <<"argument">>}},
%%%     {<<"dialingTimeout">>, #{type => <<"argument">>}}, % TODO?
%%     {<<"reinviteMode">>, #{type => <<"list">>,
%%                            items => [{0,<<"not_used">>},
%%                                      {1,<<"auto">>},
%%                                      {2,<<"sendonly">>},
%%                                      {3,<<"inactive">>}] }},
%%     {<<"huntblockenabled">>, #{type => <<"argument">>}},
%%     {<<"huntblockcode">>, #{type => <<"argument">>}},
%%     {<<"transferSubTerminated">>, #{type => <<"transfer">>}},
%%     {<<"transferRejected">>, #{type => <<"transfer">>}},
%%     {<<"transferTimeout">>, #{type => <<"transfer">>}}].
%%
%%%% ====================================================================
%%%% Internal functions
%%%% ====================================================================
%%
%%%% ----------------------------
%%-spec get_refer_to(State::#state_scr{}) -> Refer::#refer{}.
%%%% ----------------------------
%%get_refer_to(#state_scr{active_component=#component{data=CompData}=_Comp}=State) ->
%%    ReferTo = ?SCR_ARG:get_string(<<"referToUri">>, CompData, <<>>, State),
%%    RCallId = ?SCR_ARG:get_string(<<"replacesCallId">>, CompData, <<>>, State),
%%    RToTag = ?SCR_ARG:get_string(<<"replacesToTag">>, CompData, <<>>, State),
%%    RFromTag = ?SCR_ARG:get_string(<<"replacesFromTag">>, CompData, <<>>, State),
%%    HBlock = case ?SCR_ARG:get_string(<<"huntblockenabled">>, CompData, <<"false">>, State) of
%%                <<"true">> ->
%%                    Code = ?SCR_ARG:get_string(<<"huntblockcode">>, CompData, <<>>, State),
%%                    <<";hunt=", Code/binary>>;
%%                 _ -> <<>>
%%            end,
%%    get_refer_to(ReferTo, RCallId, RToTag, RFromTag, HBlock).
%%
%%%% ----------------------------
%%-spec get_refer_to(UriTo::binary(), CallId::binary(), ToTag::binary(), FromTag::binary(), HBlock::binary()) -> Refer::#refer{}.
%%%% ----------------------------
%%get_refer_to(UriTo, CallId, ToTag, FromTag, HBlock) ->
%%    Uris = ?U:parse_uris(UriTo),
%%    case Uris of
%%        [#uri{}=Uri|_] ->
%%            case {CallId,FromTag,ToTag} of
%%                {<<>>,_,_} -> {error,{invalid_params,<<"'replacesCallId' could not be empty">>}};
%%                {_,<<>>,_} -> {error,{invalid_params,<<"'replacesFromTag' could not be empty">>}};
%%                {_,_,<<>>} -> {error,{invalid_params,<<"'replacesToTag' could not be empty">>}};
%%                _ ->
%%                    ReferToReplace = ?IVR_SCRIPT_UTILS:build_referto_replaces(Uri,{CallId, FromTag, ToTag}, HBlock),
%%                    #refer{referto=fun(_State) -> ReferToReplace end}
%%            end;
%%        _ ->
%%            {error,{invalid_params,<<"Parse param 'referToUri' fail">>}}
%%    end.
