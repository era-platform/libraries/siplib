%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 19.04.2016
%%% @doc
%%% @todo @ivr

-module(r_sip_ivr_script_refer).
-author('Peter Bukashin <tbotc@yandex.ru>').

%%-export([init/1, handle_event/2]).
%%-export([send_refer/2, send_bye/2, assign/3, multi_assign/2]).
%%-export([cmd_prepare_stream/2, cmd_restore_stream/2]).
%%-export([reinvite_stream_mode/1]).
%%
%%%% ====================================================================
%%%% Defines
%%%% ====================================================================
%%
%%-include("r_sip.hrl").
%%-include("r_sip_ivr_script_refer.hrl").
%%
%%-include("r_sip_mgc.hrl").
%%
%%%% ====================================================================
%%%% Public functions
%%%% ====================================================================
%%
%%reinvite_stream_mode(0) -> undefined;
%%reinvite_stream_mode(1) -> auto;
%%reinvite_stream_mode(2) -> sendonly;
%%reinvite_stream_mode(3) -> inactive;
%%reinvite_stream_mode(_) -> undefined.
%%
%%%% ====================================================================
%%%% Callback functions (script)
%%%% ====================================================================
%%
%%%% -------------------------------------
%%init(#state_scr{active_component=#component{state={error,Reason}}}=State) ->
%%    fin({error,Reason}, State);
%%init(#state_scr{active_component=#component{state=#refer{reinvite_mode=undefined}=CS}=Comp}=State) ->
%%    CmdRef = make_ref(),
%%    start_refer(State#state_scr{active_component=Comp#component{state=CS#refer{ref=CmdRef}}});
%%init(#state_scr{active_component=#component{state=#refer{reinvite_mode=Mode}=CS}=Comp}=State) ->
%%    CmdRef = make_ref(),
%%    start_reinvite(Mode,State#state_scr{active_component=Comp#component{state=CS#refer{ref=CmdRef}}}).
%%
%%%% --------------------------------------
%%handle_event(Msg,State) ->
%%    do_handle_event(Msg,State).
%%
%%%% --------------------------
%%%% State == REINVITING1
%%%% --------------------------
%%
%%%% timeout of reinviting on start
%%do_handle_event({timeout_state,Ref,S}, #state_scr{active_component=#component{state=#refer{state=S=?S_REINVITING1,ref=Ref}}}=State) ->
%%    Err = {error,{timeout,<<"Ticket timeout">>}},
%%    fin(Err, State);
%%
%%%% backup remotesdp at start
%%do_handle_event({backup_rsdp,CmdRef,RSdp}, #state_scr{active_component=#component{state=#refer{state=?S_REINVITING1,ref=CmdRef}=CS}=Comp}=State) ->
%%    State1 = case CS of
%%                 #refer{rsdp=undefined} -> State#state_scr{active_component=Comp#component{state=CS#refer{rsdp=RSdp}}};
%%                 _ -> State
%%             end,
%%    {ok, State1};
%%
%%%% backup localsdp at start
%%do_handle_event({backup_lsdp,CmdRef,LSdp}, #state_scr{active_component=#component{state=#refer{state=?S_REINVITING1,ref=CmdRef}=CS}=Comp}=State) ->
%%    State1 = case CS of
%%                 #refer{lsdp=undefined} -> State#state_scr{active_component=Comp#component{state=CS#refer{lsdp=LSdp}}};
%%                 _ -> State
%%             end,
%%    {ok, State1};
%%
%%%% ticket reinvite/mgc error (s = reinviting)
%%do_handle_event({ticket,CmdRef,{error,_}=Err}, #state_scr{active_component=#component{state=#refer{state=?S_REINVITING1,ref=CmdRef,timer_ref=TimerRef}}}=State) ->
%%    erlang:cancel_timer(TimerRef),
%%    fin(Err, State);
%%
%%%% ticket send re-INVITE retry_after (s = wait_ticket)
%%do_handle_event({ticket,CmdRef,{retry_after,Timeout,_}}, #state_scr{active_component=#component{state=#refer{state=?S_REINVITING1, ref=CmdRef, timer_ref=TimerRef}=CS}=Comp}=State) ->
%%    erlang:cancel_timer(TimerRef),
%%    CmdRef1 = make_ref(),
%%    TimerRef1 = ?SCR_COMP:event_after(Timeout, {'retry_reinvite',CmdRef1}),
%%    {ok, State#state_scr{active_component=Comp#component{state=CS#refer{ref=CmdRef1, timer_ref=TimerRef1}}}};
%%
%%%% ticket ok proto setup (s = reinviting)
%%do_handle_event({ticket,CmdRef,{ok,Mode}}, #state_scr{active_component=#component{state=#refer{state=?S_REINVITING1,ref=CmdRef,timer_ref=TimerRef}=CS}=Comp}=State) ->
%%    erlang:cancel_timer(TimerRef),
%%    CmdRef1 = make_ref(),
%%    start_refer(State#state_scr{active_component=Comp#component{state=CS#refer{ref=CmdRef1,
%%                                                                               mode=Mode}}});
%%
%%%% ----------
%%
%%%% restart reinvite after timeout
%%do_handle_event({'retry_reinvite',CmdRef}, #state_scr{active_component=#component{state=#refer{state=?S_REINVITING1,ref=CmdRef,reinvite_mode=Mode}=CS}=Comp}=State) ->
%%    CmdRef1 = make_ref(),
%%    start_reinvite(Mode, State#state_scr{active_component=Comp#component{state=CS#refer{ref=CmdRef1}}});
%%
%%%% ----------
%%
%%%% timeout of wait_ticket
%%do_handle_event({timeout_state,CmdRef,S}, #state_scr{active_component=#component{state=#refer{state=S=?S_WAIT_TICKET,ref=CmdRef}}}=State) ->
%%    Err = {error, {timeout, <<"Ticket timeout">>}},
%%    fin(Err, State);
%%
%%%% ticket send REFER error (s = wait_ticket)
%%do_handle_event({ticket,CmdRef,{error,_}=Err}, #state_scr{active_component=#component{state=#refer{state=?S_WAIT_TICKET,ref=CmdRef,timer_ref=TimerRef}}}=State) ->
%%    erlang:cancel_timer(TimerRef),
%%    fin(Err, State);
%%
%%%% ticket send REFER retry_after (s = wait_ticket)
%%do_handle_event({ticket,CmdRef,{retry_after,Timeout,_}}, #state_scr{active_component=#component{state=#refer{state=?S_WAIT_TICKET,ref=CmdRef,timer_ref=TimerRef}=CS}=Comp}=State) ->
%%    erlang:cancel_timer(TimerRef),
%%    CmdRef1 = make_ref(),
%%    TimerRef1 = ?SCR_COMP:event_after(Timeout, {'retry_refer',CmdRef1}),
%%    {ok, State#state_scr{active_component=Comp#component{state=CS#refer{ref=CmdRef1, timer_ref=TimerRef1}}}};
%%
%%%% ticket send REFER ok (s = wait_ticket)
%%do_handle_event({ticket,CmdRef,{ok,_}}, #state_scr{active_component=#component{state=#refer{state=?S_WAIT_TICKET,ref=CmdRef,timer_ref=TimerRef}=CS}=Comp}=State) ->
%%    erlang:cancel_timer(TimerRef),
%%    TimerRef1 = ?SCR_COMP:event_after(?REFER_TRY_TIMEOUT, {'timeout_state',CmdRef,S=?S_WAIT_RESPONSE}),
%%    {ok, State#state_scr{active_component=Comp#component{state=CS#refer{state=S,
%%                                                                        timer_ref=TimerRef1}}}};
%%
%%%% ----------
%%
%%%% restart refer after timeout
%%do_handle_event({'retry_refer',CmdRef}, #state_scr{active_component=#component{state=#refer{state=?S_WAIT_TICKET,ref=CmdRef}=CS}=Comp}=State) ->
%%    CmdRef1 = make_ref(),
%%    start_refer(State#state_scr{active_component=Comp#component{state=CS#refer{ref=CmdRef1}}});
%%
%%%% ----------
%%
%%%% timeout of pending
%%do_handle_event({timeout_state,CmdRef,S}, #state_scr{active_component=#component{state=#refer{state=S=?S_PENDING,ref=CmdRef}}}=State) ->
%%    start_refer(State);
%%
%%%% ----------
%%
%%%% timeout of wait_response
%%do_handle_event({timeout_state,CmdRef,S}, #state_scr{active_component=#component{state=#refer{state=S=?S_WAIT_RESPONSE,ref=CmdRef}}}=State) ->
%%    Err = {error, {timeout, <<"Refer response timeout">>}},
%%    fin(Err, State);
%%
%%%% sip response on REFER received (s = wait_response)
%%do_handle_event({uac_response,#sipmsg{class={resp,SipCode,_},cseq={CSeq,'REFER'}}=_Resp},
%%             #state_scr{active_component=#component{data=Data,state=#refer{state=ST,ref=CmdRef,timer_ref=TimerRef,pendings=PT}=CS}=Comp}=State)
%%  when ST==?S_WAIT_RESPONSE; ST==?S_TRYING ->
%%    erlang:cancel_timer(TimerRef),
%%    case SipCode of
%%        100 ->
%%            TimerRef1 = ?SCR_COMP:event_after(?REFER_TRY_TIMEOUT, {'timeout_state',CmdRef,S=?S_TRYING}),
%%            {ok, State#state_scr{active_component=Comp#component{state=CS#refer{state=S,
%%                                                                                timer_ref=TimerRef1}}}};
%%        _N when _N>=200, _N=<299  ->
%%            TimerRef1 = ?SCR_COMP:event_after(?REFER_WAIT_TIMEOUT, {'timeout_state',CmdRef,S=?S_ACCEPTED}),
%%            {ok, State#state_scr{active_component=Comp#component{state=CS#refer{state=S,
%%                                                                                timer_ref=TimerRef1,
%%                                                                                refer_event_id=CSeq}}}};
%%        _N when _N==491, PT < 20 ->
%%            Timeout = ?EU:random(300,700) + case PT < 3 of true -> 0; false -> 1000 end,
%%            TimerRef1 = ?SCR_COMP:event_after(Timeout, {'timeout_state',CmdRef,S=?S_PENDING}),
%%            {ok, State#state_scr{active_component=Comp#component{state=CS#refer{state=S,
%%                                                                                timer_ref=TimerRef1,
%%                                                                                pendings=PT+1}}}};
%%        N when N>=300 ->
%%            Var = ?SCR_COMP:get(<<"sipRejectCodeVar">>, Data, <<>>),
%%            State1 = assign(Var, N, State),
%%            restore_reinvite(refer_rejected, State1);
%%        _N ->
%%            {ok, State}
%%    end;
%%
%%%% timeout of trying
%%do_handle_event({timeout_state,CmdRef,S}, #state_scr{active_component=#component{state=#refer{state=S=?S_TRYING,ref=CmdRef}}}=State) ->
%%    restore_reinvite(timeout, State);
%%
%%%% timeout of accepted
%%do_handle_event({timeout_state,CmdRef,S}, #state_scr{active_component=#component{state=#refer{state=S=?S_ACCEPTED,ref=CmdRef}}}=State) ->
%%    restore_reinvite(timeout, State);
%%
%%%% ----------
%%
%%%% sip request NOTIFY received
%%do_handle_event({sip_notify,Req}, #state_scr{active_component=#component{state=#refer{state=?S_ACCEPTED}=CS}=Comp}=State) ->
%%    #refer{ref=CmdRef, timer_ref=TimerRef, refer_event_id=EventId}=CS,
%%    BEventId = ?EU:to_binary(EventId),
%%    case Req#sipmsg.event of
%%        {<<"refer">>, [{<<"id">>,BEventId}]} ->
%%            #sipmsg{body=Body}=Req,
%%            Fdefault = fun() -> case ?SUBSTATE(Req) of
%%                                    invalid ->
%%                                        {ok, State};
%%                                    {active, Expires} ->
%%                                        erlang:cancel_timer(TimerRef),
%%                                        TimerRef1 = ?SCR_COMP:event_after(Expires*1000, {'timeout_state',CmdRef,?S_ACCEPTED}),
%%                                        {ok, State#state_scr{active_component=Comp#component{state=CS#refer{timer_ref=TimerRef1}}}};
%%                                    {pending, Expires} ->
%%                                        erlang:cancel_timer(TimerRef),
%%                                        TimerRef1 = ?SCR_COMP:event_after(Expires*1000, {'timeout_state',CmdRef,?S_ACCEPTED}),
%%                                        {ok, State#state_scr{active_component=Comp#component{state=CS#refer{timer_ref=TimerRef1}}}};
%%                                    {terminated,_,_} ->
%%                                        erlang:cancel_timer(TimerRef),
%%                                        fin(sub_terminated, State)
%%                                end end,
%%            case Body of
%%                <<"SIP/2.0 ", Rest/binary>> ->
%%                    [RCode|RestReason] = lists:filter(fun(<<>>) -> false; (_) -> true end, binary:split(Rest, <<" ">>, [global])),
%%                    case ?EU:to_integer(RCode) of
%%                        X when X =< 199 ->
%%                            Fdefault();
%%                        X when X =< 299 ->
%%                            erlang:cancel_timer(TimerRef),
%%                            #state_scr{ownerpid=OwnerPid}=State,
%%                            OwnerPid ! {scriptmachine, {ivr_cmd, CmdRef, {ext, ?MODULE, send_bye, self(), #{}}}},
%%                            TimerRef1 = ?SCR_COMP:event_after(?TIMEOUT, {'timeout_state',CmdRef,S=?S_WAIT_BYE}),
%%                            {ok, State#state_scr{active_component=Comp#component{state=CS#refer{state=S,
%%                                                                                                timer_ref=TimerRef1}}}};
%%                        X ->
%%                            VarList = [{?VarResCode, <<"008">>},
%%                                       {?VarResDescr, <<"Refer rejected">>},
%%                                       {?VarResSipCode, ?EU:to_binary(X)},
%%                                       {?VarResSipReas, ?EU:join_binary(RestReason, <<" ">>)}],
%%                            State1 = multi_assign(VarList,State),
%%                            erlang:cancel_timer(TimerRef),
%%                            restore_reinvite(invite_rejected, State1)
%%                        % ----------------------------
%%                    end;
%%                _ -> Fdefault()
%%            end;
%%        _ -> {ok, State}
%%    end;
%%
%%%% ----------
%%
%%%% timeout of wait_ticket
%%do_handle_event({timeout_state,CmdRef,S}, #state_scr{active_component=#component{state=#refer{state=S=?S_WAIT_BYE,ref=CmdRef}}}=State) ->
%%    Err = {error, {timeout, <<"Ticket timeout">>}},
%%    fin(Err, State);
%%
%%%% ticket send REFER error (s = wait_bye)
%%do_handle_event({ticket,CmdRef,{error,_}=Err}, #state_scr{active_component=#component{state=#refer{state=?S_WAIT_BYE, ref=CmdRef, timer_ref=TimerRef}}}=State) ->
%%    erlang:cancel_timer(TimerRef),
%%    fin(Err, State);
%%
%%%% ticket send REFER ok (s = wait_bye)
%%do_handle_event({ticket,CmdRef,ok}, #state_scr{active_component=#component{state=#refer{state=?S_WAIT_BYE, ref=CmdRef, timer_ref=TimerRef}}}=State) ->
%%    erlang:cancel_timer(TimerRef),
%%    fin(ok,State);
%%do_handle_event({ticket,CmdRef,{ok,_}}, #state_scr{active_component=#component{state=#refer{state=?S_WAIT_BYE, ref=CmdRef, timer_ref=TimerRef}}}=State) ->
%%    erlang:cancel_timer(TimerRef),
%%    fin(ok,State);
%%
%%%% --------------------------
%%%% State == REINVITING2
%%%% --------------------------
%%
%%%% timeout of reinviting on stop (s = reinviting2)
%%do_handle_event({timeout_state,Ref,S}, #state_scr{active_component=#component{state=#refer{state=S=?S_REINVITING2,ref=Ref}}}=State) ->
%%    Err = {error,{timeout,<<"Ticket timeout">>}},
%%    fin(Err, State);
%%
%%%% ticket reinviting on stop error (s = reinviting2)
%%do_handle_event({ticket,CmdRef,{error,_}=Err}, #state_scr{active_component=#component{state=#refer{state=?S_REINVITING2,ref=CmdRef,timer_ref=TimerRef}}}=State) ->
%%    erlang:cancel_timer(TimerRef),
%%    fin(Err, State);
%%
%%%% ticket reinviting on stop ok (s = reinviting2)
%%do_handle_event({ticket,CmdRef,ok}, #state_scr{active_component=#component{state=#refer{state=?S_REINVITING2,ref=CmdRef,timer_ref=TimerRef,finmode=FinMode}}}=State) ->
%%    erlang:cancel_timer(TimerRef),
%%    fin(FinMode, State);
%%
%%%% ----------
%%
%%%% sip request INFO received
%%do_handle_event({sip_info,_Req}, State) ->
%%    {ok, State};
%%
%%%% sip request MESSAGE received
%%do_handle_event({sip_message,_Req}, State) ->
%%    {ok, State};
%%
%%%% ----------
%%
%%do_handle_event(_Ev, State) ->
%%    {ok, State}.
%%
%%%% ====================================================================
%%%% Internal functions
%%%% ====================================================================
%%
%%start_refer(#state_scr{ownerpid=OwnerPid, active_component=#component{state=#refer{ref=CmdRef,referto=ReferTo}=CS}=Comp}=State) ->
%%    %?OUT("REFER. Start refer"),
%%    OwnerPid ! {scriptmachine, {ivr_cmd, CmdRef, {ext, ?MODULE, send_refer, self(), #{referto => ReferTo}}}},
%%    TimerRef1 = ?SCR_COMP:event_after(?TIMEOUT, {'timeout_state',CmdRef,S=?S_WAIT_TICKET}),
%%    {ok, State#state_scr{active_component=Comp#component{state=CS#refer{state=S,
%%                                                                        ref=CmdRef,
%%                                                                        timer_ref=TimerRef1}}}}.
%%
%%% ---
%%% when starting - switch to sendonly/inactive
%%start_reinvite(Mode, #state_scr{ownerpid=OwnerPid, active_component=#component{state=#refer{ref=CmdRef}=CS}=Comp}=State)
%%  when Mode==auto; Mode==sendonly; Mode==inactive ->
%%    %?OUT("REFER. Start reinvite"),
%%    OwnerPid ! {scriptmachine, {ivr_cmd, CmdRef, {ext, ?MODULE, cmd_prepare_stream, self(), #{mode => Mode}}}},
%%    TimerRef1 = ?SCR_COMP:event_after(?TIMEOUT, {'timeout_state',CmdRef,S=?S_REINVITING1}),
%%    {ok, State#state_scr{active_component=Comp#component{state=CS#refer{reinvite_mode=Mode,
%%                                                                        state=S,
%%                                                                        ref=CmdRef,
%%                                                                        timer_ref=TimerRef1}}}}.
%%
%%% when stopping by failure - switch back to sendrecv/recvonly/inactive
%%restore_reinvite(FinMode, #state_scr{active_component=#component{state=#refer{reinvite_mode=undefined}}}=State) ->
%%    fin(FinMode,State);
%%restore_reinvite(FinMode, #state_scr{ownerpid=OwnerPid, active_component=#component{state=#refer{ref=CmdRef}=CS}=Comp}=State) ->
%%    %?OUT("REFER. Restore reinvite"),
%%    OwnerPid ! {scriptmachine, {ivr_cmd, CmdRef, {ext, ?MODULE, cmd_restore_stream, self(), CS}}},
%%    TimerRef1 = ?SCR_COMP:event_after(?TIMEOUT, {'timeout_state',CmdRef,S=?S_REINVITING2}),
%%    {ok, State#state_scr{active_component=Comp#component{state=CS#refer{finmode=FinMode,
%%                                                                        state=S,
%%                                                                        ref=CmdRef,
%%                                                                        timer_ref=TimerRef1}}}}.
%%
%%
%%%% ====================================================================
%%%% Functional iface of SIP IVR dlg (linked to its fsm)
%%%% ====================================================================
%%
%%%% -------------------------------
%%%% reinvite to inactive (copy from fax)
%%%% -------------------------------
%%
%%% @private
%%cmd_prepare_stream({Ref, FromPid, _Opts}, #state_ivr{a=#side{state=S}}=State) when S/=dialog ->
%%    %?OUT("REFER. prepare stream err"),
%%    ?SCR_COMP:event(FromPid, {'ticket',Ref,{error,{invalid_operation,<<"Refer could not be started, state mismatch">>}}}),
%%    {next_state, ?CURRSTATE, State};
%%cmd_prepare_stream({Ref, FromPid, _OptsMap}, #state_ivr{acked=false,a=#side{dir='in'}}=State) ->
%%    %?OUT("REFER. prepare stream wait ack"),
%%    ?SCR_COMP:event(FromPid, {'ticket',Ref,{retry_after,100,<<"Dialog was not acked yet">>}}),
%%    {next_state, ?CURRSTATE, State};
%%cmd_prepare_stream({_Ref, _FromPid, Opts}=P, #state_ivr{a=#side{state=S}}=State) when S==dialog -> % #277 acked=true or dir=out
%%    %?OUT("REFER. prepare stream"),
%%    Mode = maps:get(mode,Opts),
%%    do_cmd_prepare_stream(P, Mode, #state_ivr{a=#side{state=S}}=State).
%%
%%% @private
%%do_cmd_prepare_stream(P, 'auto', State) ->
%%    do_cmd_prepare_stream(P, 'inactive', State);
%%do_cmd_prepare_stream({Ref, FromPid, _}=P, Mode, #state_ivr{a=#side{remotesdp=RSdp}}=State) when Mode=='inactive'; Mode=='sendonly' ->
%%    %?OUT("REFER. do_cmd_prepare stream sendrecv=~500tp, mode=~500tp", [?M_SDP:check_sendrecv(RSdp), Mode]),
%%    case check_sdp(Mode, RSdp) of
%%        true ->
%%            %?OUT("REFER. do_cmd_prepare stream. Sdp Ok"),
%%            ?SCR_COMP:event(FromPid, {'ticket',Ref,{ok,Mode}}),
%%            {next_state, ?CURRSTATE, State};
%%        false ->
%%            %?OUT("REFER. do_cmd_prepare stream. Reinvite ~500tp", [Mode]),
%%            ?SCR_COMP:event(FromPid, {'backup_rsdp',Ref,RSdp}),
%%            do_cmd_reinvite(P, Mode, State)
%%    end.
%%
%%% @private
%%check_sdp(Mode,Sdp) ->
%%    case {?EU:to_binary(Mode), ?EU:to_binary(?M_SDP:get_audio_mode(Sdp))} of
%%        {<<"sendonly">>,<<"recvonly">>} -> true;
%%        {<<"sendonly">>,<<"inactive">>} -> true;
%%        {<<"inactive">>,<<"inactive">>} -> true;
%%        {<<"inactive">>,<<"recvonly">>} -> true;
%%        _ -> false
%%    end.
%%
%%% @private
%%% make reinvite and prepare response fun
%%do_cmd_reinvite({Ref, FromPid, _}=_P, Mode, State) ->
%%    FunBackupL = fun(LSdp) -> ?SCR_COMP:event(FromPid, {'backup_lsdp',Ref,LSdp}) end,
%%    FunResult = fun({error,_}=Err) ->
%%                        %?OUT("Err: ~120p",[Err]),
%%                        ?SCR_COMP:event(FromPid, {'ticket',Ref,Err});
%%                   ({ok,RSdp}) ->
%%                        %?OUT("REFER. reinvite result: ~500tp, ~s, ~500tp", [RSdp,Mode,?M_SDP:get_audio_mode(RSdp)]),
%%                        case check_sdp(Mode, RSdp) of
%%                            true -> ?SCR_COMP:event(FromPid, {'ticket',Ref,{ok,Mode}});
%%                            false -> ?SCR_COMP:event(FromPid, {'ticket',Ref,{error,{operation_failed, <<"Refer media could not be set">>}}})
%%                        end,
%%                        ok
%%                end,
%%    ?ACTIVE_REINVITE:send_reinvite(Mode, FunBackupL, FunResult, State).
%%
%%%% -------------------------------
%%%% reinvite back (to sendrecv) restore (copy from fax)
%%%% -------------------------------
%%
%%cmd_restore_stream({_Ref, _FromPid, _Args}=P, #state_ivr{a=#side{state=S}}=State) when S==dialog ->
%%    %?OUT("REFER. restore_stream"),
%%    do_cmd_restore_stream(P, #state_ivr{a=#side{state=S}}=State);
%%cmd_restore_stream({Ref, FromPid, _Args}, #state_ivr{}=State) ->
%%    %?OUT("REFER. restore_stream err"),
%%    ?SCR_COMP:event(FromPid, {'ticket',Ref,{error,{invalid_operation,<<"Fax could not be rolled back, state mismatch">>}}}),
%%    {next_state, ?CURRSTATE, State}.
%%
%%% @private
%%do_cmd_restore_stream({Ref, FromPid, #refer{lsdp=LSdp1}}=P, State) ->
%%    %?OUT("do_cmd_restore_stream"),
%%    case LSdp1 of
%%        undefined ->
%%            %?OUT("REFER. do_cmd_restore_stream. Ok, notchanged"),
%%            ?SCR_COMP:event(FromPid, {'ticket',Ref,ok}),
%%            {next_state, ?CURRSTATE, State};
%%        #sdp{} ->
%%            do_cmd_reinvite_restore(P, ?M_SDP:sendrecv(LSdp1), State)
%%    end.
%%
%%% @private
%%% make reinvite and prepare response fun
%%do_cmd_reinvite_restore({Ref, FromPid, _}=_P, LSdp, State) ->
%%    FunResult = fun({error,_}=Err) -> ?SCR_COMP:event(FromPid, {'ticket',Ref,Err});
%%                   ({ok,_RSdp}) ->
%%                        %?OUT("REFER. reinvite result: ~500tp", [_RSdp]),
%%                        ?SCR_COMP:event(FromPid, {'ticket',Ref,ok}),
%%                        ok
%%                end,
%%    ?ACTIVE_REINVITE:send_reinvite(LSdp, undefined, FunResult, State).
%%
%%%% -----------------------------------------------
%%%% refer (ivr fsm)
%%%% -----------------------------------------------
%%send_refer({_Ref, _FromPid, _OptsMap}=P, #state_ivr{a=#side{state=S}}=State) when S==dialog ->
%%    do_send_refer_0(P, State);
%%send_refer({Ref, FromPid, _OptsMap}, State) ->
%%    %?OUT("REFER. send refer dialog state mismatch"),
%%    ?SCR_COMP:event(FromPid, {'ticket',Ref,{error,{invalid_operation,<<"Call could not be referred, state mismatch">>}}}),
%%    {next_state, ?CURRSTATE, State}.
%%
%%% @private
%%do_send_refer_0({Ref, FromPid, _OptsMap}, #state_ivr{acked=false,a=#side{dir='in'}}=State) ->
%%    %?OUT("REFER. send refer wait ack"),
%%    ?SCR_COMP:event(FromPid, {'ticket',Ref,{retry_after,100,<<"Dialog was not acked yet">>}}),
%%    {next_state, ?CURRSTATE, State};
%%do_send_refer_0(P, State) -> % #277 acked=true or dir=out
%%    %?OUT("REFER. send refer"),
%%    do_send_refer_1(P, State).
%%
%%% @private
%%do_send_refer_1({Ref, FromPid, OptsMap}, State) ->
%%    %RefTo = #uri{scheme=sip, user=maps:get(referto, OptsMap), domain=maps:get(domain,Map)},
%%    GetReferToUri = maps:get(referto, OptsMap),
%%    case erlang:is_function(GetReferToUri,1) of
%%        true ->
%%            RefTo = GetReferToUri(State),
%%            do_send_refer_2(Ref, FromPid, RefTo, State);
%%        false ->
%%            ?SCR_COMP:event(FromPid, {'ticket',Ref,{error,{internal_error,<<"Error get referto">>}}}),
%%            {next_state, ?CURRSTATE, State}
%%    end.
%%
%%% @private
%%do_send_refer_2(Ref, FromPid, RefTo, State) ->
%%    #state_ivr{map=Map, a=Side}=State,
%%    RefBy = ?U:make_uri(maps:get(aor,Map)),
%%    ReferOpts = [{refer_to, RefTo},
%%                 {add, {?ReferredBy, ?U:unparse_uri(RefBy)}},
%%                 {event, <<"refer">>},
%%                 user_agent],
%%    case send_refer_offer(ReferOpts, Side, State) of
%%        error ->
%%            ?SCR_COMP:event(FromPid, {'ticket',Ref,{error,{internal_error,<<"Error sending refer">>}}}),
%%            {next_state, ?CURRSTATE, State};
%%        {ok,_YReqHandle} ->
%%            ?SCR_COMP:event(FromPid, {'ticket',Ref,{ok,started}}),
%%            {next_state, ?CURRSTATE, State}
%%    end.
%%% @private
%%send_refer_offer(ReferOpts, Side, State) ->
%%    #side{dhandle=DlgHandle,
%%          localtag=LTag}=Side,
%%    case catch nksip_uac:refer(DlgHandle, [async|ReferOpts]) of
%%        {'EXIT',Err} ->
%%            d(State, "send_refer_offer to LTag=~500tp, Caught error=~500tp", [LTag, Err]),
%%            error;
%%        {error,_R}=Err ->
%%            d(State, "send_refer_offer to LTag=~500tp, Error=~500tp", [LTag, Err]),
%%            error;
%%        {async,ReqHandle} ->
%%            d(State, "send_refer_offer to LTag=~500tp", [LTag]),
%%            {ok,ReqHandle}
%%    end.
%%
%%%% -----------
%%%% bye (ivr fsm)
%%%% -----------
%%send_bye({Ref, FromPid, _OptsMap}, #state_ivr{a=#side{state=S}=Side}=State) when S==dialog ->
%%    {ok,State1} = ?ACTIVE_UTILS:do_stop_bye(State),
%%    State2 = State1#state_ivr{a=Side#side{state=bye,
%%                                          finaltime=os:timestamp()}},
%%    ?SCR_COMP:event(FromPid, {'ticket',Ref,ok}),
%%    ?IVR_UTILS:return_stopping(?IVR_UTILS:do_finalize_ivr(State2));
%%send_bye({Ref, FromPid, _OptsMap}, State) ->
%%    ?SCR_COMP:event(FromPid, {'ticket',Ref,{error,{invalid_operation,<<"Call could not be stopped, state mismatch">>}}}),
%%    {next_state, ?CURRSTATE, State}.
%%
%%%% ====================================================================
%%%% Internal functions
%%%% ====================================================================
%%
%%%% finalize
%%fin({error,_}=Err, _State) -> Err;
%%fin(refer_rejected, State) -> ?SCR_COMP:next([<<"transferRejected">>], State);
%%fin(invite_rejected, State) -> ?SCR_COMP:next([<<"transferRejected">>], State);
%%fin(sub_terminated, State) -> ?SCR_COMP:next(<<"transferSubTerminated">>, State);
%%fin(timeout, State) -> ?SCR_COMP:next([<<"transferTimeout">>], State);
%%fin(ok, State) -> {stop,State}. % stop main script body, normal type
%%
%%%% assign value to variable
%%assign(Var, Value, #state_scr{active_component=#component{data=Data}}=State) ->
%%    VarId = case ?EU:get_by_key(Var,Data,undefined) of
%%                undefined -> Var;
%%                Id -> Id
%%            end,
%%    case ?SCR_VAR:set_value(VarId, Value, State) of
%%        {ok, State1} -> State1;
%%        _R -> State
%%    end.
%%%--------- attended ---------
%%multi_assign([], State) -> State;
%%multi_assign([{Var, Value}|T], State) ->
%%    NewState = assign(Var, Value, State),
%%    multi_assign(T, NewState).
%%
%%% @private ----
%%%d(State, Text) -> d(State, Text, []).
%%d(State, Fmt, Args) ->
%%    #state_ivr{ivrid=IvrId}=State,
%%    ?LOGSIP("IVR fsm ~500tp '~500tp':" ++ Fmt, [IvrId,?CURRSTATE] ++ Args).
