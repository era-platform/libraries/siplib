
%% ====================================================================
%% Types
%% ====================================================================

%-include("../../era_script/include/r_script_export.hrl").

-include("../include/r_sip_ivr.hrl").
-include("../include/r_sip_nk.hrl").
-include("../include/r_sip_headers.hrl").

%% ====================================================================
%% Define modules
%% ====================================================================

-define(SUBSTATE, nksip_subscription_lib:state).

%-define(U, r_sip_utils).

-define(REFER, r_sip_ivr_script_refer).

%% ====================================================================
%% Define
%% ====================================================================
-define(CURRSTATE, 'active').

-define(TIMEOUT, 10000).
-define(TIMEOUT_REINVITE, 16500).

-define(STAGE_TRYING, refer_trying).
-define(REFER_TRY_TIMEOUT, 30000).
-define(REFER_WAIT_TIMEOUT, 60000).
-define(PENDING_TIMEOUT, 500).

-define(S_REINVITING1, 'reinviting1').
-define(S_REINVITING2, 'reinviting2').
-define(S_WAIT_TICKET, 'wait_ticket').
-define(S_WAIT_RESPONSE, 'wait_response').
-define(S_PENDING, 'pending').
-define(S_TRYING, 'trying').
-define(S_ACCEPTED, 'accepted').
-define(S_WAIT_BYE, 'wait_bye').

%--------- attended ---------
%% component results in variables
-define(VarResCode,<<"resultCode">>).
-define(VarResDescr,<<"resultDescription">>).
-define(VarResSipCode,<<"resultSipCode">>).
-define(VarResSipReas,<<"resultSipReason">>).
-define(VarResSipHeaders,<<"resultSipHeaders">>).

%% ====================================================================
%% Records
%% ====================================================================

-record(refer, {
    state :: wait_ticket | wait_response | trying | accepted,
    ref,
    timer_ref,
    pendings=0,
    refer_event_id,
    referto = undefined :: fun((Elem :: #state_ivr{}) -> Uri :: #uri{}),
    % reinvite
    reinvite_mode = undefined,
    finmode = undefined,
    mode = sendrecv,
    rsdp = undefined,
    lsdp = undefined,
    % refer attended
    dialerpid,
    dialermonitorpid
  }).
