%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 5.01.2017
%%% @doc IVR script metadata.
%%%        Returns ivr components for script-machine and metadata
%%%        Returns ivr expression functions for metadata
%%%        Returns common metadata for ivr components
%%%        Returns metadata expression functions

-module(r_sip_ivr_script_metadata).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([components/0,
         component_sort_index/1,
         component_meta_props/2,
         image/1,
         expression_funs/1,
         meta_expr/0,
         default_funs/0]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_nk.hrl").
-include("../include/r_sip_ivr.hrl").

-define(InvalidMode, <<"invalid_mode">>).
-define(InvalidKey, <<"invalid_key">>).

%% ====================================================================
%% API functions
%% ====================================================================

% return ivr component modules
components() ->
    [{201, r_sip_ivr_script_component_bye},
     {202, r_sip_ivr_script_component_answer},
     {203, r_sip_ivr_script_component_refer},
     {204, r_sip_ivr_script_component_play},
     {205, r_sip_ivr_script_component_playnum},
     {206, r_sip_ivr_script_component_dtmf},
     {207, era_sip_ivr_script_component_senddtmf},
     {208, r_sip_ivr_script_component_record},
     {213, r_sip_ivr_script_component_fax_send},
     {214, r_sip_ivr_script_component_fax_recv},
     {215, r_sip_ivr_script_component_refer_replace},
     {217, r_sip_ivr_script_component_refer_attended},
     {218, r_sip_ivr_script_component_parking},
     {219, r_script_component_operation_ivr},
     {220, r_sip_ivr_script_component_tts},
     {221, r_sip_ivr_script_component_asr},
     {222, r_sip_ivr_script_component_tts_yandex},
     {223, r_sip_ivr_script_component_asr_yandex},
     {224, era_sip_ivr_script_component_api},
     {225, era_sip_ivr_script_component_answer_detector},
     %----------ghost----------%
     {50001, r_sip_ivr_script_component_ghost_hunt}].

% return common component sort index
%  use from metadata
-spec component_sort_index(IdT::integer()) -> integer().

component_sort_index(Id) ->
    case Id of
        201 -> 211;
        202 -> 210;
        203 -> 221;
        204 -> 201;
        205 -> 202;
        206 -> 203;
        207 -> 204;
        208 -> 205;
        213 -> 280;
        214 -> 281;
        215 -> -1;
        217 -> 222;
        218 -> 252;
        219 -> 300;
        220 -> -1;
        221 -> -1;
        222 -> 232;
        223 -> 233;
        224 -> 350;
        225 -> 351;
        50001 -> -1;
        _ -> 10000
    end.

% return basic metadata of component by type
component_meta_props(IdT,Lang) -> object_meta(IdT,Lang).

% return imageb64 for custom components
image(_IdT) -> <<>>.

% return default ivr expression funs (for script-machine)
%  NOTE, some ivr expression functions defined in supra system, (ex.queue) and supplied as parameters in run-time
%  So, this method could not be used to define metadata, only on execution time.
expression_funs(#state_ivr{map=Map}=_State) ->
    CallParams = maps:get(call_params,Map),
    FromUriUser = case maps:get(init_req, Map, undefined) of undefined -> ?InvalidMode; #sipmsg{from={#uri{user=U}, _}} -> U end,
    StartParams = maps:get(startparams,Map,#{}), % RP-759
    F_invalid0 = fun() -> ?InvalidMode end,
    F_invalid1 = fun(_) -> ?InvalidMode end,
    EF1 = maps:get(expression_funs,Map,[]), % priority
    EF2 = [% usual ivr functions
           {{'startparam',1}, fun(X) -> maps:get(X,StartParams,undefined) end},
           {{'ivrparam',1}, fun(X) -> ?EU:get_by_key(?EU:to_binary(X),CallParams,?InvalidKey) end},
           {{'callednum',0}, fun() -> ?EU:get_by_key(<<"called">>,CallParams,?InvalidMode) end},
           {{'calledext',0}, fun() -> ?EU:get_by_key(<<"ext">>,CallParams,?InvalidMode) end},
           {{'remotenum',0}, fun() -> FromUriUser end},
           {{'callid',0}, fun() -> apply_script_state(fun(#state_ivr{acallid=ACallId}) -> ACallId end, <<>>) end},
           % RP-1468 hunt's preivrscript functions (default values, if no override)
           {{'q_displayname',0}, F_invalid0},
           {{'q_username',0}, F_invalid0},
           {{'q_domain',0}, F_invalid0},
           {{'r_number',0}, F_invalid0},
           % RP-1468 hunt's qivrscript functions (default values, if no override),
           {{'huntid',0}, F_invalid0},
           {{'huntobjid',0}, F_invalid0},
           {{'queuepriority',0}, F_invalid0},
           {{'queuepriority',1}, F_invalid1},
           {{'queueposition',0}, F_invalid0},
           {{'queueestimatedsec',0}, F_invalid0},
           {{'queuerescount',0}, F_invalid0},
           {{'queuerescount',1}, F_invalid1},
           {{'queuestate',0}, F_invalid0}],
    lists:ukeymerge(1,lists:ukeysort(1,EF1), lists:ukeysort(1,EF2)).

% return ivr expression funs (for metadata)
meta_expr() ->
    QueueFuns = case ?ENVCFG:get_nodes_containing_role_dyncfg_check_free('huntq') of
                    [] -> [];
                    _ -> queue_funs()
                end,
    QueueFuns ++ default_funs().

%% ====================================================================
%% Private functions
%% ====================================================================

% #306
% return result of function over ivr_state.
% economly applied in ivr machine process! economly take ivr pid directly in script process!
% used in ivr expression functions
apply_script_state(Fapply,Default) when is_function(Fapply,1) ->
    case apply_script_state(Fapply) of
        {error,_} -> Default;
        T -> T
    end.
apply_script_state(Fapply) when is_function(Fapply,1) ->
    Fowner = fun(State) -> ?ScriptMachine:get_ownerpid(State) end,
    case ?ScriptExpressions:apply_script_state(Fowner) of
        {error,_}=Err -> Err;
        {ok,undefined} -> {error,invalid_state};
        {ok,OwnerPid} when is_pid(OwnerPid) ->
            {Self,Ref} = {self(),make_ref()},
            F = fun({RefX,FromPid,_OptsMap}, #state_ivr{}=State) -> FromPid ! {ok,RefX,Fapply(State)}, ok end,
            OwnerPid ! {scriptmachine, {ivr_cmd, Ref, {extf, F, Self, #{}}}},
            receive {ok,Ref,Res} -> Res
            after 5000 -> {error,timeout}
            end end.

% ----------------------
% @private
default_funs() ->
    [{"ivrparam","str"},
     {"callednum",""},
     {"calledext",""},
     {"remotenum",""},
     {"callid",""}].

queue_funs() ->
    [% hunt's qivrscript functions
     {"huntid",""},
     {"huntobjid",""},
     {"queuepriority",""},
     {"queuepriority","int"},
     {"queueposition",""},
     {"queueestimatedsec",""},
     {"queuerescount",""},
     {"queuerescount","str"}, % ?
     {"queuestate",""},
     % RP-1468 hunt's preivrscript functions
     {"q_displayname",""},
     {"q_username",""},
     {"q_domain",""},
     {"r_number",""}].

% ----------------------
% @private
object_meta(201,Lang) ->
    [{<<"defaultName">>, lang(Lang,"Отбой")},
     {<<"hint">>, lang(Lang,"Отбой (SIP-запрос BYE)")},        % Отправка SIP-запроса BYE
     {<<"serialIndex">>, 2020} | group(pbx,Lang)];

object_meta(202,Lang) ->
    [{<<"defaultName">>, lang(Lang,"Ответ")},
     {<<"hint">>, lang(Lang,"SIP-ответ")},                     % Отправка SIP-ответа
     {<<"serialIndex">>, 2010} | group(pbx,Lang)];

object_meta(203,Lang) ->
    [{<<"defaultName">>, lang(Lang,"Перевод")},
     {<<"hint">>, lang(Lang,"Перевод без сопровождения")},    % Отправка SIP-запроса REFER
     {<<"serialIndex">>, 2080} | group(pbx,Lang)];

object_meta(204,Lang) ->
    [{<<"defaultName">>, lang(Lang,"Воспроизведение")},
     {<<"hint">>, lang(Lang,"Воспроизведение")},
     {<<"serialIndex">>, 2030} | group(pbx,Lang)];

object_meta(205,Lang) ->
    [{<<"defaultName">>, lang(Lang,"Воспроизведение числительных")},
     {<<"hint">>, lang(Lang,"Воспроизведение числительных")},
     {<<"serialIndex">>, 2060} | group(pbx,Lang)];

object_meta(206,Lang) ->
    [{<<"defaultName">>, lang(Lang,"Ввод DTMF")},
     {<<"hint">>, lang(Lang,"Ввод DTMF")},
     {<<"serialIndex">>, 2050} | group(pbx,Lang)];

object_meta(207,Lang) ->
    [{<<"defaultName">>, lang(Lang,"Генерация DTMF")},
     {<<"hint">>, lang(Lang,"Генерация DTMF")},
     {<<"serialIndex">>, 2070} | group(pbx,Lang)];

object_meta(208,Lang) ->
    [{<<"defaultName">>, lang(Lang,"Запись")},
     {<<"hint">>, lang(Lang,"Запись")},
     {<<"serialIndex">>, 2040} | group(pbx,Lang)];

object_meta(213,Lang) ->
    [{<<"defaultName">>, lang(Lang,"Отправка факса")},
     {<<"hint">>, lang(Lang,"Отправка факсимильного сообщения")},
     {<<"serialIndex">>, 2070} | group(messaging,Lang)];

object_meta(214,Lang) ->
    [{<<"defaultName">>, lang(Lang,"Прием факса")},
     {<<"hint">>, lang(Lang,"Прием факсимильного сообщения")},
     {<<"serialIndex">>, 2072} | group(messaging,Lang)];

object_meta(215,Lang) ->
    [{<<"defaultName">>, lang(Lang,"Замещение")},
     {<<"hint">>, lang(Lang,"Замещение (SIP-запрос REFER+replace)")},     % Отправка SIP-запроса REFER+replace
     {<<"serialIndex">>, 2084} | group(pbx,Lang)];

object_meta(217,Lang) ->
    [{<<"defaultName">>, lang(Lang,"Перевод с сопровождением")},
     {<<"hint">>, lang(Lang,"Перевод с сопровождением (Вызов, ожидание, SIP-запрос REFER+replace)")},  % Вызов, ожидание и REFER+replace
     {<<"serialIndex">>, 2082} | group(pbx,Lang)];

object_meta(218,Lang) ->
    [{<<"defaultName">>, lang(Lang,"Парковка")},
     {<<"hint">>, lang(Lang,"Постановка и взятие с парковки")},
     {<<"serialIndex">>, 2090} | group(pbx,Lang)];

object_meta(219,Lang) ->
    [{<<"defaultName">>, lang(Lang,"Операция")},
     {<<"hint">>, lang(Lang,"Операция")},
     {<<"serialIndex">>, 2095} | group(management,Lang)];

object_meta(220,Lang) ->
    [{<<"defaultName">>, lang(Lang,"Синтез")},
     {<<"hint">>, lang(Lang,"Синтез речи (MRCPv2)")},
     {<<"serialIndex">>, 2096} | group(pbx,Lang)];

object_meta(221,Lang) ->
    [{<<"defaultName">>, lang(Lang,"Распознавание речи")},
     {<<"hint">>, lang(Lang,"Распознавание речи (MRCPv2)")},
     {<<"serialIndex">>, 2097} | group(pbx,Lang)];

object_meta(222,Lang) ->
    [{<<"defaultName">>, lang(Lang,"Синтез речи")},
     {<<"hint">>, lang(Lang,"Синтез речи на Yandex Cloud SpeechKit")},
     {<<"serialIndex">>, 2098} | group(pbx,Lang)];

object_meta(223,Lang) ->
    [{<<"defaultName">>, lang(Lang,"Распознавание речи")},
     {<<"hint">>, lang(Lang,"Распознавание речи на Yandex Cloud SpeechKit")},
     {<<"serialIndex">>, 2099} | group(pbx,Lang)];

object_meta(224,Lang) ->
    [{<<"defaultName">>, lang(Lang,"Внешнее управление")},
     {<<"hint">>, lang(Lang,"Внешнее управление через API")},
     {<<"serialIndex">>, 2096} | group(management,Lang)];

object_meta(225,Lang) ->
    [{<<"defaultName">>, lang(Lang,"Детектор ответов")},
        {<<"hint">>, lang(Lang,"Детектор ответов")},
        {<<"serialIndex">>, 2096} | group(pbx,Lang)];

object_meta(_,_) -> undefined.

% ------
% @private
group(X,Lang) -> ?APP_SCRIPT:meta_group_props(X,Lang).
lang(Lang,X) -> ?APP_SCRIPT:meta_u_lang(Lang,X).
