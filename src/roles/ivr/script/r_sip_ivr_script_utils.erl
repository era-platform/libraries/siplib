%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 21.04.2016
%%% @doc
%%% @todo @ivr

-module(r_sip_ivr_script_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

%%-compile([export_all,nowarn_export_all]).
%%
%%%% ====================================================================
%%%% Defines
%%%% ====================================================================
%%
%%%-include("../include/r_script.hrl").
%%-include("../../era_script/include/r_script.hrl").
%%-include("../include/r_sip_ivr.hrl").
%%-include("../include/r_sip_nk.hrl").
%%
%%-define(U, r_sip_utils).
%%-define(CFG, r_sip_config).
%%
%%%% ====================================================================
%%%% API functions
%%%% ====================================================================
%%
%%% return list of interrupt sequences (from component's property)
%%parse_interrupts(PropertyKey, CompData) ->
%%    binary:split(
%%      binary:replace(
%%        binary:replace(?SCR_COMP:get(PropertyKey, CompData, <<>>),
%%                       [<<" ">>,<<"\t">>,<<"\r">>,<<"\n">>], <<>>, [global]),
%%        <<";">>, <<",">>, [global]),
%%      <<",">>, [global, trim_all]).
%%
%%% update refer-to uri by replaces option
%%build_referto_replaces({_S,_U,_D}=AOR, {_RCallId, _RFromTag, _RToTag}=RepOpts) ->
%%    build_referto_replaces(?U:make_uri(AOR), RepOpts);
%%build_referto_replaces(#uri{}=Uri, {_RCallId, _RFromTag, _RToTag}=RepOpts) ->
%%    build_referto_replaces(Uri, RepOpts, <<>>).
%%build_referto_replaces(#uri{domain=Domain}=Uri, {RCallId, RFromTag, RToTag}, HuntBlockStr) ->
%%    ReplaceTo = ?EU:to_binary(?EU:urlencode(?EU:to_list(<<RCallId/bitstring, ";from-tag=", RFromTag/bitstring, ";to-tag=", RToTag/bitstring, HuntBlockStr/bitstring>>))),
%%    Uri#uri{domain= <<Domain/bitstring, "?Replaces=", ReplaceTo/bitstring>>}.
%%
%%%% ----------------------------------------------------
%%%% MG temp files
%%%% ----------------------------------------------------
%%
%%%%
%%get_mgid(#state_scr{ownerpid=OwnerPid}=_State) ->
%%    CmdRef = make_ref(),
%%    F = fun({Ref,FromPid,_},#state_ivr{}=StateIvr) ->
%%                {ok,#{mgid:=MGID}=_MediaInfo} = ?IVR_MEDIA:get_current_media_link(StateIvr),
%%                FromPid ! {ok,Ref,MGID},
%%                ok
%%        end,
%%    OwnerPid ! {scriptmachine, {ivr_cmd, CmdRef, {extf, F, self(), #{}}}},
%%    receive {ok,Ref,MGID} when Ref == CmdRef -> {ok,MGID}
%%    after 5000 -> false
%%    end.
%%
%%%%
%%push_file_to_mg(MGID,FileInfo) ->
%%    case get_mg_fileinfo(MGID,FileInfo) of
%%        false ->
%%            false;
%%        {ok,MGFileInfo} ->
%%            SrcPath = maps:get(filepath,FileInfo),
%%            MGInfo = maps:get(mginfo,MGFileInfo),
%%            [MGFilePath,MGSite,MGNode] = ?EU:maps_get([filepath,site,node],MGInfo),
%%            CurrentDest = {?CFG:get_current_site(),node()},
%%            ok = ?ENVCROSS:call_node({MGSite,MGNode}, {filelib, ensure_dir, [MGFilePath]}, false),
%%            case {?EU:file_md5(?EU:to_unicode_list(SrcPath)),
%%                  ?ENVCROSS:call_node({MGSite,MGNode}, {?EU, file_md5, [?EU:to_unicode_list(MGFilePath)]}, false)} of
%%                {_A,_A} -> ok;
%%                {_A,_B} -> ok = ?ENVCROSS:call_node({MGSite,MGNode}, {?ENVCOPY, copy_file, [CurrentDest,?EU:to_unicode_list(SrcPath),?EU:to_unicode_list(MGFilePath)]}, false)
%%            end,
%%            {ok, MGFileInfo}
%%    end.
%%
%%%%
%%get_mg_fileinfo(MGID,FileInfo) ->
%%    case maps:get(dest,FileInfo,rec) of
%%        sync -> get_mg_fileinfo_sync(MGID,FileInfo);
%%        _ -> get_mg_fileinfo_rec(MGID,FileInfo)
%%    end.
%%
%%%%
%%get_mg_fileinfo_rec(MGID,FileInfo) ->
%%    case ?U:parse_mg_devicename(?U:get_mg_devicename(MGID)) of
%%        undefined ->
%%            false;
%%        {Addr,Postfix} ->
%%            case ?CFG:get_mg_node_by_addr_postfix(Addr,Postfix) of
%%                undefined -> false;
%%                {MGSite,MGNode} ->
%%                    Filename = maps:get(filename,FileInfo),
%%                    SMPid = maps:get(smpid,FileInfo),
%%                    MGRecFolder = filename:join([?CFG:get_record_path(Addr,Postfix)]), % "E:/!!DEL!!/erlang/site1/records"
%%                    {_TS,RecPath} = ?U:get_media_context_rec_opts(), % ":RECORD/Srv-006/2019-07-17"
%%                    FP1 = filename:join([RecPath, ?SCR_FILE:pid_to_dirname(SMPid), Filename]),
%%                    MGFilePath = filename:join([MGRecFolder|lists:nthtail(1,filename:split(FP1))]),
%%                    {ok, #{filename => Filename,
%%                           mginfo => #{filepath => MGFilePath,
%%                                       filepath_by_rec => binary:replace(?EU:to_binary(MGFilePath),?EU:to_binary(MGRecFolder),<<":RECORD">>),
%%                                       %recfolder => MGRecFolder,
%%                                       %recpath => RecPath,
%%                                       site => MGSite,
%%                                       node => MGNode}}}
%%            end end.
%%
%%get_mg_fileinfo_sync(MGID,FileInfo) ->
%%    case ?U:parse_mg_devicename(?U:get_mg_devicename(MGID)) of
%%        undefined ->
%%            false;
%%        {Addr,Postfix} ->
%%            case ?CFG:get_mg_node_by_addr_postfix(Addr,Postfix) of
%%                undefined -> false;
%%                {MGSite,MGNode} ->
%%                    RelFilePath = maps:get(relfilepath,FileInfo),
%%                    case ?ENVCROSS:call_node({MGSite,MGNode},{?ENVFILE,get_local_path,[RelFilePath]},undefined,5000) of
%%                        undefined -> false;
%%                        MGAbsFilePath ->
%%                            {ok, FileInfo#{mginfo => #{filepath => MGAbsFilePath,
%%                                                       site => MGSite,
%%                                                       node => MGNode}}}
%%                    end end end.
%%%%
%%drop_mg_temp_dir(MGID,SMPid) ->
%%    case ?U:parse_mg_devicename(?U:get_mg_devicename(MGID)) of
%%        undefined -> false;
%%        {Addr,Postfix} ->
%%            case ?CFG:get_mg_node_by_addr_postfix(Addr,Postfix) of
%%                undefined -> false;
%%                {MGSite,MGNode} ->
%%                    MGRecFolder = filename:join([?CFG:get_record_path(Addr,Postfix)]), % "E:/!!DEL!!/erlang/site1/records"
%%                    {_TS,RecPath} = ?U:get_media_context_rec_opts(), % ":RECORD/Srv-006/2019-07-17"
%%                    FP1 = filename:join([RecPath, ?SCR_FILE:pid_to_dirname(SMPid)]),
%%                    MGFolderPath = filename:join([MGRecFolder|lists:nthtail(1,filename:split(FP1))]),
%%                    Fdel = fun() -> ?ENVCROSS:call_node({MGSite,MGNode}, {?EU, directory_delete, [MGFolderPath]}, false) end,
%%                    case Fdel() of
%%                        ok -> ok;
%%                        _ -> spawn(fun() -> timer:sleep(5000), Fdel() end)
%%                    end
%%            end
%%    end.
%%
%%%% --------------------------------
%%file_copy_to_mg_temp([],_Funs,State) -> {ok,[],State};
%%file_copy_to_mg_temp([File],Funs,State) when is_binary(File) ->
%%    {ok,File1,State1} = file_copy_to_mg_temp(File, Funs, State),
%%    {ok,[File1],State1};
%%file_copy_to_mg_temp([_File|_]=Files,Funs,State) when is_binary(_File) ->
%%    lists:foldr(fun(File,{ok,AccFiles,AccState}) when is_binary(File) ->
%%                    {ok,File1,State1} = file_copy_to_mg_temp(File, Funs, AccState),
%%                    {ok,[File1|AccFiles],State1};
%%                    (_,Acc) -> Acc
%%                end, {ok,[],State}, Files);
%%file_copy_to_mg_temp(<<":LOCAL/",_/binary>>=File,{F_store_mgid,F_get_mgid}=_Funs,#state_scr{}=State) when is_binary(File) ->
%%    Pid = self(),
%%    LocalPart = ?ENVFILE:get_local_path(<<":LOCAL/">>),
%%    FileInfo = #{filename => filename:basename(File),
%%                 filepath => binary:replace(File,<<":LOCAL">>,LocalPart),
%%                 smpid => Pid},
%%    {ok,MGID} = F_get_mgid(State),
%%    State1 = F_store_mgid(MGID,State),
%%    case ?IVR_SCRIPT_UTILS:push_file_to_mg(MGID,FileInfo) of
%%        {ok,MGFileInfo} ->
%%            File1 = maps:get(filepath_by_rec,maps:get(mginfo,MGFileInfo)),
%%            {ok,File1,State1};
%%        false ->
%%            {ok,false,State1}
%%    end;
%%file_copy_to_mg_temp(<<":TEMP/",_/binary>>=File,{F_store_mgid,F_get_mgid}=_Funs,#state_scr{}=State) when is_binary(File) ->
%%    Pid = self(),
%%    LocalPath = ?EU:to_list(?SCR_FILE:get_script_temp_directory(Pid)),
%%    FileInfo = #{filename => filename:basename(File),
%%                 filepath => binary:replace(File,<<":TEMP">>,LocalPath),
%%                 smpid => Pid},
%%    {ok,MGID} = F_get_mgid(State),
%%    State1 = F_store_mgid(MGID,State),
%%    case ?IVR_SCRIPT_UTILS:push_file_to_mg(MGID,FileInfo) of
%%        {ok,MGFileInfo} ->
%%            File1 = maps:get(filepath_by_rec,maps:get(mginfo,MGFileInfo)),
%%            {ok,File1,State1};
%%        false ->
%%            {ok,false,State1}
%%    end;
%%file_copy_to_mg_temp(File,_Funs,State) -> {ok,File,State}.
%%
%%%% ====================================================================
%%%% Internal functions
%%%% ====================================================================
%%

