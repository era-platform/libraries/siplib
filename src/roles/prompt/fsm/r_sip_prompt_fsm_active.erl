%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2022 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Presents 'active' state of PROMPT FSM

-module(r_sip_prompt_fsm_active).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([init/2,
         start/1,
         stop_external/2,
         test/1,
         build_tag_sync/2,
         replace_abonent/2,
         sip_cancel/2,
         sip_bye/2,
         sip_reinvite/2,
         sip_notify/3,
         sip_info/3,
         sip_message/3,
         uas_dialog_response/2,
         call_terminate/2,
         fork_timeout/2,
         uac_response/2,
         mg_dtmf/2,
         mg_event/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_prompt.hrl").
-include("../include/r_sip_headers.hrl").
-include("../include/r_sip_mgc.hrl").

% -include("../include/r_nksip.hrl").

-define(CURRSTATE, active).

%% ====================================================================
%% API functions
%% ====================================================================

init(Args, State) ->
    do_init(Args, State).

start(State) ->
    do_start(State).

% ----
stop_external(Reason, State) ->
    d(State, "stop_external(~p)", [Reason]),
    #state_prompt{forks=Forks}=State,
    lists:foreach(fun({_,#{}=ForkCall}) -> ?PROMPT_UTILS:cancel_active_forks([maps:get(fork,ForkCall)], [], State) end, Forks),
    StopReason = ?PROMPT_UTILS:reason_external(Reason),
    ?PROMPT_UTILS:return_stopping(?PROMPT_UTILS:do_finalize_dlg(State#state_prompt{stopreason=StopReason})).

% ----
test(State) ->
    d(State, "test"),
    {next_state, ?CURRSTATE, State}.

% ----
build_tag_sync([_Req], State) ->
    {Tag,State1} = ?PROMPT_UTILS:generate_tag(State),
    Reply = Tag,
    {reply, Reply, ?CURRSTATE, State1}.

%% =========================

% ----
replace_abonent([#sipmsg{call_id=CallId}=Req], State) ->
    d(State, "replace, CallId=~120p", [CallId]),
    do_replace_abonent([CallId,Req], State).

% ----
sip_cancel([CallId, _Req, _InviteReq], State) ->
    d(State, "sip_cancel, CallId=~120p", [CallId]),
    do_stop(CallId, State).

% ----
sip_bye([CallId, Req], State) ->
    d(State, "sip_bye, CallId=~120p", [CallId]),
    State1 = ?PROMPT_UTILS:send_response(Req, ok, State),
    case State1 of
        #state_prompt{a=#side{callid=CallId}} ->
            do_stop(CallId, State1);
        _ ->
            % from store
            ?DLG_STORE:unlink_dlg(CallId),
            {next_state, ?CURRSTATE, State}
    end.

%% =========================

% ----
sip_reinvite([CallId, _Req]=P, State) ->
    d(State, "sip_reinvite, CallId=~120p", [CallId]),
    do_reinvite_incoming(P, State).

% ----
sip_notify([CallId, Req]=_P, _StateName, State) ->
    d(State, "sip_notify, CallId=~120p", [CallId]),
    State1 = ?PROMPT_UTILS:send_response(Req, ?Forbidden("PROMPT. Not implemented"), State),
    State1.

% ----
sip_info([CallId, Req], _StateName, State) ->
    d(State, "sip_info, CallId=~120p", [CallId]),
    % State1 = ?PROMPT_UTILS:send_response(Req, ?Forbidden("PROMPT. Not implemented"), State),
    ?PROMPT_UTILS:send_response(Req, {200,[]}, State).

% ----
sip_message([CallId,Req]=_P, _StateName, State) ->
    d(State, "sip_message, CallId=~120p", [CallId]),
    State1 = ?PROMPT_UTILS:send_response(Req, ?Forbidden("PROMPT. Not implemented"), State),
    State1.

%% =========================

% ----
uas_dialog_response([_Resp]=P, State) ->
    #sipmsg{class={resp,SipCode,_},
            cseq={CSeq,Method},
            call_id=CallId}=_Resp,
    d(State, "uas_dialog_response ~p:~p -> ~p, CallId=~120p", [Method,CSeq,SipCode,CallId]),
    do_on_uas_dialog_response(P, State).

% ----
call_terminate([Reason, Call], State) ->
    #call{call_id=CallId}=Call,
    case State of
        #state_prompt{acallid=CallId} ->
            d(State, "call_terminate, CallId=~120p, Reason=~120p", [CallId, Reason]),
            do_stop(CallId, State);
        _ -> {next_state, ?CURRSTATE, State}
    end.

% ============================
% ----
fork_timeout([CallId, BLTag]=P, State) ->
    d(State, "fork_timeout BLTag=~120p, CallId=~120p", [BLTag, CallId]),
    do_on_fork_timeout(P, State).

% ----
uac_response([_Resp]=Args, State) ->
    #sipmsg{class={resp,SipCode,SipReason},
            cseq={CSeq,Method},
            call_id=CallId}=_Resp,
    d(State, "uac_response ~p:~p -> ~p ~p, CallId=~120p", [Method,CSeq,SipCode,SipReason,CallId]),
    do_on_uac_response(Args, State).

% ============================
mg_dtmf([_CtxId, CtxTermId, #{dtmf:=Dtmf}=_Event]=_Args, State) ->
    ?LOGSIP("DTMF RFC2833/inband in conf: ~p from ~p", [Dtmf, CtxTermId]),
    {next_state, ?CURRSTATE, State}.

% ----
mg_event([_CtxId, _CtxTermId, _EType, #{}=_Event]=_Args, State) ->
    ?LOGSIP("Event ~s in conf from ~p skipped", [_EType, _CtxTermId]),
    {next_state, ?CURRSTATE, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% --------
%% when init prompt dlg
%% --------
do_init([Opts]=_Args, State) ->
    Ref = make_ref(),
    [Req] = ?EU:extract_required_props([req], Opts),
    {ASide,State1} = ?ACTIVE_UTILS:prepare_side(Req, State),
    State2 = State1#state_prompt{a=ASide,
                                 acallid=Req#sipmsg.call_id,
                                 ref=Ref,
                                 timer_ref=erlang:send_after(?PROMPT_TIMEOUT, self(), ?PROMPT_TIMEOUT_MSG)},
    #side{callid=ACallId,localuri=#uri{domain=ADomain}}=ASide,
    ?STAT_FACADE:link_domain(ACallId, ADomain),
    self() ! {start, Ref},
    State2.

%% --------
%% first queue message to make prompt dlg active (Remote MG connect)
%% --------
do_start(#state_prompt{req=Req,a=Side}=State) ->
    case build_prompt_data(Req) of
        {ok,Prompt} ->
            State1 = State#state_prompt{prompt=Prompt},
            do_start_1(State1);
        {error,Reason} when is_list(Reason) ->
            ?PROMPT_UTILS:error_prompt_final(Reason, State, Side, ?InternalError(?EU:str("PROMPT. ~1000tp",[Reason])))
    end.

%% @private
build_prompt_data(Req) ->
    case nksip_request:header(?PromptHeaderLow,Req) of
        {ok,[H|_]} ->
            Parts = lists:map(fun(P) -> list_to_tuple(binary:split(P,<<"=">>,[global,trim_all]))
                              end, binary:split(H,<<";">>,[global,trim_all])),
            [DlgId0,Mode,Side] = ?EU:extract_required_props([<<"dlgid">>,<<"mode">>,<<"side">>], Parts),
            DlgId = ?EU:to_binary(?EU:urldecode(?EU:to_list(DlgId0))),
            SrvIdx = ?EU:parse_srvidx(DlgId),
            case ?CFG:get_sipserver_by_index(SrvIdx) of
                undefined -> {error, <<"node_inaccessible">>};
                {Site, Node}=Dest ->
                    Request = {?APP,b2b_dialog_getinfo,[DlgId,{activesides,[unparse]}]},
                    case ?ENVCROSS:call_node(Dest, Request, undefined) of
                        {ok,DlgInfo} ->
                            case maps:get(state,DlgInfo) of
                                dialog ->
                                    DlgPid = maps:get(pid,DlgInfo),
                                    MonRef = case ?ENVCFG:get_current_site() of
                                                 Site -> erlang:monitor(process,DlgPid);
                                                 _ -> undefined % TODO: cross site
                                             end,
                                    TermId = ?EU:random(1000000),
                                    {ok, #prompt{dlgid=DlgId,
                                                 mode=mode_to_atom(Mode),
                                                 side=side_to_atom(Side),
                                                 site=Site,node=Node,
                                                 pid=DlgPid,monref=MonRef,
                                                 termid=TermId}};
                                _ -> {error,"Invalid destination dialog state"}
                            end;
                        _ -> {error,"Destination B2B server unavailable"}
                    end
            end;
        _ -> {error,"Prompt header expected"}
    end.

%% @private
mode_to_atom(<<"mesh">>) -> mesh;
mode_to_atom(<<"monitor">>) -> monitor;
mode_to_atom(<<"prompt">>) -> prompt.

%% @private
side_to_atom(<<"a">>) -> a;
side_to_atom(<<"b">>) -> b.

%% ------
%% @private
do_start_1(State) ->
    #state_prompt{a=Side,
                  req=Req}=State,
    #sdp{}=RSdp=?U:extract_sdp(Req),
    case ?PROMPT_MEDIA:attach(RSdp,State) of
        {error,connect_failure} ->
            ?PROMPT_UTILS:error_prompt_final("Connect error", State, Side, ?InternalError("PROMPT. Connect error"));
        {error,restricted} ->
            ?PROMPT_UTILS:error_prompt_final("Attach restricted", State, Side, ?InternalError("PROMPT. Attach restricted"));
        {error,no_media} ->
            ?PROMPT_UTILS:error_prompt_final("No destination media", State, Side, ?InternalError("PROMPT. No destination media"));
        {ok,State1,LSdp} ->
            d(State1, " -> created"),
            % TODO: send 200 ok
            State2 = ?ACTIVE_UTILS:send_200ok(LSdp, Side#side{req=Req, remotesdp=RSdp}, State1),
            {next_state, ?CURRSTATE, State2}
    end.

%% -------
%% stops prompt session
%% -------
do_stop(_CallId, State) ->
    {ok,State1} = ?PROMPT_MEDIA:detach(State),
    ?PROMPT_UTILS:return_stopping(?PROMPT_UTILS:do_finalize_dlg(State1)).

%% -------
%% replaces
%% -------
do_replace_abonent([XCallId, XReq], State) ->
    Self = self(),
    #state_prompt{dlgid=DlgId,
                  a=#side{callid=ACallId}=ASide}=State,
    % new to store
    ?DLG_STORE:push_dlg(XCallId, {DlgId,Self}),
    ?MONITOR:append_fun(self(), fun() -> ?DLG_STORE:unlink_dlg(XCallId) end),
    % prev from store
    ?PROMPT_UTILS:send_bye(ASide, State),
    ?DLG_STORE:unlink_dlg(ACallId),
    % new side
    #sdp{}=RSdp=?U:extract_sdp(XReq),
    {XSide,State1} = ?ACTIVE_UTILS:prepare_side(XReq, State),
    State2 = State1#state_prompt{a=XSide,
                                 acallid=XCallId},
    case ?PROMPT_MEDIA:replace(RSdp,State2) of
        {error,connect_failure} ->
            % ?PROMPT_UTILS:error_prompt_final("Connect error", State, XSide, ?InternalError("PROMPT. Connect error"));
            ?PROMPT_UTILS:return_stopping(?PROMPT_UTILS:do_finalize_dlg(State2));
        {ok,State3,LSdp} ->
            d(State3, " -> replaced"),
            State4 = ?ACTIVE_UTILS:send_200ok(LSdp, XSide#side{req=XReq, remotesdp=RSdp}, State3),
            {next_state, ?CURRSTATE, State4}
    end.

%% --------
%% modifies prompt's media by incoming reinvite
%% --------
do_reinvite_incoming([CallId, Req], State) ->
    #state_prompt{a=#side{callid=CallId}=Side}=State,
    #sdp{}=RSdp=?U:extract_sdp(Req),
    case ?PROMPT_MEDIA:session_change(RSdp, State) of
        {error,connect_failure} ->
            % ?PROMPT_UTILS:error_prompt_final("Connect error", State, Side, ?InternalError("PROMPT. Connect error"));
            ?PROMPT_UTILS:return_stopping(?PROMPT_UTILS:do_finalize_dlg(State));
        {ok,State1,LSdp} ->
            d(State1, " -> session_changed"),
            State2 = ?ACTIVE_UTILS:send_200ok(LSdp, Side#side{req=Req, remotesdp=RSdp}, State1),
            {next_state, ?CURRSTATE, State2}
    end.

%% ====================================================================

%% -----
%% UAC fork timeout
%% -----
do_on_fork_timeout([CallId, _LTag]=P, State) ->
    #state_prompt{forks=Forks}=State,
    case lists:keyfind(CallId,1,Forks) of
        false ->
            {next_state, ?CURRSTATE, State};
        {_,#side_fork{}=_Side} ->
            {next_state, ?CURRSTATE, State};
        {_,#{type:='refer'}=Refer} ->
            ?ACTIVE_REFER:fork_timeout(P, Refer, State)
    end.

%% -----
%% UAC response handlers
%% -----
do_on_uac_response([#sipmsg{cseq={_,'INVITE'}, call_id=CallId}=Resp]=P, State) ->
    #sipmsg{class={resp,SipCode,_}}=Resp,
    #state_prompt{forks=Forks}=State,
    d(State, "uac_response('INVITE') ~p from Z: ~p", [SipCode, CallId]),
    case lists:keyfind(CallId,1,Forks) of
        false ->
            {next_state, ?CURRSTATE, State};
        {_,#side_fork{}=_Side} ->
            {next_state, ?CURRSTATE, State};
        {_,#{type:='refer'}=Refer} ->
            ?ACTIVE_REFER:uac_response(P, Refer, State)
    end;
%
do_on_uac_response([#sipmsg{cseq={_,'NOTIFY'}}=Resp], State) ->
    #sipmsg{class={resp,SipCode,_}, call_id=CallId}=Resp,
    d(State, "uac_response('NOTIFY') ~p from ~p", [SipCode, CallId]),
    {next_state, ?CURRSTATE, State};
%
do_on_uac_response([#sipmsg{cseq={_,Method}}=_Resp], State) ->
    d(State, "uac_response(~p) skipped", [Method]),
    {next_state, ?CURRSTATE, State}.

%% ====================================================================

%% ============================
%% UAS response, dialog ready
%% ============================

do_on_uas_dialog_response([#sipmsg{class={resp,SipCode,_}, cseq={_,'INVITE'}}=Resp], #state_prompt{a=#side{callid=ACallId}=Side}=State)
  when SipCode >= 200, SipCode < 300 ->
    %#sipmsg{to={_,ToTag}}=Resp,
    {ok,XCallId} = nksip_response:call_id(Resp),
    {ok,XDlgHandle} = nksip_dialog:get_handle(Resp),
    State1 = case ACallId==XCallId of
                 true -> State#state_prompt{a=Side#side{dhandle=XDlgHandle}};
                 false -> State
             end,
    {next_state, ?CURRSTATE, State1};
do_on_uas_dialog_response(_P, State) ->
    {next_state, ?CURRSTATE, State}.

%% =====================================================================

% @private ----
d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    #state_prompt{dlgid=DlgId}=State,
    ?LOGSIP("PROMPT. fsm ~p '~p':" ++ Fmt, [DlgId,?CURRSTATE] ++ Args).
