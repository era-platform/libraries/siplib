%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2022 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Service utils of 'active' state of CONF FSM

-module(r_sip_prompt_fsm_active_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_prompt.hrl").
-include("../include/r_sip_mgc.hrl").
-include("../include/r_sip_headers.hrl").

% -include("../include/r_nksip.hrl").

-define(CURRSTATE, 'active').

%% ====================================================================
%% API functions
%% ====================================================================

%% ---------------------------------
%% prepare side rec by opts for new participant
%% ---------------------------------
-spec prepare_side(Req::#sipmsg{}, State::#state_prompt{}) -> {XSide::#side{}, NewState::#state_prompt{}}.
%% ---------------------------------
prepare_side(#sipmsg{}=Req, State) ->
    Now = os:timestamp(),
    % -------
    #sipmsg{call_id=XCallId,
            from={XRUri,XRTag}}=Req,
    {ok, XReqHandle} = nksip_request:get_handle(Req),
    #sdp{}=XRSdp=?U:extract_sdp(Req),
    %
    #sipmsg{from={XRUri,_},
            to={#uri{ext_opts=XLExtOpts}=XLUri,_},
            contacts=XRContacts,
            to_tag_candidate=ToTagCandidate}=Req,
    % side local uri
    XLTag1 = ToTagCandidate,
    State1 = case State of
                 #state_prompt{usedtags=[]} ->
                     State#state_prompt{usedtags=[XRTag, XLTag1]};
                 _ -> State
             end,
    XLUri1 = XLUri#uri{ext_opts = ?U:store_value(<<"tag">>, XLTag1, XLExtOpts)},
    % side local contact
    XLContact = ?U:build_local_contact(XLUri), % @localcontact? (cfg=default! | incoming: top via's domain | outgoing: uri's route )
    % side rec
    XSide = #side{callid=XCallId,
                  rhandle=XReqHandle,
                  dhandle=undefined,
                  dir='in',
                  req=Req,
                  %
                  remoteuri=XRUri,
                  remotetag=XRTag,
                  remotecontacts=XRContacts,
                  remotesdp=XRSdp,
                  remoteparty=?U:clear_uri(XRUri), % update display not need, cause b2bua should do it
                  %
                  localuri=XLUri1,
                  localtag=XLTag1,
                  localcontact=XLContact,
                  %
                  starttime=Now
                 },
    State2 = State1#state_prompt{a=XSide},
    {XSide,State2}.

%% ---------------------------------
%% make side rec by fork of new participant
%% ---------------------------------
-spec make_side_by_fork(Fork::#side_fork{}, Resp::#sipmsg{}) -> Side::#side{}.
%% ---------------------------------
make_side_by_fork(#side_fork{}=Fork, #sipmsg{class={resp,_,_}}=Resp) ->
    #side_fork{}=Fork,
    {ok,DlgHandle} = nksip_dialog:get_handle(Resp),
    #sipmsg{to={RUri,RTag},contacts=RContacts}=Resp,
    #sdp{}=RSdp=?U:extract_sdp(Resp),
    Now = os:timestamp(),
    XSide = #side{callid=Fork#side_fork.callid,
                  rhandle=Fork#side_fork.rhandle,
                  dhandle=DlgHandle,
                  dir='out',
                  req=Resp,
                  %
                  remoteuri=RUri,
                  remotetag=RTag,
                  remotecontacts=RContacts,
                  remotesdp=RSdp,
                  remoteparty= ?U:build_remote_party_id(Resp, Fork#side_fork.remoteuri), % remote-party-id header should exist, guaranteed by b2bua
                  %
                  localuri=Fork#side_fork.localuri,
                  localtag=Fork#side_fork.localtag,
                  localcontact=Fork#side_fork.localcontact,
                  %
                  starttime=Fork#side_fork.starttime,
                  answertime=Now
                 },
    XSide.

%% ---------------------------------
%% update fork_side rec of new participant by 1xx response with sdp
%% ---------------------------------
-spec update_side_fork(Fork::#side_fork{}, Resp::#sipmsg{}) -> UpFork::#side_fork{}.
%% ---------------------------------
update_side_fork(#side_fork{}=Fork, #sipmsg{class={resp,SipCode,_}}=Resp) ->
    #side_fork{}=Fork,
    {ok,DlgHandle} = nksip_dialog:get_handle(Resp),
    #sipmsg{contacts=RContacts,to={_,RTag}}=Resp,
    RSdp = ?U:extract_sdp(Resp),
    Fork#side_fork{dhandle=DlgHandle,
                   last_response_code=SipCode,
                   remotetag=RTag,
                   remotecontacts=RContacts,
                   remotesdp=RSdp}.

%% ---------------------------------
%% send response 200 ok to caller's invite
%% ---------------------------------
-spec send_200ok(LSdp::#sdp{}, Side::#side{}, State::#state_prompt{}) -> State1::#state_prompt{}.
%% ---------------------------------
send_200ok(LSdp, #side{req=Req}=Side, State) ->
    LContact = ?U:build_local_contact(#uri{scheme=sip, user= <<"prompt">>}), % @localcontact (cfg=default! | Top via's domain!)
    Opts = [{body, LSdp},
            {contact, LContact},
            user_agent],
    State2 = ?PROMPT_UTILS:send_response(Req, {200,Opts}, State),
    Now = os:timestamp(),
    State2#state_prompt{a=Side#side{answertime=Now}}.

%% ---------------------------------
%% send bye to prompter abonent
%% ---------------------------------
-spec send_bye(CallId::binary(), State::#state_prompt{}) -> {ok, State::#state_prompt{}} | stop.
%% ---------------------------------
send_bye(CallId, #state_prompt{a=#side{callid=CallId}=Side}=State) ->
    ?DLG_STORE:unlink_dlg(CallId),
    ?PROMPT_UTILS:send_bye(Side, State).

%% ----------------------
%% send bye and detach current participant
%% ----------------------
-spec do_stop_bye(State::#state_prompt{}) -> {ok, State::#state_prompt{}}.
%% ----------------------
do_stop_bye(State) ->
    #state_prompt{a=Side}=State,
    ?PROMPT_UTILS:send_bye(Side, State),
    do_stop(State).

%% ----------------------
%% send bye and detach selected participant
%% ----------------------
-spec do_stop_bye(Side::#side{}, State::#state_prompt{}) -> {ok, State::#state_prompt{}}.
%% ----------------------
do_stop_bye(#side{callid=CallId}=Side, State) ->
    ?PROMPT_UTILS:send_bye(Side, State),
    ?DLG_STORE:unlink_dlg(CallId),
    {ok,State}.

%% ----------------------
%% detach side abonent (last should destroy conference)
%% ----------------------
-spec do_stop(State::#state_prompt{}) -> {ok, State::#state_prompt{}}.
%% ----------------------
do_stop(State) ->
    #state_prompt{a=#side{callid=CallId}=_Side}=State,
    % from store
    ?DLG_STORE:unlink_dlg(CallId),
    {ok, State}.

%% ------------------------------------------------------------------------
%% Get info about conference
%% ------------------------------------------------------------------------

%% ---------------------------------
%% Return conference info
%% ---------------------------------
-spec get_info(Opts::list(),State::#state_prompt{}) -> {ok,Info::map()} | {error,Reason::term()}.
%% ---------------------------------
get_info(_Opts,#state_prompt{dlgid=DlgId}=_State) ->
    {ok, #{dlgid => DlgId}}.

%% @private
build_uri(Uri,true) -> ?U:unparse_uri(?U:clear_uri2(Uri));
build_uri(Uri,_) -> Uri.

%% ====================================================================
%% Internal functions
%% ====================================================================

% @private ----
d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    #state_prompt{dlgid=DlgId}=State,
    ?LOGSIP("PROMPT. fsm ~p '~p':" ++ Fmt, [DlgId,?CURRSTATE] ++ Args).
