%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2022 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc

-module(r_sip_prompt_invite_router).
-author('Peter Bukashin <tbotc@yandex.ru>').

%% -------------------------------------------------------------------
%%
%% -------------------------------------------------------------------

-export([sip_invite/2]).

%-compile([export_all,nowarn_export_all]).

%% ==========================================================================
%% Defines
%% ==========================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_prompt.hrl").

% -include("../include/r_nksip.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------
%% start from incoming call (featurecode)
%% ------------------------
-spec(sip_invite(AOR::tuple(), Request::nksip:request()) ->
        {reply, nksip:sipreply()} | noreply | block).
%% ------------------------
sip_invite(AOR, Req) ->
    case build_route_opts(AOR, Req) of
        {reply,_}=R -> R;
        noreply -> noreply;
        block -> block
    end.

%% ------------------------
-spec(build_route_opts(AOR::tuple(), Request::nksip:request()) ->
        {reply, nksip:sipreply()} | {ok, RouteOpts::list()} | noreply | block).
%% ------------------------
build_route_opts(AOR, Req) ->
    #sipmsg{ruri=#uri{domain=_RUriDomain}=_RUri,
            vias=[#via{domain=ViaDomain, port=_ViaPort}|_],
            nkport=#nkport{remote_ip=RemoteIp}}=Req,
    % from inside or from outside?
    % check via domain existence to detect is this call from inside or outside
    Insiders = ?CFG:get_all_sipserver_addrs(),
    BRemoteIp = ?EU:to_binary(inet:ntoa(RemoteIp)),
    case {lists:member(ViaDomain,Insiders), lists:member(BRemoteIp,Insiders)} of
        {false,_} -> ?FILTER:check_block_response_malware(invalid_address, Req, {reply, ?InternalError("PROMPT. Access denied, remote address is unknown")});
        {true,true} -> do_invite_prompt(AOR, Req);
        {true,false} ->
            ?OUT("PROMPT. Incoming request forbidden, via ~120p is cluster address, but remote ~120p is not", [ViaDomain, BRemoteIp]),
            ?FILTER:check_block_response_malware(invalid_address, Req, {reply, ?Forbidden("PROMPT. Invalid remote address")})
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% enter existing or absent prompt by uri=AOR
do_invite_prompt({_,U,_}=AOR, Req) ->
    case ?U:parse_prompt_user(U) of
        false -> ?FILTER:check_block_response_malware(unknown_account, Req, {reply, ?Forbidden("PROMPT. User not found")});
        {ok,_} ->
            case check_referred(Req) of
                {true,true,[_,H2]} -> do_replace_prompt(AOR, H2, Req);
                _ -> do_start_prompt(AOR, Req)
            end
    end.

%% -----------------------------------
%% replace existing prompt session
%% -----------------------------------
do_replace_prompt(_AOR, Replaces, Req) ->
    RepMap = ?U:parse_replaces(Replaces),
    RCallId = maps:get(callid,RepMap),
    case ?PROMPT_SRV:pull_fsm_by_callid(RCallId) of
        false -> {reply, ?CallLegTransDoesNotExist("IVR. Replacing falure. Session not found")};
        DlgX ->
            ?STAT_FACADE:link_replaced(RCallId, Req#sipmsg.call_id), % @stattrace
            ?PROMPT_SRV:replace_abonent(DlgX, Req),
            noreply
    end.

%% -----------------------------------
%% create new prompt session
%% -----------------------------------
do_start_prompt(AOR, Req) ->
    Opts = [{app,?SIPAPP},
            {req,Req},
            {aor,AOR},
            {call_pid,self()}],
    ?PROMPT_SRV:start(Opts),
    noreply.

%% -----------------------------------
%% check request is referred
%% -----------------------------------
-spec check_referred(Req::#sipmsg{}) -> {HasReferred::boolean(),HasReplaces::boolean(),H::list()} | error .
%% -----------------------------------
check_referred(Req) ->
    case {nksip_request:header("referred-by", Req), nksip_request:header("replaces", Req)} of
        {{ok,[]},{ok,[]}} -> {false, false, []};
        {{ok,[H1|_]},{ok,[]}} -> {true, false, [H1]};
        {{ok,[]},{ok,_}} -> error;
        {{ok,[H1|_]},{ok,[H2|_]}} -> {true, true, [H1,H2]};
        _ -> error
    end.
