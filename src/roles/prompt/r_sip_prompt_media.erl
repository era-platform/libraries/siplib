%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2022 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Media utils of 'active' state of PROMPT FSM
%%% @todo

-module(r_sip_prompt_media).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_prompt.hrl").
-include("../include/r_sip_mgc.hrl").

% -include("../include/r_nksip.hrl").

-define(CURRSTATE, 'active').

%% ====================================================================
%% API functions
%% ====================================================================

%% ---------------------------------
%% connect to b2b and attach external termination to it's media with remote and local sdp
%% ---------------------------------
-spec attach(RSdp::nksip:sdp(),State::#state_prompt{})
      -> {ok,State::#state_prompt{},LSdp::nksip:sdp()} | {error,Reason::connect_failure | restricted | no_media}.
%% ---------------------------------
attach(RSdp,#state_prompt{prompt=#prompt{site=Site,node=Node,dlgid=DlgId,mode=Mode,termid=TermId}=Prompt}=State) ->
    Request = {?APP,b2b_external_term_attach,[DlgId,#{id=>TermId,termtype=>rtp,dir=>in,sdp=>RSdp}]}, % means create new by remote sdp
    case ?ENVCROSS:call_node({Site,Node}, Request, undefined, 5000) of
        {error,_}=Err -> Err;
        ok -> ok;
        {ok,LSdp} when Mode=='mesh' -> {ok,State,LSdp};
        {ok,LSdp} ->
            case setup_topology(Prompt) of
                ok -> {ok,State,LSdp};
                {error,_}=Err ->
                    detach(Prompt),
                    Err
            end
    end.

%% @private
setup_topology(#prompt{mode='monitor'}=Prompt) ->
    setup_topology(Prompt,[a,b]);
setup_topology(#prompt{mode='prompt',side=Side}=Prompt) ->
    setup_topology(Prompt,[a,b]--[Side]).

%% @private
setup_topology(#prompt{site=Site,node=Node,dlgid=DlgId,termid=TermId}, OnewaySides) ->
    Request = {?APP,b2b_external_term_setup_topology,[DlgId,#{id=>TermId,oneway_sides=>OnewaySides}]}, % means create new by remote sdp
    case ?ENVCROSS:call_node({Site,Node}, Request, undefined, 5000) of
        ok -> ok;
        {error,_}=Err -> Err
    end.

%% ---------------------------------
%% connect to b2b and replace external termination at it's media
%% ---------------------------------
-spec replace(RSdp::nksip:sdp(),State::#state_prompt{})
      -> {ok,State::#state_prompt{},LSdp::nksip:sdp()} | {error,Reason::connect_failure}.
%% ---------------------------------
replace(RSdp,State) ->
    {ok,State1} = detach(State),
    attach(RSdp,State1).

%% ---------------------------------
%% connect to b2b and replace external termination at it's media
%% ---------------------------------
-spec session_change(RSdp::nksip:sdp(),State::#state_prompt{})
      -> {ok,State::#state_prompt{},LSdp::nksip:sdp()} | {error,Reason::connect_failure}.
%% ---------------------------------
session_change(RSdp,State) ->
    replace(RSdp,State).

%% ---------------------------------
%% connect to b2b and detach external termination from it's media
%% ---------------------------------
-spec detach(State::#state_prompt{}) -> ok.
%% ---------------------------------
detach(#state_prompt{prompt=#prompt{site=Site,node=Node,dlgid=DlgId,termid=TermId}}=State) ->
    Request = {?APP,b2b_external_term_detach,[DlgId,#{id=>TermId}]},
    ?ENVCROSS:call_node({Site,Node}, Request, undefined, 5000),
    {ok,State}.

%% ---------------------------------
%% connect to b2b and attach external termination to it's media with only local sdp
%% ---------------------------------
-spec add_outgoing(CallId::binary(), State::#state_prompt{})
      -> {ok,State::#state_prompt{},LSdp::nksip:sdp()} | {error,Reason::connect_failure | restricted | no_media}.
%% ---------------------------------
add_outgoing(_CallId,#state_prompt{prompt=#prompt{site=Site,node=Node,dlgid=DlgId,termid=TermId}}=State) ->
    Request = {?APP,b2b_external_term_attach,[DlgId,#{id=>TermId,termtype=>rtp,dir=>out}]}, % means create new, initialize local sdp
    case ?ENVCROSS:call_node({Site,Node}, Request, undefined, 5000) of
        ok -> ok;
        {ok,LSdp} -> {ok,State,LSdp};
        {error,_}=Err -> Err
    end.

%% ---------------------------------
%% connect to b2b and update external termination to it's media
%% ---------------------------------
-spec update_outgoing_by_remote(Fork::#side_fork{}, RSdp::nksip:sdp(), State::#state_prompt{})
      -> {ok,State::#state_prompt{}} | {error,Reason::connect_failure | restricted | no_media}.
%% ---------------------------------
update_outgoing_by_remote(_Fork, RSdp, #state_prompt{prompt=#prompt{site=Site,node=Node,dlgid=DlgId,termid=TermId}}=State) ->
    Request = {?APP,b2b_external_term_attach,[DlgId,#{id=>TermId,termtype=>rtp,dir=>out,sdp=>RSdp}]}, % means update existing by remote sdp
    case ?ENVCROSS:call_node({Site,Node}, Request, undefined, 5000) of
        ok -> {ok,State};
        {error,_}=Err -> Err
    end.

%% ---------------------------------
media_stop(State) ->
    detach(State).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private ----
d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    #state_prompt{dlgid=DlgId}=State,
    ?LOGSIP("PROMPT. fsm ~p '~p':" ++ Fmt, [DlgId,?CURRSTATE] ++ Args).
