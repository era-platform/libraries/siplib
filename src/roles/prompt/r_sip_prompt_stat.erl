%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2022 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc
%%% @todo

-module(r_sip_prompt_stat).
-author('Peter Bukashin <tbotc@yandex.ru>').

%% -------------------------------------------------------------------
%%
%% -------------------------------------------------------------------

-export([query/2]).

%% ==========================================================================
%% Defines
%% ==========================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_prompt.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

query(<<"active">>, #{}=_Map) ->
    M = #{dlgcount => ?DLG_STORE:get_dialog_count(),
          callcount => ?DLG_STORE:get_callid_count()},
    {ok, M}.

%% ====================================================================
%% Internal functions
%% ====================================================================
