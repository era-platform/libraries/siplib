%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2022 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc
%%% @todo

-module(r_sip_prompt_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([]).

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_prompt.hrl").
-include("../include/r_sip_mgc.hrl").

% -include("../include/r_nksip.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

% =============================================
% Local Tags
% =============================================

build_tag() ->
    CurSrvCode = ?U:get_current_srv_textcode(),
    <<Tag0:48/bitstring, _/bitstring>> = ?U:luid(),
    <<"rPR-", CurSrvCode/bitstring, "-", Tag0/bitstring>>.

%% -------------------
%% Generates non existing tag
%% -------------------
generate_tag(#state_prompt{usedtags=Tags}=State) ->
    Tag = build_tag(),
    case lists:member(Tag, Tags) of
        false -> {Tag, State#state_prompt{usedtags=[Tag|Tags]}};
        true -> generate_tag(State)
    end.

%% ----------------------------------------------------------
%% Build call id for out call. Checks for non exist in current prompt dlg
%% ----------------------------------------------------------

build_out_callid(#state_prompt{}=_State) ->
    CurSrvCode = ?U:get_current_srv_textcode(),
    CallId = ?U:luid(),
    <<"rPR-", CurSrvCode/bitstring, "-", CallId/bitstring>>.

% =============================================
% Log to callid-link log
% =============================================

log_callid(outgoing,DlgNum,CallId,Domain) ->
    ?BLlog:write({sip,callid}, {"dlg='~s'; callid='~s'; domain='~s'",[DlgNum,CallId,Domain]});
log_callid(incoming,DlgNum,CallId,Domain) ->
    ?BLlog:write({sip,callid}, {"dlg='~s'; callid='~s'; domain='~s'",[DlgNum,CallId,Domain]}).
log_callid(referred,DlgNum,CallId,RCallId,Domain) ->
    ?BLlog:write({sip,callid}, {"dlg='~s'; callid='~s'; rfcallid='~s'; domain='~s'",[DlgNum,CallId,RCallId,Domain]});
log_callid(replaces,DlgNum,CallId,RCallId,Domain) ->
    ?BLlog:write({sip,callid}, {"dlg='~s'; callid='~s'; rpcallid='~s'; domain='~s'",[DlgNum,CallId,RCallId,Domain]}).

%% ----------------------------------------------------------
%% Build prompt display name
%% ----------------------------------------------------------
make_prompt_display(#state_prompt{aor={_,U,_}, map=Map}=_State) ->
    D = case lists:keyfind(<<"displayname">>, 1, maps:get(opts,Map)) of
            false -> ?U:make_prompt_display(U);
            {_,Disp} -> Disp
        end,
    ?U:quote_display(D).

%% ----------------------------------------------------------
%% Send sip response to incoming request
%% -------------------
send_response(#sipmsg{class={req,_}}=Req, SipReply, State) ->
    {ok,ReqHandle} = nksip_request:get_handle(Req),
    send_response(ReqHandle, SipReply, State);
send_response(ReqHandle, {SipCode,_Opts}=SipReply, State) ->
    d(State, "send_response ~p", [SipCode]),
    ?U:send_sip_reply(fun() -> nksip_request:reply(SipReply, ReqHandle) end),
    State;
send_response(ReqHandle, SipReply, State)  ->
    ?U:send_sip_reply(fun() -> nksip_request:reply(SipReply, ReqHandle) end),
    State.

%% -------------------
%% Finalize dlg
%% -------------------
do_finalize_dlg(State) ->
    % TODO: del from g_store - site storage (links to dlg user)
    % life timer
    #state_prompt{timer_ref=TimerRef}=State,
    erlang:cancel_timer(TimerRef),
    % unlink callids (on error it hangs incoming)
    unlink_callids(State),
    State.

%% @private
%% Unlink callids
unlink_callids(#state_prompt{acallid=ACallId,forks=Forks}=_State) ->
    F = fun(CallId, Acc) ->
                case lists:member(CallId, Acc) of
                    false -> ?DLG_STORE:unlink_dlg(CallId), [CallId|Acc];
                    true -> Acc
                end end,
    X0 = [],
    X1 = F(ACallId, X0),
    lists:foldl(fun({CallId,#{}=_ForkCall}, Acc) -> F(CallId, Acc) end, X1, Forks).

%% -------------------
%% Switch prompt dlg to stopping state
%% -------------------
return_stopping(#state_prompt{}=State) ->
    d(State, " -> switch to '~p'", [?STOPPING_STATE]),
    State1 = case State of
                 #state_prompt{stopreason=undefined} -> State#state_prompt{stopreason=#{}};
                 _ -> State
             end,
    % from media
    {ok,State2} = ?PROMPT_MEDIA:media_stop(State1),
    % timeout
    erlang:send_after(?STOPPING_TIMEOUT, self(), {stopping_timeout}),
    {next_state, ?STOPPING_STATE, State2}.

%% -------------------
%% finish with error of mgc
%% -------------------
error_mgc_final(Reason, State, Side) ->
    error_mgc_final(Reason, State, Side, ?InternalError("PROMPT. MGC error")).

error_mgc_final("", State, Side, SipReply) -> error_final(State, Side, SipReply);
error_mgc_final(Reason, State, Side, SipReply) ->
    d(State, "MGC error: ~120p", [Reason]),
    State1 = State#state_prompt{stopreason=#{error => reason_error([media], <<>>, Reason)}},
    error_final(State1, Side, SipReply).

%% ----
error_prompt_final(Reason, State, Side, SipReply) ->
    d(State, "PROMPT error: ~120p", [Reason]),
    State1 = State#state_prompt{stopreason=#{error => reason_error([attach], <<>>, Reason)}},
    error_final(State1, Side, SipReply).

%% -----
%% @private
error_final(State, undefined, _) ->
    ?PROMPT_UTILS:return_stopping(?PROMPT_UTILS:do_finalize_dlg(State));
error_final(State, Side, SipReply) ->
    #side{rhandle=RHandle}=Side,
    State1 = ?PROMPT_UTILS:send_response(RHandle, SipReply, State),
    error_final(State1, undefined, SipReply).

%% -------------------
%% send bye to side
%% -------------------
send_bye(Side, _State) ->
    #side{dhandle=DlgHandle}=Side,
    Opts = [user_agent],
    catch nksip_uac:bye(DlgHandle, [async|Opts]).

%% -------------------
%% Cancel active forks
%%
cancel_active_forks([],_,_) -> ok;
cancel_active_forks([Fork|Rest], Opts, State) ->
    #side_fork{rhandle=ReqHandle,
               cmnopts=CmnOpts}=Fork,
    cancel_timer(Fork),
    catch nksip_uac:cancel(ReqHandle, [async|Opts++CmnOpts]),
    cancel_active_forks(Rest, Opts, State).
%% @private
cancel_timer(#side_fork{rule_timer_ref=T1, resp_timer_ref=T2}=Fork) ->
    case T1 of undefined -> false; _ -> erlang:cancel_timer(T1) end,
    case T2 of undefined -> false; _ -> erlang:cancel_timer(T2) end,
    Fork#side_fork{rule_timer_ref=undefined,
                   resp_timer_ref=undefined}.


% =============================================
% Make stop reason
% =============================================

%
reason_error(_, _, #{}=Reason) -> Reason;
reason_error(Operations, <<>>, Reason) ->
    #{type => error,
      operations => Operations,
      reason => Reason};
reason_error(Operations, CallId, Reason) ->
    #{type => error,
      operations => Operations,
      callid => CallId,
      reason => Reason}.

%
reason_timeout(Operations, <<>>, Reason) ->
    #{type => timeout,
      operations => Operations,
      reason => Reason};
reason_timeout(Operations, CallId, Reason) ->
    #{type => timeout,
      operations => Operations,
      callid => CallId,
      reason => Reason}.

%
reason_external(#{}=Reason) -> Reason;
reason_external(Reason) ->
    #{type => external,
      reason => Reason}.

%% ====================================================================
%% Internal functions
%% ====================================================================

% @private ----
d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    #state_prompt{dlgid=DlgId}=State,
    ?LOGSIP("PROMPT. fsm ~p '~p':" ++ Fmt, [DlgId,?ACTIVE_STATE] ++ Args).
