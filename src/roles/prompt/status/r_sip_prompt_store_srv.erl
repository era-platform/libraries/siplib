%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2022 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 25.10.2019
%%% @doc RP-1707. Saves operation dlg info in domain's CallStore

-module(r_sip_prompt_store_srv).

-behaviour(gen_server).

-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).
-export([start_link/1,
         add_dlg/1,
         del_dlg/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_prompt.hrl").

-include_lib("stdlib/include/ms_transform.hrl").

-record(stat_event_s, {
          ets = undefined :: undefined | reference(),
          pid = undefined :: undefined | pid(),
          debug = false :: boolean(),
          ref = undefined :: undefined | reference()
         }).

-record(stat_dlg, {
          domain :: binary(),
          dlgid :: binary(),
          data :: map(),
          timerref :: reference(),
          dlg_monref :: reference(),
          dlg_pid :: pid()
         }).

-define(TIMEOUT, 280000).
-define(TIMEOUTSec, (?TIMEOUT + 20000) div 1000).
-define(TYPE, <<"dlg">>).

-define(KEYDlg, <<"dlgid">>).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) when is_map(Opts) ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, Opts, []).

%% -----------------------------------------------
-spec add_dlg(State::#state_prompt{}) -> ok | {error, binary()}.
%% -----------------------------------------------
add_dlg(#state_prompt{}=State) ->
    case make_data(State) of
        {error,_}=Error -> Error;
        #{}=Data -> gen_server:cast(?MODULE, {event, add, Data})
    end.

%% -----------------------------------------------
-spec del_dlg(DlgID::binary()) -> ok | {error, binary()}.
%% -----------------------------------------------
del_dlg(DlgID) ->
    gen_server:cast(?MODULE, {event, del, DlgID}).

%% ====================================================================
%% Behavioural functions
%% ====================================================================

%% -----------------------------------------------
init(Opts) ->
    Debug = maps:get(<<"debug">>, Opts, false),
    ETS = ets:new(callstats,[public,set,{keypos,#stat_dlg.dlgid}]),
    Ref = erlang:make_ref(),
    State = #stat_event_s{ets=ETS, debug=Debug, pid=self(), ref=Ref},
    d(State, "DlgStats srv started. Opts: ~120p, ETS: ~120p", [Opts,ETS]),
    {ok, State}.

%% -----------------------------------------------
handle_call(Request, _From, State) ->
    d(State, "Call: ~120p",[Request]),
    {noreply, State}.

%% -----------------------------------------------
handle_cast({event, add, Data}, State) ->
    DlgID = maps:get(?KEYDlg, Data),
    d(State, "Cast: ~120p (~120p)", [add,DlgID]),
    ?WORKER:cast_workf(DlgID, fun() -> send_event(add, DlgID, Data, State) end),
    {noreply, State};

handle_cast({event, del, DlgID}, State) ->
    case ets_get(DlgID, State) of
        undefined -> ok;
        #stat_dlg{data=Data} ->
            d(State, "Cast: ~120p (~120p)", [del,DlgID]),
            ?WORKER:cast_workf(DlgID, fun() -> send_event(del, DlgID, Data, State) end)
    end,
    {noreply, State};
    
handle_cast({down,DlgID,Data}, State) ->
    d(State, "Monitor down: ~120p (~120p)", [DlgID]),
    ?OUT("DLG DOWN: ~p", [DlgID]),
    ?WORKER:cast_workf(DlgID, fun() -> on_down(DlgID, Data, State) end),
    {noreply, State};

handle_cast({restart}, State) ->
    {stop, restart, State};

handle_cast(Msg, State) ->
    d(State, "Cast: unknown msg - ~120p",[Msg]),
    {noreply, State}.

%% -----------------------------------------------
handle_info({prolongation, DlgID, Ref}, #stat_event_s{ref=Ref}=State) ->
    d(State, "Info: prolongation ~120p",[DlgID]),
    ?WORKER:cast_workf(DlgID, fun() -> prolongation(DlgID, State) end),
    {noreply, State};

handle_info(Info, State) ->
    d(State, "Info: unknown msg - ~120p",[Info]),
    {noreply, State}.

%% -----------------------------------------------
terminate(Reason, State) ->
    d(State, "callstat - terminate: ~120p",[Reason]),
    ok.

%% -----------------------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

d(#stat_event_s{debug=true}, Fmt, Args) -> ?OUT("dlgstat - " ++ Fmt,Args);
d(#stat_event_s{debug=false}, _, _) -> ok.

%% @private
make_data(#state_prompt{pid=Pid,dlgid=DlgID,aor=AOR,invite_ts=ITS,starttime=StartTime}) ->
    {_,UN,Domain} = AOR,
    #{<<"site">> => ?CFG:get_current_site(),
      <<"domain">> => Domain,
      <<"pid">> => Pid,
      <<"dlgid">> => DlgID,
      <<"dlguri">> => <<"<sip:",UN/binary,"@",Domain/binary,">">>,
      <<"startts">> => ITS,
      <<"starttime">> => StartTime}.

%% -----------------------------------------------
%% worker fun
%% -----------------------------------------------
%%
send_event(add, DlgID, Data, #stat_event_s{}=State) ->
    DlgID = maps:get(?KEYDlg, Data),
    #stat_dlg{}=StatConf = case ets_get(DlgID, State) of
                                undefined -> new_stat_dlg(DlgID,Data);
                                #stat_dlg{}=X -> X
                            end,
    ets_put(StatConf, State),
    store_put(StatConf);
%%
send_event(del, DlgID, _Data, #stat_event_s{}=State) ->
    case ets_get(DlgID, State) of
        undefined -> ok;
        #stat_dlg{}=StatConf ->
            ets_del(StatConf, State),
            store_delete(StatConf)
    end.

%% @private
new_stat_dlg(DlgID,Data) ->
    Domain = maps:get(<<"domain">>, Data),
    DlgPid = maps:get(<<"pid">>, Data),
    ?MONITOR:append_fun(DlgPid, call_stats, fun() -> gen_server:cast(?MODULE, {down,DlgID,Data}) end),
    #stat_dlg{domain=Domain,
              dlgid=DlgID,
              data=Data,
              timerref=undefined,
              dlg_pid=DlgPid}.

%%
on_down(DlgID,Data,State) ->
    send_event(del, DlgID, Data, State).

%% -----------------------------------------------
cancel_timer(undefined) -> ok;
cancel_timer(TimerRef) -> erlang:cancel_timer(TimerRef).

%% -----------------------------------------------
prolongation(DlgID, State) ->
    case ets_get(DlgID, State) of
        undefined -> ok;
        #stat_dlg{}=StatConf ->
            store_nochange(StatConf),
            ets_put(StatConf, State)
    end.

%% -----------------------------------------------
%% CallStore fun
%% -----------------------------------------------
store_put(#stat_dlg{dlgid=DlgID,domain=Domain,data=Data}) ->
    call_sipstore(Domain, {put, [DlgID, Data, ?TIMEOUTSec]}).

%% ---------------------------
store_delete(#stat_dlg{dlgid=DlgID,domain=Domain}) ->
    ?ENVCALL:cast_callstore(Domain, {del, [DlgID]}).

%% ---------------------------
store_nochange(#stat_dlg{dlgid=DlgID,domain=Domain,data=Data}) when is_map(Data) ->
    call_sipstore(Domain, {put, [DlgID, Data, ?TIMEOUTSec]}).

%% @private
call_sipstore(Domain, Request) ->
    case ?ENVCALL:call_callstore(Domain, Request, undefined) of
        ok -> ok;
        true -> ok;
        Other -> ?LOGSIP("~120p -- call_sipstore: ~120p (~120p) -> ~120p",[?MODULE, Domain, Request, Other])
    end,
    ok.

%% -----------------------------------------------
%% ETS fun
%% -----------------------------------------------
ets_get(DlgID, #stat_event_s{ets=ETS}) ->
    case ets:lookup(ETS,DlgID) of
        [#stat_dlg{}=StatConf|_] -> StatConf;
        [] -> undefined
    end.

%% ---------------------------
ets_put(#stat_dlg{dlgid=DlgID,timerref=TimerRef}=StatConf, #stat_event_s{ets=ETS,pid=Pid,ref=Ref}) ->
    cancel_timer(TimerRef),
    NewTimerRef = erlang:send_after(?TIMEOUT, Pid, {prolongation, DlgID, Ref}),
    ets:insert(ETS, StatConf#stat_dlg{timerref=NewTimerRef}).

%% ---------------------------
ets_del(#stat_dlg{dlgid=DlgID, timerref=TimerRef}=_StatConf, #stat_event_s{ets=ETS}) ->
    cancel_timer(TimerRef),
    ets:delete(ETS, DlgID).
