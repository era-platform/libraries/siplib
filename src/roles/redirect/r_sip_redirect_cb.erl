%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc
%%% @todo

-module(r_sip_redirect_cb).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([start/0, stop/0]).
-export([handle_call/3, handle_cast/2, handle_info/2]).

-compile([export_all,nowarn_export_all]).

%% ==========================================================================
%% Defines
%% ==========================================================================

-define(TIME_CHECK, 10000).

-define(SIPAPP, r_sip_redirect).

%% ==========================================================================
%% Types
%% ==========================================================================

-include("../include/r_sip.hrl").

% -include("../include/r_nksip.hrl").

-record(state, {
    auto_check
}).

%% ==========================================================================
%% API functions
%% ==========================================================================

%% --------------------------------
%% @doc Starts a new SipApp, listening on port 5060 for udp and tcp and 5061 for tls,
%% and acting as a registrar.
%%
start() ->
    {ok,AppOpts} = ?APP:get_opts(?MODULE),
    ?SIPSTORE:store_u('local_contact_port', ?U:parse_contact_port(AppOpts)),
    ?SIPSTORE:store_u('local_ports', ?U:parse_ports(AppOpts)),
    %
    Plugins = [r_plug_bestinterface,
               r_plug_localdomain,
               r_plug_log
               %
               %nksip_registrar,
               %nksip_100rel,
               %nksip_gruu,
               %nksip_outbound,
               %nksip_timers,
               %nksip_refer,
              ],
    CoreOpts = #{plugins => lists:filter(fun(false) -> false; (_) -> true end, Plugins),
                certfile => code:priv_dir(era_sip) ++ "/ssl/server.crt",
                keyfile => code:priv_dir(era_sip) ++ "/ssl/server.key",
                sip_no_100 => true,
                transports => ?U:parse_transports(AppOpts)},
    nksip:start_link(?MODULE, CoreOpts).


%% --------------------------------
%% @doc Stops the SipApp.
%%
stop() ->
    nksip:stop(?SIPAPP).

%% --------------------------------
%% @doc Updates role configuration opts
%%
update_opts(Opts) ->
    ?U:check_update_opts(Opts).

%% ==================================================================================
%% Callback functions
%% ==================================================================================

%% ----------------------------------------------------------------------------------
%% @doc SipApp Callback: Initialization.
%% ----------------------------------------------------------------------------------

init([]) ->
    %erlang:start_timer(?TIME_CHECK, self(), check_speed),
    %nksip:put(?SIPAPP, speed, []),
    {ok, #state{auto_check=false}}.


%% ----------------------------------------------------------------------------------
%% @doc SipApp Callback: Called to check user's password.
%% ----------------------------------------------------------------------------------

%sip_get_user_pass(User, Realm, _Req, _Call) ->
%   ?ACCOUNTS:get_password_lazy(User, Realm).

%% ----------------------------------------------------------------------------------
%% @doc SipApp Callback: Called to check if a request should be authorized.
%% ----------------------------------------------------------------------------------

sip_authorize(_Auth, _Req, _Call) ->
    ok.

%% ----------------------------------------------------------------------------------
%% @doc SipApp Callback: Called to decide how to route every new request.
%% ----------------------------------------------------------------------------------

% Routing Request
% Handles REGISTER, SUBSCRIBE and PUBLISH requests, everything other is 403 forbidden.
% If request for known internal logical domain - defines responsible sg proxy and redirects with '405 Use Proxy'
% If request for local Ip - search Route header for known internal logical domain.
%     if found -> redirects to responsible sg
%     if not -> forbidden
sip_route(_Scheme, _User, <<"224.0.1.75">>, _Req, _Call) -> block; % auto provision broadcast
sip_route(Scheme, User, Domain, Req, _Call) ->
    Method = nksip_request:method(Req),
    ?LOGSIP("sip_route ~p, ~p, ~p, ~p", [Scheme, User, Domain, Method]),
    ?STAT_FACADE:incoming_request(Req), % @stattrace
    Proceed = case Method of
        {ok, M}
          when M=='REGISTER';
               M=='SUBSCRIBE';
               M=='PUBLISH';
               M=='INVITE'; % CANCEL,BYE,REFER,NOTIFY
               M=='NOTIFY';
               M=='MESSAGE';
               M=='INFO';
               M=='OPTIONS'
               -> true;
        {ok, _} -> {reply, {method_not_allowed, <<"REGISTER, SUBSCRIBE, PUBLISH, INVITE, MESSAGE, INFO, OPTIONS">>}}
    end,
    case Proceed of
        {reply,_}=Reply -> Reply;
        true ->
%%             case ?FILTER:check_route_inside(Req) of
%%                 ok -> route(1, Req#sipmsg.ruri, Req);
%%                 block -> block;
%%                 {reply,_}=Reply -> Reply
%%             end
        route(1, Req#sipmsg.ruri, Req)
    end.

%% ----------------------------------------------------------------------------------
%% @doc sip_log plugin callback
%% ----------------------------------------------------------------------------------

sip_log(Query, AppId) ->
    ?LOGGING:sip_log(Query, AppId).

%% ----------------------------------------------------------------------------------
%% @doc Domain Callback: Synchronous user call.
%% ----------------------------------------------------------------------------------

sip_checkclusterdomain(Domain, _AppId) ->
    ?LOCALDOMAIN:is_cluster_domain(Domain).

%% ----------------------------------------------------------------------------------
%% @doc SipApp Callback: Synchronous user call.
%% ----------------------------------------------------------------------------------

handle_call(_Msg, _From, _State) ->
    %{reply, null, State}.
    continue.

%% ----------------------------------------------------------------------------------
%% @doc SipApp Callback: Asynchronous user cast.
%% ----------------------------------------------------------------------------------

handle_cast(_, _State) ->
    %{noreply, State}.
    continue.

%% ----------------------------------------------------------------------------------
%% @doc SipApp Callback: External erlang message received.
%% The programmed timer sends a `{timeout, _Ref, check_speed}' message
%% periodically to the SipApp.
%% ----------------------------------------------------------------------------------

handle_info(_, _State) ->
    %{noreply, State}.
    continue.

%% ==========================================================================
%% Internal functions
%% ==========================================================================


%% -----------------------------------------------
%% ROUTE
%% -----------------------------------------------

%% --------------
%% Searching for logical domain in RUri and Route headers
%%
% -- ruri local domain
route(1, #uri{domain=Domain}=Uri, Req) ->
    case ?LOCALDOMAIN:is_cluster_domain(Domain) of
        true -> redirect(Uri, Req);
        false -> route(2, Uri, Req)
    end;

% -- ruri local ip
route(2, #uri{}=Uri, Req) ->
    case ?U:is_local_ruri(Req) of
        false -> {reply, ?Forbidden(<<"REDIRECT. Unknown ruri domain/address">>)};
        true -> route(3, Uri, Req)
    end;

% -- to local domain
route(3, _Uri, Req) ->
    #sipmsg{to={#uri{domain=Domain}=ToUri, _Tag}} = Req,
    case ?LOCALDOMAIN:is_cluster_domain(Domain) of
        true -> redirect(ToUri, Req);
        false -> route(4, ToUri, Req)
    end;

% -- has route
route(4, _Uri, Req) ->
    #sipmsg{routes=Routes} = Req,
    case Routes of
        [] ->{reply, ?Forbidden(<<"REDIRECT. Unknown destination">>)};
        [#uri{}=RouteUri|Rest] -> route(5, RouteUri, Req#sipmsg{routes=Rest})
    end;

% -- route local domain
route(5, #uri{domain=Domain}=Uri, Req) ->
    %{IsLocalDomainToUri, IsLocalIpRuri, IsClusterIpRuri}
    case ?ROUTE:get_local_domain_flags(Domain) of
        {true, _, _} -> redirect(Uri, Req);
        {false, false, _} -> {reply, ?Forbidden(<<"REDIRECT. Unknown route domain">>)};
        {false, true, _} -> route(4, Uri, Req)
    end.

%% --------------
%% Sends 3xx answer
%%
redirect(Uri, Req) ->
    case ?U:get_redirect_multi(false) of
        false -> redirect(single, Uri, Req);
        true -> redirect(multi, Uri, Req)
    end.

% redirect to single gate address in contact
redirect(single, Uri, Req) ->
    #uri{scheme=Scheme, user=User, domain=Domain}=Uri,
    #sipmsg{nkport=#nkport{transp=Proto}=NkPort}=Req,
    case ?SERVERS:find_responsible_sg(Scheme, User, Domain, NkPort) of
        false -> {reply, service_unavailable};
        ProxyIp ->
            [Ip,PortB] = binary:split(ProxyIp, <<":">>),
            {Port,_} = string:to_integer(binary:bin_to_list(PortB)),
            % Proxy uri (gate + transport + expires)
            Uri1 = Uri#uri{user= <<>>, domain=Ip, port=Port},
            Uri2 = case Proto of
                       udp -> Uri1;
                       _ -> Uri1#uri{opts=[{<<"transport">>,Proto}]}
                   end,
            Uri3 = case ?U:get_redirect_expires(0) of
                       Expires when is_integer(Expires), Expires>0 -> Uri2#uri{ext_opts=[{<<"expires">>,Expires}]};
                       _ -> Uri2 % default from rfc3261: 3600
                   end,
            ProxyUri = Uri3,
            % SIP code
            SipCode = case ?U:get_redirect_sipcode(305) of
                          300 -> 300; % redirect
                          301 -> 301; % redirect_permanent
                          302 -> 302; % redirect_temporary
                          305 -> 305; % use_proxy
                          _ -> 305
                      end,
            {reply_stateless, {SipCode, [{contact,ProxyUri}]}}
    end;

% redirect to multi addressed contacts (all selected gates or gates on site)
% @todo #418(c) take addrs of allowed sg nodes all over configuration and take appropriate proto port of every gate
redirect(multi, Uri, #sipmsg{nkport=#nkport{transp=Proto}=_Transp}=_Req) ->
    case [Addr || {_Node, [Addr|_RestAddrs]} <- ?CFG:get_site_sg_nodes()] of
        [] -> {reply, service_unavailable};
        GateAddrs ->
            F = fun(ProxyIp) ->
                        [Ip,PortB] = binary:split(ProxyIp, <<":">>),
                        {Port,_} = string:to_integer(binary:bin_to_list(PortB)),
                        % Proxy uri (gate + transport)
                        Uri1 = Uri#uri{user= <<>>, domain=Ip, port=Port},
                        Uri2 = case Proto of
                                   udp -> Uri1;
                                   _ -> Uri1#uri{opts=[{<<"transport">>,Proto}]}
                               end,
                        Uri3 = case ?U:get_redirect_expires(0) of
                                   Expires when is_integer(Expires), Expires>0 -> Uri1#uri{ext_opts=[{<<"expires">>,Expires}]};
                                       _ -> Uri2 % default from rfc3261: 3600
                               end,
                        Uri3
                end,
            {reply_stateless, {302, [{contact,?EU:randomize(lists:map(fun(Addr) -> F(Addr) end, GateAddrs))}]}}
    end.
