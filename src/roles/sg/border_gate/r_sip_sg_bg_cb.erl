%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Anton Makarov <anton@mastermak.ru>, Peter Bukashin <tbotc@yandex.ru>
%%% @doc

-module(r_sip_sg_bg_cb).

-compile([export_all,nowarn_export_all]).

%% ==========================================================================
%% Defines
%% ==========================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_sg_bg.hrl").

-define(CallIdTOut,7500000).

%% ====================================================================
%% API functions
%% ====================================================================
delete_tout_ctx(#state{media=Media}=_State) ->
    ?BG_MEDIA:destroy_ctx(Media).

%% ====================================================================
%% Public functions
%% ====================================================================

%% ----------------------------------------------------------------------------------
%% @doc border_gate callback interface
%% ----------------------------------------------------------------------------------

%% ----------------------------------------------------------------------------------
%% @doc Called when the UAC is preparing a request to be sent
%% ----------------------------------------------------------------------------------
%%
bg_uac_pre_request(#sipmsg{class={req,Method}}=Req,Host) ->
    do_forward_request(Method,Req,Host).

%% ----------------------------------------------------------------------------------
%% @doc Called when preparing a UAS dialog response
%% ----------------------------------------------------------------------------------
bg_uas_dialog_response(#sipmsg{class={resp,SipCode,_},cseq={_,'INVITE'}}=Resp) ->
    do_forward_response(SipCode,Resp);

bg_uas_dialog_response(Resp) ->
    Resp.

%% ====================================================================
%% Internal functions
%% ====================================================================
%% ----------------------------------------------------------------------------------
%% Request Handler
%% ----------------------------------------------------------------------------------
do_forward_request('INVITE',#sipmsg{call_id=CallId,contacts=Contacts}=Req,Host) ->
    case get_stored_context(CallId) of
        undefined ->
            case check_use_media(Req,Host) of
                true ->
                    #sipmsg{from={_,FromTag}}=Req,
                    #sdp{}=SDP = ?U:extract_sdp(Req), % TODO RP-956
                    {T1Alias, T2Alias} = get_both_interface_aliases(Req,Host),
                    case has_sdp(Req) of
                        true ->
                            Opts = [{termpos, t2},
                                    {sdp, SDP},
                                    {txalias, T2Alias},
                                    {t1alias, T1Alias},
                                    {t2alias, T2Alias},
                                    {contacts, Contacts}],
                            case ?BG_MEDIA:create_first_offer_media(Opts) of
                                {ok,Media,SDP1} ->
                                    State = create_state(#state{state=offered,contacts=Contacts,fromtag=FromTag,t1alias=T1Alias,t2alias=T2Alias},Media),
                                    store_context(CallId,State),
                                    update_sdp(SDP1, Req);
                                {error,_} ->
                                    store_context(CallId,#state{state=error}),
                                    Req
                            end;
                        false ->
                            State = create_state(#state{state=empty,contacts=Contacts,fromtag=FromTag,t1alias=T1Alias,t2alias=T2Alias,no_sdp_invite=true},#{}),
                            store_context(CallId,State),
                            Req
                    end;
                false -> Req
            end;
        #state{state=error}=_State -> Req;
        State -> do_forward_reinvite(State,Req)
    end;

do_forward_request('ACK',#sipmsg{call_id=CallId,body= <<>>}=Req,_Host) ->
    case get_stored_context(CallId) of
        undefined -> Req;
        #state{state=error}=_State -> Req;
        #state{media=Media}=State ->
            NewState = modify_state(State#state{state=dialog},Media),
            store_context(CallId,NewState),
            Req
    end;
do_forward_request('ACK',#sipmsg{call_id=CallId,to={_,ToTag}}=Req,_Host) ->
    #sdp{}=SDP = ?U:extract_sdp(Req),
    case get_stored_context(CallId) of
        undefined -> Req;
        #state{state=offered,media=OldMedia}=State ->
            Opts = [{termpos,t1a},
                    {sdp,SDP},
                    {media,OldMedia}],
            case ?BG_MEDIA:create_first_answer_media(Opts) of
                {ok,Media,SDP1} ->
                    NewState = modify_state(State#state{state=dialog,totag=ToTag},Media),
                    store_context(CallId,NewState),
                    update_sdp(SDP1, Req);
                {error,_} ->
                    store_context(CallId,State#state{state=error}),
                    Req
            end;
        #state{state=answered,media=Media}=State ->
            TP = get_term_for_update(State,Req),
            Opts = [{termpos,TP},
                    {sdp,SDP},
                    {media,Media}],
            case ?BG_MEDIA:update_terms(Opts) of
                {ok,Media1,SDP1} ->
                    NewState = modify_state(State#state{state=dialog},Media1),
                    store_context(CallId,NewState),
                    update_sdp(SDP1, Req);
                {error, _}=_Err ->
                    Req
            end;
        #state{state=error}=_State -> Req
    end;

do_forward_request('BYE',#sipmsg{call_id=CallId}=Req,_Host) ->
    case get_stored_context(CallId) of
        undefined -> Req;
        #state{media=Media}=_State ->
            ?BG_MEDIA:destroy_ctx(Media),
            delete_context(CallId),
            Req
    end;

do_forward_request(_,#sipmsg{}=Req,_Host) ->
    Req.

do_forward_reinvite(#state{media=Media}=State,#sipmsg{call_id=CallId}=Req) ->
    #sdp{}=SDP = ?U:extract_sdp(Req),
    TP = get_term_for_update(State,Req),
    Opts = [{termpos,TP},
            {sdp,SDP},
            {media,Media}],
    case ?BG_MEDIA:update_terms(Opts) of
        {ok,NewMedia,SDP1} ->
            NewState = modify_state(State,NewMedia),
            store_context(CallId,NewState),
            update_sdp(SDP1, Req);
        {error, _}=_Err -> Req
    end.

%% ----------------------------------------------------------------------------------
%% Response Handler
%% ----------------------------------------------------------------------------------
do_forward_response(SipCode,#sipmsg{body= <<>>,call_id=CallId}=Resp) when SipCode >= 300 ->
    case get_stored_context(CallId) of
        undefined ->
            Resp;
        #state{state=StateType,media=#gatemedia{t1_prevsdp=T1SDP,t2_prevsdp=T2SDP}=Media}=State
          when StateType==answered orelse StateType==dialog ->
            TP = get_term_for_update(State,Resp),
            PrevSDP =
                case TP of
                    t1 -> T2SDP;
                    t2 -> T1SDP
                end,
            Opts = [{termpos,TP},
                    {sdp,PrevSDP},
                    {media,Media}],
            case ?BG_MEDIA:rollback_terms(Opts) of
                {ok, NewMedia, _NewSDP} ->
                    NewState = modify_state(State,NewMedia),
                    store_context(CallId,NewState),
                    Resp;
                {error, _}=_Err ->
                    Resp
            end;
        #state{media=Media}=_State ->
            ?BG_MEDIA:destroy_ctx(Media),
            delete_context(CallId),
            Resp
    end;

do_forward_response(_SipCode,#sipmsg{body= <<>>}=Resp) -> Resp;
do_forward_response(_SipCode,#sipmsg{call_id=CallId, contacts=Contacts, to={_,ToTag}}=Resp) ->
    StoredCtx = get_stored_context(CallId),
    case StoredCtx of
        undefined -> Resp;
        #state{state=error}=_State -> Resp;
        _ ->
            #sdp{}=SDP = ?U:extract_sdp(Resp),
            case StoredCtx of
                #state{state=empty,t1alias=T1Alias,t2alias=T2Alias}=State ->
                    #sipmsg{from={_,FromTag}}=Resp,
                    Opts = [{termpos, t1},
                            {sdp, SDP},
                            {txalias, T1Alias},
                            {t1alias, T1Alias},
                            {t2alias, T2Alias},
                            {contacts, Contacts}],
                    case ?BG_MEDIA:create_first_offer_media(Opts) of
                        {ok,Media,SDP1} ->
                            NewState = modify_state(State#state{state=offered,fromtag=FromTag},Media),
                            store_context(CallId,NewState),
                            update_sdp(SDP1, Resp);
                        {error,_} ->
                            store_context(CallId,State#state{state=error}),
                            Resp
                    end;
                #state{state=offered,media=Media,no_sdp_invite=true}=State ->
                    TP = get_term_for_update(State,Resp),
                    Opts = [{termpos,TP},
                            {sdp,SDP},
                            {media,Media}],
                    case ?BG_MEDIA:update_terms(Opts) of
                        {ok,NewMedia,SDP1} ->
                            NewState = modify_state(State#state{state=offered},NewMedia),
                            store_context(CallId,NewState),
                            update_sdp(SDP1, Resp);
                        {error, _}=_Err ->
                            Resp
                    end;
                #state{state=offered,media=OldMedia}=State ->
                    Opts = [{termpos,t2a}, % completes t2
                            {sdp,SDP},
                            {media,OldMedia}],
                    case ?BG_MEDIA:create_first_answer_media(Opts) of
                        {ok,Media,SDP1} ->
                            NewState = modify_state(State#state{state=answered,totag=ToTag},Media),
                            store_context(CallId,NewState),
                            update_sdp(SDP1, Resp);
                        {error, _}=_Err ->
                            store_context(CallId,State#state{state=error}),
                            Resp
                    end;
                #state{state=answered,media=Media}=State ->
                    TP = get_term_for_update(State,Resp),
                    Opts = [{termpos,TP},
                            {sdp,SDP},
                            {media,Media}],
                    case ?BG_MEDIA:update_terms(Opts) of
                        {ok,NewMedia,SDP1} ->
                            NewState = modify_state(State,NewMedia),
                            store_context(CallId,NewState),
                            update_sdp(SDP1, Resp);
                        {error, _}=_Err ->
                            Resp
                    end;
                #state{state=dialog,media=Media}=State ->
                    TP = get_term_for_update(State,Resp),
                    Opts = [{termpos,TP},
                            {sdp,SDP},
                            {media,Media}],
                    case ?BG_MEDIA:update_terms(Opts) of
                        {ok,NewMedia,SDP1} ->
                            NewState = modify_state(State,NewMedia),
                            store_context(CallId,NewState),
                            update_sdp(SDP1, Resp);
                        {error, _}=_Err ->
                            Resp
                    end
            end
    end.

%% ----------------------------------------------------------------------------------
%% Support Functions
%% ----------------------------------------------------------------------------------
get_stored_context(CallId) ->
    case ?SIPSTORE:find_t({CallId,<<"bgate_state">>}) of
        false -> undefined;
        {_, Value} -> Value
    end.

store_context(CallId,State) ->
    ?SIPSTORE:store_t({CallId,<<"bgate_state">>}, State, ?CallIdTOut).

delete_context(CallId) ->
    ?SIPSTORE:delete_t({CallId,<<"bgate_state">>}).

create_state(State,Media) ->
    State#state{media=Media}.

modify_state(State,Media) ->
    State#state{media=Media}.


% @private
check_use_media(#sipmsg{nkport=NkPort,call_id=CallId}=_Req,Host) ->
    #nkport{remote_ip=SrcAddr} = NkPort,
    IpAddrRcvReq = ?U:get_host_addr(SrcAddr), %% need check on two network card %%     IpAddrRcvReq = SrcAddr,
    RcvAlias = ?U:get_interface_alias(IpAddrRcvReq),
    SendAlias = ?U:get_interface_alias(Host),
    UseMedia = use_media(RcvAlias, SendAlias),
    ?LOGSIP("BGMG (CallId=~120p): (recv:~120p;~120p) -> (send:~120p;~120p) => ~120p", [CallId,IpAddrRcvReq,RcvAlias,Host,SendAlias,UseMedia]),
    UseMedia.

% @private
use_media(undefined,_) -> false;
use_media(_,undefined) -> false;
use_media(A,A) -> false;
use_media(_,_) -> true.


%% return true if request has sdp in content, false otherwise
has_sdp(#sipmsg{}=Req) ->
    case ?U:extract_sdp(Req) of
        #sdp{} -> true;
        _ -> false
    end.

% set sdp to request after update
update_sdp(Sdp, #sipmsg{content_type={<<"application/",T/binary>>,_}}=Req) when T==<<"sdp">>; T==<<"SDP">> -> Req#sipmsg{body=Sdp};
update_sdp(Sdp, #sipmsg{}=Req) -> Req#sipmsg{body=Sdp, content_type={<<"application/sdp">>,[]}}.

%
get_term_for_update(#state{fromtag=StateFromTag,totag=StateToTag}=_State,#sipmsg{class={req, _},from={_,FromTag},to={_,ToTag}}=_Req) ->
    case {FromTag,ToTag} of
        {StateFromTag,StateToTag} -> t1;
%%         case _Req#sipmsg.call_id of <<"rB2", _/bitstring>> -> ?OUT("Before Pause:~n"),timer:sleep(5000),?OUT("After pause:~n"); _-> ok end, t1; %% FOR tests
        {StateToTag,StateFromTag} -> t2
    end;
get_term_for_update(#state{fromtag=StateFromTag,totag=StateToTag}=_State,#sipmsg{class={resp, _, _},from={_,FromTag},to={_,ToTag}}=_Req) ->
    case {FromTag,ToTag} of
        {StateFromTag,StateToTag} -> t2;
        {StateToTag,StateFromTag} -> t1
    end.


get_both_interface_aliases(#sipmsg{nkport=NkPort}=_Req,Host) ->
     #nkport{remote_ip=SrcAddr} = NkPort,
    IpAddrRcvReq = ?U:get_host_addr(SrcAddr), %% need check on two network card %%     IpAddrRcvReq = SrcAddr,
     RcvAlias = ?U:get_interface_alias(IpAddrRcvReq),
      SendAlias = ?U:get_interface_alias(Host),
    {RcvAlias,SendAlias}.

