%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Anton Makarov <anton@mastermak.ru>, Peter Bukashin <tbotc@yandex.ru>
%%% @doc

-module(r_sip_sg_bg_media).

%% ====================================================================
%% API functions
%% ====================================================================
-export([create_first_offer_media/1,
         create_first_answer_media/1,
         update_terms/1,
         rollback_terms/1,
         destroy_ctx/1]).

%% ==========================================================================
%% Types
%% ==========================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_mgc.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

create_first_offer_media(Opts) ->
    [TermPos,ARSdp,Alias,Contacts,T1Alias,T2Alias] = ?EU:extract_required_props(['termpos','sdp','txalias','contacts','t1alias','t2alias'], Opts),
    MSID = get_msid(),
    MGOpts = get_mgopts(Alias),
    case ?MEDIA_GATE:fwd_start_media({MSID, ARSdp, MGOpts, Contacts, Alias, TermPos,T1Alias,T2Alias}) of
        {ok, Media, SDP} -> {ok, Media, SDP};
        {reply, Reply} -> {error, Reply};
        {error, Reply} -> {error, Reply}
    end.

create_first_answer_media(Opts) ->
    [TermPos,SDP,Media] = ?EU:extract_required_props(['termpos','sdp','media'], Opts),
    case ?MEDIA_GATE:fwd_invite_response_to_a({Media, SDP, TermPos}) of
        {ok, _NewMedia, _NewSDP}=Result -> Result;
        {error, Reply} -> {error, Reply}
    end.

update_terms(Opts) ->
    [TermPos,SDP,Media] = ?EU:extract_required_props(['termpos','sdp','media'], Opts),
    case ?MEDIA_GATE:fwd_update_rex_to_x({Media, SDP, TermPos}) of
        {ok, _NewMedia, _NewSDP}=Result -> Result;
        {error, Reply} -> {error, Reply}
    end.

rollback_terms(Opts) ->
    update_terms(Opts).

destroy_ctx(Media) ->
    ?MEDIA_GATE:stop_media(Media).

%% ====================================================================
%% Internal functions
%% ====================================================================

get_msid() ->
    <<_B4:32/bitstring, _/bitstring>> = ?U:luid().
    %MSID = <<DialogId/bitstring, "_", B4/bitstring>>,

get_mgopts(_Alias) ->
    % 07.11.2016 Peter
    %  We should use bg-mg, containing same aliases as current sg.
    %  If sg should not use mg, then no aliases used, and media operations filtered earlier.
    BAliasHash = ?EU:to_binary(erlang:phash2(lists:usort(?U:get_aliases()))),
    [
%%      {mgaddr, ?EU:to_binary(inet:ntoa(?U:get_host_addr()))},
     {mgsfx, <<"bg",BAliasHash/bitstring>>}].
