%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 18.03.2022
%%% @doc

-module(era_sip_sg_substitute).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([setup/1]).

-export([
    on_uac_headers/1,
    on_uas_dialog_response/1,
    do_forward_request/2,
    do_forward_response/2
]).

-export([do_substitute/4]).

%% ==========================================================================
%% Define
%% ==========================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_headers.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

setup([]) ->
    ?LOG('$info', "SIP substitutions clear", []),
    application:unset_env(?APP, {internal,substitute_fun});

setup(Substitutions) ->
    Insiders = lists:map(fun(Addr) -> {ok,IpAddr} = inet:parse_address(?EU:to_list(Addr)), IpAddr end, ?CFG:get_all_sipserver_addrs()),
    ?LOG('$info', "SIP substitutions setup: ~120tp~n\tInsiders: ~120tp", [Substitutions,Insiders]),
    %
    Fun = fun(#sipmsg{nkport=#nkport{remote_ip=RemoteIp}}=SipMsg, DoUpdateRuri) ->
                Dir = case {lists:member(RemoteIp,Insiders) orelse RemoteIp==undefined, class(SipMsg)} of
                          {true,req} -> outside;
                          {false,req} -> inside;
                          {true,resp} -> inside;
                          {false,resp} -> outside
                      end,
                ?MODULE:do_substitute(SipMsg,DoUpdateRuri,Dir,Substitutions)
          end,
    application:set_env(?APP,{internal,substitute_fun},Fun).

%% @private
class(#sipmsg{class={req,_}}) -> req;
class(#sipmsg{class={resp,_,_}}) -> resp.

%% @private
%method(#sipmsg{class={req,Method}}) -> Method;
%method(#sipmsg{class={resp,_,_},cseq={_,Method}}) -> Method.

%% ----------------------------------------------------------------------------------
%% @doc Called when the UAC is preparing a request to be sent
%% ----------------------------------------------------------------------------------
on_uac_headers(Req) ->
    do_forward_request(Req,false).

%% ----------------------------------------------------------------------------------
%% @doc Called when preparing a UAS dialog response
%% ----------------------------------------------------------------------------------
on_uas_dialog_response(Resp) ->
    do_forward_response(Resp,false).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ----------------------------------------------------------------------------------
%% Request Handler
%% ----------------------------------------------------------------------------------
do_forward_request(Req,DoUpdateRuri) ->
    case get_substitute_fun() of
        undefined -> Req;
        Fun -> Fun(Req,DoUpdateRuri)
    end.

%% ----------------------------------------------------------------------------------
%% Response Handler
%% ----------------------------------------------------------------------------------
do_forward_response(Resp,DoUpdateRuri) ->
    case get_substitute_fun() of
        undefined -> Resp;
        Fun -> Fun(Resp,DoUpdateRuri)
    end.

%% ----------------------------------------------------------------------------------
%% Support Functions
%% ----------------------------------------------------------------------------------

%% @private
%%do_substitute(#sipmsg{ruri=Ruri,
%%                      from={#uri{domain=Value}=FromUri,FromTag},
%%                      to={#uri{domain=Value}=ToUri,ToTag}}=SipMsg,
%%              DoUpdateRuri, Dir, Substitutions) ->
%%    R = lists:filtermap(fun({X,Y}) when Y==Value, Dir==outside -> {true,X};
%%                           ({X,Y}) when X==Value, Dir==inside -> {true,Y};
%%                           (_) -> false
%%                        end, Substitutions),
%%    case R of
%%        [] -> SipMsg;
%%        [Y|_] when DoUpdateRuri ->
%%            SipMsg#sipmsg{ruri=Ruri#uri{domain=Y},
%%                          from={FromUri#uri{domain=Y},FromTag},
%%                          to={ToUri#uri{domain=Y},ToTag}};
%%        [Y|_] ->
%%            SipMsg#sipmsg{from={FromUri#uri{domain=Y},FromTag},
%%                          to={ToUri#uri{domain=Y},ToTag}}
%%    end;
do_substitute(SipMsg, DoUpdateRuri, Dir, Substitutions) ->
    #sipmsg{ruri=Ruri,
            from={#uri{domain=FromDomain}=FromUri,FromTag},
            to={#uri{domain=ToDomain}=ToUri,ToTag},
            headers=Headers}=SipMsg,
    Class = class(SipMsg),
    % From & To - subst domain => INVITE
    % From - unknown domain, Y - subst domain => SUBSCRIBE
    % From - subst domain, Y = other domain => INVITE after cross domain refer
    R = lists:filtermap(fun({X,Y}) when Y==ToDomain, Dir==outside -> {true,{Y,X}}; % from Y to X
                           ({X,Y}) when X==ToDomain, Dir==inside -> {true,{X,Y}}; % from X to Y
                           ({X,Y}) when X==FromDomain, Dir==inside -> {true,{X,Y}}; % from X to Y
                           (_) -> false
                        end, Substitutions),
    FunKey = fun(#sipmsg{call_id=CallId,cseq=CSeq}) -> {substitute,CallId,CSeq} end,
    case R of
        [] -> SipMsg;
        [{X,Y}|_] when Class==req ->
            SipMsg1 = case Dir of
                          inside when FromDomain==ToDomain ->
                              % Invite normal (From & To - subst domain)
                              SipMsg#sipmsg{from={FromUri#uri{domain=Y},FromTag},
                                            to={ToUri#uri{domain=Y},ToTag}};
                          inside when ToDomain==X ->
                              % Subscribe (From - unknown, To - subst domain)
                              SavedDomain = FromUri#uri.domain,
                              ?SIPSTORE:store_t(FunKey(SipMsg),SavedDomain,60000),
                              SipMsg#sipmsg{from={FromUri#uri{domain=Y},FromTag},
                                            to={ToUri#uri{domain=Y},ToTag},
                                            headers=Headers++[{?RcvFromDomHeader,SavedDomain}]};
                          inside when FromDomain==X ->
                              % Invite by refer (from - subst domain, to - other domain)
                              SipMsg#sipmsg{from={FromUri#uri{domain=Y},FromTag}};
                          outside ->
                              RestoredDomain = override_domain(SipMsg,Y),
                              SipMsg#sipmsg{from={FromUri#uri{domain=Y},FromTag},
                                            to={ToUri#uri{domain=RestoredDomain},ToTag},
                                            headers=lists:keydelete(?RcvFromDomHeaderLow,1,Headers)}
                      end,
            SipMsg2 = substitute_refer_to(SipMsg1, Dir, {X,Y}),
            SipMsg3 = substitute_body(SipMsg2, Dir, {X,Y}),
            case DoUpdateRuri of
                true -> SipMsg3#sipmsg{ruri=Ruri#uri{domain=Y}};
                false -> SipMsg3
            end;
        [{_,Y}|_] when Class==resp ->
            case Dir of
                inside ->
                    SipMsg#sipmsg{from={FromUri#uri{domain=Y},FromTag},
                                  to={ToUri#uri{domain=Y},ToTag}};
                outside ->
                    RestoredDomain = case ?SIPSTORE:find_t(FunKey(SipMsg)) of
                                         false -> Y;
                                         {_,Saved} -> Saved
                                     end,
                    SipMsg#sipmsg{from={FromUri#uri{domain=RestoredDomain},FromTag},
                                  to={ToUri#uri{domain=Y},ToTag}}
            end
    end.

%% @private
substitute_refer_to(Req, _, _) -> Req;
substitute_refer_to(#sipmsg{class={req,'REFER'}}=Req, _Dir, {_X,Y}) ->
    #sipmsg{headers=Headers}=Req,
    % case nksip_request:header("refer-to", Req) of
    case lists:keytake(<<"refer-to">>, 1, Headers) of
        false -> Req;
        {value,{_,Value},Rest} ->
            [#uri{}=RefToUri|_] = ?U:parse_uris(Value),
            RefToUri1 = ?U:unparse_uri(RefToUri#uri{domain=Y}),
            Req#sipmsg{headers=[{<<"refer-to">>,RefToUri1}|Rest]}
    end;
substitute_refer_to(Req, _, _) -> Req.

%% @private
substitute_body(#sipmsg{body= <<>>}=SipMsg, _, _) -> SipMsg;
substitute_body(#sipmsg{cseq=CSeq,body=Body}=SipMsg, Dir, {X,Y}) ->
    Apply = case CSeq of
                {_,'NOTIFY'} when Dir==outside -> true;
                _ -> false
            end,
    case Apply of
        false -> SipMsg;
        true -> SipMsg#sipmsg{body = binary:replace(Body, X, Y, [global])}
    end.

%% ---------------------
%% @private
get_substitute_fun() ->
    application:get_env(?APP, {internal,substitute_fun}, undefined).

%% ---------------------
%% @private
override_domain(SipMsg, Default) ->
    case get_from_domain(SipMsg) of
        undefined -> Default;
        Saved -> Saved
    end.

%% @private
get_from_domain(#sipmsg{class={req,_}}=Req) ->
    case nksip_request:header(?RcvFromDomHeaderLow, Req) of
        {ok,[FromDomain|_]} -> FromDomain;
        _ -> undefined
    end;
get_from_domain(#sipmsg{class={resp,_,_}}=Req) ->
    case nksip_response:header(?RcvFromDomHeaderLow, Req) of
        {ok,[FromDomain|_]} -> FromDomain;
        _ -> undefined
    end.