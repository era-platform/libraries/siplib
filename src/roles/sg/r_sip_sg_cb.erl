%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc
%%% @todo
%%% -------------------------------------------------------------------
%%% Required Opts:
%%%    -
%%% Optional Opts:
%%%    certdir ("/priv/ssl")
%%%    keypass ("")
%%% -------------------------------------------------------------------

-module(r_sip_sg_cb).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([start/0, stop/0]).
-export([sni_fun/1,
         clear_cert_cache/0]).
-export([sip_route/5]).
-export([sip_options/2]).
-export([sip_dialog_update/3, sip_session_update/3]).
-export([handle_call/3, handle_cast/2, handle_info/2]).

-compile([export_all,nowarn_export_all]).

%% ==========================================================================
%% Defines
%% ==========================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_headers.hrl").
-include("../include/r_sip_sg.hrl").

-define(TIME_CHECK, 10000).

-record(state, {
     auto_check
}).

%% ==========================================================================
%% API functions
%% ==========================================================================

%% --------------------------------
%% @doc Starts a new SipApp, listening on port 5060 for udp and tcp and 5061 for tls,
%% and acting as a registrar.
%% --------------------------------
start() ->
    ?SUPV:start_child({?SG_SUPV, {?SG_SUPV, start_link, []}, permanent, 1000, supervisor, [?SG_SUPV]}),
    %
    {ok,AppOpts} = ?APP:get_opts(?MODULE),
    ?SIPSTORE:store_u('local_contact_port', ?U:parse_contact_port(AppOpts)),
    ?SIPSTORE:store_u('local_ports', ?U:parse_ports(AppOpts)),
    ?SIPSTORE:store_u('use_srtp', ?U:parse_use_srtp(AppOpts)),
    ?SIPSTORE:store_u('stateless', Stateless=?U:parse_stateless(AppOpts,false)), % block duplicates, auto repeat udp to devices
    ?SIPSTORE:store_u('certdir', CertDir=?U:parse_certdir(AppOpts)),
    ?SIPSTORE:store_u('keypass', KeyPass=?U:parse_keypass(AppOpts)),
    ?SIPSTORE:store_u('sip_alg', SipAlgAddrs = ?U:parse_sip_alg(AppOpts)),
    ?SIPSTORE:store_u('substitute_domains', Subst = ?U:parse_substitute_domains(AppOpts)),
    %
    ?SipAlg:setup(SipAlgAddrs),
    ?SG_Substitute:setup(Subst),
    %
    case ?SG_REREGMON_OPTS:check_rereg_in_opts(AppOpts) of true -> ?SG_REREGMON_OPTS:start_rereg();    _ -> ok    end,
    %
    Plugins = [r_plug_bestinterface,
               r_plug_filter,
               r_plug_sg,
               % r_plug_localdomain,
               r_plug_log
               %
               %nksip_registrar,
               %nksip_100rel,
               %nksip_gruu,
               %nksip_outbound,
               %nksip_timers,
               %nksip_refer
              ],
    {VerificationResult, UpOpts} = ?U:check_and_filter_ssl_opts(AppOpts),
    ?OUT("era_sip. sg. check_and_filter_ssl_opts VerificationResult: ~120p",[VerificationResult]),
    DirCRT = filename:join(CertDir, ?EU:to_list(?ENVCERT:cert_filename())),
    DirKEY = filename:join(CertDir, ?EU:to_list(?ENVCERT:key_filename())),
    CoreOpts1 = #{plugins => lists:filter(fun(false) -> false; (_) -> true end, Plugins),
                 tls_certfile => DirCRT,
                 tls_keyfile => DirKEY,
                 tls_sni_fun => fun(ServerName) -> ?MODULE:sni_fun(ServerName) end,
                 tls_ciphers => ?U:ciphers(),
                 sip_no_100 => case not ?Auto100answer of false -> Stateless; true -> true end, % test
                 %sip_no_100 => Stateless,
                 transports => ?U:parse_transports(UpOpts)},
    CoreOpts2 = append_tls_pass(CoreOpts1, KeyPass),
    nksip:start_link(?MODULE, CoreOpts2).

%% @private RP-2170
append_tls_pass(CoreOpts, []) -> CoreOpts;
append_tls_pass(CoreOpts, KeyPass) ->
    [{tls_password, KeyPass} | CoreOpts].

%% --------------------------------
%% @doc Stops the SipApp.
%% --------------------------------
stop() ->
    nksip:stop(?SIPAPP),
    %
    ?SG_REREGMON_OPTS:stop_rereg(),
    ?SUPV:terminate_child(?SG_SUPV),
    ?SUPV:delete_child(?SG_SUPV).

%% --------------------------------
%% @doc Updates role configuration opts
%% --------------------------------
update_opts(Opts) ->
    case ?U:check_update_opts(Opts) of
        restart -> restart;
        ok ->
            ?SG_REREGMON_OPTS:update_rereg(Opts),
            update_opts_2(Opts)
    end.
%% @private
update_opts_2(Opts) ->
    {_,D1} = ?SIPSTORE:find_u('certdir'),
    D2 = ?U:parse_certdir(Opts),
    case D1 == D2 of
        false -> restart;
        true -> update_opts_3(Opts)
    end.
%% @private
update_opts_3(Opts) ->
    {_,K1} = ?SIPSTORE:find_u('keypass'),
    K2 = ?U:parse_keypass(Opts),
    case K1 == K2 of
        false -> restart;
        true -> ok
    end.

%% --------------------------------
%% @doc Notified when configuration was changed
-spec update_cfg() -> ok.
%% --------------------------------
update_cfg() ->
    {ok,[SipRole]} = ?ENV:get_env(siprole),
    {ok,AppOpts} = ?APP:get_opts(SipRole),
    ?SIPSTORE:store_u('sip_alg', SipAlgAddrs = ?U:parse_sip_alg(AppOpts)),
    ?SIPSTORE:store_u('substitute_domains', Subst = ?U:parse_substitute_domains(AppOpts)),
    ?SipAlg:setup(SipAlgAddrs),
    ?SG_Substitute:setup(Subst).

%% ---
-spec clear_cert_cache() -> ok.
%% ---
clear_cert_cache() ->
    ssl:clear_pem_cache(),
    ?ENVSNICERT:clear_cache(#{module => ?MODULE,
                              store => ?SIPSTORE}),
    ok.


%% ==================================================================================
%% Callback functions
%% ==================================================================================

%% --------------------------------------
%% TLS SNI
%% --------------------------------------
sni_fun(ServerName) ->
    ?ENVSNICERT:sni_fun(ServerName,#{module => ?MODULE,
                                     store => ?SIPSTORE}).

%% ----------------------------------------------------------------------------------
%% @doc SipApp Callback: Initialization.
%% ----------------------------------------------------------------------------------

init([]) ->
    %erlang:start_timer(?TIME_CHECK, self(), check_speed),
    %nksip:put(?SIPAPP, speed, []),
    {ok, #state{auto_check=false}}.


%% ----------------------------------------------------------------------------------
%% @doc SipApp Callback: Called to check user's password.
%% ----------------------------------------------------------------------------------

%sip_get_user_pass(User, Realm, _Req, _Call) ->
%   ?ACCOUNTS:get_password_lazy(User, Realm).

%% ----------------------------------------------------------------------------------
%% @doc SipApp Callback: Called to check if a request should be authorized.
%% ----------------------------------------------------------------------------------

%sip_authorize(_Auth, _Req, _Call) ->
%    ok.

%% ----------------------------------------------------------------------------------
%% @doc SipApp Callback: Called to decide how to route every new request.
%% ----------------------------------------------------------------------------------

%% sip_route(Scheme, User, Domain, Req, Call) ->
%%     {reply, ?ServiceUnavailable("Gate. Test")};
sip_route(_Scheme, _User, <<"224.0.1.75">>, _Req, _Call) -> block; % auto provision broadcast
sip_route(Scheme, User, Domain, Req, Call) ->
    NowGS = ?EU:current_gregsecond(),
    FunStore = fun(undefined) -> 1; (N) -> N+1 end,
    ?SIPSTORE:func_t({count_all_req,NowGS}, 20000, FunStore),
    #sipmsg{cseq={_,Method}}=Req,
    stat(Method,Req),
    case Method of
        'REGISTER' -> ?SIPSTORE:func_t({count_register,NowGS}, 20000, FunStore);
        'NOTIFY' -> ?SIPSTORE:func_t({count_notify,NowGS}, 20000, FunStore);
        'SUBSCRIBE' -> ?SIPSTORE:func_t({count_subscribe,NowGS}, 20000, FunStore);
        'INVITE' -> ?SIPSTORE:func_t({count_invite,NowGS}, 20000, FunStore);
        _ -> ok
    end,
    route(Scheme, User, Domain, Req, Call).

%% @private
stat(Method,Req) ->
    % store stat
    ?STAT_FACADE:incoming_request(Req), % @stattrace
    ?STAT_FACADE:link_domain_filter(Method,Req#sipmsg.call_id,(element(1,Req#sipmsg.to))#uri.domain), % RP-415
    % #354
    case ?STAT_FACADE:store_dialog(Req,10800000) of
        false -> ok;
        _ ->
            #sipmsg{call_id=CallId,cseq={_,Method}}=Req,
            ?BLlog:write({sip,callid}, {"callid='~s'; method='~s'",[CallId,Method]}) % #354
            %?BLlog:write({sip,callid}, {"callid='~s'; method='~s'; from='~s'; rcvfrom='~s'",[CallId,Method,?U:unparse_uri(?U:clear_uri2(element(1,Req#sipmsg.from))),rcv_from(Req)]}) % #354
    end.

%% ----------------------------------------------------------------------------------
%% dialog_update
%% ----------------------------------------------------------------------------------
-spec(sip_dialog_update(DialogStatus, Dialog::nksip:dialog(), Call::nksip:call()) -> ok
     when DialogStatus :: start | target_update | stop |
                          {invite_status, nksip_dialog:invite_status() | {stop, nksip_dialog:stop_reason()}} |
                          {invite_refresh, SDP::nksip_sdp:sdp()} |
                          {subscription_status, nksip_subscription:status(), nksip:subscription()}).
%% --------------------------------
sip_dialog_update(_Status, _Dialog, _Call) ->
    ok.

%% ----------------------------------------------------------------------------------
%% session_update
%% ----------------------------------------------------------------------------------
-spec( sip_session_update(SessionStatus, Dialog::nksip:dialog(), Call::nksip:call()) -> ok
    when SessionStatus :: {start, Local, Remote} | {update, Local, Remote} | stop,
         Local::nksip_sdp:sdp(), Remote::nksip_sdp:sdp()).
%% --------------------------------
sip_session_update(_Status, _Dialog, _Call) ->
    ok.

%% ----------------------------------------------------------------------------------
%% @doc sip_log plugin callback
%% ----------------------------------------------------------------------------------
-spec sip_log(Query::{trn_send | trn_recv, NKPort::#nkport{}, Packet::binary()}, AppId::atom()) -> ok.
%% -------------------------
sip_log(Query, AppId) ->
    ?LOGGING:sip_log(Query, AppId).

%% ----------------------------------------------------------------------------------
%% @doc sip_filter plugin callback
%% ----------------------------------------------------------------------------------
-spec sip_filter(Query::{recv, NKPort::#nkport{}, Packet::binary()}, AppId::atom()) -> ok | block.
%% --------------------------------
sip_filter(Query, AppId) ->
    ?FILTER:transport_filter(dynamic, Query, AppId).

%% ----------------------------------------------------------------------------------
%% @doc border_gate callback interface
%% ----------------------------------------------------------------------------------

%% ----------------------------------------------------------------------------------
%% @doc Called when the UAC is preparing a request to be sent
%% ----------------------------------------------------------------------------------
%% -spec nks_sip_uac_pre_request(nksip:request(), nksip:optslist(),
%%                            nksip_call_uac:uac_from(), nksip:call()) ->
%%     {continue, list()}.
%%
%% nks_sip_uac_pre_request(Req, Opts, From, Call) ->
%%     ?LOGSIP("SIP. Border Gate. nks_sip_uac_pre_request!"),
%%     NewReq = ?BG_CB:bg_uac_pre_request(Req,[]),
%%     {continue, [NewReq, Opts, From, Call]}.

%% ----------------------------------------------------------------------------------
%% @doc Called to add headers just before sending the request
%% ----------------------------------------------------------------------------------
-spec nks_sip_transport_uac_headers(nksip:request(), nksip:optslist(), nksip:scheme(),
                                    nkpacket:transport(), binary(), inet:port_number()) ->
          {ok, nksip:request()}.
%% --------------------------------
nks_sip_transport_uac_headers(Req, _Opts, _Scheme, _Transp, Host, _Port) ->
    %?LOGSIP("SIP. Border Gate. nks_sip_transport_uac_headers!"),
    Req1 = ?BG_CB:bg_uac_pre_request(Req,Host),
    Req2 = ?SG_TRANSLIT:trans_uac_headers(Req1),
    Req3 = ?SG_Substitute:on_uac_headers(Req2),
    {continue, [Req3, _Opts, _Scheme, _Transp, Host, _Port]}.


%% ----------------------------------------------------------------------------------
%% @doc Called when preparing a UAS dialog response
%% ----------------------------------------------------------------------------------
-spec nks_sip_uas_dialog_response(nksip:request(), nksip:response(), nksip:optslist(), nksip:call()) ->
          {ok, nksip:response(), nksip:optslist()}.
%% --------------------------------
nks_sip_uas_dialog_response(_Req, Resp, Opts, _Call) ->
    %?LOGSIP("SIP. Border Gate. nks_sip_uas_dialog_response!"),
    Resp1 = ?BG_CB:bg_uas_dialog_response(Resp),
    Resp2 = ?SG_TRANSLIT:trans_uas_dialog_response(Resp1),
    Resp3 = ?SG_Substitute:on_uas_dialog_response(Resp2),
    {continue, [_Req, Resp3, Opts, _Call]}.

%% ----------------------------------------------------------------------------------
%% @doc Domain Callback: Synchronous user call.
%% ----------------------------------------------------------------------------------
%% sip_checkclusterdomain(Domain, _AppId) ->
%%     ?LOCALDOMAIN:is_cluster_domain(Domain).

%% ----------------------------------------------------------------------------------
%% @doc SipApp Callback: Synchronous user call.
%% ----------------------------------------------------------------------------------

handle_call(_Msg, _From, _State) ->
    %%{reply, null, State}.
    continue.

%% ----------------------------------------------------------------------------------
%% @doc SipApp Callback: Asynchronous user cast.
%% ----------------------------------------------------------------------------------

handle_cast(_Msg, _State) ->
    %%{noreply, State}.
    continue.

%% ----------------------------------------------------------------------------------
%% @doc SipApp Callback: External erlang message received.
%% The programmed timer sends a `{timeout, _Ref, check_speed}' message
%% periodically to the SipApp.
%% ----------------------------------------------------------------------------------

handle_info(_Msg, _State) ->
    %%{noreply, State}.
    continue.

%% ==========================================================================
%% Internal functions
%% ==========================================================================

%% --------------------------------------
%% ROUTE
%% --------------------------------------

%% --------------------------------
%% Routing Request
%% + 1) ruri is known internal logical domain -> route to forkrouter
%% - ruri is my ip -> check To:
%%      1a) internal logical domain -> route to forkrouter
%%        2a) my ip -> invalid
%%       3a) others -> invalid

%% - ruri is my ip -> check routes
%% *    2x) no routes -> forbidden
%% -    found routes -> check first as ruri
%% +         2a) internal logical domain -> route to forkrouter
%% *         2b) my ip -> loop detected
%% ~             2c) known internal ip -> proxy_stateless
%% ~            2d) unknown -> forbidden
%% - ruri is outside or known internal ip -> check routes
%% ?       3x) no routes -> forbidden | proxy_stateless to ruri
%% -       found routes -> check first
%% ?             3a) internal logical domain -> forbidden - (error, webrtc always set first route to domain), so allow
%% +         3b) my ip -> proxy_stateless to ruri
%% ?            3c) known internal ip -> forbidden | proxy_stateless to ruri
%% ?         3d) unknown -> forbidden | proxy_stateless to ruri

% @todo test via & record route double on repeat on gate
%route(_,_,_,#sipmsg{cseq={_,'ACK'}},_) -> ?OUT("Block ACK"), block;

route(Scheme, User, Domain, Req, _Call) ->
    {_,Method} = nksip_request:method(Req),
    ?LOGSIP("sip_route ~p, ~p, ~p, ~p", [Scheme, User, Domain, Method]),
    %
    % proxing options - need to add path in REGISTER to participate in routes (;received)
    Opts = case Method of
               'INVITE' -> [record_route,followredirects];
               'SUBSCRIBE' -> [record_route,followredirects];
               _ -> [followredirects]
           end,
    FunPath = fun() ->
                      #sipmsg{supported=Supported}=Req,
                      case Method=='REGISTER' orelse Method=='SUBSCRIBE' of
                          true ->
                              case lists:member(<<"path">>, Supported) of
                                  true -> [path];
                                  false -> [path, pathreg, {insert, <<"Supported">>, <<"pathreg">>}]
                              end;
                          false -> []
                      end end,
    Req1 = ?SG_Substitute:do_forward_request(Req,true),
    do_route(Method,Opts,FunPath,Req1).
%%
do_route(Method,Opts,FunPath,Req) ->
    case Method=='OPTIONS' andalso not is_outgoing_request(Req) of
        true -> process;
        _ -> do_route_1(Opts,FunPath,Req)
    end.
%%
do_route_1(Opts,FunPath,Req) ->
    %new_route(Opts,FunPath,Req).
    old_route(Opts,FunPath,Req).

%% =========================================
%% Old routing code
%% =========================================

%% @private
old_route(Opts,FunPath,Req) ->
    old_route_1(Opts,FunPath,Req).

%% @private
old_route_1(Opts,FunPath,Req) ->
    % ?OUT("R1"),
    #sipmsg{ruri=RUri}=Req,
    % routing inside to responsible sip-server
    %{IsLocalDomainRuri, IsLocalIpRuri, IsClusterIpRuri}
    case ?ROUTE:get_local_domain_flags(RUri) of
        % RURI is known cluster domain
        {true, _, _} ->
            % ?OUT("R1 A"),
            route_inside(Req, RUri#uri{port=0}, Opts ++ FunPath()); % 1)
        % RURI is not known cluster domain
        {false, IsLocalIpRUri_, _} ->
            % ?OUT("R1 B"),
            old_route_2({Opts,FunPath,IsLocalIpRUri_}, Req)
    end.

%% @private
old_route_2(P, Req) ->
    % ?OUT("R2"),
    case ?ROUTE:get_top_route_uri(Req) of
        % found route where is text domain or non-current server address
        #uri{}=TopRouteUri ->
            % ?OUT("R2 A"),
            old_route_by_toproute(TopRouteUri, P, Req);
        % no routes (or this server's address in all existing routes)
        notfound ->
            % ?OUT("R2 B"),
            #sipmsg{to={#uri{domain=ToDomain}, _Tag}}=Req,
            % ?OUT("ToDomain: ~120tp", [ToDomain]),
            case ?ROUTE:get_local_domain_flags(ToDomain) of
                % to is local domain
                {true, _, _} ->
                    % ?OUT("R2 B1"),
                    old_route_by_todomain(P, Req);
                _ ->
                    % ?OUT("R2 B2"),
                    % ?OUT("Req: ~120tp", [Req]),
                    old_route_unknown(P, Req)
            end end.

%% @private @sub
old_route_by_toproute(TopRouteUri, {Opts,FunPath,IsLocalIpRUri_}, Req) ->
    % ?OUT("TR"),
    %{IsLocDomTR, IsLocIpTR, IsClustrIpTR}
    case {IsLocalIpRUri_, ?ROUTE:get_local_domain_flags(TopRouteUri)} of
        % ruri=myip, route=mydomain
        {true, {true, _, _}} ->
            % ?OUT("TR A"),
            route_inside(Req, TopRouteUri#uri{port=0}, Opts ++ FunPath()); % 2a)
        % ruri=myip, route=myip - it's impossible due to get_top_route_uri/1
        {true, {false, true, _}} ->
            % ?OUT("TR B"),
            {reply, ?LoopDetected("Gate. Looped route")}; % 2b)
        % ruri=myip, route=unknown (@todo may be cluster)
        {true, {false, false, _}} ->
            % ?OUT("TR C"),
            route_proxy_auto(Req,ruri,Opts); % 2c)
        % ruri=unknown, route=mydomain
        {false, {true, _, _}} ->
            % ?OUT("TR D"),
            % {reply, ?Forbidden("Gate. Invalid route")}; % 3a)
            route_inside(Req, TopRouteUri#uri{port=0}, Opts ++ FunPath()); % 3a)
        % ruri=unknown, route=myip - it's impossible due to get_top_route_uri/1
        {false, {false, true, _}} ->
            % ?OUT("TR E"),
            route_proxy_auto(Req,ruri,Opts); % 3b)
        % ruri=unknown, route=unknown - bye,ack on incoming
        _ ->
            % ?OUT("TR F"),
            route_proxy_auto(Req,ruri,Opts)
    end.

%% @private @sub
old_route_by_todomain({Opts,FunPath,IsLocalIpRUri_}, Req) ->
    % ?OUT("TD"),
    % wrong when ack after 4xx without route. send back to b2bua
    %   route_inside(Req, ToUri#uri{disp= <<>>, user= <<>>}, Opts ++ FunPath()); % 1x
    % it should be:
    %   {proxy_stateless, ruri, Opts};
    % but register to local address should be forwarded inside, so backwards
    #sipmsg{vias=[#via{domain=ViaDomain}|_],
            ruri=#uri{domain=RUriDomain}=_RUri,
            to={ToUri,_}}=Req,
    case is_insider_sip(ViaDomain, Req) of
        true ->
            % ?OUT("TD A"),
            route_proxy(Req,ruri,Opts);
        false ->
            % ?OUT("TD B"),
            case IsLocalIpRUri_ of
                true ->
                    % ?OUT("TD B1"),
                    route_inside(Req, ToUri#uri{disp= <<>>, user= <<>>}, Opts ++ FunPath());
                false ->
                    % ?OUT("TD B2"),
                    case is_insider_sip(RUriDomain, Req) of
                        true ->
                            % ?OUT("TD B21"),
                            %route_inside(Req, RUri, Opts ++ FunPath());
                            route_proxy_inside(Req,ruri,Opts); % #244,#248,#262
                        false ->
                            % ?OUT("TD B22"),
                            check_filter_on_reply(Req, {reply, ?Forbidden("Gate. Unknown domain,route")})
                    end end end.

%% @private @sub
old_route_unknown({Opts,FunPath,IsLocalIpRUri_}, Req) ->
    % ?OUT("UN"),
    % when bye to external - no unknown route (only current gate in route)
    %                       - 'To:' domain is unknown (external account)
    % only record-route in outgoing b2bua invite makes it work.
    % if not - we should check if ruri domain is cluster server
    % {reply, ?Forbidden("Gate. No route and unknown to")} % 1x), 2x), 3x)
    #sipmsg{ruri=#uri{domain=RUriDomain}=RUri}=Req,
    case is_insider_sip(RUriDomain, Req) andalso not IsLocalIpRUri_ of
        true ->
            % ?OUT("UN A"),
            route_proxy(Req,ruri,Opts); % directly to contact?
        false ->
            % ?OUT("UN B"),
            % referring to esg external account
            %  or from is localdomain but to is localip (asterisk pochta)
            #sipmsg{from={#uri{domain=FromDomain},_}}=Req,
            case ?ROUTE:get_local_domain_flags(FromDomain) of
                {true, _, _} ->
                    % ?OUT("UN B1"),
                    route_inside(Req, RUri, Opts);
                _ ->
                    % ?OUT("UN B2"),
                    % #140 29.03.2017
                    #sipmsg{nkport=#nkport{transp=Proto,remote_ip=RcvIp,remote_port=RcvPort}}=Req,
                    RcvFrom = {Proto,RcvIp,RcvPort},
                    case ?NOREG:find(RcvFrom) of
                        {ok,_} ->
                            % ?OUT("UN B21"),
                            % #140 route inside
                            route_inside(Req, RUri#uri{port=0}, Opts ++ FunPath());
                        _ ->
                            % ?OUT("UN B22"),
                            case is_insider_sip(?EU:to_binary(inet:ntoa(RcvIp)), Req) of
                                % #140 route to outer
                                true ->
                                    % ?OUT("UN B221"),
                                    route_proxy(Req,ruri,Opts);
                                false ->
                                    % ?OUT("UN B222"),
                                    check_filter_on_reply(Req, {reply, ?Forbidden("Gate. Unknown domain,to,route")}) % 1x), 2x), 3x)
                            end end end end.

%% =========================================
%% New routing code
%% =========================================

%% % @private
%% new_route(Opts,FunPath,Req) ->
%%      case is_outgoing_request(Req) of
%%          true -> do_route_outside()
%%          false -> do_route_inside()
%%      end.
%%
%% do_route_outside() -> route_proxy(Req,ruri,Opts).
%% do_route_inside() -> ok. .. todomain, toproute, #140,

%% =========================================
check_filter_on_reply(Req, Reply) ->
    case ?FILTER:check_route_inside(Req) of
        block -> block;
        _ -> ?FILTER:check_block_response_malware(Reply)
    end.

%% -------------------------
%%  Makes routing inside cluster
%% -------------------------
route_inside(Req, RequestUri, Opts) ->
    #sipmsg{call_id=_CallId}=Req,
    case ?FILTER:check_route_inside(Req) of
        ok -> route_inside_1(Req, RequestUri, Opts);
        block -> block;
        {reply,_}=Reply -> Reply
    end.
%%
route_inside_1(Req, RequestUri, Opts) ->
    #sipmsg{call_id=CallId} = Req,
    % @TODO check apply here
    %   find_responseible_b2bua only from store. if found - go there.
    %   if not - check number prefix (pickup, conference, ...). if found - script operation on every prefix type, for ex check pickup could be applied and go to b2bua where dialog found. and store.
    %   if not - common behaviour
    case ?SERVERS:find_responsible_b2bua(CallId) of
        false -> {reply, ?ServiceUnavailable("Gate. Responsible servers not found")};
        RouteDomain ->
            RouteUri = {route, ?U:unparse_uri(#uri{scheme=sip,domain=RouteDomain, opts=?CFG:get_transport_opts()++[<<"lr">>]})},
            SrvIdxT = ?U:get_current_srv_textcode(),
            Opts1 = case ?U:is_b2bmedia() of
                        false ->  [{replace, {?B2BHeader, <<"media=0">>}}|Opts];
                        true -> Opts
                    end,
            % ---
            {get_proxy_mode(), RequestUri, [RouteUri,
                                            {replace, {?OwnerHeader, <<"rGK-", SrvIdxT/bitstring>>}},
                                            {replace, {?RcvFromHeader, rcv_from(Req)}} % #149
                                           |Opts1]}
    end.

%% #149
%% @private
rcv_from(Req) ->
    #sipmsg{nkport=NkPort}=Req,
    #nkport{transp=Transp, remote_ip=Ip, remote_port=Port}=NkPort,
    {BTransp,BIp,BPort} = {?EU:to_binary(Transp),?EU:to_binary(inet:ntoa(Ip)),?EU:to_binary(Port)},
    <<"<", BTransp/bitstring, ":", BIp/bitstring, ":", BPort/bitstring, ">">>.

%% --------------------------
%% Makes routing through
%% -------------------------
route_proxy(#sipmsg{vias=[#uri{domain=D,port=P}|_],routes=[#uri{domain=D,port=P}|_]}, RequestUri, Opts) ->
    ?U:refresh_nksip_cache_local_ips(),
    timer:sleep(500),
    {get_proxy_mode(), RequestUri, Opts};
route_proxy(_Req, RequestUri, Opts) ->
    {get_proxy_mode(), RequestUri, Opts}.

%% Makes routing through, select direction by rcvfrom address (is insider sip server?)
route_proxy_auto(Req, RequestUri, Opts) ->
    #sipmsg{nkport=#nkport{remote_ip=RcvIp}}=Req,
    case is_insider_sip(?EU:to_binary(inet:ntoa(RcvIp)), Req) of
        true -> route_proxy(Req, RequestUri, Opts);
        false -> route_proxy_inside(Req, RequestUri, Opts) % #149
    end.

%% Makes routing through to insider srv (add some headers)
route_proxy_inside(Req, RequestUri, Opts) ->
    % #149
    #sipmsg{nkport=#nkport{transp=Transp, remote_ip=Ip, remote_port=Port}}=Req,
    {BTransp,BIp,BPort} = {?EU:to_binary(Transp),?EU:to_binary(inet:ntoa(Ip)),?EU:to_binary(Port)},
    RcvFrom = <<"<", BTransp/bitstring, ":", BIp/bitstring, ":", BPort/bitstring, ">">>,
    Opts1 = [{replace, {?RcvFromHeader, RcvFrom}} | Opts],
    route_proxy(Req, RequestUri, Opts1).

%% ---
%% @private
get_proxy_mode() ->
    case ?SIPSTORE:find_u('stateless') of
        {_,true} -> proxy_stateless;
        {_,false} -> proxy
    end.

%% ---
%% @private
is_outgoing_request(Req) ->
    #sipmsg{nkport=#nkport{transp=_Proto,remote_ip=RcvIp,remote_port=_RcvPort}}=Req,
    is_insider_sip(?EU:to_binary(inet:ntoa(RcvIp)), Req).

%% @private
is_insider_sip(Addr, Req) ->
    Key = {sip_insider_addr, Addr},
    case ?SIPSTORE:find_t(Key) of
        false -> false;
        {_,_} -> not is_misc_sender(Req)
    end.

%% @private
is_misc_sender(Req) ->
    case nksip_request:header(?MiscHeaderLow, Req) of
        {ok,[_|_]} -> true;
        _ -> false
    end.

%% @private
is_b2bua(Addr) ->
    Key = {b2bua_addr, Addr},
    case ?SIPSTORE:find_t(Key) of
        false -> false;
        {_,_} -> true
    end.

%% ----------------------------------------------------------------------------------
%% OPTIONS
%% ----------------------------------------------------------------------------------
-spec(sip_options(Request::nksip:request(), Call::nksip:call()) ->
             {reply, nksip:sipreply()} | noreply).
%% -------------------------
sip_options(Req, _Call) ->
    PutSdp = case nksip_request:header(<<"accept">>, Req) of
                 {ok, []} -> true;
                 {ok, Accept} -> find_in_binary(Accept, <<"application/sdp">>);
                 {error,_} -> false
             end,
    Opts = [{replace, {<<"Allow">>, <<"REGISTER, INVITE, ACK, CANCEL, BYE, REFER, OPTIONS, NOTIFY, SUBSCRIBE, INFO, MESSAGE, UPDATE">>}}, % <<"PUBLISH">>
            {replace, {<<"Allow-Events">>, <<"refer, presence, dialog">>}}, % <<"kpml(Zoiper) talk,hold,conferencecheck_sync(Yealink)">>
            % {replace, {<<"Accept-Encoding">>, <<"">>}},
            % {replace, {<<"Accept-Language">>, <<"en">>}},
            {replace, {<<"Supported">>, <<"replaces, timer, path">>}} % <<"outbound, norefersub(Zoiper), extended-refer(Zoiper), gruu(JsSIP)">>
           ],
    Opts1 = case PutSdp of
                true ->
                    [{replace, {<<"Accept">>, <<"application/sdp">>}},
                     {replace, {<<"Content-Type">>, <<"application/sdp">>}},
                     {body, ?M_SDP:make_options_sdp()}
                    |Opts];
                false -> Opts
            end,
    {reply, {200,Opts1}}.

%% @private
find_in_binary([], _Key) -> false;
find_in_binary([Item|Rest], Key) ->
    case binary:match(?EU:to_lower(Item), Key) of
        nomatch -> find_in_binary(Rest, Key);
        {_From,_Count} -> true
    end.
