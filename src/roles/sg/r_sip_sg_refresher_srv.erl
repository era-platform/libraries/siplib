%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 22.04.2017
%%% @doc Implements some sg refresh services, ex.
%%%     Store insider addrs from config list into store ets

-module(r_sip_sg_refresher_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("../include/r_sip.hrl").

-record(state, {ref,timerref}).

-define(RefreshTimeout, 10000).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(StartArgs) ->
    gen_server:start_link(?MODULE, StartArgs, []).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(_Args) ->
    Ref = make_ref(),
    State = #state{ref=Ref,
                   timerref=erlang:send_after(0, self(), {timer,Ref})},
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

%% other
handle_call(_Request, _From, State) ->
    Reply = ok,
    {reply, Reply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

%% other
handle_cast(_Request, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

handle_info({timer,Ref}, #state{ref=Ref}=State) ->
    State1 = refresh_insiders(State),
    State2 = State1#state{timerref=erlang:send_after(?RefreshTimeout, self(), {timer,Ref})},
    {noreply, State2};

handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

refresh_insiders(State) ->
    refresh_insiders(),
    State.

refresh_insiders() ->
    B2BUAs = ?CFG:get_all_b2bua(),
    Fmap = fun(List) ->
                   F = fun(Addr) -> [A|_] = binary:split(Addr,<<":">>), A end,
                   lists:usort(lists:flatten([lists:map(F, Addrs) || {_,Addrs} <- List]))
           end,
    %
    B2BUAs1 = Fmap(B2BUAs),
    FKeyB2B = fun(Addr) -> {b2bua_addr, Addr} end,
    lists:foreach(fun(Addr) -> ?SIPSTORE:store_t(FKeyB2B(Addr), true, ?RefreshTimeout * 3) end, B2BUAs1),
    %
    Insiders = ?CFG:get_all_sipserver_addrs(),
    FKeyInsiders = fun(Addr) -> {sip_insider_addr, Addr} end,
    lists:foreach(fun(Addr) -> ?SIPSTORE:store_t(FKeyInsiders(Addr), true, ?RefreshTimeout * 3) end, Insiders),
    %
    Map = maps:from_list(lists:map(fun(Addr) -> {Addr,true} end, Insiders)),
    ?SIPSTORE:store_t(sip_insider_addrs, Map, ?RefreshTimeout * 3),
    ?SIPSTORE:store_t(b2bua_addrs, Map, ?RefreshTimeout * 3).
