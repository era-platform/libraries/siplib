%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc
%%% @todo

-module(r_sip_sg_stat).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ==========================================================================
%% Defines
%% ==========================================================================

-include("../include/r_sip.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% server's activity (count of requests per interval)
query(<<"active">>, #{}=_Map) ->
    NowGS = ?EU:current_gregsecond(),
    Fmap = fun(K,V,Acc) ->
                    case maps:find(K,Acc) of
                        error -> maps:put(K, [V], Acc);
                        {_,V1} -> maps:put(K, [V|V1], Acc)
                    end end,
    Msg = [count_all_req,
           count_invite,
           count_register,
           count_notify,
           count_subscribe],
    Ffilter = fun({{R,GS},N}, Acc) ->
                      case lists:member(R, Msg) of
                          true -> Fmap(R,{GS,N},Acc);
                          false -> Acc
                      end;
                  (_,Acc) -> Acc
                end,
    Facc = fun({GS,_},Acc) when GS==NowGS -> Acc;
                 ({GS,N},[Ss1,Ss20]) when GS==NowGS-1 -> [Ss1+N,Ss20];
                 ({_,N},[Ss1,Ss20]) -> [Ss1,Ss20+N]
           end,
    Lst = lists:sort(maps:to_list(?SIPSTORE:foldl_t(Ffilter,maps:new()))),
    R = lists:foldl(fun({K,V}, Acc) ->
                        X = lists:reverse(lists:sort(V)),
                        Y = lists:zip([sec1,sec20],lists:foldl(Facc,[0,0],X)),
                        [{K,Y}|Acc]
                    end, [], Lst),
    Z = lists:sort(lists:zip(Msg, lists:map(fun(_) -> [{sec1,0},{sec20,0}] end, lists:seq(1, length(Msg))))),
    R1 = lists:ukeymerge(1, lists:sort(R), Z),
    {ok, #{load => R1}};

%% read server's dynamic banned remote addrs
query(<<"banned_read">>, #{}=_Map) ->
    NowGS = ?EU:current_gregsecond(),
    R = ?FILTER_SRV:get_banned(),
    R1 = lists:map(fun(A) ->
                           [{<<"ip">>, ?EU:to_binary(inet:ntoa(maps:get(remoteip,A,{0,0,0,0})))},
                            {<<"expires">>, maps:get(expiregs,A)-NowGS}]
                   end, R),
    {ok, #{banned => R1}};

%% create server's dynamic banned addr
query(<<"banned_create">>, #{}=Map) ->
    case ?EU:extract_optional_props([ip,expires], maps:to_list(Map)) of
        [undefined,_] -> #{result => false};
        [_,undefined] -> #{result => false};
        [Ip,Expires] ->
            ?FILTER_SRV:ban(Ip, Expires),
            {ok, #{result => true}}
    end;

%% clear server's dynamic banned addrs
query(<<"banned_clear">>, #{}=_Map) ->
    ?FILTER_SRV:clear_banned(),
    {ok, #{result => true}};

%% delete ip from server's dynamic banned addrs
query(<<"banned_delete">>, #{}=Map) ->
    case maps:get(ip, Map, undefined) of
        undefined -> #{result => false};
        Ip ->
            ?FILTER_SRV:unban(Ip),
            {ok, #{result => true}}
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================


