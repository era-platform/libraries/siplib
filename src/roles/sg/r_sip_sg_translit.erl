%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Anton Makarov <anton@mastermak.ru>, Peter Bukashin <tbotc@yandex.ru>
%%% @doc @todo Add description to r_sip_sg_translit.

-module(r_sip_sg_translit).

-export([trans_uac_headers/1,
         trans_uas_dialog_response/1]).

%% ==========================================================================
%% Defines
%% ==========================================================================

-include("../include/r_sip.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ----------------------------------------------------------------------------------
%% @doc gate translit callback interface
%% ----------------------------------------------------------------------------------

%% ----------------------------------------------------------------------------------
%% @doc Called when the UAC is preparing a request to be sent
%% ----------------------------------------------------------------------------------
%%
trans_uac_headers(#sipmsg{}=Req) ->
    do_forward_request(Req).

%% ----------------------------------------------------------------------------------
%% @doc Called when preparing a UAS dialog response
%% ----------------------------------------------------------------------------------
trans_uas_dialog_response(#sipmsg{}=Resp) ->
    do_forward_response(Resp).

%% ====================================================================
%% Internal functions
%% ====================================================================
%% ----------------------------------------------------------------------------------
%% Request Handler
%% ----------------------------------------------------------------------------------
do_forward_request(Req) ->
    case is_outgoing(Req) of
        true ->
            case needs_translit() of
                true ->
                    ?TRANSLIT:translit_sip_packet(Req);
                false ->
                    Req
            end;
        false ->
            Req
    end.

%% ----------------------------------------------------------------------------------
%% Response Handler
%% ----------------------------------------------------------------------------------
do_forward_response(Resp) ->
    case is_outgoing(Resp) of
        true ->
            case needs_translit() of
                true ->
                    ?TRANSLIT:translit_sip_packet(Resp);
                false ->
                    Resp
            end;
        false ->
            Resp
    end.


%% ----------------------------------------------------------------------------------
%% Support Functions
%% ----------------------------------------------------------------------------------
%% Is checked only on gate so can tell by analyzing source
is_outgoing(#sipmsg{class={req, _},nkport=NkPort}=_R) ->
     #nkport{remote_ip=RemoteAddr} = NkPort,
    case RemoteAddr of
        undefined -> true;
        _ ->
            RemoteAddrBin = ?EU:to_binary(inet:ntoa(RemoteAddr)),
            OwnSIPAddrsList = ?CFG:get_all_sipserver_addrs(),
        %%     io:format("(SrcAddrBin:~p);OWNADDR:~p;ISSSS:~p~n",[SrcAddrBin,OwnSIPAddrsList,lists:member(SrcAddrBin,OwnSIPAddrsList)]),
            lists:member(RemoteAddrBin,OwnSIPAddrsList)
    end;

is_outgoing(#sipmsg{class={resp, _, _},vias=[Via|_]}=_R) ->
    #via{domain=SrcAddrBin}=Via,
    OwnSIPAddrsList = ?CFG:get_all_sipserver_addrs(),
    not lists:member(SrcAddrBin,OwnSIPAddrsList).

needs_translit() ->
     ?U:is_translit().

