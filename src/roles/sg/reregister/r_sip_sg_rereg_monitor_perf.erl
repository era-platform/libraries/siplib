%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Pavel Abramov, Peter Bukashin <tbotc@yandex.ru>
%%% @date 21.12.2016
%%% @doc 1) Обработка запросов от r_sip_sg_rereg_monitor_srv с запуском потока и передачей пида потока сразу.
%%%         2) Формирование списка работ.
%%%         3) Формирование пула рабочих процессов r_sip_sg_rereg_monitor_work (не больше 100).
%%%         4) Мониторинг всех профессов в пуле.
%%%         5) Нагрузка пула порциями работ.

-module(r_sip_sg_rereg_monitor_perf).
-author(['Pavel Abramov','Peter Bukashin <tbotc@yandex.ru>']).

-export([add_tasks/1, pool/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip_sg_rereg_monitor.hrl").

-define(MaxProcessesCount, 3).
-define(LoopTimeout, 60000).

%% ====================================================================
%% API functions
%% ====================================================================

%----------------------------------------------------------------------
% Получение заданий от сервера.
-spec add_tasks(Tasks) -> undefined | Pid when
    Tasks :: map(), % внутри лежит RegInfoList
    Pid :: pid().
add_tasks(Tasks) ->
    case maps:get(?TaskRegInfo, Tasks, undefined) of
        undefined -> undefined;
        RegInfoList -> erlang:spawn(?MODULE, pool, [RegInfoList])
    end.

%----------------------------------------------------------------------
% Подготовка и запуск пула процессов на основании задач.
-spec pool(RegInfoList) -> ok when
    RegInfoList :: [RegInfo],
    RegInfo :: tuple(). % #reg_contact{} определен в nksip
pool(RegInfoList) ->
    F = fun(RegInfo, {Count, Acc}) ->
            case Count+1 of
                UpC1 when UpC1 > ?MaxProcessesCount -> {Count,lists:append([RegInfo], Acc)};
                UpC2 -> take_task(RegInfo), {UpC2, Acc} end end,
    {AllCount, ResidueRegInfoList} = lists:foldl(F, {0,[]}, RegInfoList),
    loop(ResidueRegInfoList, AllCount).

%% ====================================================================
%% Internal functions
%% ====================================================================

% @private
% Функция ожидания ответа от рабочих потов.
-spec loop(RegInfoList, Ref) -> ok when
    RegInfoList :: [RegInfo],
    RegInfo :: tuple(), % #reg_contact{} определен в nksip
    Ref :: reference().
loop([],0) -> ok;
loop([],Count) ->
    receive
        {'DOWN', _MonitorRef, _Type, _Object, _Info} ->
            loop([], Count-1);
        {'EXIT',_FromPid,_Reason} ->
            loop([], Count-1)
    end;
loop([RegInfo|T], Count) ->
    receive
        {'DOWN', _MonitorRef, _Type, _Object, _Info} ->
            take_task(RegInfo),
            loop(T, Count);
        {'EXIT',_FromPid,_Reason} ->
            take_task(RegInfo),
            loop(T, Count)
    end.

%----------------------------------------------------------------------
% @private
% Получение задания из списка для свободного процесса.
-spec take_task(RegInfo) -> ok when
    RegInfo :: tuple(). % #reg_contact{} определен в nksip
take_task(RegInfo) ->
    Task1 = maps:new(),
    Task2 = maps:put(?TaskRegInfo, RegInfo, Task1),
    erlang:spawn_monitor(?REREGWORK, start_worker, [Task2]),
    ok.

