%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Pavel Abramov, Peter Bukashin <tbotc@yandex.ru>
%%% @date 19.12.2016
%%% @doc 1) ген сервер
%%%         2) Обработка запросов от супервизора(или от sync_strategy)(старт,стоп).
%%%         3) Запуск таймер (раз в 1 секунду).
%%%         4) Запросы к r_sip_sg_rereg_strategy_cd (при старте, при тике таймера)
%%%         5) Формирование списка активных гейтов.
%%%         6) Формирование порций работ.
%%%         7) Отправка запросов активным гейтам (с помощью env_multical, порциями) в r_sip_sg_rereg_monitor_perf
%%% @todo

-module('r_sip_sg_rereg_monitor_srv').
-author(['Pavel Abramov','Peter Bukashin <tbotc@yandex.ru>']).
-behaviour(gen_server).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-export([start_monitor/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip_sg_rereg_monitor.hrl").

-define(Timer, 3000).
-define(TimerMsg, <<"TIMEOUT">>).
-define(MulticallTimeout, 5000).

%% ====================================================================
%% API functions
%% ====================================================================

%% --------------------
-spec start_monitor(Opts::map()) -> {ok, Pid::pid()} | {error, Reason::binary()}.
%% --------------------
start_monitor(Opts) when is_map(Opts) ->
    case gen_server:start({local, ?MODULE}, ?MODULE, Opts, []) of
        {error, _} -> {error, <<"gen_server start fail">>};
        Ok -> Ok
    end;
start_monitor(_Opts) -> {error, <<"wrong param">>}.

%% ====================================================================
%% Behavioural functions
%% ====================================================================

init(_Opts) ->
    NewState = refresh_domains(#rereg_state{}),
    UpdateState = check_gates(NewState),
    {ok, UpdateState}.

%----------------------------------------------------------------------

handle_call(_Request, _From, State) ->
    {reply, ok, State}.

%----------------------------------------------------------------------

handle_cast(_Request, State) ->
    {noreply, State}.

%----------------------------------------------------------------------

handle_info({?TimerMsg, TemerRef}, #rereg_state{timer_ref=TemerRef,counter=Cnt}=State) ->
    State1 = refresh_domains(State),
    UpdateState = check_gates(State1),
    {noreply, UpdateState#rereg_state{counter=Cnt+1}};

handle_info({'DOWN', MonitorRef, _Type, Object, _Info}, State) ->
    UpdateState= check_monitor_process(Object, MonitorRef, State),
    {noreply, UpdateState};
handle_info({'EXIT',FromPid,_Reason}, State) ->
    UpdateState= check_monitor_process(FromPid, undefined, State),
    {noreply, UpdateState};

handle_info(_Info, State) ->
    {noreply, State}.

%----------------------------------------------------------------------

terminate(_Reason, State) ->
    {ok, State}.

%----------------------------------------------------------------------

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% --------------------
%% @private
%% --------------------
-spec start_timer(#rereg_state{}) -> #rereg_state{} .
%% --------------------
start_timer(#rereg_state{}=State) ->
    Ref = erlang:make_ref(),
    erlang:send_after(?Timer, self(), {?TimerMsg, Ref}),
    State#rereg_state{timer_ref=Ref}.

%% --------------------
%% @private
%% --------------------
-spec check_gates(#rereg_state{}) -> #rereg_state{}.
%% --------------------
check_gates(#rereg_state{}=State) ->
    {ok, Down, UpdateState} = ?REREGCHECKDOWN:check_down_sg_nodes(State),
    case Down of
        [] -> start_timer(UpdateState);
        _ ->
            Monitoring = send_next_portion(Down),
            UpdateState#rereg_state{monitoring=Monitoring}
    end.

%% --------------------
%% @private
%% --------------------
-spec get_active_gates() -> Dests::[{Site::binary(),Node::node()}].
%% --------------------
get_active_gates() ->
    Site = ?CFG:get_current_site(),
    Nodes = ?SG_REREGMON_SUPV:get_node_sg_on_site(),
    lists:map(fun(N) -> {Site,N} end, Nodes).

%% --------------------
%% @private
%% Получаем список активных гейтов. Отправлям задания. Мониторим процессы.
%% --------------------
-spec send_next_portion([RegInfo::tuple()]) -> [{MonitorRef::reference(),Pid::pid()}].
%% --------------------
send_next_portion(RegInfoList) ->
    Dests = get_active_gates(),
    Tasks = maps:put(?TaskRegInfo, RegInfoList, maps:new()),
    ResultList = ?ENVMULTICALL:call_rpco(Dests, {?REREGPERF, add_tasks, [Tasks]}, ?MulticallTimeout),
    F = fun({{_Site,_Node}=_Dest,Pid}) -> case Pid of undefined -> false; _ -> {true, {erlang:monitor(process, Pid), Pid}} end end,
    lists:filtermap(F, ResultList).

%% --------------------
%% @private
%% Проверяем упавший процесс. Если есть в списке мониторинговых, то удаляем из списка.
%% Если список оказался пустым, то запускаем таймер.
%% --------------------
-spec check_monitor_process(Pid::pid(), Ref::undefined | reference(), #rereg_state{}) -> #rereg_state{}.
%% --------------------
check_monitor_process(Pid, Ref, #rereg_state{monitoring=Monitoring}=State) ->
    MonitoringResidue = case Ref of
        undefined ->
            case lists:keytake(Pid,2,Monitoring) of
                false -> Monitoring;
                {value, _, Monitoring2} -> Monitoring2
            end;
        _ -> case lists:member({Ref, Pid}, Monitoring) of
                 true -> lists:delete({Ref, Pid}, Monitoring);
                 false -> Monitoring
             end
    end,
    UpdateState = State#rereg_state{monitoring=MonitoringResidue},
    case MonitoringResidue of
        [] -> start_timer(UpdateState);
        _ -> UpdateState
    end.

%% @private
%% every 15 minutes restart
refresh_domains(#rereg_state{counter=Cnt}=State) when Cnt rem 5 > 0 -> State;
refresh_domains(#rereg_state{domains_cache=DTree}=State) -> % every 15 sec
    NowTS = ?EU:timestamp(),
    Domains = ?CFG:get_domains_in_site(),
    Domains1 = lists:zip(lists:sort(Domains),lists:map(fun(_) -> NowTS end, lists:seq(1,length(Domains)))),
    DTree1 = lists:ukeymerge(1,Domains1,DTree),
    DTree2 = lists:filter(fun({_,TS}) -> TS + 60000 > NowTS end,DTree1),
    State#rereg_state{domains_cache=DTree2}.
