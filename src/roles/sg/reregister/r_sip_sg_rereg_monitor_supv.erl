%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Pavel Abramov, Peter Bukashin <tbotc@yandex.ru>
%%% @date 19.12.2016
%%% @doc 1) Обработка запросов от супервизора гейта (старт, стоп).
%%%         2) Подготовка функции запуска и стопа r_sip_sg_rereg_monitor_srv.
%%%         3) Запуск ген сервера r_env_sync_strategy.
%%%         4) Мониторинг ген сервера r_env_sync_strategy.
%%%         5) Мониторинг ген сервера r_sip_sg_rereg_monitor_srv.
%%% @todo

-module('r_sip_sg_rereg_monitor_supv').
-author(['Pavel Abramov','Peter Bukashin <tbotc@yandex.ru>']).

-behaviour(supervisor).

-export([init/1]).
-export([start_link/0, start_monitor/1, stop_monitor/1, get_node_sg_on_site/0]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip_sg_rereg_monitor.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%----------------------------------------------------------------------
-spec start_link() -> {ok, Pid::pid()} | {error, Reason::binary()}.

start_link() ->
    ?OUT("~p -- start_link", [?MODULE]),
    case supervisor:start_link({local, ?MODULE}, ?MODULE, []) of
        {ok, _SupRef}=Result -> Result;
        _ -> {error, <<"supervisor not started">>}
    end.

%----------------------------------------------------------------------
-spec start_monitor(SupRef::pid()) -> {ok, Pid:: pid()} | {error, Reason::binary()}.

start_monitor(SupRef) ->
    ?OUT("~p -- start_monitor", [?MODULE]),
    Opts = #{},
    ChildSpec = {?REREGSRV, {?REREGSRV, start_monitor, [Opts]}, permanent, 1000, worker, [?REREGSRV]},
    case supervisor:check_childspecs([ChildSpec]) of
        {error, _} -> {error, <<"wrong childspecs">>};
        _ ->
            case supervisor:start_child(SupRef, ChildSpec) of
                {error,_} -> {error, <<"start child fail">>};
                {ok, undefined} -> {error, <<"child undefined">>};
                {ok, undefined, _} -> {error, <<"child undefined">>};
                {ok, Pid1} -> {ok, Pid1};
                {ok, Pid2, _} -> {ok, Pid2}
            end
    end.

%----------------------------------------------------------------------
-spec stop_monitor(SupRef:: pid()) -> ok | {error, Reason:: binary()}.

stop_monitor(SupRef) ->
    ?OUT("~p -- stop_monitor", [?MODULE]),
    case supervisor:terminate_child(SupRef, ?REREGSRV) of
        {error, 'not_found'} -> {error, <<"terminate child not_found">>};
        {error, 'simple_one_for_one'} -> {error, <<"terminate child simple_one_for_one">>};
        _ ->
            case supervisor:delete_child(SupRef, ?REREGSRV) of
                {error, _} -> {error, <<"delete child fail">>};
                _ -> ok
            end
    end.

%----------------------------------------------------------------------
-spec get_node_sg_on_site() -> [Nodes::{Site2::binary(),Node::node(),SrvIdx::pos_integer(),Addr::binary()}].

get_node_sg_on_site() ->
    [Node || {_,Node,_,_} <- ?ENVCFG:get_nodes_containing_role_site('sg')].

%% ====================================================================
%% Behavioural functions
%% ====================================================================

init([]) ->
    SupRef = self(),
    StartLockOpts = [{fun_start, fun() -> start_monitor(SupRef) end},
                     {fun_stop, fun() -> stop_monitor(SupRef) end},
                     {fun_nodes, fun() -> get_node_sg_on_site() end},
                     {regname, 'REREG_LOCKER'}],
    ChildSpec = {?SYNC, {?SYNC, start_link, [StartLockOpts]}, permanent, 1000, worker, [?SYNC]},
    ?LOG(" GATE-REREG supv inited"),
    {ok,{{one_for_all, 10, 2}, [ChildSpec]}}.

%% ====================================================================
%% Internal functions
%% ====================================================================


