%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Pavel Abramov, Peter Bukashin <tbotc@yandex.ru>
%%% @date 21.12.2016
%%% @doc 1) Обработка запросa от r_r_sip_sg_rereg_monitor_perf (start)
%%%         2) Формирование сообщения NOTIFY
%%%         3) Передача подготовленного сообщения в r_sip_sg_rereg_strategy_sn.
%%% @todo

-module(r_sip_sg_rereg_monitor_work).
-author(['Pavel Abramov','Peter Bukashin <tbotc@yandex.ru>']).

-export([start_worker/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip_sg_rereg_monitor.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

-spec start_worker(Task::map()) -> ok.

start_worker(Task) ->
    case maps:get(?TaskRegInfo, Task, undefined) of
        undefined -> ok;
        RegInfo ->
            case prepare_notify(RegInfo) of
                {error, _} -> ok;
                {ok, Notify} ->    send_notify(Notify)
            end
    end,
    ok.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
%% Пример RegInfo:
%% [{{sip,<<"sip1">>,<<"test.rootdomain.ru">>},
%%   {reg_contact,
%%    {sip,udp,<<"sip1">>,<<"192.168.0.173">>,5062},
%%    {uri,sip,<<"sip1">>,<<>>,<<"192.168.0.173">>,5062,<<>>,[],[],[{<<"expires">>,<<"60">>}],[],<<>>},
%%    1482305500971000,1482305560,1.0,<<"404131138@192.168.0.173">>,50091,
%%    {nkport,{nksip,aa0ugd6},nksip_protocol,tcp,{192,168,0,73},5090,{192,168,0,73},51874,{0,0,0,0},5090,<7991.740.0>,#Port<7991.7009>,#{}},
%%    [{uri,sip,<<"r_Q22sJHX">>,<<>>,<<"192.168.0.73">>,5060,<<>>,[{<<"transport">>,<<"tcp">>},<<"lr">>],[],[],[],<<>>}],
%%     []}}]
-spec prepare_notify(RegInfo) -> {ok, NotifyParam} | {error, Reason} when
    RegInfo :: tuple(),
    NotifyParam :: {UriReq, Opt},
    UriReq :: #uri{},
    Opt :: list(),
    Reason :: binary().
prepare_notify({{_S,U,_D}=AOR, RegContact}=_RegInfo) ->
    {_,_,Uri,_,_,_,CallId,_,_,[UriFromPath|Path],_} = RegContact,

    UriTo = ?U:make_uri(AOR),
    UriReq = ?U:clear_uri(Uri),
    Contact = ?U:clear_uri(UriFromPath#uri{user=U}), % @localcontact! (by: UriTo's route? | fix: RegContact' top path's domain !)
    UriFrom = Contact,

    Opt = [{from,UriFrom},
           {to,UriTo},
           {call_id,CallId},
           {cseq_num, 1},
           {contact,Contact},
           {event, <<"reload-account">>},
           server,
           {subscription_state, {terminated,deactivated}}],

    Headers = Uri#uri.headers,
    UriReq2 = case Path of
        [] -> UriReq;
        _ -> UriReq#uri{headers=nksip_headers:update(Headers, [{multi, <<"route">>, Path}])}
    end,
    {ok, {UriReq2, Opt}}.

%----------------------------------------------------------------------
% @private
-spec send_notify(NotifyParam) -> ok | {error, Reason} when
    NotifyParam :: {UriReq, Opt},
    UriReq :: #uri{},
    Opt :: list(),
    Reason :: binary().
send_notify(NotifyParam) -> ?REREGSENDER:send_notify(NotifyParam).

