%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Pavel Abramov, Peter Bukashin <tbotc@yandex.ru>
%%% @date 23.12.2016
%%% @doc Обработка опции роли.

-module(r_sip_sg_rereg_opts).
-author(['Pavel Abramov','Peter Bukashin <tbotc@yandex.ru>']).

-export([check_rereg_in_opts/1, start_rereg/0, stop_rereg/0, update_rereg/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip_sg_rereg_monitor.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ---
% @private
% Check reregister in options for sg
-spec check_rereg_in_opts(Opts::list()) -> Result::boolean().

check_rereg_in_opts(Opts) ->
    case lists:keyfind(<<"reregister">>,1,Opts) of
        false -> false;
        {_,B} -> ?EU:to_bool(B)
    end.

%% ---
% @private
-spec start_rereg() -> ok.

start_rereg() ->
    ?SIPSTORE:store_u('reregister', true),
    ?SG_SUPV:start_child({?SG_REREGMON_SUPV, {?SG_REREGMON_SUPV, start_link, []}, permanent, 1000, supervisor, [?SG_REREGMON_SUPV]}),
    ok.

%% ---
% @private
-spec stop_rereg() -> ok.

stop_rereg() ->
    ?SIPSTORE:store_u('reregister', false),
    ?SG_SUPV:terminate_child(?SG_REREGMON_SUPV),
    ?SG_SUPV:delete_child(?SG_REREGMON_SUPV),
    ok.

%% ---
% @private
-spec update_rereg(Opts::list()) -> ok.

update_rereg(Opts) ->
    InStore = case ?SIPSTORE:find_u('reregister') of
                  {_,true} -> true;
                  _ -> false
              end,
    case {InStore, check_rereg_in_opts(Opts)} of
        {false, true} -> start_rereg();
        {true, false} -> stop_rereg();
        {Result, Result} -> ok
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================


