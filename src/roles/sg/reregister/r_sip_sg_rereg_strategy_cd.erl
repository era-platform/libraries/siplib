%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Pavel Abramov, Peter Bukashin <tbotc@yandex.ru>
%%% @date 19.12.2016
%%% @doc Check Down gates module
%%%      1) Запросы к конфигурации.
%%%      2) Анализ списков гейтов (всего, активных, в кэше, по счетчику падений).
%%%      3) Фильтрация гейтов (по сайту, по домену)
%%%      4) Запросы к registrar по списку гейтов.
%%%      5) Фильтрация регистрационной информации (по юзерагенту, по нату)
%%% @todo

-module(r_sip_sg_rereg_strategy_cd).
-author(['Pavel Abramov','Peter Bukashin <tbotc@yandex.ru>']).

-export([check_down_sg_nodes/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip_sg_rereg_monitor.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ----------------------
-spec check_down_sg_nodes(State::#rereg_state{}) -> {ok, [RegInfo], UpdateState::#rereg_state{}}
    when RegInfo :: tuple(). % #reg_contact{} определен в nksip
%% ----------------------
check_down_sg_nodes(#rereg_state{domains_cache=DTree,skip=Skip}=State) ->
    case check_down(State) of
        {ok, [], State1} -> {ok,[],State1#rereg_state{skip=0}};
        {ok, DownGates, UpdateState} ->
            ?LOG("REREG. Found gates down: ~n\t~120tp",[DownGates]),
            case ?ENVGLOBALX:whereis_name(domain_master) of
                undefined -> {ok, [], State}; % no skip counter
                _ ->
                    {Domains0,_} = lists:unzip(DTree),
                    Domains = ?CFG:get_domains_in_site(),
                    case Domains0--Domains of
                        [_|_]=NotFoundDomains when Skip < 40 -> % 120 seconds
                            ?LOG("REREG. Skip while domains not found (cnt=~120tp): ~n\t~120tp",[Skip,?EU:join_binary(NotFoundDomains,<<", ">>)]),
                            {ok, [], State#rereg_state{skip=Skip+1}};
                        _ ->
                            GeneralDomain = ?EU:to_binary(?CFG:get_general_domain()),
                            FilterDomains = lists:delete(GeneralDomain,Domains),
                            Sites = get_sites_on_domains(FilterDomains),
                            DomainGates = filter_gates_domains(DownGates, Sites),
                            {ok, RegInfoList} = get_reginfo(DomainGates),
                            {ok,filter_regs_nat(filter_regs_user(RegInfoList)),UpdateState#rereg_state{skip=0}}
                    end end end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ----------------------
%% @private
%% Получение всех гейтов сайта из конфига (адрес+порт).
%% ----------------------
-spec get_all_gates() -> {ok, AllGates}
    when AllGates :: [Gate],
         Gate :: {Site::binary(), Node::node(), Address::binary(), Port::non_neg_integer()}.
%% ----------------------
get_all_gates() -> {ok, ?CFG:get_static_sg_nodes_addrs()}.

%% ----------------------
%% @private
%% Возвращает ноды с ролями активных гейтов с сайта.
%% ----------------------
-spec get_active_gates() -> {ok, ActiveGates}
    when ActiveGates :: [Gate],
         Gate :: {Site::binary(), Node::node(), undefined, undefined}.
%% ----------------------
get_active_gates() -> {ok, ?ENVCFG:get_nodes_containing_role_site('sg')}.

%% ----------------------
%% @private
%% Поиск упавших гейтов.
%% ----------------------
-spec check_down(State::#rereg_state{}) -> {ok, DownGates, UpdateState::#rereg_state{}}
    when DownGates :: [Gate],
         Gate :: {Site::binary(), Address::binary(), Port::non_neg_integer()}.
%% ----------------------
check_down(#rereg_state{cache=undefined}=State) ->
    {ok, AllGates} = get_all_gates(),
    {ok, ActiveGates0} = get_active_gates(),
    ActiveGates = lists:filter(fun({_,Node,_,_}) -> lists:keymember(Node,2,ActiveGates0) end, AllGates),
    Cache1 = #rereg_cache{all_gates=AllGates, active_gates=ActiveGates},
    F = fun({Site, Node, Address, Port}) -> case lists:keymember(Node, 2, ActiveGates0) of true -> false; _ -> {true, {Site, Address, Port, 1}} end end,
    Cache2 = Cache1#rereg_cache{zombie_gates=lists:filtermap(F, AllGates)},
    {ok, [], State#rereg_state{cache=Cache2}};
check_down(#rereg_state{cache=#rereg_cache{all_gates=AllCache,active_gates=ActiveCache}=_Cache}=State) ->
    {ok, AllGates} = get_all_gates(),
    {ok, ActiveGates0} = get_active_gates(),
    ActiveGates = lists:filter(fun({_,Node,_,_}) -> lists:keymember(Node,2,ActiveGates0) end, AllGates),
    GChanges = get_changes(AllCache, ActiveCache, AllGates, ActiveGates),
    UpdateState = lists:foldl(
                fun({G,'a_na'}, AccState) -> add_to_zombie(G, AccState);
                   ({G,'a_no'}, AccState) -> add_to_zombie(G, AccState);
                   ({G,'na_a'}, AccState) -> remove_from_zombie(G, AccState);
                   ({G,'no_a'}, AccState) -> remove_from_zombie(G, AccState);
                   (_, AccState) -> AccState
                end, State, GChanges),
    UpdateCache = UpdateState#rereg_state.cache,
    update_zombie_counter(UpdateState#rereg_state{cache=UpdateCache#rereg_cache{all_gates=AllGates,active_gates=ActiveGates}}).

%% ----------------------
%% @private
%% Анализ списков на предмет изменений.
%% ----------------------
-spec get_changes(AllCache,ActiveCache,All,Active) -> Result when
    AllCache :: [Gate],     % Всего в кеше
    ActiveCache :: [Gate],  % Активных в кэше
    All :: [Gate],          % Всего сейчас
    Active :: [Gate],       % Активных сейчас
    Result :: [{Gate,Type}],
    Gate :: {Site::binary(), Address::binary(), Port::non_neg_integer()},
    Type :: 'na_na' |    % был неактивный, есть неактивный
            'na_a'  |    % был неактивный, есть активный
            'na_no' |    % был неактивный, нет
            'a_na'  |    % был активный, есть неактивный
            'a_a'   |    % был активный, есть активный
            'a_no'  |    % был активный, нет
            'no_na' |    % не было, есть неактивный
            'no_a'.      % не было, есть активный
%% ----------------------
get_changes(AllCache,ActiveCache,All,Active) ->
    {C1,C2,N1,N2} = {lists:usort(AllCache),lists:usort(ActiveCache),lists:usort(All),lists:usort(Active)},
    % subsets
    C3 = ordsets:subtract(C1,C2),  % deactivated in cache
    N3 = ordsets:subtract(N1,N2),  % deactivated now
    New = ordsets:subtract(N1,C1), % added
    Del = ordsets:subtract(C1,N1), % deleted
    Act = ordsets:subtract(N2,C2), % activated (both added and cached)
    Dea = ordsets:subtract(C2,N2), % deactivated (both deleted and deaactivated)
    % lists
    A1 = ordsets:intersection(N3,C3),  % 'na_na'
    A2 = ordsets:intersection(N2,C3),  % 'na_a'
    A4 = ordsets:intersection(N3,C2),  % 'a_na'
    A5 = ordsets:intersection(N2,C2),  % 'a_a'
    A6 = ordsets:intersection(Del,Dea), % 'a_no'
    A3 = ordsets:subtract(Del,A6),  % 'na_no'
    A8 = ordsets:intersection(New,Act), % 'no_a'
    A7 = ordsets:subtract(New,A8),  % 'no_na'
    % map lists to make [{Gate,Type}]
    lists:foldl(fun({L,T}, Acc) ->
        lists:map(fun(I) -> {I,T} end, L) ++ Acc end, [],
                  [{A1,'na_na'},
                   {A2,'na_a'},
                   {A3,'na_no'},
                   {A4,'a_na'},
                   {A5,'a_a'},
                   {A6,'a_no'},
                   {A7,'no_na'},
                   {A8,'no_a'}]).

%% ----------------------
%% @private
%% Фильтрация гейтов по доменам.
%% ----------------------
-spec filter_gates_domains(Gates, [{Site::binary(), [Domain::binary()]}]) -> UpdateGates
    when Gates :: [Gate],
         Gate :: {Site::binary(), Address::binary(), Port::non_neg_integer()},
         UpdateGates :: [{Domain::binary(), [Gate]}].
%% ----------------------
filter_gates_domains(Gates, Sites) ->
    F = fun({S,A,P}, Acc1) ->
            case lists:keysearch(S, 1, Sites) of
                false -> Acc1;
                {value, {_, Domains}} ->
                    F1 = fun(Domain, Acc3) ->
                                 case lists:keytake(Domain, 1, Acc3) of
                                     {value, {_, Gates1}, Acc2} -> lists:append([{Domain, lists:append([{A,P}],Gates1)}],Acc2);
                                     false -> lists:append([{Domain, [{A,P}]}],Acc3)
                                 end end,
                    lists:foldl(F1,Acc1,Domains)
            end end,
    lists:foldl(F, [], Gates).

%% ----------------------
%% @private
%% Запрос к регистрару на получение информации по списку гейтов.
%% ----------------------
-spec get_reginfo(DomainGates) -> {ok, [RegInfo]} | {error, Reason::binary()}
    when DomainGates :: [{Domain::binary(), [Gate]}],
         Gate :: {Site::binary(), Address::binary(), Port::non_neg_integer()},
         RegInfo :: tuple(). % #reg_contact{} определен в nksip
%% ----------------------
get_reginfo(DomainGates) ->
    {ok, lists:foldl(fun({Domain, Paths}, Acc) -> lists:append(?REGISTRAR:get_by_path(Domain,Paths), Acc) end, [], DomainGates)}.

%% ----------------------
%% @private
%% Фильтрация полученной информации из регистрата по юзерам.
%% ----------------------
-spec filter_regs_user(RegInfoList) -> UpdateRegInfoList
    when RegInfoList :: [RegInfo],
         UpdateRegInfoList :: [RegInfo],
         RegInfo :: tuple(). % #reg_contact{} определен в nksip
%% ----------------------
filter_regs_user(RegInfoList) -> RegInfoList.

%% ----------------------
%% @private
%% Фильтрация полученной информации из регистрата по нату.
%% ----------------------
-spec filter_regs_nat(RegInfoList) -> UpdateRegInfoList
    when RegInfoList :: [RegInfo],
         UpdateRegInfoList :: [RegInfo],
         RegInfo :: tuple(). % #reg_contact{} определен в nksip
%% ----------------------
filter_regs_nat(RegInfoList) -> RegInfoList.

%% ----------------------
%% @private
%% Увеличиваем счетчик недоступных гейтов.
%% Формируем список гейтов с счетчиком 5 ((5-1)*3 = 12 секунд).
%% ----------------------
-spec update_zombie_counter(State::#rereg_state{}) -> {ok, DownGates, UpdateState::#rereg_state{}}
    when DownGates :: [Gate],
         Gate :: {Site::binary(), Address::binary(), Port::non_neg_integer()}.
%% ----------------------
update_zombie_counter(#rereg_state{cache=#rereg_cache{zombie_gates=Zombie}=Cache}=State) ->
    F = fun({S,A,P,C}, {Z,D}) -> case C+1 of 6 -> {Z, lists:append([{S,A,P}], D)}; UpC -> {lists:append([{S,A,P,UpC}],Z), D} end end,
    {UpdateZombie, Down} = lists:foldl(F, {[],[]}, Zombie),
    {ok, Down, State#rereg_state{cache=Cache#rereg_cache{zombie_gates=UpdateZombie}}}.

%% ----------------------
%% @private
%% ----------------------
-spec remove_from_zombie(ActiveGate, State:: #rereg_state{}) -> UpdateState:: #rereg_state{}
    when ActiveGate :: {Site::binary(), Address::binary(), Port::non_neg_integer()}.
%% ----------------------
remove_from_zombie({_Site, _Address, _Port}=Active, #rereg_state{cache=#rereg_cache{zombie_gates=Zombie}=Cache}=State) ->
    case lists:member(Active, Zombie) of
        true ->
            UpdateZombie = lists:delete(Active, Zombie),
            State#rereg_state{cache=Cache#rereg_cache{zombie_gates=UpdateZombie}};
        _ -> State
    end.

%% ----------------------
%% @private
%% ----------------------
-spec add_to_zombie(InactiveGate, State::#rereg_state{}) -> UpdateState::#rereg_state{}
    when InactiveGate :: {Site::binary(), Address::binary(), Port::non_neg_integer()}.
%% ----------------------
add_to_zombie({Site, Address, Port}=InactiveGate, #rereg_state{cache=#rereg_cache{zombie_gates=Zombie}=Cache}=State) ->
    case lists:member(InactiveGate, Zombie) of
        true -> State;
        _ ->
            UpdateZombie = lists:append([{Site, Address, Port, 0}], Zombie),
            State#rereg_state{cache=Cache#rereg_cache{zombie_gates=UpdateZombie}}
    end.

%% ----------------------
%% @private
%% Получение списка сайтов по доменам.
%% ----------------------
-spec get_sites_on_domains([Domain::binary()]) -> [{Site::binary(),Domain::binary()}].
%% ----------------------
get_sites_on_domains(Domains) ->
    F = fun(Domain, Acc1) ->
                Sites = ?CFG:get_domain_sites(Domain),
                F2 = fun(Site, Acc2) -> case lists:keytake(Site, 1, Acc2) of
                                      false -> lists:append([{Site, [Domain]}],Acc2);
                                      {value, {_, Domains2}, TupleList2} ->
                                          Domains3 = lists:append([Domain],Domains2),
                                          lists:append([{Site, Domains3}],TupleList2)
                                  end
                     end,
                lists:foldl(F2, Acc1, Sites) end,
    lists:foldl(F,[], Domains).

