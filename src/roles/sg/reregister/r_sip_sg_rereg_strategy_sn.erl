%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Pavel Abramov, Peter Bukashin <tbotc@yandex.ru>
%%% @date 21.12.2016
%%% @doc SendNotify module
%%%      1) Обработка запросa от r_sip_sg_rereg_monitor_work (send_notify)
%%%      2) Дополнительная настройка NOTIFY
%%%      3) Работа с nksip.
%%% @todo

-module(r_sip_sg_rereg_strategy_sn).
-author(['Pavel Abramov','Peter Bukashin <tbotc@yandex.ru>']).

-export([send_notify/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip_sg_rereg_monitor.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

-spec send_notify(NotifyParam) -> ok | {error, Reason} when
    NotifyParam :: {UriReq, Opt},
    UriReq :: #uri{},
    Opt :: list(),
    Reason :: binary().
send_notify(NotifyParam) -> send(update_notify(NotifyParam)).

%% ====================================================================
%% Internal functions
%% ====================================================================

% @private
-spec update_notify(NotifyParam) -> UpdateNotifyParam when
    NotifyParam :: {UriReq, Opt},
    UriReq :: #uri{},
    Opt :: list(),
    UpdateNotifyParam :: {UriReq, Opt}.
update_notify(NotifyParam) -> NotifyParam.

%----------------------------------------------------------------------
% @private
-spec send(NotifyParam) -> ok | {error, Reason} when
    NotifyParam :: {UriReq, Opt},
    UriReq :: #uri{},
    Opt :: list(),
    Reason :: binary().
send({UriReq, Opt}=_NotifyParam) ->
    case nksip_uac:notify(?SIPAPP, UriReq, Opt) of
        {error, _} -> {error, <<"nksip notify fail">>};
        _ -> ok
    end.


