%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 10.07.2019
%%% @doc RP-1470
%%%      General facade of MRCP-FSM.
%%%      Manages SIP dialog flow to MRCP server
%%%      States: 'dialing', 'active'
%%%      Based on IVR (r_sip_ivr_fsm)

-module(r_sip_mrcp_fsm).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([init/1]).
-export([handle_event/4]).
-export([dialing/2,
         active/2,
         terminating/2,
         handle_event/3,
         handle_sync_event/4,
         handle_info/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_mrcp.hrl").
-include("../include/r_sip_ivr.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---------------------
%% initiates MRCP-FSM
%% ---------------------
init(Opts) ->
    [App,Domain,DlgId,OwnerPid,ParentCallId,ScriptMachine] = ?EU:maps_get([app,domain,dlgid,ownerpid,parent_callid,scriptmachine],Opts),
    Map=#{fromdomain => <<"test.rootdomain.ru">>,
          to => #uri{scheme=sip,user= <<"sip3">>,domain= <<"test.rootdomain.ru">>},
          routeuri => #uri{scheme=sip,user= <<"sip3">>,domain= <<"192.168.0.146">>,port=5065,opts=[{<<"transport">>,<<"tcp">>}]},
          callerid => <<"123456">>,
          callername => <<"ABC">>,
          headers => []},
    StateData = #state_mrcp{opts=Opts,
                            dlgid=DlgId,
                            app=App,
                            domain=Domain,
                            ownerpid=OwnerPid, % if parent ivr was started by any owner
                            scriptmachine=ScriptMachine, % map to scriptmachine meta
                            parent_callid=ParentCallId,
                            starttime=os:timestamp(),
                            map=Map},
    d(StateData, 'init', "srv inited"),
    self() ! {mrcp,{start_dialing,DlgId}},
    %
    erlang:send_after(10000,self(),{mrcp,stop}),
    FunStarted = maps:get(fun_started,Opts,fun() -> ok end),
    FunStarted(),
    {ok,?MRCP_FSM_DIALING_STATE,StateData#state_mrcp{opts=Opts}}.

%% ---------------------
%% handling filtered MRCP's events
%% ---------------------
handle_event(EventType,EventContent,MrcpStateName,MrcpStateData) ->
    case catch ?FSMT:handle_event(?MODULE,EventType,EventContent,MrcpStateName,MrcpStateData) of
        {'EXIT',_}=E -> ?LOG("Error: ~120p", [E]), stop;
        T -> T
    end.

%% ====================================================================
%% Callback functions
%% ====================================================================

%% -------------------
dialing({stop_external,Reason}, StateData) ->
    ?MRCP_FSM_DIALING:stop_external(Reason,StateData);

dialing({uac_response,Args}, StateData) ->
    ?MRCP_FSM_DIALING:uac_response(Args, StateData);

dialing({sip_bye, Args}=_Event, StateData) ->
    ?MRCP_FSM_DIALING:sip_bye(Args, StateData);

dialing({call_terminate, Args}=_Event, StateData) ->
    ?MRCP_FSM_DIALING:call_terminate(Args, StateData);

dialing(_Event,_StateData) -> continue.

%% -------------------
active({sip_bye, Args}=_Event, StateData) ->
    ?MRCP_FSM_ACTIVE:sip_bye(Args, StateData);

active({uac_response,Args}, StateData) ->
    ?MRCP_FSM_ACTIVE:uac_response(Args, StateData);

active({uas_dialog_response,Args}, StateData) ->
    ?MRCP_FSM_ACTIVE:uas_dialog_response(Args, StateData);

active({call_terminate, Args}=_Event, StateData) ->
    ?MRCP_FSM_ACTIVE:call_terminate(Args, StateData);

active({stop_external, Reason}, StateData) ->
    ?MRCP_FSM_ACTIVE:stop_external(Reason, StateData);

active(_Event,_StateData) -> continue.

%% -------------------
terminating(_Event, _StateData) -> continue.

%% -------------------
handle_event({stop_forcely}, _StateName, StateData) ->
    {stop, normal, StateData};

handle_event(_Event, _StateName, _StateData) -> continue.

%% -------------------
handle_sync_event(_Event, _From, _StateName, _StateData) -> continue.

%% -------------------
handle_info({start_dialing, DlgId}, ?MRCP_FSM_DIALING_STATE, #state_mrcp{dlgid=DlgId}=StateData) ->
    case ?MRCP_FSM_DIALING:start(StateData) of
        {next_state,_,_}=Res -> Res;
        {ok,StateData1} -> {next_state, ?MRCP_FSM_DIALING_STATE, StateData1};
        {{error,_},StateData1} -> terminate(StateData1)
    end;

handle_info(stop, _StateName, StateData) ->
    terminate(StateData);

%%
handle_info({mrcp_fork_timeout,_Args}=Event, ?MRCP_FSM_DIALING_STATE, StateData) ->
    ?MRCP_FSM_DIALING:timeout(Event, StateData);
handle_info({mrcp_fork_timeout,_}, StateName, StateData) ->
    {next_state, StateName, StateData};

%% down of owner for dialing by mode
handle_info({'DOWN', _Ref, process, Pid, _Result}, ?MRCP_FSM_DIALING_STATE, #state_mrcp{ownerpid=Pid}=StateData) ->
    ?MRCP_FSM_DIALING:stop_external(<<"Owner_down">>,StateData);

%% down of owner (proc is linked, and auto down instead of message)
handle_info({'DOWN', _Ref, process, Pid, _Result}, ?MRCP_FSM_ACTIVE_STATE, #state_mrcp{ownerpid=Pid}=StateData) ->
    ?MRCP_FSM_ACTIVE:stop_external(<<"Owner_down">>,StateData);

%% down of scriptmachine
handle_info({'DOWN', _Ref, process, Pid, _Result}, ?MRCP_FSM_DIALING_STATE, #state_mrcp{scriptmachine=#{pid:=Pid}}=StateData) ->
    ?MRCP_FSM_DIALING:stop_external(<<"Owner_down">>,StateData);

%% down of owner (proc is linked, and auto down instead of message)
handle_info({'DOWN', _Ref, process, Pid, _Result}, ?MRCP_FSM_ACTIVE_STATE, #state_mrcp{scriptmachine=#{pid:=Pid}}=StateData) ->
    ?MRCP_FSM_ACTIVE:stop_external(<<"Owner_down">>,StateData);

%% other down
handle_info({'DOWN', _Ref, process, _Pid, _Result}, StateName, #state_mrcp{}=StateData) ->
    {next_state, StateName, StateData};

handle_info(_Event, _StateName, _StateData) -> continue.

%% ====================================================================
%% Internal functions
%% ====================================================================

terminate(#state_mrcp{opts=Opts}=StateData) ->
    Fun = maps:get(fun_error,Opts,fun(_) -> ok end),
    Fun({error,<<"Test error">>}),
    ?MRCP_FSM_UTILS:return_terminating(?MRCP_FSM_UTILS:do_finalize(StateData)).

%% =====================================================================

%% @private
%% d(State, Text) -> d(State, Text, []).
d(StateData, StateName, Text) -> d(StateData,StateName,Text,[]).
d(StateData, _StateName, Fmt, Args) ->
    #state_mrcp{state=State,dlgid=DlgId}=StateData,
    ?LOG("MRCP fsm ~p '~p':" ++ Fmt, [DlgId,State] ++ Args).