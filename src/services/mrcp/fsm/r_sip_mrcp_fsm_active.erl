%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 10.07.2019
%%% @doc RP-1470
%%%      Implements 'active' state of MRCP-FSM.
%%%      Initializes MRCP-TCP connection and performs MRCP-proto exchange by internal FSM
%%%      States of internal FSM: connecting, requesting, inprogress, finalizing
%%%      Based on IVR (r_sip_ivr_fsm_active)

-module(r_sip_mrcp_fsm_active).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([stop_external/2,
         sip_bye/2,
         uas_dialog_response/2,
         call_terminate/2,
         fork_timeout/2,
         uac_response/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_mrcp.hrl").
-include("../include/r_sip_ivr.hrl").

-define(CURRSTATE, ?MRCP_FSM_ACTIVE_STATE).

%% ====================================================================
%% Public functions
%% ====================================================================

%% --------------------
stop_external(Reason, State) ->
    d(State, "stop_external(~p)", [Reason]),
    % @todo bye/4xx to a, cancel forks
    #state_mrcp{a=Side}=State,
    State1 = case State of
                 #state_mrcp{a=#side{}=Side} ->
                     State#state_mrcp{a=?MRCP_FSM_UTILS:send_bye(Side, State)};
                 _ -> State
             end,
    StopReason = ?MRCP_FSM_UTILS:reason_external(Reason),
    ?MRCP_FSM_UTILS:return_terminating(?MRCP_FSM_UTILS:do_finalize(State1#state_mrcp{stopreason=StopReason})).

%% --------------------
sip_bye([CallId, Req], State) ->
    d(State, "sip_bye, CallId=~120p", [CallId]),
    State1 = ?MRCP_FSM_UTILS:send_response(Req, ok, State),
    % #300
    case State of
        #state_mrcp{a=#side{callid=CallId}=Side} ->
            do_stop(CallId, State1#state_mrcp{a=Side#side{state=bye}});
        _ ->
            % from store
            ?DLG_STORE:unlink_dlg(CallId),
            {next_state, ?CURRSTATE, State}
    end.

%% --------------------
uas_dialog_response([_Resp]=P, State) ->
    #sipmsg{class={resp,SipCode,_},
            cseq={CSeq,Method},
            call_id=CallId}=_Resp,
    d(State, "uas_dialog_response ~p:~p -> ~p, CallId=~120p", [Method,CSeq,SipCode,CallId]),
    do_on_uas_dialog_response(P, State).

%% --------------------
call_terminate([Reason, Call], State) ->
    #call{call_id=CallId}=Call,
    case State of
        #state_mrcp{acallid=CallId} ->
            d(State, "call_terminate, CallId=~120p, Reason=~120p", [CallId, Reason]),
            do_stop(CallId, State);
        _ -> {next_state, ?CURRSTATE, State}
    end.

% ============================

%% --------------------
fork_timeout([CallId, BLTag]=P, State) ->
    d(State, "fork_timeout BLTag=~120p, CallId=~120p", [BLTag, CallId]),
    do_on_fork_timeout(P, State).

%% --------------------
uac_response([_Resp]=Args, State) ->
    #sipmsg{class={resp,SipCode,SipReason},
            cseq={CSeq,Method},
            call_id=CallId}=_Resp,
    d(State, "uac_response ~p:~p -> ~p ~p, CallId=~120p", [Method,CSeq,SipCode,SipReason,CallId]),
    do_on_uac_response(Args, State).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% --------------------
%% stops mrcp-sip session
%% --------------------
do_stop(_CallId, State) ->
    ?MRCP_FSM_UTILS:return_terminating(?MRCP_FSM_UTILS:do_finalize(State)).

%% ====================================================================

%% --------------------
%% UAC fork timeout
%% --------------------
do_on_fork_timeout([CallId, _LTag]=_P, State) ->
    #state_mrcp{forks=Forks}=State,
    case lists:keyfind(CallId,1,Forks) of
        false -> {next_state, ?CURRSTATE, State};
        {_,#side_fork{}=_Side} -> {next_state, ?CURRSTATE, State}
    end.

%% -----
%% UAC response handlers
%%
do_on_uac_response([#sipmsg{cseq={_,Method}}=Resp]=P, State) ->
    #sipmsg{class={resp,SipCode,_}, call_id=CallId}=Resp,
    d(State, "uac_response(~p) ~p from ~p", [Method, SipCode, CallId]),
    {ok,State1} = ?ACTIVE_SM:uac_response(Resp, State),
    do_on_uac_response_1(P, State1).

%
do_on_uac_response_1([#sipmsg{cseq={_,'INVITE'}, call_id=CallId}=Resp]=_P, State) ->
    #sipmsg{class={resp,SipCode,_}}=Resp,
    #state_mrcp{forks=Forks}=State,
    d(State, "uac_response('INVITE') ~p from Z: ~p", [SipCode, CallId]),
    case lists:keyfind(CallId,1,Forks) of
        false -> {next_state, ?CURRSTATE, State};
        {_,#side_fork{}=_Side} -> {next_state, ?CURRSTATE, State}
    end;
do_on_uac_response_1(_, State) ->
    {next_state, ?CURRSTATE, State}.

%% ====================================================================

%% ============================
%% UAS response, dialog ready
%% ============================

do_on_uas_dialog_response([#sipmsg{class={resp,SipCode,_}, cseq={_,'INVITE'}}=Resp], State)
  when SipCode >= 200, SipCode < 300 ->
    {ok,XDlgHandle} = nksip_dialog:get_handle(Resp),
    #state_mrcp{a=Side}=State,
    State1 = State#state_mrcp{a=Side#side{dhandle=XDlgHandle}},
    {next_state, ?CURRSTATE, State1};
do_on_uas_dialog_response(_P, State) ->
    {next_state, ?CURRSTATE, State}.

%% =====================================================================

%% @private ----
%d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    #state_mrcp{dlgid=DlgId}=State,
    ?LOGSIP("MRCP fsm ~p '~p':" ++ Fmt, [DlgId,?CURRSTATE] ++ Args).