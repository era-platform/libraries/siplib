%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 10.07.2019
%%% @doc RP-1470
%%%      Implements 'dialing' state of MRCP-FSM.
%%%      Initializes options and makes INVITE to MRCP-server, wait for Answer
%%%      Based on IVR (r_sip_ivr_fsm_dialing)

-module(r_sip_mrcp_fsm_dialing).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([start/1]).
-export([stop_external/2,
         timeout/2,
         uac_response/2,
         sip_bye/2,
         call_terminate/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_mrcp.hrl").
-include("../include/r_sip_ivr.hrl").

-define(CURRSTATE, ?MRCP_FSM_DIALING_STATE).

%% ====================================================================
%% Public functions
%% ====================================================================

%% --------------------
%% initiates call to mrcp-sip
%% --------------------
start(#state_mrcp{}=State) ->
    ?MRCP_DIALING_CALL:start_outgoing_call(State).

%% --------------------
stop_external(Reason, State) ->
    d(State, "stop_external(~p)", [Reason]),
    #state_mrcp{a=Side,forks=Forks}=State,
    lists:foreach(fun({_,#{}=ForkCall}) -> ?MRCP_DIALING_UTILS:cancel_active_forks([maps:get(fork,ForkCall)], [], State) end, Forks),
    State1 = case Side of
                 #side{} -> State#state_mrcp{a=?MRCP_FSM_UTILS:send_bye(Side, State)};
                 _ -> State
             end,
    StopReason = ?MRCP_FSM_UTILS:reason_external(Reason),
    ?MRCP_FSM_UTILS:return_terminating(?MRCP_FSM_UTILS:do_finalize(State1#state_mrcp{stopreason=StopReason})).

%% --------------------
timeout({mrcp_fork_timeout,_Args}, State) ->
    StopReason = #{type => cancel,
                   reason => <<"invite timeout">>},
    ?MRCP_FSM_UTILS:return_terminating(?MRCP_FSM_UTILS:do_finalize(State#state_mrcp{stopreason=StopReason})).

%% --------------------
uac_response([_Resp]=Args, State) ->
    #sipmsg{class={resp,SipCode,SipReason},
            cseq={CSeq,Method},
            call_id=CallId}=_Resp,
    d(State, "uac_response ~p:~p -> ~p ~p, CallId=~120p", [Method,CSeq,SipCode,SipReason,CallId]),
    do_on_uac_response(Args, State).

%% --------------------
sip_bye([CallId, Req], State) ->
    d(State, "sip_bye, CallId=~120p", [CallId]),
    State1 = ?MRCP_FSM_UTILS:send_response(Req, ok, State),
    %
    #state_mrcp{a=Side}=State,
    do_stop(CallId, State1#state_mrcp{a=Side#side{state=bye}}).

%% --------------------
call_terminate([Reason, Call], State) ->
    #call{call_id=CallId}=Call,
    d(State, "call_terminate, CallId=~120p, Reason=~120p", [CallId, Reason]),
    do_stop(CallId, State).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% --------------------
%% UAC response handlers
%% --------------------
do_on_uac_response([#sipmsg{cseq={_,'INVITE'}, call_id=CallId}=Resp]=P, State) ->
    #sipmsg{class={resp,SipCode,_}}=Resp,
    #state_mrcp{forks=Forks}=State,
    d(State, "uac_response('INVITE') ~p from Z: ~p", [SipCode, CallId]),
    case lists:keyfind(CallId,1,Forks) of
        false ->
            {next_state, ?CURRSTATE, State};
        {_,#side_fork{}=_Side} ->
            {next_state, ?CURRSTATE, State};
        {_,#{type:='call'}=ForkCall} ->
            ?MRCP_DIALING_CALL:uac_response(P, ForkCall, State) %%@@todo check
    end;
%
do_on_uac_response([#sipmsg{cseq={_,'NOTIFY'}}=Resp], State) ->
    #sipmsg{class={resp,SipCode,_}, call_id=CallId}=Resp,
    d(State, "uac_response('NOTIFY') ~p from ~p", [SipCode, CallId]),
    {next_state, ?CURRSTATE, State};
%
do_on_uac_response([#sipmsg{cseq={_,Method}}=_Resp], State) ->
    d(State, "uac_response(~p) skipped", [Method]),
    {next_state, ?CURRSTATE, State}.

%% --------------------
%% stops mrcp-sip session
%% --------------------
do_stop(_CallId, State) ->
    ?MRCP_FSM_UTILS:return_terminating(?MRCP_FSM_UTILS:do_finalize(State)).

%% =====================================================================

%% @private
%% d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    #state_mrcp{dlgid=DlgId}=State,
    ?LOGSIP("MRCP fsm ~p '~p':" ++ Fmt, [DlgId,?CURRSTATE] ++ Args).