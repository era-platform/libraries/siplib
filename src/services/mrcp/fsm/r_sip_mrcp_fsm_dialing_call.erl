%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.07.2019
%%% @doc RP-1470
%%%      Dialing routines of MRCP-FSM
%%%      Based on IVR (r_sip_ivr_fsm_dialing_call)

-module(r_sip_mrcp_fsm_dialing_call).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([start_outgoing_call/1,
         uac_response/3]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_headers.hrl").
-include("../include/r_sip_ivr.hrl").
-include("../include/r_sip_mgc.hrl").
-include("../include/r_sip_mrcp.hrl").

-define(CURRSTATE, ?MRCP_FSM_DIALING_STATE).

-define(MRCP_TIMEOUT_MSG, {mrcp_fork_timeout}).

%% ====================================================================
%% Types
%% ====================================================================

%% ====================================================================
%% API functions
%% ====================================================================

start_outgoing_call(State) ->
    ZCallId = ?MRCP_DIALING_UTILS:build_out_callid(State),
    FRes = fun({error,_}=Result, ResState) -> ?DLG_STORE:unlink_dlg(ZCallId), {Result,ResState};
              (Result, ResState) -> {Result,ResState}
           end,
    do_start_call(State#state_mrcp{acallid=ZCallId}, FRes).

uac_response(P, Refer, State) ->
    do_on_uac_response(P, Refer, State).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -----------------
%% create media context
%% -----------------
do_start_call(#state_mrcp{acallid=ZCallId,map=Map}=State, FRes) ->
    case ?MRCP_MEDIA:media_add_outgoing(ZCallId, State) of
        {error, _}=Err -> FRes(Err, State);
        {ok, State2, ZLSdp} -> do_start_call_1(State2#state_mrcp{map=Map#{lsdp=>ZLSdp}}, FRes)
    end.

%% @private
%% make invite options (inside cluster call)
do_start_call_1(State, FRes) ->
    #state_mrcp{dlgid=DlgId,map=Map,acallid=ZCallId}=State,
    To = maps:get(to,Map),
    RouteUri = maps:get(routeuri,Map),
    FrD = maps:get(fromdomain,Map),
    ZLSdp = maps:get(lsdp,Map),
    %
    ?DLG_STORE:push_dlg(ZCallId, {DlgId,self()}),
    ?MONITOR:append_fun(self(), ZCallId, fun() -> ?DLG_STORE:unlink_dlg(ZCallId) end),
    %
    CallerId = maps:get(callerid,Map),
    CallerName = maps:get(callername,Map,<<>>),
    ExtHeaders = maps:get(headers,Map,[]),
    HeadersToAdd = [{add,{HK,HV}} || {HK,HV} <- ExtHeaders, is_binary(HK) andalso is_binary(HV)],
    %
    ZRToUri = case To of
                  {ToS,ToU,ToD} when is_atom(ToS), is_binary(ToU), is_binary(ToD) -> #uri{scheme=ToS, user=ToU, domain=ToD};
                  {ToU,ToD} when is_binary(ToU), is_binary(ToD) -> #uri{scheme=sip, user=ToU, domain=ToD};
                  ToU when is_binary(ToU) ->
                      case catch ?U:parse_uris(ToU) of
                          [Uri|_] -> Uri; % RP-1297
                          _ -> #uri{scheme=sip, user=ToU, domain=FrD}
                      end;
                  #uri{} -> To
              end,
    ZLUri = #uri{scheme=sip, disp=?U:quote_display(CallerName), user=CallerId, domain=FrD},
    ZLTag = ?MRCP_DIALING_UTILS:build_tag(),
    ZLFromUri = ZLUri#uri{ext_opts=[{<<"tag">>,ZLTag}]},
    ZLContact = ?U:build_local_contact(ZLUri),
    %
    Timeout = 5000,
    Route = {route, RouteUri},
    CmnOpts = [{call_id,ZCallId},
               {from,ZLFromUri},
               {to,ZRToUri},
               %user_agent,
               Route,
               {contact,ZLContact}],
    InviteOpts = [auto_2xx_ack,
                  %{add, {?OwnerHeader, <<"rMRCP-", SrvIdxT/bitstring>>}},
                  %{add, {?CallerTypeHeader, <<"mrcp">>}},
                  {cseq_num,1},
                  {body,ZLSdp}
                  |HeadersToAdd],
    %
    ZFork = #side_fork{callid=ZCallId,
                       %
                       localuri=ZLFromUri,localtag=ZLTag,localcontact=ZLContact,
                       remoteuri=ZRToUri,
                       requesturi=ZRToUri,
                       rule_timeout=Timeout,
                       cmnopts=CmnOpts,
                       inviteopts=InviteOpts,
                       starttime=os:timestamp()},
    %
    do_start_call_invite(ZFork, State, FRes).

%% =================================================
%% Invite services
%% =================================================

%% ---------------------
%% send invite and initialize state
%% ---------------------
do_start_call_invite(Fork, State, FRes) ->
    #state_mrcp{forks=Forks}=State,
    #side_fork{callid=ZCallId}=Fork,
    case send_invite(Fork, State) of
        {error,Err2}=Err1 ->
            {ok, State1} = ?MRCP_MEDIA:media_detach(ZCallId, State),
            Err = case Err2 of {error,_} -> Err2; _ -> Err1 end,
            FRes(Err, State1);
        {ok, Fork1} ->
            ForkCall = #{type => 'call',
                         fork => Fork1,
                         zcallid => ZCallId,
                         startgs => ?EU:current_gregsecond()},
            State1 = State#state_mrcp{forks=[{ZCallId,ForkCall}|Forks]},
            FRes(ok, State1)
    end.

%% @private
%% sends invite to Z
send_invite(Fork, State) ->
    Now = os:timestamp(),
    #state_mrcp{app=App}=State,
    #side_fork{callid=ZCallId,
               requesturi=Uri,
               localtag=ZLTag,
               rule_timeout=Timeout,
               cmnopts=CmnOpts,
               inviteopts=InviteOpts}=Fork,
    %
    case catch nksip_uac:invite_noopts(App, Uri, [async|CmnOpts++InviteOpts]) of
        {'EXIT',Err} ->
            d(State, "invite_fork LTag=~p, Caught error=~120p", [ZLTag, Err]),
            {error, Err};
        {error,_R}=Err ->
            d(State, "invite_fork LTag=~p, Error=~120p", [ZLTag, Err]),
            {error, Err};
        {async,ReqHandle} ->
            d(State, "invite_fork LTag=~p", [ZLTag]),
            Fork1 = Fork#side_fork{rhandle=ReqHandle,
                                   starttime=Now,
                                   rule_timer_ref=erlang:send_after(Timeout, self(), {mrcp,{?MRCP_TIMEOUT_MSG, [ZCallId, ZLTag]}})},
            {ok, Fork1}
    end.

%% =================================================

%% -------------------------------------
%% UAC response (only invite filtered)
%% -------------------------------------
do_on_uac_response([#sipmsg{cseq={_,'INVITE'}, call_id=CallId}=Req]=_P, ForkCall, State) ->
    fork_invite_response([CallId, Req], ForkCall, State).

%% -------------------------------------
%% @private Fork invite response
%% -------------------------------------
fork_invite_response([_CallId, #sipmsg{class={resp,SipCode,_}}=_Resp]=P, ForkCall, State) when SipCode >= 100, SipCode < 200 ->
    fork_invite_response_1xx(P,ForkCall,State);

fork_invite_response([_CallId, #sipmsg{class={resp,SipCode,_}}=_Resp]=P, ForkCall, State) when SipCode >= 200, SipCode < 300 ->
    fork_invite_response_2xx(P,ForkCall,State);

fork_invite_response([CallId, #sipmsg{class={resp,SipCode,_}}=Resp], ForkCall, State) when SipCode >= 300, SipCode < 400 ->
    fork_error(CallId, ForkCall, Resp, State);

fork_invite_response([CallId, #sipmsg{class={resp,SipCode,_}}=Resp], ForkCall, State) when SipCode >= 400, SipCode < 700 ->
    fork_error(CallId, ForkCall, Resp, State);

fork_invite_response([CallId, #sipmsg{class={resp,_SipCode,_}}=Resp], ForkCall, State) ->
    fork_error(CallId, ForkCall, Resp, State).

%% -------------------------------------
%% @private Fork invite response 1xx
%% -------------------------------------
fork_invite_response_1xx([CallId,#sipmsg{class={resp,_SipCode,_SipReason}}=Resp],ForkCall,State) ->
    #state_mrcp{forks=Forks}=State,
    Fork = maps:get(fork,ForkCall),
    Fork1 = ?MRCP_DIALING_UTILS:update_side_fork(Fork,Resp),
    ForkCall1 = ForkCall#{fork:=Fork1},
    State1 = State#state_mrcp{forks=lists:keyreplace(CallId, 1, Forks, {CallId,ForkCall1})},
    case ?U:extract_sdp(Resp) of
        #sdp{}=RSdp -> fork_invite_response_1xx_connect([CallId,Resp,RSdp],ForkCall1,State1);
        _ -> {next_state, ?CURRSTATE, State1}
    end.

% @private
fork_invite_response_1xx_connect([CallId,#sipmsg{}=Resp,RSdp],ForkCall,State) ->
    #state_mrcp{forks=Forks}=State,
    #side_fork{rhandle=ReqHandle}=Fork=maps:get(fork,ForkCall),
    case ?MRCP_MEDIA:media_update_outgoing_by_remote_ext(Fork, RSdp, State) of
        {error,_E} ->
            State1 = ?MRCP_FSM_UTILS:send_response(ReqHandle, ?InternalError("MRCP. Attach media error"), State),
            State2 = State1#state_mrcp{forks=lists:keydelete(CallId,1,Forks)},
            {next_state, ?CURRSTATE, State2};
        {ok, State1} ->
            Fork1 = ?MRCP_DIALING_UTILS:update_side_fork(Fork,Resp),
            State2 = State1#state_mrcp{forks=lists:keyreplace(CallId, 1, Forks, {CallId,ForkCall#{fork:=Fork1}})},
            {next_state, ?CURRSTATE, State2}
    end.

%% -------------------------------------
%% @private Fork invite response 2xx
%% -------------------------------------
fork_invite_response_2xx([CallId,#sipmsg{class={resp,_SipCode,_},to={_,ToTag}}=Resp],ForkCall,State) ->
    #state_mrcp{forks=Forks}=State,
    Fork = maps:get(fork,ForkCall),
    Side = ?MRCP_DIALING_UTILS:make_side_by_fork(Fork,Resp),
    State1 = State#state_mrcp{forks=lists:keydelete(CallId, 1, Forks),
                              a=Side#side{state=dialog,remotetag=ToTag}},
    #sdp{}=RSdp=?U:extract_sdp(Resp),
    case ?MRCP_MEDIA:media_update_outgoing_by_remote_ext(Fork, RSdp, State1) of
        {error,_E} ->
            {ok,State2} = ?MRCP_DIALING_UTILS:do_stop_bye(Side, State1),
            ?MRCP_FSM_UTILS:return_terminating(?MRCP_FSM_UTILS:do_finalize(State2));
        {ok, State2} ->
            State3 = ?MRCP_DIALING_UTILS:prepare_to_start_active(CallId,State2),
            {next_state, ?ACTIVE_STATE, State3}
    end.

%% -------------------------------------
%% @private Delete fork on error
%% -------------------------------------
fork_error(CallId, ForkCall, #sipmsg{class={resp,_SipCode,_Descr}}=_Resp, State) ->
    State1 = case ?MRCP_MEDIA:media_detach(CallId, State) of
                 {ok,StateX} -> StateX;
                 {error,_} -> State#state_mrcp{media=undefined}
             end,
    #state_mrcp{forks=Forks}=State1,
    State2 = case lists:keytake(CallId, 1, Forks) of
                 false -> State1;
                 {value,_,Forks1} ->
                     Fork = maps:get(fork,ForkCall),
                     % remove fork
                     #side_fork{rule_timer_ref=TimerRef}=Fork,
                     ?EU:cancel_timer(TimerRef),
                     ?DLG_STORE:unlink_dlg(CallId),
                     State1#state_mrcp{forks=Forks1}
             end,
    ?MRCP_FSM_UTILS:return_terminating(?MRCP_FSM_UTILS:do_finalize(State2)).

%% =============================================================

%% @private
%% d(State, Text) -> d(State, Text, []).
d(State, Fmt, Args) ->
    #state_mrcp{dlgid=DlgId}=State,
    ?LOGSIP("MRCP fsm ~p '~p':" ++ Fmt, [DlgId,?CURRSTATE] ++ Args).