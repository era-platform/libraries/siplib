%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.07.2019
%%% @doc RP-1470
%%%      Routines for dialing state of MRCP-FSM
%%%      Based on IVR (r_sip_ivr_fsm_dialing_utils)

-module(r_sip_mrcp_fsm_dialing_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([build_out_callid/1,
         build_tag/0,
         build_routeuri/1,
         update_side_fork/2,
         make_side_by_fork/2,
         prepare_to_start_active/2,
         cancel_active_forks/3,
         do_stop_bye/1,
         do_stop_bye/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_headers.hrl").
-include("../include/r_sip_ivr.hrl").
-include("../include/r_sip_mgc.hrl").
-include("../include/r_sip_mrcp.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---------------------------------
%% Build call id for out call. Checks for non exist in current conf
%% ---------------------------------
build_out_callid(#state_mrcp{a=undefined}) ->
    CurSrvCode = ?U:get_current_srv_textcode(),
    CallId = ?U:luid(),
    <<"rMRCP-", CurSrvCode/bitstring, "-", CallId/bitstring>>.

%% ---------------------------------
%% Build local tag for out call
%% ---------------------------------
build_tag() ->
    CurSrvCode = ?U:get_current_srv_textcode(),
    <<Tag0:48/bitstring, _/bitstring>> = ?U:luid(),
    <<"rIV-", CurSrvCode/bitstring, "-", Tag0/bitstring>>.

%% ---------------------------------
%% Build route uri for out call by mrcp account info
%% ---------------------------------
build_routeuri(#state_mrcp{}=_State) ->
    RouteUser = undefined,
    RouteDomain = undefined,
    ?U:unparse_uri(#uri{scheme=sip, user=RouteUser, domain=RouteDomain, opts=?CFG:get_transport_opts()++[<<"lr">>]}).

%% ---------------------------------
%% update fork_side rec of new competitor by 1xx response with sdp
%% ---------------------------------
-spec update_side_fork(Fork::#side_fork{}, Resp::#sipmsg{}) -> UpFork::#side_fork{}.
%% ---------------------------------
update_side_fork(#side_fork{}=Fork, #sipmsg{class={resp,SipCode,_}}=Resp) ->
    #side_fork{}=Fork,
    {ok,DlgHandle} = nksip_dialog:get_handle(Resp),
    #sipmsg{contacts=RContacts}=Resp,
    RSdp = ?U:extract_sdp(Resp),
    Fork#side_fork{dhandle=DlgHandle,
                   last_response_code=SipCode,
                   remotecontacts=RContacts,
                   remotesdp=RSdp}.

%% ---------------------------------
-spec make_side_by_fork(Fork::#side_fork{},Resp::#sipmsg{}) -> Side::#side{}.
%% ---------------------------------
make_side_by_fork(Fork,Resp) ->
    #side_fork{}=Fork,
    {ok,DlgHandle} = nksip_dialog:get_handle(Resp),
    #sipmsg{contacts=RContacts}=Resp,
    #sdp{}=RSdp=?U:extract_sdp(Resp),
    Now = os:timestamp(),
    XSide = #side{callid=Fork#side_fork.callid,
                  rhandle=Fork#side_fork.rhandle,
                  dhandle=DlgHandle,
                  dir='out',
                  req=Resp,
                  %
                  remoteuri=Fork#side_fork.remoteuri,
                  remotetag=Fork#side_fork.remotetag,
                  remotecontacts=RContacts,
                  remotesdp=RSdp,
                  %
                  localuri=Fork#side_fork.localuri,
                  localtag=Fork#side_fork.localtag,
                  localcontact=Fork#side_fork.localcontact,
                  %
                  starttime=Fork#side_fork.starttime,
                  answertime=Now
                 },
    XSide.

%% ----------------------------------------------------------
%% Prepare after success dialing to start active
%% ----------------------------------------------------------
prepare_to_start_active(CallId,#state_mrcp{dlgid=DlgId}=State) ->
    Self = self(),
    % Local node storage
    ?DLG_STORE:push_dlg(CallId, {DlgId,Self}),
    % Monitor to clear
    Flog = fun(StopReason) -> ?OUT("Process ~p terminated (mrcp, dlgid=~p, callid=~p): ~120p", [Self, DlgId, CallId, StopReason]) end,
    Fclear = fun() -> ?DLG_STORE:unlink_dlg(CallId) end,
    [?MONITOR:append_fun(Self, CallId, FunX) || FunX <- [Flog, Fclear]],
    State.

%% ----------------------------------------------------------
%% Cancel active forks
%% ----------------------------------------------------------
cancel_active_forks([],_,_) -> [];
cancel_active_forks([Fork|Rest], Opts, State) ->
    #side_fork{rhandle=ReqHandle,
               cmnopts=CmnOpts}=Fork,
    cancel_timer(Fork),
    spawn(fun() -> catch nksip_uac:cancel(ReqHandle, [async|Opts++CmnOpts]) end),
    cancel_active_forks(Rest, Opts, State).
% @private
cancel_timer(#side_fork{rule_timer_ref=T1, resp_timer_ref=T2}=Fork) ->
    case T1 of undefined -> false; _ -> ?EU:cancel_timer(T1) end,
    case T2 of undefined -> false; _ -> ?EU:cancel_timer(T2) end,
    Fork#side_fork{rule_timer_ref=undefined,
                   resp_timer_ref=undefined}.

%% ---------------------------------
%% detach existing participant
%% ---------------------------------

%% --------------------
%% send bye and detach current participant
%% --------------------
-spec do_stop_bye(State::#state_mrcp{}) -> {ok, State::#state_mrcp{}}.
%% --------------------
do_stop_bye(State) ->
    #state_mrcp{a=Side}=State,
    ?MRCP_FSM_UTILS:send_bye(Side, State),
    do_stop(State).

%% --------------------
%% send bye and detach selected participant
%% --------------------
-spec do_stop_bye(Side::#side{}, State::#state_mrcp{}) -> {ok, State::#state_mrcp{}}.
%% --------------------
do_stop_bye(Side, State) ->
    ?MRCP_FSM_UTILS:send_bye(Side, State),
    do_stop(State).

%% --------------------
%% detach participant from conference (last should destroy conference)
%% --------------------
-spec do_stop(State::#state_mrcp{}) -> {ok, State::#state_mrcp{}}.
%% --------------------
do_stop(State) ->
    #state_mrcp{a=#side{callid=CallId}=_Side}=State,
    % from store
    ?DLG_STORE:unlink_dlg(CallId),
    {ok, State}.