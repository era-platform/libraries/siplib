%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.07.2019
%%% @doc Routines of MRCP's SIP FSM
%%%      Based on IVR (r_sip_ivr_utils)

-module(r_sip_mrcp_fsm_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([send_response/3,
         send_bye/2,
         do_finalize/1,
         return_terminating/1,
         error_mgc_final/3,
         reason_external/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_mrcp.hrl").
-include("../include/r_sip_ivr.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% --------------------------
%% send bye to participant
%% --------------------------
send_bye(#side{state=bye}=Side, _State) -> Side;
send_bye(Side, _State) ->
    #side{dhandle=DlgHandle}=Side,
    Opts = [user_agent],
    spawn(fun() -> catch nksip_uac:bye(DlgHandle, [async|Opts]) end),
    Side#side{state=bye}.

%% --------------------------
%% Send sip response to incoming request
%% --------------------------
send_response(#sipmsg{class={req,_}}=Req, SipReply, State) ->
    {ok,ReqHandle} = nksip_request:get_handle(Req),
    send_response(ReqHandle, SipReply, State);
send_response(ReqHandle, {SipCode,_Opts}=SipReply, State) ->
    d(State, "send_response ~p", [SipCode]),
    ?U:send_sip_reply(fun() -> nksip_request:reply(SipReply, ReqHandle) end),
    State;
send_response(ReqHandle, SipReply, State)  ->
    ?U:send_sip_reply(fun() -> nksip_request:reply(SipReply, ReqHandle) end),
    State.

%% --------------------------
%% Finalize mrcp-sip
%% --------------------------
do_finalize(State) ->
    finalize_notify(
        finalize_media(
            finalize_disconnect_tcp(
                finalize_callids(
                    finalize_bye(
                        finalize_timer(State)))))).

%% @private
finalize_timer(#state_mrcp{timer_ref=TimerRef}=State) ->
    ?EU:cancel_timer(TimerRef),
    State#state_mrcp{timer_ref=undefined}.

%% @private
finalize_bye(#state_mrcp{a=ZSide,forks=Forks}=State) ->
    case State#state_mrcp.state of
        ?MRCP_FSM_ACTIVE_STATE -> State#state_mrcp{a=send_bye(ZSide, State)};
        ?MRCP_FSM_DIALING_STATE ->
            Forks1 = lists:map(fun({CallId,#{}=ForkCall}) ->
                                      ?MRCP_DIALING_UTILS:cancel_active_forks([maps:get(fork,ForkCall)], [], State),
                                      {CallId,undefined}
                               end, Forks),
            State#state_mrcp{forks=Forks1}
    end.

%% @private
%% unlink callids from store
finalize_callids(#state_mrcp{acallid=ACallId, forks=Frk}=State) ->
    F = fun(CallId, Acc) ->
                case lists:member(CallId, Acc) of
                    false -> ?DLG_STORE:unlink_dlg(CallId), [CallId|Acc];
                    true -> Acc
                end end,
    X0 = [],
    X1 = F(ACallId, X0),
    _X2 = lists:foldl(fun({CallId,_}, Acc) -> F(CallId, Acc) end, X1, Frk),
    State#state_mrcp{forks=[]}.

%% @private
finalize_disconnect_tcp(State) ->
    % TODO disconnect MRCP-TCP
    State.

%% @private
finalize_media(#state_mrcp{acallid=ZCallId}=State) ->
    case ?MRCP_MEDIA:media_detach(ZCallId,State) of
        {ok,StateX} -> StateX;
        {error,_} -> State#state_mrcp{media=undefined}
    end.

%% @private
%% send event to mrcp's operation owner (subscriber)
finalize_notify(#state_mrcp{opts=Opts,mrcpreason=Reason,stopreason=StopReason}=State) ->
    case Reason of
        undefined ->
            FunError = maps:get(fun_error,Opts,fun(_) -> ok end),
            FunError({error,StopReason});
        _ ->
            FunComplete = maps:get(fun_complete,Opts,fun(_) -> ok end),
            FunComplete(Reason)
    end,
    State.

%% --------------------------
%% Switch mrcp-fsm to terminating state
%% --------------------------
return_terminating(#state_mrcp{}=_State) ->
    % stop OR switch to 'terminating' state to wait BYE-response
    stop.

%% --------------------------
%% finish with error of mgc
%% --------------------------
error_mgc_final(Reason, State, Side) ->
    error_mgc_final(Reason, State, Side, ?InternalError("MRCP. MGC error")).
error_mgc_final(_Reason, State, #side{rhandle=RHandle}=_Side, SipReply) ->
    State1 = ?MRCP_FSM_UTILS:send_response(RHandle, SipReply, State),
    ?MRCP_FSM_UTILS:return_terminating(?MRCP_FSM_UTILS:do_finalize(State1));
error_mgc_final(_Reason, State, undefined, _SipReply) ->
    ?MRCP_FSM_UTILS:return_terminating(?MRCP_FSM_UTILS:do_finalize(State)).

%% --------------------------
reason_external(#{}=Reason) -> Reason;
reason_external(Reason) ->
    #{type => external,
      reason => Reason}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% =============================================================

%% @private
%% d(State, Text) -> d(State, Text, []).
d(StateData, Fmt, Args) ->
    #state_mrcp{state=State,dlgid=DlgId}=StateData,
    ?LOG("MRCP fsm ~p '~p':" ++ Fmt, [DlgId,State] ++ Args).