%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 10.07.2019
%%% @doc RP-1470
%%%      Adapter for StateData and process from IVR-FSM into MRCP-FSM
%%%      Filters all messages to IVR-FSM, separates all related to MRCP-FSM and continues others.

-module(r_sip_mrcp_adapter4ivr).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([handle_event/4]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_mrcp.hrl").
-include("../include/r_sip_ivr.hrl").
-include("../include/r_sip_mgc.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ----------------------
%% filter mrcp events and translates them into MRCP-FSM.
%% ----------------------
handle_event(EventType,EventContent,_StateName,#state_ivr{ext_data=MrcpData}=StateData) ->
    CallId = case MrcpData of
                 #state_mrcp{acallid=C}=MrcpData -> C;
                 _ -> undefined
             end,
    X = case EventContent of
            {extension, {mrcp_start,Opts}} ->
                #state_ivr{ivrid=IvrId,media=Media,map=Map,acallid=ACallId}=StateData,
                #media_ivr{mgc=MGC,msid=MSID,mgid=MGID,ctx=CTX}=Media,
                ?MRCP_FSM:init(Opts#{dlgid => IvrId,
                                     media_context_info => #{mgc=>MGC,msid=>MSID,mgid=>MGID,ctx=>CTX},
                                     app => maps:get(app,Map),
                                     domain => maps:get(domain,Map),
                                     ownerpid => maps:get(ownerpid,Map,undefined),
                                     parent_callid => ACallId,
                                     scriptmachine => StateData#state_ivr.scriptmachine});
            {mrcp,EventContent1} -> {take,EventContent1};
            {sip_bye, [CallId,_Req]} -> take;
            {sip_reinvite, [CallId,_Req]} -> take;
            {sip_refer, [CallId,_Req]} -> take;
            {sip_notify, [CallId,_Req]} -> take;
            {sip_info, [CallId,_Req]} -> take;
            {sip_message, [CallId,_Req]} -> take;
            {uac_response,[#sipmsg{call_id=CallId}=_Resp]} -> take;
            {uas_dialog_response,[#sipmsg{call_id=CallId}=_Resp]} -> take;
            {call_terminate, [_Reason, #call{call_id=CallId}=_Call]} -> take;
            {stop_external, _Reason} -> take_stop;
            {'DOWN',_,_,_,_} -> take_continue;
            %{stop_forcely} -> ok;
            %{replace_abonent, [Req]} -> ok;
            %{save_callpid, [{callpid,CallPid}]} -> ok;
            %{mg_dtmf, [_CtxId, CtxTermId, #{}=Event]=_Args} -> ok;
            %{mg_trunknovoice, Args} -> ok;
            %{mg_event, [_CtxId, CtxTermId, EType, #{}=Event]=_Args} -> ok;
            %{mg_disconnected, Args} -> ok;
            %{mg_migrate, [_MSID,_MG,_Mode]=Args} -> ok;
            %{mg_migrate_priv,[_MSID,_MG,_Mode]=Args,I,T} -> ok;
            %{change_mg} -> ok;
            %{scriptmachine_event} -> ok;
            _ -> continue
        end,
    X1 = case X of
             take -> {take,EventContent};
             {take,EventContent2} -> {take,EventContent2};
             R -> R
         end,
    Ftake = fun(EContent) -> ?MRCP_FSM:handle_event(EventType,EContent,MrcpData#state_mrcp.state,MrcpData) end,
    case X1 of
        {take,EventContent3} -> return(Ftake(EventContent3),StateData);
        take_continue -> Ftake(EventContent), return(continue,StateData);
        take_stop -> stop(Ftake(EventContent),StateData);
        Result -> return(Result,StateData)
    end.

%% ----------------------
%% @private
%% translates mrcp result into ivr result
%% ----------------------
return(continue,_) -> continue;
return(Result,StateData) ->
    case Result of
        {ok,S1,D1} -> {keep_state,StateData#state_ivr{ext_data=D1#state_mrcp{state=S1}}};
        {keep_state,D1,ReplyOpts} -> {keep_state,StateData#state_ivr{ext_data=D1},ReplyOpts};
        {keep_state,D1} -> {keep_state,StateData#state_ivr{ext_data=D1}};
        {next_state,S1,D1,ReplyOpts} -> {keep_state,StateData#state_ivr{ext_data=D1#state_mrcp{state=S1}},ReplyOpts};
        {next_state,S1,D1} -> {keep_state,StateData#state_ivr{ext_data=D1#state_mrcp{state=S1}}};
        stop -> {keep_state,StateData#state_ivr{ext_data=undefined,ext_handler_fun=undefined}}
    end.

%% ----------------------
%% @private
%% finalize mrcp
%% ----------------------
stop(_Result,StateData) -> {keep_state,StateData#state_ivr{ext_data=undefined,ext_handler_fun=undefined}}.