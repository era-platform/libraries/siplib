%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 10.07.2019
%%% @doc RP-1470
%%%      Adapter from MRCP-FSM into Media routines
%%%      Uses owned #media_ivr structure, but linked to the same media-context as parent IVR-FSM
%%%      Based on IVR (r_sip_ivr_media)

-module(r_sip_mrcp_media).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([media_add_outgoing/2,
         media_detach/2,
         media_update_outgoing_by_remote_ext/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_mrcp.hrl").
-include("../include/r_sip_ivr.hrl").
-include("../include/r_sip_mgc.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% -------------------------------
%% Attach outgoing termination to parent's context. Constructs owned #media_ivr structure
%% -------------------------------
-spec media_add_outgoing(ZCallId::binary(), State::#state_mrcp{}) -> {ok,State::#state_mrcp{},LSdp::#sdp{}} | {error, Reason::term()}.
%% -------------------------------
media_add_outgoing(ZCallId, #state_mrcp{opts=Opts}=State) ->
    [MGC,MSID,MGID,CTX] = ?EU:maps_get([mgc,msid,mgid,ctx], maps:get(media_context_info,Opts)),
    Media = #media_mrcp{mgc = MGC,
                        msid = MSID,
                        mgid = MGID,
                        ctx = CTX},
    case ?MEDIA_MRCP:add_outgoing_term(Media, maps:get(resource,Opts), maps:get(mode,Opts,sendrecv), ZCallId) of
        {ok,Media1,LSdp} -> {ok,State#state_mrcp{media=Media1},LSdp};
        {error,_}=Err -> Err
    end.

%% -------------------------------
%% Removes termination from parent's context
%% -------------------------------
-spec media_detach(ZCallId::binary(), State::#state_mrcp{}) -> {ok, State1::#state_mrcp{}}.
%% -------------------------------
media_detach(_ZCallId, #state_mrcp{media=undefined}=State) -> {ok,State};
media_detach(ZCallId, #state_mrcp{media=Media}=State) ->
    case ?MEDIA_MRCP:del_term(Media, ZCallId) of
        {ok,Media1} -> {ok,State#state_mrcp{media=Media1}};
        {error,_}=Err -> Err
    end.

%% -------------------------------
%% Modifies owned termination in parent's context
%% -------------------------------
-spec media_update_outgoing_by_remote_ext(Fork::#side_fork{}, RSdp::#sdp{}, State::#state_mrcp{}) -> {ok,State::#state_mrcp{}} | {error,Reason::term()}.
%% -------------------------------
media_update_outgoing_by_remote_ext(#side_fork{callid=CallId}=_Fork, RSdp, #state_mrcp{media=Media}=State) ->
    case ?MEDIA_MRCP:update_outgoing_term_by_remote(Media, CallId, RSdp) of
        {ok,Media1} -> {ok,State#state_mrcp{media=Media1}};
        {error,_}=Err -> Err
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================