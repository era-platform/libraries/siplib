%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Facade service to obtain registrations by number (over domaincenter and registrar)

-module(r_sip_callee).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([get_callee/2, prepare_routed_uris_on_reg_contacts/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").

-include("../_build/default/lib/nksip/include/nksip_registrar.hrl").

%% ====================================================================
%% API functions
%% ====================================================================


%% ----------------------------------------
%% Preparing list of callee by calledid using numberplan or username
%%
get_callee('map'=Mode, {_Scheme,_User,_Domain}=AOR) ->
    case get_callee_mode(Mode, AOR) of
        {reply,_}=R -> R;
        [] -> {reply, ?TemporarilyUnavailable("B2B. Unregistered")};
        List when is_list(List) ->
            case lists:flatten(List) of
                [] -> {reply, ?TemporarilyUnavailable("B2B. Unregistered")};
                _ -> List
            end
    end.

%% ----------------------------------------
% #41 @abramov 07.02.2017
% Preparing routed uris (path routing, nat traversal, proto)
-spec prepare_routed_uris_on_reg_contacts(RegContacts::[#reg_contact{}]) -> Uris::[#uri{}].

prepare_routed_uris_on_reg_contacts(RegContacts) ->
    get_routed_uris_sub(RegContacts, []).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
%% Preparing list of callee by calledid using numberplan or username

get_callee_mode(Mode, {Scheme, User, Domain}) ->
    % #238, fix by #246
    get_callee_mode_1(Mode,{Scheme,User,Domain}).

% 1. user direct
get_callee_mode_1(_Mode, {Scheme, User, Domain}) ->
    case ?ACCOUNTS:check_sipuser_exists(User, Domain) of
        true ->
            [_Phone,_DName,Opts] = get_sipuser(User,Domain),
            UserItem = case maps:get(<<"calltimesec">>,Opts,undefined) of
                           undefined -> User;
                           Timeout when is_number(Timeout) -> {User,Timeout*1000}
                       end,
            [get_routed_uris(Scheme,UserItem,Domain)];
        false ->
            get_callee_mode_2(_Mode, {Scheme, User, Domain})
    end.

% 2. user by number
get_callee_mode_2(_Mode, {Scheme, User, Domain}) ->
    Num = User,
    case ?NUMBERPLAN:find_users(Num, Domain) of
        undefined -> {reply, ?InternalError("B2B. Domain inaccessible")};
        [] -> {reply, ?NotFound("B2B. Unknown number")};
        SeqList when is_list(SeqList) ->
            ?LOGSIP("Call to [~140p, ~140p] -> ~140p", [Num, Domain, SeqList]),
            F1 = fun(SeqItem, Acc) -> [get_routed_uris(Scheme, SeqItem, Domain) | Acc] end,
            lists:foldr(F1, [], SeqList)
    end.

% @private
% Preparing routed uris (path routing, nat traversal, proto)
% Call to registrar for all forks,
% Returns map of uri, aor, [timeout]
get_routed_uris(Scheme, UserName, Domain) when is_binary(UserName) ->
    AOR = {Scheme, UserName, Domain},
    case get_routed_uris_sub(?REGISTRAR:get(AOR), []) of
        [] -> [];
        RC -> SUI = get_sipuser_info(UserName,Domain),
              [#{aor=>{Uri#uri.scheme,UserName,Domain}, uri=>Uri, sipuser=>SUI} || Uri <- RC]
    end;
get_routed_uris(Scheme, {UserName,Timeout}, Domain) when is_binary(UserName), is_integer(Timeout) ->
    AOR = {Scheme, UserName, Domain},
    case get_routed_uris_sub(?REGISTRAR:get(AOR), []) of
        [] -> [];
        RC -> SUI = get_sipuser_info(UserName,Domain),
              [#{aor=>{Uri#uri.scheme,UserName,Domain}, uri=>Uri, sipuser=>SUI, ftimeout=>Timeout} || Uri <- RC] % @forktimeout
    end;
get_routed_uris(Scheme, ForkList, Domain) when is_list(ForkList) ->
    F = fun(ForkItem) -> get_routed_uris(Scheme, ForkItem, Domain) end,
    lists:flatten([F(ForkItem) || ForkItem <- ForkList]).


%% @private
get_routed_uris_sub([], Uris) -> lists:reverse(Uris);
get_routed_uris_sub([RegContact|RestRC], Uris) ->
    #reg_contact{contact=#uri{opts=Opts, headers=Headers}=Uri, path=Path, nkport=RTransp} = RegContact,
    Uri1 = Uri#uri{disp= <<>>},
    % Nat traversal
    Uri2 = case ?ROUTE:check_nat(Uri, RTransp, Path) of
        true -> Uri1#uri{opts=[{<<"reqopts">>, [{nat_traversal, RTransp}]} | Opts]};
        false -> Uri1
    end,
    % Registered Path -> Route
    Uri3 = case Path of
        [] -> Uri2;
        _ -> Uri2#uri{headers=nksip_headers:update(Headers, [{multi, <<"route">>, Path}])}
    end,
    get_routed_uris_sub(RestRC, [Uri3|Uris]).

%% @private
get_sipuser_info(UserName, Domain) ->
    [Phone,DName,Opts] = get_sipuser(UserName,Domain),
    [{login,UserName},
     {domain,Domain},
     {phone,Phone},
     {name,DName},
     {opts,Opts},
     {extension,undefined}].

% @private
get_sipuser(UserName, Domain) ->
    case ?ACCOUNTS:get_sipuser(UserName,Domain) of
        {ok,SU} -> ?EU:maps_get([phonenumber,name,opts],SU);
        _ -> [[],<<>>,[]]
    end.
