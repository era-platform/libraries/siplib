%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Provides log writer service of transport data (trn).
%%%        Handles nksip's private callback, if registered by sip role at start.

-module(r_sip_do_log).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([sip_log/2,
         banned/2]).

%% ==========================================================================
%% Types
%% ==========================================================================

-include("../include/r_sip.hrl").

% -include("../include/r_nksip.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

% sip_log(_, _) -> ok;

sip_log({trn_send, [Transp, Packet, Result]}=_Query, _AppId) ->
    #nkport{transp=Proto, remote_ip=Ip, remote_port=Port} = Transp,
    ?LOGTRN("Send to ~p ~s:~p (~140p)~n~s", [Proto,inet:ntoa(Ip),Port,Result,format_packet(Packet)]),
    ok;

sip_log({trn_recv, [Transp, Packet]}=_Query, _AppId) ->
    #nkport{transp=Proto, remote_ip=Ip, remote_port=Port} = Transp,
    ?LOGTRN("Recv from ~p ~s:~p ~n~s", [Proto,inet:ntoa(Ip),Port,format_packet(Packet)]),
    ok.

%% print to trn about banned request
banned(Transp, MsgMode) ->
    #nkport{transp=Proto, remote_ip=Ip, remote_port=Port}=Transp,
    %case ?U:is_trn_enabled() of
    %    true -> ?LOGTRN("Recv from ~p ~s:~p ~s~n", [Proto,inet:ntoa(Ip),Port,MsgMode]);
    %    _ -> ok
    %end,
    ?LOGTRN("Recv from ~p ~s:~p ~s~n", [Proto,inet:ntoa(Ip),Port,MsgMode]).

%% ====================================================================
%% Internal functions
%% ====================================================================

format_packet(<<>>) -> "\r\n";
format_packet(<<"\r\n">>) -> "\t\\r\\n\r\n";
format_packet(<<"\r\n\r\n">>) -> "\t\\r\\n\\r\\n\r\n";
format_packet(Packet) when is_binary(Packet) ->
    [$\t|binary_to_list(binary:replace(Packet, <<"\r\n">>, <<"\r\n\t">>, [global]))].
