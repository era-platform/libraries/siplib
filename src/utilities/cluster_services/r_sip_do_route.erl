%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Provides route routines

-module(r_sip_do_route).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([get_local_domain_flags/1,
         get_top_route_uri/1,
         check_local_group_address/1,
         check_nat/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").

-include("../_build/default/lib/nksip/include/nksip_registrar.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ---------------------------------------
%% Checking if request-uri's domain is local domain
%% ---------------------------------------
get_local_domain_flags(#uri{domain=Domain,port=Port}) ->
    IsLocalDomain = ?LOCALDOMAIN:is_cluster_domain(Domain),
    IsLocalIp = case DomainIp = ?U:to_ip(Domain) of
            {ok, Ip} ->
                case ?U:is_local_ip(Ip) of
                    false -> false;
                    true ->
                        {_, LocalPorts} = ?SIPSTORE:find_u('local_ports'),
                        CheckPort = case Port of 0 -> 5060; _ -> Port end,
                        lists:member(CheckPort, LocalPorts)
                end;
            error -> false
        end,
    IsClusterIp = check_local_group_address(DomainIp),
    {IsLocalDomain, IsLocalIp, IsClusterIp};

get_local_domain_flags(Domain) ->
    IsLocalDomain = ?LOCALDOMAIN:is_cluster_domain(Domain),
    IsLocalIp = case DomainIp = ?U:to_ip(Domain) of
            {ok, Ip} -> ?U:is_local_ip(Ip);
            error -> false
        end,
    IsClusterIp = check_local_group_address(DomainIp),
    {IsLocalDomain, IsLocalIp, IsClusterIp}.

%% ---------------------------------------
%% Checking if Ip is cluster's
%% ---------------------------------------
check_local_group_address(_Ip) ->
    % list:member(Ip, ?CFG:get_all_sipserver_addrs()).
    unknown.

%% ---------------------------------------
%% Builds request-uri from top route header value
%% ---------------------------------------
get_top_route_uri(Req) ->
    case Req of
        #sipmsg{routes=[]} -> notfound;
        #sipmsg{routes=Routes} ->
            LocalSockets = {?U:get_host_interfaces_ipv4(),?U:get_local_ports()},
            get_route(LocalSockets, Routes)
    end.

%% ---------------------------------------
%% @private
%% ---------------------------------------
-spec get_route(LocalSockets::[{[LocalIp::binary()],[LocalPort::integer()]}], Routes::[#uri{}]) -> #uri{} | notfound.
%% ---------------------------------------
get_route(_, []) -> notfound;
get_route({LA,LP}=L, [R|Rest]) ->
    #uri{domain=D, port=P, opts=_O}=R,
    case lists:member(D,LA) of
        true ->
            P1 = case P of 0 -> 5060; _ -> P end,
            case lists:member(P1,LP) of
                true -> get_route(L,Rest);
                _ -> R
            end;
        _ -> R
    end.

%% ---------------------------------------
%% General check nat usage func
%% ---------------------------------------
-spec check_nat(nksip:uri(), nksip:transport(), [nksip:uri()]) -> true | false.
%% ---------------------------------------
check_nat(_, undefined, _) -> false;
check_nat(_, #nkport{remote_ip=undefined}, _) -> false;
check_nat(#uri{domain=UriHost}=_Uri, #nkport{remote_ip=ReceivedAddr}=_Transp, []) ->
    list_to_binary(inet:ntoa(ReceivedAddr)) /= UriHost;
check_nat(_Uri, #nkport{remote_ip=ReceivedAddr}=_Transp, [#uri{domain=TopPathHost}=_TopPath|_Rest]) ->
    list_to_binary(inet:ntoa(ReceivedAddr)) /= TopPathHost.
