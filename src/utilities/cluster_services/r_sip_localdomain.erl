%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Provides service to check if selected domain is cluster domain.
%%%        Leans on fact, that first-level-domain names are the base of sub-domain names
%%% @todo

-module(r_sip_localdomain).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([is_cluster_subdomain/1,
         is_cluster_domain/1]).

%% ================================================================================
%% Types
%% ================================================================================

-include("../include/r_sip.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ----------------------------------------
%% Returns true if domain name belongs to cluster (subname of first-level-domain)
%% ----------------------------------------
is_cluster_subdomain(Domain) when is_binary(Domain) ->
    F = fun() ->
            case ?DC:get_root_domains() of
               R when is_list(R) -> R;
               _ -> []
            end end,
    Key = 'root_domains',
    case ?SIPSTORE:find_t(Key) of
        false ->
            RootDomains = F(),
            ?SIPSTORE:store_t(Key, RootDomains, 10000);
        {_,RootDomains} -> ok
    end,
    ?EU:check_issubdomain(Domain, RootDomains).

%% ----------------------------------------
%% Returns true if domain name belongs to cluster domain tree
%% ----------------------------------------
is_cluster_domain(Domain) when is_binary(Domain) ->
    F = fun() ->
            case ?DC:get_domain_tree() of
                R when is_list(R) -> R;
                _ -> []
            end end,
    Key = 'domain_tree',
    case ?SIPSTORE:find_t(Key) of
        false ->
            DomainTree = F(),
            ?SIPSTORE:store_t(Key, DomainTree, 10000);
        {_,DomainTree} -> ok
    end,
    lists:member(Domain,DomainTree).