%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Service
%%%         Computes representative of caller for callee

-module(r_sip_representative).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([get_user_phonenumber/3,
         get_user_phonenumber/4,
         apply/5,
         test/1]).

%-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").

-include("../include/r_sip_nk.hrl").

-type aor() :: {sip,Username::binary(),Domain::binary()}.

%% ====================================================================
%% API functions
%% ====================================================================

%% ----------------------------------------------
%% @TODO call could be invited from internalpbx, 'from' is some remote pbx number FU, but it should be replaced with SipUserNum+FU and sent to representative to provide redial and callerid
%%        local domain internal calls from sipuser directly should be forwarded as is
%%         ext calls should be forwarded as is
%%         everything unknown should be forwarded as is
%% ----------------------------------------------

%% ----------------------------------------------
%% returns phonenumber of FromAOR for display to ToAOR
%% it could be global, crossdomain, inside domain
%% ----------------------------------------------
-spec get_user_phonenumber(FromAOR::aor(), ToAOR::aor(), FromDisplay::binary()) -> {ok,Number::binary(),Display::binary()}.
%% ----------------------------------------------
get_user_phonenumber(FromAOR, ToAOR, FromDisplay) ->
    get_user_phonenumber(FromAOR, ToAOR, FromDisplay, undefined).

get_user_phonenumber({_,FU,FD}=FromAOR, {_,_,TD}=ToAOR, FromDisplay, Flog) ->
    case ?ACCOUNTS:get_phonenumber(FU,FD,[]) of
        {error,_} -> {ok,FU,FromDisplay};
        undefined -> {ok,FU,FromDisplay};
        FPhone when TD==FD -> {ok,FPhone,FromDisplay};
        FPhone ->
            Default = {undefined,<<"undefined">>,FromDisplay},
            find_representative(FromAOR, ToAOR, FromDisplay, FPhone, Default, Flog)
    end.

%% ----------------------------------------------
%% returns phonenumber of FromAOR for display to ToAOR
%% it could be global, crossdomain, inside domain
%%  Used by B2BUA. Considers that FromAOR could be remote pbx user.
%%   So compare from uri and username from auth header. If same - ok, if differ - uses as internalpbx
%% --------
%% x1. from sipuser not defined (ext,ivr,conf,...)                 => From.username
%% x2. from sipuser, from.domain == to.domain, not from pbx        => SipUser's phone
%% x3. from sipuser, from.domain /= to.domain, not from pbx        => representative (default undefined)
%% x4. from sipuser, from pbx                                    => representative (default From.username)
%% ----------------------------------------------
-spec apply(FromAOR::aor(), ToAOR::aor(), SipUserInfo::map(), FromDisplay::binary(), Flog::function())
      -> {ok,Number::binary(),Display::binary()}.
%% ----------------------------------------------
apply({_,FU,_}=FromAOR, ToAOR, undefined, FromDisplay, Flog) -> %FU;
    Default = {undefined,FU,FromDisplay},
    find_representative(FromAOR, ToAOR, FromDisplay, FU, Default, Flog);
apply(FromAOR, ToAOR, SipUserInfo, FromDisplay, Flog) ->
    [Login,Phone] = ?EU:extract_required_props([login,phone],SipUserInfo),
    apply_sipuser(FromAOR,ToAOR,{Login,Phone},FromDisplay,Flog).

%% @private
%% FU contains ready for representative number from username field of uri, returned by r_sip_from to r_sip_b2bua_router_invite
apply_sipuser({_,FU,FD}=_FromAOR, {_,_,TD}=_ToAOR, {_,SipUserPhone}, FromDisplay, _Flog) when TD==FD, FU==SipUserPhone ->
    {ok,SipUserPhone,FromDisplay};
apply_sipuser({FS,FU,FD}=_FromAOR, ToAOR, {SipUserName,SipUserPhone}, FromDisplay, Flog) when FU==SipUserPhone ->
    FromAOR1 = {FS,SipUserName,FD},
    Default = {undefined,<<"undefined">>,FromDisplay},
    find_representative(FromAOR1, ToAOR, FromDisplay, FU, Default, Flog);
apply_sipuser({FS,FU,FD}=_FromAOR, ToAOR, {SipUserName,_}, FromDisplay, Flog) ->
    FromAOR1 = {FS,SipUserName,FD},
    Default = {undefined,FU,FromDisplay},
    find_representative(FromAOR1, ToAOR, FromDisplay, FU, Default, Flog).


%% --------------------------
%% TESTING
%% --------------------------

%% ---------------------------
-spec test(Map::map()) -> {ok, Trace::[{Idx::integer(),[String::binary()]}]}.
%% ---------------------------
test(#{}=Map) ->
    ?ENV:set_group_leader(),
    case ?EU:maps_get([fromdomain,fromuser,todomain,touser],Map) of
        {error,_} -> {error, {invalid_params,<<"Invalid params. Expected 'fromdomain', 'fromuser', 'fromdisplayname', 'todomain', 'touser'">>}};
        [FD,FU,_TD,_TU]=Params ->
            FDisplay = maps:get(fromdisplayname,Map,<<>>),
            FPhone = case ?ACCOUNTS:get_phonenumber(FU,FD,[]) of
                         {error,_} -> FU;
                         undefined -> FU;
                         T -> T
                     end,
            do_test_1(Params,FDisplay,FPhone)
    end.

%% @private
do_test_1([FD,FU,TD,TU],FDisplay,FPhone) when FD==TD ->
    Result = [ln("~p. Start: from='~ts' in '~ts', to='~ts' in '~ts' (displayname='~ts')",[1,FU,FD,TU,TD,FDisplay]),
              ln("~p. Representative rules are not used. Result: num='~ts', displayname='~ts'",[2,FPhone,FDisplay])],
    {ok,Result};
do_test_1([FD,FU,TD,TU],FDisplay,FPhone) ->
    Ftrace = ?ENVTRACE:start(),
    Ftrace({start,FD,FU,TD,TU,FDisplay}),
    case catch fr({sip,FU,FD}, {sip,TU,TD}, FDisplay, FPhone, {undefined,FU}, [trace], undefined, Ftrace) of X -> X end,
    {ok,Trace} = ?ENVTRACE:stop(Ftrace),
    ?OUT("Test representative K:~n\t~120tp", [Trace]),
    ?OUT("Test representative L:~n\t~120tp", [make_trace(Trace)]),
    {ok, make_trace(Trace)}.

%% -------------
%% @private
make_trace([]) -> [];
make_trace(undefined) -> [];
make_trace(Trace) ->
    % map dc trace
    T1 = lists:foldl(fun({dc,Domain,Key,_,T}, Acc) -> lists:reverse(lists:map(fun({Id,Pr,Txt}) -> {dc,Domain,Key,Id,Pr,Txt} end, T)) ++ Acc;
                        (T,Acc) -> [T|Acc]
                     end, [], Trace),
    % convert dc trace to string
    {_,T2} = lists:foldl(fun(A,{Idx,Acc}) -> {Idx+1,[make_ln(Idx,A)|Acc]} end, {1,[]}, T1),
    lists:reverse(T2).

%% @private
make_ln(I, {start,FD,FU,TD,TU,FDisp}) -> ln("~p. Start: from='~ts' in '~ts', to='~ts' in '~ts' (displayname='~ts')",[I,FU,FD,TU,TD,FDisp]);
make_ln(I, {result,ResNum,ResDisp}) ->   ln("~p. Result: num='~ts', displayname='~ts'",[I,ResNum,ResDisp]);
make_ln(I, {default,DefNum,DefDisp}) ->  ln("~p. Use default: num='~ts', displayname='~ts'",[I,DefNum,DefDisp]);
make_ln(I, {rules,D,Key,start}) ->       ln("~p. DC start as '~ts' in '~ts'", [I,Key,D]);
make_ln(I, {rules,D,_Key,undefined}) ->  ln("~p. DC '~ts' service not found", [I,D]);
make_ln(I, {dc,D,_Key,Id,Pr,Txt}) ->     ln("~p. DC '~ts' rule=(priority=~1000p, id='~ts'): ~ts", [I,D,Pr,Id,Txt]);
make_ln(I, _) ->                         ln("~p. Unknown trace line",[I]).

%% @private
ln(Fmt,Args) -> ?EU:strbin(Fmt,Args).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
build_logfun(FromAOR,ToAOR,Flog) ->
    fun(Src,{FromN,ToN},{IdR,ResNum,ResDisp}) when is_function(Flog,1) ->
                Flog(#{from=>FromAOR,fromn=>FromN,to=>ToAOR,ton=>ToN,src=>Src,ruleid=>IdR,result_un=>ResNum,result_dn=>ResDisp});
       (_,_,_) -> ok
    end.

%% @private
build_tracefun(Ftrace) ->
    fun(X) when is_function(Ftrace,1) -> Ftrace(X);
       (_) -> ok
    end.

%% @private
find_representative(FromAOR, ToAOR, FDisplay, FPhone, Default, Flog) ->
    fr(FromAOR, ToAOR, FDisplay, FPhone, Default, [], Flog, undefined).

%% @private
%% prepare
fr(FromAOR, {_,TU,TD}=ToAOR, FDisplay, FPhone, Default, Opts, Flog, Ftrace) ->
    Flog3 = build_logfun(FromAOR,ToAOR,Flog),
    Ftrace3 = build_tracefun(Ftrace),
    % get phonenumber of ToUser
    TPhone = case ?ACCOUNTS:get_phonenumber(TU,TD,[]) of
                 {error,_} -> TU;
                 undefined -> TU;
                 T -> T
             end,
    fr_1([FromAOR, ToAOR, FDisplay, FPhone, TPhone, Default, Opts, Flog3, Ftrace3]).

%% "to" domain. Ask dest domain if there are rules to build callerid of src user
fr_1([FromAOR, {_,_,TD}=ToAOR, FDisplay, FPhone, TPhone, Default, Opts, Flog3, Ftrace3]) ->
    fr_call({find_representative, TD, 'cross', <<"to">>, fun fr_2/1},
            [FromAOR, ToAOR, FDisplay, FPhone, TPhone, Default, Opts, Flog3, Ftrace3]).

%% "from" domain. Ask src domain if there is rules to build own user's global callerid
fr_2([{_,_,FD}=FromAOR, ToAOR, FDisplay, FPhone, TPhone, Default, Opts, Flog3, Ftrace3]) ->
    fr_call({find_representative, FD, 'inner', <<"from">>, fun fr_3/1},
            [FromAOR, ToAOR, FDisplay, FPhone, TPhone, Default, Opts, Flog3, Ftrace3]).

%% "from" top domain. Search global rule in top-level domain of source
fr_3([{_,_,FD}=FromAOR, ToAOR, FDisplay, FPhone, TPhone, Default, Opts, Flog3, Ftrace3]) ->
    fr_call({find_representative_top, FD, 'global', <<"fromtop">>, fun fr_4/1},
            [FromAOR, ToAOR, FDisplay, FPhone, TPhone, Default, Opts, Flog3, Ftrace3]).

%% default
fr_4([_FromAOR, _ToAOR, _FDisplay, FPhone, TPhone, Default, _Opts, Flog3, Ftrace3]) ->
    {IdR,DefNumber,DefDisplay} = Default,
    Flog3(<<"na">>,{FPhone,TPhone},{IdR,DefNumber,DefDisplay}),
    Ftrace3({default, DefNumber, DefDisplay}),
    fr_fin(DefNumber,DefDisplay,Ftrace3).

%% fin
fr_fin(ResNumber, ResDisplay, Ftrace3) ->
    Ftrace3({result, ResNumber, ResDisplay}),
    {ok, ResNumber, ResDisplay}.

%% @private
fr_call({FName,D,KeyT,KeyL,FNext},
        [{_,FU,FD}=_FromAOR, {_,TU,TD}=_ToAOR,
        FDisplay, FPhone, TPhone, _Default, Opts, Flog3, Ftrace3]=P) ->
    Ftrace3({rules, D, KeyT, start}),
    case ?DC:FName(D,{FD,FU,FPhone,?U:unquote_display(FDisplay)},{TD,TU,TPhone},Opts) of
        {ok,{IdR,ResNumber,ResDisplay},Trace} when is_binary(ResNumber) ->
            ResDisplay1 = case ResDisplay of
                              <<>> -> ResDisplay;
                              _ when is_binary(ResDisplay) -> ?U:quote_display(ResDisplay);
                              _ -> FDisplay
                          end,
            Flog3(KeyL,{FPhone,TPhone},{IdR,ResNumber,ResDisplay1}),
            Ftrace3({dc, D, KeyT, ok, Trace}),
            fr_fin(ResNumber,ResDisplay1,Ftrace3);
        % fallback
        {ok,{IdR,ResNumber},Trace} when is_binary(ResNumber) ->
            ResDisplay = FDisplay,
            Flog3(KeyL,{FPhone,TPhone},{IdR,ResNumber,ResDisplay}),
            Ftrace3({dc, D, KeyT, ok, Trace}),
            fr_fin(ResNumber,ResDisplay,Ftrace3);
        % errors
        {ok,_,Trace} ->
            Ftrace3({dc, D, KeyT, not_found, Trace}),
            FNext(P);
        % dc not respond?
        undefined ->
            Ftrace3({rules, D, KeyT, undefined}),
            FNext(P)
    end.
