%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Provides service to find responsible sip server for routing purposes.
%%%      Uses site store to link callid to certain sip server
%%% @todo

-module(r_sip_responsible_servers).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([find_responsible_sg/4,
         get_redirected_sg_nodes/0,

         find_responsible_b2bua/1,
         find_responsible_sip_srvidx/2,
         find_responsible_b2bua_nostore/1,

         delete_stored_responsible/1]).

%% ================================================================================
%% Types
%% ================================================================================

-include("../include/r_sip.hrl").

-define(TIMEOUT_STORE, 18000).
-define(TIMEOUT_RESTORE, 3600).

%% ====================================================================
%% API functions
%% ====================================================================

%% ---------------------------------------------
%% For REDIRECT sipserver
%% ---------------------------------------------

%% Defines responsible sg(proxy) for sip requests from device on RemoteIp
%%   and returns its reachable addr:port. Used by sip-redirect.
%%
-spec find_responsible_sg(nksip:scheme(), binary(), binary(), nksip:nkport()) -> binary().

find_responsible_sg(_Scheme, _User, _Domain, NkPort) ->
    % #418(a) check gates by configuration params (gate functionality could differ, so use role opts of redirect)
    case get_redirected_sg_nodes() of
        [] -> false;
        Gates ->
            % simple randomize
            NodesAddrs = ?EU:randomize(Gates),
            % parse opts
            #nkport{transp=Transp,local_port=LocalPort,remote_ip=RemoteIp}=NkPort,
            CurSite = ?CFG:get_current_site(),
            Proto = get_local_port_proto(Transp,LocalPort),
            % #418(b) take reachable gate's interface for address by proto
            Faddr = fun({Dest,Addrs}) ->
                            case ?ENVCROSS:call_node(Dest,{?U,get_host_addrport,[RemoteIp,Proto]},undefined,1000) of
                                undefined when Addrs==[] -> false;
                                undefined -> lists:nth(1,Addrs);
                                AddrPort when is_binary(AddrPort) -> throw({ok,AddrPort})
                            end end,
            % ping
            Fping = fun({Node,Addrs}) when is_atom(Node) ->
                            case ?ENVPING:ping(Node, {1000,1000}, #{disconnect => true}) of
                                pong -> Faddr({{CurSite,Node},Addrs});
                                pang -> false;
                                timeout -> false
                            end;
                       ({{_S,_N}=Dest,Addrs}) -> Faddr({Dest,Addrs})
                    end,
            % fold randomized gates
            try lists:foldl(fun({Node,Addrs},false) -> Fping({Node,Addrs}) end, false, NodesAddrs)
            catch throw:{ok,AddrPort} -> AddrPort
            end end.

% @private #418(a)
get_redirected_sg_nodes() ->
    F = fun() ->
                CurSite = ?CFG:get_current_site(),
                AppOpts = ?U:get_current_role_opts(),
                case lists:keyfind(<<"gates">>,1,AppOpts) of
                    false -> ?CFG:get_site_sg_nodes();
                    {_,[]} -> ?CFG:get_site_sg_nodes();
                    {_,List} when is_list(List) ->
                        List1 = lists:filtermap(fun(A) -> case catch ?EU:to_int(A) of I when is_integer(I) -> {true,I}; _ -> false end end, List),
                        lists:filtermap(fun({Site,Node,SrvIdx,_Addr}) ->
                                                case lists:member(SrvIdx,List1) of
                                                    true when Site==CurSite -> {true,{Node,[]}}; % addr does not contain port
                                                    true -> {true,{{Site,Node},[]}}; % addr does not contain port
                                                    _ -> false
                                                end end,
                                        ?ENVCFG:get_nodes_containing_role_dyncfg_check_free('sg'))
                end end,
    ?SIPSTORE:lazy_t(redirect_gates, F, {60000,30000,5000}, {true,true}).

% @private #418(b)
get_local_port_proto(Transp,LocalPort) ->
    case ?SIPSTORE:find_u({local_port_proto,LocalPort}) of
        false -> ?EU:to_binary(Transp);
        {_,ProtoKey} -> ProtoKey
    end.

%% ---------------------------------------------
%% For sg server
%% ---------------------------------------------

%% Defines responsible b2bua to handle request
%% Uses call-id hashing,
%% Also can use storage for dialogs to exclude hardware-down dependency
%%
-spec find_responsible_b2bua(CallId::binary()) -> Ip::binary().

find_responsible_b2bua(IncomingCallId) ->
    FunAvailable = fun() -> ?CFG:get_site_b2bua() end,
    find_responsible_b2bua_store(IncomingCallId, FunAvailable).

find_responsible_b2bua_nostore(IncomingCallId) ->
    CallId = IncomingCallId,
    find_responsible(CallId, ?CFG:get_site_b2bua(), false).

%% ---
find_responsible_b2bua_store(IncomingCallId, FunAvailable) ->
    CallId = ?B2BUA_UTILS:extract_callid(IncomingCallId),
    FunFind = fun() -> find_responsible(CallId, FunAvailable(), true) end,
    case get_stored_responsible(CallId) of
        {_,Value,TTLRemain} -> check_from_stored(Value, #{c=>CallId,r=>TTLRemain,f=>FunFind});
        false -> FunFind();
        undefined -> false % cannot call if store not found or not accessible
    end.

%% ---
find_responsible_sip_srvidx(SrvIdx, _IncomingCallId) ->
    case ?CFG:get_sipsrv_by_index(SrvIdx) of
        false -> false;
        {_,[]} -> false;
        {_,[A|_]} -> A;
        {_,A} -> A
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ------------------------
get_stored_responsible(CallId) ->
    ?U:call_store(CallId, fun() -> {get_ttl, [CallId]} end, undefined).
    % false.

set_stored_responsible(CallId, {Node,Addr}, TTL) ->
    ?U:cast_store(CallId, fun() -> {put_ttl, [CallId, callid, {Node,Addr}, TTL]} end),
    Addr.

update_stored_responsible(CallId, {Node,Addr}, TTL) ->
    ?U:cast_store(CallId, fun() -> {put_ttl, [CallId, callid, {Node,Addr}, TTL]} end),
    Addr.

delete_stored_responsible(CallId) ->
    ?U:cast_store(CallId, fun() -> {del, [CallId]} end).

%% ------------------------
find_responsible(CallId, Servers, DoStore) ->
    case Servers of
        [] -> false;
        List when is_list(List) ->
            Pos = erlang:phash2(CallId) rem length(List),
            {_A, [B|_Rest]} = lists:split(Pos, List),
            case B of
                {undefined, _} ->
                    find_responsible(CallId, lists:filter(fun({undefined,_}) -> false;
                                                             (_) -> true
                                                          end, List), DoStore);
                {Node, [Addr|_]} when is_binary(Addr) ->
                    check_node_new({Node, Addr}, #{c=>CallId, s=>Servers, p=>Pos, store=>DoStore})
            end
    end.

%% ------------------------
check_node_new({Node, _Addr}=P, Args) ->
    R = ?ENVPING:ping(Node, {1000,1000}, #{disconnect => true}),
    check_result_1(R, P, Args).

check_result_1('pong', {_Node,Addr}, #{store:=false}) ->
    Addr;
check_result_1('pong', {Node,Addr}, #{c:=CallId}) ->
    set_stored_responsible(CallId, {Node,Addr}, ?TIMEOUT_STORE);
check_result_1(Res, {Node,Addr}, #{c:=CallId, s:=Servers, p:=Pos, store:=DoStore})
  when Res=='pang'; Res=='timeout' ->
    ?LOG("Found responsible server ~s n (~p, ~p)!", [Res,Node,Addr]),
    {P1, [_|P2]} = lists:split(Pos, Servers),
    find_responsible(CallId, P1++P2, DoStore).

%% ------------------------
check_from_stored({Node, _Addr}=P, Args) ->
    R = ?ENVPING:ping(Node, {1000,1000}, #{disconnect => true}),
    check_result_2(R, P, Args);
check_from_stored(_, [CallId, _, FunFind]) ->
    delete_stored_responsible(CallId),
    FunFind().

check_result_2(pong, {_,Addr}, #{r:=TTLRemain}) when TTLRemain > ?TIMEOUT_RESTORE -> Addr;
check_result_2(pong, V, #{c:=CallId}) -> update_stored_responsible(CallId, V, ?TIMEOUT_STORE);
check_result_2(Res, {Node,Addr}, #{f:=FunFind})
  when Res=='pang'; Res=='timeout' ->
    ?LOG("Found responsible server ~s s (~p, ~p)!", [Res,Node,Addr]),
    FunFind().