%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Delegate/Proxy/Facade pattern
%%%        Provides access to domaincenter over user-account functionality
%%% @todo

-module(r_sip_accountstore).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([check_sipuser_exists/2,
         get_sipuser/2, get_sipuser_by_id/2, get_sipuser_fields/3,
         get_displayname/2,
         get_phonenumber/3]).

%% ================================================================================
%% Types
%% ================================================================================

-include("../include/r_sip.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ----------------------------------------
%% Checks if sipuser exists in domain's db
%% Uses dccache
%% ----------------------------------------
check_sipuser_exists(User,Domain)
  when is_binary(User) andalso is_binary(Domain) ->
    case get_sipuser(User,Domain) of
        {ok,_} -> true;
        _ -> false
    end.

%% ----------------------------------------
%% Returns sipuser item (some limited fields for b2bua usage)
%% ----------------------------------------
get_sipuser_by_id(User,Domain) ->
    get_sipuser_fields({ids,User},Domain,[id,login,name,phonenumber,lic,opts]).

get_sipuser(User,Domain) ->
    get_sipuser_fields({keys,User},Domain,[id,login,name,phonenumber,lic,opts]).

%% ----------------------------------------
%% Returns sipuser item (some selected fields)
%% ----------------------------------------
get_sipuser_fields({Mode,User},Domain,Fields) when Mode==ids; Mode==keys ->
    case ?ENVDC:get_object_sticky(Domain,'sipuser',[{Mode,[User]},{fields,Fields}],false,auto) of
        {ok,[Item],_NHC} -> {ok,Item#{domain=>Domain}};
        _ -> undefined
    end.

%% ----------------------------------------
%% Returns sipuser's display name
%% ----------------------------------------
get_displayname(UserLogin, Domain)
  when is_binary(UserLogin) andalso is_binary(Domain) ->
    case get_sipuser(UserLogin, Domain) of
        {ok,Item} -> maps:get(name,Item);
        _ -> <<>>
    end.

%% ----------------------------------------
%% Returns phonenumber of sipuser (updated by opts to globalize for somebody)
%% Uses dccache
%% ----------------------------------------
get_phonenumber(UserLogin, Domain, _Opts)
  when is_binary(UserLogin) andalso is_binary(Domain) ->
    case get_sipuser(UserLogin, Domain) of
        {ok,Item} -> maps:get(phonenumber,Item);
        _ -> undefined
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================
