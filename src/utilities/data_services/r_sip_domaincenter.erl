%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Adapter/Delegate/Proxy/Facade pattern
%%%         Provides access to domain center to make requests over common functionality

-module(r_sip_domaincenter).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([get_root_domains/0,
         get_domain_tree/0,
         check_domain_exists/1,

         get_sipuser_by_id/2,

         find_route_rule/2,

         find_provider_account_by_code/2,
         find_provider_account_by_id/2,
         load_provider_accounts/2,
         find_provider_normalization_rule/7,

         get_active_redirect/2,
         get_redirect_codes/2,

         find_featurecode/1,

         find_insidepbx/1,

         find_representative/4,
         find_representative_top/4,

         find_record_rule/2,

         find_asr_rule/2,

         get_ivrscript/2, get_ivrscript/3,
         check_ivrscript_exists_by_code/2,
         get_huntsip/2, get_huntsip/3,

         sync_border_rules/1]).

%% ================================================================================
%% Types
%% ================================================================================

-include("../include/r_sip.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ==========================================
%% Domains
%% ==========================================

%% ----------------------------------------
%% Returns root domains
%% ----------------------------------------
get_root_domains() ->
    Default = undefined,
    case ?U:call_domainmaster(fun() -> {get_root_domains} end, Default) of
          {error,{not_loaded,_}} -> Default;
        R -> R
    end.

%% ----------------------------------------
%% Returns domain tree
%% ----------------------------------------
get_domain_tree() ->
    Default = undefined,
    case ?U:call_domainmaster(fun() -> {get_domain_tree} end, Default) of
        {error,{not_loaded,_}} -> Default;
        R -> R
    end.

%% ----------------------------------------
%% Check domain exists (directly)
%% ----------------------------------------
check_domain_exists(Domain) when is_binary(Domain) ->
    Default = undefined,
    case ?U:call_domaincenter(Domain, fun() -> {get_domain_name} end, Default) of
        {error,{not_loaded,_}} -> Default;
        R -> R
    end.

%% ==========================================
%% Users
%% ==========================================

%% ----------------------------------------
%% Get id,phonenumber,login of sipuse
%% Uses dccache
%% ----------------------------------------
get_sipuser_by_id(Domain, UserID) ->
    Default = undefined,
    case ?ENVDC:get_object_nosticky(Domain, 'sipuser', [{ids,[UserID]}, {fields,[id,login,phonenumber]}], false, auto) of
        {ok,[Item],_NHC} -> {ok, Item};
        _ -> Default
    end.

%% ==========================================
%% Routes
%% ==========================================

%% ----------------------------------------
%% Finds rule by priority to route call
%% Could be optionally setup to return trace debug info
%% ----------------------------------------
find_route_rule([Domain, Dir, FromAOR, ToAOR, ExtAccCode, RouteCode], Opts) ->
    Default = [],
    case ?U:call_domaincenter(Domain, fun() -> {find_route_rule, Dir, FromAOR, ToAOR, ExtAccCode, RouteCode, Opts} end, Default) of
        {error,{not_loaded,_}} -> Default;
        R -> R
    end.

%% ==========================================
%% Providers
%% ==========================================

%% ----------------------------------------
%% Returns provider account by it's code
%% Uses dccache
%% ----------------------------------------
find_provider_account_by_code(Domain, Code) when is_binary(Domain) ->
    case ?ENVDC:get_object_nosticky(Domain, 'provider', [{keys,[Code]}], false, auto) of
        {ok,[Item],_NCS} -> Item#{clusterdomain => Domain};
        _ -> undefined
    end.

%% ----------------------------------------
%% Returns provider account by it's id
%% Uses dccache
%% ----------------------------------------
find_provider_account_by_id(Domain, Id) when is_binary(Domain) ->
    case ?ENVDC:get_object_nosticky(Domain, 'provider', [{ids,[Id]}], false, auto) of
        {ok,[Item],_NCS} -> Item#{clusterdomain => Domain};
        _ -> undefined
    end.

%% ----------------------------------------
%% Returns provider accounts for loading and starting by current server
%% Uses dccache
%% ----------------------------------------
load_provider_accounts(Domain, SrvIdx) when is_binary(Domain), is_integer(SrvIdx) ->
    case ?ENVDC:get_object_nosticky(Domain, 'provider', [{fldeq,[{enabled,true},{serveridx,SrvIdx}]}], false, auto) of
        {ok,Items,_NCS} -> lists:map(fun(Item) -> Item#{clusterdomain => Domain} end, Items);
        _ -> undefined
    end.

%% ----------------------------------------
%% Find provider callerid modifier rule (from header)
%% ----------------------------------------
find_provider_normalization_rule(Domain, IdP, Dir, From, To, StartFromPriority, Ftrace) ->
    Default = undefined,
    case ?U:call_domaincenter(Domain, fun() -> {find_provider_normalization_rule, IdP, Dir, From, To, StartFromPriority, Ftrace} end, Default) of
        {error,{not_loaded,_}} -> Default;
        R -> R
    end.

%% ==========================================
%% Redirect
%% ==========================================

%% ----------------------------------------
%% Returns active redirect rules for AOR, using masks
%% ----------------------------------------
get_active_redirect(FromAOR, {_,_,D}=ToAOR) ->
    Default = [],
    case ?U:call_domaincenter(D, fun() -> {get_redirect_rules_active, FromAOR, ToAOR} end, Default) of
        {error,{not_loaded,_}} -> Default;
        {ok,Map} -> maps:to_list(Map)
    end.

%% ----------------------------------------
%% Return redirect codes of domain or parent domains.
%% ----------------------------------------
-spec get_redirect_codes(Domain::binary(), Opts::map()) -> Codes::[#{sipcode => non_neg_integer(), mode => binary()}].
%% ----------------------------------------
get_redirect_codes(Domain,Opts) ->
    Default = [],
    case ?U:call_domaincenter(Domain, fun() -> {get_redirect_codes, Opts} end, Default) of
         {error,{not_loaded,_}} -> Default;
         Codes -> Codes
     end.

%% ==========================================
%% FeatureCodes
%% ==========================================

%% ----------------------------------------
%% Returns Feature::map() by called number, extracting max len.
%% ----------------------------------------
find_featurecode({_,_,D}=ToAOR) ->
    Default = [],
    case ?U:call_domaincenter(D, fun() -> {find_featurecode, ToAOR} end, Default) of
        {error,{not_loaded,_}} -> Default;
        R -> R
    end.

%% ==========================================
%% InsidePBX
%% ==========================================

%% ----------------------------------------
%% Returns Number::binary() of existing sipuser, extracting max len from called number like featurecode
%% ----------------------------------------
find_insidepbx({_,_,D}=ToAOR) ->
    Default = [],
    case ?U:call_domaincenter(D, fun() -> {find_insidepbx, ToAOR} end, Default) of
        {error,{not_loaded,_}} -> Default;
        R -> R
    end.

%% ==========================================
%% Representatives
%% ==========================================

%% ----------------------------------------
%% Returns callerid (phonenumber) if it could be explored by domain (cross or inner)
%% Of: {OfDomain,OfUser,OfPhone,OfDisplay}, For: {ForDomain,ForUser,ForPhone}
%% ----------------------------------------
find_representative(D, Of, For, Opts) ->
    Default = undefined,
    case ?U:call_domaincenter(D, fun() -> {find_representative, Of, For, Opts} end, Default) of
        {error,_} -> Default;
        undefined -> Default;
        N -> N
    end.

%% ----------------------------------------
%% Returns callerid (phonenumber) if it could be explored by top-level-domain (global),
%% called after inner could not define callerid (no routes)
%% ----------------------------------------
find_representative_top(D, Of, For, Opts) ->
    Default = undefined,
    case ?U:call_domaincenter(D, fun() -> {find_representative_top, Of, For, Opts} end, Default) of
        {error,_} -> Default;
        undefined -> Default;
        N -> N
    end.

%% ----------------------------------------
%% Returns record rule.
%% ----------------------------------------
-spec find_record_rule({Domain::binary(),Dir::inner|incoming|outgoing,FromNum::binary(),ToNum::binary(),CrossDom::binary()}, Opts::list()) ->
          {ok,{ok,Rule::map()} | false, Trace::[string()]}.
%% ----------------------------------------
find_record_rule({Domain, Dir, FromNum, ToNum, CrossDom}, Opts) ->
    Default = {ok,false,[]},
    case ?U:call_domaincenter(Domain, fun() -> {find_record_rule, Dir, FromNum, ToNum, CrossDom, Opts} end, Default) of
        {error,{not_loaded,_}} -> Default;
        R -> R
    end.

%% ----------------------------------------
%% Returns asr rule.
%% ----------------------------------------
-spec find_asr_rule({Domain::binary(),Dir::inner|incoming|outgoing,FromNum::binary(),ToNum::binary(),CrossDom::binary()}, Opts::list()) ->
    {ok,{ok,Rule::map()} | false, Trace::[string()]}.
%% ----------------------------------------
find_asr_rule({Domain, Dir, FromNum, ToNum, CrossDom}, Opts) ->
    Default = {ok,false,[]},
    case ?U:call_domaincenter(Domain, fun() -> {find_asr_rule, Dir, FromNum, ToNum, CrossDom, Opts} end, Default) of
        {error,{not_loaded,_}} -> Default;
        R -> R
    end.

%% ==========================================
%% IVR scripts
%% ==========================================

%% ----------------------------------------
%% Returns ivrscript or atom (not_found, not_changed, undefined), uses acquire mechanism inside
%% ----------------------------------------
get_ivrscript(D, Code) -> get_ivrscript(D, Code, undefined).
get_ivrscript(D, Code, Checksum)  ->
    Default = undefined,
    case ?U:call_domaincenter(D, fun() -> {get_script, {'ivr',Code}, Checksum} end, Default) of
        {error,{not_loaded,_}} -> Default;
        R -> R
    end.

%% ----------------------------------------
%% Check script exists (by type and code), uses acquire mechanism inside
%% ----------------------------------------
check_ivrscript_exists_by_code(D, Code)  ->
    Default = undefined,
    case ?U:call_domaincenter(D, fun() -> {check_script, {'ivr', Code}} end, Default) of
        {error,{not_loaded,_}} -> Default;
        R -> R
    end.

%% ----------------------------------------
%% Returns hunt or atom by number (not_found, not_changed, undefined), uses update defaults inside
%% ----------------------------------------
get_huntsip(D, SearchKey) -> get_huntsip(D, SearchKey, undefined).
get_huntsip(D, SearchKey, Checksum) ->
    Default = undefined,
    case ?U:call_domaincenter(D, fun() -> {get_huntsip, SearchKey, Checksum} end, Default) of
        {error,{not_loaded,_}} -> Default;
        R -> R
    end.

%% ==========================================
%% Border rules
%% ==========================================

%% ----------------------------------------
%% Get site's sg border rules from masterdomain
%% ----------------------------------------
sync_border_rules(Hashcode) ->
    Default = undefined,
    case ?U:call_domaincenter(?CFG:get_general_domain(), fun() -> {sync_site_border_rules, ?CFG:get_current_site(), Hashcode} end, Default) of
        {error,{not_loaded,_}} -> Default;
        R -> R
    end.
