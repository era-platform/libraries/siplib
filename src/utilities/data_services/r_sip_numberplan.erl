%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Delegate/Proxy/Facade pattern
%%%        Provides access to domaincenter over numberplan functionality
%%% @todo

-module(r_sip_numberplan).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([find_users/2,
         find_pickup_users_by_groups_for/2,
         find_sipgroup_subnumbers/2]).

%% ================================================================================
%% Types
%% ================================================================================

-include("../include/r_sip.hrl").

%% ================================================================================
%% API functions
%% ================================================================================

%% ------------------------
%% returns list of sipusers or items by number to make call
%% ------------------------
-spec find_users(CalledId::binary(), Domain::binary()) -> undefined | [] | [ForkItem] | [[ForkItem]]
    when ForkItem :: SipUserName::binary() | {SipUserName::binary(),Timeout::integer()}.
%% ------------------------
find_users(CalledId, Domain)
  when is_binary(CalledId), is_binary(Domain) ->
    Default = undefined,
    case ?U:call_domaincenter(Domain, fun() -> {get_phonenumber_sipusers, CalledId} end, Default) of
        {error,{not_loaded,_}} -> Default;
        R -> R
    end.


%% ------------------------
%% returns list of sipusers, who is found in groups where selected user is participant.
%% ------------------------
find_pickup_users_by_groups_for(User, Domain)
  when is_binary(User), is_binary(Domain) ->
    Default = undefined,
    case ?U:call_domaincenter(Domain, fun() -> {get_pickup_users_by_sipgroups_for, User} end, Default) of
        {error,{not_loaded,_}} -> Default;
        R -> R
    end.


%% ------------------------
%% returns list of numbers or items by number to make call
%% ------------------------
-spec find_sipgroup_subnumbers(CalledId::binary(), Domain::binary()) -> undefined | [] | [ForkItem] | [[ForkItem]]
    when ForkItem :: Number::binary() | {Number::binary(),Timeout::integer()}.
%% ------------------------
find_sipgroup_subnumbers(CalledId, Domain)
  when is_binary(CalledId), is_binary(Domain) ->
    Default = undefined,
    case ?U:call_domaincenter(Domain, fun() -> {get_group_numbers, CalledId} end, Default) of
        {error,{not_loaded,_}} -> Default;
        R -> R
    end.


%% ================================================================================
%% Internal functions
%% ================================================================================


