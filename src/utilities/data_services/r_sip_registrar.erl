%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Adapter/Delegate/Proxy/Facade pattern
%%%         Provides access to registrar of domain
%%% @todo

-module(r_sip_registrar).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([get/1,
         get_all/1,
         get_by_path/2,
         get_minmax/1,
         put/3,
         del/1,
         del_contacts/2,
         del_all/0]).

%% ================================================================================
%% Types
%% ================================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../_build/default/lib/nksip/include/nksip_registrar.hrl").

-define(REGMOD, r_sip_registrar_modification).

%% ====================================================================
%% API functions
%% ====================================================================

%% ----------------
%% gets all contacts for AOR
%% ----------------
get({_Schema, User, Domain}=_AOR) ->
    AOR1 = {sip, User, Domain},
    ?U:call_registrar(Domain, fun() -> {get, [AOR1]} end, []).

%% ----------------
%% gets all registered AORs from domain
%% ----------------
get_all(Domain) ->
    ?U:call_registrar(Domain, fun() -> {get_all, []} end, []).

%% ----------------
%% gets all registered from domain to path
%% ----------------
-spec get_by_path(Domain::binary(), Path::[{Address::binary(), Port::non_neg_integer()}]) -> RegInfoList::[term()]. % #reg_contact{} from nksip
%% ----------------
get_by_path(Domain, Path) ->
    case ?U:call_registrar(Domain, fun() -> {get_by_path, Path} end, undefined) of
        undefined ->
            ?LOG("ERROR calling registrar:get_by_path"),
            [];
        T -> T
    end.

%% ----------------
%% #191
%% ----------------
get_minmax({_Schema, User, Domain}=_AOR) ->
    case ?ACCOUNTS:get_sipuser(User,Domain) of
        {ok,SU} ->
            SUOpts = maps:get(opts,SU),
            {ok, maps:get(<<"minexpires">>,SUOpts,30), maps:get(<<"maxexpires">>,SUOpts,3600)};
        _ -> undefined
    end.

%% ----------------
%% save contact info of AOR, updates timer
%% ----------------
put(AOR, Contacts, TTL) ->
    Key = ?CHECKLIMIT_K(self()),
    Req = case ?SIPSTORE:find_t(Key) of
              {_,Value} -> ?SIPSTORE:delete_t(Key), Value;
              _ -> undefined
          end,
    put_1(Req, AOR, Contacts, TTL).

%% ----------------
%% @private
%% remove registration
%% ----------------
put_1(_Req, AOR, Contacts, 0) -> put_call(AOR, Contacts, 0);
put_1(Req, {_Schema, User, Domain}=AOR, Contacts, TTL) ->
    case ?ACCOUNTS:get_sipuser(User,Domain) of
        {ok,SipUser} -> put_2(Req, AOR, SipUser, Contacts, TTL);
        _ -> {error, ?InternalError("REG. Account not found or DC connect error")}
    end.
%% lic count exceed
put_2(Req, {_Schema, User, Domain}=_AOR, SipUser, Contacts, TTL) ->
    Lic = maps:get(<<"devices">>,maps:get(lic,SipUser),0),
    AOR1 = {sip, User, Domain},
    F = fun() -> {get, [AOR1]} end,
    case ?U:call_registrar(Domain, F, undefined) of
        undefined -> {error, ?InternalError("REG. Store connect error")};
        RegL when is_list(RegL) ->
            I1 = lists:map(fun(#reg_contact{index=ContactIndex}) -> ContactIndex end, RegL),
            I2 = lists:map(fun(#reg_contact{index=ContactIndex}) -> ContactIndex end, Contacts),
            Merged = lists:umerge(lists:sort(I1),lists:sort(I2)),
            case length(Merged) =< Lic of
                true -> put_call(AOR1, Contacts, TTL);
                false when Req==undefined -> {error, ?Forbidden("REG. Device limit reached")};
                false ->
                    case check_contacts(Req, AOR1, RegL) of
                        false -> {error, ?Forbidden("REG. Device limit reached")};
                        {true,DelRegContact} ->
                            ResultContacts = lists:filter(fun(C) -> case C of DelRegContact -> false; _ -> true end end, Contacts),
                            put_call(AOR1,ResultContacts, TTL)
                    end end end.
%% call registrar
put_call({_Schema, User, Domain}=_AOR, Contacts, TTL) ->
    AOR1 = {sip, User, Domain},
    RContacts = lists:map(fun(#reg_contact{index=ContactIndex}=Contact) -> {ContactIndex, Contact};
                             (Contact) -> Contact
                          end, Contacts),
    F = fun() -> {put, [AOR1, RContacts, TTL]} end,
    case ?U:call_registrar(Domain, F, undefined) of
        ok -> ok;
        undefined -> {error, ?InternalError("REG. Store connect error")};
        _ -> {error, ?InternalError("REG. Store invalid response")}
    end.

%% ----------------
%% deleting whole AOR
%% ----------------
del({_Scheme, User, Domain}=_AOR) ->
    AOR1 = {sip, User, Domain},
    ?U:cast_registrar(Domain, fun() -> {del, [AOR1]} end).

%% ----------------
%% deleting some contacts of AOR
%% ----------------
del_contacts({_Scheme, User, Domain}=_AOR, ContactIndexes) ->
    AOR1 = {sip, User, Domain},
    ?U:cast_registrar(Domain, fun() -> {del_contacts, [AOR1, ContactIndexes]} end).

%% ----------------
%% skip deleting all
%% ----------------
del_all() ->
    ok.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ----------------
%% @private #41 08.02.2017
%% send OPTIONS all reg contacts.
%% ----------------
-spec check_contacts(Req, AOR, Contacts)
      -> {true,DelRegContact} | false
    when
    Req :: #sipmsg{},
    AOR :: {atom(),binary(),binary()},
    Contacts:: [#reg_contact{}],
    DelRegContact :: #reg_contact{}.
%% ----------------
check_contacts(Req, AOR, Contacts) ->
    Key = {<<"check_on_limit">>, AOR},
    case ?SIPSTORE:find_t(Key) of
      {_,Value} -> Value;
      _ ->
          case ?B2BUA:check_limit() of
               false -> ?SIPSTORE:store_t(Key, false, 20000), false;
               _ ->
                   ?REGMOD:send_trying(Req),
                   case ?REGMOD:send_options_to_contacts(AOR, Contacts) of
                       false -> ?SIPSTORE:store_t(Key, false, 20000),false;
                       {ok, #reg_contact{index=ContactIndex}=DelRegContact} ->
                           del_contacts(AOR,[ContactIndex]),
                           {true,DelRegContact}
                   end end end.
