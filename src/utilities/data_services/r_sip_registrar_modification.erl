%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Pavel Abramov, Peter Bukashin <tbotc@yandex.ru>
%%% @doc When license limit on registered devices for account is reached,
%%%      After new device attempts to register then server first attempts to check already registered devices by sending OPTIONS.
%%%      If anyone doesn't respond, then server revokes its registration and permit new device to register.

-module(r_sip_registrar_modification).
-author(['Pavel Abramov','Peter Bukashin <tbotc@yandex.ru>']).

-export([send_options_to_contacts/2, send_trying/1, send_options_to_contact/4, send_options/3]).

%% ================================================================================
%% Types
%% ================================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_b2b.hrl").
-include("../_build/default/lib/nksip/include/nksip_registrar.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% Отправляем запросы OPTIONS всем рег контактам, ждем 2 секунды,
%% возвращаем первого неответевшего.
-spec send_options_to_contacts(AOR, RegContacts)
      -> false | {ok, RegContact}
    when
    AOR :: {atom(), binary(), binary()},
    RegContacts :: [RegContact],
    RegContact :: #reg_contact{}.
send_options_to_contacts(AOR, RegContacts) ->
    Ref = erlang:make_ref(),
    F = fun(RegContact) ->
                Pid = erlang:spawn(?MODULE, send_options_to_contact, [AOR, RegContact, Ref, self()]),
                {Pid, RegContact} end,
    Pids = lists:map(F, RegContacts),
    Result = loop(Pids, Ref, calendar:datetime_to_gregorian_seconds(calendar:local_time())),
    case Result of
        [] -> false;
        [{_, Contact}|_] -> {ok, Contact}
    end.

%% --------------------------------------------------------------------
% Отправляем 100 Trying по запросу Req.
-spec send_trying(Req) -> ok when Req :: any().
send_trying(Req) ->
    {Resp100, SendOpts} = nksip_reply:reply(Req, 100),
    nksip_call_uas_transp:send_response(Resp100, SendOpts),
    ok.

%% ====================================================================
%% Worker functions
%% ====================================================================

%% Master worker -- процесс уровня RegContact
send_options_to_contact(AOR, RegContact, Ref, Pid) ->
    Ref2 = erlang:make_ref(),
    Uris = ?CALLEE:prepare_routed_uris_on_reg_contacts([RegContact]),
    F = fun(Uri) -> erlang:spawn(?MODULE, send_options, [{AOR,Uri}, Ref2, self()]) end,
    lists:foreach(F, Uris),
    receive
        {ok, _, Ref2} ->
            case erlang:is_process_alive(Pid) of
                true -> Pid ! {ok, self(), Ref};
                _ -> ok
            end;
        {exit, Pid, Ref} -> ok
    end.

%% Slave worker -- процесс уровня Uri
send_options({{_S,U,_D}=AOR,Uri}, Ref, Pid) ->
    UriTo = ?U:make_uri(AOR),
    Contact = ?U:clear_uri(Uri#uri{user=U}),
    UriFrom = Contact,

    Opt = [{from,UriFrom},
           {to,UriTo},
           {cseq_num, 1},
           {contact,Contact},
           server],
    case nksip_uac:options(?SIPAPP, Uri, Opt) of
        {error, _} -> ok;
        _R ->
            case erlang:is_process_alive(Pid) of
                true -> Pid ! {ok, self(), Ref};
                _ -> ok
            end
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
%% тут ожидает ответов процесс регистрара.
loop([], _, _) -> [];
loop(Pids, Ref, TimeStart) ->
    Now = calendar:datetime_to_gregorian_seconds(calendar:local_time()),
    Timer = case (Now-TimeStart)*1000 of 0 -> 1999; Res -> Res end,
    loop(Pids, Ref, TimeStart, Timer).
loop(Pids, Ref, _, Timer) when Timer >= 2000 ->
    lists:foreach(fun({Pid,_}) -> Pid ! {exit, self(), Ref} end, Pids),
    Pids;
loop(Pids, Ref, TimeStart, Timer) ->
    receive
        {ok, Pid, Ref} ->
            case lists:keytake(Pid, 1, Pids) of
                false -> loop(Pids, Ref, TimeStart);
                {value, _, Pids2} -> loop(Pids2, Ref, TimeStart)
            end;
        _M -> loop(Pids, Ref, TimeStart)
    after
        Timer -> loop(Pids, Ref, TimeStart)
    end.


