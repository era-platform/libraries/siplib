%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 09.02.2022
%%% @doc

-module(era_sip_alg).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([setup/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

setup([_|_]=SipAlgAddrs) ->
    Insiders = lists:map(fun(Addr) -> {ok,IpAddr} = inet:parse_address(?EU:to_list(Addr)), IpAddr end, ?CFG:get_all_sipserver_addrs()),
    {Grays,Whites} = lists:unzip(SipAlgAddrs),
    ?LOG('$info', "SIP ALG setup: ~120tp~n\tInsiders: ~120tp", [SipAlgAddrs,Insiders]),
    %
    FunRecv = fun(RemoteIp,Msg) ->
                    case lists:member(RemoteIp,Insiders) of
                        true -> Msg;
                        false -> do_map_msg(Msg,lists:zip(Whites,Grays))
                    end end,
    FunSend = fun(RemoteIp,Msg) ->
                    case lists:member(RemoteIp,Insiders) of
                        true -> Msg;
                        false -> do_map_msg(Msg,lists:zip(Grays,Whites))
                    end end,
    %
    application:set_env(?APP,{internal,map_recv_message_fun},FunRecv),
    application:set_env(?APP,{internal,map_send_message_fun},FunSend);

setup(_) ->
    ?LOG('$info', "SIP ALG Clear", []),
    application:unset_env(?APP, {internal,map_recv_message_fun}),
    application:unset_env(?APP, {internal,map_send_message_fun}).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
do_map_msg(Msg,[]) -> Msg;
do_map_msg(Msg,MappingList) ->
    case binary:split(Msg,<<"\r\n\r\n">>) of
        [<<>>] -> Msg;
        [<<>>,<<>>] -> Msg;
        [_H] -> replace_ipaddrs(Msg,MappingList);
        [_H,<<>>] -> replace_ipaddrs(Msg,MappingList);
        [H,C] ->
            H1 = replace_ipaddrs(H,MappingList),
            C1 = replace_ipaddrs(C,MappingList),
            H2 = case {size(C), size(C1)} of
                     {CL,CL} -> H1;
                     {CL1,CL2} -> replace_content_length(H1,CL1,CL2)
                 end,
            <<H2/binary,"\r\n\r\n",C1/binary>>
    end.

%% @private
replace_ipaddrs(Msg,MappingList) ->
    lists:foldl(fun({K,V},Acc) -> binary:replace(Acc,K,V,[global]) end, Msg, MappingList).

%% @private
replace_content_length(Headers,_Previous,New) ->
    NewLen = ?EU:to_binary(New),
    re:replace(Headers,<<"(Content-Length:\\s)(\\d+)">>,<<"\\g{1}",NewLen/binary>>,[caseless,{return,binary}]).