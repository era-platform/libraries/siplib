%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 29.04.2016
%%% @doc

-module(r_sip_do_filter).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([check_block_response_malware/3, check_block_response_malware/1,
         check_route_inside/1,
         auth_failed/2,
         rpc_auth_failed/2,
         transport_filter/3,
         consider_failed_request_from/1]).

%% ==========================================================================
%% Types
%% ==========================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_headers.hrl").

-include("../include/r_sip_nk.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ----------------------------------------
%% Check master-domain setting to block sip error answers
%% ----------------------------------------
check_block_response_malware(invalid_address, _Req, Reply) -> check_block_response_malware(Reply);
check_block_response_malware(unknown_account, _Req, Reply) -> check_block_response_malware(Reply);
check_block_response_malware(no_dialog, _Req, Reply) -> Reply;
check_block_response_malware(method_not_allowed, _Req, Reply) -> Reply;
check_block_response_malware(no_sdp, _Req, Reply) -> Reply;
check_block_response_malware(_, _Req, Reply) -> Reply.

%% ----------------------------------------
%% Check master-domain setting to block sip error answers
%% ----------------------------------------
check_block_response_malware(Reply) ->
    {ok,[ExtSett],_} = ?ENVDC:get_object_sticky(?EU:to_binary(?CFG:get_general_domain()),settings,[{keys,[<<"ext">>]}],false,auto),
    case maps:get(<<"block_response_malware">>,maps:get(value,ExtSett,#{}),undefined) of
        V when V==true; V==1; V==<<"true">>; V==<<"TRUE">> -> block;
        _ -> Reply
    end.

%% ----------------------------------------
%% Called on b2bua when authentication failed
%% ----------------------------------------
-spec auth_failed(Req::#sipmsg{}, Call::#call{}) -> ok.
%% ----------------------------------------
auth_failed(Req, _Call) ->
    #sipmsg{call_id=CallId, cseq={_,_Method}=CSeq}=Req,
    case nksip_request:header(?OwnerHeaderLow, Req) of
        {_,[<<"rGK-", SrvTextCode:24/bitstring>>|_]} ->
            SrvIdx = ?EU:parse_textcode_to_index(SrvTextCode),
            Request = {?MODULE,rpc_auth_failed,[CallId,CSeq]},
            case ?CFG:get_sipserver_by_index(SrvIdx) of
                undefined -> ok;
                {_Site, _Node}=Dest ->
                    ?ENVCROSS:call_node(Dest, Request, ok),
                    ok
            end;
        _ -> ok
    end.

%% ----------------------------------------
% rpc call to sg on auth failed
%% ----------------------------------------
-spec rpc_auth_failed(CallId::binary(), {CSeq::integer(), Method::atom()}) -> ok.
%% ----------------------------------------
rpc_auth_failed(CallId, CSeq) ->
    ?ENV:set_group_leader(),
    Role = application:get_env(?APP,role),
    RoleModule = ?APP:get_module_by_role(Role),
    case nksip_call_srv:find_call(RoleModule,CallId) of
        undefined -> ok;
        Pid when is_pid(Pid) ->
            {Trans,_Forks,_Dialogs} = nksip_call_srv:get_data(Pid),
            NkPort = lists:foldl(fun(#trans{class=uas, request=#sipmsg{cseq=CSeq1, nkport=NkPort}}, undefined) when CSeq1==CSeq -> NkPort;
                                    (_,Acc) -> Acc
                                  end, undefined, Trans),
            case NkPort of
                #nkport{remote_ip={_,_,_,_}=RemoteIp} ->
                    consider_failed_request_from(RemoteIp);
                _ ->
                    ok
            end,
            ok
    end.

%% ----------------------------------------------
%% when request from remote ip fails - count it and ban ip address if too much fails per interval
%% ----------------------------------------
consider_failed_request_from(RemoteIp) ->
    StoreCount = 100,
    %{CheckSec,CheckCount,BanSec} = {10,3,20},
    {CheckSec,CheckCount,BanSec} = {30,55,1200}, % {5,10,180}, {60,30,300},
    NowGS = ?EU:current_gregsecond(),
    FunStore = fun(undefined) -> [NowGS];
                  (L) when length(L)<StoreCount -> [NowGS|L];
                  (L) when length(L)==StoreCount -> [NowGS|lists:droplast(L)];
                  (L) -> {L1,_} = lists:split(StoreCount-1,L), [NowGS|L1]
               end,
    FunReply = fun(undefined) -> false;
                  (L) when length(L) < CheckCount-1 -> false;
                  (L) -> lists:nth(CheckCount-1,L) > NowGS-CheckSec
               end,
    case ?FILTER_STORE:func_t({failed_request,RemoteIp}, 3600000, FunStore, FunReply) of
        false -> ok;
        true -> ?FILTER_SRV:ban(RemoteIp, BanSec, [])
    end.

%% ----------------------------------------
%% Called on sg on request to inside (on sip_route, uas transaction already created, message parsed)
%%  every request check static rules (white and black), then dynamic rules
%%  every static check applied to address/mask, useragent, from:user/domain
%% ----------------------------------------
-spec check_route_inside(Req::#sipmsg{}) -> ok | block | {reply, Reply::{SipCode::integer(), Opts::list()}}.
%% ----------------------------------------
check_route_inside(#sipmsg{cseq={_,Method}}=Req)
  when Method=='REGISTER'; Method=='SUBSCRIBE'; Method=='PUBLISH'; Method=='INVITE' ->
    Opts = #{remoteip => get_remoteip(Req),
             user => get_user(Req),
             domain => get_domain(Req),
             useragent => get_useragent(Req)},
    check_filters(Req, Opts);
check_route_inside(Req) ->
    Opts = #{remoteip => get_remoteip(Req)},
    check_filters(Req, Opts).

%% @private
check_filters(Req, Opts) ->
    case ?FILTER_SRV:check_request(Opts) of
        allow -> ok;
        deny -> {reply, ?Forbidden(<<"Gate. Denied by border filter">>)};
        drop ->
            ?LOGGING:banned(Req#sipmsg.nkport, "BANNED by filter"),
            block
    end.

%% ----------------------------------------
%% Call on nksip_router got new binary message
%%  when nk transport got packet. aprior filter by address (no parse message yet, no create transaction, ...)
%% ----------------------------------------
-spec transport_filter(dynamic|internal, {recv, NKPort::#nkport{}, Packet::binary()}, AppId::atom()) -> ok | block.
%% ----------------------------------------

%% on border gate, by dynamic ban list
transport_filter(dynamic, {recv, [Transp, _Packet]}=_Query, _AppId) ->
    #nkport{remote_ip=Ip}=Transp,
    % r_sip_filter_srv:ban({192,168,0,149}, 30, []).
    case ?FILTER_SRV:check_transport_dynamic(Ip) of
        'allow' -> ok;
        'deny' -> ok;
        'drop' ->
            ?LOGGING:banned(Transp, "BANNED by border"),
            block
    end;

%% on internal server, allowed only cluster ips (no parse message yet, no create transaction, ...)
transport_filter(internal, {recv, [Transp, _Packet]}=_Query, _AppId) ->
    #nkport{remote_ip=Ip}=Transp,
    case lists:member(?EU:to_binary(inet:ntoa(Ip)), ?CFG:get_all_sipserver_addrs()) of
        true -> ok;
        false ->
            ?LOGGING:banned(Transp, "BANNED by internal"),
            block
    end.

%% ----------------------------------------
%% @private
%% checking frequency of incoming requests to serve only under overhead
%% ----------------------------------------
-compile([{nowarn_unused_function, [{check_frequency,1}]}]).
%% ----------------------------------------
check_frequency(#nkport{remote_ip=Ip}=_Transp) ->
    % skip insider addrs
     % lists:flatten([AddrPort1 || {_,AddrPort1} <- ?CFG:get_all_b2bua()]
    % count and store outside requests by last second,
    % check and drop if overhead
    NowGS = ?EU:current_gregsecond(),
    FunStore = fun(undefined) -> 1; (N) -> N+1 end,
    FunReply = fun(undefined) -> 0; (N) -> N end,
    case ?STAT_STORE:func_t({Ip,NowGS}, 3000, FunStore, FunReply) of
        _ -> ok
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

% @private
%% get properties of request

% return <<"peter321">>
get_user(#sipmsg{from={#uri{user=FromUser},_}}=_Req) -> FromUser.

% return <<"test.rootdomain.ru">>
get_domain(#sipmsg{from={#uri{domain=FromDomain},_}}=_Req) -> FromDomain.

% return <<"Yealink SIP-T20P 9.72.14.6">>
get_useragent(#sipmsg{}=Req) ->
    case nksip_request:header("user-agent", Req) of
        {ok,[]} -> <<>>; % ok; Abramov 29.08.2017 r_sip_filter_srv:check_rule_option down on re:run fun (315).
        {ok,[H|_]} -> H
    end.

% return {192,168,0,12}
get_remoteip(#sipmsg{nkport=#nkport{remote_ip=TrAddr}}=_Req) -> TrAddr.



