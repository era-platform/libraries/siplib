%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 6.07.2017
%%% @doc translator of gen_fsm into gen_statem

-module(r_sip_fsm_translator).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([start_link/3,
         send_event/2,
         sync_send_event/2, sync_send_event/3,
         send_all_state_event/2,
         sync_send_all_state_event/2, sync_send_all_state_event/3]).

-export([callback_mode/1, handle_event/5]).

%% ====================================================================
%% Define
%% ====================================================================


%% ====================================================================
%% API functions
%% ====================================================================

start_link(Module,Args,Opts) ->
    %gen_fsm:start_link(Module,Args,Opts).
    gen_statem:start_link(Module,Args,Opts).

send_event(FsmRef,Event) ->
    %gen_fsm:send_event(FsmRef,Event).
    gen_statem:cast(FsmRef,Event).

sync_send_event(FsmRef,Event) ->
    %gen_fsm:send_event(FsmRef,Event).
    gen_statem:call(FsmRef,Event).

sync_send_event(FsmRef,Event,Timeout) ->
    %gen_fsm:send_event(FsmRef,Event).
    gen_statem:call(FsmRef,Event,Timeout).

send_all_state_event(FsmRef,Event) ->
    %gen_fsm:send_all_state_event(FsmRef,Event).
    gen_statem:cast(FsmRef,{all_state,Event}).

sync_send_all_state_event(FsmRef,Event) ->
    %gen_fsm:sync_send_all_state_event(FsmRef,Event).
    gen_statem:call(FsmRef,{all_state,Event}).

sync_send_all_state_event(FsmRef,Event,Timeout) ->
    %gen_fsm:sync_send_all_state_event(FsmRef,Event).
    gen_statem:call(FsmRef,{all_state,Event},Timeout).

%% -------------------
%% callbacks
%% -------------------

callback_mode(_Module) -> [handle_event_function].


handle_event(Module, {call,From}, {all_state,EventContent}, StateName, StateData) ->
    case Module:handle_sync_event(EventContent, From, StateName, StateData) of
        {reply,Reply,StateName,D1} -> {keep_state,D1,[{reply,From,Reply}]};
        {reply,Reply,S1,D1} -> {next_state,S1,D1,[{reply,From,Reply}]};
        {noreply,S1,D1} -> {next_state,S1,D1};
        continue -> continue
    end;

handle_event(Module, {call,From}, EventContent, StateName, StateData) ->
    case Module:StateName(EventContent, From, StateData) of
        {reply,Reply,StateName,D1} -> {keep_state,D1,[{reply,From,Reply}]};
        {reply,Reply,S1,D1} -> {next_state,S1,D1,[{reply,From,Reply}]};
        {noreply,S1,D1} -> {next_state,S1,D1};
        continue -> continue
    end;

handle_event(Module, cast, {all_state,EventContent}, StateName, StateData) ->
    case Module:handle_event(EventContent, StateName, StateData) of
        {next_state,StateName,D1} -> {keep_state,D1};
        {next_state,S1,D1} -> {next_state,S1,D1};
        {noreply,S1,D1} -> {next_state,S1,D1};
        stop -> stop;
        continue -> continue
    end;

handle_event(Module, cast, EventContent, StateName, StateData) ->
    case Module:StateName(EventContent,StateData) of
        {next_state,StateName,D1} -> {keep_state,D1};
        {next_state,S1,D1} -> {next_state,S1,D1};
        {noreply,S1,D1} -> {next_state,S1,D1};
        stop -> stop;
        continue -> continue
    end;

handle_event(Module, info, EventContent, StateName, StateData) ->
    case Module:handle_info(EventContent, StateName, StateData) of
        {next_state,StateName,D1} -> {keep_state,D1};
        {next_state,StateName,D1,hibernate} -> {keep_state,D1};
        {next_state,S1,D1} -> {next_state,S1,D1};
        {next_state,S1,D1,hibernate} -> {next_state,S1,D1};
        {noreply,S1,D1} -> {next_state,S1,D1};
        {noreply,S1,D1,hibernate} -> {next_state,S1,D1};
        stop -> stop;
        continue -> continue
    end;

%handle_event(Module, timeout, EventContent, StateName, StateData) ->
%handle_event(Module, state_timeout, EventContent, StateName, StateData) ->
%handle_event(Module, internal, EventContent, StateName, StateData) ->

handle_event(_,_,_,_,_) -> keep_state_and_data.

%% ====================================================================
%% Internal functions
%% ====================================================================



