%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 27.03.2017
%%% @doc NoReg account service

-module(r_sip_noreg_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-export([find/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").

-record(state, {ets_devices, ets_domains, timerref}).
-record(dstate, {domain, hc, sipusers}).
-record(item, {id, login, hc, sipuser}).

-define(RefreshTimeout, 10000).
-define(RefreshMsg, {refresh}).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    gen_server:start_link(?MODULE, Opts, []).

% -------------------
find(undefined) -> false;
find({_Transp,_Ip,_Port}=RcvFrom)
  when is_atom(_Transp), is_tuple(_Ip), is_integer(_Port) ->
    case ets:lookup(?NOREG,RcvFrom) of
        [] -> false;
        [{_,L}] -> {ok,L}
    end.

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) ->
    [CmnETS,DomETS] = ?EU:extract_required_props([cmnets,domets],Opts),
    State = #state{ets_devices=CmnETS,
                   ets_domains=DomETS,
                   timerref=erlang:send_after(0,self(),?RefreshMsg)},
    ?OUT("B2BUA NoReg Cache: srv inited"),
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

handle_call(_Request, _From, State) ->
    {reply, undefined, State}.

%% ------------------------------
%% Cast
%% ------------------------------

handle_cast(_Request, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

% refresh
handle_info(?RefreshMsg, State) ->
    State1 = refresh(State),
    State2 = State1#state{timerref=erlang:send_after(?RefreshTimeout,self(),?RefreshMsg)},
    {noreply, State2};

% other
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

refresh(#state{ets_domains=DomETS}=State) ->
    case ?ENVCFGU:get_domain_tree([{undef,true},{mode,isowner}]) of
        undefined -> State;
        DomainsAll ->
            SiteDomains = lists:map(fun({D,_}) -> D end, lists:filter(fun({_,X}) -> X end, DomainsAll)),
            SiteDomains1 = lists:delete(?EU:to_binary(?CFG:get_general_domain()), SiteDomains),
            % delete excluded domains
            ToDel = lists:map(fun({V,_}) -> V end, ets:tab2list(DomETS)) -- SiteDomains1,
            lists:foreach(fun(D) -> clear_domain(D,State) end, ToDel),
            % refresh domains
            lists:foldl(fun(D,Acc) -> refresh_domain(D,Acc) end, State, SiteDomains1)
    end.

% @private
clear_domain(D,#state{ets_domains=DomETS}=State) ->
    case ets:lookup(DomETS,D) of
        [] -> ok;
        [{_,#dstate{sipusers=ETS}=DState}] ->
            F = fun({_,#item{}=PItem}, {AccDState,AccState}) ->
                        do_delete_item(D,PItem,AccDState,AccState)
                end,
            {_DState1,_State1} = ets:foldl(F, {DState,State}, ETS),
            ets:delete(DomETS,D)
    end.

% @private
refresh_domain(D,#state{ets_domains=DomETS}=State) ->
    case ets:lookup(DomETS, D) of
        [] ->
            DS = #dstate{domain=D, sipusers=ets:new(sipusers,[public,set]), hc=-1},
            refresh_domain_1(D, DS, State);
        [{_,#dstate{}=DS}] ->
            refresh_domain_1(D, DS, State)
    end.
%
refresh_domain_1(D,#dstate{hc=HC}=DState,State) ->
    case ?ENVDC:get_object_nosticky(D,'sipuser',['item_hc',
                                                  {'fields',[id,item_hc]},
                                                  {'fldeq',[{reg,false}]}],false,HC) of
        {ok,ItemsX,HC1} -> state(update_stamps(D,{HC1},refresh_domain_2(D,ItemsX,DState,State)));
        _ -> State
    end.
%
refresh_domain_2(D,ItemsX,DState,State) ->
    {DState1,State1} = do_delete(D,ItemsX,DState,State),
    {_DState2,_State2} = do_update(D,ItemsX,DState1,State1).

% @private
update_stamps(D,{HC},{DState,#state{ets_domains=DomETS}=State}) ->
    ets:insert(DomETS,{D, DState#dstate{hc=HC}}),
    {DState,State}.

% @private
state({_DState,State}) -> State.

% --------
% @private
do_delete(D, ItemsX, #dstate{sipusers=ETS}=DState, State) ->
    P = ordsets:from_list(lists:map(fun({Id,_}) -> Id end, ets:tab2list(ETS))),
    N = ordsets:from_list(lists:map(fun(Item) -> maps:get(id,Item) end, ItemsX)),
    ToDel = ordsets:subtract(P, N),
    lists:foldl(fun(Id,{AccDState,AccState}) ->
                        [{_,#item{}=PItem}] = ets:lookup(ETS,Id),
                        do_delete_item(D,PItem,AccDState,AccState)
                end, {DState,State}, ToDel).
%
do_delete_item(D, #item{id=Id,sipuser=SU}=PItem, #dstate{sipusers=ETS}=DState, State) ->
    Devices = maps:get(devices,SU,[]),
    do_delete_devices(D, Devices, PItem, DState, State),
    ets:delete(ETS, Id),
    {DState,State}.

%
do_delete_devices(Domain, Devices, #item{login=Login}, DState, State) ->
    State1 = lists:foldl(fun(Device, AccState) ->
                                 delete_common(Device, {Login,Domain}, AccState)
                         end, State, Devices),
    {DState,State1}.

% --------
% @private
do_update(D, ItemsX, #dstate{sipusers=ETS}=DState, State) ->
    F = fun(ItemX,{AccM,AccDState,AccState}) ->
                [Id,IHC] = ?EU:maps_get([id,item_hc],ItemX),
                case ets:lookup(ETS,Id) of
                    [] -> {maps:put(Id,0,AccM),AccDState,AccState};
                    [{_,#item{hc=IHC}}] -> {AccM,AccDState,AccState};
                    [{_,#item{sipuser=Item}=PItem}] ->
                        ToDel = maps:get(devices,Item,[]) -- maps:get(devices,ItemX,[]),
                        {AccDState1,AccState1} = do_delete_devices(D, ToDel, PItem, AccDState, AccState),
                        {maps:put(Id,1,AccM),AccDState1,AccState1}
                end end,
    {IdsMap,DState1,State1} = lists:foldl(F, {maps:new(),DState,State}, ItemsX),
    do_update_items(D, IdsMap, DState1, State1).

% @private
do_update_items(D, IdsMap, #dstate{sipusers=ETS}=DState, #state{}=State) ->
    case ?ENVDC:get_object_nosticky(D,'sipuser',['item_hc',
                                                  {ids,IdsMap},
                                                  {'fldeq',[{reg,false}]}], false, auto) of
        {ok,ItemsX,_HC1} ->
            lists:foldl(fun(SU,{AccDState,AccState}=Acc) ->
                                [Id,Login,IHC] = ?EU:maps_get([id,login,item_hc],SU),
                                case ets:lookup(ETS,Id) of
                                    [] -> do_create_item({Id,Login,D,IHC,SU},AccDState,AccState);
                                    [{_,#item{login=Login}=PItem}] ->
                                        do_update_item(PItem,{Id,Login,D,IHC,SU},AccDState,AccState), Acc;
                                    [{_,#item{sipuser=OldSU}=PItem}] ->
                                        OldDevices = maps:get(devices,OldSU,[]),
                                        {AccDState1,AccState1} = do_delete_devices(D,OldDevices,PItem,AccDState,AccState),
                                        do_update_item(PItem,{Id,Login,D,IHC,SU},AccDState,AccState),
                                        {AccDState1,AccState1}
                                  end end, {DState,State}, ItemsX);
        _ -> State
    end.

% @private
do_create_item(P,DState,State) ->
    do_update_item(#item{},P,DState,State).

% @private
do_update_item(#item{}=PItem,{Id,Login,Domain,IHC,SU},#dstate{sipusers=ETS}=DState,State) ->
    Devices = maps:get(devices,SU,[]),
    State1 = lists:foldl(fun(Device, AccState) ->
                                 append_common(Device, {Login,Domain}, AccState)
                         end, State, Devices),
    ets:insert(ETS,{Id,PItem#item{id=Id,login=Login,hc=IHC,sipuser=SU}}),
    {DState,State1}.

% ---------------------------

% @private
key(Device) ->
    Proto = proto(maps:get(<<"proto">>,Device,udp)),
    Addr = maps:get(<<"device">>,Device,<<>>),
    case binary:split(Addr,<<":">>,[global,trim_all]) of
        [BAddr] -> {Proto,parse_address(BAddr),5060};
        [BAddr,BPort] -> {Proto,parse_address(BAddr),try ?EU:to_int(BPort) catch _:_ -> 5060 end}
    end.
parse_address(BAddr) ->
    case inet:parse_address(?EU:to_list(BAddr)) of
        {ok,Addr} -> Addr;
        _ -> BAddr
    end.

% @private
proto(Proto) ->
    case string:to_lower(?EU:to_list(Proto)) of
        "udp" -> udp;
        "tcp" -> tcp;
        "tls" -> tls;
        _ -> udp
    end.

% @private
delete_common(Device, {_Login,_Domain}=LD, #state{ets_devices=ETS}=State) ->
    Key = key(Device),
    case ets:lookup(ETS,Key) of
        [] -> ok;
        [{_,L}] when is_list(L) ->
            case lists:delete(LD, L) of
                L -> ok;
                [] -> ets:delete(ETS, Key);
                L1 when is_list(L1) ->
                    ets:insert(ETS,{Key,L1})
            end end,
    State.

% @private
append_common(Device, {_Login,_Domain}=LD, #state{ets_devices=ETS}=State) ->
    Key = key(Device),
    case ets:lookup(ETS,Key) of
        [] -> ets:insert(ETS,{Key,[LD]});
        [{_,L}] when is_list(L) ->
            case lists:usort([LD|L]) of
                L -> ok;
                L1 when is_list(L1) ->
                    ets:insert(ETS,{Key,L1})
            end end,
    State.
