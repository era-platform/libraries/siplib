%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc Provides active dialog - call storage

-module(r_sip_store_dlg_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1,
         %
         push_dlg/2,
         pull_dlg/1,
         unlink_dlg/1,
         %
         store_t/3,
         refresh_t/2,
         find_t/1,
         delete_t/1,
         %
         get_dialog_count/0,
         get_callid_count/0,
         %
         get_all_dlgs_keys/0,
         get_all_dlgs_links/0,
         get_all_dlgs_attrs/1,
         find_dlg_by_attr_fun/1,
         %
         get_all_dlgs/0,
         get_all_calls/0,
         debug_clear/0]).

-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("../include/r_sip.hrl").

-record(trec, {
    value,
    ref,
    timerref,
    timeoutgs
  }).

-define(CONTROLTIMER, erlang:send_after(10000, self(), {control_timer})).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    gen_server:start_link({local,?MODULE}, ?MODULE, Opts, []).


push_dlg(CallId, DialogId) ->
    gen_server:cast(?MODULE, {push_dlg, CallId, DialogId}).

pull_dlg(CallId) ->
    gen_server:call(?MODULE, {pull_dlg, CallId}).

unlink_dlg(CallId) ->
    gen_server:cast(?MODULE, {unlink_dlg, CallId}).


get_all_dlgs_keys() ->
    gen_server:call(?MODULE, {get_all_dlgs_keys}).

get_all_dlgs_links() ->
    gen_server:call(?MODULE, {get_all_dlgs_links}).

get_all_dlgs_attrs(Attrs) when is_list(Attrs) ->
    gen_server:call(?MODULE, {get_all_dlgs_attrs,Attrs}).

find_dlg_by_attr_fun(Fun) when is_function(Fun,1) ->
    gen_server:call(?MODULE, {find_dlg_by_attr_fun,Fun}).


get_all_dlgs() ->
    gen_server:call(?MODULE, {get_all_dlgs}).

get_all_calls() ->
    gen_server:call(?MODULE, {get_all_calls}).

debug_clear() ->
    gen_server:cast(?MODULE, {debug_clear}).


store_t(Key, Value, Timeout) ->
    gen_server:cast(?MODULE, {store_t, Key, Value, Timeout}).

refresh_t(Key, Timeout) ->
    gen_server:cast(?MODULE, {refresh_t, Key, Timeout}).

find_t(Key) ->
    gen_server:call(?MODULE, {find_t, Key}).

delete_t(Key) ->
    gen_server:cast(?MODULE, {delete_t, Key}).


get_dialog_count() ->
    gen_server:call(?MODULE, {count_dialogs}).

get_callid_count() ->
    gen_server:call(?MODULE, {count_callids}).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------------------------------------
%% Init
%% ------------------------------------------------------------
init(Opts) ->
    [CallidsETS, DialogsETS, Supv] = ?EU:extract_required_props([ets_c, ets_d, supv], Opts),
    State = #{callids => CallidsETS,
              dialogs => DialogsETS,
              supv => Supv,
              timerref => ?CONTROLTIMER},
    ?OUT(" Dlg Store. inited (~140p)", [Opts]),
    {ok, State}.

%% ------------------------------------------------------------
%% Call
%% ------------------------------------------------------------

%% print state
handle_call({print}, _From, State) ->
    ?OUT(" Dlg Store. State: ~140p", [State]),
    {reply, ok, State};

%% returns count of callid-dialog pairs linked
handle_call({count_callids}, _From, State) ->
    Size = ets:info(maps:get(callids,State), size),
    {reply, Size, State};

%% returns count of callid-dialog pairs linked
handle_call({count_dialogs}, _From, State) ->
    Size = ets:info(maps:get(dialogs,State), size),
    {reply, Size, State};


%% pull link from callid to dlg
handle_call({pull_dlg, CallId}, _From, State) ->
    Reply = case ets:lookup(maps:get(callids,State), CallId) of
                [] -> false;
                [{_CallId, DlgV}] -> DlgV
            end,
    {reply, Reply, State};


%% find timered value
handle_call({find_t, Key}, _From, State) ->
    Reply = case ets:lookup(maps:get(dialogs,State), Key) of
                [{Key,#trec{value=Value}}] -> {Key,Value};
                [] -> false
            end,
    {reply, Reply, State};

%% return all dlg keys
handle_call({get_all_dlgs_keys}, _From, State) ->
    Reply = ets:foldl(fun({K,_},Acc) -> [K|Acc] end, [], maps:get(dialogs,State)),
    {reply, Reply, State};

%% return all dlg links
handle_call({get_all_dlgs_links}, _From, State) ->
    Reply = ets:foldl(fun({_,#trec{value=Value}},Acc) -> [maps:get(link,Value)|Acc] end, [], maps:get(dialogs,State)),
    {reply, Reply, State};

%% return all dlg selected attrs
handle_call({get_all_dlgs_attrs,Attrs}, _From, State) ->
    Reply = ets:foldl(fun({_,#trec{value=Value}},Acc) ->
                              X = lists:map(fun(Attr) -> maps:get(Attr,Value,undefined) end, Attrs),
                              [X|Acc]
                      end, [], maps:get(dialogs,State)),
    {reply, Reply, State};

%% return dialogs, satisfying Fun (return true)
handle_call({find_dlg_by_attr_fun,Fun}, _From, State) ->
    Reply = ets:foldl(fun({_,#trec{value=Value}},Acc) ->
                              case Fun(Value) of
                                  true -> [Value|Acc];
                                  _ -> Acc
                              end end, [], maps:get(dialogs,State)),
    {reply, Reply, State};


%% debug dlgs
handle_call({get_all_dlgs}, _From, State) ->
    Reply = ets:foldl(fun(I,Acc) -> [I|Acc] end, [], maps:get(dialogs,State)),
    {reply, Reply, State};

%% debug calls
handle_call({get_all_calls}, _From, State) ->
    Reply = ets:foldl(fun(I,Acc) -> [I|Acc] end, [], maps:get(callids,State)),
    {reply, Reply, State};

%% other
handle_call(_Request, _From, State) ->
    Reply = ok,
    {reply, Reply, State}.

%% ------------------------------------------------------------
%% Cast
%% ------------------------------------------------------------

%% push link from callid to dlg
handle_cast({push_dlg, CallId, DialogId}, State) ->
    ets:insert(maps:get(callids,State), {CallId, DialogId}),
    {noreply, State};

%% push link from callid to dlg
handle_cast({unlink_dlg, CallId}, State) ->
    ets:delete(maps:get(callids,State), CallId),
    {noreply, State};


%% store timered value
handle_cast({store_t, Key, Value, Timeout}, State) ->
    ETS = maps:get(dialogs,State),
    case ets:lookup(ETS, Key) of
        [{Key,#trec{timerref=TimerRef}}] -> erlang:cancel_timer(TimerRef);
        [] -> ok
    end,
    Ref = make_ref(),
    Rec = #trec{value=Value,
                ref = Ref,
                timerref = erlang:send_after(Timeout, self(), {store_timeout, Key, Ref}),
                timeoutgs = ?EU:current_gregsecond() + (Timeout div 1000) + 1},
    ets:insert(ETS, {Key, Rec}),
    {noreply, State};

%% refresh timered value timeout
handle_cast({refresh_t, Key, Timeout}, State) ->
    ETS = maps:get(dialogs,State),
    case ets:lookup(ETS, Key) of
        [{Key,#trec{value=Value, timerref=TimerRef}}] ->
            erlang:cancel_timer(TimerRef),
            Ref = make_ref(),
            Rec = #trec{value=Value,
                        ref = Ref,
                        timerref = erlang:send_after(Timeout, self(), {store_timeout, Key, Ref}),
                        timeoutgs = ?EU:current_gregsecond() + (Timeout div 1000) + 1},
            ets:insert(ETS, {Key, Rec});
        [] -> ok
    end,
    {noreply, State};

%% delete timered value by key
handle_cast({delete_t, Key}, State) ->
    ETS = maps:get(dialogs,State),
    case ets:lookup(ETS, Key) of
        [{Key,#trec{timerref=TimerRef}}] -> erlang:cancel_timer(TimerRef);
        [] -> ok
    end,
    ets:delete(ETS, Key),
    {noreply, State};

%% clear storage
handle_cast({debug_clear}, State) ->
    [ETS1,ETS2] = ?EU:maps_get([dialogs,callids],State),
    ets:delete_all_objects(ETS1),
    ets:delete_all_objects(ETS2),
    {noreply, State};

%% other
handle_cast(Request, State) ->
    ?LOG(" Dlg Store. handle_cast(~140p)", [Request]),
    {noreply, State}.

%% ------------------------------------------------------------
%% Info
%% ------------------------------------------------------------

%% timeout timered value
handle_info({store_timeout, DialogId, Ref}, State) ->
    [DialogsETS,CallidsETS,GenSupv] = ?EU:maps_get([dialogs,callids,supv],State),
    case ets:lookup(DialogsETS, DialogId) of % {DialogId, #trec{value=#{}}}
        [{_,#trec{ref=Ref,
                  value=Value}}] ->
            {H,M,S} = maps:get(timestart,Value),
            [ACallId|_]=DlgCallIds = maps:get(callids,Value),
            FunClean = maps:get(fun_cleanup,Value),
            Supv = maps:get(supv,Value,GenSupv),
            ?OUT(" Dlg Store. Delete zombie dialog ~120p (at ~2..0B:~2..0B:~2..0B, callid=~120p)", [DialogId, H,M,S, ACallId]),
            lists:foreach(fun(K) -> ets:delete(CallidsETS, K) end, DlgCallIds),
            ets:delete(DialogsETS, DialogId),
            Supv:drop_child(DialogId),
            catch FunClean();
        [] -> ok;
        _ -> ok
    end,
    {noreply, State};

%% control_timer for hang expired
handle_info({control_timer}, State) ->
    [DialogsETS,CallidsETS] = ?EU:maps_get([dialogs,callids],State),    
    Now = ?EU:current_gregsecond(),
    % dialogs
    DelDlgCnt = ets:foldl(fun({Key,#trec{timeoutgs=TimeoutGS}}, Acc) when TimeoutGS+5 < Now -> ets:delete(DialogsETS, Key), Acc+1;
                             (_, Acc) -> Acc end, 0, DialogsETS),
    % calls
    DelCallCnt = ets:foldl(fun({Key,{DlgId,_Pid}}, Acc) ->
                                   case ets:lookup(DialogsETS,DlgId) of
                                       [] -> ets:delete(CallidsETS, Key), Acc+1;
                                       _ -> Acc
                                   end;
                              (_, Acc) -> Acc end, 0, CallidsETS),
    %
    case (DelDlgCnt + DelCallCnt) > 0 of
        true -> ?OUT("Dlg Store. control_timer removed expired items: ~p dialogs, ~p calls", [DelDlgCnt,DelCallCnt]);
        false -> ok
    end,
    {noreply, State#{timerref => ?CONTROLTIMER}};

%% other
handle_info(Info, State) ->
    ?LOG(" Dlg Store. handle_info(~140p)", [Info]),
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.


%% ====================================================================
%% Internal functions
%% ====================================================================


