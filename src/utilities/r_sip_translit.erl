%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Anton Makarov <anton@mastermak.ru>, Peter Bukashin <tbotc@yandex.ru>
%%% @doc @todo Add description to r_sip_translit.


-module(r_sip_translit).

-export([translit_sip_packet/1]).

%% ==========================================================================
%% Defines
%% ==========================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_headers.hrl").

-define(LOW(A),?EU:to_binary(string:to_lower(?EU:to_list(A)))).

-define(HList,[?RemotePartyId,?PAssertedIdentity,?PPreferredIdentity]).

%% ====================================================================
%% API functions
%% ====================================================================
translit_sip_packet(#sipmsg{}=Rex) ->
    do_translit_forward(Rex).

%% ====================================================================
%% Public functions
%% ====================================================================


%% ====================================================================
%% Internal functions
%% ====================================================================
do_translit_forward(#sipmsg{class=Class,to={#uri{disp=ToDisp}=TU,ToTag},from={#uri{disp=FromDisp}=FU,FromTag},contacts=C,headers=H}=Rex) ->
    FunTransH = fun(HNameBin,Acc) ->
                        HNameList = string:to_lower(?EU:to_list(HNameBin)),
                        case get_header_value(HNameList,Rex) of
                            {ok,[]} ->
                                Acc;
                            {ok,[V|_]} ->
                                [#uri{disp=Disp}=HUri] = ?U:parse_uris(V),
%%                                 io:format("INC V:~120p~n",[V]),
                                HUri1 = ?U:unparse_uri(HUri#uri{disp=disp_translit(Disp)}),
%%                                 io:format("OUT V:~120p~n",[TV]),
                                nksip_headers:update(Acc,[{single,?LOW(HNameBin),HUri1}]);
                            _ ->
                                Acc
                        end
                end,
    NewH = lists:foldl(FunTransH,H,?HList),
    case Class of
        {req,_} ->
            NewTo = {TU#uri{disp=disp_translit(ToDisp)},ToTag},
            NewFrom = {FU#uri{disp=disp_translit(FromDisp)},FromTag};
        {resp,_,_} ->
            NewTo = {TU,ToTag},
            NewFrom = {FU,FromTag}
    end,
    NewC = lists:map(fun(#uri{disp=ContDisp}=Contact) -> Contact#uri{disp=disp_translit(ContDisp)} end,C),
    Rex#sipmsg{to=NewTo,from=NewFrom,contacts=NewC,headers=NewH}.

%% ---
get_header_value(HNameList,#sipmsg{class={req, _}}=Rex) ->
    nksip_request:header(HNameList, Rex);
get_header_value(HNameList,#sipmsg{class={resp, _, _}}=Rex) ->
    nksip_response:header(HNameList, Rex).


%% ---
disp_translit(V) ->
    ResV = ?U:unquote_display(V),
    TV = ?EU:str_translit(ResV),
    ?U:quote_display(TV).
