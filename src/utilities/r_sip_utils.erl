%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc
%%% @todo

-module(r_sip_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([]).

-compile([export_all,nowarn_export_all]).

%% ================================================================================
%% Types
%% ================================================================================

-include("../include/r_sip.hrl").
-include("../include/r_sip_headers.hrl").

% -include("../include/r_nksip.hrl").

-define(FROM, r_sip_from).

%% ====================================================================
%% API functions
%% ====================================================================

% =============================================
% Transform types
% =============================================

addr_to_binary(Addr) when is_binary(Addr) -> Addr;
addr_to_binary(Addr) when is_tuple(Addr) -> ?EU:to_binary(inet:ntoa(Addr)).

%% ---------------------------------------------------------
%% start monitor of pid with simple fun (log on shutdown)
%% ----------------------------------------------------------
start_monitor_simple(Pid) -> start_monitor_simple(Pid, "").
start_monitor_simple(Pid, Name) ->
    ?MONITOR:start_monitor(Pid, Name, [fun(normal) -> ok;
                                          (Reason) -> ?OUT("process ~p terminated: ~120p", [Pid,Reason]) end]).

%% =============================================
%% Call to cluster services
%% =============================================

%% ---------------------------------------------------------
%% call globally named domain center
%% ----------------------------------------------------------
call_domainmaster(FunRequest, Default) ->
    ?ENVCALL:call_domainmaster_local(FunRequest, Default).

call_domaincenter(Domain, FunRequest, Default) ->
    ?ENVCALL:call_domaincenter_local(?EU:to_binary(Domain), FunRequest, Default).

%% ---------------------------------------------------------
%% call/cast globally named domain registrar
%% ----------------------------------------------------------
call_registrar(Domain, FunRequest, Default) ->
    ?ENVCALL:call_registrar_local(?EU:to_binary(Domain), FunRequest, Default).

cast_registrar(Domain, FunRequest) ->
    ?ENVCALL:cast_registrar_local(?EU:to_binary(Domain), FunRequest).

%% ---------------------------------------------------------
%% call/cast globally named domain reserver
%% ----------------------------------------------------------
call_reserver(Domain, FunRequest, Default) ->
    ?ENVCALL:call_reserver_local(?EU:to_binary(Domain), FunRequest, Default).

cast_reserver(Domain, FunRequest) ->
    ?ENVCALL:cast_reserver_local(?EU:to_binary(Domain), FunRequest).

%% ---------------------------------------------------------
%% call/cast globally named states-storage
%% ----------------------------------------------------------
call_states_store(Domain, FunRequest, Timeout, Default) ->
    ?ENVCALL:call_states_local(?EU:to_binary(Domain), FunRequest, Timeout, Default).

cast_states_store(Domain, FunRequest, CastOpts) ->
    ?ENVCALL:cast_states_local(?EU:to_binary(Domain), FunRequest, CastOpts).

%% ---------------------------------------------------------
%% call/cast globally named subscription-storage
%% ----------------------------------------------------------
call_subscription_store(Domain, FunRequest, Timeout, Default) ->
    ?ENVCALL:call_subscr_local(?EU:to_binary(Domain), FunRequest, Timeout, Default).

cast_subscription_store(Domain, FunRequest) ->
    ?ENVCALL:cast_subscr_local(?EU:to_binary(Domain), FunRequest).

%% ---------------------------------------------------------
%% call/cast globally named callid-storage
%% ----------------------------------------------------------
call_store(Key, FunRequest, Default) ->
    ?ENVCALL:call_store_local(Key, FunRequest, Default).

cast_store(Key, FunRequest) ->
    ?ENVCALL:cast_store_local(Key, FunRequest).

call_store(Site, Key, FunRequest, Default) ->
    ?ENVCALL:call_store_site(Site, Key, FunRequest, Default).

%% ---------------------------------------------------------
%% call/cast globally named media-controller
%% ----------------------------------------------------------
call_media_controller(MGC, FunRequest, Default) ->
    ?ENVGLOBAL:global_call_server(MGC, FunRequest, Default).

cast_media_controller(MGC, FunRequest) ->
    ?ENVGLOBAL:global_cast_server(MGC, FunRequest).

%% =============================================
%% Local and cluster server addrs
%% =============================================

%% ---------------------------------------------------------
%% returns host interface address (for sdp, contact, route-checks)
%%   RemoteIp:: <<"192.168.0.12">> | <<"192.168.0.12:5060">> | "192.168.0.12" | "192.168.0.12:5060" | {192,168,0,12}
%% TODO: later could be updated to search best interface address for destination
%% TODO: linux should have inet:getifaddrs() and filter there, cause inet:getaddr returns {127,0,1,1}

%get_host_addr() -> get_host_addr({1,1,1,1}).
get_host_addr() ->
    {ok,Addr} = inet:parse_address(?EU:to_list(?CFG:get_my_sipserver_addr())),
    Addr.
get_host_addr(RemoteIp) when is_binary(RemoteIp) -> get_host_addr(?EU:to_list(RemoteIp));
get_host_addr(RemoteIp) when is_list(RemoteIp) ->
    [RemoteIp1|_] = string:tokens(RemoteIp,":"),
    case inet:parse_address(RemoteIp1) of
        {ok,RemoteIp2} -> get_host_addr(RemoteIp2);
        {error,einval} ->
            ResolveKey = {'resolve_cache',RemoteIp1},
            case ?SIPSTORE:find_t(ResolveKey) of
                {_,error} -> get_host_addr();
                {_,RemoteIp2} -> get_host_addr(RemoteIp2);
                false ->
                    case inet:getaddrs(RemoteIp1, inet) of
                        {ok,[RemoteIp2|_]} ->
                            ?SIPSTORE:store_t(ResolveKey,RemoteIp2,600000), % 10 min
                            get_host_addr(RemoteIp2);
                        {error,nxdomain} ->
                            ?SIPSTORE:store_t(ResolveKey,error,600000), % 10 min
                            get_host_addr()
                    end end end;
get_host_addr(RemoteIp) when is_tuple(RemoteIp) ->
    ?BESTIFACE:getbestinterface(RemoteIp).

%% returns local IP4 interfaces, excluding localhost
get_host_interfaces_ipv4() ->
    lists:filtermap(fun({{127,_,_,_},_M}) -> false;
                       ({{_,_,_,_}=A,_M}) -> {true, ?EU:to_binary(inet:ntoa(A))};
                       (_) -> false
                    end, [{A,M} || {_,{A,M}} <- ?BESTIFACE:getinterfaces()]).

get_host_addrs() ->
    ?CFG:get_current_server_addrs().

get_local_port() ->
    {_,LocalPort} = ?SIPSTORE:find_u('local_contact_port'),
    LocalPort.

get_local_port(tls) ->
    case ?SIPSTORE:find_u('local_tls_port') of
        {_,LocalPort} -> LocalPort;
        false -> get_local_port()
    end;
get_local_port(_) -> get_local_port().


get_local_ports() ->
    {_,LocalPorts} = ?SIPSTORE:find_u('local_ports'),
    LocalPorts.

get_host_addrport(RemoteIp,ProtoKey) ->
    Addr = get_host_addr(RemoteIp),
    DefaultPort = ?U:get_opt(<<"udp">>,5060),
    Port = ?U:get_opt(ProtoKey,DefaultPort),
    <<(?EU:to_binary(inet:ntoa(Addr)))/binary,":",(?EU:to_binary(Port))/binary>>.

%% ---------------------------------------------------------
%% returns stun server
%% ----------------------------------------------------------
get_stun_servers() ->
    [<<"sip:stun.sipnet.ru:3478">>, <<"sip:demo.era.ru:5060">>].

%% ---------------------------------------------------------
%% Returns call source
%% ----------------------------------------------------------

%% ----------------------------
-spec get_call_source(From::#uri{}, Req::#sipmsg{}) -> Source::'inner' | 'outer'.
%% ----------------------------
%% check esg header and from domain existence to detect if this call from inside or outside
get_call_source(#uri{domain=FD}, Req) ->
    case nksip_request:header(?ExtAccountHeaderLow, Req) of
        {ok,[_|_]} -> 'outer';
        _ ->
            case ?DC:check_domain_exists(FD) of
                undefined -> 'outer';
                _ -> 'inner'
            end
    end.

%% ---------------------------------------------------------
%% Functions for work with mg with several ifaces
%% ----------------------------------------------------------

%% ---
-spec get_caller_address(Source::'inner'|'outer',Req::#sipmsg{}) -> Address::inet:ip_address() | undefined.
%% ---
get_caller_address('inner',#sipmsg{vias=Vias}) ->
    ?EU:get_by_key(<<"received">>,(lists:last(Vias))#via.opts,undefined);
get_caller_address('outer',#sipmsg{vias=Vias}=Req) ->
    case ?U:parse_ext_account_header(Req) of
        undefined -> ?EU:get_by_key(<<"received">>,(lists:last(Vias))#via.opts,undefined); % same as from inner
        {{_U,_Domain},Opts} ->
            % get ?OwnerHeaderLow without case because this header must be
            {_,[<<"rEG-", SrvTextCode:24/bitstring>>|_]} = nksip_request:header(?OwnerHeaderLow, Req),
            SrvIdx = ?EU:parse_textcode_to_index(SrvTextCode),
            [ExtId,ClusterDomain] = ?EU:parse_required_opts([<<"id">>,<<"cd">>],Opts),
            case ?DC:find_provider_account_by_id(ClusterDomain,ExtId) of
                ExtAccount when is_map(ExtAccount) ->
                    case ?CFG:get_sipserver_by_index(SrvIdx) of
                        undefined ->
                            ?LOG("Aliased MG. ESG by index not found"),
                            undefined;
                        {_EsgSite,_EsgNode}=EsgDest ->
                            case ?ENVCROSS:call_node(EsgDest, {?APP, esg_get_provider_addr, [ExtAccount]}, {error,connect_error}, 1000) of
                                {error,_} ->
                                    ?LOG("Aliased MG. Get provider address error"),
                                    undefined;
                                {ok,Address} -> Address
                            end
                    end;
                _ ->
                    ?LOG("Aliased MG. Ext account for outer call not found"),
                    undefined
            end
    end.

%% ---
-spec get_callee_address(RuleMap::map()) -> {ok,Address::inet:ip_address()} | {error,Reason::binary()}.
%% ---
get_callee_address(RuleMap) when is_map(RuleMap) ->
    get_callee_address(RuleMap,undefined).

%% ---
-spec get_callee_address(RuleMap::map(),DefaultHostAddress::inet:ip_address() | undefined) -> {ok,Address::inet:ip_address()} | {error,Reason::binary()}.
%% ---
get_callee_address(RuleMap,DefaultHost) when is_map(RuleMap) ->
    [Dir0,IsFeature] = ?EU:maps_get_default([{dir,'inside'},{is_feature,false}],RuleMap),
    Dir = case IsFeature of
              true -> 'feature';
              false -> Dir0
          end,
    case get_fork_address(Dir,RuleMap,DefaultHost) of
        {error,_}=Err -> Err;
        {ok,_}=Ok -> Ok
    end.

%% @private
get_fork_address('inside',RuleMap,_) ->
    % reg
    % noreg
    %  -> ruri, route, gate at reginfo
    % -----
    % FIX MULTI-IFACE MG on all cases:
    %   Local registered on same interface - @received, @uri.domain
    %   Local registered on other interface - @received, @uri.domain
    %   Registered by sip proxy - @received (should be added), (uri.domain is client address, could be unaccessible directly, received is proxy address)
    %   Without registration - from @uri.domain (uri.domain is general, received is absent)
    %   Nat - @received (uri.domain is unaccessible)
    %   WebRTC - @received (uri.domain is invalid)
    % -----
    #uri{domain=UriD,headers=Headers} = maps:get(uri,RuleMap),
    Route = ?EU:get_by_key(<<"route">>,Headers,<<>>),
    [#uri{opts=RouteOpts}|_] = parse_uris(Route),
    RecvD = ?EU:get_by_key(<<"received">>,RouteOpts,undefined),
    case RecvD of
        undefined -> {ok,UriD};
        _ -> {ok,RecvD}
    end;
get_fork_address('outside',RuleMap,_) ->
    ExtAcc = maps:get(extaccount,RuleMap),
    SrvIdx = maps:get(serveridx,ExtAcc),
    case ?CFG:get_sipserver_by_index(SrvIdx) of
        undefined -> {error,<<"ESG by index not found">>};
        {_EsgSite,_EsgNode}=EsgDest ->
            case ?ENVCROSS:call_node(EsgDest, {?APP, esg_get_provider_addr, [ExtAcc]}, {error,connect_error}, 1000) of
                {error,_}=Err -> Err;
                {ok,_Address}=Ok -> Ok
            end
    end;
get_fork_address('feature',RuleMap,DefaultHost) ->
    % for feature code return address of featurecode service
    %   it could be local node address as default tgw interface.
    Address = maps:get(feature_rdomain,RuleMap,DefaultHost),
    {ok,Address}.

%% ---
-spec get_mg_alias_for_addr(MgId::binary(),Host::inet:ip_address() | undefined) -> {ok,Address::inet:ip_address()} | {error,Reason::binary()}.
%% ---
get_mg_alias_for_addr(_MgId,undefined) ->
    % undefined address can come from get_caller_address
    {error,<<"Fork address is undefined">>};
get_mg_alias_for_addr(MgId,ForkAddress) ->
    case ?U:parse_mg_devicename(?U:get_mg_devicename(MgId)) of
        undefined -> {error,<<"Parse MGID error">>};
        {Addr,Postfix} ->
            case ?CFG:get_mg_node_by_addr_postfix(Addr,Postfix) of
                undefined ->
                    ?LOG("Aliased MG. get_mg_alias_for_addr mg node by addr and postfix not found"),
                    {error, mg_not_found};
                {_Site,MgNode} ->
                    case ?ENVRPC:call(MgNode, ?APP_MG, get_mg_alias_for_addr, [ForkAddress], 500) of
                        {badrpc,Err} ->
                            ?LOG("Aliased MG. get_mg_alias_for_addr call node error:~120p",[Err]),
                            {error,call_mg_error};
                        {error,_}=Err -> Err;
                        {ok,_}=Result -> Result
                    end
            end
    end.

%% ---
-spec finalize_mg_alias({ok,Alias::binary()} | {error,Reason:: param_not_found | route_not_found | mg_not_found | call_mg_error | binary()}) -> ok.
%% ---
finalize_mg_alias({error,Reason})
  when Reason==param_not_found; Reason==mg_not_found; Reason==call_mg_error ->
    {ok,<<"tgw">>};
finalize_mg_alias({error,route_not_found}) ->
    {error, <<"MG route not found">>};
finalize_mg_alias({error,Reason}) ->
    ?LOG("Aliased MG. finalize_mg_alias error:~120p",[Reason]),
    {error, Reason};
finalize_mg_alias({ok, Alias}) ->
    {ok,Alias}.

%% ---
-spec is_default_alias(MgAlias::binary()) -> boolean().
%% ---
is_default_alias(MgAlias) ->
    ?EU:to_list(MgAlias)=="tgw".

%% =============================================
%% Parse opts
%% =============================================

%% ---------------------------------------------------------
%% parses ports from opts to start cb modules
%% ----------------------------------------------------------
parse_transports(Opts) ->
    Keys = [<<"udp">>,<<"tcp">>,<<"tls">>,<<"ws">>,<<"wss">>],
    [Udp,Tcp,Tls,Ws,Wss]=Vals = ?EU:extract_optional_props(Keys, Opts),
    lists:foreach(fun({_,undefined}) -> ok;
                     ({Key,Val}) -> ?SIPSTORE:store_u({local_port_proto,?EU:to_int(Val)},Key) end, lists:zip(Keys,Vals)),
    %
    Transp1 = case Udp of
                  undefined -> [];
                  _ -> ["<sip:all:"++?EU:to_list(?EU:to_int(Udp))++">"]
              end,
    Transp2 = case Tcp of
                  undefined -> Transp1;
                  _ when Tcp == Udp -> Transp1;
                  _ -> ["<sip:all:"++?EU:to_list(?EU:to_int(Tcp))++";transport=tcp>"|Transp1]
              end,
    Transp3 = case Tls of
                  undefined -> Transp2;
                  _ when Tls == Udp; Tls == Tcp  -> Transp2;
                  _ ->
                      TlsPort = ?EU:to_int(Tls),
                      ?SIPSTORE:store_u('local_tls_port', TlsPort),
                      ["<sip:all:"++?EU:to_list(TlsPort)++";transport=tls>"|Transp2]
              end,
    Transp4 = case Ws of
                  undefined -> Transp3;
                  _ when Ws == Udp; Ws == Tcp; Ws == Tls -> Transp3;
                  _ -> ["<sip:all:"++?EU:to_list(?EU:to_int(Ws))++";transport=ws>"|Transp3]
              end,
    Transp5 = case Wss of
                  undefined -> Transp4;
                  _ when Wss == Udp; Wss == Tcp; Wss == Tls; Wss == Ws -> Transp4;
                  _ -> ["<sip:all:"++?EU:to_list(?EU:to_int(Wss))++";transport=wss>"|Transp4]
              end,
    string:join(lists:reverse(Transp5), ", ").

%% --
parse_ports(Opts) ->
    Ports = ?EU:extract_optional_props([<<"udp">>,<<"tcp">>,<<"tls">>,<<"ws">>,<<"wss">>], Opts),
    Ports1 = lists:filter(fun(undefined) -> false; (_) -> true end, Ports),
    lists:foldl(fun(A,Acc) -> case lists:member(A,Acc) of true -> Acc; false -> [?EU:to_int(A)|Acc] end end, [], Ports1).

%% --
parse_contact_port(Opts) ->
    [Udp,Tcp] = ?EU:extract_optional_props([<<"udp">>,<<"tcp">>], Opts),
    case Tcp of
        undefined ->
            case Udp of
                undefined -> 5060;
                _ -> ?EU:to_int(Udp)
            end;
        _ -> ?EU:to_int(Tcp)
    end.

%% --
parse_sip_alg(Opts) ->
    SipAlgAddrs = ?EU:get_by_key(<<"sip_alg">>, Opts, []),
    case SipAlgAddrs of
        [] -> [];
        [_|_] ->
            FunIsAddr = fun(Addr) ->
                            case inet:parse_address(?EU:to_list(Addr)) of
                                {ok,_} -> true;
                                _ -> false
                            end end,
            FunFilter = fun({Gray,White}) when is_binary(Gray), is_binary(White) ->
                                case lists:all(FunIsAddr, [Gray,White]) of
                                    true -> {true,{Gray,White}};
                                    false -> false
                                end;
                           ([{<<"gray">>,Gray},{<<"white">>,White}]) when is_binary(Gray), is_binary(White) ->
                               case lists:all(FunIsAddr, [Gray,White]) of
                                   true -> {true,{Gray,White}};
                                   false -> false
                               end;
                           (_) -> false
                        end,
            lists:sort(lists:filtermap(FunFilter,SipAlgAddrs));
        _ -> []
    end.

%% --
parse_substitute_domains(Opts) ->
    Subst = ?EU:get_by_key(<<"substitute_domains">>, Opts, []),
    case Subst of
        [] -> [];
        [_|_] ->
            FunIsAddr = fun(Addr) ->
                                case inet:parse_address(?EU:to_list(Addr)) of
                                    {ok,_} -> true;
                                    _ -> false
                                end end,
            FunFilter = fun({Addr,Domain}) when is_binary(Addr), is_binary(Domain) ->
                                case FunIsAddr(Addr) of
                                    true -> {true,{Addr,Domain}};
                                    false -> false
                                end;
                           ([{<<"addr">>,Addr},{<<"domain">>,Domain}]) when is_binary(Addr), is_binary(Domain) ->
                               case FunIsAddr(Addr) of
                                   true -> {true,{Addr,Domain}};
                                   false -> false
                               end;
                           (_) -> false
                        end,
            lists:sort(lists:filtermap(FunFilter,Subst));
        _ -> []
    end.

%% --
%% for TLS uris could be used both rtp(AVP) and srtp(SAVP). Russia restricts using srtp, so use only rtp by default
parse_use_srtp(Opts) -> parse_use_srtp(Opts, false).
parse_use_srtp(Opts, Default) ->
    case lists:keyfind(<<"use_srtp">>,1,Opts) of
        false -> Default;
        {_,B} -> ?EU:to_bool(B)
    end.

%% --
parse_stateless(Opts) -> parse_stateless(Opts,false).
parse_stateless(Opts, Default) ->
    case lists:keyfind(<<"stateless">>,1,Opts) of
        false -> Default;
        {_,B} -> ?EU:to_bool(B)
    end.

%% --
default_cert_dir() ->
    Dir = filename:join(code:priv_dir(era_sip), "ssl"),
    ?Rfilelib:ensure_dir(Dir++"/"),
    Dir.
parse_certdir(Opts) ->
    parse_certdir(Opts, default_cert_dir()).
parse_certdir(Opts, Default) ->
    case lists:keyfind(<<"certdir">>,1,Opts) of
        false -> Default;
        {_,D} -> ?EU:to_list(D)
    end.

%% --
parse_keypass(Opts) ->
    parse_keypass(Opts, "").
parse_keypass(Opts, Default) ->
    case lists:keyfind(<<"keypass">>,1,Opts) of
        false -> Default;
        {_,D} -> ?EU:to_list(D)
    end.

%% --
%% RP-2170
%% --
-spec check_and_filter_ssl_opts(Opts::[{binary(),binary()}]) ->
          {VerificationResult::ok|error, UpOpts::[{binary(),binary()}]}.
%% --
check_and_filter_ssl_opts(Opts) ->
    CertDir = parse_certdir(Opts),
    KeyPass = parse_keypass(Opts),
    Cert = ?ENVCERT:cert_filename(),
    CertKey = ?ENVCERT:key_filename(),
    case ?ENVCERT:verify(?EU:to_binary(CertDir), Cert, CertKey, ?EU:to_binary(KeyPass)) of
        {error,_} -> {error,Opts};
        _OK -> {ok, Opts}
    end.

%% --
check_update_opts(Opts) ->
    Fs = [fun() ->
                  {_,P1} = ?SIPSTORE:find_u('local_contact_port'),
                  P2 = ?U:parse_contact_port(Opts),
                  case P1 == P2 of
                      true -> ok;
                      false -> restart
                  end end,
          fun() ->
                  {_,P1} = ?SIPSTORE:find_u('local_ports'),
                  P2 = ?U:parse_ports(Opts),
                  case P1 == P2 of
                      true -> ok;
                      false -> restart
                  end end
         ],
    lists:foldl(fun(F, ok) -> F();
                   (_, restart) -> restart end, ok, Fs).

%% -- for sg/esg
get_interface_alias(Iface) ->
    AllAliases = get_current_role_opt(<<"aliases">>,[{undefined,undefined}]),
    IpForCompare = case Iface of
                       {_,_,_,_} -> inet:ntoa(Iface);
                       A when is_list(A) -> A;
                       B -> ?EU:to_list(B)
                   end,
    FunGetAlias = fun({CfgAlias,CfgIpAddr},undefined) ->
                          case IpForCompare == ?EU:to_list(CfgIpAddr) of
                              true -> CfgAlias;
                              false -> undefined
                          end;
                     (_,Acc) -> Acc
                  end,
    lists:foldl(FunGetAlias,undefined,AllAliases).

%% -- for sg & esg
% returns list of registered aliases from opts of role. [] if not found
get_aliases() ->
    get_opt(<<"aliases">>,[]).

%% -- for sg & esg & ivr
is_b2bmedia() ->
    get_opt(<<"b2bmedia">>,true).

%% -- for sg
is_translit() ->
    get_opt(<<"translit">>,false).

%% #182 for sg
get_max_udp_size() ->
    get_opt(<<"max_udp_size">>,4000).

%% -- for b2bua
is_cfg_send_reinvite_on_ack() ->
    get_opt(<<"send_reinvite_on_ack">>,false).

is_allow_ac_set() ->
    get_opt(<<"allow_ac_set">>,true).

is_check_by_options_on_limit() ->
    get_opt(<<"check_by_options_on_limit">>,false).

is_record_ivr() ->
    get_opt(<<"record_ivr">>,false).

is_record_conf() ->
    get_opt(<<"record_conf">>,false).

is_record_prompt() ->
    get_opt(<<"record_prompt">>,false).

% #305 (default: 711a, 711u, cn, 729a, telephone-event)
get_payloads(offer,<<"audio">>) ->
    get_opt(<<"payloads_audio_offer">>,[<<"G729/8000">>]);
% #475 (default: empty)
get_payloads(offer,<<"video">>) ->
    get_opt(<<"payloads_video_offer">>,[]).

% #475
is_use_video_transcoding() ->
    get_opt(<<"use_video_transcoding">>,false).

% #315
get_redirect_sipcode(Default) ->
    get_opt(<<"sipcode">>,Default).

get_redirect_multi(Default) ->
    get_opt(<<"multicontact">>,Default).

get_redirect_expires(Default) ->
    get_opt(<<"expires">>,Default).

%% #350 for ivr
is_usemedia() ->
    get_opt(<<"usemedia">>,true).

%% RP-1910
mg_postfix(Default) ->
    case get_opt(<<"mg_postfix">>,Default) of
        <<>> -> Default;
        T -> T
    end.

%% ciphers
get_ciphers() ->
    Default = [],
    get_opt(<<"tls_ciphers">>,Default).

%% -----
%% private utils
%% -----

% @private
% return current sip role options
get_current_role_opts() ->
    {ok,[RoleModule]} = ?ENV:get_env(siprole),
    {ok,AppOpts} = ?APP:get_opts(RoleModule),
    AppOpts.

% @private
% return current sip role option by key (use default value)
get_current_role_opt(Key,Default) when is_binary(Key) ->
    [R] = ?EU:extract_optional_default([Key],get_current_role_opts(),[Default]),
    R;
get_current_role_opt(Keys,Defaults) when is_list(Keys), is_list(Defaults) ->
    ?EU:extract_optional_default(Keys,get_current_role_opts(),Defaults).

% @private
% return function to get current sip role option by key (and default value)
get_current_role_opt_fun(Key,Default) ->
    fun() -> get_current_role_opt(Key,Default) end.

% @private
% return current sip role option by key & default, use automatic cache
get_opt(Key,Default) ->
    ?SIPSTORE:lazy_t({cfg,Key}, get_current_role_opt_fun(Key,Default), {30000,20000,2000}, {true,true}).

%% ----------------------------------------------------------
%% return true if trn enabled
%% ----------------------------------------------------------
is_trn_enabled() ->
    case application:get_env(?APP,?LOGFILETRN,false) of
        true -> true;
        _ -> false
    end.

% =============================================
% Sip Server Index / Code
% =============================================

%% ----------------------------------------------------------
%% Builds 3-sym string code by current B2BUA server index.
%% Supported srvidx up to 238328
%% ----------------------------------------------------------
get_current_srv_textcode() ->
    case ?SIPSTORE:find_u('SIPSRV_index') of
        {_,Value} -> Value;
        false ->
            TextCode = ?U:build_textcode_by_index(?CFG:get_my_sipserver_index(), 3),
            ?SIPSTORE:store_u('SIPSRV_index', TextCode),
            TextCode
    end.

build_textcode_by_index(Idx, Size)
  when is_integer(Idx), is_integer(Size) ->
    String = ?EU:to_list(build_textcode(Idx, <<>>)),
    ?EU:to_binary(enlarge_textcode(String, Size - length(String))).

% @private
enlarge_textcode(List, N) when N =< 0 -> List;
enlarge_textcode(List, Size) ->
    enlarge_textcode(["0"|List], Size-1).

build_textcode(0, Acc) -> Acc;
build_textcode(Idx, Acc) ->
    C = ?EU:to_binary(make_textcode(Idx rem 62)),
    build_textcode(Idx div 62, <<C/bitstring, Acc/bitstring>>).

make_textcode(Idx) when Idx < 10 -> [48 + Idx];
make_textcode(Idx) when Idx < 36 -> [65 + Idx - 10];
make_textcode(Idx) when Idx < 62 -> [97 + Idx - 36].

parse_textcode(Code) when Code >= 48, Code < 48+10 -> Code - 48;
parse_textcode(Code) when Code >= 65, Code < 65+26 -> Code - 65 + 10;
parse_textcode(Code) when Code >= 97, Code < 97+26 -> Code - 97 + 36.

int32_hash(I) -> int_hash(I,32).
int64_hash(I) -> int_hash(I,64).

int_hash(I,64) when is_integer(I), I >= -9223372036854775808, I < 9223372036854775808 -> I;
int_hash(I,64) when is_integer(I) -> I rem 9223372036854775808;
int_hash(I,32) when is_integer(abs(I)), I >= -2147483648, I < 2147483648 -> I;
int_hash(I,32) when is_integer(abs(I)) -> I rem 2147483648;
int_hash(B,Pow) when is_binary(B) -> int_hash(?EU:to_list(B),Pow);
int_hash(S,Pow) when is_list(S) ->
    F = fun F([],Acc) -> Acc;
            F([B|Rest],Acc) -> F(Rest,Acc * 62 + parse_textcode(B))
        end,
    int_hash(F(S,0),Pow).

% =============================================
% URI routines
% =============================================

%% ----------------------------------------------------------
%% Gets phonenumber by opts from DC for local URI
%% ----------------------------------------------------------
get_sipuser_phonenumber(#uri{user=User,domain=Domain}, Opts, Default) ->
    get_sipuser_phonenumber(User, Domain, Opts, Default).

get_sipuser_phonenumber(User, Domain, Opts, Default) ->
    case ?ACCOUNTS:get_phonenumber(User,Domain,Opts) of
        {error,_} -> Default;
        undefined -> Default;
        N -> N
    end.

%% ----------------------------------------------------------
%% Do translit for display to phone
%% ----------------------------------------------------------
do_translit(Name) -> Name.

%% ----------------------------------------------------------
%% Update uri by properties
%% ----------------------------------------------------------
update_uri(Uri, Opts) ->
    Uri0 = case lists:keyfind(scheme,1,Opts) of
               false -> Uri;
               {_,S} -> Uri#uri{scheme=S}
           end,
    Uri1 = case lists:keyfind(disp,1,Opts) of
               false -> Uri0;
               {_,DN} -> Uri0#uri{disp=DN}
           end,
    Uri2 = case lists:keyfind(user,1,Opts) of
               false -> Uri1;
               {_,UN} -> Uri1#uri{user=UN}
           end,
    Uri3 = case lists:keyfind(domain,1,Opts) of
               false -> Uri2;
               {_,D} -> Uri2#uri{domain=D}
           end,
    Uri4 = case lists:keyfind(port,1,Opts) of
               false -> Uri3;
               {_,P} -> Uri3#uri{port=P}
           end,
    Uri5 = case lists:keyfind(ext_opts,1,Opts) of
               false -> Uri4;
               {_,EO} -> Uri4#uri{ext_opts=EO}
           end,
    Uri6 = case lists:keyfind(opts,1,Opts) of
               false -> Uri5;
               {_,O} -> Uri5#uri{opts=O}
           end,
    Uri7 = case lists:keyfind(user_orig,1,Opts) of
               false -> Uri6;
               {_,UO} -> Uri6#uri{user_orig=UO}
           end,
    Uri7.

%% ----------------------------------------------------------
%% Parse URI B2BUA Rule (aor, uri, timeout, extaccount, bl_uri, bl_contact)
%% ----------------------------------------------------------
parse_uri_rule(#{aor:=_, uri:=_, ftimeout:=_ForkTimeout}=P, _) when is_integer(_ForkTimeout) -> P;
parse_uri_rule(#{uri:=#uri{scheme=S,user=U,domain=D}, ftimeout:=_ForkTimeout}=P, _) when is_integer(_ForkTimeout) -> P#{aor=>{S,U,D}};
parse_uri_rule(#{aor:=_, uri:=_}=P, {DefTimeout}) -> P#{ftimeout=>DefTimeout};
parse_uri_rule(#{uri:=#uri{scheme=S,user=U,domain=D}}=P, {DefTimeout}) -> P#{aor=>{S,U,D}, ftimeout=>DefTimeout}.

%% ----------------------------------------------------------
%% Canonizes URI B2BUA Rules
%% ----------------------------------------------------------
canonize_uri_rules(UriRules) ->
    lists:filtermap(fun([]) -> false;
                       (L) when is_list(L) -> true;
                       (E) -> {true,[E]}
                    end, UriRules).

%% ----------------------------------------------------------
%% builds local contact based on uri (later: uri for username, later2: displayname deleted)
%% ----------------------------------------------------------
build_local_contact() ->
    build_local_contact(#uri{scheme=sip}).

build_local_contact(BaseUri) ->
    LocalAddr = ?CFG:get_my_sipserver_addr(),
    LocalPort = ?U:get_local_port(),
    build_local_contact_srv({LocalAddr,LocalPort}, BaseUri).

% RemoteAddr :: <<"192.168.0.12">> | <<"192.168.0.12:5060">> | "192.168.0.12" | "192.168.0.12:5060" | {192,168,0,12}
build_local_contact(RemoteAddr,BaseUri) ->
    LocalAddr = ?EU:to_binary(inet:ntoa(?U:get_host_addr(RemoteAddr))),
    LocalPort = ?U:get_local_port(),
    build_local_contact_srv({LocalAddr,LocalPort}, BaseUri).

build_local_contact_srv({SrvAddr,SrvPort}, BaseUri) ->
    BaseUri#uri{scheme=sip,
                disp= <<>>,
                domain=?EU:to_binary(SrvAddr),
                port=SrvPort,
                opts=?CFG:get_transport_opts(),
                ext_opts=[],
                headers=[]}.

%% ----------------------------------------------------------
%% clear uri from opts
%% ----------------------------------------------------------
clear_uri(#uri{opts=[], ext_opts=[], headers=[]}=Uri) -> Uri;
clear_uri(#uri{}=Uri) -> Uri#uri{opts=[], ext_opts=[], headers=[]}.

clear_uri(#uri{}=Uri, []) -> clear_uri(Uri);
clear_uri(#uri{}=Uri, [user_orig]) -> Uri#uri{opts=[], ext_opts=[], headers=[], user_orig= <<>>}. % #429

clear_uri2(#uri{disp= <<>>, opts=[], ext_opts=[], headers=[]}=Uri) -> Uri;
clear_uri2(#uri{}=Uri) -> Uri#uri{disp= <<>>, opts=[], ext_opts=[], headers=[]}.

%% ----------------------------------------------------------
%% set tag to uri
%% ----------------------------------------------------------
set_uri_tag(#uri{ext_opts=[]}=Uri, Tag) -> Uri#uri{ext_opts=[{<<"tag">>,Tag}]};
set_uri_tag(#uri{ext_opts=ExtOpts}=Uri, Tag) ->
    Uri#uri{ext_opts=[{<<"tag">>,Tag}|lists:keydelete(<<"tag">>,1,ExtOpts)]}.

%% ----------------------------------------------------------
%% update uri user by phonenumber in local DC
%% ----------------------------------------------------------
update_uri_user_to_phonenumber(Uri) ->
    Uri#uri{user= get_sipuser_phonenumber(Uri, [], Uri#uri.user)}.

%% ----------------------------------------------------------
%% update uri display name by AOR in local DC
%% ----------------------------------------------------------
update_uri_display(Uri) ->
    Disp = get_display_name(Uri, Uri#uri.disp),
    Disp1 = unquote_display(Disp),
    Disp2 = quote_display(Disp1),
    Uri#uri{disp=Disp2}.

% @private
%  Gets display name from DC for local URI
get_display_name(#uri{user=User,domain=Domain}, Default) ->
    get_display_name(User, Domain, Default).

get_display_name(<<?IvrPrefix,_/bitstring>> =User, _Domain, _Default) -> make_ivr_display(User);
get_display_name(<<?HuntPrefix,_/bitstring>> =User, _Domain, _Default) -> make_ivr_display(User);
get_display_name(<<?ConfPrefix,_/bitstring>> =User, _Domain, _Default) -> make_conf_display(User);
get_display_name(<<?PromptPrefix,_/bitstring>> =User, _Domain, _Default) -> make_prompt_display(User);
get_display_name(User, Domain, Default) ->
    case ?ACCOUNTS:get_sipuser(User,Domain) of
        {ok,SU} ->
            [Login,Name,Phone] = ?EU:maps_get([login,name,phonenumber],SU),
            ?FROM:apply_modifier_disp(Name, Default, [Default,Login,Phone]);
        _ -> Default
    end.

%% ----------------------------------------------------------
%% quote/unquote display name
%% ----------------------------------------------------------
%%unquote_display(Disp) ->
%%    ?EU:ensure_unquoted(Disp).

%% NOTE! non rfc2822, but yealink working
unquote_display(Disp) ->
    Disp1 = ?EU:to_unicode_binary(string:trim(?EU:to_unicode_list(Disp),both,[32])),
    binary:replace(Disp1, [<<"\"">>,<<"\\">>], <<>>, [global]).
%% NOTE! rfc2822, but yealink not working
%%unquote_display(Disp) ->
%%    try jsx:decode(Disp)
%%    catch _:_ -> Disp
%%    end.

quote_display(<<>>) -> <<>>;
%quote_display(Disp) -> jsx:encode(Disp). % doesn't work on yealink if escaped \" located inside displayname
quote_display(Disp) -> ?EU:ensure_quoted(binary:replace(Disp, [<<"\"">>,<<"\\">>], <<>>, [global])). % ?EU:quote(Disp)

%% ----------------------------------------------------------
%% extract unquoted non empty display name (user if not)
%% ----------------------------------------------------------
extract_display(#uri{disp=Disp,user=User}) ->
    case unquote_display(Disp) of
        <<>> -> User;
        T -> T
    end.

%% ----------------------------------------------------------
%% Extract remoteparty uri or build it by selected uri
%% ----------------------------------------------------------
build_remote_party_id(SipMsg, RUri) ->
    Fdisp = fun() -> ?U:update_uri_display(?U:clear_uri(RUri)) end,
    case extract_remoteparty_uri(SipMsg) of
        undefined -> Fdisp();
        Uri -> Uri
    end.

%% ----------------------------------------------------------
%% Builds remote-party-id opts for sending SIP Request/Response
%% ----------------------------------------------------------
remotepartyid_opts(Uri,'1xx') ->
    [{replace,{?RemotePartyId, Uri#uri{ext_opts=?RemotePartyIdOpts1xx}}},
     {replace,{?PAssertedIdentity, ?U:clear_uri(Uri)}}];
remotepartyid_opts(Uri,'2xx') ->
    [{replace,{?RemotePartyId, Uri#uri{ext_opts=?RemotePartyIdOpts2xx}}},
     {replace,{?PAssertedIdentity, ?U:clear_uri(Uri)}}].

%% ---------------------------
%% makes aors by uris and back
%% ----------------------------------------------------------
make_aor(#uri{scheme=S,user=U,domain=D}) -> {S,U,D}.
make_aors(Uris) -> lists:map(fun(Uri) -> make_aor(Uri) end, Uris).

make_uri({S,U,D}=_AOR) -> #uri{scheme=S, user=U, domain=D}.
make_uris(AORs) -> lists:map(fun(AOR) -> make_uri(AOR) end, AORs).

%% ----------------------------------------------------------
%% conference user
%% ----------------------------------------------------------
make_conf_user(<<?ConfPrefix, _/bitstring>> =U) -> U;
make_conf_user(U) when is_binary(U) -> <<?ConfPrefix, U/bitstring>>.

parse_conf_user(<<?ConfPrefix, U/bitstring>>) -> {ok, clear_conf_user(U)};
parse_conf_user(_) -> false.

clear_conf_user(<<?ConfPrefix, U/bitstring>>) -> clear_conf_user(U);
clear_conf_user(U) -> U.

make_conf_display(U) ->
    {ok,RoomNo} = ?U:parse_conf_user(U),
    <<"Conference ", RoomNo/bitstring>>.

%% ----------------------------------------------------------
%% prompt user
%% ----------------------------------------------------------
make_prompt_user(<<?PromptPrefix, _/bitstring>> =U) -> U;
make_prompt_user(U) when is_binary(U) -> <<?PromptPrefix, U/bitstring>>.

parse_prompt_user(<<?PromptPrefix, U/bitstring>>) -> {ok, clear_prompt_user(U)};
parse_prompt_user(_) -> false.

clear_prompt_user(<<?PromptPrefix, U/bitstring>>) -> clear_prompt_user(U);
clear_prompt_user(U) -> U.

make_prompt_display() -> <<"PROMPT">>.

make_prompt_display(U) ->
    {ok,RoomNo} = ?U:parse_prompt_user(U),
    <<"Prompt ", RoomNo/bitstring>>.

%% ----------------------------------------------------------
%% ivr user
%% ----------------------------------------------------------
make_ivr_user() -> <<"ivr">>.
make_hunt_user() -> <<"hunt">>.

make_ivr_user(<<?DummyIvrPrefix, _/bitstring>> =U) -> U; % RP-1344
make_ivr_user(<<?IvrPrefix, _/bitstring>> =U) -> U;
make_ivr_user(U) when is_binary(U) -> <<?IvrPrefix, U/bitstring>>.

make_hunt_user(<<?HuntPrefix, _/bitstring>> =U) -> U;
make_hunt_user(U) when is_binary(U) -> <<?HuntPrefix, U/bitstring>>.

parse_ivr_user(<<?DummyIvrPrefix, _/bitstring>> =Dummy) -> {ok, {'ivr', Dummy}}; % RP-1344
parse_ivr_user(<<?IvrPrefix, U/bitstring>>) -> {ok, {'ivr', clear_ivr_user(U)}};
parse_ivr_user(<<?HuntPrefix, U/bitstring>>) -> {ok, {'hunt', clear_ivr_user(U)}};
parse_ivr_user(_) -> false.

clear_ivr_user(<<?DummyIvrPrefix, U/bitstring>>) -> clear_ivr_user(U); % RP-1344
clear_ivr_user(<<?IvrPrefix, U/bitstring>>) -> clear_ivr_user(U);
clear_ivr_user(<<?HuntPrefix, U/bitstring>>) -> clear_ivr_user(U);
clear_ivr_user(U) -> U.

make_ivr_display() -> <<"IVR">>.
make_hunt_display() -> <<"HUNT">>.

make_ivr_display(U) ->
    case ?U:parse_ivr_user(U) of
        {ok,{'ivr',<<?DummyIvrPrefix,_/binary>>}} -> <<"dummy">>; % RP-1344
        {ok,{'ivr',N}} -> <<"IVR ", N/bitstring>>;
        {ok,{'hunt',N}} -> <<"HUNT ", N/bitstring>>
    end.

%% =============================================
%% SIP headers
%% =============================================

%% ----------------------------------------------------------
%% parse replaces
%% ----------------------------------------------------------
-spec parse_replaces(Replaces::binary() | string()) -> #{callid=>binary(),fromtag=>binary(),totag=>binary(),opts=>[{Key::binary(),Value::binary()}]}.
%% ----------------------------------------------------------
parse_replaces(Replaces) when is_binary(Replaces) -> parse_replaces(?EU:to_list(Replaces));
parse_replaces(Replaces) when is_list(Replaces) ->
    {_,R} = lists:foldl(fun($;, {AP,AR}) -> {[],[lists:reverse(AP)|AR]};
                           (I, {AP,AR}) -> {[I|AP],AR}
                        end, {[],[]}, lists:reverse([$;|lists:reverse(Replaces)])),
    case R of
        [] -> #{callid => <<>>, fromtag => <<>>, totag => <<>>, opts => []};
        _ ->
            CallId = lists:last(R),
            Fun2 = fun($=) -> false; (_) -> true end,
            Fun3 = fun(I) -> {lists:takewhile(Fun2, I), lists:reverse(lists:droplast(lists:reverse(lists:dropwhile(Fun2, I))))} end,
            Opts = lists:map(Fun3, lists:droplast(R)),
            [FTag, TTag] = ?EU:extract_optional_default([{"from-tag",""}, {"to-tag",""}], Opts),
            #{callid => ?EU:to_binary(CallId),
              fromtag => ?EU:to_binary(FTag),
              totag => ?EU:to_binary(TTag),
              opts => lists:map(fun({K,V}) -> {?EU:to_binary(K),?EU:to_binary(V)} end,
                                lists:keydelete("from-tag",1,lists:keydelete("to-tag",1,Opts)))}
    end.

%% -----
%% parse refer, extract referred-by, refer-to headers from refer request, make headers for invite
%% -----
parse_refer_headers(Req) ->
    {ok,[XRefByV|_]} = nksip_request:header("referred-by", Req),
    {ok,[XRefToV|_]} = nksip_request:header("refer-to", Req),
    %
    [#uri{}=XRefBy|_] = ?U:parse_uris(XRefByV),
    [#uri{headers=XRefToH}=XRefTo|_] = ?U:parse_uris(XRefToV),
    %
    Opts = [{add, ?ReferredBy, XRefByV}],
    Opts1 = case lists:keyfind(?ReplacesLow, 1, XRefToH) of
                {_H,V} ->
                    XReplaces = ?EU:urldecode(?EU:to_list(V)),
                    RepMap = ?U:parse_replaces(XReplaces),
                    [{add, ?Replaces, build_replaces_header(RepMap)} | Opts];
                _ -> Opts
            end,
    {XRefBy, XRefTo#uri{headers=[]}, Opts1}.

%% -----
%% build 'Replaces' header value
%% -----
-spec build_replaces_header(RepMap::map()) -> binary().
%% -----
build_replaces_header(RepMap) when is_map(RepMap) ->
    [CallId,FTag,TTag] = ?EU:maps_get([callid,fromtag,totag],RepMap),
    Opts = maps:get(opts,RepMap,[]),
    OptsB = lists:foldl(fun({K,V},Acc) -> <<Acc/binary,";",(?EU:strbin("~ts=~ts",[K,V]))/binary>> end, <<>>, Opts),
    <<CallId/bitstring,";from-tag=",FTag/bitstring,";to-tag=",TTag/bitstring,OptsB/binary>>.

%% ---------------------------------------------------------------
%% parse x-era-extaccount header
%% ----------------------------------------------------------
parse_ext_account_header(Req) ->
    case nksip_request:header(?ExtAccountHeaderLow, Req) of
        {ok,[H|_]} ->
            Ftrim = fun(T) -> binary:replace(T,<<" ">>,<<>>) end,
            [UD|Opts] = binary:split(Ftrim(H), <<";">>, [global, trim_all]),
            [U,D] = binary:split(UD, <<"@">>, [global]),
            Opts1 = lists:map(fun(A) -> case binary:split(A, <<"=">>, [global]) of [K,V] -> {Ftrim(K),Ftrim(V)}; [K] -> {Ftrim(K),undefined} end end, Opts),
            {{U,D},Opts1};
        _ ->
            undefined
    end.

%% ----------------------------------------------------------
%% parse x-era-bindings header
%% ----------------------------------------------------------
parse_bindings_header(Req) ->
    case nksip_request:header(?BindingsHeaderLow, Req) of
        {ok,[H|_]} ->
            Ftrim = fun(T) -> binary:replace(T,[<<" ">>,<<"\t">>,<<"\n">>,<<"\r">>],<<>>,[global]) end,
            binary:split(Ftrim(H), <<";">>, [global, trim_all]);
        _ ->
            undefined
    end.

%% ----------------------------------------------------------
%% prepares response string for sip message
%% ----------------------------------------------------------
get_response_string(SipCode,Phrase) ->
    BinCode = ?EU:to_binary(SipCode),
    BinPhrase = ?EU:to_binary(Phrase),
    <<"SIP/2.0 ", BinCode/bitstring, " ", BinPhrase/bitstring, "\r\n">>.

%% =============================================
%% Body
%% =============================================

extract_sdp(#sipmsg{content_type=CT,body=Body}) ->
    extract_sdp_auto(CT,Body).

extract_sdp_auto(CT, Body) ->
    case CT of
        {<<"application/",T/binary>>,_} when T== <<"sdp">>; T== <<"SDP">> ->
            case Body of
                #sdp{}=Sdp -> Sdp;
                _ when is_binary(Body) ->
                    case ?U:parse_sdp(Body) of
                        #sdp{}=Sdp -> Sdp;
                        error -> undefined
                    end;
                _ -> undefined
            end;
        {<<"multipart/mixed">>,Opts} -> extract_sdp_from_multipart(CT,Body,Opts);
        {<<"multipart/alternative">>,Opts} -> extract_sdp_from_multipart(CT,Body,Opts);
        _ -> undefined
    end.

-define(MULTIPART, cow_multipart).

% @private
extract_sdp_from_multipart(_CT,Body,Opts) ->
    case lists:keyfind(<<"boundary">>,1,Opts) of
        false -> undefined;
        {_,Boundary} when is_binary(Body) ->
            F = fun ParsePart(B) ->
                        try {ok, Head1, Rest} = ?MULTIPART:parse_headers(B, Boundary),
                             {done, Body1, Rest1} = ?MULTIPART:parse_body(Rest, Boundary),
                             done = ?MULTIPART:parse_body(Rest1, Boundary),
                             case lists:keyfind(<<"content-type">>, 1, Head1) of
                                 false -> ParsePart(Rest1);
                                 {_,CT1} ->
                                     {_,CT2} = ?U:parse_header(<<"content-type">>, CT1),
                                     extract_sdp_auto(CT2, Body1)
                             end
                        catch _:_ -> undefined
                        end end,
            F(Body);
        _ -> undefined
    end.

% =============================================
% Media
% =============================================

%% ----------------------------------------------------------
%% returns media record opts for context
%% ----------------------------------------------------------
get_media_context_rec_opts() ->
    get_media_context_rec_opts(os:timestamp()).
get_media_context_rec_opts(TS) when is_integer(TS) ->
    Timestamp = ?EU:to_list(TS),
    {{Y,M,D},{H,Mi,_}} = ?EU:timestamp_to_datetime(TS),
    RecPath = ?EU:str(":RECORD/Srv-~s/~4..0B-~2..0B-~2..0B/~2..0B:~2..0B/~ts", [?EU:to_list(?U:get_current_srv_textcode()),Y,M,D,H,Mi,Timestamp]),
    {Timestamp,RecPath};
get_media_context_rec_opts({_,_,_}=TS) ->
    Timestamp = ?EU:to_list(?EU:timestamp(TS)),
    {{Y,M,D},{H,Mi,_}} = calendar:now_to_universal_time(TS),
    RecPath = ?EU:str(":RECORD/Srv-~s/~4..0B-~2..0B-~2..0B/~2..0B:~2..0B/~ts", [?EU:to_list(?U:get_current_srv_textcode()),Y,M,D,H,Mi,Timestamp]),
    {Timestamp,RecPath}.

%% ----------------------------------------------------------
%% returns mg devicename by connhandle
%% ----------------------------------------------------------
get_mg_devicename({_,_,{_,DeviceName}}) -> DeviceName.

%% parse device name, returns address and postfix as binary
parse_mg_devicename(DeviceName) ->
    ?ENVCFG:parse_mg_devicename(DeviceName).

% =============================================
% DTMF
% =============================================

%% ----------------------------------------------------------
%% extract dtmf symbols from SIP INFO request
%% ----------------------------------------------------------
get_dtmf_from_sipinfo(Req, FunOk, FunFalse) ->
    case Req#sipmsg.content_type of
        {<<"application/dtmf-relay">>,_} ->
            #sipmsg{body=Body}=Req,
            case is_binary(Body) of
                false -> FunFalse();
                true ->
                    [P1|_] = binary:split(Body, <<"\r\n">>),
                    case P1 of
                        <<"Signal=",Dtmf/bitstring>> ->    FunOk(normalize_dtmf(Dtmf));
                        _ -> FunFalse()
                    end
            end;
        _ -> FunFalse()
    end.

% normalize dtmf symbols
normalize_dtmf(X) when is_list(X) -> ?EU:to_binary(X);
normalize_dtmf(<<"10">>) -> <<"*">>;
normalize_dtmf(<<"11">>) -> <<"#">>;
normalize_dtmf(X) when is_binary(X) -> X.

%% =============================================
%% Headers
%% =============================================

% parse parametrized string into list of props (A=B,C=D | A=B;C=D | A B=C;D=E | A B=C, D=E)
parse_params(H) ->
    lists:map(fun(X) -> case binary:split(X,<<"=">>) of [T] -> T; [T1,T2] -> {T1,T2} end end,
              lists:flatten(
                lists:map(fun(X) -> binary:split(X,[<<",">>,<<";">>],[global,trim_all]) end,
                          binary:split(H,<<" ">>,[global,trim_all])))).

% extract and parse selected custom parameter header to list of props
parse_header_params(#sipmsg{class=Class}=Msg,HLow) ->
    M = case Class of
            {req,_} -> nksip_request;
            {resp,_,_} -> nksip_response
        end,
    case M:header(HLow, Msg) of
        {ok,[]} -> [];
        {ok,[H|_]} -> parse_params(H)
    end.

% #237
% extract and parse 'Proxy-Authorization' or 'Authorization' header
parse_auth(Req) -> parse_header_params(Req, "authorization").


% extract uri from 'Remote-Party-ID', 'P-Asserted-Identity'
extract_remoteparty_uri(#sipmsg{class=Class}=SipMsg) ->
    F = fun(Hdr) ->
                M = case Class of
                        {req,_} -> nksip_request;
                        {resp,_,_} -> nksip_response
                    end,
                case M:header(Hdr, SipMsg) of
                    {ok,[RpidH|_]} ->
                        case ?U:parse_uris(RpidH) of
                            [RpidUri|_] -> RpidUri;
                            _ -> undefined
                        end;
                    _ -> undefined
                end end,
    case F(?RemotePartyIdLow) of
        #uri{}=Uri -> Uri;
        undefined -> F(?PAssertedIdentityLow)
    end.

%% =============================================
%% NkSIP adapter
%% =============================================

%%
make_id(App) -> nkservice:make_id(App).

%%
luid() -> nklib_util:luid().

%%
parse_uris(V) -> nklib_parse:uris(V).
unparse_uri(Uri) -> nklib_unparse:uri(Uri).

%%
unparse_msg(SipMsg) -> nksip_unparse:packet(SipMsg).

%%
store_value(Key, Val, Opts) -> nklib_util:store_value(Key, Val, Opts).
get_value(Key, Opts) -> nklib_util:get_value(Key, Opts).

%%
to_ip(Domain) -> nklib_util:to_ip(Domain).
is_local_ip(Ip) -> nkpacket:is_local_ip(Ip).
is_local_ruri(Req) ->
    #sipmsg{ruri=#uri{domain=Domain}}=Req,
    case inet:parse_address(?EU:to_list(Domain)) of
        {ok,_Addr} -> nksip_request:is_local_ruri(Req);
        _ ->
            case ?LOCALDOMAIN:is_cluster_domain(Domain) of
                true -> true;
                false -> nksip_request:is_local_ruri(Req)
            end end.

%%
is_sdp(Sdp) -> nksip_sdp:is_sdp(Sdp).
parse_sdp(BinSdp) -> nksip_sdp:parse(BinSdp).

%%
parse_header(Name, Value) -> nksip_parse_header:parse(Name, Value).

%% --------------------------------------
make_useropts(system) ->
    make_useropts({?EU:emptyid(),<<"System">>,?EU:to_binary(?CFG:get_general_domain())});
%%
make_useropts(masteradmin) ->
    make_useropts({?EU:emptyid(),<<"MasterAdmin">>,?EU:to_binary(?CFG:get_general_domain())});
%%
make_useropts({script,ScrOpts,Domain}) ->
    Type = maps:get(type,ScrOpts),
    Code = maps:get(code,ScrOpts),
    make_useropts({?EU:emptyid(),?EU:strbin("Script type=~s code='~s'",[Type,Code]),?EU:to_binary(Domain)});
%%
make_useropts({UId,UName,UDomain}) ->
    [{userid,UId},
     {username,UName},
     {userdomain,UDomain}].

%% --------------------------------------
%% only when not ?Auto100answer
%%send_100_trying(_) -> ok;
send_100_trying(Req) ->
    %{ok,ReqHandle} = nksip_request:get_handle(Req),
    % sync, not worker
    {Resp, SendOpts} = nksip_reply:reply(Req, 100),
    nksip_call_uas_transp:send_response(Resp#sipmsg{headers=[]}, SendOpts).

%% ---------------------------------------
ciphers() ->
    Ciphers = ?U:get_ciphers(),
    Sup = ?EU:get_by_key(supported,ssl:versions()),
    Versions = lists:foldl(fun(V,Acc) -> case lists:member(V,Sup) of true -> Acc; false -> Acc++[V] end end, Sup, ['tlsv1.1','tlsv1.2','tlsv1.3']),
    case lists:member(<<"*">>,Ciphers) of
        true ->
            CiphersX = lists:foldl(fun(V,Acc) ->
                                        X = ssl:cipher_suites(all,V),
                                        ssl:append_cipher_suites(X,Acc)
                                   end, [], Versions),
            ssl:filter_cipher_suites(CiphersX,[]);
        false ->
            F = fun F(#{cipher:=_,key_exchange:=_,mac:=_,prf:=_}=Suite,Acc) when is_map(Suite) -> [Suite|Acc];
                    F(CipherName,Acc) when is_binary(CipherName) -> F(ssl:str_to_suite(?EU:to_list(CipherName)),Acc);
                    F(CipherName,Acc) when is_list(CipherName) -> F(ssl:str_to_suite(CipherName),Acc);
                    F(_,Acc) -> Acc
                end,
            CiphersOpts = lists:foldr(F, [], Ciphers),
            %
            FVsn = fun(V,Acc) -> ssl:cipher_suites(default,V) ++ Acc end,
            CiphersVsn = lists:foldl(FVsn, [], Versions),
            %
            CiphersX = ssl:append_cipher_suites(CiphersOpts,CiphersVsn),
            ssl:filter_cipher_suites(CiphersX,[])
    end.

%% --------------------------------------
refresh_nksip_cache_local_ips() ->
    %A = nkpacket_util:get_local_ips_cached(),
    B = nkpacket_util:get_local_ips(),
    ok.
%%    case A==B of
%%        true -> ok;
%%        false ->
%%            ?OUT("Refresh nksip's cache: local_ips", []),
%%            ets:insert(nklib_config, {{nkpacket, none, local_ips}, B}),
%%            nkpacket_util:make_cache()
%%    end.

%% --------------------------------------
send_sip_reply(Fun) when is_function(Fun,0) ->
    % Fun(). % sync
    % spawn(Fun). % async
    ?SipWorkerSrv:cast_workf(Fun). % Worker
