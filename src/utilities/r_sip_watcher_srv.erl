%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc
%%% @todo

-module(r_sip_watcher_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1]).
-export([print/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("../include/r_sip.hrl").

-define(TIMERMSG, {timer_sec}).
-define(LOGENV(Fmt,Args), ?BLlog:write({env,env}, {Fmt,Args})).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(StartArgs) ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, StartArgs, []).

print() ->
    gen_server:cast(?MODULE, {print}).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) ->
    ETS = ets:new(rules,[set]),
    % ets:insert(ETS, {{prim_inet,send,3}, {250,0}}),
    %
    [Rules] = ?EU:extract_optional_default([rules],Opts,[[]]),
    lists:foreach(fun({FData,QLen,Sec}) -> ets:insert(ETS, {FData,{QLen,Sec}}) end, Rules),
    %
    State = #{filter => ETS},
    ?OUT("Watcher. inited (~p)", [Opts]),
    erlang:send_after(1000, self(), ?TIMERMSG),
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

%% other
handle_call(_Request, _From, State) ->
    % ?LOG("Watcher. handle_call(~p)", [_Request]),
    Reply = ok,
    {reply, Reply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

%% print state
handle_cast({print}, State) ->
    ?OUT("Watcher. state: ~120p", [State]),
    {noreply, State};

%% other
handle_cast(_Request, State) ->
    % ?LOG("Watcher. handle_cast(~p)", [_Request]),
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------
handle_info(?TIMERMSG, State) ->
    check_overloaded(State),
    erlang:send_after(1000, self(), ?TIMERMSG),
    {noreply, State};

handle_info(_Info, State) ->
    % ?LOG("Watcher. handle_info(~p)", [_Info]),
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.


%% ====================================================================
%% Internal functions
%% ====================================================================

check_overloaded(State) ->
    List = ?APP_TRACE:get_overloaded(50),
    check_and_kill(List, State).

check_and_kill([],_) -> ok;
check_and_kill([{_,Pid}|Rest], State) ->
    check_and_kill(Pid, State),
    check_and_kill(Rest, State);

check_and_kill(Pid, #{filter:=FilterEts}) when is_pid(Pid) ->
    case process_info(Pid, message_queue_len) of
        {_,N} when N > 50 ->
            case process_info(Pid, current_function) of
                {_,F} ->
                    case ets:lookup(FilterEts, F) of
                        [] ->
                            ?LOGENV("Process ~p found overloaded=~p in ~140p", [Pid, N, F]);
                        [{_,{QLen,_Sec}}] when QLen =< N ->
                            ?LOGENV("Process ~p found overloaded=~p in ~140p. Kill", [Pid, N, F]),
                            exit(Pid,kill)
                    end;
                _ -> ok
            end;
        _ -> ok
    end.
