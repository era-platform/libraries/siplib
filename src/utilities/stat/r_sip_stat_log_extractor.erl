%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 17.05.2016
%%% @doc
%%% @todo

-module(r_sip_stat_log_extractor).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ==========================================================================
%% Types
%% ==========================================================================

-include("../include/r_sip.hrl").

-include("../include/r_sip_nk.hrl").

% @private
-define(LOGL(Fmt,Args), ?BLlog:write(?LOGFILE, {Fmt,Args})).
-define(LOGL_PACKET(T,Cnt), case Cnt rem 10000 of 0 -> ?LOGL("    Extract ~p packet ~p - ~p", [T,Cnt, os:timestamp()]); _ -> ok end).
-define(LOGL_FILESTART(Path), ?LOGL("    Extract from ~p start - ~p", [Path, os:timestamp()])).
-define(LOGL_FILESTOP(Path), ?LOGL("    Extract from ~p stop - ~p", [Path, os:timestamp()])).

%% ====================================================================
%% API functions
%% ====================================================================

extract_trn({_CallIds, _Now, _Dur}=Params) ->
    ?ENV:set_group_leader(),
    Priority = process_flag(priority, low),
    Res = try do_extract(Params)
          catch _:R -> {error, R}
          end,
    process_flag(priority, Priority),
    Res.

% @private
do_extract(Params) ->
    TempDir = ?BLlog:get_basic_log_dir() ++ "/temp/" ++ ?EU:to_list(?EU:timestamp()) ++ "/",
    ?Rfilelib:ensure_dir(TempDir),
    %
    F = fun(_, {error,_}=Err) -> Err;
           (T, Acc) ->
                case do_extract(T,Params,TempDir) of
                    {error,_}=Err -> Err;
                    {ok,TempFile} -> [TempFile|Acc] % TODO remove useless code after ?Rzip:create_from_dir/2 is implemented.
                end end,
    case lists:foldl(F, [], [trn,cdr]) of
        {error,_} = Err -> ?EU:directory_delete(TempDir), Err;
        TempFiles ->
            R = return_zip_data(TempDir, TempFiles),
            ?EU:directory_delete(TempDir),
            R
    end.

do_extract(T,Params,TempDir) ->
    Files = find_log_files(T,Params),
    TempFile = build_filename(T),
    WFilePath = TempDir ++ TempFile,
    case ?Rfile:open(WFilePath, [raw, append, {delayed_write,16384,100}]) of
        {error,_}=Err -> Err;
        {ok,WF} ->
            F = fun(RFilePath) ->
                    case ?Rfile:open(RFilePath, [raw, read, binary]) of
                         {error,_}=Err ->
                            ?Rfile:write(WF, ?EU:strbin("Error reading file: ~120p", [Err]));
                        {ok,RF} ->
                            ?LOGL_FILESTART(RFilePath),
                            ?Rfile:write(WF, ?EU:to_binary(RFilePath)),
                            ?Rfile:write(WF, <<"\r\n\r\n">>),
                            ?U:start_monitor_simple(self(), "Sip log extractor"),
                            FDt = extract_file_date_from_path(RFilePath),
                            try extract(T, Params, FDt, RF, WF, [])
                            catch _:R -> ?Rfile:write(WF, ?EU:strbin("Error reading file: ~120p", [R]))
                            end,
                            ?LOGL_FILESTOP(RFilePath),
                            ?Rfile:close(RF)
                    end end,
            lists:foreach(F, Files),
            ?Rfile:close(WF),
            {ok,TempFile}
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

% @private
% search for log files (trn/cdr), take only last few, that contain requested period in seconds
find_log_files(trn=T, Params) -> find_log_files(T, Params, ?BLlog:get_log_dir(?LOGFILETRN), "^trn_.+\\.\\d+\\.log");
find_log_files(cdr=T, Params) -> find_log_files(T, Params, ?BLlog:get_log_dir(?LOGFILECDR), "^cdr_.+\\.\\d+\\.log").
% @private
find_log_files(T, Params, Dir, Reg) ->
    AllFiles = lists:reverse(lists:sort(?Rfilelib:fold_files(Dir, Reg, false, fun(A,Acc) -> [A|Acc] end, []))),
    filter_files(T, Params, AllFiles).

% @private
% filter log files by requested params
filter_files(T, {undefined,_,_}=Params, AllFiles) ->
    {Files,_} = lists:foldl(fun(_, {_,true}=Acc) -> Acc;
                               (FPath, {Acc,false}) ->
                                    FDt = extract_file_date_from_path(FPath),
                                    case extract_file_first_packet(T,FPath) of
                                        {ok,Packet,_} ->
                                            PacketL = lists:reverse(Packet),
                                            ?OUT("FPath: ~120tp,~nDt -> ~120tp,~nPacket: ~120tp,~ncheck_time -> ~120tp", [FPath, FDt, PacketL, check_time(FDt,Params,PacketL)]),
                                            {[FPath|Acc], not check_time(FDt,Params,PacketL)};
                                        false -> {Acc, true}
                                    end end, {[],false}, AllFiles),
    Files;
filter_files(_T, {_,undefined,undefined}=_Params, AllFiles) ->
    MaxSize = 120 * 1024 * 1024, % Max 120 MB
    {Files,_} = lists:foldl(fun(_, {_,Size}=Acc) when Size>=MaxSize -> Acc;
                               (FPath, {Acc,Size}) -> {[FPath|Acc], Size+?Rfilelib:file_size(FPath)} end, {[],0}, AllFiles),
    filter_files_lwt(Files, 3600).

% @private
% filter log files by last write time (<= LeaveSeconds)
filter_files_lwt(Files, LeaveSeconds) ->
    NowGS = ?EU:current_gregsecond(),
    lists:filter(fun(FPath) ->
                      case ?EU:gregsecond(?Rfilelib:last_modified(FPath)) of
                          LWT when NowGS - LWT > LeaveSeconds -> false; % max 1 hour ahead
                          _ -> true
                      end end, Files).

% @private
extract_file_date_from_path(FPath) ->
    FN = filename:basename(FPath),
    [_|X] = lists:dropwhile(fun($_) -> false; (_) -> true end, FN),
    <<Y:32/bitstring,"-",M:16/bitstring,"-",D:16/bitstring,_/bitstring>> = ?EU:to_binary(X),
    {?EU:to_int(Y),?EU:to_int(M),?EU:to_int(D)}.

% @private
% extract first packet from log file
extract_file_first_packet(T,FPath) ->
    case ?Rfile:open(FPath, [raw, read, binary]) of
         {error,_} -> undefined;
        {ok,RF} ->
            P = try extract_packet(T, RF, [])
                catch _:_ -> undefined
                end,
            ?Rfile:close(RF),
            P
    end.

% @private
extract_packet(trn,RF,Acc) -> extract_trn_packet(RF,Acc);
extract_packet(cdr,RF,Acc) -> extract_cdr_packet(RF,Acc).

% @private
build_filename(trn) -> "trn.log";
build_filename(cdr) -> "cdr.log".

% @private
extract(trn, Params, FDt, RF, WF, FirstLine) -> extract_trn_cnt(Params, FDt, RF, WF, FirstLine, 0);
extract(cdr, Params, FDt, RF, WF, FirstLine) -> extract_cdr_cnt(Params, FDt, RF, WF, FirstLine, 0).

% -----------------------------
% cdr files operations
% -----------------------------

% @private
extract_cdr_cnt(Params, FDt, RF, WF, FirstLine, Cnt) ->
    case extract_cdr_packet(RF, FirstLine) of
        false -> ok;
        {ok,PacketL,FirstLine1} when is_list(PacketL) ->
            ?LOGL_PACKET(cdr,Cnt),
            PacketL1 = lists:reverse(PacketL),
            case check_time(FDt, Params, PacketL1) andalso check_cdr_callid(Params, PacketL1) of
                undefined -> ok;
                false -> ok;
                true ->
                    Packet = lists:foldl(fun(Ln,Acc) -> <<Acc/bitstring,Ln/bitstring>> end, <<>>, PacketL1),
                    ?Rfile:write(WF, Packet)
            end,
            extract_cdr_cnt(Params, FDt, RF, WF, FirstLine1, Cnt+1)
    end.

% @private
extract_cdr_packet(RF, Acc) ->
    case ?Rfile:read_line(RF) of
        eof when Acc==[] -> false;
        eof -> {ok,Acc,[]};
        %
        {error,_}=Err -> throw("error cdr file read: " ++ ?EU:str("~p", [Err]));
        %
        {ok,<<"===",_/bitstring>>} when length(Acc) > 1 -> {ok,Acc,[]};
        {ok,<<"===",_/bitstring>>} -> extract_cdr_packet(RF,[]);
        %
        {ok,<<_:16/bitstring,$:,_:16/bitstring,$:,_:16/bitstring,$.,_:24,$\t,_/bitstring>>=Ln} when length(Acc) > 0 -> {ok,Acc,[Ln]};
        {ok,<<_:16/bitstring,$:,_:16/bitstring,$:,_:16/bitstring,$.,_:24,$\t,_/bitstring>>=Ln} -> extract_cdr_packet(RF, [Ln]);
        {ok,_Ln} when Acc==[] -> extract_cdr_packet(RF, []);
        {ok,Ln} -> extract_cdr_packet(RF, [Ln|Acc])
    end.

% @private
check_cdr_callid({undefined,_,_}, _PacketL) -> true;
check_cdr_callid({CallIds,_,_}=_Params, [Ln1|_]=_PacketL) ->
    case binary:matches(Ln1, CallIds) of
        [] -> false;
        _ -> true
    end.

% -----------------------------
% trn files operations
% -----------------------------

% @private
extract_trn_cnt(Params, FDt, RF, WF, FirstLine, Cnt) ->
    case extract_trn_packet(RF, FirstLine) of
        false -> ok;
        {ok,PacketL,FirstLine1} when is_list(PacketL) ->
            ?LOGL_PACKET(trn,Cnt),
            PacketL1 = lists:reverse([<<"\t\n">>|PacketL]),
            case check_time(FDt, Params, PacketL1) andalso check_trn_callid(Params, PacketL1) of
                undefined -> ok;
                false -> ok;
                true ->
                    Packet = lists:foldl(fun(Ln,Acc) -> <<Acc/bitstring,Ln/bitstring>> end, <<>>, PacketL1),
                    ?Rfile:write(WF, Packet)
            end,
            extract_trn_cnt(Params, FDt, RF, WF, FirstLine1, Cnt+1)
    end.

% @private
extract_trn_packet(_, Acc) when length(Acc)>500 ->
    throw("error trn file format, too large packet found");
extract_trn_packet(RF, Acc) ->
    case ?Rfile:read_line(RF) of
        eof when Acc==[] -> false;
        eof -> {ok,Acc,[]};
        %
        {error,_}=Err when Acc==[] -> throw("error trn file read: " ++ ?EU:str("~p", [Err]));
        {error,_}=Err -> throw("error trn file read: " ++ ?EU:str("~p", [Err]));
        %
        {ok,<<"===",_/bitstring>>} when length(Acc) > 3 -> {ok,Acc,[]};
        {ok,<<"===",_/bitstring>>} -> extract_trn_packet(RF,[]);
        %
        {ok,<<_:16/bitstring,$:,_:16/bitstring,$:,_:16/bitstring,$.,_:24,$\t,_/bitstring>>=Ln} when length(Acc) > 3 -> {ok,Acc,[Ln]};
        {ok,<<_:16/bitstring,$:,_:16/bitstring,$:,_:16/bitstring,$.,_:24,$\t,_/bitstring>>=Ln} -> extract_trn_packet(RF, [Ln]);
        {ok,Ln} -> extract_trn_packet(RF, [Ln|Acc])
    end.

% @private
check_trn_callid({undefined,_,_}, _PacketL) -> true;
check_trn_callid({CallIds,_,_}=_Params, PacketL) when is_list(PacketL) ->
    F = fun(_,Acc) when is_boolean(Acc) -> Acc;
           (Ln,undefined) ->
                Res = case Ln of
                          <<$\t,"Call-ID:",CallId/bitstring>> -> {ok,CallId};
                          <<$\t,"Call-Id:",CallId/bitstring>> -> {ok,CallId};
                          <<$\t,"CALL-ID:",CallId/bitstring>> -> {ok,CallId};
                          <<$\t,"call-id:",CallId/bitstring>> -> {ok,CallId};
                          _ -> false
                      end,
                case Res of
                    {ok,CallId1} ->
                        CallId2 = binary:replace(CallId1, [<<"\r">>,<<"\n">>,<<" ">>,<<"\t">>], <<>>, [global]),
                        lists:member(CallId2, CallIds);
                    _ -> undefined
                end end,
    lists:foldl(F, undefined, PacketL).

% ---------------------------------------------
% Internal functions
% ---------------------------------------------

% @private
check_time(_FDt,{_,undefined,undefined}=_Props, _PacketL) -> true;
check_time(FDt,{_,Now,Dur}=_Props, [Ln|_]=PacketL) when is_list(PacketL) ->
    case Ln of
        <<H:16/bitstring,$:,M:16/bitstring,$:,S:16/bitstring,$.,_:24,$\t,_/bitstring>> ->
            {_Days,Time} = calendar:time_difference({FDt,{?EU:to_int(H),?EU:to_int(M),?EU:to_int(S)}}, Now),
            case calendar:time_to_seconds(Time) of
                Sec when Sec < Dur -> true;
                _ -> false
            end;
        _ -> false
    end.

% @private
return_zip_data(TempDir, TempFiles) ->
    % TODO use ?Rzip:create_from_dir/2 and remove the madness with collecting file names above.
    case ?Rzip:create(TempDir ++ "trn.zip", TempFiles, [{compress,all},{cwd,TempDir}]) of
        {error,_}=Err -> Err;
        {ok,ZFile} ->
            case ?Rfile:read_file(ZFile) of
                {error,_}=Err -> Err;
                {ok,Binary} -> {ok,Binary}
            end
    end.
