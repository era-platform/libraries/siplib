%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 16.05.2016
%%% @doc
%%% @todo

-module(r_sip_stat_req).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([store_req/2,
         store_domain/2,
         store_lnk/1, store_lnk/2,
         store_dialog/2,
         %
         filter_domain/1,
         trace_find/1,
         trace_closure/1]).

%% ==========================================================================
%% Types
%% ==========================================================================

-include("../include/r_sip.hrl").

-include("../include/r_sip_nk.hrl").

-define(TIMEOUT, 12600000). % 10 mins -> 3.5 hours

%% ====================================================================
%% API functions
%% ====================================================================

%% ---------------------------------
%% store incoming sip request data (invite group)
%% ---------------------------------
store_req(Dir, #sipmsg{call_id=CallId}=Req) ->
    Key = {'req', Dir, CallId},
    TS = os:timestamp(),
    TimeUtc = calendar:now_to_universal_time(TS),
    #sipmsg{cseq={CSeq,Method},
            from={FromUri,FTag},
            to={ToUri,TTag},
            nkport=#nkport{remote_ip=RemoteIp}}=Req,
    Item = #{tick => TS,
             timegs => ?EU:current_gregsecond(TimeUtc),
             method => Method,
             cseq => CSeq,
             callid => CallId,
             from => ?U:unparse_uri(?U:clear_uri(FromUri#uri{disp= <<>>})),
             fromtag => FTag,
             to => ?U:unparse_uri(?U:clear_uri(ToUri)),
             totag => TTag,
             remoteip => RemoteIp},
    ?STAT_STORE_Q:store_t(Key, Item, ?TIMEOUT).

%% ---------------------------------
%% RP-415
%% store link of callid to domain
%% ---------------------------------
store_domain(CallId, Domain) ->
    Key = {'domains', CallId},
    FunStore = fun(undefined) -> [Domain]; (List) -> ordsets:add_element(Domain, List) end,
    ?STAT_STORE_Q:func_t(Key, ?TIMEOUT, FunStore).

%% ---------------------------------
%% store empty link between callids
%% ---------------------------------
store_lnk(CallId) ->
    Key = {'lnk', CallId},
    FunStore = fun(undefined) -> []; (List) -> List end,
    ?STAT_STORE_Q:func_t(Key, ?TIMEOUT, FunStore).

%% ---------------------------------
%% store link between callids
%% ---------------------------------
store_lnk(CallId, LinkedCallId) ->
    store_lnk_1(CallId,LinkedCallId),
    store_lnk_1(LinkedCallId,CallId).

%% @private
store_lnk_1(A,B) ->
    Key = {'lnk', A},
    FunStore = fun(undefined) -> [B]; (List) -> [B|List] end,
    ?STAT_STORE_Q:func_t(Key, ?TIMEOUT, FunStore).

%% ---------------------------------
%% #354
%% for sg (to filter duplicates in callid log)
%% ---------------------------------
store_dialog(#sipmsg{class={req,_},call_id=CallId}, Timeout) ->
    Key = {'callid', CallId},
    FunStore = fun(undefined) -> {true,1}; ({true,N}) -> {true,N+1} end,
    FunReply = fun(undefined) -> true; (_) -> false end,
    ?STAT_STORE:func_t(Key, Timeout, FunStore, FunReply).

%% ===============================

%% -------------------------------
%% RP-415
%% return sub list of callids where every callid defines dialog when at least one side belongs to selected domain
%% -------------------------------
filter_domain(Map) ->
    Domain = maps:get(domain,Map,<<>>),
    CallIds = maps:get(callids,Map,[]),
    Fdom = fun(CallId) ->
                   KeyDom = {'domains', CallId},
                   case ?STAT_STORE:find_t(KeyDom) of
                       false -> false;
                       {_,Domains} -> ordsets:is_element(Domain,Domains)
                   end end,
    Flnk = fun(CallId) ->
                KeyLnk = {'lnk', CallId},
                L1 = case ?STAT_STORE:find_t(KeyLnk) of
                         false -> [CallId];
                         {_,L} -> [CallId|L]
                     end,
                lists:any(Fdom,L1)
           end,
    {ok, #{callids => lists:filter(Flnk,CallIds)}}.

%% -------------------------------
%% return list of stored calls (time, tick, remoteip, callid, from, to)
%% -------------------------------
trace_find(Map) ->
    ?ENV:set_group_leader(),
    _NowGS = ?EU:current_gregsecond(),
    FilterFun = get_fun_filter_by_domain_callids(Map), % RP-415
    F = fun({{'req',in,_},V}, Acc) ->
                case FilterFun(maps:get(callid, V)) of
                    false -> Acc;
                    true ->
                        RemoteIp = maps:get(remoteip, V),
                        TS = maps:get(tick, V),
                        {{Y,M,D},{H,Mi,S},Ms} = ?EU:utcdatetime_ms(TS),
                        Time = ?EU:strbin("~4..0B-~2..0B-~2..0BT~2..0B:~2..0B:~2..0B.~2..0B+00:00",[Y,M,D,H,Mi,S,Ms div 10]),
                        Item = maps:merge(maps:with([callid,from,to], V),
                                          #{tick => ?EU:timestamp(TS),
                                            time => Time,
                                            remoteip => ?EU:to_binary(inet:ntoa(RemoteIp))}),
                        [Item|Acc]
                end;
           (_, Acc) -> Acc
        end,
    Calls = ?STAT_STORE:foldl_t(F, []),
    Role = maps:get(role,Map,undefined),
    {ok, #{calls => modify_online_by_media(Role, lists:map(fun(C) -> maps:to_list(C) end, Calls))}}.

%% @private RP-415
get_fun_filter_by_domain_callids(Map) ->
    case maps:get(domain,Map,undefined) of
        undefined -> fun(_) -> true end;
        Domain ->
            EtsCallIds = get_domain_callids(Domain),
            EtsCallIds1 = get_closure(EtsCallIds),
            fun(CallId) -> case ets:lookup(EtsCallIds1, CallId) of [] -> false; [_] -> true end end
    end.

%% @private RP-415
get_domain_callids(Domain) ->
    F = fun({{'domains',CallId},V},Acc) ->
                case lists:member(Domain,V) of
                    true when is_reference(Acc) -> ets:insert(Acc, {CallId,0}), Acc;
                    true when is_list(Acc) -> [CallId|Acc];
                    false -> Acc
                end;
           (_,Acc) -> Acc
        end,
    ?STAT_STORE:foldl_t(F, ets:new(tmp,[public,set])).

%% @private
modify_online_by_media(Role, Calls) ->
    case Role of
        b2bua -> modify_online_by_media_1(Role, r_sip_b2bua_fsm, Calls);
        esg -> modify_online_by_media_1(Role, r_sip_esg_fsm, Calls);
        conf -> modify_online_by_media_1(Role, r_sip_conf_fsm, Calls);
        ivr -> modify_online_by_media_1(Role, r_sip_ivr_fsm, Calls);
        _ -> Calls
    end.

%% @private
modify_online_by_media_1(_Role,Fsm,Calls) ->
    Fonline = fun(Item, {AccL,AccMap}=Acc) ->
                      [CallId] = ?EU:extract_required_props([callid], Item),
                      case Fsm:pull_fsm_by_callid(CallId) of
                          false -> {[Item|AccL],AccMap};
                          DlgX ->
                              case Fsm:get_current_media_link(DlgX) of
                                  {ok,#{}=M} -> modify_online_by_media_2(Item, M, Acc);
                                  _ -> {[Item ++ [{media, false}]|AccL],AccMap}
                              end end end,
    {Calls1,_} = lists:foldr(Fonline, {[],maps:new()}, Calls),
    Calls1.

%% @private
modify_online_by_media_2(Item, M, {AccL,AccMap}) ->
    {ok,{MGC,MgcNode,MgcSrvIdx},AccMap1} = get_mgc_props(M,AccMap),
    DeviceName = ?U:get_mg_devicename(maps:get(mgid,M)),
    Item1 = Item ++ [{media, [{mgc,MGC},
                              {mgc_srvidx,MgcSrvIdx},
                              {mgc_node,MgcNode},
                              {mg,?EU:to_binary(DeviceName)},
                              {msid, maps:get(msid,M)},
                              {ctx, maps:get(ctx,M)}]}],
    {[Item1|AccL], AccMap1}.

%% @private
get_mgc_props(M, AccMap) ->
    MGC = maps:get(mgc,M),
    case maps:find(MGC,AccMap) of
        {_,MGCProps} -> AccMap1 = AccMap;
        error ->
            MGCProps = ?ENVCROSS:call_server({?CFG:get_current_site(),MGC}, {get_node_props}, undefined),
            AccMap1 = maps:put(MGC,MGCProps,AccMap)
    end,
    case MGCProps of
        {ok,{Node,SrvIdx}} -> {ok,{MGC,Node,SrvIdx},AccMap1};
        _ -> {ok,{MGC,undefined,-1},AccMap1}
    end.

%% --------------------------------
%% return closure of callids (by stored links)
%% --------------------------------
trace_closure(Map) ->
    ?ENV:set_group_leader(),
    CallIds = maps:get(callids,Map,[]),
    {ok, #{closure => get_closure(CallIds)}}.

%% @private
get_closure(L) when is_list(L) ->
    get_closure([], lists:usort(L));
%%
get_closure(Ets) when is_reference(Ets) ->
    get_closure(Ets,0).

%% @private
get_closure(L,[]) when is_list(L) -> L;
%% % excluding absent callid from request
get_closure(L,CallIds) when is_list(L), is_list(CallIds) ->
    {L1, CallIds1} = lists:foldl(fun(C, {AccL, AccC}) ->
                                        case get_linked(C) of
                                            undefined -> {AccL,AccC};
                                            Lst -> {[C|AccL], AccC ++ Lst}
                                        end end, {L,[]}, CallIds),
    L2 = ordsets:from_list(L1),
    CallIds2 = ordsets:from_list(CallIds1),
    CallIds3 = ordsets:subtract(CallIds2, L2),
    get_closure(L2, CallIds3);
%%
get_closure(Ets,Level) when is_reference(Ets), is_integer(Level) ->
    NewCnt = ets:foldl(fun({K,V},Acc) when V==Level ->
                                case get_linked(K) of
                                    undefined -> Acc;
                                    Lst ->
                                        lists:foldl(fun(NC,Acc0) ->
                                            case ets:lookup(Ets,NC) of
                                                [] ->
                                                    ets:insert(Ets,{NC,Level+1}),
                                                    Acc0 + 1;
                                                _ -> Acc0
                                            end end, Acc, Lst)
                                end;
                          (_,Acc) -> Acc
                       end, 0, Ets),
    case NewCnt of
        0 -> Ets;
        _ -> get_closure(Ets)
    end.

%% @private
get_linked(CallId) when is_binary(CallId) ->
    Key = {'lnk', CallId},
    case ?STAT_STORE:find_t(Key) of
        false -> undefined;
        {_,Lst} -> Lst
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================
