%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 20.03.2018
%%% @doc Genserver of storing queue.
%%%      Check prior properties before storing or skip.
%%%      Skip store if trn disabled or size of ets is over limited

-module(r_sip_stat_store_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/0]).
-export([store_t/3,
         func_t/3]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("../include/r_sip.hrl").

-define(LIMIT_SIZE, 300000). % limit size (count) of stat_store ets
-define(REFRESH_PORTION, 100).

%% ====================================================================
%% API functions
%% ====================================================================

start_link() ->
    gen_server:start_link({local,?MODULE}, ?MODULE, [], []).

%%
store_t(Key, Item, Timeout) ->
    gen_server:cast(?MODULE, {?STAT_STORE, store_t, [Key,Item,Timeout]}).

%%
func_t(Key, Timeout, FunStore) ->
    gen_server:cast(?MODULE, {?STAT_STORE, func_t, [Key,Timeout,FunStore]}).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(_Opts) ->
    State = #{cnt => 0,
              size => 0},
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

handle_call(_Request, _From, State) ->
    {reply, undefined, State}.

%% ------------------------------
%% Cast
%% ------------------------------

handle_cast({?STAT_STORE, Method, Args}, State) ->
    case ?U:is_trn_enabled() of
        false ->
            {noreply, State};
        true ->
            State1 = case refresh_state(State) of
                         {ok,State1a,Size} when Size >= ?LIMIT_SIZE -> State1a;
                         {ok,State1a,_Size} ->
                             erlang:apply(?STAT_STORE,Method,Args),
                             State1a
                     end,
            {noreply, State1}
    end;

handle_cast(_Msg, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

% other
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% update state's cnt, size, trn every ?REFRESH_PORTION store operations
refresh_state(State) ->
    Cnt = maps:get(cnt,State)+1,
    Size = case Cnt rem ?REFRESH_PORTION of
               0 -> element(2,?STAT_STORE:size());
               _ -> maps:get(size,State)
           end,
    State1 = State#{size => Size,
                    cnt => Cnt},
    {ok,State1,Size}.