%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 16.05.2016
%%% @doc
%%% @todo

-module(r_sip_stat_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([incoming_request/1,
         link_domain_filter/3, link_domain/2,
         link_fork/1, link_fork/2,
         link_replaced/2,
         store_dialog/2,
         sip_trace/2]).

%% ==========================================================================
%% Types
%% ==========================================================================

-include("../include/r_sip.hrl").

-include("../include/r_sip_nk.hrl").

-define(TIMEOUT, 600000).

%% ====================================================================
%% API functions
%% ====================================================================

%% ---------------------------------
%% store
%% consider processed request to stat store
%% ---------------------------------
incoming_request(#sipmsg{cseq={_,'INVITE'}, call_id=CallId}=Req) ->
    ?STAT_REQ:store_req('in',Req),
    ?STAT_REQ:store_lnk(CallId);
incoming_request(_) -> ok.

%% ---------------------------------
%% RP-415
%% store
%% link sip callid to sip dialog's domain (1-1)
%% ---------------------------------
link_domain_filter(Method, CallId, Domain)
  when Method=='INVITE'; Method=='CANCEL'; Method=='REFER'; Method=='BYE' ->
    link_domain(CallId,Domain);
link_domain_filter(_,_,_) -> ok.
% ---
link_domain(CallId, Domain) ->
    ?STAT_REQ:store_domain(CallId, Domain).

%% ---------------------------------
%% store
%% link outgoing fork request's BCallId to ACallId
%% ---------------------------------
link_fork(BCallId, ACallId) ->
    ?STAT_REQ:store_lnk(ACallId, BCallId),
    ?STAT_REQ:store_lnk(BCallId, ACallId).

%% ---------------------------------
%% store
%% outgoing fork request's BCallId link to []
%% ---------------------------------
link_fork(BCallId) ->
    ?STAT_REQ:store_lnk(BCallId).

%% ---------------------------------
%% store
%% link replacing incoming request's CallId to replaced one
%% ---------------------------------
link_replaced(ReplacedCallId, CallId) ->
    ?STAT_REQ:store_lnk(CallId, ReplacedCallId),
    ?STAT_REQ:store_lnk(ReplacedCallId, CallId).

%% ---------------------------------
%% #354
%% store callid by sg
%% ---------------------------------
store_dialog(#sipmsg{cseq={_,M}}=Req,Timeout)
  when M=='INVITE';
       M=='UPDATE';
       M=='CANCEL';
       M=='REFER';
       %M=='NOTIFY';
       M=='BYE' ->
    ?STAT_REQ:store_dialog(Req,Timeout);
store_dialog(_,_) -> false.

%% ===========================================

%% ---------------------------------
%% trace request
%% ---------------------------------
sip_trace(<<"calls_clear">>, _Map) ->
    ?STAT_STORE:clear_t(),
    ?STAT_STORE:clear_u(),
    {ok, #{result => true}};

%% --------
sip_trace(<<"calls_read">>, Map) ->
    ?STAT_REQ:trace_find(Map);

%% --------
sip_trace(<<"filter_domain">>, Map) ->
    ?STAT_REQ:filter_domain(Map);

%% --------
sip_trace(<<"calls_closure">>, Map) ->
    ?STAT_REQ:trace_closure(Map);

%% --------
sip_trace(<<"trn_by_closure">>, Map) ->
    {ok, M} = ?STAT_REQ:trace_closure(Map),
    case maps:get(closure,M,[]) of
        [] -> {ok, #{res => false,
                     reason => {skip, "closure not found"}}};
        _ ->
            CallIds = maps:get(callids,Map,[]),
            case ?STAT_LOGEXTRACTOR:extract_trn({CallIds,undefined,undefined}) of
                {error,_}=Err -> {ok, #{res => false,
                                        result => Err}};
                {ok,Binary} -> {ok, #{res => true,
                                      data => Binary}}
            end end;

%% --------
sip_trace(<<"trn_by_sec">>, Map) ->
    case maps:get(sec,Map,undefined) of
        Sec when is_integer(Sec), Sec>0, Sec<86400 ->
            case ?STAT_LOGEXTRACTOR:extract_trn({undefined,erlang:localtime(),Sec}) of
                {error,_}=Err -> {ok, #{res => false,
                                        result => Err}};
                {ok,Binary} -> {ok, #{res => true,
                                      data => Binary}}
            end;
        _ -> {ok, #{res => false,
                    result => {error, {invalid_params, <<"Invalid 'sec'. Expected seconds::integer > 0, < 86400">>}}}}
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================
