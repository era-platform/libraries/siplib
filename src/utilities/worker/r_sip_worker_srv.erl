%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2022 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 24.02.2022
%%% @doc Common sip worker srv

-module(r_sip_worker_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1, stop/1]).
-export([call_work/3, cast_work/3, cast_work/2,
         call_workf/1, cast_workf/1, call_workf/2, cast_workf/2, call_workf/3]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("r_sip.hrl").

-define(WSUPV, ?SipWorkerSupv).
-define(WORKER(A), erlang:phash2(A) rem ?WSUPV:get_worker_count()).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Name) ->
    gen_server:start_link({local, Name}, ?MODULE, [{name, Name}], []).

stop(Name) ->
    gen_server:stop(Name).

%% work (all workers)
call_work(M,F,A) ->
    gen_server:call(?WSUPV:get_worker_name(?WORKER(A)), {work,M,F,A}).

cast_work(M,F,A) ->
    gen_server:cast(?WSUPV:get_worker_name(?WORKER(A)), {work,M,F,A}).

cast_work(HashCode, {M,F,A}) ->
    gen_server:cast(?WSUPV:get_worker_name(?WORKER(HashCode)), {work,M,F,A}).

%% work (all workers)
call_workf(F) when is_function(F) ->
    gen_server:call(?WSUPV:get_worker_name(?WORKER(F)), {workf,F}).

cast_workf(F) when is_function(F) ->
    gen_server:cast(?WSUPV:get_worker_name(?WORKER(F)), {workf,F}).

call_workf(HashCode,F) when is_function(F) ->
    gen_server:call(?WSUPV:get_worker_name(?WORKER(HashCode)), {workf,F}).

cast_workf(HashCode, F) when is_function(F) ->
    gen_server:cast(?WSUPV:get_worker_name(?WORKER(HashCode)), {workf,F}).

call_workf(HashCode,F,Timeout) when is_function(F),is_integer(Timeout) ->
    gen_server:call(?WSUPV:get_worker_name(?WORKER(HashCode)), {workf,F}, Timeout).


%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) ->
    {Name} = parse_opts(Opts),
    State1 = Name,
    ?LOG(" Environment: Worker ~p inited", [Name]),
    {ok, State1}.

%% parse start options
parse_opts(Opts) ->
    {_, Name} = lists:keyfind(name, 1, Opts),
    {Name}.

%% ------------------------------
%% Call
%% ------------------------------

handle_call({work,M,F,A}, _From, State) ->
    {reply, erlang:apply(M,F,A), State};

handle_call({workf,F}, _From, State) ->
    {reply, F(), State};

handle_call(_Request, _From, State) ->
    {reply, undefined, State}.

%% ------------------------------
%% Cast
%% ------------------------------

handle_cast({work,M,F,A}, State) ->
    erlang:apply(M,F,A),
    {noreply, State};

handle_cast({workf,F}, State) ->
    F(),
    {noreply, State};

handle_cast(_Request, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

%% other
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.
