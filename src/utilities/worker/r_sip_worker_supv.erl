%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2022 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 24.02.2022
%%% @doc Common sip worker's supv

-module(r_sip_worker_supv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([start_link/1]).
-export([init/1]).
-export([get_worker_count/0, get_worker_name/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("r_sip.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Args) ->
    supervisor:start_link(?MODULE, Args).

get_worker_count() -> 48.

get_worker_name(0) -> sip_worker_0;
get_worker_name(1) -> sip_worker_1;
get_worker_name(2) -> sip_worker_2;
get_worker_name(3) -> sip_worker_3;
get_worker_name(4) -> sip_worker_4;
get_worker_name(5) -> sip_worker_5;
get_worker_name(6) -> sip_worker_6;
get_worker_name(7) -> sip_worker_7;
get_worker_name(8) -> sip_worker_8;
get_worker_name(9) -> sip_worker_9;
get_worker_name(10) -> sip_worker_10;
get_worker_name(11) -> sip_worker_11;
get_worker_name(12) -> sip_worker_12;
get_worker_name(13) -> sip_worker_13;
get_worker_name(14) -> sip_worker_14;
get_worker_name(15) -> sip_worker_15;
get_worker_name(16) -> sip_worker_16;
get_worker_name(17) -> sip_worker_17;
get_worker_name(18) -> sip_worker_18;
get_worker_name(19) -> sip_worker_19;
get_worker_name(20) -> sip_worker_20;
get_worker_name(21) -> sip_worker_21;
get_worker_name(22) -> sip_worker_22;
get_worker_name(23) -> sip_worker_23;
get_worker_name(24) -> sip_worker_24;
get_worker_name(25) -> sip_worker_25;
get_worker_name(26) -> sip_worker_26;
get_worker_name(27) -> sip_worker_27;
get_worker_name(28) -> sip_worker_28;
get_worker_name(29) -> sip_worker_29;
get_worker_name(30) -> sip_worker_30;
get_worker_name(31) -> sip_worker_31;
get_worker_name(32) -> sip_worker_32;
get_worker_name(33) -> sip_worker_33;
get_worker_name(34) -> sip_worker_34;
get_worker_name(35) -> sip_worker_35;
get_worker_name(36) -> sip_worker_36;
get_worker_name(37) -> sip_worker_37;
get_worker_name(38) -> sip_worker_38;
get_worker_name(39) -> sip_worker_39;
get_worker_name(40) -> sip_worker_40;
get_worker_name(41) -> sip_worker_41;
get_worker_name(42) -> sip_worker_42;
get_worker_name(43) -> sip_worker_43;
get_worker_name(44) -> sip_worker_44;
get_worker_name(45) -> sip_worker_45;
get_worker_name(46) -> sip_worker_46;
get_worker_name(47) -> sip_worker_47;
get_worker_name(A) -> ?EU:to_atom_new("sip_worker_" ++ ?EU:to_list(A)).


%% ====================================================================
%% Callbacks
%% ====================================================================

init(Args) ->
    ChildSpec = prepare_child_specs(lists:seq(0,get_worker_count()-1), []),
    ?LOG('$trace', " SIP: worker supervisor inited (~p)", [Args]),
    {ok, {{one_for_one, 10, 2}, ChildSpec}}.

%% ====================================================================
%% Internal functions
%% ====================================================================

prepare_child_specs([], ChildSpecs) -> lists:reverse(ChildSpecs);
prepare_child_specs([F|Rest], ChildSpecs) ->
    Name = get_worker_name(F),
    Spec = {Name, {?WORKER, start_link, [Name]}, permanent, 1000, worker, [?WORKER]},
    prepare_child_specs(Rest, [Spec|ChildSpecs]).


